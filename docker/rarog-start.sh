#!/usr/bin/env bash

mkdir -p $RAROG_HOME/config $RAROG_HOME/logs $RAROG_HOME/data

export JAVA_OPTS="-Dhome.directory=$RAROG_HOME $JVM_ARGS"

if [[ ! -z "$DATABASE_URL" ]]; then
echo "spring.datasource.driver-class-name=$DATABASE_DRIVER
spring.datasource.querydsl-templates-class-name=$DATABASE_TEMPLATES
spring.datasource.url=$DATABASE_URL
spring.datasource.password=$DATABASE_PASSWORD
spring.datasource.username=$DATABASE_USERNAME
" > $RAROG_HOME/config/database.properties
fi

catalina.sh run $@
