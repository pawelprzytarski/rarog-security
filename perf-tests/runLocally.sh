#!/usr/bin/env bash

SCRIPT_DIR=$( dirname -- "$0"; )

# remember to copy template_.env.sh to .env.sh and fill values in it
source "$SCRIPT_DIR/.env.sh"

docker inspect --type=image k6-rarog > /dev/null 2>&1 || $SCRIPT_DIR/docker/build.sh

docker run --rm --env K6_INFLUXDB_TOKEN="$K6_INFLUXDB_TOKEN" --env TEST_SERVER_BASE_URL=$TEST_SERVER_BASE_URL --network host -v $(pwd)/$SCRIPT_DIR/tests:/tests -i k6-rarog

exit $?
