// IMPORT: users_10k
import {Faker} from "k6/x/faker";
import csv from 'k6/experimental/csv'
import {open} from 'k6/experimental/fs'
import {browser} from 'k6/browser';
import file from 'k6/x/file'

export const options = {
    scenarios: {
        default: {
            executor: "constant-vus",
            duration: "1m",
            vus: 2,
            options: {
                browser: {
                    type: "chromium"
                }
            }
        }
    },
    thresholds: {
        // values taken based on local tests on MacOS M3
        'iteration_duration': ['avg<1000', 'p(95)<1500'],
        'browser_web_vital_lcp': ['p(95)<300'],
        'browser_web_vital_fid': ['p(95)<1'],
        'browser_web_vital_cls': ['p(95)<0.1']
    }
}
const faker = new Faker();
let users;
(async () => {
    users = await csv.parse(await open('../util/data/users_10k.csv'), {skipFirstLine: true});
})();

export default async function () {
    const user = users[faker.numbers.number(0, users.length - 1)]
    const page = await browser.newPage();
    try {
        await page.goto(`${__ENV.TEST_SERVER_BASE_URL}/login/`);
        await page.type("#login", user[1]);
        await page.type("#password", user[0])
        await page.click("#submit")
        await page.waitForNavigation()
    } catch (e) {
        const errorCode = faker.numbers.int8();
        console.log("Fail " + errorCode + " Error: " + e)
        file.writeBytes('/results/smoke/error-' + errorCode + '.png', await page.screenshot())
    } finally {
        await page.close();
    }
}
