import http from 'k6/http';

import { check } from 'k6';

export const options = {
    vus: 10,
    duration: '10m',
    thresholds: {
        http_req_duration: ['p(95)<5']
    }
}

export default function () {
    const res = http.get(`${__ENV.TEST_SERVER_BASE_URL}/status`);
    check(res, {
        'is status 200:': r => r.status === 200,
        'body contains expected content': r => r.body.includes('"serverStatus":"Ok"')
    })
}
