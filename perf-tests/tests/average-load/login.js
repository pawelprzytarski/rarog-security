// IMPORT: users_10k
import * as login from '../smoke/login.js'

export const options = {
    scenarios: {
        default: {
            executor: "constant-vus",
            duration: "1m",
            vus: 10,
            options: {
                browser: {
                    type: "chromium"
                }
            }
        }
    },
    thresholds: {
        'iteration_duration': ['avg<1000', 'p(95)<10000'],
        'browser_web_vital_lcp': ['p(95)<2500'], //https://web.dev/articles/lcp#what-is-a-good-lcp-score
        'browser_web_vital_fid': ['p(95)<100'], // https://web.dev/articles/fid#what_is_a_good_fid_score
        'browser_web_vital_cls': ['p(95)<0.1'] // https://web.dev/articles/cls#what-is-a-good-cls-score
    }
}

export default async function () {
    await login.default() // just reusing existing code
}
