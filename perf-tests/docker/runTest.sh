#!/usr/bin/env ash

if [[ "$TESTS_LOCATION" != "*/" ]]; then
  TESTS_LOCATION=$TESTS_LOCATION/
fi;

if [[ -z "$1" ]];then
  exit 0
fi

echo "Running test $1"

TEST_TAG=${1#"$TESTS_LOCATION"}
TEST_FILE=$1
RESULT_FILE="${TEST_FILE/$TESTS_LOCATION//results/}.json"
mkdir -p -- "${RESULT_FILE%/*}"

FIRST_LINE=$(head -1 "$TEST_FILE")

echo "$FIRST_LINE"

if [[ "$FIRST_LINE" == "// IMPORT: *" ]]; then
  set +e
  IMPORT_SQL=${FIRST_LINE:11}
  echo "Importing ${IMPORT_SQL}"
  curl -u "perfTestUser:${TOMCAT_PASSWORD}" -I --silent "${TEST_SERVER_BASE_URL}/manager/text/stop?path=/"

  responseCode=0
  count=0
  while [[ $responseCode != 404 && $count -lt 1200 ]] ; do
    responseCode=$(curl -I -w '%{http_code}\n' --silent -o /dev/null "${TEST_SERVER_BASE_URL}/status");
    ((count=count+1))
    sleep 0.1
  done;

  gunzip -c "${TESTS_LOCATION}util/data/${IMPORT_SQL}.sql.gz"| PGPASSWORD=RarogTest psql -U RarogTest -h postgres -p 5432 -d Rarog
  curl -u "perfTestUser:${TOMCAT_PASSWORD}" ${TEST_SERVER_BASE_URL}/manager/text/start?path=/

  responseCode=302
  count=0
  while [[ $responseCode == 302 && $count -lt 1200 ]] ; do
    responseCode=$(curl -I -w '%{http_code}\n' --silent -o /dev/null "${TEST_SERVER_BASE_URL}/login/")
    ((count=count+1))
    sleep 0.1
  done;

  set -e
fi

k6 run -o xk6-influxdb --summary-export="$RESULT_FILE" "$TEST_FILE" --tag FILE="$TEST_TAG"

EXIT_CODE=$?

if [[ $DUMP_DATABASE ]]; then
  echo "Dumping database to ${TEST_FILE/$TESTS_LOCATION//results/}.sql"
  PGPASSWORD=RarogTest pg_dump -U RarogTest -h postgres -p 5432 --clean Rarog | gzip > "${TEST_FILE/$TESTS_LOCATION//results/}.sql.gz"
fi

exit $EXIT_CODE
