#!/usr/bin/env bash

cp -r /usr/local/tomcat/webapps.dist/manager /usr/local/tomcat/webapps/manager;
cp /tmp/context.xml /usr/local/tomcat/webapps/manager/META-INF/context.xml;
cp /tmp/tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml;
if [[ -z "${TOMCAT_PASSWORD}" ]];then
  echo "TOMCAT_PASSWORD cannot be empty"
  exit 1
fi;
sed -i -e "s/{TOMCAT_PASSWORD}/${TOMCAT_PASSWORD}/g" /usr/local/tomcat/conf/tomcat-users.xml
./rarog-start.sh
