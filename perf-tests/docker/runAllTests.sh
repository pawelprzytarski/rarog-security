#!/usr/bin/env ash

SCRIPT_DIR=$( dirname -- "$0"; )

TEST_LOOKUP_DIR=$TESTS_LOCATION

a=$TEST_TYPE; div=','; set --
while
    b=${a#*"$div"}
    set -- "$@" "${a%%"$div"*}"
    [ "$a" != "$b" ]
do
    a=$b
done

if [[ "$SINGLE_TEST" ]];then
  echo "Running single test $TESTS_LOCATION$SINGLE_TEST"
  "$SCRIPT_DIR/runTest.sh" "$TESTS_LOCATION$SINGLE_TEST"
  exit $?
fi;

for TEST_TYPE in "$@";do
  if [[ $TEST_TYPE != "ALL" ]]; then
    TEST_LOOKUP_DIR="$TESTS_LOCATION$TEST_TYPE"
  fi

  echo "Running func tests in $TEST_LOOKUP_DIR"
  find "$TEST_LOOKUP_DIR" -wholename *.js -not -path "$TESTS_LOCATION/util/*" -print0 | xargs -0n1 $SCRIPT_DIR/runTest.sh

  RETURN_CODE=$?
  if [[ $RETURN_CODE -ne 0 ]];then
    exit $RETURN_CODE
  fi
done

exit $RETURN_CODE
