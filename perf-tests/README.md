# Perf tests

This directory contains set of perf tests prepared for Rarog Platform. We decided to use K6 as testing framework, due
to its versatility and popularity. However, it comes with set of restrictions, you need to be aware of when writing
tests. This document describes this restrictions, as well as general guideline for writing tests.

## Basics

Tests are located in `tests` directory. Directory `tests/util` do not contain tests, but rather datasets and shared 
pieces of code. Tests added to this directory will not be executed.

If you are running your tests using locally running Rarog (aka not on docker), you need to copy file `template_.env.sh` 
to `.env.sh` and replace placeholder values in `.env.sh` with valid tokens and configuration variables.

Results of tests will show up in directory `results`. These are summaries of tests. Full tests results should be reported
to InfluxDB and viewed with Grafana. If you want to report to your local InfluxDB instance see `docker/Dockerfile` for
which environmental variables to override.

## Preparing service

K6 works on assumption that tested service is already running at the time of test. For this reason before running tests
we start docker-compose with Postgres and Rarog, to satisfy this requirement. If you use `./rmake.py perf-tests` you
don't need to worry about it. You just must make sure that you have docker installed and running. However rmake may
ask you for admin password if you OS configuration require elevated permissions for running docker commands.

## No advanced data preparation

As K6 assume that tested service exists externally to K6 tests, there is no any advanced environment setup like
restoring database, setting up cluster, etc. K6 assume service is ready for testing as it is. As it seems not reasonable
to setup Rarog instance with potentially millions of rows in database, we prepared small script that restores database
before running K6 test. This script detected if first line of test contains `// IMPORT: <sql file name>` and if it does,
then restores requested file and restart Rarog server. For example `// IMPORT users_10k` will
restore `util/data/users_10k.sql.gz`.

However, we intend to eventually run tests on staging Rarog instance, where dynamic data restore will not be available.
So please stick to preexisting presets (as they will be available also on staging) or if you need to add new set make
sure, you updated all backup files. And contact maintainers to make sure that your new data align with overall direction
of Rarog.

## Categories of tests

Performance tests are split into different categories, to be able easily run relevant subset of tests. Currently
existing
categories are:

* smoke - tests intended to check if basic functionalities did not suffer fatal performance impact. These tests
  are intended to be run locally and very often on CI
* average-load - tests intended to simulated average load we plan to support with Rarog. You can treat is as promise
  we give you, users and admins of Rarog Platform
* soak - tests intended to make sure there is no long term degradations in system (most likely due to problems with
  caches indexes or long-running tasks).
* stress - tests intended to evaluate how much traffic Rarog Platform can support, by creating very high load scenarios

