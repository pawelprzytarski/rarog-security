var homeFile = new File(basedir, "target/home")
assert homeFile.exists()

var databaseConfigFile = new File(homeFile, "config/database.properties")
assert databaseConfigFile.exists()

var databaseConfig = (String) databaseConfigFile.text
assert databaseConfig.contains("spring.datasource.driver-class-name=org.h2.Driver")
assert databaseConfig.contains("spring.datasource.querydsl-templates-class-name=com.querydsl.sql.H2Templates")
assert databaseConfig.contains("spring.datasource.url=jdbc:h2:file:${basedir}/target/db/rarog;USER=sa;PASSWORD=sa;MODE=PostgreSQL;DATABASE_TO_LOWER=TRUE;CASE_INSENSITIVE_IDENTIFIERS=TRUE")
assert databaseConfig.contains("spring.datasource.password=sa")
assert databaseConfig.contains("spring.datasource.username=sa")

var watchListFile = new File(homeFile, "config/reload_watchlist.txt")
assert watchListFile.exists()
assert watchListFile.text.contains("/target/test-1.0.0-test.jar")

var pluginsDirectory = new File(homeFile, "plugins/")
assert pluginsDirectory.exists()
assert new File(pluginsDirectory, "quick-reload-plugin.jar").exists()
assert new File(pluginsDirectory, "backdoor-plugin.jar").exists()
assert new File(pluginsDirectory, "telemetry-plugin.jar").exists()
assert new File(pluginsDirectory, "test-1.0.0-test.jar").exists()

var logsDirectory = new File(homeFile, "logs/")
assert new File(logsDirectory, "catalina.log").exists()
var rarogLogFile = new File(logsDirectory, "rarog.log")
assert rarogLogFile.exists()
assert rarogLogFile.text.contains("Rarog Platform started successfully!")
assert rarogLogFile.text.contains("Shutting down plugins system")
