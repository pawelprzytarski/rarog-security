import java.util.jar.JarFile

var manifestFile = new File(basedir, "target/classes/META-INF/MANIFEST.MF")
var rarogFile = new File(basedir, "target/classes/META-INF/rarog-plugin.json")
var libFiles = new File(basedir, "target/classes/META-INF/lib")
var artifactFile = new File(basedir, "target/test-1.0.0-test.jar")

assert manifestFile.exists()
assert rarogFile.exists()
assert libFiles.exists()
assert libFiles.listFiles().length == 1
assert artifactFile.exists()

var jar = new JarFile(artifactFile)
assert jar.getEntry("META-INF/MANIFEST.MF") != null
assert jar.getEntry("META-INF/rarog-plugin.json") != null
assert jar.getEntry("META-INF/lib") != null