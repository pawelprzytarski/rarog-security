var manifestFile = new File(basedir, "target/classes/META-INF/MANIFEST.MF")
var rarogFile = new File(basedir, "target/classes/META-INF/rarog-plugin.json")

assert manifestFile.exists()
assert rarogFile.exists()

var manifestContent = (String) manifestFile.text
assert manifestContent.contains("Bundle-Name: test")
assert manifestContent.contains("Bundle-Version: 1.0.0.test")
assert manifestContent.contains("Import-Package: org.springframework.context")
assert manifestContent.contains("Export-Package: export.package;version=\"1.0.0.test\"")

var rarogManifestContent = (String) rarogFile.text
assert rarogManifestContent.contains("\"key\" : \"it.rarogsoftware.rarog.maven.plugins:test\"")
assert rarogManifestContent.contains("\"name\" : \"test\"")
assert rarogManifestContent.contains("\"version\" : \"1.0.0-test\"")