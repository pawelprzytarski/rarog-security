var homeFile = new File(basedir, "target/home")
var databaseConfigFile = new File(basedir, "target/home/config/database.properties")

assert homeFile.exists()
assert databaseConfigFile.exists()

var databaseConfig = (String) databaseConfigFile.text
assert databaseConfig.contains("spring.datasource.driver-class-name=org.h2.Driver")
assert databaseConfig.contains("spring.datasource.querydsl-templates-class-name=com.querydsl.sql.H2Templates")
assert databaseConfig.contains("spring.datasource.url=jdbc:h2:file:${basedir}/target/db/rarog;USER=sa;PASSWORD=sa;MODE=PostgreSQL;DATABASE_TO_LOWER=TRUE;CASE_INSENSITIVE_IDENTIFIERS=TRUE")
assert databaseConfig.contains("spring.datasource.password=sa")
assert databaseConfig.contains("spring.datasource.username=sa")

var pluginsDirectory = new File(basedir, "target/home/plugins/")
assert pluginsDirectory.exists()
assert new File(pluginsDirectory, "quick-reload-plugin.jar").exists()
assert new File(pluginsDirectory, "backdoor-plugin.jar").exists()
assert new File(pluginsDirectory, "telemetry-plugin.jar").exists()

var watchlistFile = new File(basedir, "target/home/config/reload_watchlist.txt")
assert watchlistFile.exists()
var artifactFile = new File(basedir, "/target/test-1.0.0-test.jar")
assert watchlistFile.text.contains(artifactFile.getAbsolutePath())
