package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import org.apache.maven.model.Build;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class AbstractAppMojoTest {
    private static final String H2_CONNECTION_STRING = "spring.datasource.url=jdbc:h2:file:null/db/rarog;USER=sa;PASSWORD=sa;MODE=PostgreSQL;DATABASE_TO_LOWER=TRUE;CASE_INSENSITIVE_IDENTIFIERS=TRUE\n";
    private static final String H2_TEMPLATES = "spring.datasource.querydsl-templates-class-name=com.querydsl.sql.H2Templates\n";
    private static final String H2_PASSWORD = "spring.datasource.password=sa\n";
    private static final String H2_USERNAME = "spring.datasource.username=sa\n";
    private static final String h2_DATABASE_DRIVER = "spring.datasource.driver-class-name=org.h2.Driver";
    @Mock
    MavenProject mavenProject;
    @Mock
    Build build;
    @InjectMocks
    AbstractAppMojoTestVersion testMojo;
    private final String TEST_GROUP = "testGroup";
    private final String TEST_ARTIFACT_ID = "testArtifact";
    private final String TEST_VERSION = "testVersion";
    private final String TEST_TYPE = "testType";
    private final String TEST_CLASSIFIER = "testClassifier";
    private final AbstractAppMojo.Artifact TEST_ARTIFACT = new AbstractAppMojo.Artifact(TEST_GROUP, TEST_ARTIFACT_ID, TEST_VERSION, TEST_TYPE, TEST_CLASSIFIER);
    private final AbstractAppMojo.Artifact BACKDOOR_PLUGIN_ARTIFACT = new AbstractAppMojo.Artifact(
            "eu.rarogsoftware.rarog.platform.plugins",
            "backdoor-plugin",
            "0.0.1-SNAPSHOT",
            "jar",
            ""
    );
    private final AbstractAppMojo.Artifact QUICK_RELAOD_PLUGIN_ARTIFACT = new AbstractAppMojo.Artifact(
            "eu.rarogsoftware.rarog.platform.plugins",
            "quick-reload-plugin",
            "0.0.1-SNAPSHOT",
            "jar",
            ""
    );

    @BeforeEach
    void setUp() {
        lenient().when(mavenProject.getBuild()).thenReturn(build);
        lenient().when(build.getOutputDirectory()).thenReturn("outputDirectory");
    }

    @Test
    void testGetDefaultConfigForH2() {
        testMojo.databaseType = "h2";

        var properties = testMojo.getDatabasePropertiesString();

        assertThat(properties).contains(H2_CONNECTION_STRING)
                .contains(H2_TEMPLATES)
                .contains(h2_DATABASE_DRIVER)
                .contains(H2_PASSWORD)
                .contains(H2_USERNAME);
    }

    @Test
    void testGetDefaultConfigForPostgres() {
        testMojo.databaseType = "postgres";

        var properties = testMojo.getDatabasePropertiesString();

        assertThat(properties).contains("spring.datasource.url=jdbc:postgresql://localhost:15432/Rarog")
                .contains("spring.datasource.driver-class-name=org.postgresql.Driver")
                .contains("spring.datasource.querydsl-templates-class-name=com.querydsl.sql.PostgreSQLTemplates")
                .contains("spring.datasource.password=RarogTest")
                .contains("spring.datasource.username=RarogTest");
    }

    @Test
    void testGetMixedDatabaseConfig() {
        testMojo.databaseType = "h2";
        testMojo.databaseQueryDslTemplatesClass = "templates";
        testMojo.databasePassword = "testPassword";

        var properties = testMojo.getDatabasePropertiesString();

        assertThat(properties).contains(H2_CONNECTION_STRING)
                .contains("spring.datasource.querydsl-templates-class-name=templates\n")
                .contains("spring.datasource.password=testPassword\n")
                .contains(H2_USERNAME);
    }

    @Test
    void testGetFullCustomDatabaseConfig() {
        testMojo.databaseQueryDslTemplatesClass = "templates";
        testMojo.databasePassword = "testPassword";
        testMojo.databaseUsername = "testUsername";
        testMojo.databaseUrl = "testUrl";
        testMojo.databaseDriverClass = "testDriver";

        var properties = testMojo.getDatabasePropertiesString();

        assertThat(properties).contains("spring.datasource.url=testUrl")
                .contains("spring.datasource.querydsl-templates-class-name=templates")
                .contains("spring.datasource.password=testPassword")
                .contains("spring.datasource.username=testUsername")
                .contains("spring.datasource.driver-class-name=testDriver");
    }

    @Test
    void testGetPluginArtifactsToInstallWithoutAnhSettings() throws MojoExecutionException {
        var results = testMojo.getPluginArtifactsToInstall();

        assertThat(results).isEmpty();
    }

    @Test
    void testGetPluginArtifactsToInstallWithArtifactListOnly() throws MojoExecutionException {
        var pluginArtifacts = Collections.singletonList(this.TEST_ARTIFACT);
        testMojo.pluginArtifacts = pluginArtifacts;

        var results = testMojo.getPluginArtifactsToInstall();

        assertThat(results).isEqualTo(pluginArtifacts);
    }

    @Test
    void testGetPluginArtifactsToInstallWithArtifactListAndDevMode() throws MojoExecutionException {
        testMojo.pluginArtifacts = Collections.singletonList(TEST_ARTIFACT);
        testMojo.debugMode = true;

        var results = testMojo.getPluginArtifactsToInstall();

        assertThat(results).containsExactlyInAnyOrder(TEST_ARTIFACT, BACKDOOR_PLUGIN_ARTIFACT);
    }

    @Test
    void testGetPluginArtifactsToInstallWithArtifactListAndReloadPluginInstalled() throws MojoExecutionException {
        testMojo.pluginArtifacts = Collections.singletonList(TEST_ARTIFACT);
        testMojo.installQuickReload = true;

        var results = testMojo.getPluginArtifactsToInstall();

        assertThat(results).containsExactlyInAnyOrder(TEST_ARTIFACT, QUICK_RELAOD_PLUGIN_ARTIFACT);
    }

    @Test
    void testGetPluginArtifactsToInstallWithArtifactListAndReloadPluginInstalledAndDevMode() throws MojoExecutionException {
        testMojo.pluginArtifacts = Collections.singletonList(TEST_ARTIFACT);
        testMojo.installQuickReload = true;
        testMojo.debugMode = true;

        var results = testMojo.getPluginArtifactsToInstall();

        assertThat(results).containsExactlyInAnyOrder(TEST_ARTIFACT, QUICK_RELAOD_PLUGIN_ARTIFACT, BACKDOOR_PLUGIN_ARTIFACT);
    }

    @Test
    void testGetCombinedJvmArgsForDefaultConfig() {
        testMojo.appHomeDirectory = new File("/testDirectory");
        testMojo.maxMemory = 100;

        var result = testMojo.getCombinedJvmArgs();

        assertThat(result).isEqualTo("-Xms100m -Dhome.directory=/testDirectory ");
    }

    @Test
    void testGetCombinedJvmArgsForDefaultConfigWithJvmArgs() {
        testMojo.appHomeDirectory = new File("/testDirectory");
        testMojo.maxMemory = 100;
        testMojo.jvmArgs = "arguments";
        testMojo.startJvmArgs = "other arguments";

        var result = testMojo.getCombinedJvmArgs();

        assertThat(result).isEqualTo("-Xms100m -Dhome.directory=/testDirectory arguments");
    }

    @Test
    void testGetCombinedJvmArgsForAllCustomArgs() {
        testMojo.appHomeDirectory = new File("/testDirectory");
        testMojo.maxMemory = 100;
        testMojo.debugMode = true;
        testMojo.jvmArgs = "arguments";
        testMojo.startJvmArgs = "other arguments";

        var result = testMojo.getCombinedJvmArgs();

        assertThat(result).isEqualTo("-Xms100m -Dhome.directory=/testDirectory -DdevMode -Drarog.logs.app.level=DEBUG arguments");
    }

    @Test
    void testGetCargoStartJvmArgsForDefaultConfig() {
        var result = testMojo.getCargoStartJvmArgs();

        assertThat(result).isEmpty();
    }

    @Test
    void testGetCargoStartJvmArgsForAllCustomArgs() {
        testMojo.debugMode = true;
        testMojo.debugPort = 4004;
        testMojo.startJvmArgs = "startArguments";

        var result = testMojo.getCargoStartJvmArgs();

        assertThat(result).isEqualTo("startArguments -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=*:4004");
    }

    static class AbstractAppMojoTestVersion extends AbstractAppMojo {
        private final MavenProject mavenProject;

        AbstractAppMojoTestVersion(MavenProject mavenProject) {
            this.mavenProject = mavenProject;
        }

        @Override
        protected MavenProject getProject() {
            return mavenProject;
        }

        @Override
        public void execute() throws MojoExecutionException, MojoFailureException {
            // do nothing we are testing setting generation
        }
    }
}
