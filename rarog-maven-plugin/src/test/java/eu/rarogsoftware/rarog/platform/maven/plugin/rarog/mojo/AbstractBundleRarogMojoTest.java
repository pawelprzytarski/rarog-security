package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.MavenContext;
import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils.OsgiManifestHeaders;
import org.apache.maven.model.Build;
import org.apache.maven.model.Dependency;
import org.apache.maven.project.MavenProject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Map;

import static eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo.AbstractBundleRarogMojo.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AbstractBundleRarogMojoTest {
    private final static String SPRING_IMPORTS = "org.springframework.context,org.springframework.context.event,org.springframework.context.config,org.springframework.cglib,org.springframework.cglib.core,org.springframework.cglib.proxy,org.springframework.cglib.reflect,org.springframework.beans,org.springframework.beans.factory,org.springframework.beans.factory.annotation,org.springframework.aop.framework.autoproxy,org.springframework.aop.framework,org.springframework.aop,org.aopalliance.aop,org.aopalliance.intercept,org.slf4j,!com.fasterxml.jackson.core.*,!com.fasterxml.jackson.core,!com.fasterxml.jackson.*";
    @Mock
    Build build;
    @Mock
    MavenProject mavenProject;
    @Mock
    MavenContext mavenContext;
    @InjectMocks
    TestBundleRarogMojo bundleMojo;

    @BeforeEach
    void setUp() {
        when(mavenContext.project()).thenReturn(mavenProject);
        when(mavenProject.getBuild()).thenReturn(build);
        when(build.getOutputDirectory()).thenReturn("/outputDirectory");
    }

    @Test
    void testGenerateManifestForEmptyProject() {
        Map<String, String> expectedInstructions = Map.of(
                OsgiManifestHeaders.NOEE, "true"
        );

        var resultInstructions = bundleMojo.getMergedInstructions();

        assertThat(expectedInstructions).containsExactlyInAnyOrderEntriesOf(resultInstructions);
    }

    @Test
    void testGenerateManifestForSimpleProjectWithNameOnly() {
        Map<String, String> expectedInstructions = Map.of(
                OsgiManifestHeaders.NOEE, "true",
                OsgiManifestHeaders.BUNDLE_SYMBOLICNAME, "simple-name",
                OsgiManifestHeaders.BUNDLE_VERSION, "0.0.0",
                OsgiManifestHeaders.BUNDLE_NAME, "Plugin name"
        );
        when(mavenProject.getName()).thenReturn("Plugin name");
        bundleMojo.artifactVersion = "0.0.0";
        when(mavenProject.getArtifactId()).thenReturn("simple-name");

        var resultInstructions = bundleMojo.getMergedInstructions();

        assertThat(expectedInstructions).containsExactlyInAnyOrderEntriesOf(resultInstructions);
    }

    @Test
    void testGenerateManifestForSimpleProjectWithInstructions() {
        Map<String, String> expectedInstructions = Map.of(
                OsgiManifestHeaders.NOEE, "true",
                OsgiManifestHeaders.BUNDLE_SYMBOLICNAME, "simple-name",
                OsgiManifestHeaders.BUNDLE_VERSION, "0.0.0",
                OsgiManifestHeaders.BUNDLE_NAME, "Plugin name"
        );
        bundleMojo.instructions = expectedInstructions;

        var resultInstructions = bundleMojo.getMergedInstructions();

        assertThat(expectedInstructions).containsExactlyInAnyOrderEntriesOf(resultInstructions);
    }

    @Test
    void testGenerateManifestForSpringPlugin() {
        Map<String, String> expectedInstructions = Map.of(
                OsgiManifestHeaders.NOEE, "true",
                OsgiManifestHeaders.IMPORT_PACKAGE, SPRING_IMPORTS + ",*"
        );
        bundleMojo.springBasedPlugin = true;

        var resultInstructions = bundleMojo.getMergedInstructions();

        assertThat(expectedInstructions).containsExactlyInAnyOrderEntriesOf(resultInstructions);
    }

    @Test
    void testGenerateManifestForSpringPluginWithExistingImportsExports() {
        Map<String, String> expectedInstructions = Map.of(
                OsgiManifestHeaders.NOEE, "true",
                OsgiManifestHeaders.IMPORT_PACKAGE, SPRING_IMPORTS + ",testPackage,*",
                OsgiManifestHeaders.EXPORT_PACKAGE, "testPackage"
        );
        bundleMojo.springBasedPlugin = true;
        bundleMojo.instructions = Map.of(
                OsgiManifestHeaders.IMPORT_PACKAGE, "testPackage,*",
                OsgiManifestHeaders.EXPORT_PACKAGE, "testPackage"
        );

        var resultInstructions = bundleMojo.getMergedInstructions();

        assertThat(expectedInstructions).containsExactlyInAnyOrderEntriesOf(resultInstructions);
    }

    @Test
    void testGenerateManifestForSpringPluginWithActivator() {
        Map<String, String> expectedInstructions = Map.of(
                OsgiManifestHeaders.NOEE, "true",
                OsgiManifestHeaders.IMPORT_PACKAGE, SPRING_IMPORTS + ",*",
                OsgiManifestHeaders.BUNDLE_ACTIVATOR, SPRING_BUNDLE_ACTIVATOR_CLASS
        );
        bundleMojo.springBasedPlugin = true;
        var dependency = mock(Dependency.class);
        when(dependency.getScope()).thenReturn("compile");
        when(dependency.getGroupId()).thenReturn(SPRING_ACTIVATOR_GROUP_ID);
        when(dependency.getArtifactId()).thenReturn(SPRING_ACTIVATOR_ARTIFACT_ID);
        when(mavenProject.getDependencies()).thenReturn(Collections.singletonList(dependency));

        var resultInstructions = bundleMojo.getMergedInstructions();

        assertThat(expectedInstructions).containsExactlyInAnyOrderEntriesOf(resultInstructions);
    }

    @Test
    void testGenerateManifestForSpringPluginWithActivatorButInWrongScope() {
        Map<String, String> expectedInstructions = Map.of(
                OsgiManifestHeaders.NOEE, "true",
                OsgiManifestHeaders.IMPORT_PACKAGE, SPRING_IMPORTS + ",*"
        );
        bundleMojo.springBasedPlugin = true;
        var dependency = mock(Dependency.class);
        when(dependency.getScope()).thenReturn("provided");
        when(dependency.getGroupId()).thenReturn(SPRING_ACTIVATOR_GROUP_ID);
        when(dependency.getArtifactId()).thenReturn(SPRING_ACTIVATOR_ARTIFACT_ID);
        when(mavenProject.getDependencies()).thenReturn(Collections.singletonList(dependency));

        var resultInstructions = bundleMojo.getMergedInstructions();

        assertThat(expectedInstructions).containsExactlyInAnyOrderEntriesOf(resultInstructions);
    }

    @Test
    void testGenerateManifestForSpringPluginWithActivatorButWithInstructionWithActivatorSpecified() {
        Map<String, String> expectedInstructions = Map.of(
                OsgiManifestHeaders.NOEE, "true",
                OsgiManifestHeaders.IMPORT_PACKAGE, SPRING_IMPORTS + ",*",
                OsgiManifestHeaders.BUNDLE_ACTIVATOR, "test"
        );
        bundleMojo.instructions = Map.of(
                OsgiManifestHeaders.BUNDLE_ACTIVATOR, "test"
        );
        bundleMojo.springBasedPlugin = true;

        var resultInstructions = bundleMojo.getMergedInstructions();

        assertThat(expectedInstructions).containsExactlyInAnyOrderEntriesOf(resultInstructions);
    }


    static class TestBundleRarogMojo extends AbstractBundleRarogMojo {
        private final MavenContext mockedContext;

        TestBundleRarogMojo(MavenContext mockedContext) {
            this.mockedContext = mockedContext;
        }

        @Override
        protected MavenContext getMavenContext() {
            return mockedContext;
        }

        @Override
        public void execute() {

        }
    }
}