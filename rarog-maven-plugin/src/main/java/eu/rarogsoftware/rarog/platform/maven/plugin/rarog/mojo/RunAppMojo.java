package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

/**
 * Runs Rarog application using cargo-maven-plugin and tomcat as servlet server
 */
@Mojo(name = "run", requiresDependencyResolution = ResolutionScope.TEST)
@Execute(phase = LifecyclePhase.PACKAGE)
public class RunAppMojo extends StartAppMojo {
    public RunAppMojo() {
        super("run");
    }
}
