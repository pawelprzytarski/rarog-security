package eu.rarogsoftware.rarog.platform.maven.plugin.rarog;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.twdata.maven.mojoexecutor.MojoExecutor.plugin;

public class MavenContext {
    private static final Map<String, String> DEFAULT_PLUGIN_VERSIONS;

    static {
        Map<String, String> defaultVersions = new HashMap<>();
        defaultVersions.put("org.codehaus.cargo:cargo-maven3-plugin", "1.10.3");
        defaultVersions.put("org.apache.felix:maven-bundle-plugin", "5.1.7");
        defaultVersions.put("org.apache.maven.plugins:maven-dependency-plugin", "3.3.0");
        defaultVersions.put("org.apache.maven.plugins:maven-jar-plugin", "3.3.0");
        defaultVersions.put("org.apache.maven.plugins:maven-help-plugin", "3.3.0");
        DEFAULT_PLUGIN_VERSIONS = Collections.unmodifiableMap(defaultVersions);
    }

    private final MavenProject project;
    private final List<MavenProject> reactor;
    private final MavenSession session;
    private final BuildPluginManager buildPluginManager;
    private final Log log;
    private Map<String, String> pluginVersions = new HashMap<>();

    public MavenContext(MavenProject project, List<MavenProject> reactor, MavenSession session,
                        BuildPluginManager buildPluginManager, Log log) {
        this.project = project;
        this.reactor = reactor;
        this.session = session;
        this.buildPluginManager = buildPluginManager;
        this.log = log;
    }

    public MavenProject project() {
        return project;
    }

    public List<MavenProject> reactor() {
        return reactor;
    }

    public MavenSession session() {
        return session;
    }

    public BuildPluginManager buildPluginManager() {
        return buildPluginManager;
    }

    public Log log() {
        return log;
    }

    public Plugin getPlugin(final String groupId, final String artifactId) {
        return plugin(groupId, artifactId, getPluginVersion(groupId, artifactId));
    }

    private String getPluginVersion(String groupId, String artifactId) {
        var pluginVersion = getPluginVersionFromProject(groupId, artifactId).orElseGet(() -> DEFAULT_PLUGIN_VERSIONS.get(groupId + ":" + artifactId));

        if (StringUtils.isBlank(pluginVersion)) {
            log().error("Failed to calculate version for plugin %s:%s".formatted(groupId, artifactId));
            throw new IllegalArgumentException(format("No version available for '%s'", artifactId));
        }
        log().info("Using plugin %s:%s version %s".formatted(groupId, artifactId, pluginVersion));
        return pluginVersion;
    }

    private Optional<String> getPluginVersionFromProject(String groupId, String artifactId) {
        if (pluginVersions == null) {
            if (project.getPluginManagement() != null) {
                pluginVersions = loadVersionOverridesFromPluginManagement();
            } else {
                pluginVersions = Collections.emptyMap();
            }
        }
        return Optional.ofNullable(pluginVersions.get(groupId + ":" + artifactId));
    }

    private Map<String, String> loadVersionOverridesFromPluginManagement() {
        return project.getPluginManagement().getPlugins().stream()
                .collect(Collectors.toMap(plugin -> plugin.getGroupId() + ":" + plugin.getArtifactId(), Plugin::getVersion));
    }

    public String getBuildDirectory() {
        return project.getBuild().getDirectory();
    }
}
