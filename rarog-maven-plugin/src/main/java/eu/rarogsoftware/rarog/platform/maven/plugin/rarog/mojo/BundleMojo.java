package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import static org.twdata.maven.mojoexecutor.MojoExecutor.*;

/**
 * Generates jar artifact using results of previous steps in lifecycle
 */
@Mojo(name = "bundle", defaultPhase = LifecyclePhase.PACKAGE, requiresDependencyResolution = ResolutionScope.COMPILE)
public class BundleMojo extends AbstractRarogMojo {
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info("Generating plugin bundle");

        getMojoExecutor().executeMergeWithProjectConfig(
                getMavenContext().getPlugin("org.apache.maven.plugins", "maven-jar-plugin"),
                goal("jar"),
                configuration(
                        element(name("archive"), element(name("manifestFile"), "${project.build.outputDirectory}/META-INF/MANIFEST.MF"))
                )
        );
    }
}
