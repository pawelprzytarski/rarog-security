package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;


import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;

/**
 * Generate content of rarog home so app can integration test or local instance of rarog can start without need to go through setup.
 * It generates database connection config, creates home directory, initialize necessary configs.
 */
@Mojo(name = "prepare-home", defaultPhase = LifecyclePhase.PROCESS_TEST_RESOURCES)
public class PrepareHomeMojo extends AbstractAppMojo {
    /**
     * If true prepare-home goal will be skipped
     */
    @Parameter(property = "skip.home.prepare", defaultValue = "false")
    boolean skipHomePrepare;
    /**
     * If true prepare-home goal will remove home directory with all its content and then will recreate it
     */
    @Parameter(property = "clean.home.if.exist", defaultValue = "false")
    boolean clearHomeIfExist;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (skipHomePrepare) {
            getLog().info("Skipping rarog app home preparations");
            return;
        }
        try {
            if (appHomeDirectory.exists()) {
                if (clearHomeIfExist) {
                    FileUtils.deleteDirectory(appHomeDirectory);
                } else {
                    getLog().info("Home directory exists. Skipping generation...");
                    return;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        generateHomeDirectory();
    }
}
