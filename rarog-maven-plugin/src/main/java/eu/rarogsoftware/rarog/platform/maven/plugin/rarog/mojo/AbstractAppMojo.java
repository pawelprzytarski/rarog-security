package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils.PluginHelper;
import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils.ZipUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils.PluginHelper.artifactItemElement;
import static org.twdata.maven.mojoexecutor.MojoExecutor.*;

public abstract class AbstractAppMojo extends AbstractRarogMojo {
    protected static final String RAROG_APP_GROUP_ID = "eu.rarogsoftware.rarog.platform";
    protected static final String RAROG_APP_ARTIFACT_ID = "rarog-server";
    protected static final String RAROG_APP_WAR_TYPE = "war";
    protected static final String TOMCAT_HOSTNAME = "localhost";
    protected static final String RAROG_PLUGIN_PACKAGING = "rarog-plugin";
    protected static final String FALLBACK_TOMCAT_TYPE = "tomcat9x";
    protected static final String FALLBACK_TOMCAT_VERSION = "9.0.55-privately-hosted";
    protected static final String TOMCAT_CONTAINER_TYPE_EFFECTIVE_POM_PROPERTY = "tomcat.container.type";
    protected static final String TOMCAT_VERSION_EFFECTIVE_POM_PROPERTY = "tomcat.default.version";
    private static final ObjectMapper XML_MAPPER = new XmlMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    /**
     * Address of home directory for rarog app started using rarog:run or rarog:start
     */
    @Parameter(property = "rarog.app.home.directory", defaultValue = "${project.build.directory}/home")
    File appHomeDirectory;
    /**
     * Servlet path under which rarog app will start
     */
    @Parameter(property = "rarog.app.servlet.path", defaultValue = "rarog")
    String appServletPath;
    /**
     * Port on which tomcat server will start app
     */
    @Parameter(property = "rarog.app.port", defaultValue = "8180")
    String appPort;
    /**
     * JVM arguments to be passed to tomcat server
     */
    @Parameter(property = "rarog.app.jvmargs", defaultValue = "")
    String jvmArgs;
    /**
     * JVM arguments used to start tomcat server
     */
    @Parameter(property = "rarog.app.start.jvmargs", defaultValue = "")
    String startJvmArgs;
    /**
     * Tomcat version to use
     */
    @Parameter(property = "rarog.app.tomcat.version")
    String tomcatVersion;
    /**
     * Container type for cargo-maven-plugin
     */
    @Parameter(property = "rarog.app.tomcat.container.type")
    String tomcatContainerType;
    /**
     * Rarog App version to use
     */
    @Parameter(property = "rarog.app.version")
    String rarogVersion;
    /**
     * If true jvm with rarog app will start with debug port exposed and with installed debug mode apps
     */
    @Parameter(property = "rarog.app.debug.mode", defaultValue = "false")
    boolean debugMode;
    /**
     * Debug port for rarog app exposed in debug mode
     */
    @Parameter(property = "rarog.app.debug.port", defaultValue = "5005")
    int debugPort;
    /**
     * Suspend debug on rarog app jvm start
     */
    @Parameter(property = "rarog.app.debug.suspend", defaultValue = "false")
    boolean debugSuspend;
    /**
     * Max memory limit for tomcat container in megabytes
     */
    @Parameter(property = "rarog.app.max.memory", defaultValue = "512")
    int maxMemory;
    /**
     * Defines set of default values for database connection.
     * Available values (case insensitive):
     * <ul>
     *     <li>H2</li>
     *     <li>Postgres</li>
     *     <li>custom</li>
     * </ul>
     */
    @Parameter(property = "rarog.app.database.type", defaultValue = "h2")
    String databaseType;
    /**
     * Database url for app to connect
     */
    @Parameter(property = "rarog.app.database.url")
    String databaseUrl;
    /**
     * Database driver classname to use
     */
    @Parameter(property = "rarog.app.database.driver")
    String databaseDriverClass;
    /**
     * QueryDSL templates classname to use
     */
    @Parameter(property = "rarog.app.querydsl.driver")
    String databaseQueryDslTemplatesClass;
    /**
     * Username to use to authorize to database
     */
    @Parameter(property = "rarog.app.database.username")
    String databaseUsername;
    /**
     * Password to use to authorize to database
     */
    @Parameter(property = "rarog.app.database.password")
    String databasePassword;
    /**
     * If true install quick-reload-plugin in addition to plugins listed in pluginArtifacts
     */
    @Parameter(property = "rarog.app.install.quickreload", defaultValue = "false")
    boolean installQuickReload;
    /**
     * List of plugin artifact to be installed
     */
    @Parameter
    List<Artifact> pluginArtifacts;
    /**
     * List of dependencies to attach to tomcat jvm
     */
    @Parameter
    List<Artifact> dependencies;

    String getDatabasePropertiesString() {
        var databaseConfig = getDatabaseConfig();
        return "spring.datasource.url="
                + databaseConfig.url
                + "\nspring.datasource.driver-class-name="
                + databaseConfig.driver
                + "\nspring.datasource.querydsl-templates-class-name="
                + databaseConfig.templates
                + "\nspring.datasource.password="
                + databaseConfig.password
                + "\nspring.datasource.username="
                + databaseConfig.username
                + "\n";
    }

    protected DatabaseConfig getDatabaseConfig() {
        DatabaseConfig defaultConfig = new DatabaseConfig("", "", "", "", "");
        if ("h2".equalsIgnoreCase(databaseType)) {
            getLog().info("Loading default database configuration for H2");
            defaultConfig = new DatabaseConfig(
                    "jdbc:h2:file:%s/db/rarog;USER=sa;PASSWORD=sa;MODE=PostgreSQL;DATABASE_TO_LOWER=TRUE;CASE_INSENSITIVE_IDENTIFIERS=TRUE".formatted(getProject().getBuild().getDirectory()),
                    "org.h2.Driver",
                    "com.querydsl.sql.H2Templates",
                    "sa",
                    "sa"
            );
        } else if ("postgres".equalsIgnoreCase(databaseType)) {
            getLog().info("Loading default database configuration for Postgres");
            defaultConfig = new DatabaseConfig(
                    "jdbc:postgresql://localhost:15432/Rarog",
                    "org.postgresql.Driver",
                    "com.querydsl.sql.PostgreSQLTemplates",
                    "RarogTest",
                    "RarogTest"
            );
        }
        return new DatabaseConfig(
                Optional.ofNullable(databaseUrl).orElse(defaultConfig.url),
                Optional.ofNullable(databaseDriverClass).orElse(defaultConfig.driver),
                Optional.ofNullable(databaseQueryDslTemplatesClass).orElse(defaultConfig.templates),
                Optional.ofNullable(databaseUsername).orElse(defaultConfig.username),
                Optional.ofNullable(databasePassword).orElse(defaultConfig.password)
        );
    }

    protected void generateHomeDirectory() throws MojoExecutionException {
        try {
            getLog().info("Generating default content of home directory for Rarog app");
            appHomeDirectory.mkdirs();
            var databaseConfigFile = new File(appHomeDirectory, "config/database.properties");
            databaseConfigFile.getParentFile().mkdirs();
            getLog().info("Writing database config to " + databaseConfigFile);
            FileUtils.write(databaseConfigFile, getDatabasePropertiesString(), Charset.forName(getEncoding()));

            if (installQuickReload) {
                var watchlistFile = new File(appHomeDirectory, "config/reload_watchlist.txt");
                var artifactPath = new File(getProject().getBuild().getDirectory(), getProject().getBuild().getFinalName() + ".jar");
                getLog().info("Creating default config for quick reload plugin");
                FileUtils.write(watchlistFile, artifactPath.getAbsolutePath(), Charset.forName(getEncoding()));
            }

            var plugins = getPluginArtifactsToInstall();
            if (plugins.isEmpty()) {
                return;
            }
            getLog().info("Installing plugins to home directory");
            var pluginsDirectory = new File(appHomeDirectory, "plugins");
            getMojoExecutor()
                    .execute(
                            PluginHelper.getDependencyPlugin(getMavenContext()),
                            goal("copy"),
                            configuration(
                                    element(
                                            name("artifactItems"), plugins.stream()
                                                    .map(artifact ->
                                                            element(name("artifactItem"),
                                                                    element(name("groupId"), artifact.groupId()),
                                                                    element(name("artifactId"), artifact.artifactId()),
                                                                    element(name("version"), artifact.version()),
                                                                    element(name("type"), artifact.type()),
                                                                    element(name("classifier"), artifact.classifier())
                                                            )
                                                    ).distinct().toArray(Element[]::new)
                                    ),
                                    element(name("outputDirectory"), pluginsDirectory.getAbsolutePath()),
                                    element(name("overWriteSnapshots"), "true"),
                                    element(name("stripVersion"), "true"),
                                    element(name("stripClassifier"), "true")
                            )
                    );
            getLog().info("Home generation ended successfully");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<Artifact> getPluginArtifactsToInstall() throws MojoExecutionException {
        var fullList = new ArrayList<>(pluginArtifacts != null ? pluginArtifacts : Collections.emptyList());
        if (installQuickReload) {
            fullList.add(new Artifact(
                    "eu.rarogsoftware.rarog.platform.plugins",
                    "quick-reload-plugin",
                    getPluginProperties().getProperty("rarog.quick.reload.version"),
                    "jar",
                    ""
            ));
        }
        if (debugMode) {
            fullList.add(new Artifact(
                    "eu.rarogsoftware.rarog.platform.plugins",
                    "backdoor-plugin",
                    getPluginProperties().getProperty("rarog.backdoor.version"),
                    "jar",
                    ""
            ));
        }
        return fullList;
    }

    protected void executeCargoGoalForRunningApp(String goal) throws MojoExecutionException {
        var defaults = getDefaultTomcatValues();

        var rarogAppFile = downloadRarogArtifact();
        var cargoContainer = installTomcat(defaults);
        installProjectArtifact();

        getMojoExecutor()
                .execute(
                        PluginHelper.getCargoPlugin(getMavenContext()),
                        goal(goal),
                        getCargoConfiguration(rarogAppFile, cargoContainer)
                );
    }

    protected void installProjectArtifact() throws MojoExecutionException {
        try {
            if (RAROG_PLUGIN_PACKAGING.equalsIgnoreCase(getProject().getPackaging())) {
                var artifactPath = new File(getProject().getBuild().getDirectory(), getProject().getBuild().getFinalName() + ".jar");
                var targetFile = new File(appHomeDirectory, "plugins/" + getProject().getBuild().getFinalName() + ".jar");
                if (!artifactPath.exists()) {
                    return;
                }
                getLog().info("Installing artifact " + getProject().getBuild().getFinalName() + " to " + targetFile);
                if (targetFile.exists()) {
                    targetFile.delete();
                }
                FileUtils.copyFile(artifactPath, targetFile);

            }
        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }

    protected DefaultTomcatValues getDefaultTomcatValues() throws MojoExecutionException {
        var defaultContainerType = FALLBACK_TOMCAT_TYPE;
        var defaultTomcatVersion = FALLBACK_TOMCAT_VERSION;
        getLog().info("Reading default tomcat config from Rarog app pom file");
        var effectivePomFile = new File(getProject().getBuild().getDirectory(), "rarog-effective-pom.xml");
        getLog().info("Generating effective pom for Rarog app to " + effectivePomFile);
        getMojoExecutor()
                .execute(
                        getMavenContext().getPlugin("org.apache.maven.plugins", "maven-help-plugin"),
                        goal("effective-pom"),
                        configuration(
                                element(name("artifact"), RAROG_APP_GROUP_ID + ":" + RAROG_APP_ARTIFACT_ID + ":" + getRarogVersion()),
                                element(name("output"), effectivePomFile.getAbsolutePath())
                        )
                );
        try {
            var pom = XML_MAPPER.readTree(effectivePomFile);
            var properties = pom.get("properties");
            if (properties != null) {
                var type = properties.get(TOMCAT_CONTAINER_TYPE_EFFECTIVE_POM_PROPERTY);
                var version = properties.get(TOMCAT_VERSION_EFFECTIVE_POM_PROPERTY);
                if (type != null) {
                    defaultContainerType = type.asText();
                } else {
                    getLog().warn("Effective pom for Rarog app doesn't contains tomcat container type using default from rarog-maven-plugin");
                }
                if (version != null) {
                    defaultTomcatVersion = version.asText();
                } else {
                    getLog().warn("Effective pom for Rarog app doesn't contains tomcat version using default from rarog-maven-plugin");
                }
            } else {
                getLog().warn("Effective pom for Rarog app doesn't contains properties");
            }
        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
        getLog().info("Using tomcat version %s of type %s".formatted(defaultTomcatVersion, defaultContainerType));
        return new DefaultTomcatValues(defaultContainerType, defaultTomcatVersion);
    }

    protected File downloadRarogArtifact() throws MojoExecutionException {
        var rarogVersionToUse = getRarogVersion();
        getLog().info("Getting rarog app version " + rarogVersionToUse);
        var rarogAppDirectory = new File(getProject().getBuild().getDirectory(), "app/");
        var rarogAppFile = new File(rarogAppDirectory, "rarog-server-" + rarogVersionToUse + ".war");
        getMojoExecutor()
                .execute(
                        PluginHelper.getDependencyPlugin(getMavenContext()),
                        goal("copy"),
                        PluginHelper.getCopyConfiguration(rarogAppDirectory.getAbsolutePath(),
                                false,
                                PluginHelper.artifactItemElement(RAROG_APP_GROUP_ID,
                                        RAROG_APP_ARTIFACT_ID,
                                        rarogVersionToUse,
                                        RAROG_APP_WAR_TYPE))
                );
        return rarogAppFile;
    }

    private String getRarogVersion() throws MojoExecutionException {
        return Optional.ofNullable(rarogVersion).orElse(getPluginProperties().getProperty("rarog.app.version"));
    }

    protected ContainerInfo installTomcat(DefaultTomcatValues defaults) throws MojoExecutionException {
        var containerInfo = getContainerInfo(defaults);
        getLog().info("Installing Apache Tomcat " + containerInfo.version() + " to directory " + containerInfo.installDirectory());
        getMojoExecutor()
                .execute(
                        PluginHelper.getDependencyPlugin(getMavenContext()),
                        goal("copy"),
                        PluginHelper.getCopyConfiguration(containerInfo.directory().getAbsolutePath(),
                                true,
                                PluginHelper.artifactItemElement(
                                        "org.apache.tomcat",
                                        "apache-tomcat",
                                        containerInfo.version(),
                                        "zip"))
                );

        ZipUtils.extractZipFile(containerInfo.installDirectory(), containerInfo.archiveFile(), true);
        return containerInfo;
    }

    protected Xpp3Dom getCargoConfiguration(File rarogAppFile, ContainerInfo container) {
        return configuration(
                getCargoContainerConfig(container),
                getCargoConfiguration(container),
                getRarogAppConfig(rarogAppFile)
        );
    }

    protected Element getCargoContainerConfig(ContainerInfo containerInfo) {
        var tomcatLogFile = new File(appHomeDirectory, "logs/catalina.log");
        var dependenciesToInstall = Optional.ofNullable(dependencies).orElse(Collections.emptyList());

        return element(name("container"),
                element(name("containerId"), containerInfo.type()),
                element(name("type"), "installed"),
                element(name("home"), containerInfo.installDirectory().getAbsolutePath()),
                element(name("output"), tomcatLogFile.getAbsolutePath()),
                element(name("timeout"), "120000"),
                element(name("dependencies"), dependenciesToInstall.stream()
                        .map(artifact ->
                                PluginHelper.artifactItemElement(artifact.groupId(),
                                        artifact.artifactId(),
                                        artifact.version(),
                                        artifact.type(),
                                        artifact.classifier()
                                )
                        ).distinct().toArray(Element[]::new)
                )
        );
    }

    protected Element getCargoConfiguration(ContainerInfo containerInfo) {
        var cargoHome = new File(getProject().getBuild().getDirectory(), "target/" + containerInfo.type());
        var combinedJvmArgs = getCombinedJvmArgs();
        var cargoStartJvmArgs = getCargoStartJvmArgs();
        getLog().info("Configuring tomcat to run at port " + appPort + " in directory " + cargoHome);
        getLog().info("Tomcat will start with following arguments `%s` and start arguments `%s`".formatted(combinedJvmArgs, cargoStartJvmArgs));
        return element(name("configuration"),
                element(name("type"), "standalone"),
                element(name("home"), cargoHome.getAbsolutePath()),
                element(name("properties"),
                        element(name("cargo.jvmargs"), combinedJvmArgs),
                        element(name("cargo.start.jvmargs"), cargoStartJvmArgs),
                        element(name("cargo.servlet.port"), appPort),
                        element(name("cargo.hostname"), TOMCAT_HOSTNAME)
                )
        );
    }

    protected String getCombinedJvmArgs() {
        return "-Xms" + maxMemory + "m "
                + "-Dhome.directory=" + appHomeDirectory.getAbsolutePath()
                + (debugMode ? " -DdevMode -Drarog.logs.app.level=DEBUG" : "")
                + " " + (jvmArgs == null ? "" : jvmArgs);
    }

    protected String getCargoStartJvmArgs() {
        return (startJvmArgs == null ? "" : startJvmArgs)
                + (debugMode ? " " + getJvmDebugSettings() : "");
    }

    protected String getJvmDebugSettings() {
        return "-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=%s,address=*:%d".formatted(debugSuspend ? "y" : "n", debugPort);
    }

    protected Element getRarogAppConfig(File rarogAppFile) {
        return element(name("deployables"),
                element(name("deployable"),
                        element(name("groupId"), RAROG_APP_GROUP_ID),
                        element(name("artifactId"), RAROG_APP_ARTIFACT_ID),
                        element(name("type"), RAROG_APP_WAR_TYPE),
                        element(name("location"), rarogAppFile.getAbsolutePath()),
                        element(name("properties"),
                                element(name("context"), appServletPath)
                        )
                )
        );
    }

    protected ContainerInfo getContainerInfo(DefaultTomcatValues defaults) {
        var tomcatVersionToUse = Optional.ofNullable(this.tomcatVersion).orElse(defaults.version());
        var tomcatDirectory = new File(getProject().getBuild().getDirectory(), "tomcat/" + tomcatVersionToUse);
        var tomcatInstalledDirectory = new File(tomcatDirectory, "installed");
        var tomcatFile = new File(tomcatDirectory, "apache-tomcat.zip");
        var containerType = Optional.ofNullable(tomcatContainerType).orElse(defaults.type());
        return new ContainerInfo(containerType, tomcatVersionToUse, tomcatDirectory, tomcatInstalledDirectory, tomcatFile);
    }

    record DatabaseConfig(String url, String driver, String templates, String username, String password) {
    }

    public static final class Artifact {
        private String groupId;
        private String artifactId;
        private String version;
        private String type;
        private String classifier;

        public Artifact() {
            // needed because of plexus
        }

        public Artifact(String groupId, String artifactId, String version, String type, String classifier) {
            this.groupId = groupId;
            this.artifactId = artifactId;
            this.version = version;
            this.type = type;
            this.classifier = classifier;
        }

        public String groupId() {
            return groupId;
        }

        public String artifactId() {
            return artifactId;
        }

        public String version() {
            return version;
        }

        public String type() {
            return type;
        }

        public String classifier() {
            return classifier;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (!(o instanceof Artifact artifact)) return false;

            return new EqualsBuilder().append(groupId, artifact.groupId).append(artifactId, artifact.artifactId).append(version, artifact.version).append(type, artifact.type).append(classifier, artifact.classifier).isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37).append(groupId).append(artifactId).append(version).append(type).append(classifier).toHashCode();
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("groupId", groupId)
                    .append("artifactId", artifactId)
                    .append("version", version)
                    .append("type", type)
                    .append("classifier", classifier)
                    .toString();
        }
    }

    record ContainerInfo(String type, String version, File directory, File installDirectory, File archiveFile) {
    }

    record DefaultTomcatValues(String type, String version) {
    }
}
