package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils;

import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.MojoExecutionException;
import org.codehaus.plexus.util.xml.Xpp3Dom;

public interface SimpleMojoExecutor {
    /**
     * Executes the specified mojo, using provided configuration.
     *
     * @param plugin        Maven plugin to execute
     * @param goal          goal (mojo) to execute
     * @param configuration configuration to apply
     * @throws MojoExecutionException if executing the mojo fails
     */
    void execute(Plugin plugin, String goal, Xpp3Dom configuration)
            throws MojoExecutionException;

    /**
     * Executes the specified mojo, using provided configuration merged with configuration provided by project.
     * It allows to skip some configuration option by using plugin configs directly from project pom.
     *
     * @param plugin        Maven plugin to execute
     * @param goal          goal (mojo) to execute
     * @param configuration configuration to merge
     * @throws MojoExecutionException if executing the mojo fails
     */
    void executeMergeWithProjectConfig(Plugin plugin, String goal, Xpp3Dom configuration) throws MojoExecutionException;
}
