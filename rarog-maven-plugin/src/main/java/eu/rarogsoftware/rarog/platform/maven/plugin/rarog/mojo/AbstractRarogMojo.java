package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils.DefaultSimpleMojoExecutor;
import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils.SimpleMojoExecutor;
import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.MavenContext;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.util.List;
import java.util.Objects;
import java.util.Properties;

public abstract class AbstractRarogMojo extends AbstractMojo {
    /**
     * The artifact version.
     */
    @Parameter(property = "artifactVersion", required = true, readonly = true, defaultValue = "${project.version}")
    protected String artifactVersion;
    /**
     * Project source files encoding.
     */
    @Parameter(property = "encoding", defaultValue = "${project.build.sourceEncoding}")
    protected String encoding;
    @Component
    private BuildPluginManager buildPluginManager;
    /**
     * The current Maven project.
     */
    @Parameter(property = "project", required = true, readonly = true)
    private MavenProject project;
    /**
     * The list of modules being built, the reactor.
     */
    @Parameter(property = "reactorProjects", required = true, readonly = true)
    private List<MavenProject> reactor;
    /**
     * The Maven session.
     */
    @Parameter(property = "session", required = true, readonly = true)
    private MavenSession session;
    private MavenContext mavenContext;
    private SimpleMojoExecutor mojoExecutor;
    private Properties pluginProperties;

    protected MavenContext getMavenContext() {
        if (mavenContext == null) {
            mavenContext = new MavenContext(project, reactor, session, buildPluginManager, getLog());
        }
        return mavenContext;
    }

    protected SimpleMojoExecutor getMojoExecutor() {
        if (mojoExecutor == null) {
            mojoExecutor = new DefaultSimpleMojoExecutor(getMavenContext());
        }
        return mojoExecutor;
    }

    protected BuildPluginManager getBuildPluginManager() {
        return buildPluginManager;
    }

    protected MavenProject getProject() {
        return project;
    }

    protected List<MavenProject> getReactor() {
        return reactor;
    }

    protected MavenSession getSession() {
        return session;
    }

    protected String getEncoding() {
        return encoding;
    }

    protected String getBuildOutputDirectory() {
        return getMavenContext().project().getBuild().getOutputDirectory();
    }

    protected Properties getPluginProperties() throws MojoExecutionException {
        if (pluginProperties == null) {
            pluginProperties = new Properties();
            var versionsUrl = Thread.currentThread().getContextClassLoader().getResource("META-INF/versions.properties");
            try (var versionStream = Objects.requireNonNull(versionsUrl).openStream()) {
                pluginProperties.load(versionStream);
            } catch (Exception e) {
                throw new MojoExecutionException(e);
            }
        }
        return pluginProperties;
    }
}
