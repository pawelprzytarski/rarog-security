package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils;


public interface OsgiManifestHeaders {
    String AUTOMATIC_MODULE_NAME = "Automatic-Module-Name";
    String BND_ADDXMLTOTEST = "Bnd-AddXMLToTest";
    String BUNDLE_ACTIVATIONPOLICY = "Bundle-ActivationPolicy";
    String BUNDLE_ACTIVATOR = "Bundle-Activator";
    String BUNDLE_BLUEPRINT = "Bundle-Blueprint";
    String BUNDLE_CATEGORY = "Bundle-Category";
    String BUNDLE_CLASSPATH = "Bundle-ClassPath";
    String BUNDLE_CONTACTADDRESS = "Bundle-ContactAddress";
    String BUNDLE_COPYRIGHT = "Bundle-Copyright";
    String BUNDLE_DESCRIPTION = "Bundle-Description";
    String BUNDLE_DOCURL = "Bundle-DocURL";
    String BUNDLE_ICON = "Bundle-Icon";
    String BUNDLE_LICENSE = "Bundle-License";
    String BUNDLE_LOCALIZATION = "Bundle-Localization";
    String BUNDLE_MANIFESTVERSION = "Bundle-ManifestVersion";
    String BUNDLE_NAME = "Bundle-Name";
    String BUNDLE_NATIVECODE = "Bundle-NativeCode";
    String BUNDLE_REQUIREDEXECUTIONENVIRONMENT = "Bundle-RequiredExecutionEnvironment";
    String BUNDLE_SYMBOLICNAME = "Bundle-SymbolicName";
    String BUNDLE_UPDATELOCATION = "Bundle-UpdateLocation";
    String BUNDLE_VENDOR = "Bundle-Vendor";
    String BUNDLE_VERSION = "Bundle-Version";
    String BUNDLE_DEVELOPERS = "Bundle-Developers";
    String BUNDLE_CONTRIBUTORS = "Bundle-Contributors";
    String BUNDLE_SCM = "Bundle-SCM";
    String DYNAMICIMPORT_PACKAGE = "DynamicImport-Package";
    String EXPORT_PACKAGE = "Export-Package";
    String EXPORT_SERVICE = "Export-Service";
    String FRAGMENT_HOST = "Fragment-Host";
    String IMPORT_PACKAGE = "Import-Package";
    String IMPORT_SERVICE = "Import-Service";
    String LAUNCHER_PLUGIN = "Launcher-Plugin";
    String MAIN_CLASS = "Main-Class";
    String META_PERSISTENCE = "Meta-Persistence";
    String PROVIDE_CAPABILITY = "Provide-Capability";
    String REQUIRE_BUNDLE = "Require-Bundle";
    String REQUIRE_CAPABILITY = "Require-Capability";
    String SERVICE_COMPONENT = "Service-Component";
    String TESTER_PLUGIN = "Tester-Plugin";
    String PRIVATE_PACKAGE = "Private-Package";
    String IGNORE_PACKAGE = "Ignore-Package";
    String INCLUDE_RESOURCE = "Include-Resource";
    String CONDITIONAL_PACKAGE = "Conditional-Package";
    String BND_LASTMODIFIED = "Bnd-LastModified";
    String CREATED_BY = "Created-By";
    String TOOL = "Tool";
    String TESTCASES = "Test-Cases";
    String REPOSITORIES = "Repositories";
    String EMBED_DEPENDENCY = "Embed-Dependency";
    String EMBED_TRANSITIVE = "Embed-Transitive";
    String NOEE = "-noee";
}