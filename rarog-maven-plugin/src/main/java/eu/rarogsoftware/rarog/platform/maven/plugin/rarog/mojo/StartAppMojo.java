package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

/**
 * Starts Rarog application using cargo-maven-plugin and tomcat as servlet server
 */
@Mojo(name = "start", defaultPhase = LifecyclePhase.PRE_INTEGRATION_TEST, requiresDependencyResolution = ResolutionScope.COMPILE)
public class StartAppMojo extends AbstractAppMojo {
    private final String goal;
    /**
     * If true will skip running app
     */
    @Parameter(property = "skipAppRun", defaultValue = "false")
    boolean skipAppRun;
    /**
     * If true will not attempt to create home directory if it does not exist
     */
    @Parameter(property = "app.home.creation.skip", defaultValue = "false")
    boolean skipHomeCreation;

    public StartAppMojo(String goal) {
        this.goal = goal;
    }

    public StartAppMojo() {
        this("start");
    }

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (skipAppRun) {
            getLog().info("Skipping app start");
            return;
        }
        // necessary when goal started outside of lifecycle
        if (!appHomeDirectory.exists() && !skipHomeCreation) {
            getLog().info("Populating home directory");
            generateHomeDirectory();
        } else {
            getLog().info("Skipping populating home directory");
        }
        executeCargoGoalForRunningApp(goal);
    }

}
