package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils;

import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.MavenContext;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.descriptor.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.codehaus.plexus.util.xml.Xpp3DomUtils;
import org.twdata.maven.mojoexecutor.MavenCompatibilityHelper;
import org.twdata.maven.mojoexecutor.MojoExecutor;

import java.util.List;

public class DefaultSimpleMojoExecutor implements SimpleMojoExecutor {
    private final MojoExecutor.ExecutionEnvironment executionEnvironment;

    public DefaultSimpleMojoExecutor(MavenContext mavenContext) {
        this.executionEnvironment = MojoExecutor.executionEnvironment(mavenContext.project(), mavenContext.session(), mavenContext.buildPluginManager());
    }

    @Override
    public void execute(Plugin plugin, String goal, Xpp3Dom configuration) throws MojoExecutionException {
        MojoExecutor.executeMojo(plugin, goal, configuration, executionEnvironment);
    }

    @Override
    public void executeMergeWithProjectConfig(Plugin plugin, String goal, Xpp3Dom configuration) throws MojoExecutionException {
        var mergedConfig = mergeWithProjectConfig(plugin, goal, configuration, executionEnvironment);

        execute(plugin, goal, mergedConfig);
    }

    // >>> BEGIN: stolen shamelessly from AMPS (https://bitbucket.org/atlassian/amps/src/master/) - Apache2.0
    private Xpp3Dom mergeWithProjectConfig(Plugin plugin,
                                           String goal,
                                           Xpp3Dom defaultConfig,
                                           MojoExecutor.ExecutionEnvironment executionEnvironment)
            throws MojoExecutionException {
        var goalConfig = getGoalConfigFromProject(plugin, goal, executionEnvironment.getMavenProject());
        if (goalConfig == null) {
            return defaultConfig;
        }

        // Treat goal config as primary one. We only want to extend existing config
        var merged = Xpp3DomUtils.mergeXpp3Dom(goalConfig, defaultConfig, true);

        var parameters = getGoalParameters(plugin, goal, executionEnvironment);
        if (parameters == null || parameters.isEmpty()) {
            return merged;
        }

        var finalConfig = MojoExecutor.configuration();
        for (var parameter : parameters) {
            var element = merged.getChild(parameter.getName());
            if (element == null && parameter.getAlias() != null) {
                element = merged.getChild(parameter.getAlias());
            }

            if (element != null) {
                finalConfig.addChild(element);
            }
        }

        return finalConfig;
    }

    private Xpp3Dom getGoalConfigFromProject(Plugin plugin, String goal, MavenProject project) {
        String executionId = null;
        int index = goal.indexOf('#');
        if (index != -1) {
            executionId = goal.substring(index + 1);
            goal = goal.substring(0, index);
        }
        return project.getGoalConfiguration(plugin.getGroupId(), plugin.getArtifactId(), executionId, goal);
    }

    private List<Parameter> getGoalParameters(Plugin plugin,
                                              String goal,
                                              MojoExecutor.ExecutionEnvironment executionEnvironment)
            throws MojoExecutionException {
        try {
            return MavenCompatibilityHelper.loadPluginDescriptor(
                            plugin,
                            executionEnvironment,
                            executionEnvironment.getMavenSession()
                    )
                    .getMojo(goal)
                    .getParameters();
        } catch (MojoExecutionException e) {
            throw e;
        } catch (Exception e) {
            throw new MojoExecutionException("Failed to load descriptor for " + plugin.getGroupId() + ":" + plugin.getArtifactId(), e);
        }
    }
    // >>> END: stolen shamelessly from AMPS (https://bitbucket.org/atlassian/amps/src/master/) - Apache2.0
}
