package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils;

import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.MojoExecutionException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipFile;

public class ZipUtils {
    private ZipUtils() {
    }

    public static void extractZipFile(File unpackDirectory, File archiveFile, boolean removeFirstDirectoryFromFile) throws MojoExecutionException {
        try (var zipFile = new ZipFile(archiveFile)) {
            var entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                var entry = entries.nextElement();
                var entryName = removeFirstDirectoryFromFile ? entry.getName().substring(entry.getName().indexOf("/")) : entry.getName();
                var entryDestination = new File(unpackDirectory, entryName);
                if (entryDestination.getCanonicalPath().startsWith(unpackDirectory.getCanonicalPath())) {
                    if (entry.isDirectory()) {
                        entryDestination.mkdirs();
                    } else {
                        entryDestination.getParentFile().mkdirs();
                        try (var in = zipFile.getInputStream(entry);
                             var out = new FileOutputStream(entryDestination)) {
                            IOUtils.copy(in, out);
                        }
                    }
                }
            }
        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }
}
