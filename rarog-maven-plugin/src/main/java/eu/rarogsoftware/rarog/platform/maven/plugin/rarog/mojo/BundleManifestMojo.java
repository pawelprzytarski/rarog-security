package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils.PluginHelper;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import static org.twdata.maven.mojoexecutor.MojoExecutor.goal;

/**
 * Generates manifest required by OSGi to make jar artifact OSGi compatible.
 */
@Mojo(name = "manifest", defaultPhase = LifecyclePhase.PREPARE_PACKAGE, requiresDependencyResolution = ResolutionScope.COMPILE)
public class BundleManifestMojo extends AbstractBundleRarogMojo {
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info("Generating manifest");
        getMojoExecutor().execute(PluginHelper.getBundlePlugin(getMavenContext()),
                goal("manifest"),
                getManifestConfig());
    }
}
