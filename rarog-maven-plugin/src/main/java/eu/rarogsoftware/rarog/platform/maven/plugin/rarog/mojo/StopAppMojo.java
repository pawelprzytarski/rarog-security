package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils.PluginHelper;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import static org.twdata.maven.mojoexecutor.MojoExecutor.configuration;

/**
 * Stops started container using cargo-maven-plugin
 */
@Mojo(name = "stop", defaultPhase = LifecyclePhase.POST_INTEGRATION_TEST)
public class StopAppMojo extends AbstractAppMojo {
    /**
     * If true will skip running app
     */
    @Parameter(property = "skipAppRun", defaultValue = "false")
    boolean skipAppRun;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (skipAppRun) {
            getLog().info("Skipped app start");
            return;
        }
        var containerInfo = getContainerInfo(getDefaultTomcatValues());

        getMojoExecutor()
                .execute(
                        PluginHelper.getCargoPlugin(getMavenContext()),
                        "stop",
                        configuration(
                                getCargoContainerConfig(containerInfo),
                                getCargoConfiguration(containerInfo)
                        )
                );
    }
}
