package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils;

import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.MavenContext;
import org.apache.maven.model.Plugin;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.twdata.maven.mojoexecutor.MojoExecutor;

import static org.twdata.maven.mojoexecutor.MojoExecutor.*;

public final class PluginHelper {
    private PluginHelper() {
    }

    public static Plugin getBundlePlugin(MavenContext context) {
        return context.getPlugin("org.apache.felix", "maven-bundle-plugin");
    }

    public static Plugin getCargoPlugin(MavenContext context) {
        return context.getPlugin("org.codehaus.cargo", "cargo-maven3-plugin");
    }

    public static Plugin getDependencyPlugin(MavenContext context) {
        return context.getPlugin("org.apache.maven.plugins", "maven-dependency-plugin");
    }

    public static Xpp3Dom getCopyConfiguration(String outputDirectory, boolean skipVersion, MojoExecutor.Element... element) {
        return configuration(
                element(
                        name("artifactItems"),
                        element
                ),
                element(name("outputDirectory"), outputDirectory),
                element(name("overWriteSnapshots"), "true"),
                element(name("stripClassifier"), "true"),
                element(name("stripVersion"), skipVersion ? "true" : "false")
        );
    }

    public static Element artifactItemElement(String groupId, String artifactId, String version, String type) {
        return element(name("artifactItem"),
                element(name("groupId"), groupId),
                element(name("artifactId"), artifactId),
                element(name("version"), version),
                element(name("type"), type)
        );
    }

    public static Element artifactItemElement(String groupId, String artifactId, String version, String type, String classifier) {
        return element(name("artifactItem"),
                element(name("groupId"), groupId),
                element(name("artifactId"), artifactId),
                element(name("version"), version),
                element(name("type"), type),
                element(name("classifier"), classifier)
        );
    }
}
