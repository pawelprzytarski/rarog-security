package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils.OsgiManifestHeaders;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.twdata.maven.mojoexecutor.MojoExecutor;

import java.io.File;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.twdata.maven.mojoexecutor.MojoExecutor.*;

public abstract class AbstractBundleRarogMojo extends AbstractRarogMojo {
    public static final String SPRING_ACTIVATOR_GROUP_ID = "eu.rarogsoftware.rarog.platform";
    public static final String SPRING_ACTIVATOR_ARTIFACT_ID = "spring-plugin-activator";
    public static final String SPRING_BUNDLE_ACTIVATOR_CLASS = "eu.rarogsoftware.rarog.platform.plugins.context.SpringContextPluginBundleActivator";
    private static final String[] SPRING_IMPORTS = new String[]{
            "org.springframework.context",
            "org.springframework.context.event",
            "org.springframework.context.config",
            "org.springframework.cglib",
            "org.springframework.cglib.core",
            "org.springframework.cglib.proxy",
            "org.springframework.cglib.reflect",
            "org.springframework.beans",
            "org.springframework.beans.factory",
            "org.springframework.beans.factory.annotation",
            "org.springframework.aop.framework.autoproxy",
            "org.springframework.aop.framework",
            "org.springframework.aop",
            "org.aopalliance.aop",
            "org.aopalliance.intercept",
            "org.slf4j",
            "!com.fasterxml.jackson.core.*",
            "!com.fasterxml.jackson.core",
            "!com.fasterxml.jackson.*"
    };

    /**
     * Info for plugin if default configuration for spring plugins should be automatically applied.
     * <p>
     * If true plugin will merge provided configuration with default configuration for spring based plugins.
     * </p>
     * <p>
     * This property replaces following part of configuration
     * <pre>
     *     &lt;instructions&gt;
     * 			&lt;Bundle-SymbolicName&gt;${project.artifactId}&lt;/Bundle-SymbolicName&gt;
     * 			&lt;Bundle-Name&gt;${project.name}&lt;/Bundle-Name&gt;
     * 			&lt;Bundle-Version&gt;${project.version}&lt;/Bundle-Version&gt;
     * 			&lt;Bundle-Activator&gt;
     * 				eu.rarogsoftware.rarog.platform.plugins.context.SpringContextPluginBundleActivator
     * 			&lt;/Bundle-Activator&gt;
     * 			&lt;Import-Package&gt;
     * 				org.osgi.framework.*,
     * 				org.springframework.context.event,
     * 				org.springframework.context.config,
     * 				org.springframework.cglib.core,
     * 				org.springframework.cglib.proxy,
     * 				org.springframework.cglib.reflect,
     * 				org.springframework.beans.factory,
     * 				org.springframework.beans.factory.annotation,
     * 				org.slf4j,
     * 				*
     * 			&lt;/Import-Package&gt;
     * 			&lt;Embed-Dependency&gt;*;scope=compile|runtime&lt;/Embed-Dependency&gt;
     * 			&lt;Embed-Transitive&gt;true&lt;/Embed-Transitive&gt;
     * 		&lt;/instructions&gt;
     *     </pre>
     * </p>
     */
    @Parameter(property = "spring.based.plugin", defaultValue = "true")
    protected boolean springBasedPlugin;

    /**
     * Instructions for bundle-maven-plugin.
     * Refer to <a href="https://felix.apache.org/documentation/subprojects/apache-felix-maven-bundle-plugin-bnd.html">bundle-maven-plugin docs</a>
     * for details.
     * <p>
     * Instructions provided through this property will be merged with properties calculated by rarog-maven-plugin
     * </p>
     */
    @Parameter
    protected Map<String, String> instructions;

    protected Map<String, String> getMergedInstructions() {
        var project = getMavenContext().project();
        var mergedInstructions = new HashMap<String, String>();
        if (instructions != null) {
            mergedInstructions.putAll(instructions);
        }
        mergedInstructions.computeIfAbsent(OsgiManifestHeaders.BUNDLE_SYMBOLICNAME, key -> project.getArtifactId());
        mergedInstructions.computeIfAbsent(OsgiManifestHeaders.BUNDLE_VERSION, key -> artifactVersion);
        if (!mergedInstructions.containsKey(OsgiManifestHeaders.BUNDLE_NAME) && StringUtils.isNotBlank(project.getName())) {
            mergedInstructions.put(OsgiManifestHeaders.BUNDLE_NAME, project.getName());
        }
        if (!mergedInstructions.containsKey(OsgiManifestHeaders.NOEE)) {
            mergedInstructions.put(OsgiManifestHeaders.NOEE, "true");
        }

        var outputDirPath = Path.of(getBuildOutputDirectory());
        var classpath = Optional.of(new File(getBuildOutputDirectory(), "META-INF/lib"))
                .filter(File::exists)
                .map(File::listFiles)
                .stream()
                .flatMap(Arrays::stream)
                .map(file -> outputDirPath.relativize(file.toPath()).toString())
                .collect(Collectors.joining(","));
        if (StringUtils.isNotBlank(classpath)) {
            mergedInstructions.put(OsgiManifestHeaders.BUNDLE_CLASSPATH, ".," + classpath);
        }

        if (springBasedPlugin) {
            var importInstructions = Stream.concat(
                    Arrays.stream(SPRING_IMPORTS),
                    Arrays.stream(mergedInstructions.getOrDefault(OsgiManifestHeaders.IMPORT_PACKAGE, "*").trim().split(","))
                            .map(String::trim)
            ).collect(Collectors.joining(","));
            mergedInstructions.put(OsgiManifestHeaders.IMPORT_PACKAGE, importInstructions);
            if (!mergedInstructions.containsKey(OsgiManifestHeaders.BUNDLE_ACTIVATOR) &&
                    hasActivatorDependency(project)) {
                mergedInstructions.put(OsgiManifestHeaders.BUNDLE_ACTIVATOR, SPRING_BUNDLE_ACTIVATOR_CLASS);
            }
        }
        return mergedInstructions;
    }

    private static boolean hasActivatorDependency(MavenProject project) {
        return ((List<Dependency>) project.getDependencies()).stream()
                .anyMatch(dep -> SPRING_ACTIVATOR_GROUP_ID.equalsIgnoreCase(dep.getGroupId())
                        && SPRING_ACTIVATOR_ARTIFACT_ID.equalsIgnoreCase(dep.getArtifactId())
                        && "compile".equalsIgnoreCase(dep.getScope()));
    }

    protected Xpp3Dom getManifestConfig() {
        final List<MojoExecutor.Element> instructionsElements = new ArrayList<>();
        for (final Map.Entry<String, String> entry : getMergedInstructions().entrySet()) {
            instructionsElements.add(element(entry.getKey(), entry.getValue()));
        }
        return configuration(
                element(name("instructions"), instructionsElements.toArray(new Element[0])),
                getSupportedProjectTypes()
        );
    }

    protected static MojoExecutor.Element getSupportedProjectTypes() {
        return element(name("supportedProjectTypes"),
                element(name("supportedProjectType"), "jar"),
                element(name("supportedProjectType"), "bundle"),
                element(name("supportedProjectType"), "war"),
                element(name("supportedProjectType"), "rarog-plugin"));
    }
}
