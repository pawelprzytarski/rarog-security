package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Generates rarog plugin manifest required by Rarog Platform to identify jar as valid rarog plugin.
 * This goal generated full manifest based on provided configuration. If no additional info is provided
 * goal reads basic data from maven project.
 */
@Mojo(name = "rarog-manifest", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class RarogManifestMojo extends AbstractRarogMojo {
    private final ObjectMapper objectMapper = new ObjectMapper()
            .configure(SerializationFeature.INDENT_OUTPUT, true);
    /**
     * Name of rarog plugin displayed in system.
     */
    @Parameter(property = "rarog.manifest.name", defaultValue = "${project.name}")
    private String rarogManifestName;
    /**
     * Unique key of rarog plugin. System identifies plugin using this key.
     * There cannot be installed two plugin with the same name.
     */
    @Parameter(property = "rarog.manifest.key", defaultValue = "${project.groupId}:${project.artifactId}")
    private String rarogManifestKey;
    /**
     * Version of rarog plugin. By default, the same version as artifact version.
     */
    @Parameter(property = "rarog.manifest.version", defaultValue = "${project.version}")
    private String rarogManifestVersion;
    /**
     * Description of rarog plugin.
     */
    @Parameter(property = "rarog.manifest.description", defaultValue = "${project.description}")
    private String rarogManifestDescription;
    /**
     * Location of plugin image in artifact.
     */
    @Parameter(property = "rarog.manifest.image")
    private String rarogManifestImage;
    /**
     * Location to write rarog plugin manifest.
     */
    @Parameter(property = "rarog.manifest.location", defaultValue = "${project.build.outputDirectory}/META-INF/rarog-plugin.json")
    private File rarogManifestLocation;
    /**
     * If true rarog-maven-plugin will generate fresh manifest each time. Otherwise it will generate manifest only when it does no exists at write location.
     */
    @Parameter(property = "rarog.manifest.override", defaultValue = "true")
    private boolean rarogManifestOverride;
    /**
     * If true rarog-maven-plugin will do not generate manifest file.
     */
    @Parameter(property = "rarog.manifest.skip", defaultValue = "false")
    private boolean rarogManifestSkip;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (rarogManifestSkip) {
            getLog().info("Skipping manifest generation.");
            return;
        }
        if (rarogManifestLocation.exists() && !rarogManifestOverride) {
            getLog().info("Manifest already exists. Skipping generation. You can override this behaviour with property 'rarog.manifest.override'.");
            return;
        }
        getLog().info("Writing rarog manifest: " + rarogManifestLocation);
        try {
            rarogManifestLocation.getParentFile().mkdirs();
            rarogManifestLocation.createNewFile();
        } catch (IOException e) {
            throw new MojoExecutionException("Manifest writing error", e);
        }
        try (var stream = new FileOutputStream(rarogManifestLocation)) {
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            objectMapper.writeValue(stream, new PluginManifest(rarogManifestKey, rarogManifestName, rarogManifestVersion, rarogManifestDescription, rarogManifestImage));
        } catch (IOException e) {
            throw new MojoExecutionException("Manifest writing error", e);
        }
    }

    record PluginManifest(String key, String name, String version, String description, String image) {
    }
}
