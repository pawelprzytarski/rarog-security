package eu.rarogsoftware.rarog.platform.maven.plugin.rarog.mojo;

import eu.rarogsoftware.rarog.platform.maven.plugin.rarog.utils.PluginHelper;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import static org.twdata.maven.mojoexecutor.MojoExecutor.*;

/**
 * Copies all dependencies in compile and runtime scope to META-INF/lib, so bundled plugin artifact can be
 * used as standalone rarog plugin.
 * This step is necessary in rarog plugin build process. If you want manually manage dependencies,
 * you should skip this step.
 */
@Mojo(name = "copy-embedded-dependencies", defaultPhase = LifecyclePhase.PREPARE_PACKAGE, requiresDependencyResolution = ResolutionScope.TEST)
public class CopyEmbedDependencies extends AbstractRarogMojo {
    /**
     * Set to true to skip this step.
     */
    @Parameter(property = "skip.dependency.bundling", defaultValue = "false")
    boolean skipDependencyBundling;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (skipDependencyBundling) {
            getLog().info("Skipping embedding dependencies");
            return;
        }
        getLog().info("Coping embedded dependencies to " + getBuildOutputDirectory() + "/META-INF/lib/");
        getMojoExecutor()
                .execute(
                        PluginHelper.getDependencyPlugin(getMavenContext()),
                        goal("copy-dependencies"),
                        configuration(
                                // Here, runtime means only compile- and runtime-scoped dependencies
                                element(name("includeScope"), "runtime"),
                                element(name("includeTypes"), "jar"),
                                element(name("outputDirectory"), "${project.build.outputDirectory}/META-INF/lib")
                        )
                );
    }
}
