import yaml
import shutil

from yaml import SafeLoader


class DocsUpdater:
    def __init__(self, base_path = './', dry_run = False):
        self.base_path = base_path
        self.dry_run = dry_run

    def update_docs(self):
        if not self.dry_run:
            shutil.copy(self.base_path + 'docs/antora.yml', self.base_path + 'docs/antora.yml.old')
        else:
            print("------------------- Dry run -------------------")
        project_version = self.read_project_version()
        print('Updating docs for release ' + project_version)

        antora_config = self.read_config()
        antora_config['version'] = project_version
        antora_config['display_version'] = project_version if not project_version.endswith("-SNAPSHOT") else "Latest snapshot"

        if not self.dry_run:
            self.save_changelog(antora_config)
        else:
            print(yaml.dump(antora_config))
        print('Docs updated')

    def save_changelog(self, config):
        with open(self.base_path + 'docs/antora.yml', 'w') as config_file:
            config_file.write(yaml.dump(config))

    def read_config(self):
        with open(self.base_path + 'docs/antora.yml', 'r') as config_file:
            return yaml.load(config_file, Loader=SafeLoader)

    def read_project_version(self):
        from xml.etree import ElementTree as elementTree
        ns = "http://maven.apache.org/POM/4.0.0"
        elementTree.register_namespace('', ns)
        tree = elementTree.ElementTree()
        tree.parse(self.base_path + 'pom.xml')
        project_version = tree.getroot().find("{%s}version" % ns).text
        return project_version


if __name__ == '__main__':
    DocsUpdater("./", False).update_docs()