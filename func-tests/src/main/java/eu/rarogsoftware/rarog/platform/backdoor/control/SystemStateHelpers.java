package eu.rarogsoftware.rarog.platform.backdoor.control;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import org.apache.http.Header;

import java.io.IOException;
import java.util.List;

public class SystemStateHelpers extends BaseControl {
    public SystemStateHelpers(EnvironmentData environmentData) {
        super(environmentData);
    }

    public AccountInfo getLoggedInAccountState(Header... headers) throws IOException {
        return getEntity(systemStatePath().path("account/loggedin"), AccountInfo.class, headers);
    }

    private PathFragments systemStatePath() {
        return backdoorPath().path("system").path("state");
    }


    public record AccountInfo(String name, String type, List<String> authorities, String userClassType) {
    }
}
