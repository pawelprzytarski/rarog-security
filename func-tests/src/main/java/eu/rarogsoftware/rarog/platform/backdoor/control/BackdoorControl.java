package eu.rarogsoftware.rarog.platform.backdoor.control;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;

public class BackdoorControl {
    private final ApplicationSettingsControl applicationSettingsControl;
    private final PluginsControl pluginsControl;
    private final UsersControl usersControl;
    private final SystemStateHelpers systemStateHelpers;
    private final TomcatControl tomcatControl;

    public BackdoorControl(EnvironmentData environmentData) {
        applicationSettingsControl = new ApplicationSettingsControl(environmentData);
        pluginsControl = new PluginsControl(environmentData);
        usersControl = new UsersControl(environmentData);
        systemStateHelpers = new SystemStateHelpers(environmentData);
        tomcatControl = new TomcatControl(environmentData);
    }

    public ApplicationSettingsControl applicationSettings() {
        return applicationSettingsControl;
    }

    public PluginsControl pluginsControl() {
        return pluginsControl;
    }

    public UsersControl usersControl() {
        return usersControl;
    }

    public SystemStateHelpers systemStateHelpers() {
        return systemStateHelpers;
    }

    public TomcatControl tomcatControl() {
        return tomcatControl;
    }
}
