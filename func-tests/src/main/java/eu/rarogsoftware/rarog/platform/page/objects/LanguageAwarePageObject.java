package eu.rarogsoftware.rarog.platform.page.objects;

import eu.rarogsoftware.commons.test.webdriver.playwright.PlaywrightPageObject;

public abstract class LanguageAwarePageObject extends PlaywrightPageObject {
    private static String defaultLanguage = "en-MOON";
    private String language = defaultLanguage;

    public static void changeDefaultLanguage(String tag) {
        defaultLanguage = tag;
    }

    public void setCurrentLanguage(String tag) {
        language = tag;
    }

    @Override
    public int getTimeout() {
        return 60000;
    }

    protected String wrapUrlWithLanguageParameter(String url) {
        if (url.contains("?")) {
            return url + "&lng=" + language;
        } else {
            return url + "?lng=" + language;
        }
    }
}
