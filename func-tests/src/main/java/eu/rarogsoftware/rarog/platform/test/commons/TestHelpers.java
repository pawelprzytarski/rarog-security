package eu.rarogsoftware.rarog.platform.test.commons;

import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpRequestBase;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class TestHelpers {
    public static final String BASIC_AUTH_HEADER_WITH_ADMIN_CREDENTIALS = "Basic " + Base64.getEncoder().encodeToString("admin:admin".getBytes(StandardCharsets.UTF_8));
    public static final String BASIC_AUTH_HEADER_WITH_USER_CREDENTIALS = "Basic " + Base64.getEncoder().encodeToString("user1:user1".getBytes(StandardCharsets.UTF_8));

    public static void addAdminAuthHeader(HttpRequestBase httpPost) {
        httpPost.addHeader(HttpHeaders.AUTHORIZATION, BASIC_AUTH_HEADER_WITH_ADMIN_CREDENTIALS);
    }

    public static void addUserAuthHeader(HttpRequestBase httpPost) {
        httpPost.addHeader(HttpHeaders.AUTHORIZATION, BASIC_AUTH_HEADER_WITH_USER_CREDENTIALS);
    }

    public static void addDisableCsrfHeader(HttpRequestBase httpPost) {
        httpPost.addHeader("X-CSRF-DISABLE", "true");
    }
}
