package eu.rarogsoftware.rarog.platform.junit.extensions;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class HttpClientExtension implements BeforeEachCallback, AfterEachCallback {
    private static final int TIMEOUT = 5000;

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        if (context.getTestInstance().isEmpty()) {
            return;
        }
        var o = context.getTestInstance().get();
        var field = ExtensionHelpers.getDataField(o, CloseableHttpClient.class);
        if (field == null) {
            return;
        }
        field.set(o, createDefaultHttpClient());
    }

    public CloseableHttpClient createDefaultHttpClient() {
        return HttpClientBuilder.create()
                .disableAutomaticRetries()
                .setDefaultRequestConfig(RequestConfig.copy(RequestConfig.DEFAULT)
                        .setConnectionRequestTimeout(TIMEOUT)
                        .setConnectTimeout(TIMEOUT)
                        .setSocketTimeout(TIMEOUT)
                        .build())
                .build();
    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        if (context.getTestInstance().isEmpty()) {
            return;
        }
        var o = context.getTestInstance().get();
        var field = ExtensionHelpers.getDataField(o, CloseableHttpClient.class);
        if (field == null) {
            return;
        }
        var httpClient = field.get(o);
        if (httpClient instanceof CloseableHttpClient closeableHttpClient) {
            closeableHttpClient.close();
        }
    }
}
