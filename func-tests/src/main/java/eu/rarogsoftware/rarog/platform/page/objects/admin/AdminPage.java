package eu.rarogsoftware.rarog.platform.page.objects.admin;

import com.microsoft.playwright.Frame;
import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import eu.rarogsoftware.rarog.platform.page.objects.LanguageAwarePageObject;

import java.util.List;
import java.util.stream.IntStream;

public abstract class AdminPage extends LanguageAwarePageObject {
    private Locator navbar;
    private Locator content;

    private final QuickNav quickNav = new QuickNav();
    private final ProfileNav profileNav = new ProfileNav();

    public Locator getMenuElement(String elementKey) {
        return navbar.locator("span[data-key=\"%s\"]".formatted(elementKey));
    }

    public Locator getCurrentContent() {
        var iframeBody = getPage().frameLocator("main iframe").locator("body");
        if (iframeBody.isVisible()) {
            return iframeBody;
        }
        return content;
    }

    public String getContentText() {
        return getCurrentContent().textContent();
    }

    public void pushHotKey(String keys) {
        content.press(keys);
    }

    public QuickNav quickNav() {
        return quickNav;
    }

    public ProfileNav profileNav() {
        return profileNav;
    }

    public void navigateByLinkClick(String elementKey) {
        var elements = elementKey.split("/");
        var currentKey = new StringBuilder(elements[0]);
        for (int i = 1; i < elements.length; i++) {
            var element = getMenuElement(currentKey.toString());
            if (!element.getAttribute("class").contains("active")) {
                element.click();
            }
            currentKey.append("/").append(elements[i]);
        }
        getMenuElement(currentKey.toString()).click();
    }

    @Override
    public void bind() {
        navbar = getPage().locator("#navBar");
        content = getPage().locator("main.setting");
        quickNav.bind(getPage());
        profileNav.bind(getPage());
    }

    @Override
    public boolean isAt() {
        return navbar.isVisible() && content.isVisible();
    }

    public static class QuickNav {
        private Locator input;
        private Locator dropdown;

        void bind(Page page) {
            input = page.locator("#adminQuickNavigation input");
            dropdown = page.locator("#adminQuickNavigation .dropdown-menu");
        }

        public boolean isMenuVisible() {
            return dropdown.isVisible();
        }

        public void selectItem(int index) {
            dropdown.locator(".dropdown-item").nth(index).click();
        }

        public List<Element> getDisplayedElements() {
            var elements = dropdown.locator(".dropdown-item");
            return IntStream.range(0, elements.count())
                    .mapToObj(elements::nth)
                    .map(element -> new Element(
                            element.locator("span.name").textContent(),
                            element.locator("span.keywords").textContent()
                    ))
                    .toList();
        }

        public void open() {
            input.click();
        }

        public void close() {
            input.press("Escape");
        }

        public void input(String text) {
            input.fill(text);
        }

        public record Element(String name, String keywords) {
        }
    }

    public static class ProfileNav {
        private Locator profileLink;
        private Locator logoutLink;
        private Locator menuButton;

        void bind(Page page) {
            menuButton = page.locator("#quickAccessMenu .profile button");
            profileLink = page.locator("#quickAccessMenu .profile a[data-key=\"profile\"]");
            logoutLink = page.locator("#quickAccessMenu .profile a[data-key=\"logout\"]");
        }

        public void toggleMenu() {
            menuButton.click();
        }

        public void clickProfileLink() {
            clickLink(profileLink);
        }

        private void clickLink(Locator linkItem) {
            if (!linkItem.isVisible()) {
                toggleMenu();
            }
            linkItem.click();
        }

        public void clickLogoutLink() {
            clickLink(logoutLink);
        }
    }
}
