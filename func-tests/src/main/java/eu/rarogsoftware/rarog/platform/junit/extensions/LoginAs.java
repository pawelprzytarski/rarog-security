package eu.rarogsoftware.rarog.platform.junit.extensions;

import java.lang.annotation.*;

/**
 * Performs logout and login before running annotated test (set of tests in class), if needed.
 */
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginAs {
    /**
     * Username of user to log in as
     */
    String value();

    /**
     * Password of user to log in as
     */
    String password() default "";

    /**
     * If true then it will always logout and login in, not only when currently logged in user do not match
     */
    boolean forceRelog() default false;
}
