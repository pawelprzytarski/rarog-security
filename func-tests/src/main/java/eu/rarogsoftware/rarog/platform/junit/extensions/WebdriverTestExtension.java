package eu.rarogsoftware.rarog.platform.junit.extensions;

import eu.rarogsoftware.commons.test.webdriver.junit.PlaywrightTesterExtension;

import java.util.List;

/**
 * Container extension that contains all useful extension for writing webdriver based func tests.
 */
public class WebdriverTestExtension extends FuncTestExtension {
    public WebdriverTestExtension() {
        super(List.of(
                new PlaywrightTesterExtension(),
                new LoginAsPlaywrightExtension()
        ));
    }
}
