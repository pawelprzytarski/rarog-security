package eu.rarogsoftware.rarog.platform.junit.extensions;

import eu.rarogsoftware.rarog.platform.backdoor.control.ApplicationSettingsControl;
import eu.rarogsoftware.rarog.platform.test.ApplicationSettingKeys;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class TestMetricExtension implements BeforeEachCallback, AfterEachCallback {
    private static final String STORE_KEY = TestMetricExtension.class.getName() + "_originalValues";
    private static final String TEST_SERVER_ID = "TestInstanceId";

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        var store = ExtensionHelpers.getStore(context);
        var applicationSetting = new ApplicationSettingsControl(ExtensionHelpers.getEnvironmentData(context));
        var originalData = store.get(STORE_KEY, OriginalData.class);

        applicationSetting.setSetting(ApplicationSettingKeys.SYSTEM_ID_KEY, originalData.serverId);
        applicationSetting.setSetting(ApplicationSettingKeys.METRICS_SNAPSHOT_PERIOD_KEY, originalData.snapshotPeriod);
        applicationSetting.setSetting(ApplicationSettingKeys.METRICS_EXPORT_PERIOD_KEY, originalData.exportPeriod);
        applicationSetting.setSetting(ApplicationSettingKeys.METRICS_GRAPHITE_EXPORT_ENABLED_KEY, originalData.metricsEnabled);
        applicationSetting.setSetting(ApplicationSettingKeys.METRICS_TELEMETRY_EXPORT_ENABLED_KEY, originalData.telemetryEnabled);
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        var store = ExtensionHelpers.getStore(context);
        var applicationSetting = new ApplicationSettingsControl(ExtensionHelpers.getEnvironmentData(context));
        var originalServerId = applicationSetting.getSetting(ApplicationSettingKeys.SYSTEM_ID_KEY);
        var originalSnapshotPeriod = applicationSetting.getSetting(ApplicationSettingKeys.METRICS_SNAPSHOT_PERIOD_KEY);
        var originalExportPeriod = applicationSetting.getSetting(ApplicationSettingKeys.METRICS_EXPORT_PERIOD_KEY);
        var originalMetricsEnabled = applicationSetting.getSetting(ApplicationSettingKeys.METRICS_GRAPHITE_EXPORT_ENABLED_KEY);
        var originalTelemetryEnabled = applicationSetting.getSetting(ApplicationSettingKeys.METRICS_TELEMETRY_EXPORT_ENABLED_KEY);

        store.put(STORE_KEY, new OriginalData(originalServerId.value().toString(),
                originalSnapshotPeriod.value().toString(),
                originalExportPeriod.value().toString(),
                originalMetricsEnabled.value().toString(),
                originalTelemetryEnabled.value().toString()));

        applicationSetting.setSetting(ApplicationSettingKeys.SYSTEM_ID_KEY, TEST_SERVER_ID);
        applicationSetting.setSetting(ApplicationSettingKeys.METRICS_SNAPSHOT_PERIOD_KEY, 1);
        applicationSetting.setSetting(ApplicationSettingKeys.METRICS_EXPORT_PERIOD_KEY, 15);
    }

    record OriginalData(String serverId, String snapshotPeriod, String exportPeriod, String metricsEnabled, String telemetryEnabled) {
    }
}
