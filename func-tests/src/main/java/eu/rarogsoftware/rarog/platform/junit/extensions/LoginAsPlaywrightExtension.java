package eu.rarogsoftware.rarog.platform.junit.extensions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.commons.test.webdriver.WebdriverTester;
import eu.rarogsoftware.commons.test.webdriver.junit.PlaywrightTesterExtension;
import eu.rarogsoftware.rarog.platform.page.objects.JsonPage;
import eu.rarogsoftware.rarog.platform.page.objects.login.LoginPage;
import eu.rarogsoftware.rarog.platform.page.objects.login.MfaPage;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class LoginAsPlaywrightExtension implements BeforeEachCallback {
    private final Logger logger = LoggerFactory.getLogger(LoginAsPlaywrightExtension.class);
    private final ObjectMapper objectMapper = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {

        var annotation = context.getRequiredTestMethod().getAnnotation(LoginAs.class);
        if (annotation == null) {
            annotation = context.getRequiredTestClass().getAnnotation(LoginAs.class);
        }
        if (annotation != null) {
            logger.info("Run test as `{}`", annotation.value());
            var tester = PlaywrightTesterExtension.getContext(context).tester();
            if (annotation.forceRelog() || !alreadyLoggedIn(annotation.value(), tester)) {
                logger.info("Log in user `{}`", annotation.value());
                tester.goTo("/logout");
                var loginPage = tester.goTo(LoginPage.class);
                loginPage.login(annotation.value(), StringUtils.isNotBlank(annotation.password()) ? annotation.password() : annotation.value());
                if(tester.getCurrentUrl().contains("mfa")) {
                    logger.info("Bypassing mfa");
                    tester.bindPage(MfaPage.class).byPass();
                }
            }
        }
    }

    private boolean alreadyLoggedIn(String username, WebdriverTester tester) throws JsonProcessingException {
        tester.goTo("/status/");
        var statusJson = tester.bindPage(JsonPage.class).getJsonValue();
        var data = objectMapper.readValue(statusJson, StatusData.class);
        logger.debug("Currently logged in user `{}`", data.loggedInUser());
        return Objects.equals(username, data.loggedInUser());
    }

    record StatusData(String loggedInUser) {
    }
}
