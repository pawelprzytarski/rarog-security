package eu.rarogsoftware.rarog.platform.page.objects.login;

import com.microsoft.playwright.Locator;
import eu.rarogsoftware.commons.test.webdriver.PageObject;
import eu.rarogsoftware.rarog.platform.page.objects.LanguageAwarePageObject;

public class RecoveryCodePage extends LanguageAwarePageObject {
    public Locator title;
    public Locator description;
    public Locator codeInput;
    public Locator submitButton;

    @Override
    public boolean isAt() {
        return getPage().isVisible("#code");
    }

    public void enterCode(String code) {
        codeInput.fill(code);
        getPage().waitForNavigation(() -> submitButton.click());
    }

    public <U extends PageObject> U enterCodeAndBind(String code, Class<U> bindPage) {
        codeInput.fill(code);
        submitButton.click();
        return getTester().bindPage(bindPage);
    }

    @Override
    public void bind() {
        title = getPage().locator("h3");
        description = getPage().locator("p");
        codeInput = getPage().locator("#code");
        submitButton = getPage().locator("#submit");
        getPage().waitForSelector("#code");
    }

    @Override
    public String getTargetPageUrl() {
        throw new IllegalStateException("This page cannot be accessed directly!");
    }
}
