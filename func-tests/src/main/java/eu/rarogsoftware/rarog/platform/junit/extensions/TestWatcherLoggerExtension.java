package eu.rarogsoftware.rarog.platform.junit.extensions;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.io.IOException;
import java.util.Map;

@SuppressFBWarnings(value = {"HTTP_PARAMETER_POLLUTION"}, justification = "Test only class")
public class TestWatcherLoggerExtension implements BeforeEachCallback, AfterEachCallback {
    private static final ObjectMapper MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        sendLogToServer(context, Map.of(
                "before", true,
                "testName", getTestName(context)
        ));
    }

    private static void sendLogToServer(ExtensionContext context, Map<Object, Object> value) throws IOException {
        var data = ExtensionHelpers.getEnvironmentData(context);
        try (var httpClient = HttpClientBuilder.create()
                .disableRedirectHandling()
                .build()) {
            var httpPost = new HttpPost(data.baseUrl() + "rest/backdoor/1.0/test/logging");
            httpPost.setEntity(new ByteArrayEntity(MAPPER.writeValueAsBytes(value), ContentType.APPLICATION_JSON));
            httpClient.execute(httpPost);
        }
    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        sendLogToServer(context, Map.of(
                "before", false,
                "testName", getTestName(context),
                "error", context.getExecutionException()
                        .map(TestWatcherLoggerExtension::getErrorLog)
                        .orElse("")
        ));
    }

    private static String getTestName(ExtensionContext context) {
        return context.getRequiredTestClass().getName() + "#" + context.getDisplayName();
    }

    private static String getErrorLog(Throwable throwable) {
        if (throwable instanceof AssertionError) {
            return ExceptionUtils.getMessage(throwable);
        } else {
            return ExceptionUtils.getStackTrace(throwable);
        }
    }
}
