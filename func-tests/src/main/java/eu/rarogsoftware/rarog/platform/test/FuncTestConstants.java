package eu.rarogsoftware.rarog.platform.test;

public final class FuncTestConstants {
    public static final String FUNC_TEST_PLUGIN_KEY = "eu.rarogsoftware.rarog.platform.plugins:func-test-plugin";

    private FuncTestConstants() {
    }
}
