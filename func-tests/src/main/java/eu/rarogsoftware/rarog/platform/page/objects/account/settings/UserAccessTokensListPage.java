package eu.rarogsoftware.rarog.platform.page.objects.account.settings;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.WaitForSelectorState;
import eu.rarogsoftware.rarog.platform.page.objects.LanguageAwarePageObject;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class UserAccessTokensListPage extends LanguageAwarePageObject {
    private static final String CREATE_TAB_ID = "#accesstokenstab-tab-create";
    private static final String PANEL_TAB_ID = "#accesstokenstab-tabpane-list";
    protected Locator createTabButton;

    public String getToken(Long id) {
        return StringUtils.trim(getPage().locator("#tokenItem_" + id).innerText());
    }

    public List<String> getTokens() {
        return getPage().locator("tbody > tr").allInnerTexts()
                .stream()
                .map(StringUtils::trim)
                .toList();
    }

    public List<Long> getTokenIds() {
        return getPage().mainFrame().querySelectorAll("tbody > tr a").stream()
                .map(element -> element.getAttribute("data-id"))
                .map(Long::parseLong)
                .toList();
    }

    public void removeToken(Long id) {
        getPage().locator("#deleteToken_" + id).click();
        getPage().waitForSelector("#tokenItem_" + id, new Page.WaitForSelectorOptions().setState(WaitForSelectorState.DETACHED));
    }

    public CreateUserTokenPageUser clickCreateButtonNav() {
        createTabButton.click();
        return getTester().bindPage(CreateUserTokenPageUser.class);
    }

    @Override
    public void bind() {
        var page = getPage();
        createTabButton = page.locator(CREATE_TAB_ID);
        page.waitForSelector(PANEL_TAB_ID);
    }

    @Override
    public String getTargetPageUrl() {
        return "/account/my/accesstokens#list";
    }

    @Override
    public boolean isAt() {
        return getPage().isVisible(PANEL_TAB_ID);
    }
}

