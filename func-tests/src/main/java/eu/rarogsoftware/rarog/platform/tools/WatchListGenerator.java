package eu.rarogsoftware.rarog.platform.tools;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class WatchListGenerator {
    private final Logger logger = LoggerFactory.getLogger(WatchListGenerator.class);
    private final ObjectMapper xmlMapper = new XmlMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public static void main(String[] arguments) {
        new WatchListGenerator().run(arguments);
    }

    private void run(String[] arguments) {
        logger.info("Input args: {}", (Object) arguments);
        if (arguments.length != 2) {
            logger.error("Incorrect number of arguments provided");
            System.exit(-1);
        }
        var directoriesToSearch = arguments[0];
        var outputFile = new File(arguments[1]);

        var listOfArtifacts = Arrays.stream(directoriesToSearch.split(","))
                .map(path -> {
                    try {
                        return Path.of(path);
                    } catch (InvalidPathException ignored) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .map(path -> path.normalize().toFile())
                .filter(directory -> directory.exists() && directory.isDirectory())
                .flatMap(directory -> Optional.ofNullable(directory.listFiles()).stream().flatMap(Arrays::stream))
                .filter(directory -> directory.exists() && directory.isDirectory())
                .map(directory -> {
                    var pom = new File(directory, "pom.xml");
                    if (!(pom.exists() && pom.isFile() && pom.canRead())) {
                        return "";
                    }
                    try {
                        var doc = xmlMapper.readValue(pom, Pom.class);
                        var version = doc.version != null ? doc.version : doc.parent.version;
                        var artifactFile = new File(directory, "target/" + doc.artifactId + "-" + version + ".jar");
                        if (artifactFile.exists() && artifactFile.isFile() && artifactFile.canRead()) {
                            return artifactFile.getAbsolutePath();
                        }
                        return "";
                    } catch (IOException e) {
                        return "";
                    }
                })
                .filter(StringUtils::isNotBlank)
                .filter(path -> !path.contains("/plugins/pat-plugin/"))
                .collect(Collectors.joining(";"));
        logger.info("Saving watchlist [{}]", listOfArtifacts);
        var parentFile = outputFile.getParentFile();
        if (parentFile.exists() || parentFile.mkdirs()) {
            try (var stream = new FileOutputStream(outputFile)) {
                IOUtils.write(listOfArtifacts, stream, StandardCharsets.UTF_8);
            } catch (IOException e) {
                logger.error("Failed to write watchlist file", e);
                System.exit(-1);
            }
        } else {
            logger.error("Failed to create directory {} for watchlist file", parentFile);
            System.exit(-1);
        }
    }

    record Parent(String groupId, String version) {
    }

    record Pom(String packaging, String artifactId, String version, Parent parent) {
    }
}
