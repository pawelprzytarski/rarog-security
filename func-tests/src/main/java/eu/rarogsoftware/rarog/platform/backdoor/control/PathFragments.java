package eu.rarogsoftware.rarog.platform.backdoor.control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PathFragments {
    private final List<String> fragments;

    public PathFragments(List<String> fragments) {
        this.fragments = fragments;
    }

    public PathFragments(String fragment) {
        this.fragments = Collections.singletonList(fragment);
    }


    public PathFragments path(String pathFragment) {
        var newFragments = new ArrayList<>(this.fragments);
        newFragments.add(pathFragment);
        return new PathFragments(newFragments);
    }

    public static PathFragments of(String... fragments) {
        return new PathFragments(Arrays.asList(fragments));
    }

    @Override
    public String toString() {
        return String.join("/", fragments);
    }
}
