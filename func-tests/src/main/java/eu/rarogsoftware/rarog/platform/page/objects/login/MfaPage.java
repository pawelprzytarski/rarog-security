package eu.rarogsoftware.rarog.platform.page.objects.login;

import com.microsoft.playwright.Locator;
import eu.rarogsoftware.commons.test.webdriver.PageObject;
import eu.rarogsoftware.rarog.platform.page.objects.LanguageAwarePageObject;

public class MfaPage extends LanguageAwarePageObject {
    private final String redirectUrl;
    public Locator methodsList;
    public Locator oneTimePasswordLink;
    public Locator recoveryCodeLink;
    public Locator justPassLink;

    public MfaPage(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public MfaPage() {
        this(null);
    }

    @Override
    public String getTargetPageUrl() {
        return wrapUrlWithLanguageParameter("/login/mfa/" + (redirectUrl == null ? "" : ("?redirectUrl=" + redirectUrl)));
    }

    @Override
    public void bind() {
        methodsList = getPage().locator("ul");
        oneTimePasswordLink = getPage().locator("#TOTP");
        recoveryCodeLink = getPage().locator("#RECOVERY-CODE");
        justPassLink = getPage().locator("#JUST-PASS");
        getPage().waitForSelector("#RECOVERY-CODE");
    }

    @Override
    public boolean isAt() {
        return getPage().isVisible("#RECOVERY-CODE");
    }

    public void byPass() {
        if (justPassLink == null) {
            throw new IllegalStateException("Dev mode is not enabled or mfa by pass is broken");
        }
        justPassLink.click();
        getPage().waitForSelector("#justPassLink");
        getPage().waitForNavigation(() -> getPage().click("#justPassLink"));
    }

    public RecoveryCodePage selectRecoveryCode() {
        recoveryCodeLink.click();
        return getTester().bindPage(RecoveryCodePage.class);
    }

    public <U extends PageObject> U byPassAndBind(Class<U> bindPage) {
        byPass();
        return getTester().bindPage(bindPage);
    }
}
