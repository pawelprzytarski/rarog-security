package eu.rarogsoftware.rarog.platform.page.objects.login;

import com.microsoft.playwright.Locator;
import eu.rarogsoftware.commons.test.webdriver.PageObject;
import eu.rarogsoftware.rarog.platform.page.objects.LanguageAwarePageObject;

import java.util.Arrays;
import java.util.List;

public class RecoveryCodeRegeneratedCodesPage extends LanguageAwarePageObject {
    public Locator title;
    public Locator description;
    public Locator codes;
    public Locator proceedButton;

    @Override
    public boolean isAt() {
        return getPage().isVisible("#codes");
    }

    public List<String> getCodes() {
        return Arrays.asList(codes.innerHTML().replace("<li>", "").split("</li>"));
    }

    public void proceed() {
        getPage().waitForNavigation(() -> proceedButton.click());
    }

    public <U extends PageObject> U proceedAndBind(Class<U> bindPage) {
        proceed();
        return getTester().bindPage(bindPage);
    }

    @Override
    public void bind() {
        title = getPage().locator("h3");
        description = getPage().locator("p");
        codes = getPage().locator("#codes");
        proceedButton = getPage().locator("#proceed");
        getPage().waitForSelector("#codes");
    }

    @Override
    public String getTargetPageUrl() {
        throw new IllegalStateException("This page cannot be accessed directly!");
    }
}
