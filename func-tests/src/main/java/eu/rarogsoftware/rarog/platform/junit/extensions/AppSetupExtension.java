package eu.rarogsoftware.rarog.platform.junit.extensions;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.util.concurrent.TimeUnit;

@SuppressFBWarnings(value = {"HTTP_PARAMETER_POLLUTION"}, justification = "Test only class")
public class AppSetupExtension implements BeforeAllCallback {
    private static final int APP_START_TIMEOUT = 60;
    private static final ObjectMapper MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception {
        var data = ExtensionHelpers.getEnvironmentData(extensionContext);
        try (var httpClient = HttpClientBuilder.create()
                .disableRedirectHandling()
                .build()) {
            var httpGet = new HttpGet(data.baseUrl() + "boot/");
            var stopWatch = StopWatch.createStarted();
            var started = false;
            while (stopWatch.getTime(TimeUnit.SECONDS) < APP_START_TIMEOUT) {
                var httpResponse = httpClient.execute(httpGet);
                httpGet.releaseConnection();
                if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                    started = true;
                    break;
                } else {
                    Thread.sleep(500);
                }
            }
            if (!started) {
                throw new IllegalStateException("Server didn't start on time");
            }
            var statusGet = new HttpGet(data.baseUrl() + "status/");
            var statusResponse = httpClient.execute(statusGet);
            if (statusResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new IllegalStateException("Server failed to start");
            }
            var status = MAPPER.readValue(statusResponse.getEntity().getContent(), StatusBean.class);
            if (!"OK".equalsIgnoreCase(status.serverStatus)) {
                throw new IllegalStateException("Server failed to start");
            }
        }
    }

    record StatusBean(String serverStatus) {
    }
}
