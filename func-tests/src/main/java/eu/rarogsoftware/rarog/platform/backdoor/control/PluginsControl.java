package eu.rarogsoftware.rarog.platform.backdoor.control;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.springframework.http.MediaType;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class PluginsControl extends BaseControl {
    public PluginsControl(EnvironmentData environmentData) {
        super(environmentData);
    }

    public PluginRecord[] getInstalledPlugins() throws IOException {
        return getEntity(pluginsPath(), PluginRecord[].class, adminAuthHeader());
    }

    public PluginRecord getPlugin(String pluginKey) throws IOException {
        return getEntity(pluginsPath().path(pluginKey), PluginRecord.class, adminAuthHeader());
    }

    public CloseableHttpResponse getPluginResponse(String pluginKey) throws IOException {
        return getResponse(pluginsPath().path(pluginKey).toString(), adminAuthHeader());
    }

    public CloseableHttpResponse purgePlugin(String pluginKey) throws IOException {
        return postEntity(pluginsPath().path(pluginKey).path("purge"), null, disableCsrfProtectionHeader(), adminAuthHeader());
    }

    public CloseableHttpResponse enablePlugin(String pluginKey) throws IOException {
        return putEntity(pluginsPath().path(pluginKey).path("enabled"), Map.of("value", true), adminAuthHeader());
    }

    public CloseableHttpResponse disablePlugin(String pluginKey) throws IOException {
        return putEntity(pluginsPath().path(pluginKey).path("enabled"), Map.of("value", false), adminAuthHeader());
    }

    public CloseableHttpResponse installPlugin(String path) throws IOException {
        return installPlugin(path, true);
    }

    public CloseableHttpResponse installPlugin(String path, boolean reinstall) throws IOException {
        return postEntity(pluginsPath(), Map.of("path", path, "reinstallIfNeeded", reinstall), adminAuthHeader());
    }

    public CloseableHttpResponse installPlugin(File file) throws IOException {
        return postEntity(pluginsPath(), file, new BasicHeader(HTTP.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE));
    }

    public CloseableHttpResponse uninstallPlugin(String pluginKey) throws IOException {
        return sendDelete(pluginsPath().path(pluginKey), adminAuthHeader(), disableCsrfProtectionHeader());
    }

    private PathFragments pluginsPath() {
        return basePath().path("plugins");
    }

    public record PluginManifest(String key, String name, String version, String description, String image) {
    }

    public record PluginRecord(String key, PluginManifest manifest, String state) {
    }
}
