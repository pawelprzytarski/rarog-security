package eu.rarogsoftware.rarog.platform.junit.extensions;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.extension.ExtensionContext;

import javax.inject.Inject;
import java.lang.reflect.Field;

import static eu.rarogsoftware.commons.test.webdriver.junit.EnvironmentDataExtension.STORE_BEFORE_ALL_KEY;
import static eu.rarogsoftware.commons.test.webdriver.junit.EnvironmentDataExtension.STORE_BEFORE_EACH_KEY;

public final class ExtensionHelpers {
    private ExtensionHelpers() {
    }

    public static EnvironmentData getEnvironmentData(ExtensionContext context) {
        var store = getStore(context);
        var environmentData = store.get(STORE_BEFORE_EACH_KEY, EnvironmentData.class);
        if (environmentData == null) {
            environmentData = store.get(STORE_BEFORE_ALL_KEY, EnvironmentData.class);
        }
        if (environmentData == null) {
            throw new IllegalStateException("EnvironmentalData are not available");
        }
        return environmentData;
    }

    public static ExtensionContext.Store getStore(ExtensionContext extensionContext) {
        return extensionContext.getStore(ExtensionContext.Namespace.create(extensionContext.getRequiredTestClass()));
    }

    public static <T> Field getDataField(Object o, Class<T> expectedFieldClass) {
        return getDataField(o, o.getClass(), expectedFieldClass);
    }

    public static <T> Field getDataField(Object o, Class<?> clazz, Class<T> expectedFieldClass) {
        if (clazz.equals(Object.class)) {
            return null;
        }
        Field dataField = getDataField(o, clazz.getSuperclass(), expectedFieldClass);
        for (Field field : clazz.getDeclaredFields()) {
            if (expectedFieldClass.isAssignableFrom(field.getType())
                    && (field.isAnnotationPresent(Inject.class))) {
                if (!field.canAccess(o)) {
                    field.setAccessible(true);
                }
                dataField = field;
            }
        }
        return dataField;
    }
}
