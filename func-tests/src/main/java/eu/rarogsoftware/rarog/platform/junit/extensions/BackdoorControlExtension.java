package eu.rarogsoftware.rarog.platform.junit.extensions;

import eu.rarogsoftware.rarog.platform.backdoor.control.BackdoorControl;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class BackdoorControlExtension implements BeforeEachCallback, BeforeAllCallback {
    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        if (context.getTestInstance().isEmpty()) {
            return;
        }
        var o = context.getTestInstance().get();
        var field = ExtensionHelpers.getDataField(o, BackdoorControl.class);
        if (field == null) {
            return;
        }
        var environmentData = ExtensionHelpers.getEnvironmentData(context);
        field.set(o, new BackdoorControl(environmentData));
    }

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        beforeEach(context);
    }
}
