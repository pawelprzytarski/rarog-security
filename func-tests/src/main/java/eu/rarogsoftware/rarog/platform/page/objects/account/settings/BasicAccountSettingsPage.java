package eu.rarogsoftware.rarog.platform.page.objects.account.settings;

import com.microsoft.playwright.Locator;
import eu.rarogsoftware.rarog.platform.page.objects.LanguageAwarePageObject;

public class BasicAccountSettingsPage extends LanguageAwarePageObject {
    private static final String OLD_PASSWORD_ID = "#oldPassword";
    private static final String ERROR_MESSAGE_ID = "#changePasswordErrorMessage";
    private Locator oldPasswordInput;
    private Locator newPasswordInput;
    private Locator repeatPasswordInput;
    private Locator errorMessage;
    private Locator sendButton;

    public String changePassword(String oldPassword, String newPassword, String repeatPassword) {
        oldPasswordInput.fill(oldPassword);
        newPasswordInput.fill(newPassword);
        repeatPasswordInput.fill(repeatPassword);
        sendButton.click();
        getPage().waitForSelector(ERROR_MESSAGE_ID);
        return getErrorMessage();
    }

    public String getErrorMessage() {
        return errorMessage.textContent();
    }

    @Override
    public void bind() {
        var page = getPage();
        oldPasswordInput = page.locator(OLD_PASSWORD_ID);
        newPasswordInput = page.locator("#newPassword");
        repeatPasswordInput = page.locator("#repeatPassword");
        sendButton = page.locator("#changePasswordButton");
        errorMessage = page.locator(ERROR_MESSAGE_ID);
        page.waitForSelector(OLD_PASSWORD_ID);
    }

    @Override
    public String getTargetPageUrl() {
        return wrapUrlWithLanguageParameter("/account/my");
    }

    @Override
    public boolean isAt() {
        return getPage().isVisible(OLD_PASSWORD_ID);
    }
}
