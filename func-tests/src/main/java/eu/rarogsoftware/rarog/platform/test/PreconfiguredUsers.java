package eu.rarogsoftware.rarog.platform.test;

public final class PreconfiguredUsers {
    public static final String ADMIN_USERNAME = "admin";
    public static final String ADMIN_PASSWORD = "admin";
    public static final String MFA_ADMIN_USERNAME = "mfaadmin";
    public static final String MFA_ADMIN_PASSWORD = "mfaadmin";
    public static final String USER_USERNAME = "user1";
    public static final Long USER_ID = 3L;
    public static final String USER_PASSWORD = "user1";

    private PreconfiguredUsers() {
    }
}
