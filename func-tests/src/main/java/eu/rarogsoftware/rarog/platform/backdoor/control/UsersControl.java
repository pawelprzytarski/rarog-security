package eu.rarogsoftware.rarog.platform.backdoor.control;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.api.user.management.UserRole;
import org.apache.http.client.methods.CloseableHttpResponse;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class UsersControl extends BaseControl {
    protected UsersControl(EnvironmentData environmentData) {
        super(environmentData);
    }


    public CloseableHttpResponse createUser(String username, String password, List<UserRole> roles) throws IOException {
        return postEntity(usersPath().path("create"), Map.of(
                "username", username,
                "password", password,
                "roles", roles
        ));
    }

    private PathFragments usersPath() {
        return backdoorPath().path("users");
    }

    public void removeUser(String name) throws IOException {
        sendDelete(usersPath().path(name)).close();
    }

    public record SettingBean(String name, Object value) {
    }
}
