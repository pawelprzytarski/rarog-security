package eu.rarogsoftware.rarog.platform.page.objects;

import com.microsoft.playwright.Locator;
import eu.rarogsoftware.commons.test.webdriver.playwright.PlaywrightPageObject;

public class JsonPage extends PlaywrightPageObject {
    private Locator jsonPre;

    public String getJsonValue() {
        return jsonPre.textContent();
    }

    @Override
    public void bind() {
        jsonPre = getPage().locator("pre");
    }

    @Override
    public String getTargetPageUrl() {
        throw new IllegalStateException("Cannot navigate to technical page");
    }

    @Override
    public boolean isAt() {
        return jsonPre.isVisible();
    }
}
