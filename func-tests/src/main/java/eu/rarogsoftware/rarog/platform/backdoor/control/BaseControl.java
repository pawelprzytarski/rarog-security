package eu.rarogsoftware.rarog.platform.backdoor.control;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.test.PreconfiguredUsers;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.springframework.http.MediaType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Locale;

public abstract class BaseControl {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final int TIMEOUT = 60000;
    private final CloseableHttpClient httpClient;
    private final EnvironmentData environmentData;

    protected BaseControl(EnvironmentData environmentData) {
        this.environmentData = environmentData;
        httpClient = HttpClientBuilder.create()
                .disableAutomaticRetries()
                .setDefaultRequestConfig(RequestConfig.copy(RequestConfig.DEFAULT)
                        .setConnectionRequestTimeout(TIMEOUT)
                        .setConnectTimeout(TIMEOUT)
                        .setSocketTimeout(TIMEOUT)
                        .build())
                .disableRedirectHandling()
                .disableAuthCaching()
                .disableCookieManagement()
                .build();
    }

    private static <T> T parseResponseEntity(Class<T> entityType, CloseableHttpResponse response) throws IOException {
        try {
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new HttpResponseException(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
            }
            if (!response.getEntity().getContentType().getValue().toLowerCase(Locale.ROOT).equals(MediaType.APPLICATION_JSON_VALUE)) {
                throw new IllegalArgumentException("Unsupported response type of request");
            }
            try (var os = new ByteArrayOutputStream()) {
                response.getEntity().writeTo(os);
                var contentEncoding = response.getEntity().getContentEncoding();
                var encoding = contentEncoding != null ? contentEncoding.getValue() : StandardCharsets.UTF_8.name();
                var responseJson = os.toString(encoding);
                return MAPPER.readValue(responseJson, entityType);
            }
        } finally {
            response.close();
        }
    }

    private static HttpEntity createHttpEntity(Object entity) throws JsonProcessingException {
        if (entity instanceof File file) {
            return createFileEntity(file);
        } else {
            return createJsonEntity(entity);
        }
    }

    private static FileEntity createFileEntity(File file) {
        return new FileEntity(file);
    }

    private static BasicHttpEntity createJsonEntity(Object entity) throws JsonProcessingException {
        var httpEntity = new BasicHttpEntity();
        httpEntity.setContentType(MediaType.APPLICATION_JSON_VALUE);
        var json = MAPPER.writeValueAsString(entity);
        httpEntity.setContent(new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)));
        return httpEntity;
    }

    protected CloseableHttpResponse sendDelete(PathFragments path, Header... headers) throws IOException {
        return sendDelete(path.toString(), headers);
    }

    protected CloseableHttpResponse sendDelete(String url, Header... headers) throws IOException {
        var httpDelete = new HttpDelete(url);
        return executeRequest(httpDelete, headers);
    }

    protected CloseableHttpResponse getResponse(String url, Header... headers) throws IOException {
        var httpGet = new HttpGet(url);
        return executeRequest(httpGet, headers);
    }

    private CloseableHttpResponse executeRequest(HttpUriRequest httpRequest, Header[] headers) throws IOException {
        for (var header : headers) {
            httpRequest.addHeader(header);
        }
        var response = httpClient.execute(httpRequest);
        if (response.getStatusLine().getStatusCode() >= 500) {
            throw new InternalServerException(response.getStatusLine().getStatusCode(), response);
        }
        return response;
    }

    protected <T> T getEntity(PathFragments url, Class<T> entityType, Header... headers) throws IOException {
        return getEntity(url.toString(), entityType, headers);
    }

    protected <T> T getEntity(String url, Class<T> entityType, Header... headers) throws IOException {
        Header[] adjustedHeaders = ArrayUtils.add(headers, new BasicHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE));
        var response = getResponse(url, adjustedHeaders);
        return parseResponseEntity(entityType, response);
    }

    protected <T> T postEntityReturningEntity(PathFragments path, Object entity, Class<T> entityTYpe, Header... headers) throws IOException {
        var response = postEntity(path.toString(), entity, headers);
        return parseResponseEntity(entityTYpe, response);
    }

    protected <T> T postEntityReturningEntity(String path, Object entity, Class<T> entityTYpe, Header... headers) throws IOException {
        var response = postEntity(path, entity, headers);
        return parseResponseEntity(entityTYpe, response);
    }

    protected CloseableHttpResponse postEntity(PathFragments path, Object entity, Header... headers) throws IOException {
        return postEntity(path.toString(), entity, headers);
    }

    private CloseableHttpResponse postEntity(String url, Object entity, Header[] headers) throws IOException {
        var httpPost = new HttpPost(url);
        if (entity != null) {
            var httpEntity = createHttpEntity(entity);
            httpPost.setEntity(httpEntity);
        }
        return executeRequest(httpPost, headers);
    }

    protected <T> T putEntityReturningEntity(PathFragments path, Object entity, Class<T> entityTYpe, Header... headers) throws IOException {
        var response = postEntity(path.toString(), entity, headers);
        return parseResponseEntity(entityTYpe, response);
    }

    protected <T> T putEntityReturningEntity(String path, Object entity, Class<T> entityTYpe, Header... headers) throws IOException {
        var response = postEntity(path, entity, headers);
        return parseResponseEntity(entityTYpe, response);
    }

    protected CloseableHttpResponse putEntity(PathFragments path, Object entity, Header... headers) throws IOException {
        return putEntity(path.toString(), entity, headers);
    }

    private CloseableHttpResponse putEntity(String url, Object entity, Header[] headers) throws IOException {
        var httpPut = new HttpPut(url);
        var httpEntity = createHttpEntity(entity);
        httpPut.setEntity(httpEntity);
        return executeRequest(httpPut, headers);
    }

    protected PathFragments basePath() {
        return new PathFragments(StringUtils.stripEnd(environmentData.baseUrl(), "/"));
    }

    protected PathFragments backdoorPath() {
        return basePath().path("rest/backdoor/1.0");
    }

    protected Header disableCsrfProtectionHeader() {
        return new BasicHeader("X-CSRF-DISABLE", "true");
    }

    protected Header adminAuthHeader() {
        String authPhrase = PreconfiguredUsers.ADMIN_USERNAME + ":" + PreconfiguredUsers.ADMIN_PASSWORD;
        return new BasicHeader(HttpHeaders.AUTHORIZATION, "Basic " + Base64.getEncoder().encodeToString(authPhrase.getBytes(StandardCharsets.UTF_8)));
    }
}
