package eu.rarogsoftware.rarog.platform.page.objects.account.settings;

import com.microsoft.playwright.Locator;
import eu.rarogsoftware.rarog.platform.page.objects.LanguageAwarePageObject;

import java.util.List;

public class CreateUserTokenPageUser extends LanguageAwarePageObject {
    private static final String PANEL_ID = "#accesstokenstab-tabpane-create";
    private Locator listTabButton;
    private Locator tokenNameField;
    private Locator expirationField;
    private Locator createButton;
    private Locator createdTokenField;

    public UserAccessTokensListPage clickListButtonNav() {
        listTabButton.click();
        return getTester().bindPage(UserAccessTokensListPage.class);
    }

    public String createToken(String name, String expiration, List<String> authorities) {
        tokenNameField.fill(name);
        expirationField.fill(expiration);
        authorities.forEach(auth -> getPage().locator("#authority_" + auth).check());
        createButton.click();
        getPage().waitForSelector("#createdTokenValue");
        return createdTokenField.inputValue();
    }

    @Override
    public void bind() {
        var page = getPage();
        listTabButton = page.locator("#accesstokenstab-tab-list");
        tokenNameField = page.locator("#name");
        expirationField = page.locator("#expiration");
        createButton = page.locator("#createTokenButton");
        createdTokenField = page.locator("#createdTokenValue");
        page.waitForSelector(PANEL_ID);
    }

    @Override
    public String getTargetPageUrl() {
        return "/account/my/accesstokens#create";
    }

    @Override
    public boolean isAt() {
        return getPage().isVisible(PANEL_ID);
    }
}
