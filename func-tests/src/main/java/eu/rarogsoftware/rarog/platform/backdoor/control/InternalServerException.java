package eu.rarogsoftware.rarog.platform.backdoor.control;

import org.apache.http.HttpResponse;

public class InternalServerException extends RuntimeException {
    private final transient HttpResponse response;
    public InternalServerException(int statusCode, HttpResponse response) {
        super("Server return internal error response: %d".formatted(statusCode));
        this.response = response;
    }

    public HttpResponse getResponse() {
        return response;
    }
}
