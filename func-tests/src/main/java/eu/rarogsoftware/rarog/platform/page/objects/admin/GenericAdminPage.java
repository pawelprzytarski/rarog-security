package eu.rarogsoftware.rarog.platform.page.objects.admin;

public class GenericAdminPage extends AdminPage {

    @Override
    public String getTargetPageUrl() {
        return wrapUrlWithLanguageParameter("/admin/");
    }

}
