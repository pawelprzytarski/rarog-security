package eu.rarogsoftware.rarog.platform.backdoor.control;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.message.BasicHeader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

public class TomcatControl extends BaseControl {
    private static final int APP_RESTART_TIMEOUT = 120;

    private final EnvironmentData environmentData;

    protected TomcatControl(EnvironmentData environmentData) {
        super(environmentData);
        this.environmentData = environmentData;
    }

    // call http://localhost:8080/manager/text/reload?path=/ to reload the rarogitto.
    @SuppressFBWarnings(justification = "Test only class.")
    public void reloadApplication() throws IOException, InterruptedException {
        var reloadAppUrl = String.format("%s://%s:%s/manager/text/reload?path=%s",
                environmentData.protocol(),
                environmentData.host(),
                environmentData.port(),
                environmentData.contextPath());
        this.getResponse(reloadAppUrl, tomcatAuthHeader()).close();
        var stopWatch = StopWatch.createStarted();
        while (stopWatch.getTime(TimeUnit.SECONDS) < APP_RESTART_TIMEOUT) {
            try (var httpResponse = getResponse(environmentData.baseUrl() + "boot/")) {
                if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                    return;
                } else {
                    Thread.sleep(500);
                }
            }
        }
        throw new RestartTimeoutException("Rarog startup timed out, waiting longer than %d s".formatted(APP_RESTART_TIMEOUT));
    }

    protected Header tomcatAuthHeader() {
        String authPhrase = System.getProperty("tomcat.manager.user");
        return new BasicHeader(HttpHeaders.AUTHORIZATION, "Basic " + Base64.getEncoder().encodeToString(authPhrase.getBytes(StandardCharsets.UTF_8)));
    }

    public static class RestartTimeoutException extends RuntimeException {
        public RestartTimeoutException(String message) {
            super(message);
        }
    }

    record StatusBean(String serverStatus) {
    }
}
