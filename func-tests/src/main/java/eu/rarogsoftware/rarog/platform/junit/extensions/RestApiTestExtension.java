package eu.rarogsoftware.rarog.platform.junit.extensions;

import java.util.Collections;

/**
 * Container extension that contains all useful extension for writing REST API func tests.
 */
public class RestApiTestExtension extends FuncTestExtension {
    public RestApiTestExtension() {
        super(Collections.singletonList(new HttpClientExtension()));
    }
}
