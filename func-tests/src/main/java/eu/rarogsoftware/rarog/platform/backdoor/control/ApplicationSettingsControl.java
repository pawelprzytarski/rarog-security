package eu.rarogsoftware.rarog.platform.backdoor.control;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;

import java.io.IOException;

public class ApplicationSettingsControl extends BaseControl {
    public ApplicationSettingsControl(EnvironmentData environmentData) {
        super(environmentData);
    }

    public SettingBean getSetting(String name) throws IOException {
        return getEntity(settingsPath().path(name), SettingBean.class);
    }

    private PathFragments settingsPath() {
        return backdoorPath().path("application").path("settings");
    }

    public void setSetting(String name, Object value) throws IOException {
        postEntity(settingsPath(), new SettingBean(name, value)).close();
    }

    public record SettingBean(String name, Object value){}
}
