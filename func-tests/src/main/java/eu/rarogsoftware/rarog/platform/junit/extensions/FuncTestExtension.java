package eu.rarogsoftware.rarog.platform.junit.extensions;

import eu.rarogsoftware.commons.test.webdriver.junit.EnvironmentDataExtension;
import org.junit.jupiter.api.extension.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FuncTestExtension implements BeforeAllCallback,
        BeforeEachCallback,
        AfterEachCallback,
        AfterAllCallback,
        TestInstancePostProcessor,
        AfterTestExecutionCallback {
    private final List<BeforeAllCallback> beforeAllCallbacks = new ArrayList<>();
    private final List<BeforeEachCallback> beforeEachCallbacks = new ArrayList<>();
    private final List<AfterEachCallback> afterEachCallbacks = new ArrayList<>();
    private final List<AfterAllCallback> afterAllCallbacks = new ArrayList<>();
    private final List<TestInstancePostProcessor> testInstancePostProcessors = new ArrayList<>();
    private final List<AfterTestExecutionCallback> afterTestExecutionCallbacks = new ArrayList<>();

    public FuncTestExtension() {
        this(Collections.emptyList());
    }
    public FuncTestExtension(List<Object> extensions) {
        addExtension(new EnvironmentDataExtension());
        addExtension(new AppSetupExtension());
        addExtension(new BackdoorControlExtension());
        addExtension(new TestMetricExtension());
        addExtension(new TestWatcherLoggerExtension());
        extensions.forEach(this::addExtension);
    }

    private void addExtension(Object extension) {
        if (extension instanceof BeforeAllCallback specificExtension) {
            beforeAllCallbacks.add(specificExtension);
        }
        if (extension instanceof TestInstancePostProcessor specificExtension) {
            testInstancePostProcessors.add(specificExtension);
        }
        if (extension instanceof BeforeEachCallback specificExtension) {
            beforeEachCallbacks.add(specificExtension);
        }
        if (extension instanceof AfterTestExecutionCallback specificExtension) {
            afterTestExecutionCallbacks.add(specificExtension);
        }
        if (extension instanceof AfterEachCallback specificExtension) {
            afterEachCallbacks.add(specificExtension);
        }
        if (extension instanceof AfterAllCallback specificExtension) {
            afterAllCallbacks.add(specificExtension);
        }
    }

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        for (var callback : beforeAllCallbacks) {
            callback.beforeAll(context);
        }
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        for (var callback : beforeEachCallbacks) {
            callback.beforeEach(context);
        }
    }

    @Override
    public void afterAll(ExtensionContext context) throws Exception {
        for (var callback : afterAllCallbacks) {
            callback.afterAll(context);
        }
    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        for (var callback : afterEachCallbacks) {
            callback.afterEach(context);
        }
    }

    @Override
    public void postProcessTestInstance(Object testInstance, ExtensionContext context) throws Exception {
        for (var callback : testInstancePostProcessors) {
            callback.postProcessTestInstance(testInstance, context);
        }
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        for (var callback : afterTestExecutionCallbacks) {
            callback.afterTestExecution(context);
        }
    }
}
