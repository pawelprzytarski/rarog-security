package eu.rarogsoftware.rarog.platform.page.objects.login;

import com.microsoft.playwright.Locator;
import eu.rarogsoftware.commons.test.webdriver.PageObject;
import eu.rarogsoftware.rarog.platform.page.objects.LanguageAwarePageObject;

public class LoginPage extends LanguageAwarePageObject {
    private Locator loginInput;
    private Locator passwordInput;
    private Locator sendButton;

    public void login(String login, String password) {
        login(login, password, true);
    }

    public void login(String login, String password, boolean navigate) {
        loginInput.fill(login);
        passwordInput.fill(password);
        if (navigate) {
            getPage().waitForNavigation(() -> sendButton.click());
        } else {
            sendButton.click();
        }
    }

    public <U extends PageObject> U loginAndBind(String login, String password, Class<U> bindPage) {
        login(login, password);
        return getTester().bindPage(bindPage);
    }

    public String getErrorMessage() {
        return getPage().locator("#loginErrorMessage").textContent();
    }

    @Override
    public void bind() {
        var page = getPage();
        loginInput = page.locator("#login");
        passwordInput = page.locator("#password");
        sendButton = page.locator("#submit");
        page.waitForSelector("#login");
    }

    @Override
    public String getTargetPageUrl() {
        return wrapUrlWithLanguageParameter("/login/");
    }

    @Override
    public boolean isAt() {
        return getPage().isVisible("#login");
    }
}
