package eu.rarogsoftware.rarog.platform.test;

public final class ApplicationSettingKeys {
    public static final String SYSTEM_ID_KEY = "rarog.system.id";
    public static final String METRICS_EXPORT_PERIOD_KEY = "rarog.metrics.export.period";
    public static final String METRICS_SNAPSHOT_PERIOD_KEY = "rarog.metrics.snapshot.period";
    public static final String METRICS_SNAPSHOT_ENABLED_KEY = "rarog.metrics.snapshot.enabled";
    public static final String METRICS_GRAPHITE_SERVER_KEY = "rarog.metrics.graphite.servers";
    public static final String METRICS_GRAPHITE_EXPORT_ENABLED_KEY = "rarog.metrics.graphite.export.enabled";
    public static final String METRICS_ENABLED_KEY = "rarog.metrics.enabled";
    public static final String METRICS_INFLUX_SERVER_KEY = "rarog.metrics.influx.servers";
    public static final String METRICS_INFLUX_EXPORT_ENABLED_KEY = "rarog.metrics.influx.export.enabled";
    public static final String METRICS_TELEMETRY_EXPORT_ENABLED_KEY = "rarog.metrics.rarog.telemetry.enabled";
    public static final String OPEN_METRIC_ENDPOINT_ENABLED_KEY = "rarog.metrics.open.metric.endpoint.enabled";
    public static final String OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY = "rarog.metrics.open.metric.auth.token";
    public static final String FORCE_SYSTEM_API = "rarog.prevent.system.api.override";
    public static final String PAT_USER_KEYS_ENABLED_KEY = "rarog.pat.user.keys.enabled";
    public static final String PAT_APP_KEYS_ENABLED_KEY = "rarog.pat.app.keys.enabled";

    private ApplicationSettingKeys() {
    }
}
