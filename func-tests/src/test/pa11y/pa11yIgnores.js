export default function pa11yIgnores(){
    return [
        "color-contrast",
        "WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail"
    ]
}