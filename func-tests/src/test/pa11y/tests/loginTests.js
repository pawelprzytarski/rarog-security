import {pa11yTest} from '../Pa11yTester.js';
import {Page} from 'puppeteer';

pa11yTest({
    name: 'Login page',
    url: '{baseUrl}/server/login/',
    actions: [
        'wait for element #login to be visible'
    ]
});

pa11yTest({
    name: 'Test MFA page',
    url: '{baseUrl}/server/login/',
    /**
     *
     * @param {Page} page
     * @param {Object} params
     */
    setup: async function (page, params) {
        await page.waitForSelector('#login');
        await page.type('#login', 'mfaadmin');
        await page.type('#password', 'mfaadmin');
        await page.click('#submit');
        await page.waitForSelector('ul.methodsList');
    }
});

pa11yTest({
    name: 'Change password page',
    url: '{baseUrl}/server/login/',
    setup: async function (page, params) {
        await page.waitForSelector('#login');
        await page.type('#login', 'admin');
        await page.type('#password', 'admin');
        await page.click('#submit');
        await page.waitForSelector('nav.nav');
    }
});
