import pa11y from "pa11y";
import puppeteer from "puppeteer";
import assert from "assert";
import pa11yIgnores from "./pa11yIgnores.js";

class Pa11yTester {
    constructor() {
        this.tests = [];
        this.baseUrl = undefined;
        this.browserLogs = false
    }

    /**
     * Run all registered tests
     * @returns {Promise<Object>} results of Pa11y
     */
    async runTests() {
        pa11y.defaults.runners = ["axe", "htmlcs"];
        pa11y.defaults.ignore = pa11yIgnores();
        if (this.browserLogs) {
            pa11y.defaults.log = {
                debug: console.log,
                info: console.info,
                error: console.error
            }
        }

        const browser = await puppeteer.launch({
            args: ["--no-sandbox"]
        });
        const testResults = [];
        let errorCount = 0;

        for (const test of this.tests) {
            let page = null;
            let context = null;
            try {
                context = await browser.createIncognitoBrowserContext()
                page = await context.newPage();
                const results = await this.runTest(page, context, test);
                testResults.push(results)
                errorCount += results.issues.length ?? 0;
            } catch (error) {
                console.error(error);
                testResults.push({message: "Failed to run tests due to error", error: error.message});
                errorCount++;
            } finally {
                if (page !== null && !page.isClosed()) {
                    await page.close();
                }
                await context?.close();
                context = null;
                page = null;
            }
        }
        await browser.close();
        return [errorCount, testResults];
    }

    async runTest(page, browser, test) {
        const url = test?.url.replace("{baseUrl}", this.baseUrl)
        if (test.headers) {
            await page.setExtraHTTPHeaders({headers: test.headers})
        }
        console.info("Starting test for: " + (test.name ?? url))
        if (url) {
            // because pa11y don't work well with react (or at least our app) :shrug:
            await page.goto(url, {
                waitUntil: "domcontentloaded"
            });
        }
        if (test.setup) {
            await test.setup(page, {baseUrl: this.baseUrl})
        }
        console.info("Running test for: " + (test.name ?? await page.url()))
        const results = await pa11y(url, {
            ...test,
            ignoreUrl: true,
            browser: browser,
            page: page
        });
        if (test.name) {
            results.name = test.name;
        }
        console.info("Issues detected: " + results.issues.length)
        return results;
    }

    /**
     * Registers pa11y tests to run
     */
    test(testOptions) {
        assert(typeof testOptions === 'object'
            && (typeof testOptions.url || testOptions.setup),
            "options must be object with url or setup function");
        assert(testOptions.url === undefined || (typeof testOptions.url === 'string' || testOptions.url instanceof String),
            "options.url must be function")
        assert(testOptions.setup === undefined || (typeof testOptions.setup instanceof Function || typeof testOptions.setup === 'function'),
            "options.setup must be function")
        this.tests.push(testOptions);
    }
}

export const pa11yTester = new Pa11yTester();

/**
 * Registers pa11y tests to run
 * @param testOptions option for running pa11y tests + url/setup function
 */
export function pa11yTest(testOptions) {
    pa11yTester.test(testOptions)
}