import fetch from "node-fetch";
import {pa11yTester} from "./Pa11yTester.js";
import * as fs from "fs";
import * as path from "path";
import {fileURLToPath} from "url";

const baseUrl = "http://localhost:8080/server";
const TIMEOUT = 10;
const REPORT_DIRECTORY = "target/reports/accessibility/";

const sleep = function (ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

async function* getFiles(dir) {
    const children = await fs.promises.readdir(dir, {withFileTypes: true});
    for (const child of children) {
        const res = path.resolve(dir, child.name);
        if (child.isDirectory()) {
            yield* getFiles(res);
        } else {
            yield res;
        }
    }
}

async function runPa11yTests() {
    const testsPath = path.join(__dirname, "tests");
    for await (const path of getFiles(testsPath)) {
        if (path.endsWith("Test.js") || path.endsWith("Tests.js")) {
            import(path);
        }
    }

    pa11yTester.baseUrl = baseUrl;
    const [errorCount, results] = await pa11yTester.runTests();
    await fs.promises.mkdir(REPORT_DIRECTORY, {recursive: true})
    const fullReport = JSON.stringify(results, null, 2);
    await fs.promises.writeFile(REPORT_DIRECTORY + "accessibility-full.json", fullReport, {});
    const shortReport = JSON.stringify(results.filter(value => value.issues).flatMap(value => value.issues));
    await fs.promises.writeFile(REPORT_DIRECTORY + "accessibility.json", shortReport, {});
    if (errorCount > 0) {
        console.error("Failing build due to detected issues: " + errorCount)
        await fs.promises.writeFile(REPORT_DIRECTORY + "fail", "" + errorCount);
    } else {
        console.info("No issues detected")
    }
}

async function waitForApplicationToStart() {
    let counter = 0;
    while (counter < TIMEOUT * 2) {
        const response = await fetch(baseUrl + "/boot/", {
            redirect: "manual"
        });
        if (response.status === 302) {
            console.info("Detected app started at " + baseUrl)
            return;
        }
        await sleep(500);
        counter++;
    }
    throw new Error("Application failed to start at address: " + baseUrl + " for " + TIMEOUT + " seconds");
}

await waitForApplicationToStart();
await runPa11yTests();