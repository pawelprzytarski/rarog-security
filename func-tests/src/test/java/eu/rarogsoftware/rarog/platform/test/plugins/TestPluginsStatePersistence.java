package eu.rarogsoftware.rarog.platform.test.plugins;

import eu.rarogsoftware.rarog.platform.backdoor.control.BackdoorControl;
import eu.rarogsoftware.rarog.platform.backdoor.control.PluginsControl;
import eu.rarogsoftware.rarog.platform.junit.extensions.RestApiTestExtension;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.awaitility.Awaitility.await;

@ExtendWith(RestApiTestExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Disabled
class TestPluginsStatePersistence {
    private static final String TEST_PLUGIN_KEY = "eu.rarogsoftware.rarog.platform.test.plugins:test-plugin";

    private static String testPluginPath;

    @Inject
    BackdoorControl backdoorControl;

    @BeforeAll
    static void setUp() {
        testPluginPath = System.getProperty("test.plugin.location");
        assert !StringUtils.isEmpty(testPluginPath);
    }

    @Test
    @Order(1)
    void testInstalledPluginIsPreservedAfterRestart() throws Exception {
        backdoorControl.pluginsControl().installPlugin(testPluginPath, true).close();

        backdoorControl.tomcatControl().reloadApplication();


        await().atMost(60, TimeUnit.SECONDS).untilAsserted(() -> {
            var installedPlugins = backdoorControl.pluginsControl().getInstalledPlugins();
            assertThat(installedPlugins)
                    .extracting(PluginsControl.PluginRecord::key)
                    .contains(TEST_PLUGIN_KEY);
            var installedPlugin = Arrays.stream(installedPlugins).filter(plugins -> plugins.key().equals(TEST_PLUGIN_KEY)).findFirst();
            assertThat(installedPlugin).isPresent()
                    .hasValueSatisfying(plugin -> assertThat(plugin.state()).isEqualTo("ACTIVE"));
        });
    }

    @Test
    @Order(2)
    void testDisabledPluginIsPreservedAfterRestart() throws Exception {
        backdoorControl.pluginsControl().disablePlugin(TEST_PLUGIN_KEY).close();

        backdoorControl.tomcatControl().reloadApplication();


        await().atMost(60, TimeUnit.SECONDS).untilAsserted(() -> {
            var installedPlugins = backdoorControl.pluginsControl().getInstalledPlugins();
            assertThat(installedPlugins)
                    .extracting(PluginsControl.PluginRecord::key)
                    .contains(TEST_PLUGIN_KEY);

            var installedPlugin = Arrays.stream(installedPlugins).filter(plugins -> plugins.key().equals(TEST_PLUGIN_KEY)).findFirst();

            assertThat(installedPlugin).isPresent()
                    .hasValueSatisfying(plugin -> assertThat(plugin.state()).isEqualTo("INSTALLED"));
        });
    }

    @Test
    @Order(3)
    void testUninstalledPluginIsPreservedAfterRestart() throws Exception {
        backdoorControl.pluginsControl().uninstallPlugin(TEST_PLUGIN_KEY).close();

        backdoorControl.tomcatControl().reloadApplication();

        var installedPlugins = backdoorControl.pluginsControl().getInstalledPlugins();
        assertThat(installedPlugins).isNotEmpty();
        assertThat(installedPlugins)
                .extracting(PluginsControl.PluginRecord::key)
                .doesNotContain(TEST_PLUGIN_KEY);
    }
}
