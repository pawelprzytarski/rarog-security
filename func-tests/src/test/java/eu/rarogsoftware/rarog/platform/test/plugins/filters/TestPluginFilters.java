package eu.rarogsoftware.rarog.platform.test.plugins.filters;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.junit.extensions.RestApiTestExtension;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.HttpStatus;

import javax.inject.Inject;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(RestApiTestExtension.class)
class TestPluginFilters {
    @Inject
    EnvironmentData environmentData;
    @Inject
    CloseableHttpClient httpClient;

    @Test
    void testSingleFilterIsInvoked() throws IOException {
        var httpGet = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/");

        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getFirstHeader("FUNC_TEST_FILTERED").getValue()).isEqualTo("DARK_GODS_APPROVE");
        assertThat(response.getFirstHeader("CHAIN_BREAKER_FUNC_TEST_FILTERED")).isNull();
    }

    @Test
    void testTwoFiltersAreInvoked() throws IOException {
        var httpGet = new HttpGet(environmentData.baseUrl() + "status");

        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getFirstHeader("CHAIN_BREAKER_FUNC_TEST_FILTERED").getValue()).isEqualTo("DARK_GOD_IS_ANGRY");
        assertThat(response.getFirstHeader("FUNC_TEST_FILTERED").getValue()).isEqualTo("DARK_GODS_APPROVE");
    }

    @Test
    void testFilterBreaksTheChainAndIsATeapot() throws IOException {
        var httpGet = new HttpGet(environmentData.baseUrl() + "status");
        httpGet.addHeader("RAROG_BREAKS_THE_CHAINS", "YES");

        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.I_AM_A_TEAPOT.value());
        assertThat(response.getFirstHeader("CHAIN_BREAKER_FUNC_TEST_FILTERED").getValue()).isEqualTo("DARK_GOD_IS_ANGRY");
        assertThat(response.getFirstHeader("FUNC_TEST_FILTERED")).isNull();
    }

    @Test
    void testFilterNotMatchedControllerIsUnaffected() throws IOException {
        var httpGet = new HttpGet(environmentData.baseUrl() + "plugin/func-test/test");

        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getFirstHeader("CHAIN_BREAKER_FUNC_TEST_FILTERED")).isNull();
        assertThat(response.getFirstHeader("FUNC_TEST_FILTERED")).isNull();
    }

}
