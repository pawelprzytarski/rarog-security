package eu.rarogsoftware.rarog.platform.test.admin;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.TimeoutError;
import eu.rarogsoftware.commons.test.webdriver.playwright.WebdriverTester;
import eu.rarogsoftware.rarog.platform.junit.extensions.LoginAs;
import eu.rarogsoftware.rarog.platform.junit.extensions.WebdriverTestExtension;
import eu.rarogsoftware.rarog.platform.page.objects.admin.GenericAdminPage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;

import static eu.rarogsoftware.rarog.platform.test.PreconfiguredUsers.ADMIN_USERNAME;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(WebdriverTestExtension.class)
@LoginAs(ADMIN_USERNAME)
class AdminNavigationTest {
    @Inject
    WebdriverTester tester;

    @Test
    void elementsBecomeClickableAfterClickingCategory() {
        var adminPage = tester.goTo(GenericAdminPage.class);
        var infoLink = adminPage.getMenuElement("general/info");

        assertThat(infoLink)
                .isNotNull();
        assertNotClickable(infoLink);

        adminPage.getMenuElement("general").click();
        assertClickable(infoLink);
    }

    @Test
    void clickingNavigationLinksRedirectsToPage() {
        var adminPage = tester.goTo(GenericAdminPage.class);

        adminPage.getMenuElement("general").click();
        adminPage.getMenuElement("general/info").click();

        await().untilAsserted(() -> assertThat(adminPage.getContentText()).contains("System info"));
        assertThat(tester.getCurrentUrl()).isEqualTo("/admin/general/info");

        adminPage.getMenuElement("test").click();
        adminPage.getMenuElement("test/subcategory").click();
        adminPage.getMenuElement("test/subcategory/testItem1").click();

        await().untilAsserted(() -> assertThat(adminPage.getContentText()).isEqualTo("Content of test page"));
        assertThat(tester.getCurrentUrl()).isEqualTo("/admin/test/subcategory/testItem1");
    }

    @Test
    void pageObjectCorrectlyNavigatesUsingFullyHiddenLink() {
        var adminPage = tester.goTo(GenericAdminPage.class);

        adminPage.navigateByLinkClick("test/subcategory/testItem1");

        await().untilAsserted(() -> assertThat(adminPage.getContentText()).isEqualTo("Content of test page"));
        assertThat(tester.getCurrentUrl()).isEqualTo("/admin/test/subcategory/testItem1");
    }

    @Test
    void pageObjectCorrectlyNavigatesUsingPartiallyHiddenKey() {
        var adminPage = tester.goTo(GenericAdminPage.class);
        adminPage.getMenuElement("test").click();

        adminPage.navigateByLinkClick("test/subcategory/testItem1");

        await().untilAsserted(() -> assertThat(adminPage.getContentText()).isEqualTo("Content of test page"));
        assertThat(tester.getCurrentUrl()).isEqualTo("/admin/test/subcategory/testItem1");
    }

    @Test
    void profileAndLogoutLinkInHeaderWorkCorrectly() {
        var adminPage = tester.goTo(GenericAdminPage.class);

        adminPage.profileNav().clickProfileLink();

        await().untilAsserted(() -> assertThat(tester.getCurrentUrl()).isEqualTo("/account/my"));

        adminPage = tester.goTo(GenericAdminPage.class);

        adminPage.profileNav().clickLogoutLink();

        await().untilAsserted(() -> assertThat(tester.getCurrentUrl()).isEqualTo("/login/"));
    }

    @Test
    void testGenericComponentPageRenderCorrectly() {
        var adminPage = tester.goTo(GenericAdminPage.class);
        adminPage.navigateByLinkClick("test/types/simpleGeneric");

        await().untilAsserted(() -> assertThat(adminPage.getContentText()).isEqualTo("Content of test page"));
    }

    @Test
    void testReactComponentPageRenderCorrectly() {
        var adminPage = tester.goTo(GenericAdminPage.class);
        adminPage.navigateByLinkClick("test/types/simpleReact");

        await().untilAsserted(() -> assertThat(adminPage.getContentText()).isEqualTo("Content of test page"));
    }

    @Test
    void testReactParentComponentPageRenderCorrectly() {
        var adminPage = tester.goTo(GenericAdminPage.class);
        adminPage.navigateByLinkClick("test/types/parentReact");

        await().untilAsserted(() -> assertThat(adminPage.getContentText()).isEqualTo("Content of parent page"));
    }

    @Test
    void testReactChildComponentPageRenderCorrectlyInsideParentComponent() {
        var adminPage = tester.goTo(GenericAdminPage.class);
        adminPage.navigateByLinkClick("test/types/parentReact/childReact");

        await().untilAsserted(() -> assertThat(adminPage.getContentText()).isEqualTo("Content of parent pageContent of child page"));
    }

    @Test
    void testIframePageRenderCorrectly() {
        var adminPage = tester.goTo(GenericAdminPage.class);
        adminPage.navigateByLinkClick("test/types/iframe");

        await().untilAsserted(() -> assertThat(adminPage.getContentText()).contains("Text from model:Iframe content"));
    }

    @Test
    void testNavOnlyItemIsValidLink() {
        var adminPage = tester.goTo(GenericAdminPage.class);
        adminPage.navigateByLinkClick("test/types/link");

        await().untilAsserted(() -> assertThat(adminPage.getCurrentUrl()).isEqualTo("/account/my"));
    }

    private static void assertNotClickable(Locator element) {
        assertThrows(TimeoutError.class, () -> element.click(new Locator.ClickOptions().setTrial(true).setTimeout(1.0)));
    }

    private static void assertClickable(Locator element) {
        element.click(new Locator.ClickOptions().setTrial(true).setTimeout(5000.0));
        // just don't throw anything. every framework has its shortcomings
    }
}
