package eu.rarogsoftware.rarog.platform.test.plugins;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.backdoor.control.BackdoorControl;
import eu.rarogsoftware.rarog.platform.junit.extensions.RestApiTestExtension;
import eu.rarogsoftware.rarog.platform.test.commons.TestHelpers;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.MediaType;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(RestApiTestExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TestPluginController {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final String TEST_PLUGIN_KEY = "eu.rarogsoftware.rarog.platform.test.plugins:test-plugin";
    private static final String NOT_EXISTING_PLUGIN_KEY = "NOT_EXISTING_PLUGIN";
    static String testPluginPath;
    @Inject
    BackdoorControl backdoorControl;
    @Inject
    EnvironmentData environmentData;
    @Inject
    CloseableHttpClient httpClient;

    @BeforeAll
    static void setUp() {
        testPluginPath = System.getProperty("test.plugin.location");
        assert !StringUtils.isEmpty(testPluginPath);
    }

    @Test
    @Order(1)
    void testInstallPluginFromPath() throws IOException {
        var httpRequest = new HttpPost(environmentData.baseUrl() + "plugins");
        httpRequest.setEntity(new ByteArrayEntity(MAPPER.writeValueAsBytes(Map.of(
                "path", testPluginPath,
                "reinstallIfNeeded", true
        )), ContentType.APPLICATION_JSON));
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    @Order(2)
    void testGetPluginContainsInstalledPlugin() throws IOException {
        var httpRequest = new HttpGet(environmentData.baseUrl() + "plugins");
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        var content = new String(response.getEntity().getContent().readAllBytes(), StandardCharsets.UTF_8);
        assertThat(content, containsString(TEST_PLUGIN_KEY));
    }

    @Test
    @Order(3)
    void testGetPluginStateReturnTrueForFreshlyInstalledPlugin() throws IOException {
        var httpRequest = new HttpGet(environmentData.baseUrl() + "plugins/" + TEST_PLUGIN_KEY + "/enabled");
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        var value = MAPPER.readValue(response.getEntity().getContent(), BooleanBean.class);
        assertThat(value.value(), equalTo(true));
    }

    @Test
    @Order(3)
    void testGetPluginDataForFreshlyInstalledPlugin() throws IOException {
        var httpRequest = new HttpGet(environmentData.baseUrl() + "plugins/" + TEST_PLUGIN_KEY);
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        var record = MAPPER.readValue(response.getEntity().getContent(), PluginRecord.class);
        assertThat(record.state(), equalTo("ACTIVE"));
        assertThat(record.key(), equalTo(TEST_PLUGIN_KEY));
    }

    @Test
    @Order(4)
    void testChangePluginStateToDisabled() throws IOException {
        var httpRequest = new HttpPut(environmentData.baseUrl() + "plugins/" + TEST_PLUGIN_KEY + "/enabled");
        httpRequest.setEntity(new ByteArrayEntity(MAPPER.writeValueAsBytes(new BooleanBean(false)),
                ContentType.APPLICATION_JSON));
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
        assertThat(backdoorControl.applicationSettings().getSetting("test.plugin.setting").value().toString(), startsWith("testPluginSettingValue_disabled_")); // test that plugin deactived properly
    }

    @Test
    @Order(5)
    void testGetPluginStateReturnFalseForDisabled() throws IOException {
        var httpRequest = new HttpGet(environmentData.baseUrl() + "plugins/" + TEST_PLUGIN_KEY + "/enabled");
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        var value = MAPPER.readValue(response.getEntity().getContent(), BooleanBean.class);
        assertThat(value.value(), equalTo(false));
    }

    @Test
    @Order(5)
    void testGetPluginDataForDisabledPlugin() throws IOException {
        var httpRequest = new HttpGet(environmentData.baseUrl() + "plugins/" + TEST_PLUGIN_KEY);
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        var record = MAPPER.readValue(response.getEntity().getContent(), PluginRecord.class);
        assertThat(record.state(), equalTo("INSTALLED"));
        assertThat(record.key(), equalTo(TEST_PLUGIN_KEY));
    }

    @Test
    @Order(6)
    void testChangePluginStateToEnabled() throws IOException {
        var httpRequest = new HttpPut(environmentData.baseUrl() + "plugins/" + TEST_PLUGIN_KEY + "/enabled");
        httpRequest.setEntity(new ByteArrayEntity(MAPPER.writeValueAsBytes(new BooleanBean(true)),
                ContentType.APPLICATION_JSON));
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
        assertThat(backdoorControl.applicationSettings().getSetting("test.plugin.setting").value().toString(), startsWith("testPluginSettingValue_enabled_")); // test that plugin activated properly
    }

    @Test
    @Order(7)
    void testGetPluginStateReturnTrueForEnabledPlugin() throws IOException {
        var httpRequest = new HttpGet(environmentData.baseUrl() + "plugins/" + TEST_PLUGIN_KEY + "/enabled");
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        var value = MAPPER.readValue(response.getEntity().getContent(), BooleanBean.class);
        assertThat(value.value(), equalTo(true));
    }

    @Test
    @Order(8)
    void testUninstallPlugin() throws IOException {
        var httpRequest = new HttpDelete(environmentData.baseUrl() + "plugins/" + TEST_PLUGIN_KEY);
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
        assertThat(backdoorControl.applicationSettings().getSetting("test.plugin.setting").value().toString(), startsWith("testPluginSettingValue_disabled_")); // test that plugin deactived properly
    }

    @Test
    @Order(9)
    void testGetPluginDoNotContainsUninstalledPlugin() throws IOException {
        var httpRequest = new HttpGet(environmentData.baseUrl() + "plugins");
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        var content = new String(response.getEntity().getContent().readAllBytes(), StandardCharsets.UTF_8);
        assertThat(content, not(containsString(TEST_PLUGIN_KEY)));
    }

    @Test
    @Order(10)
    void testInstallPluginFromForm() throws IOException {
        var httpRequest = new HttpPost(environmentData.baseUrl() + "plugins");
        httpRequest.setEntity(MultipartEntityBuilder.create()
                .addTextBody("reinstallIfNeeded", "true", ContentType.create("application/x-www-form-urlencoded", StandardCharsets.UTF_8))
                .addBinaryBody("file", new File(testPluginPath))
                .build());
        TestHelpers.addAdminAuthHeader(httpRequest);
        TestHelpers.addDisableCsrfHeader(httpRequest);


        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    @Order(10)
    void testInstallPluginFromJavaArchiveStream() throws IOException {
        var httpRequest = new HttpPost(environmentData.baseUrl() + "plugins");
        httpRequest.setEntity(new FileEntity(new File(testPluginPath), "application/java-archive"));
        TestHelpers.addAdminAuthHeader(httpRequest);
        TestHelpers.addDisableCsrfHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    @Order(10)
    void testInstallPluginFromBinaryStream() throws IOException {
        var httpRequest = new HttpPost(environmentData.baseUrl() + "plugins");
        httpRequest.setEntity(new FileEntity(new File(testPluginPath), MediaType.APPLICATION_OCTET_STREAM_VALUE));
        TestHelpers.addAdminAuthHeader(httpRequest);
        TestHelpers.addDisableCsrfHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    @Order(11)
    void testPurgePlugin() throws IOException {
        assertThat(backdoorControl.applicationSettings().getSetting("test.plugin.setting").value().toString(), startsWith("testPluginSettingValue_"));

        var httpRequest = new HttpPost(environmentData.baseUrl() + "plugins/" + TEST_PLUGIN_KEY + "/purge");
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
        assertThat(backdoorControl.applicationSettings().getSetting("test.plugin.setting").value(), equalTo("purged"));
    }

    @Test
    @Order(12)
    void testUninstallPurgedPlugin() throws IOException {
        var httpRequest = new HttpDelete(environmentData.baseUrl() + "plugins/" + TEST_PLUGIN_KEY);
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NO_CONTENT));
    }

    @Test
    @Order(13)
    void testGetPluginDoNotContainsPurgedUninstalledPlugin() throws IOException {
        var httpRequest = new HttpGet(environmentData.baseUrl() + "plugins");
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        var content = new String(response.getEntity().getContent().readAllBytes(), StandardCharsets.UTF_8);
        assertThat(content, not(containsString(TEST_PLUGIN_KEY)));
    }

    @Test
    @Order(100)
    void testChangePluginStateReturnsNotFoundForNotExistingPlugin() throws IOException {
        var httpRequest = new HttpPut(environmentData.baseUrl() + "plugins/" + NOT_EXISTING_PLUGIN_KEY + "/enabled");
        httpRequest.setEntity(new ByteArrayEntity(MAPPER.writeValueAsBytes(new BooleanBean(true)),
                ContentType.APPLICATION_JSON));
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NOT_FOUND));
    }

    @Test
    @Order(100)
    void testGetPluginStateReturnsNotFoundForNotExistingPlugin() throws IOException {
        var httpRequest = new HttpGet(environmentData.baseUrl() + "plugins/" + NOT_EXISTING_PLUGIN_KEY + "/enabled");
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NOT_FOUND));
    }

    @Test
    @Order(100)
    void testUninstallPluginReturnsNotFoundForNotExistingPlugin() throws IOException {
        var httpRequest = new HttpDelete(environmentData.baseUrl() + "plugins/" + NOT_EXISTING_PLUGIN_KEY);
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NOT_FOUND));
    }

    @Test
    @Order(100)
    void testPurgePluginReturnsNotFoundForNotExistingPlugin() throws IOException {
        var httpRequest = new HttpPost(environmentData.baseUrl() + "plugins/" + NOT_EXISTING_PLUGIN_KEY + "/purge");
        TestHelpers.addAdminAuthHeader(httpRequest);

        var response = httpClient.execute(httpRequest);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NOT_FOUND));
    }

    record BooleanBean(Boolean value) {
    }

    public record PluginManifest(String key, String name, String version, String description, String image) {
    }

    public record PluginRecord(String key, PluginManifest manifest, String state) {
    }
}
