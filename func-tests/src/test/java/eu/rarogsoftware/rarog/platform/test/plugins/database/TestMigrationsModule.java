package eu.rarogsoftware.rarog.platform.test.plugins.database;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.backdoor.control.BackdoorControl;
import eu.rarogsoftware.rarog.platform.junit.extensions.RestApiTestExtension;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.io.IOException;

import static eu.rarogsoftware.rarog.platform.test.FuncTestConstants.FUNC_TEST_PLUGIN_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(RestApiTestExtension.class)
public class TestMigrationsModule {
    private final ObjectMapper objectMapper = new ObjectMapper();
    @Inject
    BackdoorControl backdoorControl;
    @Inject
    CloseableHttpClient httpClient;
    @Inject
    EnvironmentData environmentData;

    @Test
    void testMigrationCreatedTableForPlugin() throws IOException {
        var getTestResult = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/database");
        var testResponse = httpClient.execute(getTestResult);

        assertThat(testResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        var resultBean = objectMapper.readValue(testResponse.getEntity().getContent(), TestResultBean.class);

        assertThat(resultBean.insertedValue, not(emptyOrNullString()));
        assertThat(resultBean.insertedValue, equalTo(resultBean.readValue));
    }

    @Test
    void testTableCanHandleAutoIncrement() throws IOException {
        var getTestResult = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/database");
        httpClient.execute(getTestResult); // first try
        var testResponse = httpClient.execute(getTestResult); //second try

        var resultBean = objectMapper.readValue(testResponse.getEntity().getContent(), TestResultBean.class);
        assertThat(resultBean.insertedValue, not(emptyOrNullString()));
        assertThat(resultBean.insertedValue, equalTo(resultBean.readValue));

        var getCountResult = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/database/count");
        var countResponse = httpClient.execute(getCountResult);

        var countBean = objectMapper.readValue(countResponse.getEntity().getContent(), TestTableRowCount.class);
        assertThat(countBean.count, greaterThanOrEqualTo(2L));
    }

    @Test
    void testLiquibaseChangesStayForPluginDisabled() throws IOException {
        var getTestResult = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/database");
        assertSuccess(httpClient.execute(getTestResult)); // first try
        assertSuccess(httpClient.execute(getTestResult)); // second try

        assertSuccess(backdoorControl.pluginsControl().disablePlugin(FUNC_TEST_PLUGIN_KEY));
        assertSuccess(backdoorControl.pluginsControl().enablePlugin(FUNC_TEST_PLUGIN_KEY));

        var getCountResult = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/database/count");
        var countResponse = httpClient.execute(getCountResult);

        var countBean = objectMapper.readValue(countResponse.getEntity().getContent(), TestTableRowCount.class);
        assertThat(countBean.count, greaterThanOrEqualTo(2L));
    }

    @Test
    void testLiquibasePurgeRemovesData() throws IOException {
        var getTestResult = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/database");
        assertSuccess(httpClient.execute(getTestResult)); // first try
        assertSuccess(httpClient.execute(getTestResult)); //second try

        assertSuccess(backdoorControl.pluginsControl().purgePlugin(FUNC_TEST_PLUGIN_KEY));
        assertSuccess(backdoorControl.pluginsControl().enablePlugin(FUNC_TEST_PLUGIN_KEY));

        var getCountResult = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/database/count");
        var countResponse = httpClient.execute(getCountResult);

        var countBean = objectMapper.readValue(countResponse.getEntity().getContent(), TestTableRowCount.class);
        assertThat(countBean.count, equalTo(0L));
    }

    private void assertSuccess(CloseableHttpResponse backdoorControl) throws IOException {
        try (var response = backdoorControl) {
            assertThat(response.getStatusLine().getStatusCode(), allOf(greaterThanOrEqualTo(200), lessThan(300)));
        }
    }

    record TestResultBean(String insertedValue, String readValue, String key) {
    }

    record TestTableRowCount(long count) {
    }
}
