package eu.rarogsoftware.rarog.platform.test.plugins.events;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.junit.extensions.RestApiTestExtension;
import eu.rarogsoftware.rarog.platform.test.commons.TestHelpers;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.inject.Inject;
import java.io.IOException;
import java.time.Duration;
import java.util.stream.Stream;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(RestApiTestExtension.class)
class TestEventsInPlugins {
    private static final String SYNC = "sync";
    private static final String ASYNC = "async";
    private static final String PARALLEL_ASYNC = "parallel_async";
    private static final String TEST_EVENT = "test";
    private static final String STRING_EVENT = "string";
    private static final String OBJECT_EVENT = "object";

    @Inject
    EnvironmentData environmentData;
    @Inject
    CloseableHttpClient httpClient;


    @BeforeEach
    void setup() throws IOException {
        HttpPost httpPost = new HttpPost(environmentData.baseUrl() + "rest/func-test/1.0/test/event/reset");
        TestHelpers.addDisableCsrfHeader(httpPost);
        httpClient.execute(httpPost).close();
    }

    @ParameterizedTest
    @MethodSource("testEventsParameters")
    void testEventsAreDelivered(String eventType, String eventClass, int expectedTestEvents, int expectedStringEvents, int expectedObjectEvents) throws IOException {

        var response = sendEvent(eventClass, eventType);

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        response.close();

        await()
            .atMost(Duration.ofSeconds(30))
            .pollDelay(Duration.ofMillis(100))
            .pollInterval(Duration.ofMillis(1000))
            .untilAsserted(() -> {
                assertEquals(expectedTestEvents, getEventCount(TEST_EVENT));
                assertEquals(expectedStringEvents, getEventCount(STRING_EVENT));
                assertEquals(expectedObjectEvents, getEventCount(OBJECT_EVENT));
            });
    }

    private static Stream<Arguments> testEventsParameters() {
        return Stream.of(
            Arguments.of(SYNC, TEST_EVENT, 1, 0, 1),
            Arguments.of(SYNC, STRING_EVENT, 0, 1, 1),
            Arguments.of(SYNC, OBJECT_EVENT, 0, 0, 1),
            Arguments.of(ASYNC, TEST_EVENT, 1, 0, 1),
            Arguments.of(ASYNC, STRING_EVENT, 0, 1, 1),
            Arguments.of(ASYNC, OBJECT_EVENT, 0, 0, 1),
            Arguments.of(PARALLEL_ASYNC, TEST_EVENT, 1, 0, 1),
            Arguments.of(PARALLEL_ASYNC, STRING_EVENT, 0, 1, 1),
            Arguments.of(PARALLEL_ASYNC, OBJECT_EVENT, 0, 0, 1)
        );
    }

    private CloseableHttpResponse sendEvent(String eventClass, String eventType) throws IOException {
        HttpPost httpPost = new HttpPost(environmentData.baseUrl() + "rest/func-test/1.0/test/event/send/%s/%s".formatted(eventClass, eventType));
        TestHelpers.addDisableCsrfHeader(httpPost);

        return httpClient.execute(httpPost);
    }

    private int getEventCount(String eventClass) throws IOException {
        HttpGet httpGet = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/event/count/" + eventClass);
        TestHelpers.addDisableCsrfHeader(httpGet);

        var response = httpClient.execute(httpGet);
        var objectMapper = new ObjectMapper();

        var result = objectMapper.readValue(response.getEntity().getContent(), EventCountBean.class).count();
        response.close();
        return result;
    }

    record EventCountBean(int count) {
    }
}
