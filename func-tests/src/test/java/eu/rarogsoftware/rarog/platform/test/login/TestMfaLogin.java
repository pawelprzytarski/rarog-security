package eu.rarogsoftware.rarog.platform.test.login;

import eu.rarogsoftware.commons.test.webdriver.playwright.PlaywrightSettings;
import eu.rarogsoftware.commons.test.webdriver.playwright.WebdriverTester;
import eu.rarogsoftware.rarog.platform.junit.extensions.WebdriverTestExtension;
import eu.rarogsoftware.rarog.platform.page.objects.login.LoginPage;
import eu.rarogsoftware.rarog.platform.page.objects.login.MfaPage;
import eu.rarogsoftware.rarog.platform.page.objects.login.RecoveryCodeRegeneratedCodesPage;
import eu.rarogsoftware.rarog.platform.test.PreconfiguredUsers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.time.Duration;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(WebdriverTestExtension.class)
@PlaywrightSettings(contextCreation = PlaywrightSettings.BrowserContextScope.METHOD)
public class TestMfaLogin {
    @Inject
    WebdriverTester tester;

    @BeforeEach
    @AfterEach
    void tearDown() {
        tester.goTo("/logout");
    }

    @Test
    void testMfaByPass() {
        var loginPage = tester.goTo(LoginPage.class);
        var mfaPage = loginPage.loginAndBind(PreconfiguredUsers.MFA_ADMIN_USERNAME, PreconfiguredUsers.MFA_ADMIN_PASSWORD, MfaPage.class);
        mfaPage.byPass();
        await().atMost(Duration.ofSeconds(10))
                .untilAsserted(() -> assertEquals("/admin", tester.getCurrentUrl()));
    }

    @Test
    void attemptToAccessOtherPage() {
        var loginPage = tester.goTo(LoginPage.class);
        loginPage.loginAndBind(PreconfiguredUsers.MFA_ADMIN_USERNAME, PreconfiguredUsers.MFA_ADMIN_PASSWORD, MfaPage.class);
        tester.goTo("/account/my");
        await().atMost(Duration.ofSeconds(10))
                .untilAsserted(() -> assertEquals("/login/mfa/?redirect=/account/my", tester.getCurrentUrl()));
    }

    @Test
    void testMfaRecoveryCode() {
        var loginPage = tester.goTo(LoginPage.class);
        var mfaPage = loginPage.loginAndBind(PreconfiguredUsers.MFA_ADMIN_USERNAME, PreconfiguredUsers.MFA_ADMIN_PASSWORD, MfaPage.class);
        var regeneratedCodesPage = mfaPage.selectRecoveryCode().enterCodeAndBind("test1", RecoveryCodeRegeneratedCodesPage.class);
        var codes = regeneratedCodesPage.getCodes();
        regeneratedCodesPage.proceed();
        await().atMost(Duration.ofSeconds(10))
                .untilAsserted(() -> assertEquals("/admin", tester.getCurrentUrl()));

        tester.goTo("/logout");

        loginPage = tester.goTo(LoginPage.class);
        mfaPage = loginPage.loginAndBind(PreconfiguredUsers.MFA_ADMIN_USERNAME, PreconfiguredUsers.MFA_ADMIN_PASSWORD, MfaPage.class);
        mfaPage.selectRecoveryCode().enterCode(codes.get(0));
        await().atMost(Duration.ofSeconds(10))
                .untilAsserted(() -> assertEquals("/admin", tester.getCurrentUrl()));
    }
}
