package eu.rarogsoftware.rarog.platform.test.pat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.backdoor.control.ApplicationSettingsControl;
import eu.rarogsoftware.rarog.platform.backdoor.control.BackdoorControl;
import eu.rarogsoftware.rarog.platform.backdoor.control.PathFragments;
import eu.rarogsoftware.rarog.platform.junit.extensions.RestApiTestExtension;
import eu.rarogsoftware.rarog.platform.test.ApplicationSettingKeys;
import eu.rarogsoftware.rarog.platform.test.PreconfiguredUsers;
import eu.rarogsoftware.rarog.platform.test.commons.TestHelpers;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.Clock;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(RestApiTestExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TestPersonalAccessToken {
    private static final String USER_TOKEN_ENDPOINT_URL = "rest/pat/1.0/token/user/";
    private static final List<String> AUTH_LIST = Arrays.asList("ROLE_USER", "ROLE_ADMIN");
    private static final String TOKEN_NAME = "Test token";
    private final ObjectMapper jsonMapper = JsonMapper.builder()
            .addModule(new JavaTimeModule())
            .build()
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, true)
            .configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
            .configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
    @Inject
    BackdoorControl backdoorControl;
    @Inject
    EnvironmentData environmentData;
    @Inject
    CloseableHttpClient httpClient;
    private List<ApplicationSettingsControl.SettingBean> originalSettings;
    private PathFragments controllerPath;

    @BeforeEach
    void setUp() throws IOException {
        controllerPath = PathFragments.of(environmentData.baseUrl(), USER_TOKEN_ENDPOINT_URL);
        originalSettings = Arrays.asList(
                backdoorControl.applicationSettings().getSetting(ApplicationSettingKeys.PAT_USER_KEYS_ENABLED_KEY),
                backdoorControl.applicationSettings().getSetting(ApplicationSettingKeys.PAT_APP_KEYS_ENABLED_KEY)
        );
    }

    @AfterEach
    void tearDown() throws IOException {
        for (var original : originalSettings) {
            backdoorControl.applicationSettings().setSetting(original.name(), original.value());
        }
    }

    @Test
    @Order(1)
    void testListOfMyTokensForEmptyState() throws IOException {
        var httpGet = new HttpGet(controllerPath.path("my").toString());
        TestHelpers.addUserAuthHeader(httpGet);
        try (var response = httpClient.execute(httpGet)) {
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
            var results = jsonMapper.readValue(response.getEntity().getContent().readAllBytes(), new TypeReference<List<TokenDisplayData>>() {
            });
            assertThat(results).isEmpty();
        }
    }

    @RepeatedTest(2)
    @Order(2)
    void testCreateToken() throws IOException {
        Instant expiration = Instant.now().plusSeconds(60);
        try (var response = createToken(expiration)) {
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(201);
            var token = readTokenFromResponse(response);
            assertThat(token.name).isEqualTo(TOKEN_NAME);
            assertThat(token.expiration).isEqualTo(getExpiration(expiration));
            assertThat(token.value).isNotBlank();
        }
    }

    private TokenFullData readTokenFromResponse(CloseableHttpResponse response) throws IOException {
        var content = response.getEntity().getContent().readAllBytes();
        return jsonMapper.readValue(content, TokenFullData.class);
    }

    private CloseableHttpResponse createToken(Instant expiration) throws IOException {
        var httpPost = new HttpPost(controllerPath.path("create").toString());
        TestHelpers.addUserAuthHeader(httpPost);
        var tokenRequest = new CreateTokenRequest(TOKEN_NAME, getExpiration(expiration), AUTH_LIST);
        httpPost.setEntity(createCreateRequest(tokenRequest));
        return httpClient.execute(httpPost);
    }

    private static ZonedDateTime getExpiration(Instant expiration) {
        return ZonedDateTime.ofInstant(expiration, Clock.systemDefaultZone().getZone());
    }

    private StringEntity createCreateRequest(CreateTokenRequest token) throws JsonProcessingException, UnsupportedEncodingException {
        var json = jsonMapper.writeValueAsString(token);
        return new StringEntity(json, ContentType.APPLICATION_JSON);
    }

    @Test
    @Order(3)
    void testListOfMyTokensForCreatedTokens() throws IOException {
        var httpGet = new HttpGet(controllerPath.path("my").toString());
        TestHelpers.addUserAuthHeader(httpGet);
        try (var response = httpClient.execute(httpGet)) {
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
            var results = jsonMapper.readValue(response.getEntity().getContent().readAllBytes(), new TypeReference<List<TokenDisplayData>>() {
            });
            assertThat(results).hasSize(2);
            assertThat(results.get(0).name).isEqualTo(TOKEN_NAME);
            assertThat(results.get(1).name).isEqualTo(TOKEN_NAME);
        }
    }

    @Test
    @Order(4)
    void testLoginWithGeneratedToken() throws IOException {
        try (var response = createToken(Instant.now().plusSeconds(60))) {
            var createdToken = readTokenFromResponse(response);
            var authHeader = new BasicHeader(HttpHeaders.AUTHORIZATION, "Token " + createdToken.value());
            var state = backdoorControl.systemStateHelpers().getLoggedInAccountState(authHeader);

            assertThat(state.name()).isEqualTo(PreconfiguredUsers.USER_USERNAME);
            assertThat(state.type()).isEqualTo("human");
            assertThat(state.authorities()).containsExactlyInAnyOrder("ROLE_USER", "AUTH_SYNTHETIC");
        }
    }

    @Test
    @Order(4)
    void testAttemptToGetTokensWithToken() throws IOException {
        try (var createResponse = createToken(Instant.now().plusSeconds(60))) {
            var createdToken = readTokenFromResponse(createResponse);
            var authHeader = new BasicHeader(HttpHeaders.AUTHORIZATION, "Token " + createdToken.value());
            var httpGet = new HttpGet(controllerPath.path("my").toString());
            httpGet.addHeader(authHeader);
            try (var response = httpClient.execute(httpGet)) {
                assertThat(response.getStatusLine().getStatusCode()).isEqualTo(403);
            }
        }
    }

    @Test
    @Order(5)
    void testAttemptAccessToOtherUserListAsNormalUser() throws IOException {
        var httpGet = new HttpGet(controllerPath.path("2").path("list").toString());
        TestHelpers.addUserAuthHeader(httpGet);
        try (var response = httpClient.execute(httpGet)) {
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(403);
        }
    }

    @Test
    @Order(6)
    void testGetTokensForUsername() throws IOException {
        var httpGet = new HttpGet(controllerPath.path(PreconfiguredUsers.USER_USERNAME).path("list").toString());
        TestHelpers.addAdminAuthHeader(httpGet);
        try (var response = httpClient.execute(httpGet)) {
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
            var results = jsonMapper.readValue(response.getEntity().getContent().readAllBytes(), new TypeReference<List<TokenDisplayData>>() {
            });
            assertThat(results).hasSize(4);
        }
    }

    @Test
    @Order(6)
    void testRemoveTokensAsAdmin() throws IOException {
        var httpGet = new HttpGet(controllerPath.path(PreconfiguredUsers.USER_ID.toString()).path("list").toString());
        TestHelpers.addAdminAuthHeader(httpGet);
        try (var response = httpClient.execute(httpGet)) {
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
            var results = jsonMapper.readValue(response.getEntity().getContent().readAllBytes(), new TypeReference<List<TokenDisplayData>>() {
            });
            var httpDelete = new HttpDelete(controllerPath.path(results.get(0).id().toString()).toString());
            TestHelpers.addAdminAuthHeader(httpDelete);
            TestHelpers.addDisableCsrfHeader(httpDelete);
            try (var delete = httpClient.execute(httpDelete)) {
                assertThat(delete.getStatusLine().getStatusCode()).isEqualTo(200);
            }
        }
    }

    @Test
    @Order(7)
    void testRemoveTokensAsNormalUser() throws IOException {
        var httpGet = new HttpGet(controllerPath.path("my").toString());
        TestHelpers.addUserAuthHeader(httpGet);
        try (var response = httpClient.execute(httpGet)) {
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
            var results = jsonMapper.readValue(response.getEntity().getContent().readAllBytes(), new TypeReference<List<TokenDisplayData>>() {
            });
            for (TokenDisplayData tokenDisplayData : results) {
                var httpDelete = new HttpDelete(controllerPath.path(tokenDisplayData.id().toString()).toString());
                TestHelpers.addUserAuthHeader(httpDelete);
                TestHelpers.addDisableCsrfHeader(httpDelete);
                try (var delete = httpClient.execute(httpDelete)) {
                    assertThat(delete.getStatusLine().getStatusCode()).isEqualTo(200);
                }
            }
        }
    }

    record CreateTokenRequest(String name, ZonedDateTime expiration, List<String> authorities) {
    }

    record TokenDisplayData(Long id, String name, ZonedDateTime expiration) {
    }

    record TokenFullData(String name, ZonedDateTime expiration, String value) {
    }
}
