package eu.rarogsoftware.rarog.platform.test.login;

import eu.rarogsoftware.commons.test.webdriver.playwright.PlaywrightSettings;
import eu.rarogsoftware.commons.test.webdriver.playwright.WebdriverTester;
import eu.rarogsoftware.rarog.platform.junit.extensions.WebdriverTestExtension;
import eu.rarogsoftware.rarog.platform.page.objects.login.LoginPage;
import eu.rarogsoftware.rarog.platform.test.PreconfiguredUsers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.time.Duration;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(WebdriverTestExtension.class)
@PlaywrightSettings(contextCreation = PlaywrightSettings.BrowserContextScope.METHOD)
class TestBasicLoginFlow {
    @Inject
    WebdriverTester tester;

    @Test
    void testSuccessfulLogin() {
        LoginPage loginPage = tester.goTo(LoginPage.class);
        loginPage.login(PreconfiguredUsers.ADMIN_USERNAME, PreconfiguredUsers.ADMIN_PASSWORD);
        await().atMost(Duration.ofSeconds(10))
                .untilAsserted(() -> assertEquals("/admin", tester.getCurrentUrl()));
    }

    @Test
    void testIncorrectPasswordLogin() {
        LoginPage loginPage = tester.goTo(LoginPage.class);
        loginPage.login("admin", "wrongPassword", false);
        String errorMessage = loginPage.getErrorMessage();
        assertEquals("login.error.badcredentials", errorMessage);
        assertEquals(loginPage.getTargetPageUrl(), tester.getCurrentUrl());
    }

    @Test
    void testIncorrectUsernameLogin() {
        LoginPage loginPage = tester.goTo(LoginPage.class);
        loginPage.login("nonExistentUser", "wrongPassword", false);
        String errorMessage = loginPage.getErrorMessage();
        assertEquals("login.error.badcredentials", errorMessage);
        assertEquals(loginPage.getTargetPageUrl(), tester.getCurrentUrl());
    }

    @Test
    void testLogout() {
        LoginPage loginPage = tester.goTo(LoginPage.class);
        loginPage.login(PreconfiguredUsers.ADMIN_USERNAME, PreconfiguredUsers.ADMIN_PASSWORD);
        await().atMost(Duration.ofSeconds(10))
                .untilAsserted(() -> assertEquals("/admin", tester.getCurrentUrl()));
        tester.goTo("/logout");
        await().atMost(Duration.ofSeconds(10))
                .untilAsserted(() -> assertEquals("/login/", tester.getCurrentUrl()));
    }
}
