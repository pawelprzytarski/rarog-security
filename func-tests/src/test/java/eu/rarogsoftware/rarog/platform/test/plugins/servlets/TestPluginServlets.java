package eu.rarogsoftware.rarog.platform.test.plugins.servlets;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.backdoor.control.ApplicationSettingsControl;
import eu.rarogsoftware.rarog.platform.backdoor.control.BackdoorControl;
import eu.rarogsoftware.rarog.platform.junit.extensions.RestApiTestExtension;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.HttpStatus;

import javax.inject.Inject;
import java.io.IOException;

import static eu.rarogsoftware.rarog.platform.test.ApplicationSettingKeys.FORCE_SYSTEM_API;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(RestApiTestExtension.class)
class TestPluginServlets {
    @Inject
    EnvironmentData environmentData;
    @Inject
    CloseableHttpClient httpClient;
    @Inject
    BackdoorControl backdoorControl;
    private ApplicationSettingsControl.SettingBean originalSetting;
    private final String SPRING_SERVLET_RESPONSE = "Spring servlet response";

    @BeforeEach
    void setUp() throws IOException {
        this.originalSetting = backdoorControl.applicationSettings().getSetting(FORCE_SYSTEM_API);
    }

    @AfterEach
    void tearDown() throws IOException {
        backdoorControl.applicationSettings().setSetting(originalSetting.name(), originalSetting.value());
    }

    @Test
    void testSpringIsNotOverriddenByLessImportantServlet() throws IOException {
        var httpGet = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/overriddenServletTest");

        var response = httpClient.execute(httpGet);
        var responseText = new String(response.getEntity().getContent().readAllBytes());

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(responseText).isEqualTo(SPRING_SERVLET_RESPONSE);
    }

    @Test
    void testSpringIsOverriddenByMoreImportantServlet() throws IOException {
        var httpGet = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/overridingServletTest");

        var response = httpClient.execute(httpGet);
        var responseText = new String(response.getEntity().getContent().readAllBytes());

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(responseText).isEqualTo("Overriding servlet response");
    }

    @Test
    void testSpringIsNotOverriddenByMoreImportantServletWhenEnforcedSpringSupremacy() throws IOException {
        backdoorControl.applicationSettings().setSetting(FORCE_SYSTEM_API, true);

        var httpGet = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/overridingServletTest");

        var response = httpClient.execute(httpGet);
        var responseText = new String(response.getEntity().getContent().readAllBytes());

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(responseText).isEqualTo(SPRING_SERVLET_RESPONSE);
    }

    @Test
    void testSpringIsNotBlockingPostRequestsWhenServletsOverridesCsrfProtection() throws IOException {
        var httpPost = new HttpPost(environmentData.baseUrl() + "rest/func-test/1.0/test/overridingServletTest");

        var response = httpClient.execute(httpPost);
        var responseText = new String(response.getEntity().getContent().readAllBytes());

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(responseText).isEqualTo("Overriding servlet response");
    }
}
