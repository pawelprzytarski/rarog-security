package eu.rarogsoftware.rarog.platform.test.login;

import eu.rarogsoftware.commons.test.webdriver.playwright.WebdriverTester;
import eu.rarogsoftware.rarog.platform.api.user.management.UserRole;
import eu.rarogsoftware.rarog.platform.backdoor.control.BackdoorControl;
import eu.rarogsoftware.rarog.platform.junit.extensions.WebdriverTestExtension;
import eu.rarogsoftware.rarog.platform.page.objects.account.settings.BasicAccountSettingsPage;
import eu.rarogsoftware.rarog.platform.page.objects.login.LoginPage;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(WebdriverTestExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TestChangePasswordForm {
    private static final String TEST_USERNAME = "TemporaryTestUser";
    private static final String NEW_PASSWORD = "NewPassword";
    private static final String INVALID_PASSWORD = "InvalidPassword";
    private static final String INVALID_FORM_MESSAGE = "Please fill fields in form and fix incorrect values";
    @Inject
    WebdriverTester tester;
    @Inject
    BackdoorControl backdoorControl;

    @Test
    @Order(1)
    void testCreateUserWithBackdoor() throws IOException {
        backdoorControl.usersControl().createUser(TEST_USERNAME, TEST_USERNAME, Collections.singletonList(UserRole.USER)).close();
        LoginPage loginPage = tester.goTo(LoginPage.class);
        var settingsPage = loginPage.loginAndBind(TEST_USERNAME, TEST_USERNAME, BasicAccountSettingsPage.class);
        assertThat(settingsPage.isAt()).isTrue();
    }

    @Test
    @Order(2)
    void testInvalidFormAttempts() {
        var settingsPage = tester.bindPage(BasicAccountSettingsPage.class);
        var emptyPassesError = settingsPage.changePassword("", "", "");
        assertThat(emptyPassesError).isEqualTo(INVALID_FORM_MESSAGE);
        var mismatchedPasswords = settingsPage.changePassword(INVALID_PASSWORD, NEW_PASSWORD, INVALID_PASSWORD);
        assertThat(mismatchedPasswords).isEqualTo(INVALID_FORM_MESSAGE);
        var incorrectOldPasswords = settingsPage.changePassword(INVALID_PASSWORD, NEW_PASSWORD, NEW_PASSWORD);
        assertThat(incorrectOldPasswords).isEqualTo("Old password is incorrect");
    }

    @Test
    @Order(3)
    void testValidAttemptWillChangePassword() {
        var settingsPage = tester.bindPage(BasicAccountSettingsPage.class);
        var emptyPassesError = settingsPage.changePassword(TEST_USERNAME, NEW_PASSWORD, NEW_PASSWORD);
        assertThat(emptyPassesError).isEqualTo("Successfully changed password");
        tester.goTo("/logout");
        tester.goTo("/account/my");
        var loginPage = tester.bindPage(LoginPage.class);
        var redirectedPage = loginPage.loginAndBind(TEST_USERNAME, NEW_PASSWORD, BasicAccountSettingsPage.class);
        Assertions.assertThat(redirectedPage.isAt()).isTrue();
    }

    @Test
    @Order(1000)
    void testRemoveUserWithBackdoor() throws IOException {
        backdoorControl.usersControl().removeUser(TEST_USERNAME);
    }
}
