package eu.rarogsoftware.rarog.platform.test;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.commons.test.webdriver.junit.EnvironmentDataExtension;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Used only {@link EnvironmentDataExtension} because we want to avoid test to fail due to other extensions.
 */
@ExtendWith(EnvironmentDataExtension.class)
public class TestApplicationStarts {
    @Inject
    private EnvironmentData environmentalData;
    private HttpClient httpClient;

    @BeforeEach
    void setUp() {
        httpClient = HttpClientBuilder.create().build();
    }

    @Test
    public void test() throws IOException {
        HttpGet httpGet = new HttpGet(environmentalData.baseUrl() + "status");

        var response = httpClient.execute(httpGet);

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }
}
