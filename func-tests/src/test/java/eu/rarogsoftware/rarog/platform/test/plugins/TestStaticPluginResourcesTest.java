package eu.rarogsoftware.rarog.platform.test.plugins;

import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.junit.extensions.RestApiTestExtension;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.io.IOException;

import static eu.rarogsoftware.rarog.platform.test.FuncTestConstants.FUNC_TEST_PLUGIN_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(RestApiTestExtension.class)
public class TestStaticPluginResourcesTest {
    @Inject
    EnvironmentData environmentData;
    @Inject
    CloseableHttpClient httpClient;

    @Test
    void testFuncTestPluginExistingResourceIsAvailable() throws IOException {
        HttpGet httpGet = new HttpGet(environmentData.baseUrl() + "static/p/" + FUNC_TEST_PLUGIN_KEY + "/test.txt");
        var response = httpClient.execute(httpGet);
        var responseText = new String(response.getEntity().getContent().readAllBytes());
        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        assertEquals("Test File in Backdoor Plugin", responseText);
    }

    @Test
    void testFuncTestPluginNonExistingResourceIsNotAvailable() throws IOException {
        HttpGet httpGet = new HttpGet(environmentData.baseUrl() + "static/p/" + FUNC_TEST_PLUGIN_KEY + "/incorrect.txt");
        var response = httpClient.execute(httpGet);
        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    void testUnknownPluginResourceIsNotAvailable() throws IOException {
        HttpGet httpGet = new HttpGet(environmentData.baseUrl() + "static/p/unknownPlugin/incorrect.txt");
        var response = httpClient.execute(httpGet);
        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    void testNoPluginResourceIsNotAvailable() throws IOException {
        HttpGet httpGet = new HttpGet(environmentData.baseUrl() + "static/p//incorrect.txt");
        var response = httpClient.execute(httpGet);
        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    void testDirectResourceIsNotAvailable() throws IOException {
        HttpGet httpGet = new HttpGet(environmentData.baseUrl() + "static/p/incorrect.txt");
        var response = httpClient.execute(httpGet);
        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    void testNoResourcePathIsNotAvailable() throws IOException {
        HttpGet httpGet = new HttpGet(environmentData.baseUrl() + "static/p/");
        var response = httpClient.execute(httpGet);
        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    void testFuncTestPluginCustomResourceResolverResolvesCorrectly() throws IOException {
        HttpGet httpGet = new HttpGet(environmentData.baseUrl() + "static/p/" + FUNC_TEST_PLUGIN_KEY + "/testStaticResource");
        var response = httpClient.execute(httpGet);
        var responseText = new String(response.getEntity().getContent().readAllBytes());
        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        assertEquals("Test File in Backdoor Plugin", responseText);
    }

    @Test
    void testFuncTestPluginCustomResourceResolverReturnsNotFoundWhenDoNotMatch() throws IOException {
        HttpGet httpGet = new HttpGet(environmentData.baseUrl() + "static/p/" + FUNC_TEST_PLUGIN_KEY + "/UnknownTestStaticResource");
        var response = httpClient.execute(httpGet);
        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }
}
