package eu.rarogsoftware.rarog.platform.test.pat;

import eu.rarogsoftware.commons.test.webdriver.playwright.WebdriverTester;
import eu.rarogsoftware.rarog.platform.api.user.management.UserRole;
import eu.rarogsoftware.rarog.platform.backdoor.control.BackdoorControl;
import eu.rarogsoftware.rarog.platform.junit.extensions.WebdriverTestExtension;
import eu.rarogsoftware.rarog.platform.page.objects.account.settings.BasicAccountSettingsPage;
import eu.rarogsoftware.rarog.platform.page.objects.account.settings.CreateUserTokenPageUser;
import eu.rarogsoftware.rarog.platform.page.objects.account.settings.UserAccessTokensListPage;
import eu.rarogsoftware.rarog.platform.page.objects.login.LoginPage;
import org.apache.http.HttpHeaders;
import org.apache.http.message.BasicHeader;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(WebdriverTestExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TestPersonalAccessTokensWebUI {
    private static final String TEST_USERNAME = "TemporaryTestUser";
    private static final String USER_AUTHORITY = UserRole.USER.getAuthority().getAuthority();
    @Inject
    WebdriverTester tester;
    @Inject
    BackdoorControl backdoorControl;

    @Test
    @Order(1)
    void createUserAndNavigateToAccessTokensPage() throws IOException {
        backdoorControl.usersControl().createUser(TEST_USERNAME, TEST_USERNAME, Arrays.asList(UserRole.USER, UserRole.ADMINISTRATOR)).close();
        LoginPage loginPage = tester.goTo(LoginPage.class);
        loginPage.login(TEST_USERNAME, TEST_USERNAME);
        tester.goTo(UserAccessTokensListPage.class);
    }

    @Test
    @Order(2)
    void testNoTokensPresent() throws IOException {
        var listPage = tester.bindPage(UserAccessTokensListPage.class);
        Assertions.assertThat(listPage.isAt()).isTrue();
        assertThat(tester.getPageSource()).contains("You have no access token created. Click here to create new one!");
        listPage.clickCreateButtonNav();
    }

    @RepeatedTest(2)
    @Order(3)
    void testCreateToken(RepetitionInfo repetitionInfo) throws IOException {
        var createPage = tester.bindPage(CreateUserTokenPageUser.class);
        var newToken = createPage.createToken("Test token " + repetitionInfo.getCurrentRepetition(), "2123-01-04T18:02:41", Collections.singletonList(USER_AUTHORITY));
        var checkResult = backdoorControl.systemStateHelpers().getLoggedInAccountState(new BasicHeader(HttpHeaders.AUTHORIZATION, "Token " + newToken));
        assertThat(checkResult.name()).isEqualTo(TEST_USERNAME);
        assertThat(checkResult.authorities()).containsExactlyInAnyOrder(USER_AUTHORITY, "AUTH_SYNTHETIC");
    }

    @Test
    @Order(4)
    void testRemoveTokens() {
        var listPage = tester.bindPage(CreateUserTokenPageUser.class).clickListButtonNav();
        assertThat(listPage.getTokens()).element(0).asInstanceOf(InstanceOfAssertFactories.STRING).startsWith("Test token 1");
        assertThat(listPage.getTokens()).element(1).asInstanceOf(InstanceOfAssertFactories.STRING).startsWith("Test token 2");
        listPage.removeToken(listPage.getTokenIds().get(0));
        assertThat(listPage.getTokens()).hasSize(1);
    }

    @Test
    @Order(1000)
    void testRemoveUserWithBackdoor() throws IOException {
        backdoorControl.usersControl().removeUser(TEST_USERNAME);
    }
}
