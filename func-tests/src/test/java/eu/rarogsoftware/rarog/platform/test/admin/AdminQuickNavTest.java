package eu.rarogsoftware.rarog.platform.test.admin;

import eu.rarogsoftware.commons.test.webdriver.playwright.WebdriverTester;
import eu.rarogsoftware.rarog.platform.junit.extensions.LoginAs;
import eu.rarogsoftware.rarog.platform.junit.extensions.WebdriverTestExtension;
import eu.rarogsoftware.rarog.platform.page.objects.admin.AdminPage;
import eu.rarogsoftware.rarog.platform.page.objects.admin.GenericAdminPage;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.time.Duration;

import static eu.rarogsoftware.rarog.platform.test.PreconfiguredUsers.ADMIN_USERNAME;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.awaitility.Awaitility.await;

@ExtendWith(WebdriverTestExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@LoginAs(ADMIN_USERNAME)
class AdminQuickNavTest {
    @Inject
    WebdriverTester tester;

    @Test
    @Order(0)
    void quickSearchReactsToHotKey() {
        var dashboard = tester.goTo(GenericAdminPage.class);
        dashboard.pushHotKey("Slash");

        await().untilAsserted(() -> assertThat(dashboard.quickNav().isMenuVisible()).isTrue());

        dashboard.quickNav().close();
        await().untilAsserted(() -> assertThat(dashboard.quickNav().isMenuVisible()).isFalse());

        dashboard.pushHotKey("Backslash");
        await().pollDelay(Duration.ofSeconds(1)).untilAsserted(() -> assertThat(dashboard.quickNav().isMenuVisible()).isFalse());
    }

    @Test
    @Order(1)
    void displayingDefaultElementsWhenSearchHistoryIsEmpty() {
        var dashboard = tester.goTo(GenericAdminPage.class);
        dashboard.quickNav().open();
        var result = dashboard.quickNav().getDisplayedElements();

        assertThat(result).asList()
                .contains(
                        new AdminPage.QuickNav.Element("Dashboard", " - Dashboard"),
                        new AdminPage.QuickNav.Element("System info", " - info, system info, system specification, specification"),
                        new AdminPage.QuickNav.Element("System settings", " - settings, basic settings, general"),
                        new AdminPage.QuickNav.Element("Some name", " - testKeyword for general category"),
                        new AdminPage.QuickNav.Element("Some name", " - testKeyword for test category")
                );
    }

    @Test
    @Order(2)
    void typingChangesDisplayedOptions() {
        var dashboard = tester.goTo(GenericAdminPage.class);
        dashboard.quickNav().open();
        dashboard.quickNav().input("Dashboard");

        await().untilAsserted(() -> assertThat(dashboard.quickNav().getDisplayedElements()).asList().hasSize(1));
        var resultForDashboardInput = dashboard.quickNav().getDisplayedElements();

        assertThat(resultForDashboardInput).asList()
                .contains(
                        new AdminPage.QuickNav.Element("Dashboard", " - Dashboard")
                );

        dashboard.quickNav().input("testKeyword");

        await().untilAsserted(() -> assertThat(dashboard.quickNav().getDisplayedElements()).asList().hasSize(2));
        var resultForTestKeyword = dashboard.quickNav().getDisplayedElements();

        assertThat(resultForTestKeyword).asList()
                .contains(
                        new AdminPage.QuickNav.Element("Some name", " - testKeyword for general category"),
                        new AdminPage.QuickNav.Element("Some name", " - testKeyword for test category")
                );
    }

    @Test
    @Order(3)
    void selectingOptionNavigates() {
        var dashboard = tester.goTo(GenericAdminPage.class);
        dashboard.quickNav().open();
        dashboard.quickNav().input("System info");

        await().untilAsserted(() -> assertThat(dashboard.quickNav().getDisplayedElements()).asList().hasSize(1));

        dashboard.quickNav().selectItem(0);

        await().untilAsserted(() -> assertThat(dashboard.getContentText()).contains("System info"));

        dashboard.quickNav().open();
        dashboard.quickNav().input("test category");

        await().untilAsserted(() -> assertThat(dashboard.quickNav().getDisplayedElements()).asList().hasSize(1));

        dashboard.quickNav().selectItem(0);

        await().untilAsserted(() -> assertThat(dashboard.getContentText()).contains("Content of test page"));
    }

    @Test
    @Order(4)
    void displayingHistoryAfterSearches() {
        var dashboard = tester.goTo(GenericAdminPage.class);
        dashboard.quickNav().open();
        var result = dashboard.quickNav().getDisplayedElements();

        assertThat(result).asList()
                .containsExactly(
                        new AdminPage.QuickNav.Element("Some name", " - testKeyword for test category"),
                        new AdminPage.QuickNav.Element("System info", " - info, system info, system specification, specification")
                );
    }
}
