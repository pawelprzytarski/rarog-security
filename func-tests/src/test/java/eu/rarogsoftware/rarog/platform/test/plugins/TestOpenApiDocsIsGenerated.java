package eu.rarogsoftware.rarog.platform.test.plugins;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.junit.extensions.RestApiTestExtension;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(RestApiTestExtension.class)
class TestOpenApiDocsIsGenerated {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    @Inject
    EnvironmentData environmentData;
    @Inject
    CloseableHttpClient httpClient;

    @Test
    void testOpenApiPicksEndpointsFromPlugins() throws IOException {
        var httpRequest = new HttpGet(environmentData.baseUrl() + "docs/api-docs");
        var response = httpClient.execute(httpRequest);

        var docTree = MAPPER.readTree(response.getEntity().getContent());
        var paths = docTree.get("paths");
        var telemetryPath = paths.get("/rest/telemetry/1.0/expose/openmetrics/");

        assertThat(telemetryPath).isNotNull();
        assertThat(telemetryPath.get("get").get("summary").asText()).contains("Returns gathered metrics in OpenMetrics format");
    }
}
