package eu.rarogsoftware.rarog.platform.test.plugins;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.junit.extensions.RestApiTestExtension;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(RestApiTestExtension.class)
class TestHandlerMappingDescriptorTest {
    @Inject
    EnvironmentData environmentData;
    @Inject
    CloseableHttpClient httpClient;

    @Test
    void testFuncTestPluginRestEndpointIsAvailable() throws IOException {
        var httpGet = new HttpGet(environmentData.baseUrl() + "rest/func-test/1.0/test/");
        var response = httpClient.execute(httpGet);
        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        var testBean = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .readValue(response.getEntity().getContent(), TestBean.class);
        assertEquals(new TestBean("", "Ok"), testBean);
    }

    @Test
    void testFuncTestPluginMvcEndpointIsAvailable() throws IOException {
        var httpGet = new HttpGet(environmentData.baseUrl() + "plugin/func-test/test");
        var response = httpClient.execute(httpGet);
        var responseText = new String(response.getEntity().getContent().readAllBytes());
        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        assertThat(responseText, containsString("<p>Text from model:<span>TestTest</span></p>"));
    }

    @Test
    void testFuncTestPluginMvcEndpointCorrectlyResolvesDynamicAssets() throws IOException {
        var httpGet = new HttpGet(environmentData.baseUrl() + "plugin/func-test/test");
        var response = httpClient.execute(httpGet);
        var responseText = new String(response.getEntity().getContent().readAllBytes());
        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        assertThat(responseText, containsString("<script src=\"/server/test.js\"></script>"));
        assertThat(responseText, containsString("<script src=\"/server/second.js\"></script>"));
    }

    @Test
    void testFuncTestPluginMvcEndpointWithCustomTemplateResolver() throws IOException {
        var httpGet = new HttpGet(environmentData.baseUrl() + "plugin/func-test/test/customTemplateResolver");
        var response = httpClient.execute(httpGet);
        var responseText = new String(response.getEntity().getContent().readAllBytes());
        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        assertThat(responseText, containsString("Custom template content"));
    }

    @Test
    void testFuncTestPluginMvcEndpointWithCustomViewResolver() throws IOException {
        var httpGet = new HttpGet(environmentData.baseUrl() + "plugin/func-test/test/customViewResolver");
        var response = httpClient.execute(httpGet);
        var responseText = new String(response.getEntity().getContent().readAllBytes());
        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        assertThat(responseText, containsString("Custom view resolver content"));
    }

    record TestBean(String loggedInUser, String serverStatus) {
    }
}
