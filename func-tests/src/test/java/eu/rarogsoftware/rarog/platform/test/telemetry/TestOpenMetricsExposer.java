package eu.rarogsoftware.rarog.platform.test.telemetry;

import com.google.common.base.Charsets;
import eu.rarogsoftware.commons.test.webdriver.EnvironmentData;
import eu.rarogsoftware.rarog.platform.junit.extensions.RestApiTestExtension;
import eu.rarogsoftware.rarog.platform.test.ApplicationSettingKeys;
import eu.rarogsoftware.rarog.platform.backdoor.control.ApplicationSettingsControl;
import eu.rarogsoftware.rarog.platform.backdoor.control.BackdoorControl;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(RestApiTestExtension.class)
class TestOpenMetricsExposer {
    private static final String TELEMETRY_ENDPOINT_URL = "rest/telemetry/1.0/expose/openmetrics/";
    private static final String TEST_TOKEN = "TEST_PASSWORD";
    private List<ApplicationSettingsControl.SettingBean> originalSettings;
    @Inject
    EnvironmentData environmentData;
    @Inject
    BackdoorControl backdoorControl;
    @Inject
    CloseableHttpClient httpClient;

    @BeforeEach
    void setUp() throws IOException {
        originalSettings = Arrays.asList(
                backdoorControl.applicationSettings().getSetting(ApplicationSettingKeys.METRICS_ENABLED_KEY),
                backdoorControl.applicationSettings().getSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_ENABLED_KEY),
                backdoorControl.applicationSettings().getSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY)
        );
    }

    @AfterEach
    void tearDown() throws IOException {
        for (var original : originalSettings) {
            backdoorControl.applicationSettings().setSetting(original.name(), original.value());
        }
    }

    @Test
    void testEndpointIsNotAvailableForDisabledMetrics() throws IOException {
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.METRICS_ENABLED_KEY, false);

        var httpGet = new HttpGet(environmentData.baseUrl() + TELEMETRY_ENDPOINT_URL);
        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NOT_FOUND));
    }

    @Test
    void testEndpointIsNotAvailableWhenEndpointIsDisabled() throws IOException {
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.METRICS_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_ENABLED_KEY, false);

        var httpGet = new HttpGet(environmentData.baseUrl() + TELEMETRY_ENDPOINT_URL);
        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NOT_FOUND));
    }

    @Test
    void testEndpointIsAvailableWhenAuthorizationIsDisabled() throws IOException {
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.METRICS_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY, null);

        var httpGet = new HttpGet(environmentData.baseUrl() + TELEMETRY_ENDPOINT_URL);
        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
    }

    @Test
    void testEndpointIsNotAvailableWhenAuthorizationIsEnabled() throws IOException {
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.METRICS_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY, BCrypt.hashpw(TEST_TOKEN, BCrypt.gensalt()));

        var httpGet = new HttpGet(environmentData.baseUrl() + TELEMETRY_ENDPOINT_URL);
        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
    }

    @Test
    void testEndpointIsNotAvailableWhenAuthorizationIsEnabledAndPassedIncorrectAuthorization() throws IOException {
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.METRICS_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY, BCrypt.hashpw(TEST_TOKEN, BCrypt.gensalt()));

        var httpGet = new HttpGet(environmentData.baseUrl() + TELEMETRY_ENDPOINT_URL);
        httpGet.addHeader("Authorization", "Bearer someToken");
        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
    }

    @Test
    void testEndpointIsNotAvailableWhenAuthorizationIsEnabledAndPassedIncorrectToken() throws IOException {
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.METRICS_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY, BCrypt.hashpw(TEST_TOKEN, BCrypt.gensalt()));

        var httpGet = new HttpGet(environmentData.baseUrl() + TELEMETRY_ENDPOINT_URL);
        httpGet.addHeader("Authorization", "Code someIncorrectToken");
        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_UNAUTHORIZED));
    }

    @Test
    void testEndpointIsAvailableWhenAuthorizationIsEnabledAndPassedCorrectToken() throws IOException {
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.METRICS_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY, BCrypt.hashpw(TEST_TOKEN, BCrypt.gensalt()));

        var httpGet = new HttpGet(environmentData.baseUrl() + TELEMETRY_ENDPOINT_URL);
        httpGet.addHeader("Authorization", "Code " + TEST_TOKEN);
        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
    }

    @Test
    void testEndpointReturnsExpectedFormat() throws IOException {
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.METRICS_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_ENABLED_KEY, true);
        backdoorControl.applicationSettings().setSetting(ApplicationSettingKeys.OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY, null);

        var httpGet = new HttpGet(environmentData.baseUrl() + TELEMETRY_ENDPOINT_URL);
        var response = httpClient.execute(httpGet);

        assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
        try (var outputStream = new ByteArrayOutputStream()) {
            response.getEntity().writeTo(outputStream);

            var responseText = outputStream.toString(Charsets.UTF_8);

            // unfortunately there is no parser or validator in Java
            assertThat(responseText, endsWith("# EOF\n"));
            assertThat(responseText, containsString("# TYPE rarog_metrics_snapshot_export_seconds histogram\n"));
            assertThat(responseText, containsString("rarog_metrics_snapshot_export_seconds_bucket{le=\"0.01\"}"));
        }
    }
}
