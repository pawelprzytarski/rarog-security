package eu.rarogsoftware.rarog.platform.test;

import eu.rarogsoftware.rarog.platform.backdoor.control.BackdoorControl;
import eu.rarogsoftware.rarog.platform.junit.extensions.FuncTestExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.inject.Inject;
import java.io.IOException;

import static eu.rarogsoftware.rarog.platform.test.ApplicationSettingKeys.SYSTEM_ID_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@ExtendWith(FuncTestExtension.class)
public class TestApplicationSettingsBackdoorIsWorking {
    private static final String TEST_SETTING_VALUE = "SOME_VERY_VERY_IMPORTANT_VALUE_YOU_CANT_EVEN_IMAGINE_HOW_IMPORTANT";
    private static final String TEST_SETTING_NAME = "NewRandomSettingThatIsNotExistingEverSoWhatEver";
    @Inject
    BackdoorControl backdoorControl;

    @Test
    void testBasicSettingsAreSet() throws IOException {
        var serverId = backdoorControl.applicationSettings().getSetting(SYSTEM_ID_KEY);
        assertThat(serverId.value(), equalTo("TestInstanceId"));
    }

    @Test
    void testSaveApplicationSetting() throws IOException {
        backdoorControl.applicationSettings().setSetting(TEST_SETTING_NAME, TEST_SETTING_VALUE);
        var retrievedSetting = backdoorControl.applicationSettings().getSetting(TEST_SETTING_NAME);
        assertThat(retrievedSetting.value(), equalTo(TEST_SETTING_VALUE));
    }
}
