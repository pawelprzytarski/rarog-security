package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.dev;

import eu.rarogsoftware.rarog.platform.api.plugins.web.ResourceResolverDescriptor;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.PluginAutoConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

import java.io.File;

@PluginAutoConfiguration
public class FrontendLiveReloadAutoconfigure {

    @ConditionalOnProperty(name = {"rarog.plugin.debug.resources.path", "rarog.plugin.key"})
    @Bean
    ResourceResolverDescriptor debugResourceResolver(@Value("${rarog.plugin.debug.resources.path}") String path,
                                                     @Value("${rarog.plugin.key}") String pluginKey) {
        var resourcesPrefix = "p/" + pluginKey + "/debug/";
        var slashEndedPath = path.endsWith(File.separator) ? path : (path + File.separator);
        return new ResourceResolverDescriptor(new DebugResourcesResourceResolver(resourcesPrefix, slashEndedPath));
    }

}
