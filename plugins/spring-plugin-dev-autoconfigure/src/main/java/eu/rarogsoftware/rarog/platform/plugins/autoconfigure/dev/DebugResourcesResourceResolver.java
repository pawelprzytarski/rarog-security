package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.dev;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.resource.ResourceResolver;
import org.springframework.web.servlet.resource.ResourceResolverChain;

import java.util.List;

class DebugResourcesResourceResolver implements ResourceResolver {
    private final String resourcesPrefix;
    private final String slashEndedPath;

    public DebugResourcesResourceResolver(String resourcesPrefix, String slashEndedPath) {
        this.resourcesPrefix = resourcesPrefix;
        this.slashEndedPath = slashEndedPath;
    }

    @Override
    public Resource resolveResource(HttpServletRequest request, String requestPath, List<? extends Resource> locations, ResourceResolverChain chain) {
        if (requestPath.startsWith(resourcesPrefix)) {
            return new FileSystemResource(slashEndedPath + requestPath.substring(resourcesPrefix.length()));
        }
        return chain.resolveResource(request, requestPath, locations);
    }

    @Override
    public String resolveUrlPath(String resourcePath, List<? extends Resource> locations, ResourceResolverChain chain) {
        if (resourcePath.startsWith(resourcesPrefix)) {
            return resourcePath;
        }
        return chain.resolveUrlPath(resourcePath, locations);
    }
}
