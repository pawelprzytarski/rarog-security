package eu.rarogsoftware.rarog.platform.plugins.autoconfigure;

import org.springframework.boot.autoconfigure.AutoConfigurationImportSelector;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PluginAutoConfigurationImportSelector extends AutoConfigurationImportSelector {
    private static final String PROPERTY_NAME_AUTOCONFIGURE_EXCLUDE = "rarog.autoconfigure.exclude";
    private static final String DISABLED_AUTOCONFIG_PROPERTY = "rarog.autoconfigure.disabled";

    @Override
    protected boolean isEnabled(AnnotationMetadata metadata) {
        return !Boolean.parseBoolean(System.getProperty(DISABLED_AUTOCONFIG_PROPERTY, "false"));
    }

    @Override
    protected Class<?> getAnnotationClass() {
        return EnablePluginAutoconfiguration.class;
    }

    @Override
    protected List<String> getCandidateConfigurations(AnnotationMetadata metadata, AnnotationAttributes attributes) {
        var configurations = PluginImportCandidates.load(PluginAutoConfiguration.class, getBeanClassLoader());
        Assert.notEmpty(configurations,
                "No auto configuration classes found in "
                        + "META-INF/rarog/eu.rarogsoftware.rarog.platform.plugins.autoconfigure.PluginAutoConfiguration.imports. If you "
                        + "are using a custom packaging, make sure that file is correct.");
        return configurations;
    }

    @Override
    protected List<String> getExcludeAutoConfigurationsProperty() {
        Environment environment = getEnvironment();
        if (environment == null) {
            return Collections.emptyList();
        }
        if (environment instanceof ConfigurableEnvironment) {
            Binder binder = Binder.get(environment);
            return binder.bind(PROPERTY_NAME_AUTOCONFIGURE_EXCLUDE, String[].class).map(Arrays::asList)
                    .orElse(Collections.emptyList());
        }
        String[] excludes = environment.getProperty(PROPERTY_NAME_AUTOCONFIGURE_EXCLUDE, String[].class);
        return (excludes != null) ? Arrays.asList(excludes) : Collections.emptyList();
    }
}
