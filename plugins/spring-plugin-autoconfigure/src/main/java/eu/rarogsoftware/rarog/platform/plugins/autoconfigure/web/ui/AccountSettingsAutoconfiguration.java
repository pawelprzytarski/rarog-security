package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web.ui;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.json.JsonMapper;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AccountSettingsMenuDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AccountSettingsMenuElement;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.PluginAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.util.List;

@PluginAutoConfiguration
public class AccountSettingsAutoconfiguration {
    @ConditionalOnMissingBean
    @ConditionalOnBean(AccountSettingsMenuElement.class)
    @Bean
    AccountSettingsMenuDescriptor accountSettingsMenuDescriptor(List<AccountSettingsMenuElement> elements) {
        return new AccountSettingsMenuDescriptor(elements);
    }

    @ConditionalOnMissingBean(value = {AccountSettingsMenuDescriptor.class, AccountSettingsMenuElement.class})
    @ConditionalOnClass(JsonMapper.class)
    @ConditionalOnResource(resources = "classpath:descriptors/accountSettingsMenuElements.json")
    @Bean
    AccountSettingsMenuDescriptor accountSettingsMenuDescriptorFromResource(ResourceLoader resourceLoader) throws IOException {
        var resource = resourceLoader.getResource("classpath:descriptors/accountSettingsMenuElements.json");
        var mapper = new JsonMapper();
        return new AccountSettingsMenuDescriptor(mapper.readValue(resource.getInputStream(), new TypeReference<>() {
        }));
    }
}
