package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web.ui;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.json.JsonMapper;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AdminPanelItem;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AdminPanelItemsDescriptor;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.PluginAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.util.List;

@PluginAutoConfiguration
public class AdminPanelAutoConfiguration {
    public static final String DESCRIPTOR_LOCATION = "classpath:descriptors/adminPanelItems.json";

    @ConditionalOnMissingBean
    @ConditionalOnBean(AdminPanelItem.class)
    @Bean
    AdminPanelItemsDescriptor adminPanelItemsDescriptor(List<AdminPanelItem> elements) {
        return new AdminPanelItemsDescriptor(elements);
    }

    @ConditionalOnMissingBean(value = {AdminPanelItemsDescriptor.class, AdminPanelItem.class})
    @ConditionalOnClass(JsonMapper.class)
    @ConditionalOnResource(resources = DESCRIPTOR_LOCATION)
    @Bean
    AdminPanelItemsDescriptor adminPanelItemsDescriptorFromResource(ResourceLoader resourceLoader) throws IOException {
        var resource = resourceLoader.getResource(DESCRIPTOR_LOCATION);
        var mapper = new JsonMapper();
        return new AdminPanelItemsDescriptor(mapper.readValue(resource.getInputStream(), new TypeReference<>() {
        }));
    }
}
