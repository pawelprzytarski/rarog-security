package eu.rarogsoftware.rarog.platform.plugins.autoconfigure;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * Indicates that a class provides configuration that can be automatically applied by
 * Rarog. Auto-configuration classes are regular
 * {@link Configuration} with the exception that
 * {@literal Configuration#proxyBeanMethods() proxyBeanMethods} is always {@code false}.
 * They are located using {@link PluginImportCandidates}.
 * <p>
 * This annotation is Rarog plugin counterpart for {@link AutoConfiguration}.
 * Rarog defines its own auto-configuration functionality to avoid collision with
 * Spring Boot, which could cause a lot hard to debug and understand problems.
 * <p>
 * Generally auto-configuration classes are marked as {@link Conditional @Conditional}
 * (most often using {@link ConditionalOnClass @ConditionalOnClass} and
 * {@link ConditionalOnMissingBean @ConditionalOnMissingBean} annotations).
 *
 * @see EnablePluginAutoconfiguration
 * @see AutoConfigureBefore
 * @see AutoConfigureAfter
 * @see Conditional
 * @see ConditionalOnClass
 * @see ConditionalOnMissingBean
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Configuration(proxyBeanMethods = false)
@AutoConfigureBefore
@AutoConfigureAfter
public @interface PluginAutoConfiguration {
    /**
     * Explicitly specify the name of the Spring bean definition associated with the
     * {@code @AutoConfiguration} class. If left unspecified (the common case), a bean
     * name will be automatically generated.
     * <p>
     * The custom name applies only if the {@code @AutoConfiguration} class is picked up
     * via component scanning or supplied directly to an
     * {@link AnnotationConfigApplicationContext}. If the {@code @AutoConfiguration} class
     * is registered as a traditional XML bean definition, the name/id of the bean element
     * will take precedence.
     *
     * @return the explicit component name, if any (or empty String otherwise)
     * @see AnnotationBeanNameGenerator
     */
    @AliasFor(annotation = Configuration.class)
    String value() default "";

    /**
     * The auto-configure classes that should have not yet been applied.
     *
     * @return the classes
     */
    @AliasFor(annotation = AutoConfigureBefore.class, attribute = "value")
    Class<?>[] before() default {};

    /**
     * The names of the auto-configure classes that should have not yet been applied.
     *
     * @return the class names
     */
    @AliasFor(annotation = AutoConfigureBefore.class, attribute = "name")
    String[] beforeName() default {};

    /**
     * The auto-configure classes that should have already been applied.
     *
     * @return the classes
     */
    @AliasFor(annotation = AutoConfigureAfter.class, attribute = "value")
    Class<?>[] after() default {};

    /**
     * The names of the auto-configure classes that should have already been applied.
     *
     * @return the class names
     */
    @AliasFor(annotation = AutoConfigureAfter.class, attribute = "name")
    String[] afterName() default {};

}
