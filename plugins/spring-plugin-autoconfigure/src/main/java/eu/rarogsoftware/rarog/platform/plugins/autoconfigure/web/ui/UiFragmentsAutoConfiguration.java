package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web.ui;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.json.JsonMapper;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AccountSettingsMenuElement;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.UiFragment;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.UiFragmentsDescriptor;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.PluginAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.util.List;

@PluginAutoConfiguration
public class UiFragmentsAutoConfiguration {
    @ConditionalOnMissingBean
    @ConditionalOnBean(AccountSettingsMenuElement.class)
    @Bean
    UiFragmentsDescriptor uiFragmentsDescriptor(List<UiFragment> elements) {
        return new UiFragmentsDescriptor(elements);
    }

    @ConditionalOnMissingBean(value = {UiFragmentsDescriptor.class, UiFragment.class})
    @ConditionalOnClass(JsonMapper.class)
    @ConditionalOnResource(resources = "classpath:descriptors/uiFragments.json")
    @Bean
    UiFragmentsDescriptor uiFragmentsDescriptorFromResource(ResourceLoader resourceLoader) throws IOException {
        var resource = resourceLoader.getResource("classpath:descriptors/uiFragments.json");
        var mapper = new JsonMapper();
        return new UiFragmentsDescriptor(mapper.readValue(resource.getInputStream(), new TypeReference<>() {
        }));
    }
}
