package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.events;

import eu.rarogsoftware.rarog.platform.api.plugins.events.EventListener;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventListenersDescriptor;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.PluginAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

import java.util.HashSet;
import java.util.List;


/**
 * Automatically exports {@link EventListener} components that are annotated, unless explicit configuration is defined.
 */
@PluginAutoConfiguration
public class EventListenersAutoconfiguration {
    @ConditionalOnBean(value = EventListener.class)
    @ConditionalOnMissingBean(EventListenersDescriptor.class)
    @Bean
    public EventListenersDescriptor eventListenersDescriptor(List<EventListener> eventListeners) {
        return new EventListenersDescriptor(new HashSet<>(eventListeners));
    }
}
