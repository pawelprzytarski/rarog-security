package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web;

import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginSecurityFilterMapping;
import jakarta.servlet.Filter;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface SecurityFilterMapping {
    PluginSecurityFilterMapping.Placement placement() default PluginSecurityFilterMapping.Placement.AT_END;

    String relativeFilterName() default "";

    Class<? extends Filter> relativeFilter() default Filter.class;
}
