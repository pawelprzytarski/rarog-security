package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.config;

import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SettingsInitializer;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.PluginAutoConfiguration;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;

import java.util.List;

/**
 * Autoconfiguration that automatically finds all {@link SettingsInitializer} instances
 * and initializes {@link ApplicationSettings} with them.
 */
@PluginAutoConfiguration
public class ApplicationSettingsInitializerAutoconfigure {
    @ConditionalOnBean(SettingsInitializer.class)
    @Bean
    SettingsInitializerProcessor settingsInitializerProcessor(List<SettingsInitializer> initializers,
                                                              @ComponentImport ApplicationSettings applicationSettings) {
        return new SettingsInitializerProcessor(initializers, applicationSettings);
    }

    static class SettingsInitializerProcessor implements InitializingBean {
        private final List<SettingsInitializer> initializers;
        private final ApplicationSettings applicationSettings;

        public SettingsInitializerProcessor(List<SettingsInitializer> initializers, ApplicationSettings applicationSettings) {
            this.initializers = initializers;
            this.applicationSettings = applicationSettings;
        }

        @Override
        public void afterPropertiesSet() {
            initializers.forEach(initializer -> initializer.initialize(applicationSettings));
        }
    }
}
