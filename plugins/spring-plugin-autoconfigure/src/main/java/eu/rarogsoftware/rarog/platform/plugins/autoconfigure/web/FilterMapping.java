package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web;

import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginFilterMapping;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginFilterMappingDescriptor;
import jakarta.servlet.DispatcherType;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * Helper annotation that marks {@link jakarta.servlet.Filter} as filter for automatic import when put on Bean or Component.
 * This annotation is automatically translated to {@link PluginFilterMapping}
 * and exported with {@link PluginFilterMappingDescriptor}.
 *
 * @see PluginFilterMapping
 * @see JakartaServletsAutoconfiguration
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
@interface FilterMapping {
    /**
     * Value for {@link PluginFilterMapping#dispatchers()}
     */
    DispatcherType[] dispatchers() default {DispatcherType.REQUEST};

    /**
     * Value for {@link PluginFilterMapping#urlPatterns()}
     */
    String[] urlMappings();
}
