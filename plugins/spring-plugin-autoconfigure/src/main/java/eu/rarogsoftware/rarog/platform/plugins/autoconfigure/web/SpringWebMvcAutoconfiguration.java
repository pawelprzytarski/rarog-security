package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web;

import eu.rarogsoftware.rarog.platform.api.i18n.I18NextResourceDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.templates.TemplateResolverDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.templates.ViewResolverDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.HandlerMappingDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.ResourceResolverDescriptor;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsDescriptor;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.PluginAutoConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.resource.ResourceResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

@PluginAutoConfiguration
public class SpringWebMvcAutoconfiguration {

    @ConditionalOnProperty(name = "rarog.plugin.web.namespace")
    @ConditionalOnMissingBean(RequestMappingHandlerMapping.class)
    @Bean("restRequestMappingHandlerMapping")
    RequestMappingHandlerMapping requestMappingHandlerMapping(
            @Value("${rarog.plugin.web.namespace}") String namespace,
            @Value("${rarog.plugin.web.rest.version:1.0}") String restVersion,
            @Value("${rarog.plugin.web.prefix.servlet:/plugin/") String servletPrefix,
            @Value("${rarog.plugin.web.prefix.rest:/rest/}") String restPrefix
    ) {
        var requestMappingHandlerMapping = new RequestMappingHandlerMapping();
        Map<String, Predicate<Class<?>>> restPrefixesConfig = new HashMap<>();
        restPrefixesConfig.put(restPrefix + namespace + "/" + restVersion + "/", clazz1 -> MergedAnnotations.from(clazz1.getAnnotations()).isPresent(RestController.class));
        Map<String, Predicate<Class<?>>> mappingPrefixesConfig = new HashMap<>(restPrefixesConfig);
        mappingPrefixesConfig.put(servletPrefix + namespace + "/", clazz -> !MergedAnnotations.from(clazz.getAnnotations()).isPresent(RestController.class));
        requestMappingHandlerMapping.setPathPrefixes(mappingPrefixesConfig);
        return requestMappingHandlerMapping;
    }

    @ConditionalOnBean(HandlerMapping.class)
    @ConditionalOnProperty(name = "rarog.plugin.web.namespace")
    @ConditionalOnMissingBean(HandlerMappingDescriptor.class)
    @Bean
    public HandlerMappingDescriptor handlerMappingDescriptor(List<HandlerMapping> handlerMappings, @Value("${rarog.plugin.web.namespace}") String namespace) {
        return new HandlerMappingDescriptor("/%s/".formatted(namespace), handlerMappings, true);
    }

    @Conditional(I18nAvailableCondition.class)
    @ConditionalOnMissingBean(I18NextResourceDescriptor.class)
    @Bean
    public I18NextResourceDescriptor i18NextResourceDescriptor() {
        return new I18NextResourceDescriptor();
    }

    @ConditionalOnBean(ITemplateResolver.class)
    @ConditionalOnMissingBean(TemplateResolverDescriptor.class)
    @Bean
    public TemplateResolverDescriptor pluginTemplateResolver(List<ITemplateResolver> templateResolvers) {
        return new TemplateResolverDescriptor(() -> templateResolvers);
    }

    @ConditionalOnBean(ViewResolver.class)
    @ConditionalOnMissingBean(ViewResolverDescriptor.class)
    @Bean
    public ViewResolverDescriptor viewResolverDescriptor(List<ViewResolver> templateResolvers) {
        return new ViewResolverDescriptor(() -> templateResolvers);
    }

    @ConditionalOnBean(value = ResourceResolver.class, name = "resourceResolver")
    @ConditionalOnMissingBean(ResourceResolverDescriptor.class)
    @Bean
    public ResourceResolverDescriptor resourceResolverDescriptor(ResourceResolver resourceResolver) {
        return new ResourceResolverDescriptor(resourceResolver);
    }

    @ConditionalOnMissingBean(DynamicAssetsDescriptor.class)
    @ConditionalOnProperty(name = {"rarog.plugin.assets.enabled"}, havingValue = "true")
    @Bean
    public DynamicAssetsDescriptor dynamicAssetsDescriptor(@Value("${rarog.plugin.assets.path:}") String assetsPath) {
        if (StringUtils.hasText(assetsPath)) {
            return new DynamicAssetsDescriptor(assetsPath);
        }
        return new DynamicAssetsDescriptor();
    }

    public static class I18nAvailableCondition extends AnyNestedCondition {

        public I18nAvailableCondition() {
            super(ConfigurationPhase.REGISTER_BEAN);
        }

        @ConditionalOnProperty(name = "rarog.plugin.web.i18n.location")
        static class PropertyExists {
            PropertyExists() {
            }
        }

        @ConditionalOnResource(resources = "classpath:i18n/")
        static class ResourcesExist {
            ResourcesExist() {
            }
        }
    }
}
