package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web;

import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.PluginServletMappingDescriptor;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;
/**
 * Helper annotation that marks {@link jakarta.servlet.Servlet} as filter for automatic import when put on Bean or Component.
 * This annotation is automatically translated to {@link eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.ServletMapping}
 * and exported with {@link PluginServletMappingDescriptor}.
 *
 * @see eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.ServletMapping
 * @see JakartaServletsAutoconfiguration
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface ServletMapping {
    /**
     * Value for {@link eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.ServletMapping#pathMapping()}
     */
    String mapping();
    /**
     * Value for {@link eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.ServletMapping#order()}
     */
    int order() default eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.ServletMapping.DEFAULT_ORDER;
}
