/*
 * Copyright 2012-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.rarogsoftware.rarog.platform.plugins.autoconfigure;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.UrlResource;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Contains {@code @Configuration} import candidates, usually auto-configurations.
 * The {@link #load(Class, ClassLoader)} method can be used to discover the import
 * candidates.
 * <p>
 * This class is based on {@link org.springframework.boot.context.annotation.ImportCandidates}
 * from spring boot, but it adjusted to work with Rarog plugins.
 */
public final class PluginImportCandidates {

    private static final String LOCATION = "META-INF/rarog/%s.imports";

    private static final String COMMENT_START = "#";

    private PluginImportCandidates() {
    }

    /**
     * Loads the names of import candidates from the classpath.
     * <p>
     * The names of the import candidates are stored in files named
     * {@code META-INF/rarog/full-qualified-annotation-name.imports} on the classpath.
     * Every line contains the full qualified name of the candidate class. Comments are
     * supported using the # character.
     *
     * @param annotation  annotation to load
     * @param classLoader class loader to use for loading
     * @return list of names of annotated classes
     */
    public static List<String> load(Class<?> annotation, ClassLoader classLoader) {
        Assert.notNull(annotation, "'annotation' must not be null");
        var classLoaderToUse = decideClassloader(classLoader);
        var urls = findUrlsInClasspath(classLoaderToUse, String.format(LOCATION, annotation.getName()));
        return StreamSupport.stream(IteratorUtils.asIterable(urls.asIterator()).spliterator(), false)
                .flatMap(PluginImportCandidates::readCandidateConfigurations)
                .toList();
    }

    private static ClassLoader decideClassloader(ClassLoader classLoader) {
        if (classLoader == null) {
            return PluginImportCandidates.class.getClassLoader();
        }
        return classLoader;
    }

    private static Enumeration<URL> findUrlsInClasspath(ClassLoader classLoader, String location) {
        try {
            return classLoader.getResources(location);
        } catch (IOException ex) {
            throw new IllegalArgumentException("Failed to load configurations from location [" + location + "]", ex);
        }
    }

    private static Stream<String> readCandidateConfigurations(URL url) {
        try (var inputStream = new UrlResource(url).getInputStream()) {
            return IOUtils.readLines(inputStream, StandardCharsets.UTF_8)
                    .stream()
                    .map(PluginImportCandidates::stripComment)
                    .filter(StringUtils::hasText);
        } catch (IOException ex) {
            throw new IllegalArgumentException("Unable to load configurations from location [" + url + "]", ex);
        }
    }

    private static String stripComment(String line) {
        int commentStart = line.indexOf(COMMENT_START);
        if (commentStart == -1) {
            return line;
        }
        return line.substring(0, commentStart);
    }

}
