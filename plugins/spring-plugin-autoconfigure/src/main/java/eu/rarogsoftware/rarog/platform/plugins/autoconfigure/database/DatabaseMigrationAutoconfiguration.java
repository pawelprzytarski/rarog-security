package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.database;

import eu.rarogsoftware.rarog.platform.api.plugins.database.DatabaseMigrationsDescriptor;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.PluginAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.context.annotation.Bean;

@PluginAutoConfiguration
public class DatabaseMigrationAutoconfiguration {
    @ConditionalOnMissingBean(DatabaseMigrationsDescriptor.class)
    @ConditionalOnResource(resources = "classpath:migrations/changelog.xml")
    @Bean
    public DatabaseMigrationsDescriptor databaseMigrationsDescriptor() {
        return new DatabaseMigrationsDescriptor("migrations/changelog.xml");
    }
}
