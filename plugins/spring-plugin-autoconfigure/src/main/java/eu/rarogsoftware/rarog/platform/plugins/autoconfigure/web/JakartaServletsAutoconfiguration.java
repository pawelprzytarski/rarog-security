package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web;

import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginFilterMapping;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginFilterMappingDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginSecurityFilterMapping;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginSecurityFilterMappingDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.PluginServletMappingDescriptor;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.PluginAutoConfiguration;
import jakarta.servlet.Filter;
import jakarta.servlet.http.HttpServlet;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Set;

/**
 * Automatically exports {@link jakarta.servlet.Servlet} and {@link Filter} components that are annotated
 * with {@link ServletMapping} and {@link FilterMapping}.
 */
@PluginAutoConfiguration
public class JakartaServletsAutoconfiguration {

    @ConditionalOnBean(value = HttpServlet.class, annotation = ServletMapping.class)
    @ConditionalOnMissingBean(PluginServletMappingDescriptor.class)
    @Bean
    PluginServletMappingDescriptor servletMappingDescriptorBeanFactory(List<HttpServlet> httpServlets) {

        return new PluginServletMappingDescriptor(
                httpServlets.stream()
                        .filter(servlet -> servlet.getClass().getAnnotation(ServletMapping.class) != null)
                        .map(servlet -> {
                            var annotation = servlet.getClass().getAnnotation(ServletMapping.class);
                            return new eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.ServletMapping(annotation.mapping(), servlet);
                        })
                        .toList()
        );
    }

    @ConditionalOnBean(value = Filter.class, annotation = FilterMapping.class)
    @ConditionalOnMissingBean(PluginFilterMappingDescriptor.class)
    @Bean
    PluginFilterMappingDescriptor pluginFilterMappingDescriptor(List<Filter> filters) {
        return new PluginFilterMappingDescriptor(
                filters.stream()
                        .filter(filter -> filter.getClass().getAnnotation(FilterMapping.class) != null)
                        .map(filter -> {
                            var annotation = filter.getClass().getAnnotation(FilterMapping.class);
                            return new PluginFilterMapping(filter, Set.of(annotation.dispatchers()), List.of(annotation.urlMappings()));
                        })
                        .toList()
        );
    }

    @ConditionalOnBean(value = Filter.class, annotation = SecurityFilterMapping.class)
    @ConditionalOnMissingBean(PluginSecurityFilterMappingDescriptor.class)
    @Bean
    PluginSecurityFilterMappingDescriptor pluginSecurityFilterMappingDescriptor(List<Filter> filters) {
        return new PluginSecurityFilterMappingDescriptor(
                filters.stream()
                        .filter(filter -> filter.getClass().getAnnotation(SecurityFilterMapping.class) != null)
                        .map(filter -> {
                            var annotation = filter.getClass().getAnnotation(SecurityFilterMapping.class);
                            return StringUtils.hasText(annotation.relativeFilterName())
                                    ? new PluginSecurityFilterMapping(
                                    filter,
                                    annotation.placement(),
                                    annotation.relativeFilterName()
                            )
                                    : new PluginSecurityFilterMapping(
                                    filter,
                                    annotation.placement(),
                                    annotation.relativeFilter()
                            );
                        })
                        .toList()
        );
    }
}
