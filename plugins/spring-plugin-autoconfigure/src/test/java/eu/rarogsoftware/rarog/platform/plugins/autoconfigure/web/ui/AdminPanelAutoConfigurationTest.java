package eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web.ui;

import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AdminPanelItem;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AdminPanelItemsDescriptor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.util.InMemoryResource;

import java.io.IOException;
import java.util.Arrays;

import static eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web.ui.AdminPanelAutoConfiguration.DESCRIPTOR_LOCATION;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AdminPanelAutoConfigurationTest {
    AdminPanelAutoConfiguration adminPanelAutoConfiguration;

    @BeforeEach
    void setUp() {
        adminPanelAutoConfiguration = new AdminPanelAutoConfiguration();
    }


    @Test
    void testConfigurationIsProperlyReadFromJson() throws IOException {
        var jsonWithConfig = """
                [
                    {
                      "key": "Test1",
                      "category": "General",
                      "name": "Test name",
                      "resource": "test resource",
                      "embedded": true,
                      "keywords": "keywords"
                    },
                    {
                      "key": "Test2",
                      "category": "General",
                      "name": "Second",
                      "resource": "test resource",
                      "embedded": false,
                      "navigationOnly": true,
                      "keywords": null
                    },
                    {
                      "key": "Test3",
                      "category": "Other",
                      "name": "Special",
                      "resource": null,
                      "embedded": true,
                      "navigationOnly": true
                    }
                ]""";
        var expected = new AdminPanelItemsDescriptor(Arrays.asList(
                new AdminPanelItem("Test1", "General", "Test name", "test resource", true, false, "keywords"),
                new AdminPanelItem("Test2", "General", "Second", "test resource", false, true, null),
                new AdminPanelItem("Test3", "Other", "Special", null, true, true, null)
        ));
        var resourceLoader = mock(ResourceLoader.class);
        when(resourceLoader.getResource(DESCRIPTOR_LOCATION)).thenReturn(new InMemoryResource(jsonWithConfig));

        var result = adminPanelAutoConfiguration.adminPanelItemsDescriptorFromResource(resourceLoader);

        assertThat(result).isEqualTo(expected);
    }
}