package eu.rarogsoftware.rarog.platform.plugins.autoconfigure;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PluginImportCandidatesTest {
    @Test
    void testDetectAutoConfigurationsDefinedInAutoconfigurationModule() {
        var results = PluginImportCandidates.load(PluginAutoConfiguration.class, PluginAutoConfiguration.class.getClassLoader());
        assertThat(results)
                .hasSizeGreaterThanOrEqualTo(3)
                .contains("eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web.SpringWebMvcAutoconfiguration",
                        "eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web.JakartaServletsAutoconfiguration",
                        "eu.rarogsoftware.rarog.platform.plugins.autoconfigure.config.ApplicationSettingsInitializerAutoconfigure");
    }
}