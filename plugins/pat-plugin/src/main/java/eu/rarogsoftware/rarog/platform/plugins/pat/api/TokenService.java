package eu.rarogsoftware.rarog.platform.plugins.pat.api;

import eu.rarogsoftware.rarog.platform.api.user.management.AppUser;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Service to create and verify token used to authorize to application.
 */
public interface TokenService {
    String createUserToken(StandardUser user, String name, Instant expiration, List<String> grantedAuthorities) throws TokenCreationException;

    String createAppToken(AppUser user, String name, Instant expiration, List<String> grantedAuthorities) throws TokenCreationException;

    List<Token> getTokensForUser(long userId);

    List<Token> getTokenForApp(String appName);

    DecodedToken verifyToken(String token) throws TokenVerificationException;

    Optional<Token> getTokenById(Long tokenId);

    void revokeToken(Token token);

    enum PrincipalType {
        USER,
        APP
    }

    record Token(long id, String name, String appName, Long userId, String secret, Instant expiration,
                 List<String> authorities) {
    }

    record EncodedToken(String principal, PrincipalType type, Long identifier, String secret) {
    }

    record DecodedToken(Object principal, PrincipalType type, List<String> authorities) {
    }
}
