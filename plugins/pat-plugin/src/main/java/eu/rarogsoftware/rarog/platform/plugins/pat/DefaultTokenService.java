package eu.rarogsoftware.rarog.platform.plugins.pat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import eu.rarogsoftware.commons.database.connection.DatabaseConnectionException;
import eu.rarogsoftware.commons.utils.RandomService;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.task.ScheduleConfig;
import eu.rarogsoftware.rarog.platform.api.task.TaskManager;
import eu.rarogsoftware.rarog.platform.api.user.management.AppUser;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.api.user.management.UserManagementException;
import eu.rarogsoftware.rarog.platform.api.user.management.UserManager;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SettingsInitializer;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenCreationException;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenService;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenVerificationException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static eu.rarogsoftware.rarog.platform.plugins.pat.api.SettingKeys.APP_KEYS_ENABLED_KEY;
import static eu.rarogsoftware.rarog.platform.plugins.pat.api.SettingKeys.USER_KEYS_ENABLED_KEY;

@Component
@ExportComponent
public class DefaultTokenService implements TokenService, SettingsInitializer {
    private static final String INVALID_TOKEN_SECRET = "Invalid token secret";
    private static final String TOKEN_EXPIRED = "Token expired";
    private static final String TOKEN_DOES_NOT_EXIST = "Token does not exist";
    private static final String DISABLED_LOGIN = "Login with tokens is disabled by system administrator";
    private final Logger logger = LoggerFactory.getLogger(DefaultTokenService.class);
    private final ObjectMapper objectMapper = new JsonMapper();
    private final ApplicationSettings applicationSettings;
    private final UserManager userManager;
    private final SecureRandom secureRandom;
    private final Clock clock;
    private final TokenStore tokenStore;

    public DefaultTokenService(@ComponentImport ApplicationSettings applicationSettings,
                               @ComponentImport UserManager userManager,
                               @ComponentImport TaskManager taskManager,
                               TokenStore tokenStore,
                               RandomService randomService,
                               Clock clock) {
        this.applicationSettings = applicationSettings;
        this.userManager = userManager;
        this.tokenStore = tokenStore;
        try {
            this.secureRandom = randomService.getSecureRandom();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
        this.clock = clock;
        taskManager.scheduleTask(this::cleanupOutdatedTokens, ScheduleConfig.<Void>builder()
                .initialDelay(Duration.ofHours(1))
                .period(Duration.ofHours(1))
                .build());
    }

    private void cleanupOutdatedTokens() {
        var removedTokens = tokenStore.cleanupOutdatedTokens(clock.instant());
        logger.info("Removed {} outdated tokens", removedTokens);
    }

    @Override
    public String createUserToken(StandardUser user, String name, Instant expiration, List<String> grantedAuthorities) throws TokenCreationException {
        try {
            if (!applicationSettings.isTrue(USER_KEYS_ENABLED_KEY)) {
                throw new TokenCreationException(DISABLED_LOGIN);
            }
            checkExpiration(expiration);
            logger.debug("Creating new token {} for user {} with expiration {} for authorities {}", name, user, expiration, grantedAuthorities);
            var secret = generateNewSecret();
            var hashedSecret = BCrypt.hashpw(secret, BCrypt.gensalt());
            var id = tokenStore.saveToken(name, expiration, user.getId(), null, grantedAuthorities, hashedSecret);
            return encodeToken(new EncodedToken(user.getUsername(), PrincipalType.USER, id, secret));
        } catch (JsonProcessingException | DatabaseConnectionException e) {
            throw new TokenCreationException(e);
        }
    }

    private String encodeToken(EncodedToken encodedToken) throws JsonProcessingException {
        return new String(Base64.getEncoder().encode(objectMapper.writeValueAsString(encodedToken).getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
    }

    private String generateNewSecret() {
        var stringBuilder = new StringBuilder();
        for (int i = 0; i < 64; i++) {
            stringBuilder.appendCodePoint(secureRandom.nextInt(32, 127));
        }
        return stringBuilder.toString();
    }

    private void checkExpiration(Instant expiration) throws TokenCreationException {
        if (expiration.isBefore(clock.instant())) {
            throw new TokenCreationException("Expiration is set in past");
        }
    }

    @Override
    public String createAppToken(AppUser user, String name, Instant expiration, List<String> grantedAuthorities) throws TokenCreationException {
        try {
            if (!applicationSettings.isTrue(APP_KEYS_ENABLED_KEY)) {
                throw new TokenCreationException(DISABLED_LOGIN);
            }
            checkExpiration(expiration);
            logger.debug("Creating new token {} for app {} with expiration {} for authorities {}", name, user, expiration, grantedAuthorities);
            var secret = generateNewSecret();
            var hashedSecret = BCrypt.hashpw(secret, BCrypt.gensalt());
            var id = tokenStore.saveToken(name, expiration, null, user.getAppName(), grantedAuthorities, hashedSecret);
            return encodeToken(new EncodedToken(user.getAppName(), PrincipalType.APP, id, secret));
        } catch (JsonProcessingException | DatabaseConnectionException e) {
            throw new TokenCreationException(e);
        }
    }

    @Override
    public List<Token> getTokensForUser(long userId) {
        return tokenStore.getTokensForUser(userId);
    }

    @Override
    public List<Token> getTokenForApp(String appName) {
        return tokenStore.getTokenForApp(appName);
    }

    @Override
    public DecodedToken verifyToken(String token) throws TokenVerificationException {
        if (StringUtils.isBlank(token)) {
            throw new TokenVerificationException("Token cannot be empty");
        }
        try {
            var json = new String(Base64.getDecoder().decode(token), StandardCharsets.UTF_8);
            var decodedToken = objectMapper.readValue(json, EncodedToken.class);
            var tokenOptional = tokenStore.getTokenById(decodedToken.identifier());
            if (tokenOptional.isEmpty()) {
                throw new TokenVerificationException(TOKEN_DOES_NOT_EXIST);
            }
            var dbToken = tokenOptional.get();
            logger.debug("Verifying token {}", dbToken.id());
            verifyTokenLoginEnabled(dbToken);
            verifyTokenWithDatabaseStoredValue(decodedToken, dbToken);
            if (dbToken.userId() != null) {
                return verifyUserToken(decodedToken, dbToken);
            } else {
                return verifyAppToken(decodedToken, dbToken);
            }
        } catch (JsonProcessingException | IllegalArgumentException e) {
            throw new TokenVerificationException("Failed to read token json", e);
        }
    }

    @Override
    public Optional<Token> getTokenById(Long tokenId) {
        return tokenStore.getTokenById(tokenId);
    }

    @Override
    public void revokeToken(Token token) {
        tokenStore.removeToken(token);
    }

    private void verifyTokenLoginEnabled(Token dbToken) throws TokenVerificationException {
        if (dbToken.userId() != null) {
            if (!applicationSettings.isTrue(USER_KEYS_ENABLED_KEY)) {
                throw new TokenVerificationException(DISABLED_LOGIN);
            }
        } else {
            if (!applicationSettings.isTrue(APP_KEYS_ENABLED_KEY)) {
                throw new TokenVerificationException(DISABLED_LOGIN);
            }
        }
    }

    private DecodedToken verifyAppToken(EncodedToken decodedEncodedToken, Token dbToken) throws TokenVerificationException {
        if (!Objects.equals(dbToken.appName(), decodedEncodedToken.principal())) {
            throw new TokenVerificationException(INVALID_TOKEN_SECRET);
        }
        List<String> authorities = getAuthorities(dbToken);
        return new DecodedToken(AppUser.builder()
                .appName(decodedEncodedToken.principal())
                .enabled(true)
                .authorities(authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet()))
                .build(),
                PrincipalType.APP, authorities);
    }

    private List<String> getAuthorities(Token dbToken) {
        var authorities = dbToken.authorities();
        if (authorities == null) {
            return Collections.emptyList();
        }
        return authorities;
    }

    private DecodedToken verifyUserToken(EncodedToken decodedEncodedToken, Token dbToken) throws TokenVerificationException {
        long userId = dbToken.userId();
        try {
            var user = userManager.getUser(userId);
            if (!Objects.equals(user.getUsername(), decodedEncodedToken.principal())) {
                throw new TokenVerificationException(INVALID_TOKEN_SECRET);
            }
            var updatedAuthorities = getCurrentAuthorities(dbToken, user);
            return new DecodedToken(user, PrincipalType.USER, updatedAuthorities);
        } catch (UserManagementException e) {
            throw new TokenVerificationException(INVALID_TOKEN_SECRET);
        }
    }

    private List<String> getCurrentAuthorities(Token dbToken, StandardUser user) {
        var currentAuthorities = user.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .toList();
        return getAuthorities(dbToken).stream()
                .filter(currentAuthorities::contains)
                .toList();
    }

    private void verifyTokenWithDatabaseStoredValue(EncodedToken decodedEncodedToken, Token dbToken) throws TokenVerificationException {
        var timestamp = dbToken.expiration();
        if (timestamp != null && timestamp.isBefore(clock.instant())) {
            throw new TokenVerificationException(TOKEN_EXPIRED);
        }
        if (!BCrypt.checkpw(decodedEncodedToken.secret(), dbToken.secret())) {
            throw new TokenVerificationException(INVALID_TOKEN_SECRET);
        }
    }

    @Override
    public void initialize(ApplicationSettings settings) {
        settings.setDefaultSetting(USER_KEYS_ENABLED_KEY, true);
        settings.setDefaultSetting(APP_KEYS_ENABLED_KEY, true);
    }
}
