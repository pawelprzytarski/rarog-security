package eu.rarogsoftware.rarog.platform.plugins.pat.api;

public final class SettingKeys {
    public static final String USER_KEYS_ENABLED_KEY = "rarog.pat.user.keys.enabled";
    public static final String APP_KEYS_ENABLED_KEY = "rarog.pat.app.keys.enabled";

    private SettingKeys() {
    }
}
