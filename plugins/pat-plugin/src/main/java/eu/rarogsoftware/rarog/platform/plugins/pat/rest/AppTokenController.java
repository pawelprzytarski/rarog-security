package eu.rarogsoftware.rarog.platform.plugins.pat.rest;

import eu.rarogsoftware.rarog.platform.api.user.management.AppUser;
import eu.rarogsoftware.rarog.platform.api.user.management.UserRole;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenCreationException;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.Clock;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@PreAuthorize("hasAuthority('AUTH_ORGANIC') and hasAuthority('TYPE_HUMAN') and hasAuthority('ROLE_ADMIN')")
@RequestMapping(value = "token/app", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Application Access Tokens Controller")
public class AppTokenController {
    private final TokenService tokenService;

    public AppTokenController(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @PostMapping("create")
    @Operation(summary = "Generates new personal access token for user", description = "Generated new token for current user using specified "
            + "expiration (cannot be null) and authorities (can be empty to use default authorities). "
            + "Token is returned only as response of this endpoint, there will no be possible to retrieve token value at later date.")
    @ApiResponse(responseCode = "201", description = "When token successfully created returns token details including token itself.")
    @ApiResponse(responseCode = "400")
    @Parameter(name = "user", hidden = true)
    public ResponseEntity<TokenControllerHelper.TokenCreatedData> createToken(@RequestBody CreateTokenRequest createTokenRequest) throws TokenCreationException {
        if (StringUtils.isBlank(createTokenRequest.name)
                || StringUtils.isBlank(createTokenRequest.appName)
                || createTokenRequest.expiration == null
                || createTokenRequest.expiration.isBefore(Clock.systemUTC().instant().atZone(createTokenRequest.expiration.getZone()))
        ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid request data provided");
        }
        var token = tokenService.createAppToken(AppUser.builder().appName(createTokenRequest.appName()).build(),
                createTokenRequest.name(),
                createTokenRequest.expiration().toInstant(),
                (createTokenRequest.authorities() == null || createTokenRequest.authorities().isEmpty())
                        ? Collections.singletonList(UserRole.USER.getAuthority().getAuthority())
                        : createTokenRequest.authorities()
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(new TokenControllerHelper.TokenCreatedData(createTokenRequest.name(),
                createTokenRequest.expiration(),
                token));
    }

    @PostMapping("create/meta")
    @Operation(summary = "Gets list of possible values for token creation request")
    @Parameter(hidden = true)
    @ApiResponse(responseCode = "200")
    public ResponseEntity<TokenMetadata> getTokenMetadata() {
        return ResponseEntity.ok(new TokenMetadata(Arrays.stream(UserRole.values())
                .map(userRole -> userRole.getAuthority().getAuthority())
                .toList()));
    }

    @GetMapping("{appName}/list")
    @Operation(summary = "Get list of tokens for specified app")
    @Parameter(name = "appName", description = "Name of application")
    @ApiResponse(responseCode = "200")
    public ResponseEntity<List<TokenControllerHelper.TokenDisplayData>> getAppTokens(@PathVariable("appName") String appName,
                                                                                     HttpServletRequest request) {
        return ResponseEntity.ok(TokenControllerHelper.convertTokensToDisplay(tokenService.getTokenForApp(appName), request));
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Removes token from system", description = "Removing token makes it impossible to login with it anymore.")
    @Parameter(name = "id", description = "Id of token to remove")
    @ApiResponse(responseCode = "200")
    @ApiResponse(responseCode = "404", description = "Not found is returned when token do not exist")
    public ResponseEntity removeToken(@PathVariable("id") Long tokenId) {
        if (tokenId == null || tokenId < 0) {
            return ResponseEntity.badRequest().build();
        }
        var token = tokenService.getTokenById(tokenId);
        if (token.isPresent() && token.get().appName() != null) {
            tokenService.revokeToken(token.get());
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Token does not exist or user have no rights to remove it");
        }
    }

    @Schema(name = "Token request details")
    record CreateTokenRequest(String name,
                              String appName,
                              ZonedDateTime expiration,
                              List<String> authorities) {
    }
}
