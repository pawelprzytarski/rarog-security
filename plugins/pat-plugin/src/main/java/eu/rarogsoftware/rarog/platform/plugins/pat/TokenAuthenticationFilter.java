package eu.rarogsoftware.rarog.platform.plugins.pat;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginSecurityFilterMapping;
import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.CsrfHelper;
import eu.rarogsoftware.rarog.platform.api.user.management.AuthenticationType;
import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web.SecurityFilterMapping;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenService;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenVerificationException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.ArrayList;

import static eu.rarogsoftware.rarog.platform.api.security.mfa.MfaService.EXCLUDE_REQUEST_FROM_MFA_ATTRIBUTE;

@SecurityFilterMapping(placement = PluginSecurityFilterMapping.Placement.BEFORE,
        relativeFilterName = "org.springframework.security.web.header.HeaderWriterFilter")
public class TokenAuthenticationFilter extends OncePerRequestFilter {
    private static final String HEADER = "Authorization";
    private static final String PREFIX = "Token ";
    private final Logger logger = LoggerFactory.getLogger(TokenAuthenticationFilter.class);
    private final TokenService tokenService;
    private final Histogram authenticationHistogram;
    private final Counter authenticationAttemptsCounter;

    public TokenAuthenticationFilter(TokenService tokenService,
                                     @ComponentImport MetricsService metricsService) {
        this.tokenService = tokenService;
        authenticationHistogram = metricsService.createHistogram(HistogramSettings.settings()
                .name("rarog_pat_authentication_duration")
                .description("Measures how long it takes to authenticate using token")
                .labels("result")
                .buckets(0.0001, 0.001, 0.01, 0.1, 1, 10));
        authenticationAttemptsCounter = metricsService.createCounter(MetricSettings.settings()
                .name("rarog_pat_authentication_attempts")
                .description("Measure number of authentication attempts during server life")
                .labels("type"));
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        if (isTokenPresent(request)) {
            authenticationAttemptsCounter.labels("all").increment();
            var stopWatch = StopWatch.createStarted();
            try {
                var token = validateToken(request);
                if (request.getSession(false) != null) {
                    throw new TokenVerificationException("Access tokens work only for stateless calls");
                }
                setUpSpringAuthentication(token);
                request.setAttribute(CsrfHelper.DISABLE_XSRF_ATTRIBUTE, true);
                request.setAttribute(EXCLUDE_REQUEST_FROM_MFA_ATTRIBUTE, true);
                authenticationHistogram.labels("success").observe(stopWatch.getTime());
                authenticationAttemptsCounter.labels("success").increment();
                logger.debug("Authenticated user {}", token.principal());
                chain.doFilter(request, response);
            } catch (TokenVerificationException e) {
                authenticationAttemptsCounter.labels("failure").increment();
                authenticationHistogram.labels("failure").observe(stopWatch.getTime());
                logger.debug("Authentication failed", e);
                response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
            } finally {
                SecurityContextHolder.clearContext();
                var session = request.getSession(false);
                if (session != null) {
                    session.invalidate();
                }
                authenticationHistogram.labels("finish").observe(stopWatch.getTime());
            }
            return;
        }
        chain.doFilter(request, response);
    }

    private TokenService.DecodedToken validateToken(HttpServletRequest request) throws TokenVerificationException {
        var token = request.getHeader(HEADER).replace(PREFIX, "");
        return tokenService.verifyToken(token);
    }

    private void setUpSpringAuthentication(TokenService.DecodedToken token) {
        var authorities = new ArrayList<>(token.authorities());
        authorities.add(AuthenticationType.SYNTHETIC.getAuthority().getAuthority());

        var auth = new UsernamePasswordAuthenticationToken(token.principal(), null,
                authorities.stream().map(SimpleGrantedAuthority::new).toList());
        logger.debug("Saving authentication: {}", auth);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    private boolean isTokenPresent(HttpServletRequest request) {
        var authenticationHeader = request.getHeader(HEADER);
        return authenticationHeader != null && authenticationHeader.startsWith(PREFIX);
    }
}
