package eu.rarogsoftware.rarog.platform.plugins.pat;

import eu.rarogsoftware.commons.cache.CacheService;
import eu.rarogsoftware.commons.utils.JdkRandomService;
import eu.rarogsoftware.commons.utils.RandomService;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AccountSettingsMenuElement;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoader;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoaderFactory;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.ResourceLoaderDynamicAssetsLoaderFactory;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.EnablePluginAutoconfiguration;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;

import java.time.Clock;

@Configuration
@EnablePluginAutoconfiguration
@EnableMethodSecurity
public class PluginConfig {
    @Bean
    Clock clock() {
        return Clock.systemDefaultZone();
    }

    @Bean
    RandomService randomService() {
        return new JdkRandomService();
    }

    @Bean
    DynamicAssetsLoaderFactory localDynamicAssetsLoaderFactory(ResourceLoader resourceLoader,
                                                               @ComponentImport CacheService cacheService,
                                                               @Value("${webpack.manifest.cache.duration:-1}")
                                                               Long cacheDuration,
                                                               @Value("${webpack.manifest.location:classpath:descriptors/webpack-{namespace}.manifest.json}")
                                                               String manifestLocation,
                                                               @Value("${webpack.manifest.override.location:classpath:descriptors/webpack-{namespace}.manifest.json}")
                                                               String manifestOverriddenLocation) {
        return new ResourceLoaderDynamicAssetsLoaderFactory(resourceLoader,
                cacheService,
                cacheDuration,
                StringUtils.isBlank(manifestOverriddenLocation) ? manifestLocation : manifestOverriddenLocation) {
            @Override
            protected String getCacheName() {
                return ResourceLoaderDynamicAssetsLoaderFactory.class.getName() + "_pat_plugin_cache";
            }
        };
    }

    @Bean
    AccountSettingsMenuElement patMenu(DynamicAssetsLoaderFactory dynamicAssetsLoaderFactory) {
        // TODO move to frontend when it will be possible to generate descriptors on frontend
        return new AccountSettingsMenuElement(
                "accesstokens",
                "",
                "account:account.setting.accesstokens.title",
                dynamicAssetsLoaderFactory.getNamedAssetsLoader("dynamicComponents")
                        .flatMap(dynamicAssetsLoader -> dynamicAssetsLoader.getAssetsForEntryPoint("accessTokenAccountSettings"))
                        .stream()
                        .flatMap(data -> data.js().stream())
                        .map(DynamicAssetsLoader.WebpackAssetData::source)
                        .toList()
        );
    }
}
