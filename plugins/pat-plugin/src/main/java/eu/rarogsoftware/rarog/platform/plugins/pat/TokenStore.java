package eu.rarogsoftware.rarog.platform.plugins.pat;

import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenService;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface TokenStore {
    Long saveToken(String name, Instant expiration, Long userId, String appName, List<String> grantedAuthorities, String secret);

    List<TokenService.Token> getTokensForUser(long userId);

    List<TokenService.Token> getTokenForApp(String appName);

    Optional<TokenService.Token> getTokenById(Long tokenId);

    void removeToken(TokenService.Token token);

    Long cleanupOutdatedTokens(Instant instant);
}
