package eu.rarogsoftware.rarog.platform.plugins.pat.rest;

import eu.rarogsoftware.rarog.platform.api.i18n.I18nHelper;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.security.AuthorityHelper;
import eu.rarogsoftware.rarog.platform.api.user.management.RarogUser;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.api.user.management.UserManagementException;
import eu.rarogsoftware.rarog.platform.api.user.management.UserManager;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenCreationException;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;

import static eu.rarogsoftware.rarog.platform.plugins.pat.rest.TokenControllerHelper.convertTokensToDisplay;

@RestController
@PreAuthorize("hasAuthority('AUTH_ORGANIC') and hasAuthority('TYPE_HUMAN')")
@RequestMapping(value = "token/user", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Personal Access Token Controller")
public class UserTokenController {
    private final TokenService tokenService;
    private final UserManager userManager;
    private final I18nHelper i18nHelper;

    public UserTokenController(TokenService tokenService,
                               @ComponentImport UserManager userManager,
                               @ComponentImport I18nHelper i18nHelper) {
        this.tokenService = tokenService;
        this.userManager = userManager;
        this.i18nHelper = i18nHelper;
    }

    @GetMapping("my")
    @Operation(summary = "Get list of tokens created for current user")
    @ApiResponse(responseCode = "200")
    @Parameter(hidden = true)
    public ResponseEntity<List<TokenControllerHelper.TokenDisplayData>> getCurrentUserTokens(@AuthenticationPrincipal StandardUser user,
                                                                                             HttpServletRequest request) {
        return ResponseEntity.ok(convertTokensToDisplay(tokenService.getTokensForUser(user.getId()), request));
    }

    @PostMapping("create")
    @Operation(summary = "Generates new personal access token for user.", description = "Generated new token for current user using specified "
            + "expiration (cannot be null for token without expiration) and authorities (can be empty to use user current authorities). "
            + "Token is returned only as response of this endpoint, there will no be possible to retrieve token value at later date.")
    @ApiResponse(responseCode = "201", description = "When token successfully created returns token details including token itself.")
    @ApiResponse(responseCode = "400")
    @Parameter(name = "user", hidden = true)
    @Parameter(name = "doNotTranslate", hidden = true)
    @Parameter(name = "createTokenRequest", description = "Token request details")
    public ResponseEntity<TokenControllerHelper.TokenCreatedData> createToken(@AuthenticationPrincipal StandardUser user,
                                                                              @RequestParam(value = "doNotTranslate", required = false, defaultValue = "false") boolean doNotTranslate,
                                                                              @RequestBody CreateTokenRequest createTokenRequest) throws TokenCreationException {
        if (StringUtils.isBlank(createTokenRequest.name)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, getTranslatedMessage(doNotTranslate, "pat:page.create.form.errors.empty.name"));
        }
        if (createTokenRequest.expiration() == null || createTokenRequest.expiration().toInstant().isBefore(Instant.now())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, getTranslatedMessage(doNotTranslate, "pat:page.create.form.errors.invalid.date"));
        }
        var token = tokenService.createUserToken(user, createTokenRequest.name(),
                createTokenRequest.expiration().toInstant(),
                createTokenRequest.authorities() == null || createTokenRequest.authorities().isEmpty()
                        ? user.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList()
                        : createTokenRequest.authorities()
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(new TokenControllerHelper.TokenCreatedData(createTokenRequest.name(),
                createTokenRequest.expiration(),
                token));
    }

    private String getTranslatedMessage(boolean doNotTranslate, String key) {
        return doNotTranslate ? key : i18nHelper.getText(key);
    }

    @GetMapping("create/meta")
    @Operation(summary = "Gets list of possible values for token creation request")
    @Parameter(hidden = true)
    @ApiResponse(responseCode = "200")
    public ResponseEntity<TokenMetadata> getTokenMetadata(@AuthenticationPrincipal StandardUser standardUser) {
        return ResponseEntity.ok(new TokenMetadata(standardUser.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .filter(authority -> !RarogUser.HUMAN_AUTHORITY.getAuthority().equals(authority))
                .toList()));
    }

    @GetMapping("{usernameOrUserId}/list")
    @PreAuthorize("hasAuthority('AUTH_ORGANIC') and hasAuthority('TYPE_HUMAN') and hasAuthority('ROLE_ADMIN')")
    @Operation(summary = "Get list of token of user specified by id or username", description = "Endpoint available only for admins.")
    @Parameter(name = "usernameOrUserId", description = "Username or id (system will autodetect which) of user")
    @ApiResponse(responseCode = "200")
    @ApiResponse(responseCode = "400")
    public ResponseEntity getOtherUserTokens(@PathVariable("usernameOrUserId") String usernameOrUserId,
                                             HttpServletRequest request) {
        Long id = null;
        try {
            id = Long.parseLong(usernameOrUserId);
        } catch (NumberFormatException e) {
            if (userManager.userExists(usernameOrUserId)) {
                try {
                    id = userManager.getUser(usernameOrUserId).getId();
                } catch (UserManagementException ex) {
                    // do nothing
                }
            }
        }
        if (id == null || !userManager.userExists(id)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, i18nHelper.getText("pat:page.list.errors.bad.user"));
        }
        return ResponseEntity.ok(convertTokensToDisplay(tokenService.getTokensForUser(id), request));
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Removes token from system", description = "Removing token makes it impossible to login with it anymore.")
    @Parameter(name = "id", description = "Id of token to remove")
    @ApiResponse(responseCode = "200")
    @ApiResponse(responseCode = "404", description = "Not found is returned when token do not exist of user do not have access to it")
    public ResponseEntity removeToken(@PathVariable("id") Long tokenId,
                                      @AuthenticationPrincipal StandardUser user) {
        if (tokenId == null || tokenId < 0) {
            return ResponseEntity.badRequest().build();
        }
        var token = tokenService.getTokenById(tokenId);
        if (token.isPresent() && token.get().userId() != null
                && (AuthorityHelper.hasAdminRights(user) || user.getId().equals(token.get().userId()))) {
            tokenService.revokeToken(token.get());
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, i18nHelper.getText("pat:page.list.errors.bad.token"));
        }
    }

    @Schema(name = "Token request details")
    record CreateTokenRequest(String name,
                              ZonedDateTime expiration,
                              List<String> authorities) {
    }
}
