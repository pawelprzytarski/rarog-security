package eu.rarogsoftware.rarog.platform.plugins.pat;

import com.querydsl.core.Tuple;
import eu.rarogsoftware.commons.cache.AbstractCacheLoader;
import eu.rarogsoftware.commons.cache.CacheService;
import eu.rarogsoftware.commons.database.connection.DatabaseConnectionProvider;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenService;
import eu.rarogsoftware.rarog.platform.plugins.pat.db.QAccessToken;
import org.springframework.stereotype.Component;

import javax.cache.Cache;
import javax.cache.integration.CacheLoaderException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Component
public class DbTokenStore implements TokenStore {
    public static final QAccessToken ACCESS_TOKEN = QAccessToken.accessToken;
    public final DatabaseConnectionProvider databaseConnectionProvider;
    private final Cache<Long, Optional<TokenService.Token>> tokenCache;

    public DbTokenStore(@ComponentImport DatabaseConnectionProvider databaseConnectionProvider,
                        @ComponentImport CacheService cacheService) {
        this.databaseConnectionProvider = databaseConnectionProvider;
        this.tokenCache = cacheService.<Long, Optional<TokenService.Token>>getBuilder()
                .name(DbTokenStore.class.getName()+"_tokenByIdCache")
                .storeByValue(false)
                .expireAfterAccess(10, TimeUnit.MINUTES)
                .cacheLoader(new AbstractCacheLoader<>() {
                    @Override
                    public Optional<TokenService.Token> load(Long tokenId) throws CacheLoaderException {
                        return Optional.ofNullable(databaseConnectionProvider.getConnection(query -> query
                                        .select(ACCESS_TOKEN.all())
                                        .from(ACCESS_TOKEN)
                                        .where(ACCESS_TOKEN.id.eq(tokenId))
                                        .fetchFirst()))
                                .map(DbTokenStore.this::convertTupleToToken);
                    }
                })
                .build();
    }

    @Override
    public Long saveToken(String name, Instant expiration, Long userId, String appName, List<String> grantedAuthorities, String secret) {
        return databaseConnectionProvider.getConnection(query -> query
                .insert(ACCESS_TOKEN)
                .set(ACCESS_TOKEN.userId, userId)
                .set(ACCESS_TOKEN.appName, appName)
                .set(ACCESS_TOKEN.secret, secret)
                .set(ACCESS_TOKEN.tokenName, name)
                .set(ACCESS_TOKEN.expiration, Timestamp.from(expiration))
                .set(ACCESS_TOKEN.authorities, String.join(",", grantedAuthorities))
                .executeWithKey(ACCESS_TOKEN.id));
    }

    @Override
    public List<TokenService.Token> getTokensForUser(long userId) {
        return databaseConnectionProvider.getConnection(query -> query
                        .select(ACCESS_TOKEN.all())
                        .from(ACCESS_TOKEN)
                        .where(ACCESS_TOKEN.userId.eq(userId))
                        .orderBy(ACCESS_TOKEN.tokenName.asc())
                        .orderBy(ACCESS_TOKEN.expiration.asc())
                        .fetch()).stream()
                .map(this::convertTupleToToken)
                .toList();
    }

    @Override
    public List<TokenService.Token> getTokenForApp(String appName) {
        return databaseConnectionProvider.getConnection(query -> query
                        .select(ACCESS_TOKEN.all())
                        .from(ACCESS_TOKEN)
                        .where(ACCESS_TOKEN.appName.eq(appName))
                        .orderBy(ACCESS_TOKEN.tokenName.asc())
                        .orderBy(ACCESS_TOKEN.expiration.asc())
                        .fetch()).stream()
                .map(this::convertTupleToToken)
                .toList();
    }

    private TokenService.Token convertTupleToToken(Tuple tuple) {
        var expiration = tuple.get(ACCESS_TOKEN.expiration);
        var authorities = tuple.get(ACCESS_TOKEN.authorities);
        return new TokenService.Token(
                tuple.get(ACCESS_TOKEN.id),
                tuple.get(ACCESS_TOKEN.tokenName),
                tuple.get(ACCESS_TOKEN.appName),
                tuple.get(ACCESS_TOKEN.userId),
                tuple.get(ACCESS_TOKEN.secret),
                expiration != null ? expiration.toInstant() : null,
                authorities != null ? Arrays.asList(authorities.split(",")) : null
        );
    }

    @Override
    public Optional<TokenService.Token> getTokenById(Long tokenId) {
        return tokenCache.get(tokenId);
    }

    @Override
    public void removeToken(TokenService.Token token) {
        tokenCache.remove(token.id());
        databaseConnectionProvider.getConnection(query ->
                query.delete(ACCESS_TOKEN)
                        .where(ACCESS_TOKEN.id.eq(token.id()))
                        .execute());
    }

    @Override
    public Long cleanupOutdatedTokens(Instant instant) {
        return databaseConnectionProvider.getConnection(query->
                query.delete(ACCESS_TOKEN)
                        .where(ACCESS_TOKEN.expiration.before(Timestamp.from(instant)))
                        .execute());
    }
}