package eu.rarogsoftware.rarog.platform.plugins.pat.rest;

import java.util.List;

record TokenMetadata(List<String> possibleAuthorities) {
}
