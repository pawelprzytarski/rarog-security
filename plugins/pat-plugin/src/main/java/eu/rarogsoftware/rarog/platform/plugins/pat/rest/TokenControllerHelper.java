package eu.rarogsoftware.rarog.platform.plugins.pat.rest;

import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenService;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.servlet.support.RequestContextUtils;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

public class TokenControllerHelper {
    public  static List<TokenDisplayData> convertTokensToDisplay(List<TokenService.Token> tokens, HttpServletRequest request) {
        var timeZone = RequestContextUtils.getTimeZone(request);
        var zoneId = timeZone == null ? ZoneId.systemDefault() : timeZone.toZoneId();
        return tokens.stream()
                .map(token -> {
                    var expiration = ZonedDateTime.ofInstant(token.expiration(), zoneId);
                    return new TokenDisplayData(token.id(), token.name(), token.appName(), expiration);
                })
                .toList();
    }

    @Schema(name = "Access token details")
    public record TokenDisplayData(Long id, String name, String appName, ZonedDateTime expiration) {
    }

    @Schema(name = "Access token creation result")
    public record TokenCreatedData(String name, ZonedDateTime expiration, String value) {
    }
}
