const WebpackAssetsManifest = require('webpack-assets-manifest');
const CompressionPlugin = require('compression-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');
const path = require('path');

function assetsManifestCommonConfig(subdirectory, name, debug) {
    return {
        publicPath: '/static/p/eu.rarogsoftware.rarog.platform.plugins:pat-plugin/' + (debug ? 'debug/' : ''),
        output: (debug ? '../' : '') + `../descriptors/${subdirectory}webpack-${name}.manifest.json`,
        entrypoints: true,
        entrypointsUseAssets: true,
        integrity: true
    };
}

module.exports = function (_env, argv) {
    const mode = _env.dev ? 'development' : 'production';
    const antiCacheSuffix = !_env.dev ? '[contenthash]' : 'debug';
    const outputPath = path.normalize(__dirname + '../../../../target/classes/static/') + (_env.dev ? 'debug/' : '');
    console.log('Output directory:' + outputPath);
    return {
        mode: mode,
        devtool: _env.dev ? 'inline-source-map' : undefined,
        target: 'web',
        entry: {
            accessTokenAccountSettings: './src/AccessTokensAccountSettings'
        },
        output: {
            path: outputPath,
            filename: `js/[name].${antiCacheSuffix}.js`,
            chunkFilename: `js/[name].${antiCacheSuffix}.js`,
            libraryTarget: 'module',
            globalObject: 'globals'
        },
        externals: {
            react: 'global react',
            '@rarog/rarog-app-utils': 'global @rarog/rarog-app-utils',
            'bootstrap': 'global bootstrap',
            'react-bootstrap': 'global react-bootstrap',
            'react-i18next': 'global react-i18next'
        },
        experiments: {
            outputModule: true
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: `css/[name].${antiCacheSuffix}.css`,
                chunkFilename: `css/[name].${antiCacheSuffix}.css`
            }),
            new webpack.DefinePlugin({
                DEVELOPMENT: JSON.stringify(!!_env.dev),
                BUILT_AT: webpack.DefinePlugin.runtimeValue(Date.now)
            }),
            new WebpackAssetsManifest(assetsManifestCommonConfig('', 'dynamicComponents', _env.dev)),
            new CompressionPlugin({
                algorithm: 'gzip',
                threshold: 1024,
                minRatio: 0.8
            })
        ].filter(Boolean),
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    use: [{
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            cacheCompression: true,
                            presets: ['@babel/preset-env', '@babel/preset-react'],
                        },
                    }]
                },
                {
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        'css-loader'
                    ]
                },
                {
                    test: /\.svg$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 8 * 1024,
                            },
                        },
                    ],
                }
            ]
        },
        resolve: {
            extensions: ['.js', '.jsx'],
        },
        watch: !!_env.watch,
        watchOptions: {
            poll: true,
            ignored: '**/node_modules/'
        }
    };
};

