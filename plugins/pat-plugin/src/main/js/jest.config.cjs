module.exports = async () => {
    return {
        collectCoverage: true,
        collectCoverageFrom: [
            'src/**/*.{js,jsx}',
            '!**/node_modules/**'
        ],
        coverageProvider: "babel"
    };
};