import {useTranslation} from 'react-i18next';
import React, {useState} from 'react';
import {AppFetch, DynamicResources, StatusCodes} from '@rarog/rarog-app-utils';
import {Alert, Spinner, Table} from 'react-bootstrap';
import PropTypes from 'prop-types';

function ListTokens({resource}) {
    const tokens = resource.read();
    const [t] = useTranslation('pat');
    const [errorMessage, setErrorMessage] = useState('');
    if (!tokens || tokens.length === 0) {
        return (
            <div>
                <a href="#create" onClick={()=>window.location.href="#create"}>{t('page.list.no.tokens')}</a>
            </div>
        );
    } else {
        const revokeToken = (event) => {
            const linkElement = event.currentTarget;
            const tokenId = linkElement.dataset.id;
            linkElement.nextSibling.classList.remove('visually-hidden');
            AppFetch('/rest/pat/1.0/token/user/' + tokenId, {
                method: 'DELETE',
            })
                .then(response => {
                    switch (response.status) {
                        case StatusCodes.UNAUTHORIZED:
                            window.location.reload();
                            return;
                        case StatusCodes.OK:
                            linkElement.parentElement.parentElement.remove();
                            return;
                        case StatusCodes.NOT_FOUND:
                            linkElement.nextSibling.classList.add('visually-hidden');
                            setErrorMessage('page.list.errors.bad.token');
                            return;
                        default:
                            linkElement.nextSibling.classList.add('visually-hidden');
                            setErrorMessage(t('common:error.unknown'));
                            return;
                    }
                })
                .catch(reason => {
                    linkElement.nextSibling.classList.add('visually-hidden');
                    setErrorMessage(t('common:error.unknown'));
                    console.error(reason);
                });
        };
        return (
            <>
                {errorMessage ? (
                    <Alert variant="danger" onClose={() => setErrorMessage(undefined)}
                           id="errorMessage"
                           dismissible>
                        <p>
                            {t(errorMessage)}
                        </p>
                    </Alert>
                ) : null}
                <Table striped hover>
                    <thead>
                    <tr>
                        <th>{t('page.list.table.name')}</th>
                        <th>{t('page.list.table.expiration')}</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    {tokens.map(token => {
                        return (
                            <tr key={token.id} id={'tokenItem_' + token.id}>
                                <td>{token.name}</td>
                                <td>{token.expiration}</td>
                                <td>
                                    <a data-id={token.id} id={'deleteToken_' + token.id} onClick={revokeToken}
                                       title={t('page.list.table.remove')}><i
                                        className="fi-rr-trash"/></a>
                                    <Spinner animation="grow" className="visually-hidden" as="div" size="sm"/>
                                </td>
                            </tr>
                        );
                    })}
                    </tbody>
                </Table>
            </>
        );
    }
}

ListTokens.propTypes = {resource: PropTypes.object.isRequired};

export default function TokensList() {
    const resource = new DynamicResources.AsyncResource(AppFetch('/rest/pat/1.0/token/user/my').then(data => data.json()));
    return (
        <ListTokens resource={resource}/>
    );
}