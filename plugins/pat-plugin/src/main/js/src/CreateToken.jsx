import {AppFetch, DynamicResources, StatusCodes} from '@rarog/rarog-app-utils';
import React, {useRef, useState} from 'react';
import {useTranslation} from 'react-i18next';
import {Alert, Button, Col, Form, InputGroup, Overlay, Row, Spinner, Tooltip} from 'react-bootstrap';
import PropTypes from 'prop-types';

function Authorities({metadataResource}) {
    const metadata = metadataResource.read();
    const [t] = useTranslation('pat');
    return (
        <>
            {metadata.possibleAuthorities.map(authority => (
                    <Form.Check
                        name="authority"
                        id={'authority_' + authority}
                        key={authority}
                        type="switch"
                        value={authority}
                        label={t('page.create.form.authority.' + authority, authority)}
                    />
                )
            )}
        </>
    );
}

Authorities.propTypes = {metadataResource: PropTypes.object.isRequired};

function CreatedTokenDisplay({createdToken}) {
    const [t] = useTranslation('pat');
    const tokenElementRef = useRef();
    const tokenInputRef = useRef();
    const [showCopiedTooltip, setShowCopiedTooltip] = useState(false);

    return (
        <>
            {createdToken ? (
                <div className="createToken">
                    <h5>{t('page.create.form.new.token')}</h5>
                    <InputGroup className="mb-3" ref={tokenElementRef}
                                onClick={async () => {
                                    await navigator.clipboard.writeText(tokenInputRef.current.value);
                                    setShowCopiedTooltip(true);
                                    setTimeout(() => setShowCopiedTooltip(false), 10000);
                                }}>
                        <InputGroup.Text id="createdTokenLabel">{createdToken.name}</InputGroup.Text>
                        <Form.Control
                            id="createdTokenValue"
                            ref={tokenInputRef}
                            aria-describedby="createdTokenLabel"
                            readOnly={true}
                            value={createdToken.value}
                        />
                        <InputGroup.Text>{createdToken.expiration}</InputGroup.Text>
                    </InputGroup>
                    <Overlay target={tokenElementRef} show={showCopiedTooltip} rootCloseEvent="mousedown"
                             placement="top">
                        {(props) => (
                            // eslint-disable-next-line react/prop-types
                            <Tooltip {...props} style={{...props.style, position: 'absolute'}}>
                                {t('page.create.form.tooltip.copied')}
                            </Tooltip>
                        )}
                    </Overlay>
                </div>
            ) : null}
        </>
    );
}

CreatedTokenDisplay.propTypes = {createdToken: PropTypes.object};

function RenderErrorMessage({errorMessage, successMessage, setErrorMessage}) {
    const [t] = useTranslation(['pat', 'common']);
    return (
        <>
            {errorMessage ? (
                <Alert variant={successMessage ? 'success' : 'danger'} onClose={() => setErrorMessage('')}
                       id="changePasswordErrorMessage"
                       dismissible>
                    <p>
                        {t(errorMessage)}
                    </p>
                </Alert>
            ) : null}
        </>
    );
}

RenderErrorMessage.propTypes = {
    errorMessage: PropTypes.string.isRequired,
    successMessage: PropTypes.bool.isRequired,
    setErrorMessage: PropTypes.func.isRequired
};

export default function CreateToken() {
    const labelWidth = 3;
    const controlWidth = 12 - labelWidth;
    const createMetadata = new DynamicResources.AsyncResource(AppFetch('/rest/pat/1.0/token/user/create/meta').then(data => data.json()));
    const [validated, setValidated] = useState(false);
    const [spinnerHidden, setSpinnerHidden] = useState(true);
    const [expirationValid, setExpirationValid] = useState(false);
    const [nameValid, setNameValid] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState(false);
    const [createdToken, setCreatedToken] = useState();
    const tokenContainerRef = useRef();
    const [t] = useTranslation(['pat', 'common']);

    const handleSubmit = (event) => {
        event.preventDefault();
        event.stopPropagation();
        const form = event.currentTarget;
        let isValid = true;

        const expiration = form.elements.expiration.valueAsDate;
        const tokenName = form.elements.name.value;
        const authorities = Array
            .from(Array(form.elements.length), (x, i) => i)
            .map(i => form.elements[i])
            .filter(el => el.name === 'authority')
            .filter(el => el.checked)
            .map(el => el.value);
        const request = {
            'name': tokenName,
            'expiration': expiration,
            'authorities': authorities
        };
        if (!expiration
            || new Date() >= expiration) {
            setExpirationValid(false);
            isValid = false;
        } else {
            setExpirationValid(true);
        }
        if (!tokenName) {
            setNameValid(false);
            isValid = false;
        } else {
            setNameValid(true);
        }
        setValidated(true);
        if (isValid) {
            setSpinnerHidden(false);
            AppFetch('/rest/pat/1.0/token/user/create?doNotTranslate=true', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(request)
            })
                .then(response => {
                    setSpinnerHidden(true);
                    switch (response.status) {
                        case StatusCodes.UNAUTHORIZED:
                            window.location.reload();
                            return;
                        case StatusCodes.CREATED:
                            return response.json()
                                .then(data => {
                                    setSuccessMessage(true);
                                    setErrorMessage('page.create.form.success');
                                    setCreatedToken(data);
                                    tokenContainerRef.current.scrollIntoView();
                                    setValidated(false);
                                    form.reset();
                                });
                        case StatusCodes.BAD_REQUEST:
                            return response.json()
                                .then(data => {
                                    if (data.message) {
                                        setErrorMessage(data.message);
                                    } else if (data.errorMessage) {
                                        setErrorMessage(data.errorMessage);
                                    } else {
                                        setErrorMessage(t('common:error.unknown'));
                                    }
                                });
                        default:
                            setErrorMessage(t('common:error.unknown'));
                            return;
                    }
                })
                .catch(reason => {
                    setSuccessMessage(false);
                    setErrorMessage(t('common:error.unknown'));
                    setSpinnerHidden(true);
                    console.error(reason);
                });
        } else {
            setSpinnerHidden(true);
        }
    };

    const getTomorrowDate = () => {
        return new Date(new Date().getTime() + new Date().getTimezoneOffset() * -60 * 1000 + 24 * 3600000).toISOString().slice(0, 19);
    };

    return (
        <>
            <Form noValidate onSubmit={handleSubmit}>
                <RenderErrorMessage errorMessage={errorMessage} successMessage={successMessage}
                                    setErrorMessage={setErrorMessage}/>
                <Form.Group as={Row} className="mb-3" controlId="name">
                    <Form.Label column sm={labelWidth}>{t('page.create.form.token.name')}</Form.Label>
                    <Col sm={controlWidth}>
                        <Form.Control type="text"
                                      placeholder="..."
                                      isValid={validated && nameValid}
                                      isInvalid={validated && !nameValid}
                                      required/>
                        <Form.Control.Feedback style={{'position': 'relative', 'top': '0'}} type="invalid" tooltip>
                            {t('page.create.form.errors.empty.name')}
                        </Form.Control.Feedback>
                    </Col>
                </Form.Group>
                <Form.Group as={Row} className="mb-3" controlId="expiration">
                    <Form.Label column sm={labelWidth}>{t('page.create.form.expiration')}</Form.Label>
                    <Col sm={controlWidth}>
                        <Form.Control type="datetime-local"
                                      defaultValue={getTomorrowDate()}
                                      pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}(:[0-9]{2})?"
                                      isValid={validated && expirationValid}
                                      isInvalid={validated && !expirationValid}
                                      required/>
                        <Form.Control.Feedback style={{'position': 'relative', 'top': '0'}} type="invalid" tooltip>
                            {t('page.create.form.errors.invalid.date')}
                        </Form.Control.Feedback>
                    </Col>
                </Form.Group>
                <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm={labelWidth}>{t('page.create.form.authorities')}</Form.Label>
                    <Col sm={controlWidth}>
                        <Authorities metadataResource={createMetadata}/>
                    </Col>
                </Form.Group>
                <Button variant="primary" type="submit" id="createTokenButton">
                    {t('page.create.form.submit')}
                </Button>
                <div className="d-grid gap-2">
                    <Spinner animation="border" variant="primary" hidden={spinnerHidden}/>
                </div>
            </Form>
            <div ref={tokenContainerRef}>
                <CreatedTokenDisplay createdToken={createdToken}/>
            </div>
        </>
    );
}
