import React, {useEffect, useState} from 'react';
import {Tab, Tabs} from 'react-bootstrap';

const CreateToken = React.lazy(() => import('./CreateToken'));
const TokensList = React.lazy(() => import('./TokensList'));

function AccessTokensAccountSettings() {
    const currentKey = window.location.hash;
    useEffect(() => {
        document.title = 'Tokens';
    });
    const [key, setKey] = useState(currentKey ? currentKey.substring(1) : 'list');
    return (
        <Tabs
            activeKey={key}
            id="accesstokenstab"
            className="mb-3"
            mountOnEnter={true}
            unmountOnExit={true}
            onSelect={(key) => {
                window.history.pushState({}, '', '#' + key);
                setKey(key);
            }}>
            <Tab eventKey="list" title="List"><TokensList/></Tab>
            <Tab eventKey="create" title="Create"><CreateToken/></Tab>
        </Tabs>
    );
}

export default AccessTokensAccountSettings;
