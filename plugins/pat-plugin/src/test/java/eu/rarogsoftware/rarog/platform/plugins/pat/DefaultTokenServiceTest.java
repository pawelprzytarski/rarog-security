package eu.rarogsoftware.rarog.platform.plugins.pat;

import eu.rarogsoftware.commons.utils.RandomService;
import eu.rarogsoftware.rarog.platform.api.task.TaskManager;
import eu.rarogsoftware.rarog.platform.api.user.management.*;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.user.management.*;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenCreationException;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenService;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenVerificationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Clock;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DefaultTokenServiceTest {
    private static final String USER_TOKENS_ENABLED = "rarog.pat.user.keys.enabled";
    private static final String APP_TOKENS_ENABLED = "rarog.pat.app.keys.enabled";
    private static final String SECRET = BCrypt.hashpw(IntStream.range(0, 64).mapToObj(value -> "1").collect(Collectors.joining()), BCrypt.gensalt());

    private static final String TOKEN_NAME = "Token name";
    private static final List<String> AUTH_LIST = Arrays.asList("auth1", "auth2");
    private static final String USER_TOKEN = "eyJwcmluY2lwYWwiOiJ0ZXN0IiwidHlwZSI6IlVTRVIiLCJpZGVudGlmaWVyIjo4LCJzZWNyZXQiOiIxMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExIn0=";
    private static final String APP_TOKEN = "eyJwcmluY2lwYWwiOiJ0ZXN0IiwidHlwZSI6IkFQUCIsImlkZW50aWZpZXIiOjgsInNlY3JldCI6IjExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTEifQ==";
    @Mock
    ApplicationSettings applicationSettings;
    @Mock
    UserManager userManager;
    @Mock
    TokenStore tokenStore;
    @Mock
    RandomService randomService;
    @Mock
    TaskManager taskManager;
    @Mock
    Clock clock;
    @Mock
    StandardUser standardUser;
    @Mock
    AppUser appUser;
    DefaultTokenService tokenService;
    private final String PRICIPAL_NAME = "test";
    private final List<SimpleGrantedAuthority> RESTRICTED_AUTH = Collections.singletonList(new SimpleGrantedAuthority("auth1"));

    @BeforeEach
    void setUp() throws NoSuchAlgorithmException {
        lenient().when(randomService.getSecureRandom()).thenReturn(new SecureRandom() { // workaround Mockito cannot mock SecureRandom class
            @Override
            public int nextInt() {
                return 34;
            }
        });
        lenient().when(clock.instant()).thenReturn(Instant.EPOCH);
        lenient().when(standardUser.getUsername()).thenReturn(PRICIPAL_NAME);
        lenient().when(appUser.getUsername()).thenReturn(PRICIPAL_NAME);
        tokenService = new DefaultTokenService(applicationSettings, userManager, taskManager, tokenStore, randomService, clock);
    }

    @Test
    void testCreateUserTokenCreatesToken() throws TokenCreationException {
        when(tokenStore.saveToken(any(), any(), any(), any(), any(), any())).thenReturn(8L);
        when(applicationSettings.isTrue(USER_TOKENS_ENABLED)).thenReturn(true);

        var result = tokenService.createUserToken(standardUser, TOKEN_NAME, Instant.ofEpochSecond(10), AUTH_LIST);

        assertThat(result).isEqualTo(USER_TOKEN);
    }

    @Test
    void testCreateUserTokenCreatesDifferentTokens() throws TokenCreationException {
        when(tokenStore.saveToken(any(), any(), any(), any(), any(), any())).thenReturn(8L, 9L);
        when(applicationSettings.isTrue(USER_TOKENS_ENABLED)).thenReturn(true);

        var result1 = tokenService.createUserToken(standardUser, TOKEN_NAME, Instant.ofEpochSecond(10), AUTH_LIST);
        var result2 = tokenService.createUserToken(standardUser, TOKEN_NAME, Instant.ofEpochSecond(10), AUTH_LIST);

        assertThat(result1).isNotEqualTo(result2);
    }

    @Test
    void testCreateUserTokenThrowsForIncorrectExpiration() {
        when(applicationSettings.isTrue(USER_TOKENS_ENABLED)).thenReturn(true);

        assertThrows(TokenCreationException.class, () -> tokenService.createUserToken(standardUser, TOKEN_NAME, Instant.ofEpochSecond(-10), AUTH_LIST));
    }

    @Test
    void testCreateUserTokenThrowsForIncorrectDisabledTokens() {
        when(applicationSettings.isTrue(USER_TOKENS_ENABLED)).thenReturn(false);

        assertThrows(TokenCreationException.class, () -> tokenService.createUserToken(standardUser, TOKEN_NAME, Instant.ofEpochSecond(-10), AUTH_LIST));
    }

    @Test
    void testCreateAppTokenCreatesToken() throws TokenCreationException {
        when(tokenStore.saveToken(any(), any(), any(), any(), any(), any())).thenReturn(8L);
        when(applicationSettings.isTrue(APP_TOKENS_ENABLED)).thenReturn(true);
        when(appUser.getAppName()).thenReturn(PRICIPAL_NAME);

        var result = tokenService.createAppToken(appUser, TOKEN_NAME, Instant.ofEpochSecond(10), AUTH_LIST);

        assertThat(result).isEqualTo(APP_TOKEN);
    }

    @Test
    void testCreateAppTokenCreatesDifferentTokens() throws TokenCreationException {
        when(tokenStore.saveToken(any(), any(), any(), any(), any(), any())).thenReturn(8L, 9L);
        when(applicationSettings.isTrue(APP_TOKENS_ENABLED)).thenReturn(true);
        when(appUser.getAppName()).thenReturn(PRICIPAL_NAME);

        var result1 = tokenService.createAppToken(appUser, TOKEN_NAME, Instant.ofEpochSecond(10), AUTH_LIST);
        var result2 = tokenService.createAppToken(appUser, TOKEN_NAME, Instant.ofEpochSecond(10), AUTH_LIST);

        assertThat(result1).isNotEqualTo(result2);
    }

    @Test
    void testCreateAppTokenThrowsForIncorrectExpiration() {
        when(applicationSettings.isTrue(APP_TOKENS_ENABLED)).thenReturn(true);

        assertThrows(TokenCreationException.class, () -> tokenService.createAppToken(appUser, TOKEN_NAME, Instant.ofEpochSecond(-10), AUTH_LIST));
    }

    @Test
    void testCreateAppTokenThrowsForIncorrectDisabledTokens() {
        when(applicationSettings.isTrue(APP_TOKENS_ENABLED)).thenReturn(false);

        assertThrows(TokenCreationException.class, () -> tokenService.createAppToken(appUser, TOKEN_NAME, Instant.ofEpochSecond(-10), AUTH_LIST));
    }

    @Test
    void testVerifyValidUserToken() throws TokenVerificationException, UserManagementException {
        when(applicationSettings.isTrue(USER_TOKENS_ENABLED)).thenReturn(true);
        when(tokenStore.getTokenById(any())).thenReturn(Optional.of(new TokenService.Token(8L, TOKEN_NAME, null, 1L, SECRET, Instant.ofEpochSecond(10), AUTH_LIST)));
        when(userManager.getUser(any(Long.class))).thenReturn(standardUser);
        when(standardUser.getUsername()).thenReturn(PRICIPAL_NAME);
        when(standardUser.getAuthorities()).thenReturn((Collection) AUTH_LIST.stream().map(SimpleGrantedAuthority::new).toList());

        var result = tokenService.verifyToken(USER_TOKEN);

        assertThat(result).isEqualTo(new TokenService.DecodedToken(standardUser,
                TokenService.PrincipalType.USER,
                AUTH_LIST
        ));
    }

    @Test
    void testVerifyFailsWhenTokensAreDisabled() {
        lenient().when(applicationSettings.isTrue(APP_TOKENS_ENABLED)).thenReturn(false);
        lenient().when(applicationSettings.isTrue(USER_TOKENS_ENABLED)).thenReturn(false);

        assertThrows(TokenVerificationException.class, () -> tokenService.verifyToken(USER_TOKEN));
        assertThrows(TokenVerificationException.class, () -> tokenService.verifyToken(APP_TOKEN));
    }

    @Test
    void testVerifyValidUserTokenButRemoveObsoleteAuthorities() throws TokenVerificationException, UserManagementException {
        when(applicationSettings.isTrue(USER_TOKENS_ENABLED)).thenReturn(true);
        when(tokenStore.getTokenById(any())).thenReturn(Optional.of(new TokenService.Token(8L, TOKEN_NAME, null, 1L, SECRET, Instant.ofEpochSecond(10), AUTH_LIST)));
        when(userManager.getUser(any(Long.class))).thenReturn(standardUser);
        when(standardUser.getUsername()).thenReturn(PRICIPAL_NAME);
        when(standardUser.getAuthorities()).thenReturn((Collection) RESTRICTED_AUTH);

        var result = tokenService.verifyToken(USER_TOKEN);

        assertThat(result).isEqualTo(new TokenService.DecodedToken(standardUser,
                TokenService.PrincipalType.USER,
                RESTRICTED_AUTH.stream().map(SimpleGrantedAuthority::getAuthority).toList()
        ));
    }

    @Test
    void testVerifyValidAppToken() throws TokenVerificationException {
        when(applicationSettings.isTrue(APP_TOKENS_ENABLED)).thenReturn(true);
        when(tokenStore.getTokenById(any())).thenReturn(Optional.of(new TokenService.Token(8L, TOKEN_NAME, PRICIPAL_NAME, null, SECRET, Instant.ofEpochSecond(10), AUTH_LIST)));

        var result = tokenService.verifyToken(APP_TOKEN);

        assertThat(result).isEqualTo(new TokenService.DecodedToken(AppUser.builder()
                .appName(PRICIPAL_NAME)
                .authorities(AUTH_LIST.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet()))
                .build(),
                TokenService.PrincipalType.APP,
                AUTH_LIST
        ));
    }

    @Test
    void testVerifyTokenForInvalidUser() throws UserManagementException {
        when(applicationSettings.isTrue(USER_TOKENS_ENABLED)).thenReturn(true);
        when(tokenStore.getTokenById(any())).thenReturn(Optional.of(new TokenService.Token(8L, TOKEN_NAME, null, 1L, SECRET, Instant.ofEpochSecond(10), AUTH_LIST)));
        when(userManager.getUser(any(Long.class))).thenThrow(UserNotFoundException.class);

        assertThrows(TokenVerificationException.class, () -> tokenService.verifyToken(USER_TOKEN));
    }

    @Test
    void testVerifyTokenForOutdatedToken() {
        when(applicationSettings.isTrue(USER_TOKENS_ENABLED)).thenReturn(true);
        when(tokenStore.getTokenById(any())).thenReturn(Optional.of(new TokenService.Token(8L, TOKEN_NAME, null, 1L, SECRET, Instant.ofEpochSecond(-5), AUTH_LIST)));

        assertThrows(TokenVerificationException.class, () -> tokenService.verifyToken(USER_TOKEN));
    }

    @Test
    void testVerifyTokenForIncorrectSecret() {
        when(applicationSettings.isTrue(USER_TOKENS_ENABLED)).thenReturn(true);
        when(tokenStore.getTokenById(any())).thenReturn(Optional.of(new TokenService.Token(8L, TOKEN_NAME, null, 1L, "otherSecret", Instant.ofEpochSecond(-5), AUTH_LIST)));

        assertThrows(TokenVerificationException.class, () -> tokenService.verifyToken(USER_TOKEN));
    }

    @Test
    void testVerifyTokenForInvalidToken() {
        assertThrows(TokenVerificationException.class, () -> tokenService.verifyToken(null));
        assertThrows(TokenVerificationException.class, () -> tokenService.verifyToken(""));
        assertThrows(TokenVerificationException.class, () -> tokenService.verifyToken("random gibberish"));
    }
}