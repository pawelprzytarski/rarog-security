package eu.rarogsoftware.rarog.platform.plugins.pat;

import eu.rarogsoftware.rarog.platform.api.metrics.MetricsService;
import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.CsrfHelper;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaService;
import eu.rarogsoftware.rarog.platform.api.user.management.AppUser;
import eu.rarogsoftware.rarog.platform.api.user.management.AuthenticationType;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenService;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenVerificationException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TokenAuthenticationFilterTest {
    @Mock
    TokenService tokenService;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    FilterChain filterChain;
    @Mock(answer = Answers.RETURNS_MOCKS)
    MetricsService metricsService;
    @Mock
    SecurityContext context;
    @InjectMocks
    TokenAuthenticationFilter filter;
    private final String TEST_TOKEN = "Token test";
    private final String TOKEN_VALUE = "test";

    @BeforeEach
    void setUp() {
        SecurityContextHolder.setContext(context);
    }

    @AfterEach
    void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    void testRequestWithoutHeaderIsPassedFurther() throws ServletException, IOException {

        filter.doFilterInternal(request, response, filterChain);

        verifyNoInteractions(context);
        verifyNoInteractions(response);
        verify(filterChain).doFilter(request, response);
    }

    @Test
    void testRequestWithBasicAuthHeaderIsPassedFurther() throws ServletException, IOException {
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("Basic something");

        filter.doFilterInternal(request, response, filterChain);

        verifyNoInteractions(context);
        verifyNoInteractions(response);
        verify(filterChain).doFilter(request, response);
    }

    @Test
    void testRequestWithIncorrectTokenAuthHeaderIsReturning403() throws ServletException, IOException, TokenVerificationException {
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(TEST_TOKEN);
        when(tokenService.verifyToken(TOKEN_VALUE)).thenThrow(TokenVerificationException.class);

        filter.doFilterInternal(request, response, filterChain);

        verifyNoInteractions(filterChain);
        verifyNoInteractions(context);
        verify(response).sendError(eq(403), any());
    }

    @Test
    void testRequestWithCorrectTokenButExistingSessionIsReturning403() throws ServletException, IOException, TokenVerificationException {
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(TEST_TOKEN);
        when(tokenService.verifyToken(TOKEN_VALUE)).thenReturn(new TokenService.DecodedToken(null, null, null));
        when(request.getSession(false)).thenReturn(mock(HttpSession.class));

        filter.doFilterInternal(request, response, filterChain);

        verifyNoInteractions(filterChain);
        verifyNoInteractions(context);
        verify(response).sendError(eq(403), any());
    }

    @Test
    void testRequestWithCorrectTokenSetsAuthenticationAndHeaders() throws ServletException, IOException, TokenVerificationException {
        var appUser = AppUser.builder().build();
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(TEST_TOKEN);
        when(tokenService.verifyToken(TOKEN_VALUE)).thenReturn(new TokenService.DecodedToken(appUser, TokenService.PrincipalType.APP, Collections.emptyList()));

        filter.doFilterInternal(request, response, filterChain);

        verifyNoInteractions(response);
        verify(context).setAuthentication(new UsernamePasswordAuthenticationToken(appUser, null,
                List.of(AuthenticationType.SYNTHETIC.getAuthority())));
        verify(filterChain).doFilter(request, response);
        verify(request).setAttribute(CsrfHelper.DISABLE_XSRF_ATTRIBUTE, true);
        verify(request).setAttribute(MfaService.EXCLUDE_REQUEST_FROM_MFA_ATTRIBUTE, true);
    }

}