package eu.rarogsoftware.rarog.platform.plugins.telemetry.rest;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.server.ResponseStatusException;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OpenMetricsExposerControllerTest {
    private static final String PLAIN_PASSWORD = "VerySecurePassword";
    private static final String PASSWORD = BCrypt.hashpw(PLAIN_PASSWORD, BCrypt.gensalt());
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    MetricsService metricsService;
    @Mock
    ApplicationSettings applicationSettings;
    @Mock
    HttpServletRequest servletRequest;
    @InjectMocks
    OpenMetricsExposerController exposerController;

    @Test
    void getOpenMetricsTextThrowsNotFoundWhenMetricsDisabled() {
        when(applicationSettings.isTrue(MetricSettingKeys.METRICS_ENABLED_KEY)).thenReturn(false);

        assertThrows(ResponseStatusException.class, () -> exposerController.getOpenMetricsText(servletRequest));
    }

    @Test
    void getOpenMetricsTextThrowsNotFoundWhenExposerDisabled() {
        when(applicationSettings.isTrue(MetricSettingKeys.METRICS_ENABLED_KEY)).thenReturn(true);
        when(applicationSettings.isTrue(MetricSettingKeys.OPEN_METRIC_ENDPOINT_ENABLED_KEY)).thenReturn(false);

        assertThrows(ResponseStatusException.class, () -> exposerController.getOpenMetricsText(servletRequest));
    }

    @Test
    void getOpenMetricsTextThrowsNotAuthorizedWhenExposerSecuredWithPasswordButPasswordNotProvided() {
        mockCommonSettings();
        when(applicationSettings.getPropertyBackedSettingAsString(OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY)).thenReturn(PASSWORD);

        assertThrows(ResponseStatusException.class, () -> exposerController.getOpenMetricsText(servletRequest));
    }

    @Test
    void getOpenMetricsTextThrowsNotAuthorizedWhenExposerSecuredWithPasswordButProvidedPasswordIncorrect() {
        mockCommonSettings();
        when(applicationSettings.getPropertyBackedSettingAsString(OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY)).thenReturn(PASSWORD);
        when(servletRequest.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("Code incorrect");

        assertThrows(ResponseStatusException.class, () -> exposerController.getOpenMetricsText(servletRequest));
    }

    @Test
    void getOpenMetricsTextThrowsNotAuthorizedWhenExposerSecuredWithPasswordButAuthorizationWithDifferentProtocol() {
        mockCommonSettings();
        when(applicationSettings.getPropertyBackedSettingAsString(OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY)).thenReturn(PASSWORD);
        when(servletRequest.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("Token " + PLAIN_PASSWORD);

        assertThrows(ResponseStatusException.class, () -> exposerController.getOpenMetricsText(servletRequest));
    }

    @Test
    void getOpenMetricsTextReturnsEmptyResponseWhenNoMetricsAndAuthenticatedCorrectly() throws Exception {
        mockCommonSettings();
        when(applicationSettings.getPropertyBackedSettingAsString(OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY)).thenReturn(PASSWORD);
        when(servletRequest.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("Code " + PLAIN_PASSWORD);

        var result = exposerController.getOpenMetricsText(servletRequest);

        assertThat(new String(result, StandardCharsets.UTF_8)).isEqualTo("# EOF\n");
    }

    @Test
    void getOpenMetricsTextReturnsCorrectlyFormattedResponse() throws Exception {
        mockCommonSettings();
        var samples = List.of(
                new Sample("test", List.of("tag1", "tag2"), List.of("Uber tag 1", "Value;for,second="), 1.0, 1),
                new Sample("test2", List.of("tag1", "tag2=,"), List.of("value1", "value2"), 34.356, 1)
        );
        var advancedSnapshot = List.of(
                new MetricSamples(MetricSettings.settings().name("TestCounter").unit("kg"), MetricSamples.Type.COUNTER, samples),
                new MetricSamples(MetricSettings.settings().name("TestGauge").description("Description").unit("cm"), MetricSamples.Type.GAUGE, samples),
                new MetricSamples(MetricSettings.settings().name("OtherGauge"), MetricSamples.Type.GAUGE, List.of(
                        new Sample("test3", List.of(), List.of(), 1.0, 1),
                        new Sample("test4", List.of(), List.of(), 34.356, 1)
                ))
        );
        when(metricsService.sample()).thenReturn(advancedSnapshot);

        var result = exposerController.getOpenMetricsText(servletRequest);

        assertThat(new String(result, StandardCharsets.UTF_8)).isEqualTo("""
                # TYPE TestCounter counter
                # HELP TestCounter TestCounter
                # UNIT TestCounter kg
                test{tag1="Uber tag 1",tag2="Value;for,second="} 1.0 1
                test2{tag1="value1",tag2=,="value2"} 34.356 1
                # TYPE TestGauge gauge
                # HELP TestGauge Description
                # UNIT TestGauge cm
                test{tag1="Uber tag 1",tag2="Value;for,second="} 1.0 1
                test2{tag1="value1",tag2=,="value2"} 34.356 1
                # TYPE OtherGauge gauge
                # HELP OtherGauge OtherGauge
                # UNIT OtherGauge\s
                test3 1.0 1
                test4 34.356 1
                # EOF
                """);
    }

    private void mockCommonSettings() {
        when(applicationSettings.isTrue(MetricSettingKeys.METRICS_ENABLED_KEY)).thenReturn(true);
        when(applicationSettings.isTrue(MetricSettingKeys.OPEN_METRIC_ENDPOINT_ENABLED_KEY)).thenReturn(true);
    }
}