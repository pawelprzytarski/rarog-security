package eu.rarogsoftware.rarog.platform.plugins.telemetry.exporters;

import eu.rarogsoftware.rarog.platform.api.metrics.MetricSamples;
import eu.rarogsoftware.rarog.platform.api.metrics.MetricSettings;
import eu.rarogsoftware.rarog.platform.api.metrics.MetricsSnapshot;
import eu.rarogsoftware.rarog.platform.api.metrics.Sample;
import eu.rarogsoftware.rarog.platform.api.security.secrets.HttpClientAuthConfigService;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys;
import eu.rarogsoftware.rarog.platform.api.task.AdvancedScheduledExecutorService;
import eu.rarogsoftware.rarog.platform.api.task.TaskManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.METRICS_INFLUX_EXPORT_ENABLED_KEY;
import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.METRICS_INFLUX_SERVER_KEY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InfluxDbSnapshotExporterTest {
    private static final String SIMPLE_METRIC_REQUEST = "test,instance=testId value=1.0 1\n";
    private static final String TEST_URL = "https://url?precision=ms";
    private static final String TEST_SERVER_STRING = "https://url|auth";
    @Mock
    ApplicationSettings applicationSettings;
    @Mock
    TaskManager taskManager;
    @Mock
    AdvancedScheduledExecutorService executorService;
    @Mock
    HttpClientAuthConfigService authConfigService;
    @Mock
    HttpClientAuthConfigService.AuthConfig authConfig;
    @Mock
    HttpClient.Builder httpClientBuilder;
    @Mock
    HttpClient httpClient;
    @Mock
    HttpResponse<String> httpResponse;
    @InjectMocks
    InfluxDbSnapshotExporter influxDbSnapshotExporter;

    @BeforeEach
    void setUp() {
        lenient().when(taskManager.asExecutorService()).thenReturn(executorService);
    }

    @Test
    void exportEmptySnapshotDoNotSentAnything() throws Exception {
        when(applicationSettings.getSettingOrDefault(METRICS_INFLUX_SERVER_KEY, String.class)).thenReturn("something");
        when(applicationSettings.isTrue(METRICS_INFLUX_EXPORT_ENABLED_KEY)).thenReturn(true);

        influxDbSnapshotExporter.export(List.of());

        verify(authConfigService, times(0)).resolveFromString(anyString());
    }

    @Test
    void exportAttemptWhenExporterDisabledDoesNothing() throws Exception {
        when(applicationSettings.getSettingOrDefault(METRICS_INFLUX_SERVER_KEY, String.class)).thenReturn("something");
        when(applicationSettings.isTrue(METRICS_INFLUX_EXPORT_ENABLED_KEY)).thenReturn(false);

        influxDbSnapshotExporter.export(List.of(new MetricsSnapshot(List.of(), -1)));

        verify(authConfigService, times(0)).resolveFromString(anyString());
    }

    @Test
    void exportAttemptWhenNoServersSpecifiedDoesNothing() throws Exception {
        when(applicationSettings.getSettingOrDefault(METRICS_INFLUX_SERVER_KEY, String.class)).thenReturn("");
        when(applicationSettings.isTrue(METRICS_INFLUX_EXPORT_ENABLED_KEY)).thenReturn(true);

        influxDbSnapshotExporter.export(List.of(new MetricsSnapshot(List.of(), -1)));

        verify(authConfigService, times(0)).resolveFromString(anyString());
    }

    @Test
    void exportSmallSnapshotsToOneServerDoesOneRequest() throws Exception {
        var sentRequests = new ArrayList<HttpRequest>();
        mockCommonSettings();
        mockAuthConfigToReplaceHttpClientWithMock(sentRequests);
        when(applicationSettings.getSettingOrDefault(METRICS_INFLUX_SERVER_KEY, String.class)).thenReturn(TEST_SERVER_STRING);
        when(httpResponse.statusCode()).thenReturn(HttpStatus.NO_CONTENT.value());

        List<MetricsSnapshot> smallSnapshot = getSimpleSnapshot();
        influxDbSnapshotExporter.export(smallSnapshot);

        assertThat(sentRequests)
                .hasSize(1);
        var httpRequest = sentRequests.get(0);
        assertThat(httpRequest.uri()).isEqualTo(URI.create(TEST_URL));
        assertThat(httpRequest.headers().map()).containsEntry(HttpHeaders.CONTENT_ENCODING, List.of("gzip"));
        var requestBody = retrieveContentFromRequest(httpRequest);
        assertThat(requestBody).isEqualTo(SIMPLE_METRIC_REQUEST);
    }

    @Test
    void exportAttemptWithMalformedUrlSkipMalformed() throws Exception {
        var sentRequests = new ArrayList<HttpRequest>();
        mockCommonSettings();
        mockAuthConfigToReplaceHttpClientWithMock(sentRequests);
        when(applicationSettings.getSettingOrDefault(METRICS_INFLUX_SERVER_KEY, String.class)).thenReturn("https://url|auth;malformed|auth;https://url2|malformed");
        when(httpResponse.statusCode()).thenReturn(HttpStatus.NO_CONTENT.value());

        List<MetricsSnapshot> smallSnapshot = getSimpleSnapshot();
        influxDbSnapshotExporter.export(smallSnapshot);

        assertThat(sentRequests)
                .hasSize(1);
        var httpRequest = sentRequests.get(0);
        assertThat(httpRequest.uri()).isEqualTo(URI.create(TEST_URL));
        var requestBody = retrieveContentFromRequest(httpRequest);
        assertThat(requestBody).isEqualTo(SIMPLE_METRIC_REQUEST);
    }

    @Test
    void exportSmallSnapshotsToMultipleServersDoesOneRequestPerServer() throws Exception {
        var sentRequests = new ArrayList<HttpRequest>();
        mockCommonSettings();
        mockAuthConfigToReplaceHttpClientWithMock(sentRequests);
        when(applicationSettings.getSettingOrDefault(METRICS_INFLUX_SERVER_KEY, String.class)).thenReturn("https://url|auth;https://url2|auth");
        when(httpResponse.statusCode()).thenReturn(HttpStatus.NO_CONTENT.value());

        influxDbSnapshotExporter.export(getSimpleSnapshot());

        assertThat(sentRequests)
                .hasSize(2);
        var httpRequest1 = sentRequests.get(0);
        assertThat(httpRequest1.uri()).isEqualTo(URI.create(TEST_URL));
        assertThat(httpRequest1.headers().map()).containsEntry(HttpHeaders.CONTENT_ENCODING, List.of("gzip"));
        var requestBody1 = retrieveContentFromRequest(httpRequest1);
        assertThat(requestBody1).isEqualTo(SIMPLE_METRIC_REQUEST);


        var httpRequest2 = sentRequests.get(0);
        assertThat(httpRequest2.uri()).isEqualTo(URI.create(TEST_URL));
        assertThat(httpRequest2.headers().map()).containsEntry(HttpHeaders.CONTENT_ENCODING, List.of("gzip"));
        var requestBody2 = retrieveContentFromRequest(httpRequest2);
        assertThat(requestBody2).isEqualTo(SIMPLE_METRIC_REQUEST);
    }

    @Test
    void exportSnapshotHasCorrectForm() throws Exception {
        var sentRequests = new ArrayList<HttpRequest>();
        mockCommonSettings();
        mockAuthConfigToReplaceHttpClientWithMock(sentRequests);
        when(applicationSettings.getSettingOrDefault(METRICS_INFLUX_SERVER_KEY, String.class)).thenReturn(TEST_SERVER_STRING);
        when(httpResponse.statusCode()).thenReturn(HttpStatus.NO_CONTENT.value());

        var samples = List.of(
                new Sample("test", List.of("tag1", "tag2"), List.of("Uber tag 1", "Value;for,second="), 1.0, 1),
                new Sample("test2", List.of("tag1", "tag2=,"), List.of("value1", "value2"), 34.356, 1)
        );
        var advancedSnapshot = new MetricsSnapshot(List.of(
                new MetricSamples(MetricSettings.settings(), null, samples),
                new MetricSamples(MetricSettings.settings(), null, List.of(
                        new Sample("test3", List.of(), List.of(), 1.0, 1),
                        new Sample("test4", List.of(), List.of(), 34.356, 1)
                ))
        ), 5);
        influxDbSnapshotExporter.export(List.of(
                makeSimpleSnapshot(samples, 3),
                makeSimpleSnapshot(samples, 4),
                advancedSnapshot
        ));

        assertThat(sentRequests)
                .hasSize(1);
        var requestBody = retrieveContentFromRequest(sentRequests.get(0));
        assertThat(requestBody).isEqualTo("""
                test,tag1=Uber_tag_1,tag2=Value;for_second_,instance=testId value=1.0 3
                test2,tag1=value1,tag2__=value2,instance=testId value=34.356 3
                test,tag1=Uber_tag_1,tag2=Value;for_second_,instance=testId value=1.0 4
                test2,tag1=value1,tag2__=value2,instance=testId value=34.356 4
                test,tag1=Uber_tag_1,tag2=Value;for_second_,instance=testId value=1.0 5
                test2,tag1=value1,tag2__=value2,instance=testId value=34.356 5
                test3,instance=testId value=1.0 5
                test4,instance=testId value=34.356 5
                """);
    }

    @Test
    void exportBigSnapshotIsSendInBatches() throws Exception {
        var sentRequests = new ArrayList<HttpRequest>();
        mockCommonSettings();
        mockAuthConfigToReplaceHttpClientWithMock(sentRequests);
        when(applicationSettings.getSettingOrDefault(METRICS_INFLUX_SERVER_KEY, String.class)).thenReturn(TEST_SERVER_STRING);
        when(httpResponse.statusCode()).thenReturn(HttpStatus.NO_CONTENT.value());

        var samples = IntStream.range(1, 7501)
                .mapToObj(i -> new Sample("test" + i, List.of(), List.of(), 10.3 + i, -1))
                .toList();
        influxDbSnapshotExporter.export(List.of(
                makeSimpleSnapshot(samples, 3),
                makeSimpleSnapshot(samples, 4)
        ));


        var metricPattern = "test%s,instance=testId value=%s %s\n";
        var expectedFirstRequest = produceMetricsString(1, 5001, metricPattern, 3);
        var expectedSecondRequest = produceMetricsString(5001, 7501, metricPattern, 3)
                + produceMetricsString(1, 2501, metricPattern, 4);
        var expectedThirdRequest = produceMetricsString(2501, 7501, metricPattern, 4);

        assertThat(sentRequests)
                .hasSize(3);
        var request1Body = retrieveContentFromRequest(sentRequests.get(0));
        assertThat(request1Body).isEqualTo(expectedFirstRequest);
        var request2Body = retrieveContentFromRequest(sentRequests.get(1));
        assertThat(request2Body).isEqualTo(expectedSecondRequest);
        var request3Body = retrieveContentFromRequest(sentRequests.get(2));
        assertThat(request3Body).isEqualTo(expectedThirdRequest);
    }

    @Test
    void exportBigSnapshotStopOnHttpError() throws Exception {
        var sentRequests = new ArrayList<HttpRequest>();
        mockCommonSettings();
        mockAuthConfigToReplaceHttpClientWithMock(sentRequests);
        when(applicationSettings.getSettingOrDefault(METRICS_INFLUX_SERVER_KEY, String.class)).thenReturn(TEST_SERVER_STRING);
        when(httpResponse.statusCode()).thenReturn(HttpStatus.NOT_FOUND.value());

        var samples = IntStream.range(1, 7501)
                .mapToObj(i -> new Sample("test" + i, List.of(), List.of(), 10.3 + i, -1))
                .toList();
        influxDbSnapshotExporter.export(List.of(
                makeSimpleSnapshot(samples, 3),
                makeSimpleSnapshot(samples, 4)
        ));

        assertThat(sentRequests)
                .hasSize(1);
    }

    private static String produceMetricsString(int startInclusive, int endExclusive, String metricPattern, int x) {
        return IntStream.range(startInclusive, endExclusive).mapToObj(i -> metricPattern.formatted(i, 10.3 + i, x)).collect(Collectors.joining());
    }

    private static List<MetricsSnapshot> getSimpleSnapshot() {
        var samples = List.of(new Sample("test", List.of(), List.of(), 1.0, 1));
        return List.of(makeSimpleSnapshot(samples, 1));
    }

    private static MetricsSnapshot makeSimpleSnapshot(List<Sample> samples, int timestamp) {
        return new MetricsSnapshot(List.of(new MetricSamples(MetricSettings.settings(), MetricSamples.Type.COUNTER, samples)), timestamp);
    }

    private static String retrieveContentFromRequest(HttpRequest httpRequest) throws IOException {
        var subscriber = new ConsumingRequestSubscriber();
        httpRequest.bodyPublisher().get().subscribe(subscriber);
        return new String(subscriber.getDeGzipBytes(), StandardCharsets.UTF_8);
    }

    private void mockCommonSettings() {
        when(applicationSettings.isTrue(METRICS_INFLUX_EXPORT_ENABLED_KEY)).thenReturn(true);
        when(applicationSettings.getSetting(SystemSettingKeys.SYSTEM_ID_KEY, String.class)).thenReturn(Optional.of("testId"));
    }

    private void mockAuthConfigToReplaceHttpClientWithMock(ArrayList<HttpRequest> sentRequests) throws IOException, InterruptedException {
        when(authConfigService.resolveFromString("auth")).thenReturn(authConfig);
        when(authConfig.configureHttpClient(any())).thenReturn(httpClientBuilder);
        when(httpClientBuilder.build()).thenReturn(httpClient);
        when(authConfig.configureHttpRequest(any())).thenCallRealMethod();
        when(httpClient.send(any(), any())).thenAnswer(invocation -> {
            sentRequests.add(invocation.getArgument(0));
            return httpResponse;
        });
    }
}
