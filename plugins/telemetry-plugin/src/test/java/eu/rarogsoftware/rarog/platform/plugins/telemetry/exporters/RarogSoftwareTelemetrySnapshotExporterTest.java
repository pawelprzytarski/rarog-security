package eu.rarogsoftware.rarog.platform.plugins.telemetry.exporters;

import eu.rarogsoftware.rarog.platform.api.metrics.MetricSamples;
import eu.rarogsoftware.rarog.platform.api.metrics.MetricSettings;
import eu.rarogsoftware.rarog.platform.api.metrics.MetricsSnapshot;
import eu.rarogsoftware.rarog.platform.api.metrics.Sample;
import eu.rarogsoftware.rarog.platform.api.security.secrets.HttpClientAuthConfigService;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.task.AdvancedScheduledExecutorService;
import eu.rarogsoftware.rarog.platform.api.task.TaskManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.METRICS_TELEMETRY_EXPORT_ENABLED_KEY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RarogSoftwareTelemetrySnapshotExporterTest {
    @Mock
    ApplicationSettings applicationSettings;
    @Mock
    TaskManager taskManager;
    @Mock
    AdvancedScheduledExecutorService executorService;
    @Mock
    HttpClientAuthConfigService authConfigService;
    @Mock
    HttpClientAuthConfigService.AuthConfig authConfig;
    @Mock
    HttpClient.Builder httpClientBuilder;
    @Mock
    HttpClient httpClient;
    @Mock
    HttpResponse<String> httpResponse;
    @InjectMocks
    RarogSoftwareTelemetrySnapshotExporter snapshotExporter;

    @BeforeEach
    void setUp() {
        lenient().when(taskManager.asExecutorService()).thenReturn(executorService);
    }

    @Test
    void exportAttemptWhenExporterDisabledDoesNothing() throws Exception {
        when(applicationSettings.isTrue(METRICS_TELEMETRY_EXPORT_ENABLED_KEY)).thenReturn(false);

        snapshotExporter.export(List.of(new MetricsSnapshot(List.of(), -1)));

        verify(authConfigService, times(0)).resolveFromString(anyString());
    }

    @Test
    void exportSendsRequestToRarogSoftwareServersUsingBuildInAuth() throws Exception {
        var sentRequests = new ArrayList<HttpRequest>();
        mockAuthConfigToReplaceHttpClientWithMock(sentRequests);
        when(applicationSettings.isTrue(METRICS_TELEMETRY_EXPORT_ENABLED_KEY)).thenReturn(true);
        when(httpResponse.statusCode()).thenReturn(HttpStatus.NO_CONTENT.value());

        List<MetricsSnapshot> smallSnapshot = getSimpleSnapshot();
        snapshotExporter.export(smallSnapshot);

        assertThat(sentRequests)
                .hasSize(1);
        var httpRequest = sentRequests.get(0);
        assertThat(httpRequest.uri()).isEqualTo(URI.create("https://influxdb.rarogsoftware.eu/api/v2/write?precision=ms"));
    }

    private static List<MetricsSnapshot> getSimpleSnapshot() {
        var samples = List.of(new Sample("test", List.of(), List.of(), 1.0, 1));
        return List.of(new MetricsSnapshot(List.of(new MetricSamples(MetricSettings.settings(), MetricSamples.Type.COUNTER, samples)), 1));
    }

    private void mockAuthConfigToReplaceHttpClientWithMock(ArrayList<HttpRequest> sentRequests) throws IOException, InterruptedException {
        when(authConfigService.resolveFromString("buildin:telemetry_clientssl")).thenReturn(authConfig);
        when(authConfig.configureHttpClient(any())).thenReturn(httpClientBuilder);
        when(httpClientBuilder.build()).thenReturn(httpClient);
        when(authConfig.configureHttpRequest(any())).thenCallRealMethod();
        when(httpClient.send(any(), any())).thenAnswer(invocation -> {
            sentRequests.add(invocation.getArgument(0));
            return httpResponse;
        });
    }
}