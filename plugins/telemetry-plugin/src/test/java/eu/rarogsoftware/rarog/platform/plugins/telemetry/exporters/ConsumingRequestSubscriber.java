package eu.rarogsoftware.rarog.platform.plugins.telemetry.exporters;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.Flow;
import java.util.zip.GZIPInputStream;

// Hack as fuck, because this API is not unit test friendly, you cannot just mock HttpClient and get request with its body.
// This body is lost, until you consume it with subscriber, because there is no methods to get it, no equals implementations
// nothing what would this code unit test friendly. So you want to consume it as good boy? Then fuck you, all classes
// that are helpful are internal and inaccessible to you. Implement your own. You need to suffer.
// I could always create nice class that would hide HttpClient details, but then how to test this new class? Who knows?
// The conclusion is only one: "Do you want to unit test if correct request is formed? Then fuck you."
class ConsumingRequestSubscriber implements Flow.Subscriber<ByteBuffer> {
    private final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    private byte[] result;

    private volatile Flow.Subscription subscription;

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        if (this.subscription != null) {
            subscription.cancel();
            return;
        }
        this.subscription = subscription;
        subscription.request(Long.MAX_VALUE);
    }

    @Override
    public void onNext(ByteBuffer item) {
        byte[] tmp = new byte[item.limit() - item.position()];
        item.get(tmp);
        try {
            byteStream.write(tmp);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onError(Throwable throwable) {
        onComplete();
    }

    @Override
    public void onComplete() {
        result = byteStream.toByteArray();
        try {
            byteStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public byte[] getDeGzipBytes() throws IOException {
        try (var byteStream = new ByteArrayInputStream(result);
             var gzipStream = new GZIPInputStream(byteStream)) {
            return gzipStream.readAllBytes();
        }
    }
}
