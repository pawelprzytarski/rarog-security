package eu.rarogsoftware.rarog.platform.plugins.telemetry.exporters;

import eu.rarogsoftware.rarog.platform.api.metrics.MetricSamples;
import eu.rarogsoftware.rarog.platform.api.metrics.MetricSettings;
import eu.rarogsoftware.rarog.platform.api.metrics.MetricsSnapshot;
import eu.rarogsoftware.rarog.platform.api.metrics.Sample;
import eu.rarogsoftware.rarog.platform.api.security.secrets.AuthConfiguredSocketFactoryService;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.net.SocketFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.METRICS_GRAPHITE_EXPORT_ENABLED_KEY;
import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.METRICS_GRAPHITE_SERVER_KEY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GraphiteSnapshotExporterTest {
    private static final String TEST_SOCKE_STRING = "server:2001|auth";
    private static final String TEST_MULTI_SOCKET_STRING = "server:2001|auth;server2:2002|auth";
    @Mock
    ApplicationSettings applicationSettings;
    @Mock
    AuthConfiguredSocketFactoryService socketFactoryService;
    @Mock
    SocketFactory socketFactory;
    @Mock
    Socket socket;
    @InjectMocks
    GraphiteSnapshotExporter snapshotExporter;

    @Test
    void exportEmptySnapshotDoNotSentAnything() throws Exception {
        when(applicationSettings.getSettingOrDefault(METRICS_GRAPHITE_SERVER_KEY, String.class)).thenReturn("something");
        when(applicationSettings.isTrue(METRICS_GRAPHITE_EXPORT_ENABLED_KEY)).thenReturn(true);

        snapshotExporter.export(List.of());

        verify(socketFactoryService, times(0)).resolveFromString(anyString());
    }

    @Test
    void exportAttemptWhenExporterDisabledDoesNothing() throws Exception {
        when(applicationSettings.getSettingOrDefault(METRICS_GRAPHITE_SERVER_KEY, String.class)).thenReturn("something");
        when(applicationSettings.isTrue(METRICS_GRAPHITE_EXPORT_ENABLED_KEY)).thenReturn(false);

        snapshotExporter.export(List.of(new MetricsSnapshot(List.of(), -1)));

        verify(socketFactoryService, times(0)).resolveFromString(anyString());
    }

    @Test
    void exportAttemptWhenNoServersSpecifiedDoesNothing() throws Exception {
        when(applicationSettings.getSettingOrDefault(METRICS_GRAPHITE_SERVER_KEY, String.class)).thenReturn("");
        when(applicationSettings.isTrue(METRICS_GRAPHITE_EXPORT_ENABLED_KEY)).thenReturn(true);

        snapshotExporter.export(List.of(new MetricsSnapshot(List.of(), -1)));

        verify(socketFactoryService, times(0)).resolveFromString(anyString());
    }

    @Test
    void exportSmallSnapshotsToOneServerDoesSendToOneSocket() throws Exception {
        var catchingStream = new ByteArrayOutputStream();
        mockCommonSettings();
        when(applicationSettings.getSettingOrDefault(METRICS_GRAPHITE_SERVER_KEY, String.class)).thenReturn(TEST_SOCKE_STRING);
        when(socketFactoryService.resolveFromString("auth")).thenReturn(socketFactory);
        when(socketFactory.createSocket("server", 2001)).thenReturn(socket);
        when(socket.getOutputStream()).thenReturn(catchingStream);

        List<MetricsSnapshot> smallSnapshot = getSimpleSnapshot();
        snapshotExporter.export(smallSnapshot);

        var result = catchingStream.toString(StandardCharsets.UTF_8);
        assertThat(result).isEqualTo("test.instance.testId 1.0 10\n");
    }

    @Test
    void exportSmallSnapshotsToMultipleServerDoesSendDataToOneSocketPerServer() throws Exception {
        var catchingStream1 = new ByteArrayOutputStream();
        var catchingStream2 = new ByteArrayOutputStream();
        var secondSocket = mock(Socket.class);
        mockCommonSettings();
        when(applicationSettings.getSettingOrDefault(METRICS_GRAPHITE_SERVER_KEY, String.class)).thenReturn(TEST_MULTI_SOCKET_STRING);
        when(socketFactoryService.resolveFromString("auth")).thenReturn(socketFactory);
        when(socketFactory.createSocket("server", 2001)).thenReturn(socket);
        when(socketFactory.createSocket("server2", 2002)).thenReturn(secondSocket);
        when(socket.getOutputStream()).thenReturn(catchingStream1);
        when(secondSocket.getOutputStream()).thenReturn(catchingStream2);

        List<MetricsSnapshot> smallSnapshot = getSimpleSnapshot();
        snapshotExporter.export(smallSnapshot);

        assertThat(catchingStream1.toString(StandardCharsets.UTF_8)).isEqualTo("test.instance.testId 1.0 10\n");
        assertThat(catchingStream2.toString(StandardCharsets.UTF_8)).isEqualTo("test.instance.testId 1.0 10\n");
    }

    @Test
    void exportSnapshotsSkipsBrokenSentAttempts() throws Exception {
        var catchingStream = new ByteArrayOutputStream();
        var secondSocket = mock(Socket.class);
        mockCommonSettings();
        when(applicationSettings.getSettingOrDefault(METRICS_GRAPHITE_SERVER_KEY, String.class)).thenReturn(TEST_MULTI_SOCKET_STRING);
        when(socketFactoryService.resolveFromString("auth")).thenReturn(socketFactory);
        when(socketFactory.createSocket("server2", 2002)).thenReturn(socket);
        when(socketFactory.createSocket("server", 2001)).thenReturn(secondSocket);
        when(socket.getOutputStream()).thenReturn(catchingStream);
        when(secondSocket.getOutputStream()).thenThrow(new IOException());

        List<MetricsSnapshot> smallSnapshot = getSimpleSnapshot();
        snapshotExporter.export(smallSnapshot);

        var result = catchingStream.toString(StandardCharsets.UTF_8);
        assertThat(result).isEqualTo("test.instance.testId 1.0 10\n");
    }

    @Test
    void exportBigSnapshotsToOneServerDoesSendWellFormedData() throws Exception {
        var catchingStream = new ByteArrayOutputStream();
        mockCommonSettings();
        when(applicationSettings.getSettingOrDefault(METRICS_GRAPHITE_SERVER_KEY, String.class)).thenReturn(TEST_SOCKE_STRING);
        when(socketFactoryService.resolveFromString("auth")).thenReturn(socketFactory);
        when(socketFactory.createSocket("server", 2001)).thenReturn(socket);
        when(socket.getOutputStream()).thenReturn(catchingStream);

        var samples = List.of(
                new Sample("test", List.of("tag1", "tag2"), List.of("Uber tag 1", "Value;for,second="), 1.0, 1),
                new Sample("test2", List.of("tag1", "tag2=,"), List.of("value1", "value2"), 34.356, 1)
        );
        var advancedSnapshot = new MetricsSnapshot(List.of(
                new MetricSamples(MetricSettings.settings(), null, samples),
                new MetricSamples(MetricSettings.settings(), null, List.of(
                        new Sample("test3", List.of(), List.of(), 1.0, 1),
                        new Sample("test4", List.of(), List.of(), 34.356, 1)
                ))
        ), 5000);
        snapshotExporter.export(List.of(
                makeSimpleSnapshot(samples, 3000),
                makeSimpleSnapshot(samples, 4000),
                advancedSnapshot
        ));

        var result = catchingStream.toString(StandardCharsets.UTF_8);
        assertThat(result).isEqualTo("""
                test.tag1.Uber_tag_1.tag2.Value_for_second_.instance.testId 1.0 3
                test2.tag1.value1.tag2__.value2.instance.testId 34.356 3
                test.tag1.Uber_tag_1.tag2.Value_for_second_.instance.testId 1.0 4
                test2.tag1.value1.tag2__.value2.instance.testId 34.356 4
                test.tag1.Uber_tag_1.tag2.Value_for_second_.instance.testId 1.0 5
                test2.tag1.value1.tag2__.value2.instance.testId 34.356 5
                test3.instance.testId 1.0 5
                test4.instance.testId 34.356 5
                """);
    }

    private void mockCommonSettings() {
        when(applicationSettings.isTrue(METRICS_GRAPHITE_EXPORT_ENABLED_KEY)).thenReturn(true);
        when(applicationSettings.getSetting(SystemSettingKeys.SYSTEM_ID_KEY, String.class)).thenReturn(Optional.of("testId"));
    }

    private static List<MetricsSnapshot> getSimpleSnapshot() {
        var samples = List.of(new Sample("test", List.of(), List.of(), 1.0, 1));
        return List.of(makeSimpleSnapshot(samples, 10000));
    }

    private static MetricsSnapshot makeSimpleSnapshot(List<Sample> samples, int timestamp) {
        return new MetricsSnapshot(List.of(new MetricSamples(MetricSettings.settings(), MetricSamples.Type.COUNTER, samples)), timestamp);
    }
}
