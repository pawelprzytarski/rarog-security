package eu.rarogsoftware.rarog.platform.plugins.telemetry;

import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.EnablePluginAutoconfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnablePluginAutoconfiguration
public class PluginConfiguration {
}
