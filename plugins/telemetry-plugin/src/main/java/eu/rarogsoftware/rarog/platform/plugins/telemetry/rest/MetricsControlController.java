package eu.rarogsoftware.rarog.platform.plugins.telemetry.rest;

import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.security.AdminOnly;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("metrics/control/")
@AdminOnly
@Tag(name = "Telemetry controls")
public class MetricsControlController {
    private final ApplicationSettings applicationSettings;

    public MetricsControlController(@ComponentImport ApplicationSettings applicationSettings) {
        this.applicationSettings = applicationSettings;
    }

    @GetMapping("config")
    @Operation(summary = "Returns current metrics config")
    public Map<String, Object> getStatus() {
        var settings = new HashMap<String, Object>();

        settings.put("enabled", applicationSettings.getSettingOrDefault(MetricSettingKeys.METRICS_ENABLED_KEY, Boolean.class));
        settings.put("snapshotsEnabled", applicationSettings.getSettingOrDefault(MetricSettingKeys.METRICS_SNAPSHOT_ENABLED_KEY, Boolean.class));
        settings.put("snapshotPeriod", applicationSettings.getSettingOrDefault(MetricSettingKeys.METRICS_SNAPSHOT_PERIOD_KEY, Integer.class));
        settings.put("exportPeriod", applicationSettings.getSettingOrDefault(MetricSettingKeys.METRICS_EXPORT_PERIOD_KEY, Integer.class));
        settings.put("exportToGraphiteEnabled", applicationSettings.getSettingOrDefault(MetricSettingKeys.METRICS_GRAPHITE_EXPORT_ENABLED_KEY, Boolean.class));
        settings.put("graphiteServers", applicationSettings.getSettingOrDefault(MetricSettingKeys.METRICS_GRAPHITE_SERVER_KEY, String.class));

        return settings;
    }

    @PutMapping("enabled")
    @Operation(summary = "Allows to enable or disable metrics in system")
    public ResponseEntity<Void> setEnabled(EnabledBean value) {
        applicationSettings.setSetting(MetricSettingKeys.METRICS_ENABLED_KEY, value.enabled);

        return ResponseEntity.accepted().build();
    }

    @PutMapping("snapshot/enabled")
    @Operation(summary = "Allows to enable or disable generating snapshot and exporting metrics to external systems. It do not disable local OpenMetrics page.")
    public ResponseEntity<Void> setSnapshotEnabled(EnabledBean value) {
        applicationSettings.setSetting(MetricSettingKeys.METRICS_SNAPSHOT_ENABLED_KEY, value.enabled);

        return ResponseEntity.accepted().build();
    }

    @PutMapping("snapshot/period")
    @Operation(summary = "Allows to change how often metric snapshots are created")
    public ResponseEntity<Void> setSnapshotPeriod(PeriodBean value) {
        if (value.seconds() <= 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "period cannot be less or equal to 0");
        }
        applicationSettings.setSetting(MetricSettingKeys.METRICS_SNAPSHOT_PERIOD_KEY, value.seconds);

        return ResponseEntity.accepted().build();
    }

    @PutMapping("export/period")
    @Operation(summary = "Allows to change how often metric snapshots are exported")
    public ResponseEntity<Void> setExportPeriod(PeriodBean value) {
        if (value.seconds() <= 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "period cannot be less or equal to 0");
        }
        applicationSettings.setSetting(MetricSettingKeys.METRICS_EXPORT_PERIOD_KEY, value.seconds);

        return ResponseEntity.accepted().build();
    }

    @PutMapping("export/graphite/enabled")
    @Operation(summary = "Allows to disable export to graphite servers (including Rarog Team servers).")
    public ResponseEntity<Void> setGraphiteEnabled(EnabledBean value) {
        applicationSettings.setSetting(MetricSettingKeys.METRICS_GRAPHITE_EXPORT_ENABLED_KEY, value.enabled);

        return ResponseEntity.accepted().build();
    }

    public record EnabledBean(boolean enabled) {
    }

    public record PeriodBean(int seconds) {
    }
}
