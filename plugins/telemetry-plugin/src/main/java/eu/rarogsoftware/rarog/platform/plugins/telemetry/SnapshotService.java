package eu.rarogsoftware.rarog.platform.plugins.telemetry;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.metrics.Histogram;
import eu.rarogsoftware.rarog.platform.api.metrics.HistogramSettings;
import eu.rarogsoftware.rarog.platform.api.metrics.MetricsService;
import eu.rarogsoftware.rarog.platform.api.metrics.MetricsSnapshot;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.task.TaskManager;
import eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys;
import eu.rarogsoftware.rarog.platform.plugins.telemetry.api.SnapshotExporter;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.HomeDirectoryHelper;
import eu.rarogsoftware.rarog.platform.plugins.context.PluginActivated;
import eu.rarogsoftware.rarog.platform.plugins.context.PluginDeactivating;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static java.lang.Boolean.TRUE;

@Component
public class SnapshotService implements ApplicationListener<ApplicationEvent> {
    private final Logger logger = LoggerFactory.getLogger(SnapshotService.class);
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final String SNAPSHOT_NAME_PREFIX = "snapshot_";
    private final ApplicationSettings applicationSettings;
    private final MetricsService metricsService;
    private final HomeDirectoryHelper directoryHelper;
    private final List<SnapshotExporter> localSnapshotExporters;
    private final Histogram snapshotGenerationHistogram;
    private final Histogram snapshotExportHistogram;
    private final TaskManager taskManager;
    private ScheduledFuture<?> snapshotTask;
    private ScheduledFuture<?> exportTask;

    public SnapshotService(@ComponentImport ApplicationSettings applicationSettings,
                           @ComponentImport MetricsService metricsService,
                           @ComponentImport HomeDirectoryHelper directoryHelper,
                           @ComponentImport TaskManager taskManager,
                           List<SnapshotExporter> snapshotExporters) {
        this.applicationSettings = applicationSettings;
        this.metricsService = metricsService;
        this.directoryHelper = directoryHelper;
        this.localSnapshotExporters = snapshotExporters;
        this.snapshotGenerationHistogram = metricsService.createHistogram(HistogramSettings.settings()
                .name("rarog_metrics_snapshot_generation")
                .description("Meta-metric for duration of saving snapshot do hard drive")
                .unit("seconds")
                .buckets(0.001, 0.01, 0.1, 1, 5, 10, 60));
        this.snapshotExportHistogram = metricsService.createHistogram(HistogramSettings.settings()
                .name("rarog_metrics_snapshot_export")
                .description("Meta-metric for duration of exporting saved snapshot to external systems")
                .unit("seconds")
                .buckets(0.01, 0.1, 1, 10, 60, 600, 1200));
        this.taskManager = taskManager;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof PluginActivated) {
            start();
        } else if (event instanceof PluginDeactivating) {
            stop();
        }
    }

    private void start() {
        if (TRUE.equals(applicationSettings.getSettingOrDefault(MetricSettingKeys.METRICS_ENABLED_KEY, Boolean.class))
                && TRUE.equals(applicationSettings.getSettingOrDefault(MetricSettingKeys.METRICS_SNAPSHOT_ENABLED_KEY, Boolean.class))) {
            int snapshotPeriod = applicationSettings.getSettingOrDefault(MetricSettingKeys.METRICS_SNAPSHOT_PERIOD_KEY, Integer.class);
            this.snapshotTask = taskManager.asExecutorService().scheduleAtFixedRate(this::saveSnapshot, snapshotPeriod, snapshotPeriod, TimeUnit.SECONDS);

            int exportPeriod = applicationSettings.getSettingOrDefault(MetricSettingKeys.METRICS_EXPORT_PERIOD_KEY, Integer.class);
            this.exportTask = taskManager.asExecutorService().scheduleAtFixedRate(this::exportSnapshots, exportPeriod, exportPeriod, TimeUnit.SECONDS);
        }
    }

    private void saveSnapshot() {
        try (var timer = snapshotGenerationHistogram.startTimer()) {
            var snapshot = metricsService.snapshot();
            if (snapshot.samples().isEmpty()) {
                logger.debug("Snapshot {} is empty. Saving skipped", snapshot.timestamp());
                return;
            }

            var snapshotFile = getSnapshotFile(snapshot);
            writeSnapshotToFile(snapshot, snapshotFile);
        } catch (Exception e) {
            logger.warn("Failed to monitor snapshot generation", e);
        }
    }

    private void writeSnapshotToFile(MetricsSnapshot snapshot, File snapshotFile) {
        try (var os = new FileOutputStream(snapshotFile)) {
            logger.trace("Saving snapshot {} to file {}", snapshot.timestamp(), snapshotFile);
            MAPPER.writeValue(os, snapshot);
            logger.trace("Saved snapshot {} to file {}", snapshot.timestamp(), snapshotFile);
        } catch (IOException e) {
            logger.warn("Failed to save snapshot {}", snapshot.timestamp(), e);
        }
    }

    @SuppressFBWarnings("PATH_TRAVERSAL_IN")
    private File getSnapshotFile(MetricsSnapshot snapshot) {
        File snapshotDirectory = getSnapshotDirectory();
        var snapshotFile = new File(snapshotDirectory, SNAPSHOT_NAME_PREFIX + snapshot.timestamp());
        snapshotFile.deleteOnExit();
        return snapshotFile;
    }

    private File getSnapshotDirectory() {
        var snapshotDirectory = new File(directoryHelper.getCacheFile(), "metrics");
        snapshotDirectory.mkdirs();
        return snapshotDirectory;
    }

    private void exportSnapshots() {
        try (var timer = snapshotExportHistogram.startTimer()) {
            var directory = getSnapshotDirectory();
            if (!directory.exists() && !directory.isDirectory()) {
                logger.warn("Snapshot directory doesn't exist or is not directory. Cannot export metrics");
                return;
            }
            logger.info("Exporting saved snapshots");
            var snapshotFiles = directory.listFiles((dir, name) -> name.startsWith(SNAPSHOT_NAME_PREFIX));
            if (snapshotFiles == null) {
                return;
            }
            logger.debug("Found {} snapshots", snapshotFiles.length);
            List<MetricsSnapshot> snapshots = readSnapshotsFromDisk(snapshotFiles);
            logger.trace("Read {} snapshots", snapshots.size());
            exportSnapshotWithExporters(snapshots);
            logger.info("Exported {} saved snapshots", snapshotFiles.length);
        } catch (Exception e) {
            logger.warn("Failed to monitor snapshots export", e);
        }
    }

    private List<MetricsSnapshot> readSnapshotsFromDisk(File[] snapshotFiles) {
        List<MetricsSnapshot> snapshots = new ArrayList<>();
        for (var snapshotFile : snapshotFiles) {
            try {
                logger.trace("Exporting snapshot {}", snapshotFile);
                snapshots.add(MAPPER.readValue(snapshotFile, MetricsSnapshot.class));
                logger.trace("Exported snapshot {}. Removing file", snapshotFile);
                Files.delete(snapshotFile.toPath());
            } catch (IOException e) {
                logger.warn("Failed to read snapshot file {}", snapshotFile, e);
            }
        }
        return snapshots;
    }

    private void exportSnapshotWithExporters(List<MetricsSnapshot> snapshots) {
        localSnapshotExporters.forEach(exporter -> {
            logger.trace("Exporting snapshots using exporter {}", exporter.getClass());
            try {
                exporter.export(snapshots);
                logger.trace("Exported snapshots using exporter {}", exporter.getClass());
            } catch (Exception e) {
                logger.warn("Failed to export snapshots with exporter {}", exporter.getClass(), e);
            }
        });
    }

    private void stop() {
        snapshotTask.cancel(false);
        exportTask.cancel(false);
    }
}
