package eu.rarogsoftware.rarog.platform.plugins.telemetry.exporters;

import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.security.secrets.HttpClientAuthConfigService;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.task.TaskManager;
import org.springframework.stereotype.Component;

import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.METRICS_TELEMETRY_EXPORT_ENABLED_KEY;

@Component
public class RarogSoftwareTelemetrySnapshotExporter extends InfluxDbSnapshotExporter{
    private final ApplicationSettings applicationSettings;

    public RarogSoftwareTelemetrySnapshotExporter(@ComponentImport ApplicationSettings applicationSettings,
                                                  @ComponentImport TaskManager taskManager,
                                                  @ComponentImport HttpClientAuthConfigService authConfigService) {
        super(applicationSettings, taskManager, authConfigService);
        this.applicationSettings = applicationSettings;
    }

    @Override
    protected boolean isEnabled() {
        return applicationSettings.isTrue(METRICS_TELEMETRY_EXPORT_ENABLED_KEY);
    }

    @Override
    protected String getListOfServers() {
        return "https://influxdb.rarogsoftware.eu/api/v2/write|buildin:telemetry_clientssl";
    }
}
