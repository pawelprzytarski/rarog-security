package eu.rarogsoftware.rarog.platform.plugins.telemetry.api;

import eu.rarogsoftware.rarog.platform.api.metrics.MetricsSnapshot;

import java.util.List;

public interface SnapshotExporter {
    void export(List<MetricsSnapshot> snapshots) throws Exception;
}
