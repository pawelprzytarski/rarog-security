package eu.rarogsoftware.rarog.platform.plugins.telemetry.api;

import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SettingsInitializer;
import org.springframework.stereotype.Component;

@Component
public final class MetricSettingKeys implements SettingsInitializer {
    public static final String METRICS_EXPORT_PERIOD_KEY = "rarog.metrics.export.period";
    public static final String METRICS_SNAPSHOT_PERIOD_KEY = "rarog.metrics.snapshot.period";
    public static final String METRICS_SNAPSHOT_ENABLED_KEY = "rarog.metrics.snapshot.enabled";
    public static final String METRICS_GRAPHITE_SERVER_KEY = "rarog.metrics.graphite.servers";
    public static final String METRICS_GRAPHITE_EXPORT_ENABLED_KEY = "rarog.metrics.graphite.export.enabled";
    public static final String METRICS_INFLUX_SERVER_KEY = "rarog.metrics.influx.servers";
    public static final String METRICS_INFLUX_EXPORT_ENABLED_KEY = "rarog.metrics.influx.export.enabled";
    public static final String METRICS_TELEMETRY_EXPORT_ENABLED_KEY = "rarog.metrics.rarog.telemetry.enabled";
    public static final String METRICS_ENABLED_KEY = "rarog.metrics.enabled";
    public static final String OPEN_METRIC_ENDPOINT_ENABLED_KEY = "rarog.metrics.open.metric.endpoint.enabled";
    public static final String OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY = "rarog.metrics.open.metric.auth.token";

    @Override
    public void initialize(ApplicationSettings settings) {
        settings.setDefaultSetting(METRICS_EXPORT_PERIOD_KEY, 3600);
        settings.setDefaultSetting(METRICS_SNAPSHOT_PERIOD_KEY, 60);
        settings.setDefaultSetting(METRICS_GRAPHITE_SERVER_KEY, "");
        settings.setDefaultSetting(METRICS_GRAPHITE_EXPORT_ENABLED_KEY, false);
        settings.setDefaultSetting(METRICS_INFLUX_SERVER_KEY, "");
        settings.setDefaultSetting(METRICS_INFLUX_EXPORT_ENABLED_KEY, false);
        settings.setDefaultSetting(METRICS_TELEMETRY_EXPORT_ENABLED_KEY, false);
        settings.setDefaultSetting(METRICS_ENABLED_KEY, true);
        settings.setDefaultSetting(METRICS_SNAPSHOT_ENABLED_KEY, true);
        settings.setDefaultSetting(OPEN_METRIC_ENDPOINT_ENABLED_KEY, false);
    }
}
