package eu.rarogsoftware.rarog.platform.plugins.telemetry.exporters;

import eu.rarogsoftware.rarog.platform.api.metrics.MetricsSnapshot;
import eu.rarogsoftware.rarog.platform.api.metrics.Sample;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.security.secrets.HttpClientAuthConfigService;
import eu.rarogsoftware.rarog.platform.api.security.secrets.HttpClientAuthConfigService.AuthConfig;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys;
import eu.rarogsoftware.rarog.platform.api.task.TaskManager;
import eu.rarogsoftware.rarog.platform.plugins.telemetry.api.SnapshotExporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;

import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.METRICS_INFLUX_EXPORT_ENABLED_KEY;
import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.METRICS_INFLUX_SERVER_KEY;

@Component
public class InfluxDbSnapshotExporter implements SnapshotExporter {
    private static final Logger logger = LoggerFactory.getLogger(InfluxDbSnapshotExporter.class);
    private static final Pattern INVALID_INFLUX_CHARS = Pattern.compile("[,\n= ]");
    private static String randomUUID = null;
    private final ApplicationSettings applicationSettings;
    private final TaskManager taskManager;
    private final HttpClientAuthConfigService authConfigService;

    public InfluxDbSnapshotExporter(@ComponentImport ApplicationSettings applicationSettings,
                                    @ComponentImport TaskManager taskManager,
                                    @ComponentImport HttpClientAuthConfigService authConfigService) {
        this.applicationSettings = applicationSettings;
        this.taskManager = taskManager;
        this.authConfigService = authConfigService;
    }

    @Override
    public void export(List<MetricsSnapshot> snapshots) throws Exception {
        String serversString = getListOfServers();
        if (isEnabled()
                && StringUtils.hasText(serversString) && !snapshots.isEmpty()) {
            var serverStrings = serversString.split("(?<!\\\\);");
            var serverId = applicationSettings.getSetting(SystemSettingKeys.SYSTEM_ID_KEY, String.class)
                    .orElseGet(InfluxDbSnapshotExporter::getStaticRandomUUID);
            for (var serverString : serverStrings) {
                sendToServer(snapshots, serverId, serverString.replace("\\;", ";"));
            }
        }
    }

    protected boolean isEnabled() {
        return applicationSettings.isTrue(METRICS_INFLUX_EXPORT_ENABLED_KEY);
    }

    protected String getListOfServers() {
        return applicationSettings.getSettingOrDefault(METRICS_INFLUX_SERVER_KEY, String.class);
    }

    private void sendToServer(List<MetricsSnapshot> snapshots, String serverId, String serverName) {
        var serverSplit = serverName.split("\\|", 2);
        var url = serverSplit[0];
        var authenticationData = serverSplit.length == 2 ? serverSplit[1] : "";
        try {
            pushToServer(snapshots, url, authenticationData, serverId);
        } catch (Exception e) {
            logger.warn("Failed to push snapshots to {}. Skipping sent to this server.", url, e);
        }
    }

    private static String getStaticRandomUUID() {
        if (randomUUID == null) {
            randomUUID = UUID.randomUUID().toString();
        }
        return randomUUID;
    }

    private void pushToServer(List<MetricsSnapshot> snapshots, String url, String authenticationData, String serverId) throws IOException {
        logger.trace("Exporting snapshots to server {}", url);
        try (var influxWriter = new InfluxWriter(authConfigService.resolveFromString(authenticationData), url)) {
            var matcher = INVALID_INFLUX_CHARS.matcher("");
            for (var snapshot : snapshots) {
                for (var metricFamilySamples : snapshot.samples()) {
                    for (var sample : metricFamilySamples.samples()) {
                        writeMetric(serverId, influxWriter, matcher, snapshot, sample);
                    }
                }
                logger.trace("Exported snapshot {}", snapshot.timestamp());
            }
            logger.trace("Exported snapshots to server {}", url);
        } catch (InterruptedException e) {
            logger.info("Metrics export task interrupted");
            Thread.currentThread().interrupt();
        }
    }

    private static void writeMetric(String serverId, InfluxWriter influxWriter, Matcher matcher, MetricsSnapshot snapshot, Sample sample) throws IOException, InterruptedException {
        var metricBuffer = new StringBuilder();
        matcher.reset(sample.name());
        metricBuffer.append(matcher.replaceAll("_"));
        for (int i = 0; i < sample.labelsNames().size(); ++i) {
            matcher.reset(sample.labelsNames().get(i));
            metricBuffer.append(",").append(matcher.replaceAll("_"));
            matcher.reset(sample.labels().get(i));
            metricBuffer.append("=").append(matcher.replaceAll("_"));
        }
        metricBuffer.append(",instance=").append(serverId);
        metricBuffer.append(" value=").append(sample.value()).append(" ").append(snapshot.timestamp()).append("\n");
        influxWriter.write(metricBuffer.toString());
    }

    private class InfluxWriter implements AutoCloseable {
        private static final Logger logger = LoggerFactory.getLogger(InfluxWriter.class);
        private static final int INFLUX_BATCH_SIZE = 5000;
        private final HttpClient httpClient;
        private final URI url;
        private final AuthConfig authConfig;
        private int bufferedCount = 0;
        private StringBuilder metricsBuffer = new StringBuilder();

        public InfluxWriter(AuthConfig authConfig, String url) {
            this.url = getUrl(url);
            var httpClientBuilder = HttpClient.newBuilder()
                    .cookieHandler(new CookieManager(null, CookiePolicy.ACCEPT_NONE))
                    .executor(taskManager.asExecutorService());
            httpClient = authConfig.configureHttpClient(httpClientBuilder)
                    .build();
            this.authConfig = authConfig;
        }

        private URI getUrl(String url) {
            if (url.contains("?")) {
                url = url + "&precision=ms";
            } else {
                url = url + "?precision=ms";
            }
            return URI.create(url);
        }

        public void write(String metric) throws IOException, InterruptedException {
            metricsBuffer.append(metric);
            bufferedCount++;
            if (bufferedCount >= INFLUX_BATCH_SIZE) {
                sendRequestToInflux();
            }
        }

        private void sendRequestToInflux() throws IOException, InterruptedException {
            var postBuilder = HttpRequest.newBuilder()
                    .version(HttpClient.Version.HTTP_2)
                    .uri(url)
                    .header(HttpHeaders.CONTENT_ENCODING, "gzip")
                    .POST(HttpRequest.BodyPublishers.ofByteArray(getCompressed()));

            metricsBuffer = new StringBuilder();
            bufferedCount = 0;

            var response = httpClient.send(authConfig.configureHttpRequest(postBuilder.build()), HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != HttpStatus.NO_CONTENT.value()) {
                logger.warn("Failed to send request to influx. Error code `{}` message `{}`", response.statusCode(), response.body());
                throw new IOException("Failed to write influx data. HTTP error: " + response.statusCode());
            }
        }

        private byte[] getCompressed() throws IOException {
            byte[] bytes = metricsBuffer.toString().getBytes(StandardCharsets.UTF_8);
            try (var byteArrayOutputStream = new ByteArrayOutputStream()) {
                try (var outputStream = new GZIPOutputStream(byteArrayOutputStream)) {
                    outputStream.write(bytes);
                } // because GZipOutputStream must be closed to return all data. Flushing is not helping. It must be closed.
                return byteArrayOutputStream.toByteArray();
            }
        }

        @Override
        public void close() throws IOException, InterruptedException {
            if (!metricsBuffer.isEmpty()) {
                sendRequestToInflux();
            }
        }
    }
}
