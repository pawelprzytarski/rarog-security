package eu.rarogsoftware.rarog.platform.plugins.telemetry.rest;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.*;

@RestController
@AnonymousAllowed
@RequestMapping("expose/openmetrics/")
@Tag(name = "OpenMetrics Telemetry")
public class OpenMetricsExposerController {
    private static final String AUTHORIZATION_METHOD = "Code ";
    private static final String OPEN_METRIC_EOL = "\n";
    private static final int KILOBYTE_SIZE = 1;
    private static final int MEGABYTE_SIZE = KILOBYTE_SIZE * 1024;
    private final Logger logger = LoggerFactory.getLogger(OpenMetricsExposerController.class);
    private final MetricsService metricsService;
    private final ApplicationSettings applicationSettings;
    private final Histogram metricCreationHistogram;
    private final Counter failedAttemptsCounter;
    private final Counter exposedCounter;
    private final Counter exceptionCounter;
    private final Histogram responseSizeHistogram;

    public OpenMetricsExposerController(@ComponentImport MetricsService metricsService,
                                        @ComponentImport ApplicationSettings applicationSettings) {
        this.metricsService = metricsService;
        this.applicationSettings = applicationSettings;
        this.metricCreationHistogram = metricsService.createHistogram(HistogramSettings.settings()
                .name("rarog_open_metrics_expose")
                .description("Meta-metric for duration of exposing metrics in OpenMetric format")
                .unit("seconds")
                .buckets(0.001, 0.01, 0.1, 1, 10, 60));
        this.responseSizeHistogram = metricsService.createHistogram(HistogramSettings.settings()
                .name("rarog_open_metrics_expose")
                .description("Meta-metric for response size of exposed metrics in OpenMetric format")
                .unit("kilobytes")
                .buckets(KILOBYTE_SIZE, KILOBYTE_SIZE * 10, MEGABYTE_SIZE / 2, MEGABYTE_SIZE, MEGABYTE_SIZE * 10));
        this.failedAttemptsCounter = metricsService.createCounter(MetricSettings.settings()
                .name("rarog_open_metrics_forbidden_attempts")
                .description("Meta-metric for count of forbidden access attempts to OpenMetric exposed metrics"));
        this.exposedCounter = metricsService.createCounter(MetricSettings.settings()
                .name("rarog_open_metrics_exposed")
                .description("Meta-metric for count of successful access attempts to OpenMetric exposed metrics"));
        this.exceptionCounter = metricsService.createCounter(MetricSettings.settings()
                .name("rarog_open_metrics_exceptions")
                .description("Meta-metric for count of access attempts to OpenMetric exposed metrics that resulted in exception"));

    }

    @GetMapping(value = "/", produces = {"application/openmetrics-text;version=1.0.0;charset=utf-8", MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary = "Returns gathered metrics in OpenMetrics format")
    public byte[] getOpenMetricsText(HttpServletRequest request) throws Exception {
        throwNotFoundIfDisabled();
        authorizeRequest(request);
        return getOpenMetricFormattedSamples();
    }

    private byte[] getOpenMetricFormattedSamples() throws Exception {
        try (var timer = metricCreationHistogram.startTimer();
             var byteArrayOutputStream = new ByteArrayOutputStream();
             var writer = new OutputStreamWriter(byteArrayOutputStream, StandardCharsets.UTF_8)) {
            logger.debug("Generating OpenMetric format response");

            for (var sampleFamily : metricsService.sample()) {
                if (sampleFamily.samples().isEmpty()) {
                    continue;
                }
                writeMetricSettings(writer, sampleFamily);
                writeMetricSamples(writer, sampleFamily);
            }

            writer.write("# EOF\n");
            writer.flush();
            byte[] responseBytes = byteArrayOutputStream.toByteArray();
            logger.debug("Generated response of length {}", responseBytes.length);
            exposedCounter.increment();
            responseSizeHistogram.observe(responseBytes.length / 1024.0);
            return responseBytes;
        } catch (Exception e) {
            exceptionCounter.increment();
            throw e;
        }
    }

    private static void writeMetricSamples(OutputStreamWriter writer, MetricSamples sampleFamily) throws IOException {
        for (var sample : sampleFamily.samples()) {
            writer.append(sample.name());
            writeLabels(writer, sample);
            writer.append(" ").append(formatValue(sample.value()));
            if (sample.timestamp() > 0) {
                writer.append(" ").append(Long.toString(sample.timestamp()));
            }
            writer.write(OPEN_METRIC_EOL);
        }
    }

    private static String formatValue(Double value) {
        return Double.toString(value);
    }

    private static void writeLabels(OutputStreamWriter writer, Sample sample) throws IOException {
        if (!sample.labels().isEmpty()) {
            writer.append("{")
                    .append(sample.labelsNames().get(0))
                    .append("=\"")
                    .append(escapeOpenMetricsString(sample.labels().get(0)))
                    .append("\"");
            for (int i = 1; i < sample.labels().size(); i++) {
                writer.append(",")
                        .append(sample.labelsNames().get(i))
                        .append("=\"")
                        .append(escapeOpenMetricsString(sample.labels().get(i)))
                        .append("\"");
            }
            writer.write("}");
        }
    }

    private static void writeMetricSettings(OutputStreamWriter writer, MetricSamples sampleFamily) throws IOException {
        var metricName = sampleFamily.settings().name();
        var metricHelp = escapeOpenMetricsString(sampleFamily.settings().description());
        var metricUnit = escapeOpenMetricsString(sampleFamily.settings().unit());
        writeMetricSettingRow(writer, "# TYPE ", metricName, getTypeString(sampleFamily));
        if (metricHelp != null) {
            writeMetricSettingRow(writer, "# HELP ", metricName, metricHelp);
        }
        if (metricUnit != null) {
            writeMetricSettingRow(writer, "# UNIT ", metricName, metricUnit);
        }
    }

    private static void writeMetricSettingRow(OutputStreamWriter writer, String settingName, String metricName, String settingValue) throws IOException {
        writer.append(settingName).append(metricName).append(" ").append(settingValue).append(OPEN_METRIC_EOL);
    }

    private static String getTypeString(MetricSamples sampleFamily) {
        return switch (sampleFamily.type()) {
            case INFO -> "info";
            case COUNTER -> "counter";
            case GAUGE -> "gauge";
            case GAUGE_HISTOGRAM -> "gaugehistogram";
            case HISTOGRAM -> "histogram";
            case STATE_SET -> "stateset";
            case SUMMARY -> "summary";
            case UNKNOWN -> "unknown";
        };
    }

    private static String escapeOpenMetricsString(String toEscape) {
        return StringUtils.replaceEach(toEscape,
                new String[]{"\n", "\"", "\\"},
                new String[]{"\\n", "\\\"", "\\\\"});
    }

    private void throwNotFoundIfDisabled() {
        if (!(applicationSettings.isTrue(METRICS_ENABLED_KEY)
                && applicationSettings.isTrue(OPEN_METRIC_ENDPOINT_ENABLED_KEY))) {
            logger.warn("Attempted to access OpenMetrics endpoint but metrics are disabled. Did you misconfigured something?");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Metrics are not enabled");
        }
    }

    private void authorizeRequest(HttpServletRequest request) {
        var authToken = applicationSettings.getPropertyBackedSettingAsString(OPEN_METRIC_ENDPOINT_AUTH_TOKEN_KEY);
        if (authToken != null) {
            var authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
            if (authorization == null || !authorization.startsWith(AUTHORIZATION_METHOD)) {
                failedAttemptsCounter.increment();
                logger.warn("Attempted to access OpenMetrics endpoint without authentication token. Attempt blocked");
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Metrics require authorization. Contact your system administrator for authentication token.");
            }
            var receivedToken = authorization.substring(AUTHORIZATION_METHOD.length());
            if (!BCrypt.checkpw(receivedToken, authToken)) {
                failedAttemptsCounter.increment();
                logger.warn("Attempted to access OpenMetrics endpoint with incorrect token. Attempt blocked");
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Metrics require authorization. Contact your system administrator for authentication token.");
            }
        }
    }
}
