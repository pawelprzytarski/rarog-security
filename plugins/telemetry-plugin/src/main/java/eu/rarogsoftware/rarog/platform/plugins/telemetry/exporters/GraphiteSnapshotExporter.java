package eu.rarogsoftware.rarog.platform.plugins.telemetry.exporters;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.metrics.MetricsSnapshot;
import eu.rarogsoftware.rarog.platform.api.metrics.Sample;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.security.secrets.AuthConfiguredSocketFactoryService;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys;
import eu.rarogsoftware.rarog.platform.plugins.telemetry.api.SnapshotExporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.METRICS_GRAPHITE_EXPORT_ENABLED_KEY;
import static eu.rarogsoftware.rarog.platform.plugins.telemetry.api.MetricSettingKeys.METRICS_GRAPHITE_SERVER_KEY;

@Component
@SuppressFBWarnings("UNENCRYPTED_SOCKET")
public class GraphiteSnapshotExporter implements SnapshotExporter {
    private static final Logger logger = LoggerFactory.getLogger(GraphiteSnapshotExporter.class);
    private static final Pattern INVALID_GRAPHITE_CHARS = Pattern.compile("[^a-zA-Z0-9_-]");
    private static String randomUUID = null;
    private final ApplicationSettings applicationSettings;
    private final AuthConfiguredSocketFactoryService socketFactoryService;

    public GraphiteSnapshotExporter(@ComponentImport ApplicationSettings applicationSettings,
                                    @ComponentImport AuthConfiguredSocketFactoryService socketFactoryService) {
        this.applicationSettings = applicationSettings;
        this.socketFactoryService = socketFactoryService;
    }

    @Override
    public void export(List<MetricsSnapshot> snapshots) throws Exception {
        var serversString = applicationSettings.getSettingOrDefault(METRICS_GRAPHITE_SERVER_KEY, String.class);
        if (applicationSettings.isTrue(METRICS_GRAPHITE_EXPORT_ENABLED_KEY) && !snapshots.isEmpty()) {
            if (StringUtils.hasText(serversString)) {
                var serverStrings = serversString.split("(?<!\\\\);");
                logger.trace("Exporting snapshots to {} servers", serverStrings.length);
                var serverId = applicationSettings.getSetting(SystemSettingKeys.SYSTEM_ID_KEY, String.class)
                        .orElseGet(GraphiteSnapshotExporter::getStaticRandomUUID);
                for (var serverString : serverStrings) {
                    try {
                        pushToServer(snapshots, serverId, serverString);
                    } catch (Exception e) {
                        logger.warn("Failed to push snapshots to {}. Skipping sent to server.", serverString, e);
                    }
                }
            } else {
                logger.warn("Graphite exporter is enabled but no servers are defined.");
            }
        }
    }

    private void pushToServer(List<MetricsSnapshot> snapshots, String serverId, String serverString) throws IOException {
        var serverStringSplit = serverString.replace("\\;", ";").split("\\|", 2);
        var serverSplit = serverStringSplit[0].split(":", 2);
        var host = serverSplit[0];
        var port = serverSplit.length == 2 ? Integer.parseInt(serverSplit[1]) : 2003;
        var authString = serverStringSplit.length == 2 ? serverStringSplit[1] : "";

        logger.trace("Exporting snapshots to server {}:{}", host, port);
        try (var s = socketFactoryService.resolveFromString(authString).createSocket(host, port);
             var writer = new BufferedWriter(new PrintWriter(new OutputStreamWriter(s.getOutputStream(), StandardCharsets.UTF_8)))) {
            pushSnapshots(snapshots, serverId, writer);
        }
        logger.trace("Exported snapshots to server {}:{}", host, port);
    }

    private static void pushSnapshots(List<MetricsSnapshot> snapshots, String serverId, BufferedWriter writer) throws IOException {
        var matcher = INVALID_GRAPHITE_CHARS.matcher("");
        for (var snapshot : snapshots) {
            logger.trace("Exporting snapshot {}", snapshot.timestamp());
            long snapshotTime = snapshot.timestamp() / 1000;
            for (var metricFamilySamples : snapshot.samples()) {
                for (var sample : metricFamilySamples.samples()) {
                    writeMetric(serverId, writer, matcher, snapshotTime, sample);
                }
            }
            logger.trace("Exported snapshot {}", snapshot.timestamp());
        }
    }

    private static void writeMetric(String serverId, BufferedWriter writer, Matcher matcher, long snapshotTime, Sample sample) throws IOException {
        matcher.reset(sample.name());
        writer.write(matcher.replaceAll("_"));
        for (int i = 0; i < sample.labelsNames().size(); ++i) {
            matcher.reset(sample.labelsNames().get(i));
            writer.write("." + matcher.replaceAll("_"));
            matcher.reset(sample.labels().get(i));
            writer.write("." + matcher.replaceAll("_"));
        }
        writer.write(".instance." + serverId);
        writer.write(" " + sample.value() + " " + snapshotTime + "\n");
    }

    private static String getStaticRandomUUID() {
        if (randomUUID == null) {
            randomUUID = UUID.randomUUID().toString();
        }
        return randomUUID;
    }

}
