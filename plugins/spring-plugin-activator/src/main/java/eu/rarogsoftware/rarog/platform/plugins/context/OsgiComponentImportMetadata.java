package eu.rarogsoftware.rarog.platform.plugins.context;

import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;

record OsgiComponentImportMetadata(Class<?> type, ComponentImport annotation) {
}
