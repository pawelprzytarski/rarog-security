package eu.rarogsoftware.rarog.platform.plugins.context;

import org.springframework.context.ApplicationEvent;

public class PluginPurge extends ApplicationEvent {
    public PluginPurge(Object source) {
        super(source);
    }
}
