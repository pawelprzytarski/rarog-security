package eu.rarogsoftware.rarog.platform.plugins.context;

import org.springframework.context.ApplicationEvent;

public class PluginDeactivating extends ApplicationEvent {
    public PluginDeactivating(Object source) {
        super(source);
    }
}
