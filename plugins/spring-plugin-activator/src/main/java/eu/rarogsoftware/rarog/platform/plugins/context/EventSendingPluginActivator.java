package eu.rarogsoftware.rarog.platform.plugins.context;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginActivator;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Set;

public class EventSendingPluginActivator implements PluginActivator {
    private final PluginActivator delegate;
    private final ApplicationEventPublisher eventPublisher;

    public EventSendingPluginActivator(PluginActivator delegate, ApplicationEventPublisher eventPublisher) {
        this.delegate = delegate;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void activate() {
        delegate.activate();
        eventPublisher.publishEvent(new PluginActivated(this));
    }

    @Override
    public void deactivate() {
        eventPublisher.publishEvent(new PluginDeactivating(this));
        delegate.deactivate();
    }

    @Override
    public void purge() {
        eventPublisher.publishEvent(new PluginPurge(this));
        delegate.deactivate();
    }

    @Override
    public Set<FeatureDescriptor> getFeatureDescriptors() {
        return delegate.getFeatureDescriptors();
    }
}
