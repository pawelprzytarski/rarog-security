package eu.rarogsoftware.rarog.platform.plugins.context.env;

import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;

/**
 * Allows for customization of plugin's {@link Environment} prior to the
 * spring context being refreshed.
 * <p>
 * EnvironmentPostProcessor implementations have to be registered in
 * {@code META-INF/rarog.factories}, using the fully qualified name of this class as the
 * key. Implementations may implement the {@link org.springframework.core.Ordered Ordered}
 * interface or use an {@link org.springframework.core.annotation.Order @Order} annotation
 * if they wish to be invoked in specific order.
 */
@FunctionalInterface
public interface EnvironmentPostProcessor {

    /**
     * Post-process the given {@code environment}.
     *
     * @param environment    the environment to post-process
     * @param resourceLoader resource loader of plugin context
     */
    void postProcessEnvironment(ConfigurableEnvironment environment, ResourceLoader resourceLoader);
}
