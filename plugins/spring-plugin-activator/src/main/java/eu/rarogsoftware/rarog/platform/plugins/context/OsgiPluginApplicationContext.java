package eu.rarogsoftware.rarog.platform.plugins.context;

import org.osgi.framework.BundleContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.util.ClassUtils;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

class OsgiPluginApplicationContext extends ClassPathXmlApplicationContext {
    private final ClassLoader osgiClassLoader;
    private final BundleContext bundleContext;
    private ClassLoader classLoader = null;

    public OsgiPluginApplicationContext(BundleContext bundleContext, Class<?> activatorClass) throws BeansException {
        super("/META-INF/spring/beans.xml", activatorClass);
        this.bundleContext = bundleContext;
        this.osgiClassLoader = activatorClass.getClassLoader();
    }

    @Override
    public void refresh() throws BeansException, IllegalStateException {
        if (bundleContext != null) { //little hack on spring being spring and doing all magic in constructor
            super.refresh();
        }
    }

    @Override
    public String getApplicationName() {
        return bundleContext.getBundle().getSymbolicName();
    }

    @Override
    protected DefaultListableBeanFactory createBeanFactory() {
        return new OsgiContextAwareBeanFactory(getInternalParentBeanFactory(), () -> bundleContext, () -> osgiClassLoader);
    }

    @Override
    public ClassLoader getClassLoader() {
        if (classLoader == null) {
            // do little hacking because spring cannot read things from OSGi bundles correctly
            classLoader = new MultiParentClassLoader(osgiClassLoader, new BundleResourceClassLoader(bundleContext), ClassUtils.getDefaultClassLoader());
        }
        return classLoader;
    }

    @Override
    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    @Override
    protected ResourcePatternResolver getResourcePatternResolver() {
        return new PathMatchingResourcePatternResolver(this) {
            @Override
            protected Set<Resource> doFindPathMatchingFileResources(Resource rootDirResource, String subPattern)
                    throws IOException {
                URL resourceURL = rootDirResource.getURL();
                if ("bundle".equals(resourceURL.getProtocol())) {
                    return findPathMatchingFileResourcesInBundle(resourceURL, subPattern);
                }
                return super.doFindPathMatchingFileResources(rootDirResource, subPattern);
            }

            private HashSet<Resource> findPathMatchingFileResourcesInBundle(URL resourceURL, String subPattern) {
                HashSet<Resource> resources = new HashSet<>();
                var pathMatcher = getPathMatcher();
                int substringStart = resourceURL.getFile().length() + 1;
                var enumeration = bundleContext.getBundle((int) Double.parseDouble(resourceURL.getHost().split("_")[1])).findEntries(resourceURL.getFile(), "*", true);
                while (enumeration.hasMoreElements()) {
                    var url = enumeration.nextElement();
                    var path = url.getFile().substring(substringStart);
                    if (pathMatcher.match(subPattern, path)) {
                        resources.add(new UrlResource(url));
                    }
                }
                return resources;
            }
        };
    }

}
