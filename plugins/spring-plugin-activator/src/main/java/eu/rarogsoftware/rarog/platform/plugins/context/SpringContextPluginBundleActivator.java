package eu.rarogsoftware.rarog.platform.plugins.context;

import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginActivator;
import eu.rarogsoftware.rarog.platform.plugins.context.env.EnvironmentPostProcessor;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.core.io.support.SpringFactoriesLoader;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

/**
 * Implementation of {@link BundleActivator} that out-of-box provide all needed functionality for spring based plugins.
 * This activator provides full set of functionality for spring based plugin without need for further configuration.
 *
 * <p>
 * It automatically:
 *     <ul>
 *         <li>initializes spring context for provided `META-INF/spring/beans.xml`,</li>
 *         <li>provides support for {@link ExportComponent} and
 *         {@link ComponentImport} annotations,</li>
 *         <li>exports and imports spring components to/from OSGi,</li>
 *         <li>invokes basic initializers available in API package,</li>
 *         <li>exposes {@link PluginActivator} if none is present in spring context,</li>
 *         <li>exposes {@link FeatureDescriptor}s available in spring context,</li>
 *         <li>cleans spring context and exposed services on bundle stop.</li>
 *     </ul>
 * </p>
 * <p>
 *     By default it exposes {@link SimplePluginActivator} if none is found in spring context. If plugin
 *     expose implementation of {@link PluginActivator} it will be used instead.
 * </p>
 * <p>
 *     Exposed spring beans have additional property `BeanName` with original bean name, that allow filtering
 *     for bean name in OSGi.
 * </p>
 *
 * @see BundleActivator
 * @see SimplePluginActivator
 * @see PluginActivator
 * @since 1.0.0
 */
public class SpringContextPluginBundleActivator implements BundleActivator {
    private final Logger logger = LoggerFactory.getLogger(SpringContextPluginBundleActivator.class);
    protected BundleContext bundleContext;
    protected OsgiPluginApplicationContext springContext;

    @Override
    public final void start(BundleContext context) throws Exception {
        bundleContext = context;
        var currentBundle = context.getBundle();
        logger.info("Starting bundle [{}] {};version={}", currentBundle.getBundleId(), currentBundle.getSymbolicName(), currentBundle.getVersion());
        springContext = new OsgiPluginApplicationContext(bundleContext, getClass());
        registerPluginActivator(context);
        logger.info("Started bundle [{}] {};version={}", currentBundle.getBundleId(), currentBundle.getSymbolicName(), currentBundle.getVersion());
    }

    private void startSpringContext(BundleContext context) {
        initializeContext();
        exportComponentsToOsgi(context);
        logger.debug("Starting spring context");
        springContext.start();
    }

    private void initializeContext() {
        var factories = SpringFactoriesLoader.forResourceLocation("META-INF/rarog.factories", getClass().getClassLoader());
        runInitializers(factories);
        initEnvironment(factories);
        springContext.refresh();
    }

    private void initEnvironment(SpringFactoriesLoader factories) {
        var environment = new StandardEnvironment();
        var postProcessors = factories.load(EnvironmentPostProcessor.class);
        postProcessors.sort(AnnotationAwareOrderComparator.INSTANCE);
        postProcessors.forEach(postProcessor -> postProcessor.postProcessEnvironment(environment, springContext));
        springContext.setEnvironment(environment);
    }

    private void runInitializers(SpringFactoriesLoader factories) {
        var initializers = factories.load(ApplicationContextInitializer.class);
        initializers.sort(AnnotationAwareOrderComparator.INSTANCE);
        initializers.forEach(initializer -> initializer.initialize(springContext));
    }

    private void exportComponentsToOsgi(BundleContext context) {
        logger.debug("Exporting spring components");
        var exportComponents = springContext.getBeansWithAnnotation(ExportComponent.class);
        exportComponents.forEach((beanName, component) -> {
            var interfaces = Arrays.stream(component.getClass().getInterfaces()).map(Class::getName).toList().toArray(new String[0]);
            logger.trace("Exporting {} as {} for interfaces {}", component.getClass(), beanName, interfaces);
            context.registerService(interfaces, component, new Hashtable<>(Map.of("BeanName", beanName)));
            logger.trace("Exported {} as {}", component.getClass(), beanName);
        });
    }

    private void registerPluginActivator(BundleContext context) {
        logger.debug("Exporting plugin activator");
        context.registerService(PluginActivator.class, new LateStartPluginActivator(context, springContext), new Hashtable<>());
    }

    @Override
    public final void stop(BundleContext context) throws Exception {
        var currentBundle = context.getBundle();
        logger.debug("Stopping bundle [{}] {};version={}", currentBundle.getBundleId(), currentBundle.getSymbolicName(), currentBundle.getVersion());
        springContext.stop();
        springContext.close();
        bundleContext = null;
        logger.info("Stopped bundle [{}] {};version={}", currentBundle.getBundleId(), currentBundle.getSymbolicName(), currentBundle.getVersion());
    }

    public BundleContext getBundleContext() {
        return bundleContext;
    }

    public OsgiPluginApplicationContext getSpringContext() {
        return springContext;
    }

    private class LateStartPluginActivator implements PluginActivator {
        private final BundleContext context;
        private final OsgiPluginApplicationContext springContext;
        private EventSendingPluginActivator delegate;

        public LateStartPluginActivator(BundleContext context,
                                        OsgiPluginApplicationContext springContext) {
            this.context = context;
            this.springContext = springContext;
        }

        private PluginActivator getDelegate() {
            if (delegate == null) {
                startSpringContext(context);
                PluginActivator pluginActivator;
                try {
                    pluginActivator = springContext.getBean(PluginActivator.class);
                } catch (NoSuchBeanDefinitionException e) {
                    logger.info("Using default plugin activator because no plugin activator is defined");
                    pluginActivator = new SimplePluginActivator(getBundleContext(), getSpringContext());
                }
                delegate = new EventSendingPluginActivator(pluginActivator, springContext);
            }
            return delegate;
        }

        @Override
        public void activate() {
            getDelegate().activate();
        }

        @Override
        public void deactivate() {
            getDelegate().deactivate();
        }

        @Override
        public Set<FeatureDescriptor> getFeatureDescriptors() {
            return getDelegate().getFeatureDescriptors();
        }
    }
}
