package eu.rarogsoftware.rarog.platform.plugins.context;

import eu.rarogsoftware.rarog.platform.api.commons.ClassHelpers;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Supplier;

class OsgiImportBeanSupplier implements Supplier<Object> {
    private final Logger logger = LoggerFactory.getLogger(OsgiImportBeanSupplier.class);
    private final OsgiComponentImportMetadata osgiImport;
    private final Supplier<BundleContext> bundleContextSupplier;

    public OsgiImportBeanSupplier(OsgiComponentImportMetadata osgiImport, Supplier<BundleContext> bundleContextSupplier) {
        this.osgiImport = osgiImport;
        this.bundleContextSupplier = bundleContextSupplier;
    }

    @Override
    public Object get() {
        var bundleContext = bundleContextSupplier.get();
        if (bundleContext == null) {
            logger.error("Failed to load non optional OSGi bean {} because bundleContext is null", osgiImport.type());
            throw new BeanCreationException("Cannot load bean from OSGi context because context is null");
        }
        try {
            var references = getServiceReferences(bundleContext);
            if (references.length == 0) {
                logger.error("Failed to load non optional OSGi bean {} because service for such bean is not registered", osgiImport.type());
                throw new BeanCreationException("Cannot load bean from OSGi context because not service is registered");
            }
            return Arrays.stream(references)
                    .map(reference -> createProxy(bundleContext, reference))
                    .filter(Objects::nonNull)
                    .findAny()
                    .orElseThrow(() -> {
                        logger.error("Failed to load non optional OSGi bean {} because service for such bean is not registered", osgiImport.type());
                        return new BeanCreationException("Cannot load bean from OSGi context because not service is registered");
                    });
        } catch (InvalidSyntaxException e) {
            throw new BeanCreationException("Cannot load bean from OSGi context because OSGi error", e);
        }
    }

    private ServiceReference<?>[] getServiceReferences(BundleContext bundleContext) throws InvalidSyntaxException {
        String beanName = osgiImport.annotation().value();
        if (!StringUtils.hasText(beanName)) {
            return bundleContext.getServiceReferences(osgiImport.type().getName(), null);
        }
        return bundleContext.getServiceReferences(osgiImport.type().getName(), "(BeanName=" + beanName + ")");
    }

    private Object createProxy(BundleContext bundleContext, ServiceReference<?> reference) {
        var object = bundleContext.getService(reference);
        if (object == null) {
            return null;
        }
        var interfaces = ClassHelpers.getImplementedInterfaces(object.getClass());
        interfaces.add(DisposableBean.class);
        return Proxy.newProxyInstance(getClass().getClassLoader(), interfaces.toArray(new Class[0]), ((proxy, method, args) -> {
            if ("destroy".equals(method.getName())) {
                bundleContextSupplier.get().ungetService(reference);
                return null;
            } else {
                try {
                    return method.invoke(object, args);
                } catch (InvocationTargetException e) {
                    throw e.getCause();
                } catch (IllegalAccessException e) {
                    logger.error("Failed to invoke method {} of object {}", method.getName(), object.getClass());
                    throw new IllegalStateException("Provided proxied object cannot be invoked by proxy", e);
                }
            }
        }));
    }
}
