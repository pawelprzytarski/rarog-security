package eu.rarogsoftware.rarog.platform.plugins.context;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginActivator;
import org.osgi.framework.BundleContext;

import java.util.HashSet;
import java.util.Set;

/**
 * The simples implementation of {@link PluginActivator} for spring based plugins.
 *
 * @since 1.0.0
 */
public class SimplePluginActivator implements PluginActivator {
    private final BundleContext bundleContext;
    private final OsgiPluginApplicationContext springContext;

    public SimplePluginActivator(BundleContext bundleContext, OsgiPluginApplicationContext springContext) {
        this.bundleContext = bundleContext;
        this.springContext = springContext;
    }

    public BundleContext getBundleContext() {
        return bundleContext;
    }

    public OsgiPluginApplicationContext getSpringContext() {
        return springContext;
    }

    @Override
    public void activate() {
        // do nothing
    }

    @Override
    public void deactivate() {
        // do nothing
    }

    @Override
    public Set<FeatureDescriptor> getFeatureDescriptors() {
        return new HashSet<>(getSpringContext().getBeansOfType(FeatureDescriptor.class).values());
    }
}
