package eu.rarogsoftware.rarog.platform.plugins.context;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Hack on spring to read resources from spring bundles.
 */
@SuppressFBWarnings("URLCONNECTION_SSRF_FD")
class BundleResourceClassLoader extends ClassLoader {
    private final BundleContext bundleContext;

    BundleResourceClassLoader(BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        throw new ClassNotFoundException();
    }

    @Override
    public URL getResource(String name) {
        return getBundleStream()
                .map(bundle -> bundle.getResource(name))
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
    }

    private Stream<Bundle> getBundleStream() {
        return Stream.of(bundleContext.getBundle(), bundleContext.getBundle(0));
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        List<Enumeration<URL>> list = new ArrayList<>();
        for (var bundle : getBundleStream().toList()) {
            Enumeration<URL> resources = bundle.getResources(name);
            if (resources != null && resources.hasMoreElements()) {
                list.add(resources);
            }
        }
        if (list.isEmpty()) {
            return Collections.emptyEnumeration();
        }
        return new Enumeration<>() {
            final Iterator<Enumeration<URL>> iterator = list.listIterator();
            Enumeration<URL> current = iterator.next();

            @Override
            public boolean hasMoreElements() {
                var hasMoreElements = current.hasMoreElements();
                while (!hasMoreElements) {
                    if (!iterator.hasNext()) {
                        return false;
                    }
                    current = iterator.next();
                    hasMoreElements = current.hasMoreElements();
                }
                return true;
            }

            @Override
            public URL nextElement() {
                return current.nextElement();
            }
        };
    }

    @Override
    public Stream<URL> resources(String name) {
        Enumeration<URL> resources = null;
        try {
            resources = getResources(name);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        final var enumeration = resources;
        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(
                        new Iterator<>() {
                            public URL next() {
                                return enumeration.nextElement();
                            }

                            public boolean hasNext() {
                                return enumeration.hasMoreElements();
                            }
                        },
                        Spliterator.ORDERED), false);
    }

    @Override
    public InputStream getResourceAsStream(String name) {
        try {
            URL resource = getResource(name);
            if (resource == null) {
                return null;
            }
            return resource.openStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
