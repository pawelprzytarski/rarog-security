package eu.rarogsoftware.rarog.platform.plugins.context;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Stream;

class MultiParentClassLoader extends ClassLoader {

    private final ClassLoader[] otherParents;

    public MultiParentClassLoader(ClassLoader firstParent, ClassLoader... otherParents) {
        super(firstParent);
        this.otherParents = otherParents;
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        var resources = super.getResources(name);
        for (var parent : otherParents) {
            if (resources.hasMoreElements()) {
                return resources;
            }
            resources = parent.getResources(name);
        }
        return resources;
    }

    @Override
    public URL getResource(String name) {
        var resource = super.getResource(name);
        for (var parent : otherParents) {
            if (resource != null) {
                return resource;
            }
            resource = parent.getResource(name);
        }
        return resource;
    }

    @Override
    public InputStream getResourceAsStream(String name) {
        var resource = super.getResourceAsStream(name);
        for (var parent : otherParents) {
            if (resource != null) {
                return resource;
            }
            resource = parent.getResourceAsStream(name);
        }
        return resource;
    }

    @Override
    public Stream<URL> resources(String name) {
        List<Stream<URL>> streams = new ArrayList<>();
        streams.add(super.resources(name));
        for (var parent : otherParents) {
            streams.add(parent.resources(name));
        }
        return streams.stream().reduce(Stream::concat).get();
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        try {
            return super.loadClass(name);
        } catch (ClassNotFoundException e) {
            for (var parent : otherParents) {
                try {
                    return parent.loadClass(name);
                } catch (ClassNotFoundException ignore) {
                }
            }
            throw e;
        }
    }
}
