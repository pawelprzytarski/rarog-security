package eu.rarogsoftware.rarog.platform.plugins.context.env;

import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.ResourceLoader;

import java.util.Collections;

public class ConfigDataEnvironmentPostProcessor implements EnvironmentPostProcessor, Ordered {
    public static final int ORDER = Ordered.HIGHEST_PRECEDENCE + 10;

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, ResourceLoader resourceLoader) {
        org.springframework.boot.context.config.ConfigDataEnvironmentPostProcessor.applyTo(
                environment,
                resourceLoader,
                null,
                Collections.emptyList());
    }

    @Override
    public int getOrder() {
        return ORDER;
    }
}
