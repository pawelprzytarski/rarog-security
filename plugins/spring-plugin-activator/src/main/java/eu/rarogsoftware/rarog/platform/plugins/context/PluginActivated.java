package eu.rarogsoftware.rarog.platform.plugins.context;

import org.springframework.context.ApplicationEvent;

public class PluginActivated extends ApplicationEvent {
    public PluginActivated(Object source) {
        super(source);
    }
}
