package eu.rarogsoftware.rarog.platform.plugins.context;

import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class OsgiContextAwareBeanFactory extends DefaultListableBeanFactory {
    private final transient Logger myLogger = LoggerFactory.getLogger(OsgiContextAwareBeanFactory.class);
    private final transient Supplier<BundleContext> bundleContextSupplier;
    private final transient Supplier<ClassLoader> beanClassLoaderSupplier;

    public OsgiContextAwareBeanFactory(BeanFactory parentBeanFactory, Supplier<BundleContext> bundleContextSupplier, Supplier<ClassLoader> beanClassLoaderSupplier) {
        super(parentBeanFactory);
        this.bundleContextSupplier = bundleContextSupplier;
        this.beanClassLoaderSupplier = beanClassLoaderSupplier;
    }

    private static List<Method> getMethods(ClassLoader beanClassLoader, String factoryBeanClassName) throws ClassNotFoundException {
        List<Method> methods = new ArrayList<>();
        var classToScan = beanClassLoader.loadClass(factoryBeanClassName);
        while (classToScan != null) {
            methods.addAll(Arrays.asList(classToScan.getDeclaredMethods()));
            classToScan = classToScan.getSuperclass();
        }
        return methods;
    }

    @Override
    public ClassLoader getBeanClassLoader() {
        return new MultiParentClassLoader(beanClassLoaderSupplier.get(), super.getBeanClassLoader());
    }

    @Override
    public void registerBeanDefinition(String beanName, BeanDefinition beanDefinition) throws BeanDefinitionStoreException {
        if (!this.containsBean(beanName)) {
            myLogger.debug("Scanning bean {} of class {} for OSGi imports", beanName, beanDefinition.getBeanClassName());
            importOsgiServices(beanDefinition);
        }
        super.registerBeanDefinition(beanName, beanDefinition);
    }

    private void importOsgiServices(BeanDefinition beanDefinition) {
        Collection<OsgiComponentImportMetadata> classesToImport = getComponentImports(beanDefinition).stream()
                .filter(osgiImport -> {
                    var beanName = getBeanName(osgiImport);
                    if(!containsBeanDefinition(beanName)){
                        return true;
                    }
                    var existingBeanDefinition = getBeanDefinition(beanName);
                    if (!(existingBeanDefinition instanceof GenericBeanDefinition genericBeanDefinition)) {
                        return true;
                    }
                    return !(genericBeanDefinition.getInstanceSupplier() instanceof OsgiImportBeanSupplier);
                })
                .toList();
        if (!classesToImport.stream()
                .filter(osgiImport -> !osgiImport.annotation().lateBind())
                .allMatch(osgiImport -> {
                    String beanName = StringUtils.hasText(osgiImport.annotation().value()) ? osgiImport.annotation().value() : null;
                    try {
                        ServiceReference<?>[] allServiceReferences;
                        if (beanName == null) {
                            allServiceReferences = bundleContextSupplier.get().getAllServiceReferences(osgiImport.type().getName(), null);
                        } else {
                            allServiceReferences = bundleContextSupplier.get().getAllServiceReferences(osgiImport.type().getName(), "(BeanName=" + beanName + ")");
                        }
                        return allServiceReferences != null && allServiceReferences.length > 0;
                    } catch (InvalidSyntaxException e) {
                        myLogger.warn("Failed to retrieve service from OSGi", e);
                        return false;
                    }
                })) {
            myLogger.error("Failed to bind OSGi imports for bean class {} using {}", beanDefinition.getBeanClassName(), classesToImport);
            throw new BeanDefinitionStoreException("Failed to bind OSGi imports");
        }
        classesToImport.forEach(osgiImport -> {
            var beanName = getBeanName(osgiImport);
            var instanceSupplier = new OsgiImportBeanSupplier(osgiImport, bundleContextSupplier);
            var osgiImportBeanDefinition = new GenericBeanDefinition();
            osgiImportBeanDefinition.setBeanClass(osgiImport.type());
            osgiImportBeanDefinition.setAutowireMode(AutowireCapableBeanFactory.AUTOWIRE_NO);
            osgiImportBeanDefinition.setLazyInit(true);
            osgiImportBeanDefinition.setSynthetic(true);
            osgiImportBeanDefinition.setDependencyCheck(AbstractBeanDefinition.DEPENDENCY_CHECK_NONE);
            osgiImportBeanDefinition.setInstanceSupplier(instanceSupplier);
            super.registerBeanDefinition(beanName, osgiImportBeanDefinition);
        });
    }

    private static String getBeanName(OsgiComponentImportMetadata osgiImport) {
        return StringUtils.hasText(osgiImport.annotation().value()) ? osgiImport.annotation().value() : osgiImport.type().getName();
    }

    private Set<OsgiComponentImportMetadata> getComponentImports(BeanDefinition beanDefinition) {
        Set<OsgiComponentImportMetadata> classesToImport = new HashSet<>();
        try {
            ClassLoader beanClassLoader = getBeanClassLoader() != null ? getBeanClassLoader() : getClass().getClassLoader();
            Class<?> clazz;
            String beanClassName = beanDefinition.getBeanClassName();
            if (!StringUtils.hasText(beanClassName)) {
                if (StringUtils.hasText(beanDefinition.getFactoryBeanName()) && StringUtils.hasText(beanDefinition.getFactoryMethodName())) {
                    var factoryBeanClass = getBeanDefinition(beanDefinition.getFactoryBeanName()).getBeanClassName();
                    var factoryMethod = getMethods(beanClassLoader, factoryBeanClass).stream()
                            .filter(method -> method.getAnnotation(Bean.class) != null && method.getName().equals(beanDefinition.getFactoryMethodName()))
                            .findAny()
                            .orElseThrow(ClassNotFoundException::new);
                    classesToImport.addAll(Arrays.stream(factoryMethod.getParameters())
                            .filter(parameter -> parameter.getAnnotation(ComponentImport.class) != null)
                            .map(parameter -> new OsgiComponentImportMetadata(parameter.getType(), parameter.getAnnotation(ComponentImport.class)))
                            .collect(Collectors.toSet()));
                    clazz = factoryMethod.getReturnType();
                } else {
                    myLogger.warn("Failed to read bean imports for {}", beanDefinition);
                    return Collections.emptySet();
                }
            } else {
                clazz = beanClassLoader.loadClass(beanClassName);
            }
            while (clazz != null && !Object.class.equals(clazz)) {
                classesToImport.addAll(Stream.concat(
                                Arrays.stream(clazz.getDeclaredFields())
                                        .filter(field -> field.getAnnotation(ComponentImport.class) != null)
                                        .map(field -> new OsgiComponentImportMetadata(field.getType(), field.getAnnotation(ComponentImport.class))),
                                Arrays.stream(clazz.getDeclaredConstructors())
                                        .flatMap(constructor -> Arrays.stream(constructor.getParameters()))
                                        .filter(parameter -> parameter.getAnnotation(ComponentImport.class) != null)
                                        .map(parameter -> new OsgiComponentImportMetadata(parameter.getType(), parameter.getAnnotation(ComponentImport.class)))
                        )
                        .collect(Collectors.toSet()));
                clazz = clazz.getSuperclass();
            }
        } catch (ClassNotFoundException e) {
            myLogger.warn("Failed to load bean for scanning. Bean: {}", beanDefinition, e);
            return Collections.emptySet();
        }
        return classesToImport;
    }
}
