package eu.rarogsoftware.rarog.platform.plugins.backdoor;

import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.EnablePluginAutoconfiguration;
import eu.rarogsoftware.rarog.platform.plugins.context.PluginActivated;
import eu.rarogsoftware.rarog.platform.plugins.context.PluginDeactivating;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
@EnablePluginAutoconfiguration
public class PluginConfiguration {
    private final Logger logger = LoggerFactory.getLogger(PluginConfiguration.class);

    @Bean
    ApplicationListener<ApplicationEvent> periodicLogger() {
        return new ApplicationListener<>() {
            private ScheduledExecutorService executorService;

            @Override
            public void onApplicationEvent(ApplicationEvent event) {
                if (event instanceof PluginActivated) {
                    executorService = new ScheduledThreadPoolExecutor(1);
                    executorService.scheduleAtFixedRate(() -> logger.error(">>>>>>>> !!!BACKDOOR PLUGIN INSTALLED. DO NOT USE IT IN PRODUCTION ENVIRONMENT!!! <<<<<<<"), 0, 1, TimeUnit.MINUTES);
                }
                if (event instanceof PluginDeactivating) {
                    executorService.shutdown();
                    try {
                        if (!executorService.awaitTermination(10, TimeUnit.SECONDS)) {
                            executorService.shutdownNow();
                        }
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        };
    }
}
