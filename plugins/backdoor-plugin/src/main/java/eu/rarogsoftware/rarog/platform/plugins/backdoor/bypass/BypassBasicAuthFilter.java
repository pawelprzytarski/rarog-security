package eu.rarogsoftware.rarog.platform.plugins.backdoor.bypass;

import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginSecurityFilterMapping;
import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.CsrfHelper;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaService;
import eu.rarogsoftware.rarog.platform.api.user.management.*;
import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.web.SecurityFilterMapping;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.web.header.HeaderWriterFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@SecurityFilterMapping(placement = PluginSecurityFilterMapping.Placement.BEFORE,
        relativeFilter = HeaderWriterFilter.class)
@Component
public class BypassBasicAuthFilter extends OncePerRequestFilter {
    private static final String HEADER = "Authorization";
    private static final String PREFIX = "Basic ";
    private final UserManager userManager;

    public BypassBasicAuthFilter(@ComponentImport UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (isTokenPresent(request)) {
            try {
                var token = validateToken(request);
                if (request.getSession(false) != null) {
                    throw new UserManagementException("Basic auth works only for stateless calls");
                }
                setUpSpringAuthentication(token);
                request.setAttribute(CsrfHelper.DISABLE_XSRF_ATTRIBUTE, true);
                request.setAttribute(MfaService.EXCLUDE_REQUEST_FROM_MFA_ATTRIBUTE, true);
                filterChain.doFilter(request, response);
            } catch (UserManagementException e) {
                response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
            } finally {
                SecurityContextHolder.clearContext();
                var session = request.getSession(false);
                if (session != null) {
                    session.invalidate();
                }
            }
            return;
        }
        filterChain.doFilter(request, response);
    }

    private RarogUser validateToken(HttpServletRequest request) throws UserManagementException {
        var token = request.getHeader(HEADER).replace(PREFIX, "");
        var credentials = new String(Base64.getDecoder().decode(token)).split(":", 2);
        var user = userManager.getUser(credentials[0]);
        if (!BCrypt.checkpw(credentials[1], user.getPassword())) {
            throw new UserNotFoundException();
        }
        return user;
    }

    private void setUpSpringAuthentication(RarogUser user) {
        List<GrantedAuthority> authorities = new ArrayList<>(user.getAuthorities());
        authorities.add(AuthenticationType.ORGANIC.getAuthority());

        var auth = new UsernamePasswordAuthenticationToken(
                user,
                null,
                authorities);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    private boolean isTokenPresent(HttpServletRequest request) {
        var authenticationHeader = request.getHeader(HEADER);
        return authenticationHeader != null && authenticationHeader.startsWith(PREFIX);
    }
}
