package eu.rarogsoftware.rarog.platform.plugins.backdoor.rest;

import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.security.CsrfDisabled;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(value = "application/settings",
        produces = MediaType.APPLICATION_JSON_VALUE)
@AnonymousAllowed
@CsrfDisabled
public class ApplicationSettingsBackdoorController {
    private final ApplicationSettings applicationSettings;

    public ApplicationSettingsBackdoorController(@ComponentImport ApplicationSettings applicationSettings) {
        this.applicationSettings = applicationSettings;
    }

    @GetMapping("{name}")
    public SettingsBean getSetting(@PathVariable("name") String settingName) {
        var settingValue = applicationSettings.getSettingOrDefault(settingName, String.class);
        return new SettingsBean(settingName, settingValue);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> setSetting(@RequestBody SettingsBean settingsBean) throws URISyntaxException {
        if(settingsBean.name() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Setting name cannot be empty");
        }
        applicationSettings.setSetting(settingsBean.name(), settingsBean.value());
        return ResponseEntity.created(new URI(settingsBean.name())).build();
    }

    public record SettingsBean(String name, Object value) {
    }
}
