package eu.rarogsoftware.rarog.platform.plugins.backdoor.rest;

import eu.rarogsoftware.rarog.platform.api.security.CsrfDisabled;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "test/logging",
        produces = MediaType.APPLICATION_JSON_VALUE)
@AnonymousAllowed
@CsrfDisabled
public class TestWatcherController {
    private final Logger testWatcherLogger = LoggerFactory.getLogger("TestWatcherLogger");

    @PostMapping
    public ResponseEntity<Void> logActivity(@RequestBody LogBean logBean) {
        testWatcherLogger.info("{} test `{}`", logBean.before ? "Starting" : "Finished", logBean.testName);
        if (StringUtils.hasText(logBean.error)) {
            testWatcherLogger.warn("Test `{}` failed with error: {}", logBean.testName, logBean.error);
        }
        return ResponseEntity.noContent().build();
    }

    public record LogBean(String testName, boolean before, String error) {
    }
}
