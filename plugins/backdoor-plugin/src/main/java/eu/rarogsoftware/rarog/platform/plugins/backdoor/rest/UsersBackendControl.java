package eu.rarogsoftware.rarog.platform.plugins.backdoor.rest;

import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.security.CsrfDisabled;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.api.user.management.UserManagementException;
import eu.rarogsoftware.rarog.platform.api.user.management.UserManager;
import eu.rarogsoftware.rarog.platform.api.user.management.UserRole;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.List;

@AnonymousAllowed
@RestController
@RequestMapping(value = "users",
        produces = MediaType.APPLICATION_JSON_VALUE)
@CsrfDisabled
public class UsersBackendControl {
    private final UserManager userManager;

    public UsersBackendControl(@ComponentImport UserManager userManager) {
        this.userManager = userManager;
    }

    @PostMapping("create")
    void createUser(@RequestBody CreateUserRequest request) {
        try {
            userManager.createUser(StandardUser.builder()
                    .username(request.username())
                    .password(request.password())
                    .roles(new HashSet<>(request.roles()))
                    .mfaEnabled(false)
                    .build());
        } catch (UserManagementException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("{username}")
    void deleteUser(@PathVariable("username") String username) {
        try {
            userManager.deleteUser(username);
        } catch (UserManagementException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    record CreateUserRequest(String username, String password, List<UserRole> roles) {
    }
}
