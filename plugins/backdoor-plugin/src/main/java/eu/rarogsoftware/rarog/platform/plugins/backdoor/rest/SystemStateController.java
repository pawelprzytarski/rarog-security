package eu.rarogsoftware.rarog.platform.plugins.backdoor.rest;

import eu.rarogsoftware.rarog.platform.api.user.management.RarogUser;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "system/state", produces = MediaType.APPLICATION_JSON_VALUE)
public class SystemStateController {
    @GetMapping("account/loggedin")
    AccountState getAccountState(@AuthenticationPrincipal RarogUser rarogUser) {
        if (rarogUser != null) {
            return new AccountState(
                    rarogUser.getUsername(),
                    rarogUser.isHuman() ? "human" : "nonhuman",
                    SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().map(GrantedAuthority::getAuthority).toList(),
                    rarogUser.getClass().getName());
        } else {
            return new AccountState(null, null, null, null);
        }
    }

    record AccountState(String name, String type, List<String> authorities, String userClassType) {
    }
}
