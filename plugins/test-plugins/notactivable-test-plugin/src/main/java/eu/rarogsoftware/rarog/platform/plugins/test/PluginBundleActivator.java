package eu.rarogsoftware.rarog.platform.plugins.test;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PluginBundleActivator implements BundleActivator {
    private final Logger logger = LoggerFactory.getLogger(PluginBundleActivator.class);
    public BundleContext bundleContext;

    @Override
    public final void start(BundleContext context) throws Exception {
        logger.info("Starting test plugin");
        logger.info("Test plugin started");
    }

    @Override
    public final void stop(BundleContext context) throws Exception {
        bundleContext = null;
        logger.info("Test plugin stopped");
    }

    public static class TestFeatureDescriptor implements FeatureDescriptor {

    }
}