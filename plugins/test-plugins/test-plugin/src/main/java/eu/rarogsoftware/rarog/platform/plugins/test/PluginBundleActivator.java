package eu.rarogsoftware.rarog.platform.plugins.test;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginActivator;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.plugins.func.test.export.ExampleServiceInterface;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Set;

public class PluginBundleActivator implements BundleActivator {
    private final Logger logger = LoggerFactory.getLogger(PluginBundleActivator.class);

    private static void attemptUsingExampleService(BundleContext context) {
        var reference = context.getServiceReference(ExampleServiceInterface.class);
        assert reference != null;
        var service = context.getService(reference);
        assert service != null;
        service.someMethod();
        context.ungetService(reference);
    }

    @Override
    public final void start(BundleContext context) throws Exception {
        logger.info("Starting test plugin");
        context.registerService(PluginActivator.class, new PluginActivator() {

            @Override
            public void activate() {
                logger.info("Test plugin activated");
                attemptUsingExampleService(context);
                setApplicationSetting(context, "testPluginSettingValue_enabled_" + Math.random());
            }

            private void setApplicationSetting(BundleContext context, String settingValue) {
                var reference = context.getServiceReference(ApplicationSettings.class);
                assert reference != null;
                var service = context.getService(reference);
                assert service != null;
                service.setSetting("test.plugin.setting", settingValue);
                context.ungetService(reference);
            }

            @Override
            public void deactivate() {
                logger.info("Test plugin deactivated");
                setApplicationSetting(context, "testPluginSettingValue_disabled_" + Math.random());
            }

            @Override
            public void purge() {
                logger.info("Test plugin purged");
                setApplicationSetting(context, "purged");
            }

            @Override
            public Set<FeatureDescriptor> getFeatureDescriptors() {
                return Collections.singleton(new TestFeatureDescriptor());
            }
        }, new Hashtable<>());
        logger.info("Test plugin started");
    }

    @Override
    public final void stop(BundleContext context) throws Exception {
        logger.info("Test plugin stopped");
    }

    public static class TestFeatureDescriptor implements FeatureDescriptor {

    }
}