package eu.rarogsoftware.rarog.platform.plugins.func.test;

import java.io.IOException;
import java.util.Objects;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

public class TestPluginFilter implements Filter {

  private static final Logger logger = LoggerFactory.getLogger(TestPluginFilter.class);

  @Override
  public void doFilter(
      ServletRequest request,
      ServletResponse response,
      FilterChain chain) throws IOException, ServletException {

    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;
    res.addHeader("CHAIN_BREAKER_FUNC_TEST_FILTERED", "DARK_GOD_IS_ANGRY");
    String shouldBeATeapot = req.getHeader("RAROG_BREAKS_THE_CHAINS");
    if (Objects.equals(shouldBeATeapot, "YES")) {
      res.sendError(HttpStatus.I_AM_A_TEAPOT.value());
      return;
    }

    logger.trace(
        "[FILTER 1] Backdoor Logging Request  {} : {}", req.getMethod(),
        req.getRequestURI());
    chain.doFilter(request, response);
    logger.trace(
        "[FILTER 1] Backdoor Logging Response :{}",
        res.getContentType());
  }
}