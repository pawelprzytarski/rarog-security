package eu.rarogsoftware.rarog.platform.plugins.func.test.db;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;

import java.io.Serial;
import java.sql.Types;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

public class QTestTable extends com.querydsl.sql.RelationalPathBase<QTestTable> {
    public static final String TABLE_NAME = "plugin_test_table";
    public static final QTestTable testTable = new QTestTable(TABLE_NAME);
    @Serial
    private static final long serialVersionUID = -6546730029860787153L;
    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath name = createString("name");

    public final StringPath test = createString("test");

    public final com.querydsl.sql.PrimaryKey<QTestTable> testPk = createPrimaryKey(id);

    public QTestTable(String variable) {
        super(QTestTable.class, forVariable(variable), "public", TABLE_NAME);
        addMetadata();
    }

    public QTestTable(String variable, String schema, String table) {
        super(QTestTable.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTestTable(String variable, String schema) {
        super(QTestTable.class, forVariable(variable), schema, TABLE_NAME);
        addMetadata();
    }

    public QTestTable(Path<? extends QTestTable> path) {
        super(path.getType(), path.getMetadata(), "public", TABLE_NAME);
        addMetadata();
    }

    public QTestTable(PathMetadata metadata) {
        super(QTestTable.class, metadata, "public", TABLE_NAME);
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.INTEGER).withSize(10).notNull());
        addMetadata(name, ColumnMetadata.named("name").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(test, ColumnMetadata.named("test").withIndex(2).ofType(Types.VARCHAR).withSize(255));
    }
}

