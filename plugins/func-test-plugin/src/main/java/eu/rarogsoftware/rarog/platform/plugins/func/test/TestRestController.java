package eu.rarogsoftware.rarog.platform.plugins.func.test;

import eu.rarogsoftware.commons.database.connection.DatabaseConnectionProvider;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventService;
import eu.rarogsoftware.rarog.platform.api.security.AdminOnly;
import eu.rarogsoftware.rarog.platform.plugins.func.test.db.QTestTable;
import eu.rarogsoftware.rarog.platform.plugins.func.test.events.FuncTestEvent;
import eu.rarogsoftware.rarog.platform.plugins.func.test.events.FuncTestEventListener;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowedLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Map;

@RequestMapping("/test")
@AnonymousAllowed(AnonymousAllowedLevel.CORE)
@RestController
public class TestRestController {
    private final Logger logger = LoggerFactory.getLogger(TestRestController.class);
    private final DatabaseConnectionProvider databaseConnectionProvider;
    private final EventService eventService;
    private final FuncTestEventListener eventListener;

    public TestRestController(@ComponentImport DatabaseConnectionProvider databaseConnectionProvider,
                              @ComponentImport EventService eventService,
                              FuncTestEventListener eventListener) {
        this.databaseConnectionProvider = databaseConnectionProvider;
        this.eventService = eventService;
        this.eventListener = eventListener;
    }

    @GetMapping("/")
    public Map<String, String> testEndpoint(Principal principal) {
        logger.info("TestRestController.testEndpoint was called.");
        return Map.of(
                "loggedInUser", principal != null ? principal.getName() : "",
                "serverStatus", "Ok"
        );
    }

    @GetMapping("/database")
    public TestResultBean testDatabase() {
        var testValue = "Test Value " + Math.random();
        var testName = "Test Name " + Math.random();
        databaseConnectionProvider.getConnection(query ->
                query.insert(QTestTable.testTable)
                        .set(QTestTable.testTable.name, testName)
                        .set(QTestTable.testTable.test, testValue)
                        .execute()
        );

        var readValue = databaseConnectionProvider.getConnection(query -> query.select(QTestTable.testTable.test)
                .from(QTestTable.testTable)
                .where(QTestTable.testTable.name.eq(testName))
                .orderBy(QTestTable.testTable.id.desc())
                .fetchFirst());

        return new TestResultBean(testValue, readValue, testName);
    }

    @GetMapping("/database/count")
    public TestTableRowCount getTableRowsCount() {
        var count = databaseConnectionProvider.getConnection(query -> query.select(QTestTable.testTable.count())
                .from(QTestTable.testTable)
                .fetchOne());

        return new TestTableRowCount(count);
    }

    @PostMapping("/event/send/{eventClass}/{eventType}")
    public void sendSyncTestEvent(@PathVariable String eventClass, @PathVariable String eventType) {
        EventService.EventType type = EventService.EventType.valueOf(eventType.toUpperCase().trim());
        Object payload;
        switch (eventClass) {
            case "test" -> payload = new FuncTestEvent();
            case "string" -> payload = "Rarog is the greatest god of all times :)";
            case "object" -> payload = new Object();
            default -> payload = null;
        }
        logger.info("Sending Event of class {} with type {}", eventClass, eventType);
        eventService.publishEvent(payload, type);
    }

    @PostMapping("/event/reset")
    public void resetEventCounts() {
        logger.info("Resetting event count.");
        eventListener.reset();
    }

    @GetMapping("/event/count/{eventClass}")
    public ResponseEntity<EventCountBean> getTestEventCount(@PathVariable String eventClass) {
        switch (eventClass) {
            case "test" -> {
                return ResponseEntity.ok(new EventCountBean(eventListener.getFuncTestEventsCount()));
            }
            case "string" -> {
                return ResponseEntity.ok(new EventCountBean(eventListener.getStringEventsCount()));
            }
            case "object" -> {
                return ResponseEntity.ok(new EventCountBean(eventListener.getObjectEventsCount()));
            }
            default -> {
                return ResponseEntity.notFound().build();
            }
        }
    }

    @GetMapping("overriddenServletTest")
    public String servletTestSpringMoreImportant() {
        return "Spring servlet response";
    }

    @GetMapping("overridingServletTest")
    @AdminOnly
    public String servletTestSpringLessImportant() {
        return "Spring servlet response";
    }

    record TestResultBean(String insertedValue, String readValue, String key) {
    }

    record TestTableRowCount(long count) {
    }

    record EventCountBean(int count) {
    }
}
