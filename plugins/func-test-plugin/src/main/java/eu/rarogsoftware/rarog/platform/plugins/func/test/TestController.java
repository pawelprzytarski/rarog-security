package eu.rarogsoftware.rarog.platform.plugins.func.test;

import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowedLevel;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import static org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter.XFRAME_OPTIONS_HEADER;

@Controller
public class TestController {
    @GetMapping("/test")
    @AnonymousAllowed(AnonymousAllowedLevel.CORE)
    public String testView(Model model) {
        model.addAttribute("test", "TestTest");
        return "test/test";
    }

    @GetMapping("/test/customTemplateResolver")
    @AnonymousAllowed(AnonymousAllowedLevel.CORE)
    public String testCustomTemplateResolver(Model model) {
        model.addAttribute("test", "TestTest");
        return "test/customerTemplateResolver";
    }

    @GetMapping("/test/customViewResolver")
    @AnonymousAllowed(AnonymousAllowedLevel.CORE)
    public String testCustomViewResolver(Model model) {
        model.addAttribute("test", "TestTest");
        return "test/customViewResolver";
    }

    @GetMapping("test/iframe")
    @AnonymousAllowed(AnonymousAllowedLevel.CORE)
    public String testIframePage(Model model, HttpServletResponse response) {
        response.setHeader(XFRAME_OPTIONS_HEADER, "SAMEORIGIN");
        model.addAttribute("test", "Iframe content");
        return "test/test";
    }
}
