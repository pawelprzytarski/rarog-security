package eu.rarogsoftware.rarog.platform.plugins.func.test;

import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.plugins.func.test.export.ExampleServiceInterface;
import org.springframework.stereotype.Component;

@ExportComponent
@Component
public class ExampleExportedService implements ExampleServiceInterface {
}
