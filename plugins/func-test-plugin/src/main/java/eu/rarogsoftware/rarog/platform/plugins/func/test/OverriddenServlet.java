package eu.rarogsoftware.rarog.platform.plugins.func.test;

import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.AuthorizationManagerAware;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
public class OverriddenServlet extends HttpServlet implements AuthorizationManagerAware {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.getWriter().write("Overridden servlet response");
    }

    @Override
    public Optional<AuthorizationManager<RequestAuthorizationContext>> getAuthorizationManager(HttpServletRequest request) {
        return Optional.of((authentication, object) -> new AuthorizationDecision(true));
    }
}
