package eu.rarogsoftware.rarog.platform.plugins.func.test;

import eu.rarogsoftware.rarog.platform.api.i18n.I18NextResourceDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.plugins.database.DatabaseMigrationsDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventListener;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventListenersDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.templates.TemplateResolverDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.templates.ViewResolverDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.HandlerMappingDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.ResourceResolverDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginFilterMappingDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginFilterMappingDescriptorBuilder;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AdminPanelItem;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AdminPanelItemsDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.UiFragment;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.UiFragmentsDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.PluginServletMappingDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.ServletMapping;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsDescriptor;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaStore;
import jakarta.servlet.DispatcherType;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.resource.ResourceResolver;
import org.springframework.web.servlet.resource.ResourceResolverChain;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.templateresolver.AbstractConfigurableTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresource.ITemplateResource;
import org.thymeleaf.templateresource.StringTemplateResource;

import java.util.*;

@Configuration
public class TestConfiguration {
    @Bean
    @ExportComponent
    public Component2 component2(RandomComponent randomComponent, @ComponentImport("DefaultMfaStore") MfaStore mfaStore) {
        return new Component2(mfaStore);
    }

    @Bean("restRequestMappingHandlerMapping")
    RequestMappingHandlerMapping requestMappingHandlerMapping() {
        RequestMappingHandlerMapping requestMappingHandlerMapping = new RequestMappingHandlerMapping();
        requestMappingHandlerMapping.setPathPrefixes(Map.of(
                "/plugin/func-test/", clazz -> !MergedAnnotations.from(clazz.getAnnotations()).isPresent(RestController.class),
                "/rest/func-test/1.0/", clazz -> MergedAnnotations.from(clazz.getAnnotations()).isPresent(RestController.class)));
        return requestMappingHandlerMapping;
    }

    @Bean
    public HandlerMappingDescriptor handlerMappingDescriptor(List<HandlerMapping> handlerMappings) {
        return new HandlerMappingDescriptor("/func-test/", handlerMappings, true);
    }

    @Bean
    public ITemplateResolver testResolver() {
        return new AbstractConfigurableTemplateResolver() {
            @Override
            protected ITemplateResource computeTemplateResource(IEngineConfiguration configuration, String ownerTemplate, String template, String resourceName, String characterEncoding, Map<String, Object> templateResolutionAttributes) {
                if ("test/customerTemplateResolver".equals(template)) {
                    return new StringTemplateResource("Custom template content");
                }
                return null;
            }
        };
    }

    @Bean
    public ViewResolver viewResolver() {
        return (viewName, locale) -> {
            if ("test/customViewResolver".equals(viewName)) {
                return (View) (model, request, response) -> {
                    response.setContentType(MediaType.TEXT_PLAIN_VALUE);
                    try (var os = response.getOutputStream()) {
                        os.print("Custom view resolver content");
                    }
                };
            }
            return null;
        };
    }

    @Bean
    public TemplateResolverDescriptor pluginTemplateResolver(List<ITemplateResolver> templateResolvers) {
        return new TemplateResolverDescriptor(() -> templateResolvers);
    }

    @Bean
    public ViewResolverDescriptor viewResolverDescriptor(List<ViewResolver> templateResolvers) {
        return new ViewResolverDescriptor(() -> templateResolvers);
    }

    @Bean
    public ResourceResolverDescriptor resourceResolverDescriptor() {
        return new ResourceResolverDescriptor(new ResourceResolver() {
            @Override
            public Resource resolveResource(HttpServletRequest request, String requestPath, List<? extends Resource> locations, ResourceResolverChain chain) {
                if ("p/eu.rarogsoftware.rarog.platform.plugins:func-test-plugin/testStaticResource".equals(requestPath)) {
                    return new UrlResource(getClass().getClassLoader().getResource("/static/test.txt"));
                }
                return chain.resolveResource(request, requestPath, locations);
            }

            @Override
            public String resolveUrlPath(String resourcePath, List<? extends Resource> locations, ResourceResolverChain chain) {
                if ("p/eu.rarogsoftware.rarog.platform.plugins:func-test-plugin/testStaticResource".equals(resourcePath)) {
                    return resourcePath;
                }
                return chain.resolveUrlPath(resourcePath, locations);
            }
        });
    }

    @Bean
    public I18NextResourceDescriptor i18NextResourceDescriptor() {
        return new I18NextResourceDescriptor();
    }

    @Bean
    public PluginFilterMappingDescriptor pluginFilterDescriptor() {
        EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST, DispatcherType.ASYNC, DispatcherType.FORWARD);

        return PluginFilterMappingDescriptorBuilder.newBuilder()
                .registerFilter(new TestPluginFilter(), dispatcherTypes, "/status/**")
                .registerFilter(new TestPluginFilter2(), dispatcherTypes, "/status/**", "/rest/func-test/**")
                .build();
    }

    @Bean
    public DatabaseMigrationsDescriptor databaseMigrationsDescriptor() {
        return new DatabaseMigrationsDescriptor("migrations/changelog.xml");
    }

    @Bean
    public DynamicAssetsDescriptor dynamicAssetsDescriptor() {
        return new DynamicAssetsDescriptor();
    }

    @Bean
    public PluginServletMappingDescriptor overriddenServletDescriptor(OverriddenServlet overriddenServlet, OverridingServlet overridingServlet) {
        return new PluginServletMappingDescriptor(Arrays.asList(
                new ServletMapping("/rest/func-test/1.0/test/overriddenServletTest", 100, overriddenServlet, Collections.emptyMap()),
                new ServletMapping("/rest/func-test/1.0/test/overridingServletTest", 1, overridingServlet, Collections.emptyMap())
        ));
    }

    @Bean
    public EventListenersDescriptor eventListenersDescriptor(List<EventListener> eventListeners) {
        return new EventListenersDescriptor(new HashSet<>(eventListeners));
    }

    @Bean
    AdminPanelItemsDescriptor testAdminMenu() {
        var jsFilePrefix = "/static/p/eu.rarogsoftware.rarog.platform.plugins:func-test-plugin/debug/js/";
        var simpleReactComponentFile = jsFilePrefix + "adminPanelTestComponent.debug.js";
        return new AdminPanelItemsDescriptor(
                Arrays.asList(
                        AdminPanelItem.simplePage("testItem1",
                                "test/subcategory",
                                "Some name",
                                simpleReactComponentFile,
                                "testKeyword for test category"),
                        AdminPanelItem.simpleCategory("test",
                                "",
                                "Test category"),
                        AdminPanelItem.simpleCategory("otherCategory",
                                "test",
                                "Other category"),
                        AdminPanelItem.simpleCategory("subcategory",
                                "test",
                                "Subcategory"),
                        AdminPanelItem.simplePage("testItem2",
                                "general",
                                "Some name",
                                simpleReactComponentFile,
                                "testKeyword for general category"),
                        AdminPanelItem.simpleCategory("types",
                                "test",
                                "Page types tests"),
                        AdminPanelItem.simplePage("iframe",
                                "test/types",
                                "Iframe page",
                                "/plugin/func-test/test/iframe"),
                        AdminPanelItem.simplePage("simpleReact",
                                "test/types",
                                "Simple React",
                                simpleReactComponentFile),
                        AdminPanelItem.simplePage("simpleGeneric",
                                "test/types",
                                "Simple Generic Component",
                                jsFilePrefix + "adminPanelTestGenericComponent.debug.js"),
                        AdminPanelItem.simplePage("parentReact",
                                "test/types",
                                "Parent React",
                                jsFilePrefix + "adminPanelParentComponent.debug.js"),
                        AdminPanelItem.embeddedChild("childReact",
                                "test/types/parentReact",
                                "Child React",
                                jsFilePrefix + "adminPanelChildComponent.debug.js"),
                        AdminPanelItem.navLink("link",
                                "test/types",
                                "Profile link",
                                "/account/my")
                )
        );
    }

    @Bean
    UiFragmentsDescriptor uiFragmentsDescriptor() {
        var jsFile = "/static/p/eu.rarogsoftware.rarog.platform.plugins:func-test-plugin/debug/js/adminPanelUiFragment.debug.js";
        var placement = "adminPanelQuickAccess";
        return new UiFragmentsDescriptor(
                Arrays.asList(
                        new UiFragment("testItem1",
                                placement,
                                jsFile,
                                1),
                        new UiFragment("testItem2",
                                placement,
                                jsFile,
                                1)
                )
        );
    }
}
