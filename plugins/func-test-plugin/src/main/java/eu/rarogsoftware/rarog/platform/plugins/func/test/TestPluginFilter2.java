package eu.rarogsoftware.rarog.platform.plugins.func.test;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestPluginFilter2 implements Filter {

  private static final Logger logger = LoggerFactory.getLogger(TestPluginFilter.class);

  @Override
  public void doFilter(
      ServletRequest request,
      ServletResponse response,
      FilterChain chain) throws IOException, ServletException {

    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;
    res.addHeader("FUNC_TEST_FILTERED", "DARK_GODS_APPROVE");
    logger.trace(
        "[FILTER 2] Backdoor Logging Request  {} : {}", req.getMethod(),
        req.getRequestURI());
    chain.doFilter(request, response);
    logger.trace(
        "[FILTER 2] Backdoor Logging Response :{}",
        res.getContentType());
  }
}