package eu.rarogsoftware.rarog.platform.plugins.func.test.events;

import eu.rarogsoftware.rarog.platform.api.plugins.events.EventHandler;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class FuncTestEventListener implements EventListener {

    private final Logger logger = LoggerFactory.getLogger(FuncTestEventListener.class);
    private int funcTestEvents = 0;
    private int stringEvents = 0;
    private int objectEvents = 0;

    @EventHandler
    public void onFuncTestEvent(FuncTestEvent event) {
        logger.info("Received FuncTestEvent. Count: {}", ++funcTestEvents);
    }

    @EventHandler
    public void onStringEvent(String event) {
        logger.info("Received String. Count: {}", ++stringEvents);
    }

    @EventHandler
    public void onObjectEvent(Object event) {
        logger.info("Received Object. Count: {}", ++objectEvents);
    }

    public void reset() {
        logger.info("Resetting FuncTestEventListener.");
        funcTestEvents = 0;
        stringEvents = 0;
        objectEvents = 0;
    }

    public int getFuncTestEventsCount() {
        return funcTestEvents;
    }

    public int getStringEventsCount() {
        return stringEvents;
    }

    public int getObjectEventsCount() {
        return objectEvents;
    }
}
