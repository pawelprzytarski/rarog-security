const WebpackAssetsManifest = require('webpack-assets-manifest');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');
const path = require('path');

function assetsManifestCommonConfig(subdirectory, name, debug) {
    return {
        publicPath: '/static/p/eu.rarogsoftware.rarog.platform.plugins:func-test-plugin/debug/',
        output: (debug ? '../' : '') + `../descriptors/${subdirectory}webpack-${name}.manifest.json`,
        entrypoints: true,
        entrypointsUseAssets: true,
        integrity: true
    };
}

module.exports = function (_env) {
    const mode = 'development';
    // eslint-disable-next-line no-undef
    const outputPath = path.normalize(__dirname + '../../../../target/classes/static/debug/');
    console.log('Output directory:' + outputPath);
    return {
        mode: mode,
        devtool: _env.dev ? 'inline-source-map' : undefined,
        target: 'web',
        entry: {
            adminPanelTestComponent: './src/AdminPanelTestComponent',
            adminPanelTestGenericComponent: './src/AdminPanelTestGenericComponent',
            adminPanelParentComponent: './src/AdminPanelParentComponent',
            adminPanelChildComponent: './src/AdminPanelChildComponent',
            adminPanelUiFragment: './src/AdminPanelUiFragment'
        },
        output: {
            path: outputPath,
            filename: `js/[name].debug.js`,
            chunkFilename: `js/[name].debug.js`,
            libraryTarget: 'module',
            globalObject: 'globals'
        },
        externals: {
            react: 'global react',
            '@rarog/rarog-app-utils': 'global @rarog/rarog-app-utils',
            'react-router-dom': 'global react-router-dom',
            'bootstrap': 'global bootstrap',
            'react-bootstrap': 'global react-bootstrap',
            'react-i18next': 'global react-i18next'
        },
        experiments: {
            outputModule: true
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: `css/[name].debug.css`,
                chunkFilename: `css/[name].debug.css`
            }),
            new webpack.DefinePlugin({
                DEVELOPMENT: JSON.stringify(!!_env.dev),
                BUILT_AT: webpack.DefinePlugin.runtimeValue(Date.now)
            }),
            new WebpackAssetsManifest(assetsManifestCommonConfig('../../descriptors/', 'dynamicComponents', _env.dev))
        ].filter(Boolean),
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    use: [{
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            cacheCompression: true,
                            presets: ['@babel/preset-env', '@babel/preset-react'],
                        },
                    }]
                },
                {
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        'css-loader'
                    ]
                }
            ]
        },
        resolve: {
            extensions: ['.js', '.jsx'],
        }
    };
};

