import React from 'react';

function AdminPanelUiFragment() {
    return (
        <div className="testUiFragment">
            Test UI fragment
        </div>
    );
}

export default AdminPanelUiFragment;
