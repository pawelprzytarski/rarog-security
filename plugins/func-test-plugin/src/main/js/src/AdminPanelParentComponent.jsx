import React, {useEffect} from 'react';
import {Outlet} from 'react-router-dom';

function AdminPanelTestComponent() {
    useEffect(() => {
        document.title = 'Test admin page using react';
    });
    return (
        <>
            <div>Content of parent page</div>
            <div><Outlet/></div>
        </>
    );
}

export default AdminPanelTestComponent;
