import React, {useEffect} from 'react';

function AdminPanelTestComponent() {
    useEffect(() => {
        document.title = 'Test admin page using react';
    });
    return (
        <>
            Content of child page
        </>
    );
}

export default AdminPanelTestComponent;
