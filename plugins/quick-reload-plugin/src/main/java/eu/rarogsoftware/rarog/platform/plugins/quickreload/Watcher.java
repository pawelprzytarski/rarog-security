package eu.rarogsoftware.rarog.platform.plugins.quickreload;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginManifest;
import eu.rarogsoftware.rarog.platform.plugins.quickreload.api.QuickReloadSettings;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys;
import eu.rarogsoftware.rarog.platform.plugins.quickreload.DefaultQuickReloadService.PluginRecord;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.WatchService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

@SuppressFBWarnings({"PATH_TRAVERSAL_IN", "HTTP_PARAMETER_POLLUTION"})
public class Watcher implements Closeable {
    private static final int TEN_SECONDS = 10000;
    private final Logger logger = LoggerFactory.getLogger(Watcher.class);
    private final ApplicationSettings applicationSettings;
    private final CloseableHttpClient httpClient;
    private String appToken = "SomeRandomShitJustToPassAdminRestrictions";
    private WatchService watchService;

    public Watcher(ApplicationSettings applicationSettings) {
        this.applicationSettings = applicationSettings;
        try {
            this.watchService = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            this.watchService = null;
            logger.warn("Failed to create watch service. Service will use slower method of comparing hashes of files on each check", e);
        }
        httpClient = HttpClientBuilder.create()
                .disableCookieManagement()
                .disableAuthCaching()
                .disableConnectionState()
                .build();
    }

    @Override
    public void close() throws IOException {
        try {
            if (watchService != null) {
                watchService.close();
            }
        } finally {
            httpClient.close();
        }
    }

    public void setAdminToken(String appToken) {
        this.appToken = appToken;
    }

    public void watch(Map<String, PluginRecord> watchList) {
        if (applicationSettings.isTrue(QuickReloadSettings.RELOAD_ENABLED_KEY)) {
            var listToCheck = new HashMap<>(watchList);
            listToCheck.forEach(this::watchEntry);
        }
    }

    private void watchEntry(String path, PluginRecord pluginRecord) {
        var file = new File(path);
        if (shouldBeSkipped(path, file, pluginRecord)) {
            return;
        }
        if (observeFirstTime(path, pluginRecord, file)) {
            return;
        }
        if (isFileToInstallForFirstTime(pluginRecord) || hasFileChanged(path, pluginRecord, file)) {
            logger.info("Detected file {} change. Waiting 10 seconds for plugin build process to stabilize artifact", path);
            pluginRecord.doNotReinstallBefore = System.currentTimeMillis() + TEN_SECONDS;
            return;
        }
        if (pluginRecord.doNotReinstallBefore >= 0) {
            if (!installPlugin(file)) {
                return;
            }
            pluginRecord.doNotReinstallBefore = -1;
        }
    }

    private static boolean isFileToInstallForFirstTime(PluginRecord pluginRecord) {
        return pluginRecord.fileHash.length == 0;
    }

    private boolean installPlugin(File file) {
        var postInstallRequest = new HttpPost(getBaseUrl() + "plugins");
        addAuthorizationHeader(postInstallRequest);
        addCsrfDisableHeader(postInstallRequest);
        postInstallRequest.setEntity(new FileEntity(file, ContentType.APPLICATION_OCTET_STREAM));
        var watch = StopWatch.createStarted();
        try (var response = httpClient.execute(postInstallRequest)) {
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_NO_CONTENT) {
                logReinstallBanner(file, watch.getTime());
                return true;
            } else {
                logger.warn("Failed to install plugin {}", file.getPath());
                return false;
            }
        } catch (IOException e) {
            logger.warn("Failed to install plugin {}", file.getPath(), e);
            return false;
        }
    }

    private static void addCsrfDisableHeader(HttpRequestBase request) {
        request.addHeader("X-CSRF-DISABLE", "true");
    }

    private void addAuthorizationHeader(HttpRequestBase getPluginData) {
        getPluginData.addHeader(HttpHeaders.AUTHORIZATION, "Token " + appToken);
    }

    private void logReinstallBanner(File file, long duration) {
        try (var artifact = new JarPluginArtifact(file)) {
            var pluginKey = artifact.getManifest().map(PluginManifest::key).orElse(file.getPath());
            logger.info(FunnyLogsHelper.getBannerMessage("Successfully reinstalled plugin {} in {}s"), pluginKey, duration / 1000.0);
        } catch (IOException e) {
            logger.info(FunnyLogsHelper.getBannerMessage("Successfully reinstalled plugin {} in {}s"), file.getPath(), duration / 1000.0);
        }
    }

    private boolean observeFirstTime(String path, PluginRecord pluginRecord, File file) {
        if (pluginRecord.fileHash == null) {
            logger.trace("File {} first time checked. Generating hash", path);
            var hash = calculateHashOfFile(file);
            if (hash.isEmpty()) {
                return true;
            }
            pluginRecord.fileHash = hash.get();
            if (pluginRecord.doNotReinstallBefore == -1) {
                return checkPluginInstalled(file, pluginRecord);
            }
        }
        return false;
    }

    private boolean checkPluginInstalled(File file, PluginRecord pluginRecord) {
        try (var artifact = new JarPluginArtifact(file)) {
            var manifest = artifact.getManifest();
            if (manifest.isEmpty()) {
                return true;
            }
            var pluginKey = URLEncoder.encode(manifest.get().key(), StandardCharsets.UTF_8);
            var getPluginData = new HttpGet(getBaseUrl() + "plugins/" + pluginKey);
            addAuthorizationHeader(getPluginData);
            try (var response = httpClient.execute(getPluginData)) {
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                    pluginRecord.doNotReinstallBefore = 0;
                }
            }
        } catch (IOException e) {
            return true;
        }
        return false;
    }

    private String getBaseUrl() {
        var selfCallUrl = applicationSettings.getSettingOrDefault(SystemSettingKeys.SELF_CALL_URL, String.class);
        if (!selfCallUrl.endsWith("/")) {
            selfCallUrl += "/";
        }
        return selfCallUrl;
    }

    private Optional<byte[]> calculateHashOfFile(File file) {
        try (var stream = new FileInputStream(file)) {
            return Optional.of(DigestUtils.sha256(stream));
        } catch (IOException e) {
            logger.debug("Failed to calculate hash of {}. Skipping this iteration of watch", file.getPath(), e);
            return Optional.empty();
        }
    }

    private boolean shouldBeSkipped(String path, File file, PluginRecord pluginRecord) {
        if (!file.exists()) {
            logger.trace("File {} do not exist. Skipping", path);
            pluginRecord.fileHash = new byte[0]; // so it is installed when created
            return true;
        }
        if (!file.isFile() || !file.canRead()) {
            logger.debug("Provided path {} is not valid readable file.", path);
            return true;
        }

        if (pluginRecord.doNotReinstallBefore > System.currentTimeMillis()) {
            logger.trace("Skipping watch of {} because it is waiting for reinstall at {}", path, pluginRecord.doNotReinstallBefore);
            return true;
        }
        return false;
    }

    private boolean hasFileChanged(String path, PluginRecord pluginRecord, File file) {
        if (watchService != null) {
            if (pluginRecord.watchKey == null) {
                try {
                    pluginRecord.watchKey = FileSystems.getDefault().getPath(file.getParent())
                            .register(watchService, ENTRY_CREATE, ENTRY_MODIFY);
                } catch (IOException e) {
                    logger.debug("Failed to create watchKey for path {}", path, e);
                }
            }
            var changed = !pluginRecord.watchKey.pollEvents().isEmpty();
            pluginRecord.watchKey.reset();
            return changed && hasFileHashChanged(pluginRecord, file);
        } else {
            return hasFileHashChanged(pluginRecord, file);
        }
    }

    private boolean hasFileHashChanged(PluginRecord pluginRecord, File file) {
        var newHash = calculateHashOfFile(file);
        if (newHash.isEmpty()) {
            return false;
        }
        return Arrays.compare(pluginRecord.fileHash, newHash.get()) != 0;
    }
}
