package eu.rarogsoftware.rarog.platform.plugins.quickreload.rest;

import eu.rarogsoftware.rarog.platform.api.security.AdminOnly;
import eu.rarogsoftware.rarog.platform.plugins.quickreload.api.QuickReloadService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

@RequestMapping(path = "watcher", produces = MediaType.APPLICATION_JSON_VALUE)
@AdminOnly
@Tag(name = "Quick reload watcher", description = "Endpoint exposed by QuickReload plugin used to manage list of "
        + "observed plugins to auto reload")
public class WatcherServiceController {
    private final QuickReloadService quickReloadService;

    public WatcherServiceController(QuickReloadService quickReloadService) {
        this.quickReloadService = quickReloadService;
    }

    @GetMapping("/")
    @Operation(summary = "Get list of watched binaries",
            description = "Returns list of watched binaries that will be automatically reinstalled on change")
    @ApiResponse(responseCode = "200", description = "List of watched binaries")
    public List<String> getListOfWatchedBinaries() {
        return quickReloadService.getWatchList();
    }

    @PostMapping("/stop")
    @Operation(summary = "Stop watching binaries",
            description = "Stopes watcher from monitoring binaries and reinstalling them")
    @ApiResponse(responseCode = "204", description = "Successfully stopped watcher")
    public ResponseEntity<Void> stopWatcher() {
        quickReloadService.stopWatching();
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/start")
    @Operation(summary = "Start watching binaries",
            description = "Starts watcher so it can monitor binaries and reinstall them")
    @ApiResponse(responseCode = "204", description = "Successfully started watcher")
    public ResponseEntity<Void> startWatcher() {
        quickReloadService.startWatching();
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/watch")
    @Operation(summary = "Add binary to watch list",
            description = "Will start monitoring specified binary and will attempt reinstall on each change.\n"
                    + "NOTE: You can add not existing yet paths to watchlist as well. They will be automatically installed on creation.")
    @ApiResponse(responseCode = "204", description = "Successfully started watching binary")
    @ApiResponse(responseCode = "400", description = "Specified path is not correct path")
    public ResponseEntity<Map<String, Object>> addToWatchList(@RequestBody PathBean pathBean) {
        if (isNotValidPath(pathBean.path())) {
            return ResponseEntity.badRequest().body(Map.of("error", "Specified path is incorrect"));
        }
        quickReloadService.addToWatch(pathBean.path());
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/install")
    @Operation(summary = "Install and watch binary",
            description = "Will install specified binary and then will start watching it.")
    @ApiResponse(responseCode = "204", description = "Successfully installed & started watching binary")
    @ApiResponse(responseCode = "400", description = "Specified path is not correct path")
    public ResponseEntity<Map<String, Object>> install(@RequestBody PathBean pathBean) {
        if (isNotValidPath(pathBean.path())) {
            return ResponseEntity.badRequest().body(Map.of("error", "Specified path is incorrect"));
        }
        quickReloadService.installAndWatch(pathBean.path());
        return ResponseEntity.noContent().build();
    }

    private boolean isNotValidPath(String path) {
        try {
            if (StringUtils.isNotBlank(path)) {
                Path.of(path);//will throw exception
                return false;
            }
        } catch (InvalidPathException ignored) {
            return false;
        }
        return true;
    }

    @PostMapping("/unwatch")
    @Operation(summary = "Remove binary from watch list",
            description = "Stops monitoring specified binary and will not attempt to reinstall it anymore")
    @ApiResponse(responseCode = "204", description = "Successfully stopped watching binary")
    @ApiResponse(responseCode = "400", description = "Specified path is not correct path")
    public ResponseEntity<Map<String, Object>> remoteFromWatchList(@RequestBody PathBean pathBean) {
        if (isNotValidPath(pathBean.path())) {
            return ResponseEntity.badRequest().body(Map.of("error", "Specified path is incorrect"));
        }
        return ResponseEntity.noContent().build();
    }

    @Schema(name = "Single path bean")
    record PathBean(String path) {
    }
}
