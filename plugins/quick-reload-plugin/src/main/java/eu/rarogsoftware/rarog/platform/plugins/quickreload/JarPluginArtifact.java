package eu.rarogsoftware.rarog.platform.plugins.quickreload;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginManifest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;

/**
 * This is simplified version of JarPluginArtifact from core. I did it because I didn't want this ugly dependency
 * on core and this one can actually be much simpler, as it only require to check if it is valid file
 */
public class JarPluginArtifact implements Closeable {
    private static final ObjectMapper jsonMapper = new ObjectMapper();
    private final Logger logger = LoggerFactory.getLogger(JarPluginArtifact.class);
    private final File file;
    private JarFile jarFile;

    public JarPluginArtifact(File file) {
        this.file = file;
    }

    public Manifest getJarManifest() throws IOException {
        return getJar().getManifest();
    }

    private JarFile getJar() throws IOException {
        if (jarFile == null) {
            jarFile = new JarFile(file);
        }
        return jarFile;
    }

    public InputStream getResource(String location) throws IOException {
        ZipEntry entry = getJar().getEntry(location);
        if (entry == null) {
            return null;
        }
        return getJar().getInputStream(entry);
    }

    public Optional<PluginManifest> getManifest() {
        logger.trace("Reading manifest file from plugin {}", file.getPath());
        try (var manifestStream = getResource("META-INF/rarog-plugin.json")) {
            if (manifestStream == null) {
                logger.warn("Jar {} is not correct rarog plugin", file.getPath());
                return Optional.empty();
            }
            return Optional.of(jsonMapper.readValue(manifestStream, PluginManifest.class));
        } catch (IOException e) {
            logger.warn("Plugin read from URL {} is not valid rarog plugin. Cannot read parse manifest file", file.getPath(), e);
            return Optional.empty();
        }
    }

    public void close() throws IOException {
        if (jarFile != null) {
            jarFile.close();
        }
    }
}
