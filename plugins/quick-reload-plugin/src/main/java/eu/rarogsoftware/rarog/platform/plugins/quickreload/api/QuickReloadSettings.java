package eu.rarogsoftware.rarog.platform.plugins.quickreload.api;

public final class QuickReloadSettings {
    public static final String PERIOD_SECONDS_KEY = "quickreload.watch.period.seconds";
    public static final String RELOAD_ENABLED_KEY = "quickreload.enabled";

    private QuickReloadSettings() {
    }
}
