package eu.rarogsoftware.rarog.platform.plugins.quickreload;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.user.management.AppUser;
import eu.rarogsoftware.rarog.platform.api.user.management.UserRole;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.HomeDirectoryHelper;
import eu.rarogsoftware.rarog.platform.api.settings.SettingsInitializer;
import eu.rarogsoftware.rarog.platform.plugins.context.PluginActivated;
import eu.rarogsoftware.rarog.platform.plugins.context.PluginDeactivating;
import eu.rarogsoftware.rarog.platform.plugins.context.PluginPurge;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenCreationException;
import eu.rarogsoftware.rarog.platform.plugins.pat.api.TokenService;
import eu.rarogsoftware.rarog.platform.plugins.quickreload.api.QuickReloadSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

@Component
@SuppressFBWarnings("PATH_TRAVERSAL_IN")
public class PluginConfigurer implements SettingsInitializer, ApplicationListener<ApplicationEvent> {
    public static final String ADMIN_USERNAME = "quick_reload_admin_account";
    private final Logger logger = LoggerFactory.getLogger(PluginConfigurer.class);
    private final DefaultQuickReloadService quickReloadService;
    private final HomeDirectoryHelper directoryHelper;

    private final TokenService tokenService;
    private final String APP_NAME = "eu.rarogsoftware.rarog.platform.plugins:quick-reload-plugin";
    private final String TOKEN_NAME = "QuickReloadRestApiAccessToken";

    public PluginConfigurer(DefaultQuickReloadService quickReloadService,
                            @ComponentImport HomeDirectoryHelper directoryHelper,
                            @ComponentImport TokenService tokenService) {
        this.quickReloadService = quickReloadService;
        this.directoryHelper = directoryHelper;
        this.tokenService = tokenService;
    }

    @Override
    public void initialize(ApplicationSettings settings) {
        settings.setDefaultSetting(QuickReloadSettings.PERIOD_SECONDS_KEY, 5);
        settings.setDefaultSetting(QuickReloadSettings.RELOAD_ENABLED_KEY, true);
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof PluginActivated) {
            logger.info(FunnyLogsHelper.getBannerMessage("Quick Reload saving your time since Pawel Przytarski raged about development time of plugins.\n"
                    + "Nothing drives innovation like pure rage fueled by f... slow development..."));
            var watchListFile = new File(directoryHelper.getConfig(), "reload_watchlist.txt");
            if (watchListFile.exists() && watchListFile.isFile() && watchListFile.canRead()) {
                try {
                    quickReloadService.readWatchList(watchListFile.getAbsolutePath());
                } catch (IOException e) {
                    logger.warn("Failed to read watch list in config directory", e);
                }
            }
            quickReloadService.startWatching();
            prepareAccessData();
        } else if (event instanceof PluginDeactivating) {
            logger.info(FunnyLogsHelper.getBannerMessage("Quick Reload will not save your time anymore\n"
                    + "Did you decide to leave path of software developer finally or just you are ready to make free your "
                    + "cursed child born from dark desires and unpure thoughts also known as Rarog plugin?"));
            disableAccess();
            quickReloadService.stopWatching();
        } else if (event instanceof PluginPurge) {
            logger.info(FunnyLogsHelper.getBannerMessage("Quick Reload will purge its data on instance\n"
                    + "I don't know what drive this decision but I wish you godspeed!"));
            purgeData();
        }
    }

    private void prepareAccessData() {
        try {
            var token = tokenService.createAppToken(AppUser.builder().appName(APP_NAME).build(),
                    TOKEN_NAME,
                    Instant.now().plus(30, ChronoUnit.DAYS),
                    Arrays.asList(UserRole.USER.getAuthority().getAuthority(),
                            UserRole.SYSTEM_ADMINISTRATOR.getAuthority().getAuthority(),
                            UserRole.ADMINISTRATOR.getAuthority().getAuthority())
            );
            quickReloadService.setAppToken(token);
        } catch (TokenCreationException e) {
            throw new RuntimeException(e);
        }
    }

    private void disableAccess() {
        var tokens = tokenService.getTokenForApp(APP_NAME);
        tokens.stream()
                .filter(token -> TOKEN_NAME.equals(token.name()))
                .forEach(tokenService::revokeToken);
    }

    private void purgeData() {
        disableAccess();
    }
}
