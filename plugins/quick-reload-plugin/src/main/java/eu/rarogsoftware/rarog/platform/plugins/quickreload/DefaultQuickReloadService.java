package eu.rarogsoftware.rarog.platform.plugins.quickreload;

import eu.rarogsoftware.rarog.platform.api.plugins.ComponentImport;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.task.TaskManager;
import eu.rarogsoftware.rarog.platform.plugins.quickreload.api.QuickReloadService;
import eu.rarogsoftware.rarog.platform.plugins.quickreload.api.QuickReloadSettings;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.WatchKey;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;

@Component
@ExportComponent
public class DefaultQuickReloadService implements QuickReloadService {
    private final Logger logger = LoggerFactory.getLogger(DefaultQuickReloadService.class);
    private static final int INITIAL_DELAY = 60;
    private final Map<String, PluginRecord> watchList = new ConcurrentHashMap<>();
    private final ApplicationSettings applicationSettings;
    private final TaskManager taskManager;
    private Watcher watcher;
    private String token;
    private ScheduledFuture<?> task;

    public DefaultQuickReloadService(@ComponentImport ApplicationSettings applicationSettings,
                                     @ComponentImport TaskManager taskManager) {
        this.applicationSettings = applicationSettings;
        this.taskManager = taskManager;
    }

    @Override
    public void startWatching() {
        if (task != null) {
            logger.debug("Already watching");
            return;
        }
        watcher = new Watcher(applicationSettings);
        if (token != null) {
            watcher.setAdminToken(token);
        }
        logger.info("Starting QuickReload. From now all listed plugin will be automatically reloaded on change");
        int period = applicationSettings.getSetting(QuickReloadSettings.PERIOD_SECONDS_KEY, Integer.class).orElse(5);
        this.task = taskManager.asExecutorService().scheduleAtFixedRate(() -> watcher.watch(watchList), INITIAL_DELAY, period, TimeUnit.SECONDS);
    }

    @Override
    public void stopWatching() {
        if (task == null) {
            logger.debug("Already stopped watching");
            return;
        }
        logger.info("Stopping QuickReload. From now no plugin will be automatically reloaded");
        task.cancel(false);
        try {
            task.get(60, TimeUnit.SECONDS);
        } catch (ExecutionException | TimeoutException e) {
            task.cancel(true);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            try {
                watcher.close();
            } catch (IOException e) {
                logger.warn("Failed to close watch service. System may remain unstable!", e);
            }
            task = null;
        }
    }

    @Override
    public void addToWatch(String path) {
        logger.info("Added {} to watch list, assuming plugin is installed in correct version.", path);
        watchList.put(path, new PluginRecord(null, null, -1));
    }

    @Override
    public void installAndWatch(String path) {
        logger.info("Added {} to watch list and scheduled (re)install", path);
        watchList.put(path, new PluginRecord(null, null, 0));
    }

    @Override
    public void removeFromWatch(String path) {
        watchList.remove(path);
    }

    @Override
    public void readWatchList(String path) throws IOException {
        try (var stream = new FileInputStream(path)) {
            readWatchList(stream.readAllBytes());
        }
    }

    @Override
    public void readWatchList(byte[] content) {
        var simpleContent = new String(content, StandardCharsets.UTF_8);
        Arrays.stream(splitBySeparator(simpleContent))
                .map(entry -> {
                    if (entry.contains("|")) {
                        var split = entry.split("\\|", 2);
                        return Pair.of(split[0], "true".equalsIgnoreCase(split[1]));
                    } else {
                        return Pair.of(entry, false);
                    }
                })
                .forEach(pair -> {
                    if (pair.getRight()) {
                        installAndWatch(pair.getLeft());
                    } else {
                        addToWatch(pair.getLeft());
                    }
                });
    }

    private static String[] splitBySeparator(String simpleContent) {
        if (simpleContent.contains(";")) {
            return simpleContent.trim().split(";");
        }
        return simpleContent.trim()
                .replace("\r\n", "\n")
                .replace("\r", "\n")
                .split("\n");
    }

    @Override
    public List<String> getWatchList() {
        return List.copyOf(watchList.keySet());
    }

    public void setAppToken(String appToken) {
        token = appToken;
        if (watcher != null) {
            watcher.setAdminToken(appToken);
        }
    }

    static final class PluginRecord {
        byte[] fileHash;
        WatchKey watchKey;
        long doNotReinstallBefore;

        PluginRecord(byte[] fileHash, WatchKey watchKey, long doNotReinstallBefore) {
            this.fileHash = fileHash;
            this.watchKey = watchKey;
            this.doNotReinstallBefore = doNotReinstallBefore;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) return true;
            if (obj == null || obj.getClass() != this.getClass()) return false;
            var that = (PluginRecord) obj;
            return Arrays.equals(this.fileHash, that.fileHash) &&
                    Objects.equals(this.watchKey, that.watchKey) &&
                    this.doNotReinstallBefore == that.doNotReinstallBefore;
        }

        @Override
        public int hashCode() {
            return Objects.hash(fileHash, watchKey, doNotReinstallBefore);
        }

        @Override
        public String toString() {
            return "PluginHash[" +
                    "fileHash=" + Arrays.toString(fileHash) + ", " +
                    "watchKey=" + watchKey + ", " +
                    "doNotReinstallBefore=" + doNotReinstallBefore + ']';
        }

    }
}
