package eu.rarogsoftware.rarog.platform.plugins.quickreload.api;

import java.io.IOException;
import java.util.List;

public interface QuickReloadService {
    void startWatching();

    void stopWatching();

    void addToWatch(String path);

    void installAndWatch(String path);

    void removeFromWatch(String path);

    void readWatchList(String path) throws IOException;

    void readWatchList(byte[] content);

    List<String> getWatchList();
}
