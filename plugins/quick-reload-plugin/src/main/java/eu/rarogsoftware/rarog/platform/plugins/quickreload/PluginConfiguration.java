package eu.rarogsoftware.rarog.platform.plugins.quickreload;

import eu.rarogsoftware.rarog.platform.plugins.autoconfigure.EnablePluginAutoconfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnablePluginAutoconfiguration
public class PluginConfiguration {
}
