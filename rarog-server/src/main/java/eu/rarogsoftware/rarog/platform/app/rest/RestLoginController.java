package eu.rarogsoftware.rarog.platform.app.rest;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.i18n.I18nHelper;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowedLevel;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaService;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.core.common.HomePageService;
import eu.rarogsoftware.rarog.platform.core.common.Range;
import eu.rarogsoftware.rarog.platform.core.security.LoginAttemptService;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.WebAttributes;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

@SuppressFBWarnings("SPRING_CSRF_UNRESTRICTED_REQUEST_MAPPING")
@RequestMapping("/login/")
@RestController
@Hidden
public class RestLoginController {
    private final I18nHelper i18nHelper;
    private final MfaService mfaService;
    private final LoginAttemptService loginAttemptService;
    private final HomePageService homePageService;

    public RestLoginController(I18nHelper i18nHelper,
                               MfaService mfaService,
                               LoginAttemptService loginAttemptService,
                               HomePageService homePageService) {
        this.i18nHelper = i18nHelper;
        this.mfaService = mfaService;
        this.loginAttemptService = loginAttemptService;
        this.homePageService = homePageService;
    }

    @RequestMapping("success")
    public SuccessfulLoginInfo showSuccessfulLoginInfo(@AuthenticationPrincipal StandardUser user) {
        List<LoginAttemptService.LoginAttempt> loginAttempts = loginAttemptService.getLoginAttempts(user, Range.inclusive(Instant.now().minus(Duration.ofDays(30)), Instant.now()));
        long failedLoginsInMonth = loginAttempts.stream()
                .filter(loginAttempt -> !loginAttempt.successful())
                .count();
        Date lastSuccessfulLogin = loginAttempts.stream().
                filter(LoginAttemptService.LoginAttempt::successful)
                .reduce((first, second) -> second).
                map(loginAttempt -> Date.from(loginAttempt.attemptTime()))
                .orElse(null);
        Date lastFailedLogin = loginAttempts.stream().
                filter(loginAttempt -> !loginAttempt.successful())
                .reduce((first, second) -> second).
                map(loginAttempt -> Date.from(loginAttempt.attemptTime()))
                .orElse(null);

        return new SuccessfulLoginInfo(
                failedLoginsInMonth,
                user.getUsername(),
                lastSuccessfulLogin,
                lastFailedLogin,
                mfaService.isMfaEnabledForUser(user),
                homePageService.getHomePageForUser(user)
        );
    }

    @AnonymousAllowed(AnonymousAllowedLevel.CORE)
    @RequestMapping("failure")
    public ResponseEntity<Map<String, String>> showAuthenticationError(HttpServletRequest request) {
        Optional<String> errorMessage = Optional.empty();
        AuthenticationException ex = (AuthenticationException) request
                .getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        if (ex instanceof BadCredentialsException || ex instanceof UsernameNotFoundException) {
            errorMessage = Optional.of(i18nHelper.getText("login.error.badcredentials"));
        } else if (ex != null) {
            errorMessage = Optional.of(ex.toString());
        }

        return ResponseEntity
                .badRequest()
                .body(Collections.singletonMap("errorMessage", errorMessage.orElseGet(() -> i18nHelper.getText("error.login.unknownerror"))));
    }

    public record SuccessfulLoginInfo(long failedLogins, String username, Date lastSuccessfulLogin,
                                      Date lastFailedLogin, boolean multiFactorLoginEnabled, String defaultAction) {
    }
}
