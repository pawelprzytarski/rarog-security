package eu.rarogsoftware.rarog.platform.app.configuration.security;

import jakarta.servlet.Filter;

public interface AutoImportingFilter {
    FilterPlace getFilterPlace();

    record FilterPlace(Class<? extends Filter> filter, Direction direction) {
        public enum Direction {
            BEFORE,
            AT,
            AFTER
        }
    }
}
