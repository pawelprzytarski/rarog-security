package eu.rarogsoftware.rarog.platform.core.task;

import eu.rarogsoftware.rarog.platform.api.task.AdvancedScheduledExecutorService;
import eu.rarogsoftware.rarog.platform.api.task.ExecutionInfo;
import eu.rarogsoftware.rarog.platform.api.task.ScheduleConfig;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

class AdvancedScheduledThreadPoolExecutor extends ScheduledThreadPoolExecutor implements AdvancedScheduledExecutorService, ServletContextListener {
    private boolean stopped = false;
    private static final AtomicLong sequencer = new AtomicLong();

    public AdvancedScheduledThreadPoolExecutor(int maxPoolSize, ThreadFactory threadFactory) {
        super(maxPoolSize, threadFactory);
        setMaximumPoolSize(maxPoolSize);
        setKeepAliveTime(10L, SECONDS);
        allowCoreThreadTimeOut(true);
    }


    @Override
    public void shutdown() {
        stopped = true;
        super.shutdown();
    }

    @Override
    public List<Runnable> shutdownNow() {
        stopped = true;
        return super.shutdownNow();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        shutdownNow();
    }

    public ScheduledFuture<?> scheduleAdvanced(Runnable command,
                                               ScheduleConfig<?> config) {
        if (command == null || config == null)
            throw new NullPointerException();
        var sft =
                new ScheduledFutureTask<>(command,
                        null,
                        triggerTime(config.initialDelay().toNanos()),
                        (ScheduleConfig<Void>) config,
                        sequencer.getAndIncrement());
        var t = decorateTask(command, sft);
        sft.outerTask = t;
        delayedExecute(t);
        return t;
    }

    public <T> ScheduledFuture<T> scheduleAdvanced(Callable<T> command,
                                                   ScheduleConfig<T> config) {
        if (command == null || config == null)
            throw new NullPointerException();
        var sft =
                new ScheduledFutureTask<>(command,
                        triggerTime(config.initialDelay().toNanos()),
                        config,
                        sequencer.getAndIncrement());
        var t = decorateTask(command, sft);
        sft.outerTask = t;
        delayedExecute(t);
        return t;
    }

    private void delayedExecute(RunnableScheduledFuture<?> task) {
        if (isShutdown()) {
            reject(task);
        } else {
            super.getQueue().add(task);
            if (!canRunInCurrentRunState(task) && remove(task)) {
                task.cancel(false);
            } else {
                prestartCoreThread();
            }
        }
    }

    final void reject(Runnable command) {
        getRejectedExecutionHandler().rejectedExecution(command, this);
    }

    long triggerTime(long delay) {
        return System.nanoTime() +
                ((delay < (Long.MAX_VALUE >> 1)) ? delay : overflowFree(delay));
    }

    private long overflowFree(long delay) {
        Delayed head = (Delayed) super.getQueue().peek();
        if (head != null) {
            long headDelay = head.getDelay(NANOSECONDS);
            if (headDelay < 0 && (delay - headDelay < 0))
                delay = Long.MAX_VALUE + headDelay;
        }
        return delay;
    }

    boolean canRunInCurrentRunState(RunnableScheduledFuture<?> task) {
        if (!isShutdown())
            return true;
        if (stopped)
            return false;
        return task.isPeriodic()
                ? getContinueExistingPeriodicTasksAfterShutdownPolicy()
                : (getExecuteExistingDelayedTasksAfterShutdownPolicy()
                || task.getDelay(NANOSECONDS) <= 0);
    }

    void reExecutePeriodic(RunnableScheduledFuture<?> task) {
        if (canRunInCurrentRunState(task)) {
            super.getQueue().add(task);
            if (canRunInCurrentRunState(task) || !remove(task)) {
                prestartCoreThread();
                return;
            }
        }
        task.cancel(false);
    }

    private class ScheduledFutureTask<V>
            extends FutureTask<V> implements RunnableScheduledFuture<V> {

        private final long sequenceNumber;
        private final ScheduleConfig<V> config;
        private final long period;
        private volatile long time;
        private RunnableScheduledFuture<V> outerTask = this;
        private V outcome;
        private long runNumber = 1;
        private long lastExecution;

        ScheduledFutureTask(Runnable runnable, V result, long triggerTime,
                            ScheduleConfig<V> config, long sequenceNumber) {
            super(runnable, result);
            this.time = triggerTime;
            this.config = config;
            this.period = config.period().isZero() ? -config.repeatAfter().toNanos() : config.period().toNanos();
            this.sequenceNumber = sequenceNumber;
        }

        ScheduledFutureTask(Callable<V> callable, long triggerTime,
                            ScheduleConfig<V> config, long sequenceNumber) {
            super(callable);
            this.time = triggerTime;
            this.config = config;
            this.period = config.period().isZero() ? config.repeatAfter().toNanos() : config.period().toNanos();
            this.sequenceNumber = sequenceNumber;
        }

        public long getDelay(TimeUnit unit) {
            return unit.convert(time - System.nanoTime(), NANOSECONDS);
        }

        public int compareTo(Delayed other) {
            if (other == this) // compare zero if same object
                return 0;
            if (other instanceof ScheduledFutureTask<?> x) {
                long diff = time - x.time;
                if (diff < 0) {
                    return -1;
                } else if (diff > 0) {
                    return 1;
                } else if (sequenceNumber < x.sequenceNumber) {
                    return -1;
                } else {
                    return 1;
                }
            }
            long diff = getDelay(NANOSECONDS) - other.getDelay(NANOSECONDS);
            return (diff < 0) ? -1 : (diff > 0) ? 1 : 0;
        }

        public boolean isPeriodic() {
            return period != 0;
        }

        private void setNextRunTime() {
            long p = period;
            if (p > 0) {
                time += p;
            } else {
                time = triggerTime(-p);
            }
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            var cancelled = super.cancel(mayInterruptIfRunning);
            if (cancelled && getRemoveOnCancelPolicy()) {
                remove(this);
            }
            return cancelled;
        }

        @Override
        public void run() {
            var thisExecution = System.nanoTime();
            if (!canRunInCurrentRunState(this)) {
                cancel(false);
            } else if (!isPeriodic()) {
                if (canRun(thisExecution)) {
                    super.run();
                }
            } else {
                if (canRun(thisExecution)) {
                    if (super.runAndReset() && !cancelRerun(thisExecution)) {
                        setNextRunTime();
                        reExecutePeriodic(outerTask);
                    }
                    runNumber++;
                    lastExecution = thisExecution;
                } else {
                    setNextRunTime();
                    reExecutePeriodic(outerTask);
                }
            }
        }

        private boolean canRun(long thisExecution) {
            var executionPredicate = config.executionCondition();
            return executionPredicate == null || executionPredicate.test(new ExecutionInfo<>(outcome, runNumber + 1, lastExecution, thisExecution));
        }

        private boolean cancelRerun(long thisExecution) {
            var cancelPredicate = config.cancelCondition();
            return !canRepeat() || (cancelPredicate != null && cancelPredicate.test(new ExecutionInfo<>(outcome, runNumber + 1, lastExecution, thisExecution)));
        }

        private boolean canRepeat() {
            return config.repeatCount() == -1 || runNumber < config.repeatCount();
        }

        @Override
        protected void set(V v) {
            super.set(v);
            outcome = v;
        }
    }
}
