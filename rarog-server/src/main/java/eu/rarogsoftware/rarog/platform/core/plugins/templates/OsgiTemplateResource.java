package eu.rarogsoftware.rarog.platform.core.plugins.templates;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.plugins.ResourceHoldingPlugin;
import org.thymeleaf.templateresource.ITemplateResource;
import org.thymeleaf.util.StringUtils;
import org.thymeleaf.util.Validate;

import java.io.*;

@SuppressFBWarnings(value = "URLCONNECTION_SSRF_FD", justification = "URLs are provided by OSGi, not by user")
class OsgiTemplateResource implements ITemplateResource {
    private final ResourceHoldingPlugin plugin;
    private final String resourceName;
    private final String characterEncoding;

    public OsgiTemplateResource(ResourceHoldingPlugin plugin, String resourceName, String characterEncoding) {
        this.plugin = plugin;
        this.resourceName = resourceName;
        this.characterEncoding = characterEncoding;
    }

    static String computeBaseName(final String path) {
        // shamelessly stolen from TemplateResourceUtils.computeBaseName
        if (path == null || path.length() == 0) {
            return null;
        }
        final String basePath = (path.charAt(path.length() - 1) == '/' ? path.substring(0, path.length() - 1) : path);
        final int slashPos = basePath.lastIndexOf('/');
        if (slashPos != -1) {
            final int dotPos = basePath.lastIndexOf('.');
            if (dotPos != -1 && dotPos > slashPos + 1) {
                return basePath.substring(slashPos + 1, dotPos);
            }
            return basePath.substring(slashPos + 1);
        } else {
            final int dotPos = basePath.lastIndexOf('.');
            if (dotPos != -1) {
                return basePath.substring(0, dotPos);
            }
        }
        return (basePath.length() > 0 ? basePath : null);
    }

    static String computeRelativeLocation(final String location, final String relativeLocation) {
        // yet again no shame -> TemplateResourceUtils.computeRelativeLocation
        final int separatorPos = location.lastIndexOf('/');
        if (separatorPos != -1) {
            final StringBuilder relativeBuilder = new StringBuilder(location.length() + relativeLocation.length());
            relativeBuilder.append(location, 0, separatorPos);
            if (relativeLocation.charAt(0) != '/') {
                relativeBuilder.append('/');
            }
            relativeBuilder.append(relativeLocation);
            return relativeBuilder.toString();
        }
        return relativeLocation;
    }

    @Override
    public String getDescription() {
        return (plugin != null ? plugin.getKey() : "nullPlugin") + ":" + resourceName;
    }

    @Override
    public String getBaseName() {
        return computeBaseName(resourceName);
    }

    @Override
    public boolean exists() {
        return plugin != null && plugin.getResource(resourceName) != null;
    }

    @Override
    public Reader reader() throws IOException {
        if (plugin == null) {
            throw new FileNotFoundException("Bundle for resource is not specified");
        }
        var resourceUrl = plugin.getResource(resourceName);
        if (resourceUrl == null) {
            throw new FileNotFoundException(String.format("Bundle `%s` doesn't contains resource \"%s\"", plugin.getKey(), resourceName));
        }
        final InputStream inputStream = resourceUrl.getInputStream();
        if (inputStream == null) {
            throw new FileNotFoundException(String.format("Bundle `%s` resource \"%s\" cannot be resolved", plugin.getKey(), resourceName));
        }
        if (!StringUtils.isEmptyOrWhitespace(this.characterEncoding)) {
            return new BufferedReader(new InputStreamReader(new BufferedInputStream(inputStream), this.characterEncoding));
        }
        return new BufferedReader(new InputStreamReader(new BufferedInputStream(inputStream)));
    }

    @Override
    public ITemplateResource relative(String relativeLocation) {
        Validate.notEmpty(relativeLocation, "Relative Path cannot be null or empty");
        final String fullRelativeLocation = computeRelativeLocation(resourceName, relativeLocation);
        return new OsgiTemplateResource(plugin, fullRelativeLocation, this.characterEncoding);
    }
}
