package eu.rarogsoftware.rarog.platform.app.security;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.app.configuration.security.WebAuthenticationDetails;
import eu.rarogsoftware.rarog.platform.core.security.LoginAttemptService;
import eu.rarogsoftware.rarog.platform.core.common.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class SpringLoginAttemptService implements LoginAttemptService, ApplicationListener<AbstractAuthenticationEvent> {
    private final Logger logger = LoggerFactory.getLogger(SpringLoginAttemptService.class);
    private final Map<Long, List<LoginAttempt>> loginAttempts = new ConcurrentHashMap<>();
    private final UserDetailsManager userDetailsManager;

    public SpringLoginAttemptService(UserDetailsManager userDetailsManager) {
        this.userDetailsManager = userDetailsManager;
    }

    @Override
    public void registerAttempt(StandardUser user, LoginAttempt loginAttempt) {
        loginAttempts.computeIfAbsent(user.getId(), key -> new ArrayList<>())
                .add(loginAttempt);
    }

    @Override
    public List<LoginAttempt> getLoginAttempts(StandardUser user, int numberOfAttempts) {
        List<LoginAttempt> attemptsList = loginAttempts.getOrDefault(user.getId(), Collections.emptyList());
        if (attemptsList.size() > numberOfAttempts) {
            return attemptsList.stream().skip((long) attemptsList.size() - numberOfAttempts).toList();
        } else {
            return Collections.unmodifiableList(attemptsList);
        }
    }

    @Override
    public List<LoginAttempt> getLoginAttempts(StandardUser user, Range<Instant> range) {
        return loginAttempts.getOrDefault(user.getId(), Collections.emptyList())
                .stream().filter(loginAttempt -> range.isInRange(loginAttempt.attemptTime()))
                .toList();
    }

    @Override
    public void onApplicationEvent(AbstractAuthenticationEvent authenticationEvent) {
        String ip = null, userAgent = null;
        boolean successful;
        if (authenticationEvent instanceof AuthenticationSuccessEvent) {
            successful = true;
        } else if (authenticationEvent instanceof AbstractAuthenticationFailureEvent) {
            successful = false;
        } else {
            logger.trace("Unrecognized authentication event: {}", authenticationEvent);
            return;
        }
        if (authenticationEvent.getSource() instanceof UsernamePasswordAuthenticationToken token) {
            if (token.getDetails() instanceof WebAuthenticationDetails details) {
                ip = details.remoteAddress();
                userAgent = details.userAgent();
            }
        }

        StandardUser user = null;

        if (authenticationEvent.getAuthentication().getPrincipal() instanceof StandardUser standardUser) {
            user = standardUser;
        }
        if (authenticationEvent.getAuthentication().getPrincipal() instanceof String userName) {
            user = (StandardUser) userDetailsManager.loadUserByUsername(userName);
        }

        if (user != null) {
            registerAttempt(user, new LoginAttempt(Instant.now(), successful, ip, userAgent));
        } else {
            logger.warn("Unrecognized application user for authentication event: {}", authenticationEvent);
        }
    }
}
