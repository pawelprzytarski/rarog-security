package eu.rarogsoftware.rarog.platform.core.metrics.noop;

import eu.rarogsoftware.rarog.platform.api.metrics.Counter;

import java.util.Collection;

class NoopCounter implements Counter {
    @Override
    public void increase(double amount) {
        // Do nothing as it is Noop implementation
    }

    @Override
    public Counter labels(Collection<String> labels) {
        return this;
    }

    @Override
    public Counter labels(String... labels) {
        return this;
    }
}
