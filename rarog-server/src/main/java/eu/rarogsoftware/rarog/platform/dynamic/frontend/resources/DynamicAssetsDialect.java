package eu.rarogsoftware.rarog.platform.dynamic.frontend.resources;

import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;

import java.util.HashSet;
import java.util.Set;

public class DynamicAssetsDialect extends AbstractProcessorDialect {
    private final boolean disableIntegrity;

    public DynamicAssetsDialect(boolean disableIntegrity) {
        super("Dynamic assets helpers", "dynamicassets", 1000);
        this.disableIntegrity = disableIntegrity;
    }

    @Override
    public Set<IProcessor> getProcessors(String dialectPrefix) {
        Set<IProcessor> processors = new HashSet<>();
        processors.add(new DynamicJsLoaderProcessor(dialectPrefix, disableIntegrity));
        processors.add(new DynamicCssLoaderProcessor(dialectPrefix, disableIntegrity));
        return processors;
    }
}
