package eu.rarogsoftware.rarog.platform.core.plugins.web.filters;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginFilterMapping;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginFilterMappingDescriptor;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModuleComponent;
import eu.rarogsoftware.rarog.platform.api.metrics.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class responsible for matching {@link jakarta.servlet.Filter}s from plugins to the request.
 */
@AutoRegisterFeatureModuleComponent(PluginFilterMappingDescriptor.class)
public class PluginFilterResolver implements FeatureModule<PluginFilterMappingDescriptor> {

    private final Logger logger = LoggerFactory.getLogger(PluginFilterResolver.class);
    private final Map<String, PluginFilterMappingDescriptor> descriptors = new ConcurrentHashMap<>();
    private List<PluginFilterMapping> filterMappings = Collections.emptyList();
    private final Map<String, AntPathRequestMatcher> matchers = new ConcurrentHashMap<>();
    private final Gauge filterCountGauge;
    private final Histogram filterResolutionHistogram;
    private final Histogram chainExecutionTimeHistogram;

    /**
     * Contructor.
     *
     * @param metricsService service used to publish resolution time metrics.
     */
    @Autowired
    public PluginFilterResolver(MetricsService metricsService) {
        this.filterCountGauge = metricsService.createGauge(MetricSettings.settings()
            .name("rarog_security_registered_plugin_filter_count")
            .description("Indicates the amount of registered filters in the system.")
            .unit("count"));
        this.filterResolutionHistogram = metricsService.createHistogram(HistogramSettings.settings()
            .name("rarog_security_plugin_filter_resolution_time")
            .description("Measures the time required for resolving plugin filters.")
            .unit("seconds")
            .buckets(1e-5, 2e-5, 3e-5, 4e-5, 5e-5, 1e-4, 1e-3, 1e-2, 1e-1)
        );
        this.chainExecutionTimeHistogram = metricsService.createHistogram(HistogramSettings.settings()
            .name("rarog_security_plugin_filter_chain_execution_time")
            .description("Indicates plugin filter chain execution time regardless of the filter count.")
            .unit("seconds")
            .buckets(1e-6, 2e-6, 3e-6, 4e-6, 5e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1)
        );
    }

    /**
     * This method creates a special {@link FilterChain} implementation, which contains {@link jakarta.servlet.Filter}s
     * registered by plugins. Filters are chosen based on request path and dispatcher type. If they match
     * the registration, Filter is added to the chain.
     *
     * @param request       The request which {@link jakarta.servlet.DispatcherType} and URL will be used to match Filters.
     * @param originalChain The chain which will be invoked after the plugin Filters.
     * @return The chain containing matching Filters
     * @throws IOException      forwarded from Filters
     * @throws ServletException forwarded from Filters
     */
    public PluginFilterChain getFilterChainForRequest(final ServletRequest request, final FilterChain originalChain)
        throws IOException, ServletException {
        PluginFilterChain pluginFilterChain = new PluginFilterChain(originalChain, chainExecutionTimeHistogram);

        final HttpServletRequest httpRequest = (HttpServletRequest) request;

        final String requestPath = httpRequest.getServletPath() + (httpRequest.getPathInfo() == null ? "" : httpRequest.getPathInfo());

        logger.trace("Matching plugin filters for request path: {}.", requestPath);
        try (var ignored = filterResolutionHistogram.startTimer()) {
            filterMappings.stream()
                .filter(mapping -> {
                    logger.trace("Mapping {} filter dispatcher types {} to request dispatcher {}.",
                        mapping.filter().getClass().getName(), mapping.dispatchers(), request.getDispatcherType());
                    return mapping.dispatchers().contains(request.getDispatcherType());
                })
                .filter(mapping -> {
                    logger.trace("Mapping {} filter patterns {} to request URL {}",
                        mapping.filter().getClass().getName(), mapping.urlPatterns(), requestPath);
                    return mapping.urlPatterns().stream()
                        .anyMatch(urlPattern -> isUrlMatching(urlPattern, httpRequest));
                })
                .forEach(mapping -> {
                    logger.trace("Adding {} filter to the chain.", mapping.filter().getClass().getName());
                    pluginFilterChain.addFilter(mapping.filter());
                });
        } catch (Exception e) {
            logger.warn("Exception occurred when closing timer.", e);
        }
        return pluginFilterChain;
    }

    @Override
    public void plugDescriptor(Plugin plugin, PluginFilterMappingDescriptor descriptor) {
        logger.debug("Registering plugin filter mapping for plugin {}", plugin.getKey());
        if (descriptors.containsKey(plugin.getKey())) {
            logger.error("Failed to register filter mapping for plugin {}. Mapping for this plugin already exists.", plugin.getKey());
            throw new IllegalArgumentException("Cannot override already existing filter mapping");
        }
        descriptors.put(plugin.getKey(), descriptor);
        logger.info("Registered filter mapping for plugin {}", plugin.getKey());
        refreshCachedMappings();
    }

    @Override
    public void unplugDescriptor(Plugin plugin, PluginFilterMappingDescriptor descriptor) {
        logger.debug("Unregistering plugin filter mapping for plugin {}", plugin.getKey());
        descriptors.remove(plugin.getKey());
        refreshCachedMappings();
    }

    private void refreshCachedMappings() {
        filterMappings = descriptors.values()
            .stream()
            .flatMap(descriptor -> descriptor.filterMappings().stream()).toList();
        filterCountGauge.set(filterMappings.size());
        matchers.clear();
    }


    /**
     * Return <code>true</code> if the context-relative request path
     * matches the requirements of the specified filter mapping;
     * otherwise, return <code>false</code>.
     * Uses {@link AntPathRequestMatcher} to match patterns.
     *
     * @param urlPattern The pattern being checked
     * @param request    Request to be matched
     */
    private boolean isUrlMatching(final String urlPattern, final HttpServletRequest request) {
        return matchers.computeIfAbsent(urlPattern, AntPathRequestMatcher::new).matches(request);
    }
}
