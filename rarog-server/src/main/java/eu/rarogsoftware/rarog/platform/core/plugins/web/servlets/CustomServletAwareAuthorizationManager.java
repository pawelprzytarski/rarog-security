package eu.rarogsoftware.rarog.platform.core.plugins.web.servlets;

import org.springframework.security.authorization.AuthenticatedAuthorizationManager;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Component
public class CustomServletAwareAuthorizationManager implements AuthorizationManager<RequestAuthorizationContext> {
    private final SmartSwitchingServlet smartSwitchingServlet;
    private AuthorizationManager<RequestAuthorizationContext> defaultManager;

    public CustomServletAwareAuthorizationManager(SmartSwitchingServlet smartSwitchingServlet) {
        this.smartSwitchingServlet = smartSwitchingServlet;
        this.defaultManager = new AuthenticatedAuthorizationManager<>();
    }

    public void setDefaultManager(AuthorizationManager<RequestAuthorizationContext> defaultManager) {
        this.defaultManager = defaultManager;
    }

    @Override
    public void verify(Supplier<Authentication> authentication, RequestAuthorizationContext object) {
        var servletManager = smartSwitchingServlet.getServletsAuthorizationManager(object.getRequest());
        if (servletManager.isPresent()) {
            servletManager.get().verify(authentication, object);
        } else {
            defaultManager.verify(authentication, object);
        }
    }

    @Override
    public AuthorizationDecision check(Supplier<Authentication> authentication, RequestAuthorizationContext object) {
        return smartSwitchingServlet.getServletsAuthorizationManager(object.getRequest())
                .map(manager -> manager.check(authentication, object))
                .orElseGet(() -> defaultManager.check(authentication, object));
    }
}
