package eu.rarogsoftware.rarog.platform.core.boot;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressFBWarnings(value = "UNVALIDATED_REDIRECT", justification = "caused by request.getContextPath()")
public class BootFilter extends HttpFilter {
    private boolean redirectEnabled = true;

    void setRedirectEnabled(boolean enabled) {
        this.redirectEnabled = enabled;
    }

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (redirectEnabled && (isNotBootstrapPage(request) || !request.getMethod().equalsIgnoreCase("GET"))) {
            response.sendRedirect(request.getContextPath() + "/boot/");
            return;
        }
        chain.doFilter(request, response);
    }

    private boolean isNotBootstrapPage(HttpServletRequest request) {
        return !(request.getServletPath().startsWith("/boot/") || request.getServletPath().equals("/boot") || request.getServletPath().equals("/error"));
    }
}
