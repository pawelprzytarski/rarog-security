package eu.rarogsoftware.rarog.platform.core.task;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.task.AdvancedScheduledExecutorService;
import eu.rarogsoftware.rarog.platform.api.task.ScheduleConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

class MonitoredScheduledThreadPoolExecutor implements AdvancedScheduledExecutorService {
    private final Logger logger = LoggerFactory.getLogger(MonitoredScheduledThreadPoolExecutor.class);
    private final AdvancedScheduledExecutorService delegate;
    private final Gauge activeTaskCountGauge;
    private final Counter executedTaskCounter;
    private final Histogram taskDurationHistogram;
    private final Histogram taskExecutionTimeDiffHistogram;
    private final Counter failedTaskCounter;

    MonitoredScheduledThreadPoolExecutor(MetricsService metricsService,
                                         AdvancedScheduledExecutorService delegate) {
        this.delegate = delegate;
        this.activeTaskCountGauge = metricsService.createGauge(MetricSettings.settings()
                .name("rarog_task_manager_active_task_count")
                .description("Shows count of active tasks submitted to TaskManager")
        );
        this.executedTaskCounter = metricsService.createCounter(MetricSettings.settings()
                .name("rarog_task_manager_executed_task_count")
                .description("Shows count of tasks executed with TaskManager during system lifetime")
        );
        this.failedTaskCounter = metricsService.createCounter(MetricSettings.settings()
                .name("rarog_task_manager_failed_task_count")
                .description("Shows count of tasks executed with TaskManager that thrown exception")
        );
        this.taskDurationHistogram = metricsService.createHistogram(HistogramSettings.settings()
                .name("rarog_task_manager_task_duration")
                .description("Shows distribution of tasks durations")
                .unit("seconds")
                .buckets(0.001, 0.01, 0.1, 1, 10, 60, 600, 3600)
        );
        this.taskExecutionTimeDiffHistogram = metricsService.createHistogram(HistogramSettings.settings()
                .name("rarog_task_manager_task_declared_execution_diff")
                .description("Shows distribution of difference between declared execution start and real execution start")
                .unit("seconds")
                .buckets(-1, -0.1, -0.01, -0.001, 0, 0.001, 0.01, 0.1, 1, 10)
        );
    }

    private Runnable wrapCommand(Runnable command) {
        logWrapTrace(command);
        return new MonitoredCallable<Void>(command, Duration.ZERO, Duration.ZERO);
    }

    private void logWrapTrace(Runnable command) {
        logTraceOfWrap(command.getClass());
    }

    private void logTraceOfWrap(Class<?> commandClass) {
        if (logger.isTraceEnabled()) {
            logger.trace("Wrapping monitoring for {}. Stack trace:\n{}", commandClass, StackWalker.getInstance().walk(
                    s -> s.skip(3).limit(5).map(Object::toString).collect(Collectors.joining("\n"))
            ));
        }
    }

    private Runnable wrapCommand(Runnable command, Duration delay) {
        logWrapTrace(command);
        return new MonitoredCallable<Void>(command, delay, Duration.ZERO);
    }

    private Runnable wrapCommand(Runnable command, Duration delay, Duration period) {
        logWrapTrace(command);
        return new MonitoredCallable<Void>(command, delay, period);
    }

    private <V> Callable<V> wrapCommand(Callable<V> command) {
        logWrapTrace(command);
        return new MonitoredCallable<>(command, Duration.ZERO, Duration.ZERO);
    }

    private <V> void logWrapTrace(Callable<V> command) {
        logTraceOfWrap(command.getClass());
    }

    private <V> Callable<V> wrapCommand(Callable<V> command, Duration delay) {
        logWrapTrace(command);
        return new MonitoredCallable<>(command, delay, Duration.ZERO);
    }

    private <V> Callable<V> wrapCommand(Callable<V> command, Duration delay, Duration period) {
        logWrapTrace(command);
        return new MonitoredCallable<>(command, delay, period);
    }

    @Override
    public ScheduledFuture<?> scheduleAdvanced(Runnable command, ScheduleConfig<?> config) {
        return delegate.scheduleAdvanced(wrapCommand(command, config.initialDelay(),
                        config.period().isZero() ? config.repeatAfter().negated() : config.period()),
                config);
    }

    @Override
    public <T> ScheduledFuture<T> scheduleAdvanced(Callable<T> command, ScheduleConfig<T> config) {
        return delegate.scheduleAdvanced(wrapCommand(command, config.initialDelay(),
                        config.period().isZero() ? config.repeatAfter().negated() : config.period()),
                config);
    }

    @Override
    public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
        return delegate.schedule(wrapCommand(command, Duration.of(delay, unit.toChronoUnit())), delay, unit);
    }

    @Override
    public <V> ScheduledFuture<V> schedule(Callable<V> command, long delay, TimeUnit unit) {
        return delegate.schedule(wrapCommand(command, Duration.of(delay, unit.toChronoUnit())), delay, unit);
    }

    @Override
    public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
        return delegate.scheduleAtFixedRate(
                wrapCommand(command, Duration.of(initialDelay, unit.toChronoUnit()), Duration.of(period, unit.toChronoUnit())),
                initialDelay,
                period,
                unit);
    }

    @Override
    public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
        return delegate.scheduleWithFixedDelay(
                wrapCommand(command, Duration.of(initialDelay, unit.toChronoUnit()), Duration.of(delay, unit.toChronoUnit()).negated()),
                initialDelay,
                delay,
                unit);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        return delegate.invokeAll(tasks.stream().map(this::wrapCommand).toList());
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
        return delegate.invokeAll(tasks.stream().map(this::wrapCommand).toList(), timeout, unit);
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
        return delegate.invokeAny(tasks.stream().map(this::wrapCommand).toList());
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return delegate.invokeAny(tasks.stream().map(this::wrapCommand).toList(), timeout, unit);
    }

    @Override
    public void execute(Runnable command) {
        delegate.execute(wrapCommand(command));
    }

    @Override
    public void shutdown() {
        delegate.shutdown();
    }

    @Override
    public List<Runnable> shutdownNow() {
        return delegate.shutdownNow();
    }

    @Override
    public boolean isShutdown() {
        return delegate.isShutdown();
    }

    @Override
    public boolean isTerminated() {
        return delegate.isTerminated();
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return delegate.awaitTermination(timeout, unit);
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        return delegate.submit(wrapCommand(task));
    }

    @Override
    public <T> Future<T> submit(Runnable task, T result) {
        return delegate.submit(wrapCommand(task), result);
    }

    @Override
    public Future<?> submit(Runnable task) {
        return delegate.submit(wrapCommand(task));
    }

    private class MonitoredCallable<V> implements Callable<V>, Runnable {
        private final Callable<V> callable;
        private final Runnable runnable;
        private final long period;
        private long expectedNextRun;

        public MonitoredCallable(Callable<V> command, Duration initialDelay, Duration period) {
            this(command, null, initialDelay.toNanos(), period.toNanos());
        }

        public MonitoredCallable(Runnable command, Duration initialDelay, Duration period) {
            this(null, command, initialDelay.toNanos(), period.toNanos());
        }

        public MonitoredCallable(Callable<V> callable, Runnable runnable, long initialDelay, long period) {
            this.callable = callable;
            this.runnable = runnable;
            this.period = period;
            this.expectedNextRun = System.nanoTime() + initialDelay;
        }

        @Override
        public V call() throws Exception {
            monitorExecutionStartDiff();
            try (var ignored = taskDurationHistogram.startTimer()) {
                monitorCounts();
                if (logger.isTraceEnabled()) {
                    logger.trace("Executing callable: {} at {}", callable.getClass(), System.currentTimeMillis());
                }
                return callable.call();
            } catch (Exception e) {
                monitorFailed();
                throw e;
            } finally {
                finalizeMonitoring();
            }
        }

        private void monitorFailed() {
            failedTaskCounter.increment();
        }

        private void finalizeMonitoring() {
            expectedNextRun = calculateNextRun();
            activeTaskCountGauge.decrement();
        }

        private void monitorCounts() {
            activeTaskCountGauge.increment();
            executedTaskCounter.increment();
        }

        private void monitorExecutionStartDiff() {
            taskExecutionTimeDiffHistogram.observe((System.nanoTime() - expectedNextRun) / 1_000_000_000.0);
        }


        private long calculateNextRun() {
            if (period > 0) {
                return expectedNextRun + period;
            } else {
                return System.nanoTime() + period;
            }
        }

        @Override
        public void run() {
            monitorExecutionStartDiff();
            try (var ignored = taskDurationHistogram.startTimer()) {
                monitorCounts();
                if (logger.isTraceEnabled()) {
                    logger.trace("Executing runnable: {} at {}", runnable.getClass(), System.currentTimeMillis());
                }
                runnable.run();
            } catch (RuntimeException e) {
                monitorFailed();
                throw e;
            } catch (Exception e) {
                //ignore because thrown by timer
            } finally {
                finalizeMonitoring();
            }
        }
    }
}
