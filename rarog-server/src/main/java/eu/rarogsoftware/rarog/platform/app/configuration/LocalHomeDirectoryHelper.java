package eu.rarogsoftware.rarog.platform.app.configuration;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.settings.HomeDirectoryHelper;
import org.springframework.util.StringUtils;

import java.io.File;

@SuppressFBWarnings("PATH_TRAVERSAL_IN")
public class LocalHomeDirectoryHelper implements HomeDirectoryHelper {
    private static final LocalHomeDirectoryHelper INSTANCE = new LocalHomeDirectoryHelper();
    private static final String HOME_PROPERTY_NAME = "home.directory";
    private static final String HOME_ENV_NAME = "RAROG_HOME";
    private static String homeDirectory;
    private static File homeFile;
    private static String configDir;
    private static String logsDir;
    private static String cacheDir;
    private static String pluginsDir;

    public static LocalHomeDirectoryHelper getInstance() {
        return INSTANCE;
    }

    private static String getHomeDirectory() {
        String property = System.getProperty(HOME_PROPERTY_NAME, System.getenv(HOME_ENV_NAME));
        if (property == null) {
            return null;
        }
        return new File(property).getAbsolutePath();
    }

    @Override
    public String getHome() {
        if (!StringUtils.hasText(homeDirectory)) {
            homeDirectory = getHomeDirectory();
            if (!StringUtils.hasText(homeDirectory)) {
                throw new IllegalStateException("Home directory is null or empty. Set system property `" + HOME_PROPERTY_NAME + "` or env variable `" + HOME_ENV_NAME + "` to set home directory");
            }
        }
        return homeDirectory;
    }

    @Override
    public File getHomeFile() {
        if (homeFile == null) {
            homeFile = getHomeDirectoryFile(getHome());
        }
        return homeFile;
    }

    @Override
    public String getConfig() {
        if (configDir == null) {
            configDir = StringUtils.trimTrailingCharacter(getHome(), '/') + "/config/";
        }
        return configDir;
    }

    @Override
    public File getConfigFile() {
        return getHomeDirectoryFile(getConfig());
    }

    @Override
    public String getLogs() {
        if (logsDir == null) {
            logsDir = System.getProperty("rarog.logs.directory");
            if (!StringUtils.hasText(logsDir)) {
                logsDir = StringUtils.trimTrailingCharacter(getHome(), '/') + "/logs/";
            }
        }
        return logsDir;
    }

    @Override
    public File getLogsFile() {
        return getHomeDirectoryFile(getLogs());
    }

    @Override
    public String getCache() {
        if (cacheDir == null) {
            cacheDir = StringUtils.trimTrailingCharacter(getHome(), '/') + "/cache/";
        }
        return cacheDir;
    }

    @Override
    public File getCacheFile() {
        return getHomeDirectoryFile(getCache());
    }

    @Override
    public String getPlugins() {
        if (pluginsDir == null) {
            pluginsDir = StringUtils.trimTrailingCharacter(getHome(), '/') + "/plugins/";
        }
        return pluginsDir;
    }

    @Override
    public File getPluginsFile() {
        return getHomeDirectoryFile(getPlugins());
    }

    public void configureSystemProperties() {
        System.setProperty("rarog.home.directory", getHome());
        System.setProperty("rarog.logs.directory", getLogs());
    }

    private File getHomeDirectoryFile(String path) {
        var file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        } else if (!file.isDirectory()) {
            throw new IllegalStateException("Rarog home directory %s is not a directory, remove the file and restart the application."
                    .formatted(file.getAbsolutePath()));
        }
        return file;
    }
}
