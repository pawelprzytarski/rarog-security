package eu.rarogsoftware.rarog.platform.core.metrics.prometheus;

import eu.rarogsoftware.rarog.platform.api.metrics.Gauge;

import java.util.concurrent.Callable;

class PrometheusGauge extends PrometheusMetric implements Gauge {
    private final io.prometheus.client.Gauge delegate;
    private final io.prometheus.client.Gauge.Child child;


    public PrometheusGauge(io.prometheus.client.Gauge gauge) {
        this(gauge, null);
    }

    public PrometheusGauge(io.prometheus.client.Gauge parent, io.prometheus.client.Gauge.Child child) {
        super(parent);
        this.delegate = parent;
        this.child = child;
    }

    @Override
    public void increase(double amount) {
        if (child != null) {
            child.inc(amount);
        } else {
            delegate.inc(amount);
        }
    }

    @Override
    public void decrease(double amount) {
        if (child != null) {
            child.dec(amount);
        } else {
            delegate.dec(amount);
        }
    }

    @Override
    public void set(double value) {
        if (child != null) {
            child.set(value);
        } else {
            delegate.set(value);
        }
    }

    @Override
    public Timer startTimer() {
        var timer = child != null ? child.startTimer() : delegate.startTimer();
        return new Timer() {
            @Override
            public double stop() {
                return timer.setDuration();
            }

            @Override
            public void close() {
                timer.close();
            }
        };
    }

    @Override
    public double measureExecutionTime(Runnable timeable) {
        return child != null ? child.setToTime(timeable) : delegate.setToTime(timeable);
    }

    @Override
    public <T> T measureExecutionTime(Callable<T> timeable) throws Exception {
        return child != null ? child.setToTime(timeable) : delegate.setToTime(timeable);
    }

    @Override
    public Gauge labels(String... labels) {
        return new PrometheusGauge(delegate, delegate.labels(labels));
    }
}
