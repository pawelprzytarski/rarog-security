package eu.rarogsoftware.rarog.platform.core.plugins.osgi;

import javax.annotation.PreDestroy;

public interface OsgiMiddlewareManager {
    boolean isStarted();

    void startSystem();

    @PreDestroy
    void stopSystem();
}
