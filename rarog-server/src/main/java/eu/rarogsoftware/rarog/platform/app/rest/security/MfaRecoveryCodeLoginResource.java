package eu.rarogsoftware.rarog.platform.app.rest.security;

import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaEndpoint;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaStore;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.security.mfa.recovery.RecoveryCodesService;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/login/mfa/recovery-codes")
@Hidden
public class MfaRecoveryCodeLoginResource {
    private final MfaStore mfaStore;
    private final RecoveryCodesService recoveryCodesService;

    public MfaRecoveryCodeLoginResource(MfaStore mfaStore, RecoveryCodesService recoveryCodesService) {
        this.mfaStore = mfaStore;
        this.recoveryCodesService = recoveryCodesService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @MfaEndpoint
    public ResponseEntity<RecoveryCodeResultBean> loginWithRecoveryCode(HttpServletRequest request, @RequestBody RecoveryCodeBean recoveryCodeBean, @AuthenticationPrincipal StandardUser user) {
        var result = recoveryCodesService.useRecoveryCode(user, recoveryCodeBean.recoveryCode());
        if (result.isSuccess()) {
            mfaStore.authorize(request);
            if (result.status() == RecoveryCodesService.RecoveryCodeLoginResult.Status.FOUND_AND_REGENERATED) {
                return ResponseEntity.ok(RecoveryCodeResultBean.regenerated(result.newCodes()));
            } else {
                return ResponseEntity.ok(RecoveryCodeResultBean.successful());
            }
        } else {
            return ResponseEntity.badRequest().body(RecoveryCodeResultBean.failure());
        }
    }

    @GetMapping("generate")
    @MfaEndpoint(registerOnly = true)
    public ResponseEntity<String> generateCodes() {
        return ResponseEntity.ok("{\"success\": false}");
    }

    public record RecoveryCodeBean(String recoveryCode) {
    }

    public record RecoveryCodeResultBean(boolean success, boolean regenerated, List<String> newCodes) {
        static RecoveryCodeResultBean successful() {
            return new RecoveryCodeResultBean(true, false, null);
        }

        static RecoveryCodeResultBean failure() {
            return new RecoveryCodeResultBean(false, false, null);
        }

        static RecoveryCodeResultBean regenerated(List<String> newCodes) {
            return new RecoveryCodeResultBean(true, true, newCodes);
        }
    }
}
