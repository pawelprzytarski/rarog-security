package eu.rarogsoftware.rarog.platform.app.configuration;

import eu.rarogsoftware.rarog.platform.api.plugins.ResourceHoldingPlugin;
import eu.rarogsoftware.rarog.platform.core.plugins.osgi.OsgiPluginManager;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.resource.ResourceResolver;
import org.springframework.web.servlet.resource.ResourceResolverChain;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;

@Component
public class OsgiPluginResourceResolver implements ResourceResolver {
    private static final String PLUGIN_PREFIX = "p/";
    private final OsgiPluginManager osgiPluginsManager;

    public OsgiPluginResourceResolver(OsgiPluginManager osgiPluginsManager) {
        this.osgiPluginsManager = osgiPluginsManager;
    }

    @Override
    public Resource resolveResource(HttpServletRequest request, String requestPath, List<? extends Resource> locations, ResourceResolverChain chain) {
        var resource = resolveResourceInternal(requestPath, locations);
        if (resource == null) {
            return chain.resolveResource(request, requestPath, locations);
        }
        return resource;
    }

    private Resource resolveResourceInternal(String requestPath, List<? extends Resource> locations) {
        if (!requestPath.startsWith(PLUGIN_PREFIX)) {
            return null;
        }
        requestPath = requestPath.substring(PLUGIN_PREFIX.length());
        if (requestPath.isEmpty()) {
            return null;
        }
        var pluginKey = requestPath.split("/")[0];
        if (pluginKey.length() + 2 > requestPath.length()) {
            return null;
        }
        var filePath = requestPath.substring(pluginKey.length() + 1);
        if (!StringUtils.hasText(filePath)) {
            return null;
        }
        var plugin = osgiPluginsManager.getPlugin(pluginKey);
        if (plugin instanceof ResourceHoldingPlugin resourcePlugin) {
            for (var location : locations) {
                if (location instanceof ClassPathResource classPathResource) {
                    var path = classPathResource.getPath();
                    var resourceUrl = resourcePlugin.getResource(path + filePath);
                    if (resourceUrl != null) {
                        return resourceUrl;
                    }
                }
            }
        }

        return null;
    }

    @Override
    public String resolveUrlPath(String resourcePath, List<? extends Resource> locations, ResourceResolverChain chain) {
        var resource = resolveResourceInternal(resourcePath, locations) != null ? resourcePath : null;
        if (resource == null) {
            return chain.resolveUrlPath(resourcePath, locations);
        }
        return resource;
    }
}
