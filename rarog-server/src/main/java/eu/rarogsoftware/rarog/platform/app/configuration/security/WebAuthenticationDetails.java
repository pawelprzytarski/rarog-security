package eu.rarogsoftware.rarog.platform.app.configuration.security;

public record WebAuthenticationDetails(String remoteAddress, String sessionId, String userAgent) {
}
