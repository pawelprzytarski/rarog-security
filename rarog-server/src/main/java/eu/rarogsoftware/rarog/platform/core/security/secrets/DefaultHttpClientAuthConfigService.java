package eu.rarogsoftware.rarog.platform.core.security.secrets;

import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.security.secrets.HttpClientAuthConfigService;
import eu.rarogsoftware.rarog.platform.core.security.secrets.http.ClientCertAuthConfig;
import eu.rarogsoftware.rarog.platform.core.security.secrets.http.HeaderAuthConfig;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ExportComponent
public class DefaultHttpClientAuthConfigService extends AbstractAuthConfigService<HttpClientAuthConfigService.AuthConfig> implements HttpClientAuthConfigService {
    private static final AuthConfig NOOP = new AuthConfig() {
    };

    public DefaultHttpClientAuthConfigService(BuildInSecretsStorage buildInSecretsStorage) {
        super(buildInSecretsStorage,
                () -> NOOP,
                Map.of(AUTH_TYPE, new AuthHeaderConfigResolver(),
                        CERT_TYPE, new CertConfigResolver()));
    }

    private static class AuthHeaderConfigResolver implements Resolver<AuthConfig> {
        @Override
        public AuthConfig resolve(String authData) {
            return new HeaderAuthConfig(authData);
        }

        @Override
        public AuthConfig resolve(Map<String, Object> authData) {
            return new HeaderAuthConfig(authData);
        }
    }

    private static class CertConfigResolver implements Resolver<AuthConfig> {
        @Override
        public AuthConfig resolve(String authData) {
            return new ClientCertAuthConfig(authData);
        }

        @Override
        public AuthConfig resolve(Map<String, Object> authData) {
            return new ClientCertAuthConfig(authData);
        }
    }
}
