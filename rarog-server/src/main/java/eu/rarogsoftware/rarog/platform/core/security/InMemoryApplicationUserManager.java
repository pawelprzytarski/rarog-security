package eu.rarogsoftware.rarog.platform.core.security;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

public class InMemoryApplicationUserManager extends ApplicationUserManager {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Map<String, StandardUser> users = new HashMap<>();

    public InMemoryApplicationUserManager() {
    }

    @Override
    public void createUser(UserDetails user) {
        Assert.isTrue(!userExists(user.getUsername()), "user should not exist");
        this.users.put(user.getUsername().toLowerCase(), createApplicationUser(user));
    }

    @Override
    public void deleteUser(String username) {
        this.users.remove(username.toLowerCase());
    }

    @Override
    public void updateUser(UserDetails user) {
        Assert.isTrue(userExists(user.getUsername()), "user should exist");
        this.users.put(user.getUsername().toLowerCase(), createApplicationUser(user));
    }

    @Override
    public boolean userExists(String username) {
        return this.users.containsKey(username.toLowerCase());
    }

    @Override
    protected void storeNewPassword(String username, String newPassword) {
        StandardUser user = this.users.get(username);
        Assert.state(user != null, "Current user doesn't exist in database.");
        user.setPassword(newPassword);
    }

    @Override
    public UserDetails updatePassword(UserDetails user, String newPassword) {
        String username = user.getUsername();
        StandardUser mutableUser = this.users.get(username.toLowerCase());
        mutableUser.setPassword(newPassword);
        return mutableUser;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails user = this.users.get(username.toLowerCase());
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return createApplicationUser(user);
    }
}
