package eu.rarogsoftware.rarog.platform.security.mfa.recovery;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;

import java.util.List;

public interface RecoveryCodesService {
    RecoveryCodeLoginResult useRecoveryCode(StandardUser user, String recoveryCode);

    List<String> generateRecoveryCodes(StandardUser user);

    record RecoveryCodeLoginResult(Status status, List<String> newCodes) {
        public static RecoveryCodeLoginResult success() {
            return new RecoveryCodeLoginResult(Status.FOUND, null);
        }

        public static RecoveryCodeLoginResult failure() {
            return new RecoveryCodeLoginResult(Status.NOT_FOUND, null);
        }

        static RecoveryCodeLoginResult regenerated(List<String> newCodes) {
            return new RecoveryCodeLoginResult(Status.FOUND_AND_REGENERATED, newCodes);
        }

        public boolean isSuccess() {
            return status != Status.NOT_FOUND;
        }

        public enum Status {
            NOT_FOUND,
            FOUND,
            FOUND_AND_REGENERATED
        }
    }
}
