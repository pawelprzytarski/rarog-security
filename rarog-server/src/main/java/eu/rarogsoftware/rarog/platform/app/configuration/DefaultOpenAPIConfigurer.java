package eu.rarogsoftware.rarog.platform.app.configuration;

import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.CsrfHelper;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.headers.Header;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.stereotype.Component;

@Component
public class DefaultOpenAPIConfigurer implements OpenAPIConfigurer {
    @Override
    public OpenAPI configure(OpenAPI openAPI) {
        return openAPI
                .components(new Components()
                        .addSecuritySchemes("logged_in",
                                new SecurityScheme()
                                        .description("User must be logged in to access resource")
                                        .type(SecurityScheme.Type.HTTP)
                                        .scheme("basic")
                                        .in(SecurityScheme.In.HEADER))
                        .addSecuritySchemes("admin",
                                new SecurityScheme()
                                        .description("User must be logged in as admin to access resource")
                                        .type(SecurityScheme.Type.HTTP)
                                        .scheme("basic")
                                        .in(SecurityScheme.In.HEADER))
                        .addHeaders(CsrfHelper.DISABLE_XSRF_ATTRIBUTE, new Header()
                                .required(false)
                                .description("Dev mode only header that disables CSRF protection mechanism for all requests"))
                )
                .info(new Info().title("Rarog Platform").version("0.0")
                        .license(new License().name("Apache 2.0").url("https://gitlab.com/rarogsoftware/rarog-platform/-/blob/master/LICENSE.txt")));
    }
}
