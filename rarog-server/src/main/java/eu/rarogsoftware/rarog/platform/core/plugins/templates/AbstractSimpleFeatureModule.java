package eu.rarogsoftware.rarog.platform.core.plugins.templates;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static eu.rarogsoftware.rarog.platform.core.plugins.web.PluginMappingHandlerMapping.HANDLER_PLUGIN_ATTRIBUTE;
import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

public abstract class AbstractSimpleFeatureModule<T extends FeatureDescriptor> implements FeatureModule<T> {
    private final Map<String, T> descriptorMap = new ConcurrentHashMap<>();

    protected static Plugin getPluginForCurrentRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            return (Plugin) requestAttributes.getAttribute(HANDLER_PLUGIN_ATTRIBUTE, SCOPE_REQUEST);
        }
        return null;
    }

    @Override
    public void plugDescriptor(Plugin plugin, T descriptor) {
        descriptorMap.put(plugin.getKey() + "-" + descriptor.getName(), descriptor);
        clearCache();
    }

    @Override
    public void unplugDescriptor(Plugin plugin, T descriptor) {
        descriptorMap.remove(plugin.getKey() + "-" + descriptor.getName());
        clearCache();
    }

    protected void clearCache() {
    }

    protected Map<String, T> getDescriptorMap() {
        return descriptorMap;
    }

}
