package eu.rarogsoftware.rarog.platform.app.rest;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.commons.utils.Pair;
import eu.rarogsoftware.rarog.platform.api.security.HumanOnly;
import eu.rarogsoftware.rarog.platform.api.user.management.AuthenticationType;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.api.user.management.UserManagementException;
import eu.rarogsoftware.rarog.platform.api.user.management.UserManager;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModule;
import eu.rarogsoftware.rarog.platform.core.plugins.templates.AbstractSimpleFeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AccountSettingsMenuDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AccountSettingsMenuElement;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoader;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoaderFactory;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "account/my/", produces = MediaType.APPLICATION_JSON_VALUE)
@AutoRegisterFeatureModule(AccountSettingsMenuDescriptor.class)
@HumanOnly
@Hidden
@SuppressFBWarnings({"IS2_INCONSISTENT_SYNC", "DC_DOUBLECHECK"})
public class MyAccountRestController extends AbstractSimpleFeatureModule<AccountSettingsMenuDescriptor> {
    private static final String PASSWORD_REGEX = "\\w{8,}";
    private final DynamicAssetsLoaderFactory dynamicAssetsLoaderFactory;
    private final PasswordEncoder passwordEncoder;
    private final UserManager userManager;
    private Map<String, List<MenuElement>> cachedElements;

    public MyAccountRestController(@Qualifier("main") DynamicAssetsLoaderFactory dynamicAssetsLoaderFactory,
                                   PasswordEncoder passwordEncoder,
                                   UserManager userManager) {
        this.dynamicAssetsLoaderFactory = dynamicAssetsLoaderFactory;
        this.passwordEncoder = passwordEncoder;
        this.userManager = userManager;
    }

    @GetMapping("navigation")
    Map<String, List<MenuElement>> getNavigationElements() {
        var elements = cachedElements;
        if (elements == null) { // fast check without locking
            synchronized (this) {
                if (cachedElements == null) { // someone could already calculate
                    // is it this famous functional programming?
                    cachedElements = new TreeMap<>(getDescriptorMap().values()
                            .stream()
                            .flatMap(descriptor -> descriptor.elements().stream())
                            .collect(Collectors.groupingBy(AccountSettingsMenuElement::category))
                            .entrySet().stream()
                            .map(entry -> Pair.of(entry.getKey(), entry.getValue().stream()
                                    .sorted(Comparator.comparing(AccountSettingsMenuElement::name))
                                    .map(element -> new MenuElement(element.key(), element.name(), element.resources()))
                                    .toList()))
                            .collect(Collectors.toMap(Pair::left, Pair::right)));
                    cachedElements.compute("", (key, list) -> list == null ? new ArrayList<>() : new ArrayList<>(list));
                    cachedElements.get("").add(0, new MenuElement("main",
                            "account.setting.main.title",
                            dynamicAssetsLoaderFactory
                                    .getNamedAssetsLoader("dynamicComponents")
                                    .flatMap(loader -> loader.getAssetsForEntryPoint("basicAccountSettings"))
                                    .map(DynamicAssetsLoader.EntrypointAssetData::js)
                                    .stream()
                                    .flatMap(List::stream)
                                    .map(DynamicAssetsLoader.WebpackAssetData::source)
                                    .toList()));
                }
                elements = cachedElements;
            }
        }
        return elements;
    }

    @Override
    protected void clearCache() {
        synchronized (this) {
            cachedElements = null;
        }
    }

    @GetMapping("main/settings")
    AccountSettings getCurrentAccountSettings(@AuthenticationPrincipal StandardUser rarogUser,
                                              Authentication authentication) {
        return new AccountSettings(rarogUser.getUsername(), canUserChangePassword(authentication), PASSWORD_REGEX);
    }

    private static boolean canUserChangePassword(Authentication authentication) {
        var organicAuthorityString = AuthenticationType.ORGANIC.getAuthority().getAuthority();
        return authentication.getAuthorities().stream()
                .anyMatch(grantedAuthority -> organicAuthorityString.equals(grantedAuthority.getAuthority()));
    }

    @PostMapping("main/settings/changePassword")
    ResponseEntity<GenericResponse> changePassword(@RequestBody PasswordChangeRequest request,
                                                   @AuthenticationPrincipal StandardUser rarogUser,
                                                   Authentication authentication) throws UserManagementException {
        if (!canUserChangePassword(authentication)) {
            return ResponseEntity.badRequest().body(new GenericResponse("account.setting.main.errors.password.change.disabled"));
        }
        if (request.newPassword == null || !request.newPassword.equals(request.repeatPassword)) {
            return ResponseEntity.badRequest().body(new GenericResponse("account.setting.main.errors.password.not.matching"));
        }
        var user = userManager.getUser(rarogUser.getId()); // because principal does not contain password
        if (request.oldPassword == null || !passwordEncoder.matches(request.oldPassword, user.getPassword())) {
            return ResponseEntity.badRequest().body(new GenericResponse("account.setting.main.errors.password.incorrect"));
        }
        userManager.updatePassword(user.getId(), request.newPassword);
        return ResponseEntity.ok(new GenericResponse("account.setting.main.forms.password.change.success"));
    }

    record GenericResponse(String message) {
    }

    record PasswordChangeRequest(String oldPassword, String newPassword, String repeatPassword) {
    }

    record AccountSettings(String username, boolean canUserChangePassword, String passwordRegex) {
    }

    record MenuElement(String key, String name, List<String> resources) {
    }
}
