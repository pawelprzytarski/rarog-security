package eu.rarogsoftware.rarog.platform.app.servlets;

import eu.rarogsoftware.rarog.platform.api.i18n.I18nHelper;
import eu.rarogsoftware.rarog.platform.api.security.AdminOnly;
import eu.rarogsoftware.rarog.platform.api.user.management.RarogUser;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AdminOnly
public class AdminHomeController {
    private final I18nHelper i18nHelper;

    public AdminHomeController(I18nHelper i18nHelper) {
        this.i18nHelper = i18nHelper;
    }

    @GetMapping(value = {"admin/**", "admin"})
    public String displaySecondFactory(Model model, @AuthenticationPrincipal RarogUser user) {
        model.addAttribute("title", i18nHelper.getText("core-admin:admin.page.title"));
        model.addAttribute("reactEntrypointName", "admin");
        model.addAttribute("theme", "theme/adminpage.html");
        model.addAttribute("loggedUserName", user.getUsername());
        model.addAttribute("loggedUserAvatar", null);//to do add user avatars support
        return "reactapp";
    }
}
