package eu.rarogsoftware.rarog.platform.app.rest.plugins;

import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginException;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginManager;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginManifest;
import eu.rarogsoftware.rarog.platform.api.security.AdminOnly;
import eu.rarogsoftware.rarog.platform.app.rest.commons.BooleanValueBean;
import eu.rarogsoftware.rarog.platform.core.plugins.osgi.JarPluginArtifact;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RequestMapping(value = "/plugins", produces = "application/json")
@RestController
@AdminOnly
@ApiResponse(responseCode = "403", description = "User attempting to access resource has not admin privileges")
@Tag(name = "Plugin control")
public class PluginController {
    private final Logger logger = LoggerFactory.getLogger(PluginController.class);
    private final PluginManager pluginManager;
    private final ResourceLoader resourceLoader;

    public PluginController(PluginManager pluginManager) {
        this.pluginManager = pluginManager;
        this.resourceLoader = new FileSystemResourceLoader();
    }

    @GetMapping
    @Operation(summary = "Get list of installed plugins",
            description = "Returns list of installed plugins together with their current state and manifest")
    @ApiResponse(responseCode = "200")
    public List<PluginRecord> getInstalledPlugins() {
        return pluginManager.listPlugins().stream()
                .map(plugin -> new PluginRecord(plugin.getKey(), plugin.getManifest(), plugin.getState()))
                .toList();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Install plugin from specified path",
            description = "Installs plugin from provided path, and uninstall plugin with the same key if specified")
    @ApiResponse(responseCode = "204", description = "Successfully installed plugin")
    @ApiResponse(responseCode = "400", description = "Provided path is invalid, provided file is not valid Rarog plugin "
            + "or plugin is already installed and reinstall was turned off")
    public ResponseEntity<Void> installPluginFromPath(@RequestBody PathInstallInstructionBean instructionBean) {
        if (StringUtils.isEmpty(instructionBean.path())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "path must be valid file path");
        }
        var resource = resourceLoader.getResource("file://" + instructionBean.path());
        if (!resource.exists() || !resource.isFile() || !resource.isReadable()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "path must be valid path to existing readable jar file");
        }
        installPlugin(resource, instructionBean.reinstallIfNeeded);
        return ResponseEntity.noContent().build();
    }

    private void installPlugin(Resource resource, boolean reinstallIfNeeded) {
        var artifactId = "unknown";
        try (var temporaryArtifact = getJarPluginArtifact(resource)) {
            artifactId = temporaryArtifact.getManifest().key();
            var existingPlugin = pluginManager.getPlugin(artifactId);
            if (existingPlugin != null) {
                if (!reinstallIfNeeded) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Plugin with key %s is already installed".formatted(existingPlugin.getKey()));
                } else {
                    if (existingPlugin.isInUse()) {
                        pluginManager.disablePlugin(existingPlugin);
                    }
                    pluginManager.uninstall(existingPlugin);
                }
            }
            var savedArtifact = pluginManager.savePluginArtifactToHome(temporaryArtifact);
            var newPlugin = pluginManager.installPlugin(savedArtifact, true);
            pluginManager.enablePlugin(newPlugin);
        } catch (PluginException | IOException e) {
            logger.error("Failed to install plugin {}. Reinstall? {}", artifactId, reinstallIfNeeded, e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to (re)install plugin. Refer to logs for more details");
        }
    }

    private JarPluginArtifact getJarPluginArtifact(Resource resource) throws PluginException, IOException {
        var pluginArtifact = new JarPluginArtifact(resource);
        pluginManager.validatePluginArtifact(pluginArtifact);
        return pluginArtifact;
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Install plugin",
            description = "Installs plugin send with form")
    @ApiResponse(responseCode = "204", description = "Successfully installed plugin")
    @Parameter(name = "file", description = "plugin binary to install", required = true)
    @Parameter(name = "reinstallIfNeeded", description = "if true and plugin already installed, plugin will be reinstalled, otherwise reinstall will not be attempted")
    @ApiResponse(responseCode = "400", description = "Provided is not valid Rarog plugin "
            + "or plugin is already installed and reinstall was turned off")
    public ResponseEntity<Void> installPluginFromForm(@RequestParam("file") MultipartFile multipartFile, @RequestParam("reinstallIfNeeded") Boolean reinstallIfNeeded) {
        try {
            installPlugin(new ByteArrayResource(multipartFile.getBytes(), multipartFile.getOriginalFilename()), reinstallIfNeeded);
        } catch (IOException e) {
            logger.error("Failed to read file from request", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to read file from request");
        }
        return ResponseEntity.noContent().build();
    }

    @PostMapping(consumes = {MediaType.APPLICATION_OCTET_STREAM_VALUE, "application/java-archive"})
    @Operation(summary = "Install plugin",
            description = "Installs plugin send as as reqeust body. Reinstall plugin if already installed.")
    @ApiResponse(responseCode = "204", description = "Successfully installed plugin")
    @ApiResponse(responseCode = "400", description = "Provided is not valid Rarog plugin")
    public ResponseEntity<Void> installPluginFromFile(HttpServletRequest request) {
        try (var requestStream = request.getInputStream();
             var bytesStream = new ByteArrayOutputStream(request.getContentLength())) {
            IOUtils.copy(requestStream, bytesStream);
            installPlugin(new ByteArrayResource(bytesStream.toByteArray(), UUID.randomUUID().toString()), true);
        } catch (IOException e) {
            logger.error("Failed to read file from request", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to read file from request");
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{pluginKey}")
    @Operation(summary = "Get plugin info", description = "Returns single plugin name, manifest and state")
    @Parameter(name = "pluginKey", required = true, in = ParameterIn.PATH)
    @ApiResponse(responseCode = "200", description = "Plugin exists and it's info is returned")
    @ApiResponse(responseCode = "400", description = "Plugin key is empty or incorrect")
    @ApiResponse(responseCode = "404", description = "Plugin does not exist")
    public PluginRecord getPlugin(@PathVariable("pluginKey") String pluginKey) {
        var plugin = validatePluginKeyAndGetPlugin(pluginKey);
        return new PluginRecord(plugin.getKey(), plugin.getManifest(), plugin.getState());
    }

    @DeleteMapping("/{pluginKey}")
    @Operation(summary = "Uninstall plugin", description = "Uninstalls specified plugin")
    @Parameter(name = "pluginKey", required = true, in = ParameterIn.PATH)
    @ApiResponse(responseCode = "204", description = "Plugin successfully uninstalled")
    @ApiResponse(responseCode = "400", description = "Plugin key is empty or incorrect")
    @ApiResponse(responseCode = "404", description = "Plugin does not exist")
    public ResponseEntity<Void> removePlugin(@PathVariable("pluginKey") String pluginKey) {
        Plugin plugin = validatePluginKeyAndGetPlugin(pluginKey);
        try {
            if (plugin.isInUse()) {
                pluginManager.disablePlugin(plugin);
            }
            pluginManager.uninstall(plugin);
        } catch (PluginException e) {
            logger.error("Failed to uninstall plugin {}", pluginKey, e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to uninstall plugin. Refer to logs for more details");
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{pluginKey}/enabled")
    @Operation(summary = "Get plugin state", description = "Returns if plugin is installed and enabled, otherwise false")
    @Parameter(name = "pluginKey", required = true, in = ParameterIn.PATH)
    @ApiResponse(responseCode = "200", description = "Plugin is installed and it's state is returned")
    @ApiResponse(responseCode = "400", description = "Plugin key is empty or incorrect")
    @ApiResponse(responseCode = "404", description = "Plugin does not exist")
    public BooleanValueBean isPluginEnabled(@PathVariable("pluginKey") String pluginKey) {
        Plugin plugin = validatePluginKeyAndGetPlugin(pluginKey);
        return new BooleanValueBean(plugin.isInUse());
    }

    private Plugin validatePluginKeyAndGetPlugin(String pluginKey) {
        if (StringUtils.isEmpty(pluginKey)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "pluginKey must be not empty string");
        }
        var plugin = pluginManager.getPlugin(pluginKey);
        if (plugin == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Plugin does not exist");
        }
        return plugin;
    }

    @PutMapping("/{pluginKey}/enabled")
    @Operation(summary = "Change plugin state", description = "Changes plugin state by enabling or disabling it. "
            + "If value is true plugin will be enabled, if value is false plugin will be disabled")
    @Parameter(name = "pluginKey", required = true, in = ParameterIn.PATH)
    @ApiResponse(responseCode = "204", description = "Plugin state successfully changed")
    @ApiResponse(responseCode = "400", description = "Plugin key is empty or incorrect")
    @ApiResponse(responseCode = "404", description = "Plugin does not exist")
    public ResponseEntity<Void> changePluginState(@PathVariable("pluginKey") String pluginKey, @RequestBody BooleanValueBean value) {
        Plugin plugin = validatePluginKeyAndGetPlugin(pluginKey);
        try {
            if (value.value() && !plugin.isInUse()) {
                pluginManager.enablePlugin(plugin);
            } else if (!value.value() && plugin.isInUse()) {
                pluginManager.disablePlugin(plugin);
            }
        } catch (PluginException e) {
            logger.error("Failed to change state of plugin {}. Attempted enabling: {}", plugin.getKey(), value.value(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to change state of plugin. Refer to logs for more details");
        }
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{pluginKey}/purge")
    @Operation(summary = "Purge plugin", description = "Removes all plugin data stored in system and disables it")
    @Parameter(name = "pluginKey", required = true, in = ParameterIn.PATH)
    @ApiResponse(responseCode = "204", description = "Plugin state successfully changed")
    @ApiResponse(responseCode = "400", description = "Plugin key is empty or incorrect")
    @ApiResponse(responseCode = "404", description = "Plugin does not exist")
    public ResponseEntity<Void> purgePlugin(@PathVariable("pluginKey") String pluginKey) {
        var plugin = validatePluginKeyAndGetPlugin(pluginKey);
        try {
            pluginManager.purgePlugin(plugin);
        } catch (PluginException e) {
            logger.error("Failed to purge plugin {}.", pluginKey, e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to purge plugin. Refer to logs for more details");
        }
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<Map<String, Object>> handleResponseStatusException(ResponseStatusException exception) {
        return ResponseEntity.status(exception.getStatusCode()).body(Map.of(
                "timestamp", Instant.now().getEpochSecond(),
                "status", exception.getStatusCode().value(),
                "error", exception.getStatusCode().toString(),
                "reason", exception.getReason() == null ? "Unknown" : exception.getReason()
        ));
    }

    @Schema(name = "Path install instruction")
    public record PathInstallInstructionBean(String path, Boolean reinstallIfNeeded) {
    }

    @Schema(name = "Plugin info")
    public record PluginRecord(String key, PluginManifest manifest, Plugin.PluginState state) {
    }
}
