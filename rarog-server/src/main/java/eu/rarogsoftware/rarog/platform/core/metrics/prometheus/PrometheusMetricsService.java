package eu.rarogsoftware.rarog.platform.core.metrics.prometheus;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.metrics.*;
import io.prometheus.client.Collector;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.SimpleCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Component
public class PrometheusMetricsService implements MetricsService {
    private static final Long DEFAULT_PERIOD = 1L;
    private final Logger logger = LoggerFactory.getLogger(PrometheusMetricsService.class);
    private final CollectorRegistry registry = new CollectorRegistry(true);
    private final Clock clock;
    private final List<MetricSamples> closedMetrics = new ArrayList<>();
    private final Map<String, MetricSamples> oldMetrics = new ConcurrentHashMap<>();

    public PrometheusMetricsService(Clock clock) {
        this.clock = clock;
    }

    public PrometheusMetricsService() {
        this(Clock.systemUTC());
    }

    @Override
    public Counter createCounter(MetricSettings settings) {
        logger.trace("Creating counter {}", settings.name());
        return new PrometheusCounter(
                setupSettingsAndClean(io.prometheus.client.Counter.build(), settings)
                        .withoutExemplars()
                        .register(registry));
    }

    private <B extends SimpleCollector.Builder<B, C>, C extends SimpleCollector, T extends BaseMetricSettings<T>> B setupSettingsAndClean(B metric, T settings) {
        oldMetrics.remove(settings.name());
        return metric
                .name(settings.name())
                .help(settings.description())
                .labelNames(settings.labels())
                .unit(settings.unit());
    }

    @Override
    public Gauge createGauge(MetricSettings settings) {
        logger.trace("Creating gauge {}", settings.name());
        return new PrometheusGauge(
                setupSettingsAndClean(io.prometheus.client.Gauge.build(), settings)
                        .register(registry));
    }

    @Override
    public Histogram createHistogram(HistogramSettings settings) {
        logger.trace("Creating histogram {}", settings.name());
        var builder = setupSettingsAndClean(io.prometheus.client.Histogram.build(), settings);
        if (settings.buckets() != null) {
            builder.buckets(settings.buckets());
        }
        return new PrometheusHistogram(builder.withoutExemplars()
                .register(registry));
    }

    @Override
    public Info createInfo(MetricSettings settings) {
        logger.trace("Creating info {}", settings.name());
        return new PrometheusInfo(setupSettingsAndClean(io.prometheus.client.Info.build(), settings).register(registry));
    }

    @Override
    public void closeMetric(BasicMetric metric) {
        if (metric instanceof PrometheusMetric prometheusMetric) {
            var samplesCounter = new AtomicLong(0);
            var samples = getMetricSamples(samplesCounter, prometheusMetric.collect().stream());
            logger.debug("Closed metric {} as {} metrics with {} samples", metric.getClass(), samples.size(), samplesCounter.get());
            synchronized (closedMetrics) {
                closedMetrics.addAll(samples);
            }
            closedMetrics.forEach(metricSamples -> oldMetrics.put(metricSamples.settings().name(), metricSamples));
            registry.unregister(prometheusMetric.getCollector());
        } else {
            logger.error("Provided metric {} is not correct prometheus adapter", metric.getClass());
            throw new IllegalArgumentException("Provided metric %s is not correct prometheus adapter".formatted(metric.getClass()));
        }
    }

    @Override
    public MetricsSnapshot snapshot() {
        long timestamp = clock.millis();
        logger.trace("Creating snapshot {}", timestamp);
        var samples = new ArrayList<MetricSamples>();
        synchronized (closedMetrics) {
            samples.addAll(closedMetrics);
            closedMetrics.clear();
        }
        samples.addAll(getMetricSamplesFromRegistry());
        return new MetricsSnapshot(samples, timestamp);
    }

    private List<MetricSamples> getMetricSamplesFromRegistry() {
        var samplesCounter = new AtomicLong(0);
        var samples = getMetricSamples(samplesCounter, StreamSupport.stream(Spliterators.spliteratorUnknownSize(
                registry.metricFamilySamples().asIterator(),
                Spliterator.ORDERED
        ), false));
        logger.trace("Collected {} metrics with {} samples", samples.size(), samplesCounter.get());
        return samples;
    }

    private List<MetricSamples> getMetricSamples(AtomicLong samplesCounter, Stream<Collector.MetricFamilySamples> stream) {
        return stream
                .map(familySamples -> {
                    var settings = MetricSettings.settings()
                            .name(familySamples.name)
                            .description(familySamples.help)
                            .unit(familySamples.unit);
                    var type = convertType(familySamples.type);
                    var innerSamples = familySamples.samples.stream()
                            .map(sample -> new Sample(sample.name, sample.labelNames, sample.labelValues, sample.value))
                            .toList();
                    samplesCounter.addAndGet(innerSamples.size());
                    return new MetricSamples(settings, type, innerSamples);
                })
                .toList();
    }

    private MetricSamples.Type convertType(Collector.Type type) {
        return switch (type) {
            case UNKNOWN -> MetricSamples.Type.UNKNOWN;
            case COUNTER -> MetricSamples.Type.COUNTER;
            case GAUGE -> MetricSamples.Type.GAUGE;
            case STATE_SET -> MetricSamples.Type.STATE_SET;
            case INFO -> MetricSamples.Type.INFO;
            case HISTOGRAM -> MetricSamples.Type.HISTOGRAM;
            case GAUGE_HISTOGRAM -> MetricSamples.Type.GAUGE_HISTOGRAM;
            case SUMMARY -> MetricSamples.Type.SUMMARY;
        };
    }

    @Override
    public Collection<MetricSamples> sample() {
        var metricSamples = new ArrayList<>(getMetricSamplesFromRegistry());
        metricSamples.addAll(oldMetrics.values());
        return metricSamples;
    }

    public void resetMetrics() {
        synchronized (closedMetrics) {
            closedMetrics.clear();
        }
        oldMetrics.clear();
        registry.clear();
    }
}
