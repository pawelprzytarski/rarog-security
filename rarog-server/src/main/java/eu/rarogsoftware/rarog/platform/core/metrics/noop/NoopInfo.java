package eu.rarogsoftware.rarog.platform.core.metrics.noop;

import eu.rarogsoftware.rarog.platform.api.metrics.Info;

import java.util.Map;

public class NoopInfo implements Info {
    @Override
    public Info labels(String... labels) {
        return this;
    }

    @Override
    public void info(Map<String, String> info) {
        // do nothing
    }
}
