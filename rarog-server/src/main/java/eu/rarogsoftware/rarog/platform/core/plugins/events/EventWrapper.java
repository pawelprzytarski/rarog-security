package eu.rarogsoftware.rarog.platform.core.plugins.events;

import eu.rarogsoftware.rarog.platform.api.plugins.events.EventService;

import java.time.Instant;

public record EventWrapper(Object event, Instant publishedTime, EventService.EventType eventType) {
}
