package eu.rarogsoftware.rarog.platform.core.settings;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SettingSerializer;
import org.springframework.core.serializer.support.SerializationFailedException;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Default implementation of {@link ApplicationSettings}.
 */
@ExportComponent
@Component
public class DefaultApplicationSettings implements ApplicationSettings {
    private final SettingsStore settingsStore;
    private final Map<Class<?>, SettingSerializer<?>> serializers = new ConcurrentHashMap<>();
    private final Map<String, String> defaults = new ConcurrentHashMap<>();

    public DefaultApplicationSettings(SettingsStore settingsStore) {
        this.settingsStore = settingsStore;
        initSerializers();
    }

    private void initSerializers() {
        serializers.put(Integer.class, Integer::parseInt);
        serializers.put(Double.class, Double::parseDouble);
        serializers.put(Boolean.class, Boolean::parseBoolean);
        serializers.put(String.class, serialized -> serialized);
    }

    @Override
    public <T> Optional<T> getSetting(String key, Class<T> type) {
        var stringValue = settingsStore.getSetting(key);
        return Optional.ofNullable(deserializeString(stringValue, type));
    }

    @Override
    public Optional<String> getSettingAsString(String key) {
        return getSetting(key, String.class);
    }

    private <T> T deserializeString(String stringValue, Class<T> type) {
        if (stringValue == null) {
            return null;
        }
        JsonSettingsSerializer<T> defaultSerializer = JsonSettingsSerializer.getInstance(type);
        SettingSerializer<T> serializer = (SettingSerializer<T>) serializers.getOrDefault(type, defaultSerializer);
        return serializer.deserialize(stringValue);
    }

    @Override
    public <T> T getSettingOrDefault(String key, Class<T> type) {
        return getSetting(key, type)
                .orElseGet(() -> deserializeString(defaults.get(key), type));
    }

    @Override
    public boolean isTrue(String key) {
        return Boolean.TRUE.equals(getSettingOrDefault(key, Boolean.class));
    }

    @Override
    public String getSettingOrDefaultAsString(String key) {
        return getSettingOrDefault(key, String.class);
    }

    @Override
    public <T> T getPropertyBackedSetting(String key, Class<T> type) {
        return Optional.ofNullable(System.getProperty(key))
                .map(value -> deserializeString(value, type))
                .orElseGet(() -> getSettingOrDefault(key, type));
    }

    @Override
    public String getPropertyBackedSettingAsString(String key) {
        return getPropertyBackedSetting(key, String.class);
    }

    @Override
    public <T> void setSetting(String key, T value) {
        settingsStore.setSetting(key, serializeValue(value));
    }

    private <T> String serializeValue(T value) {
        if (value == null) {
            return null;
        }
        JsonSettingsSerializer<T> defaultSerializer = JsonSettingsSerializer.getInstance((Class<T>) value.getClass());
        SettingSerializer<T> serializer = (SettingSerializer<T>) serializers.getOrDefault(value.getClass(), defaultSerializer);
        return serializer.serialize(value);
    }

    @Override
    public <T> void setDefaultSetting(String key, T defaultValue) {
        defaults.put(key, serializeValue(defaultValue));
    }

    @Override
    public <T> void addTypeSerializer(Class<T> type, SettingSerializer<T> serializer) {
        serializers.put(type, serializer);
    }

    static class JsonSettingsSerializer<T> implements SettingSerializer<T> {
        private static final ObjectMapper mapper = new ObjectMapper();
        private final Class<T> deserializeClass;

        JsonSettingsSerializer(Class<T> deserializeClass) {
            this.deserializeClass = deserializeClass;
        }

        public static <U> JsonSettingsSerializer<U> getInstance(Class<U> type) {
            return new JsonSettingsSerializer<>(type);
        }

        @Override
        public T deserialize(String serialized) {
            try {
                return mapper.readValue(serialized, deserializeClass);
            } catch (JsonProcessingException e) {
                throw new SerializationFailedException("Failed to deserialize {" + serialized + "} as" + deserializeClass, e);
            }
        }

        @Override
        public String serialize(T serializable) {
            try {
                return mapper.writeValueAsString(serializable);
            } catch (JsonProcessingException e) {
                throw new SerializationFailedException("Failed to serialize " + serializable.getClass().toString() + " {" + serializable + "}", e);
            }
        }
    }
}
