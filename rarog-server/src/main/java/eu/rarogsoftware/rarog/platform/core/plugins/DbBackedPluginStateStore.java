package eu.rarogsoftware.rarog.platform.core.plugins;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.querydsl.sql.SQLQueryFactory;
import eu.rarogsoftware.commons.database.connection.DatabaseConnectionProvider;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginsStateStore;
import eu.rarogsoftware.rarog.platform.db.QPluginsState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Optional;

import static eu.rarogsoftware.commons.database.connection.DatabaseConnectionProvider.TransactionLevel.READ_COMMITTED;

/**
 * Implementation of {@link PluginsStateStore} that relies on database to store plugin states.
 */
@Component
public class DbBackedPluginStateStore implements PluginsStateStore {
    private final Logger logger = LoggerFactory.getLogger(DbBackedPluginStateStore.class);
    private static final String PLUGINS_STATE_CACHE = DbBackedPluginStateStore.class.getName() + "_cache";
    private static final Duration PLUGINS_STATE_CACHE_LIFETIME = Duration.ofMinutes(10);
    private static final QPluginsState PLUGINS_STATE = QPluginsState.pluginsState;

    private final DatabaseConnectionProvider dbConnectionProvider;

    // This cache should not ever be replicated.
    private final Cache<String, String> pluginsStateCache;

    public DbBackedPluginStateStore(DatabaseConnectionProvider databaseConnectionProvider) {
        this.dbConnectionProvider = databaseConnectionProvider;
        this.pluginsStateCache = Caffeine.newBuilder()
                .expireAfterAccess(PLUGINS_STATE_CACHE_LIFETIME)
                .build();
    }

    @Override
    public Optional<Plugin.PluginState> getPluginState(Plugin plugin) {
        logger.info("Getting for plugin {}", plugin.getKey());
        var key = plugin.getKey();
        if (key == null) {
            throw new IllegalArgumentException("Plugin key cannot be null.");
        }
        var savedState = pluginsStateCache.get(key, this::loadState);
        logger.info("Saved {} for plugin {}", savedState, plugin);
        return savedState == null ? Optional.empty() : Optional.of(Plugin.PluginState.valueOf(savedState));
    }

    @Override
    public void storePluginState(Plugin plugin, Plugin.PluginState pluginState) {
        logger.info("Storing {} for plugin {}", pluginState, plugin.getKey());
        var key = plugin.getKey();
        var value = pluginState.name();
        if (key == null) {
            throw new IllegalArgumentException("Plugin key cannot be null.");
        }
        var data = dbConnectionProvider.getConnection(query -> query.select(PLUGINS_STATE.pluginKey, PLUGINS_STATE.pluginState)
                .from(PLUGINS_STATE)
                .fetchResults()).getResults();
        logger.info("Before update data {}", data);
        pluginsStateCache.invalidate(plugin.getKey());
        long connectionInTransaction = dbConnectionProvider.getConnectionInTransaction(READ_COMMITTED,
                query -> {
                    var updatedEntries = updateExistingState(query, key, value);
                    if (updatedEntries == 0) {
                        return insertNewPluginState(key, value, query);
                    }
                    return updatedEntries;
                });

        data = dbConnectionProvider.getConnection(query -> query.select(PLUGINS_STATE.pluginKey, PLUGINS_STATE.pluginState)
                .from(PLUGINS_STATE)
                .fetchResults()).getResults();
        logger.info("After update data {}", data);
        logger.info("Updated entries {}", connectionInTransaction);
    }

    private Long insertNewPluginState(String key, String value, SQLQueryFactory query) {
        return query.insert(PLUGINS_STATE)
                .set(PLUGINS_STATE.pluginKey, key)
                .set(PLUGINS_STATE.pluginState, value)
                .executeWithKey(PLUGINS_STATE.id);
    }

    private long updateExistingState(SQLQueryFactory query, String key, String value) {
        return query.update(PLUGINS_STATE)
                .set(PLUGINS_STATE.pluginState, value)
                .where(PLUGINS_STATE.pluginKey.eq(key))
                .execute();
    }

    public String loadState(String pluginKey) {
        var data = dbConnectionProvider.getConnection(query -> query.select(PLUGINS_STATE.pluginKey, PLUGINS_STATE.pluginState)
                .from(PLUGINS_STATE)
                .fetchResults()).getResults();
        logger.info("Stored data {}", data);
        return dbConnectionProvider.getConnection(query -> query.select(PLUGINS_STATE.pluginState)
                .from(PLUGINS_STATE)
                .where(PLUGINS_STATE.pluginKey.eq(pluginKey))
                .fetchOne());
    }
}
