package eu.rarogsoftware.rarog.platform.core.plugins.spring;

import eu.rarogsoftware.rarog.platform.api.plugins.PluginsService;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class SpringIocPluginsService implements PluginsService {
    private final ApplicationContext applicationContext;

    public SpringIocPluginsService(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public <T> Collection<? extends T> getInstancesOf(Class<? extends T> interfaceClass) {
        return applicationContext.getBeansOfType(interfaceClass).values();
    }
}
