package eu.rarogsoftware.rarog.platform.dynamic.frontend.resources;

public final class RenderContextHolder {
    private static final ThreadLocal<RenderContext> renderContext = new ThreadLocal<>();

    private RenderContextHolder() {
    }

    public static void initContext() {
        renderContext.set(new RenderContext());
    }

    public static void initContext(RenderContext context) {
        renderContext.set(context);
    }

    public static void clearContext() {
        renderContext.remove();
    }

    public static RenderContext getRenderContext() {
        return renderContext.get();
    }

    public static void runInContext(RenderContext context, ContextRunnable runnable) throws Exception {
        var previousContext = renderContext.get();
        try {
            initContext(context);
            runnable.run();
        } finally {
            if (previousContext == null) {
                clearContext();
            } else {
                initContext(previousContext);
            }
        }
    }

    public static void runInNewContext(ContextRunnable runnable) throws Exception {
        runInContext(new RenderContext(), runnable);
    }

    public interface ContextRunnable {
        void run() throws Exception;
    }
}
