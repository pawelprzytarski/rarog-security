package eu.rarogsoftware.rarog.platform.core.metrics.noop;

import eu.rarogsoftware.rarog.platform.api.metrics.Histogram;
import org.apache.commons.lang3.time.StopWatch;

import java.io.IOException;
import java.util.concurrent.Callable;

public class NoopHistogram implements Histogram {
    @Override
    public double measureExecutionTime(Runnable timeable) {
        var timer = startTimer();
        timeable.run();
        return timer.observeDuration();
    }

    @Override
    public <T> T measureExecutionTime(Callable<T> timeable) throws Exception {
        return timeable.call();
    }

    @Override
    public void observe(double amt) {
        // do nothing
    }

    @Override
    public Timer startTimer() {
        var stopWatch = new StopWatch();
        stopWatch.start();
        return new Timer() {
            @Override
            public double observeDuration() {
                return stopWatch.getTime()/1000.0;
            }

            @Override
            public void close() throws IOException {
                stopWatch.stop();
            }
        };
    }

    @Override
    public Histogram labels(String... labels) {
        return this;
    }
}
