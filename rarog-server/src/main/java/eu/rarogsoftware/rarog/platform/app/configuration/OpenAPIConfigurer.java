package eu.rarogsoftware.rarog.platform.app.configuration;

import io.swagger.v3.oas.models.OpenAPI;

public interface OpenAPIConfigurer {
    OpenAPI configure(OpenAPI openAPI);
}
