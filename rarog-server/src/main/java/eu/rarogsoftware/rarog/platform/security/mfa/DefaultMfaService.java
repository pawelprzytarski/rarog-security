package eu.rarogsoftware.rarog.platform.security.mfa;

import eu.rarogsoftware.rarog.platform.api.plugins.PluginsService;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaMethod;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaMethodsRegister;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaService;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SettingsInitializer;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class DefaultMfaService implements MfaService, SettingsInitializer {
    private static final String MFA_ENABLED_SETTING_NAME = "MFA_ENABLED";
    private final ApplicationSettings applicationSettings;
    private final PluginsService pluginsService;

    public DefaultMfaService(ApplicationSettings applicationSettings,
                             PluginsService pluginsService) {
        this.applicationSettings = applicationSettings;
        this.pluginsService = pluginsService;
    }

    @Override
    public boolean isMfaEnabled() {
        return applicationSettings.getSettingOrDefault(MFA_ENABLED_SETTING_NAME, Boolean.class);
    }

    @Override
    public boolean isMfaForced() {
        return false;
    }

    @Override
    public boolean isMfaEnabledForUser(StandardUser user) {
        return isMfaEnabled() && (user.isMfaEnabled() || isMfaForced());
    }

    @Override
    public boolean isMfaConfiguredForUser(StandardUser user) {
        return true;
    }

    @Override
    public Collection<MfaMethod> getMethods() {
        return pluginsService.getInstancesOf(MfaMethodsRegister.class).stream()
                .flatMap(register -> register.getMfaMethods().stream())
                .toList();
    }

    @Override
    public Collection<MfaMethod> getMethodsForUser(StandardUser standardUser) {
        return pluginsService.getInstancesOf(MfaMethodsRegister.class).stream()
                .flatMap(register -> register.getMfaMethods().stream())
                .filter(mfaMethod -> mfaMethod.condition().test(standardUser))
                .toList();
    }

    @Override
    public void initialize(ApplicationSettings settings) {
        settings.setDefaultSetting(MFA_ENABLED_SETTING_NAME, true);
    }
}
