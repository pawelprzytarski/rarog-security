package eu.rarogsoftware.rarog.platform.app.configuration;

import eu.rarogsoftware.rarog.platform.core.plugins.web.PluginMappingHandlerMapping;
import io.swagger.v3.oas.models.OpenAPI;
import org.springdoc.core.fn.RouterOperation;
import org.springdoc.core.providers.RepositoryRestResourceProvider;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.AbstractHandlerMethodMapping;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Component
class PluginAwareRepositoryRestResourceProvider implements RepositoryRestResourceProvider {
    //TODO RS-74 add caching
    private final PluginMappingHandlerMapping pluginMappingHandlerMapping;

    public PluginAwareRepositoryRestResourceProvider(PluginMappingHandlerMapping pluginMappingHandlerMapping) {
        this.pluginMappingHandlerMapping = pluginMappingHandlerMapping;
    }

    @Override
    public List<RouterOperation> getRouterOperations(OpenAPI openAPI, Locale locale) {
        return Collections.emptyList();
    }

    @Override
    public Map<String, Object> getBasePathAwareControllerEndpoints() {
        return pluginMappingHandlerMapping.getHandlerMappings().entrySet().stream()
                .map(Map.Entry::getValue)
                .filter(handler -> handler instanceof AbstractHandlerMethodMapping<?>)
                .map(handler -> (AbstractHandlerMethodMapping<RequestMappingInfo>) handler)
                .flatMap(handler -> handler.getHandlerMethods().entrySet().stream())
                .map(entry -> entry.getValue().createWithResolvedBean().getBean())
                .distinct()
                .collect(Collectors.toMap(bean -> bean.getClass().getName(), bean -> bean));
    }

    @Override
    public Map getHandlerMethods() {
        return pluginMappingHandlerMapping.getHandlerMappings().entrySet().stream()
                .map(Map.Entry::getValue)
                .filter(handler -> handler instanceof AbstractHandlerMethodMapping<?>)
                .map(handler -> (AbstractHandlerMethodMapping<RequestMappingInfo>) handler)
                .flatMap(handler -> handler.getHandlerMethods().entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Override
    public void customize(OpenAPI openAPI) {
        // do nothing here
    }
}
