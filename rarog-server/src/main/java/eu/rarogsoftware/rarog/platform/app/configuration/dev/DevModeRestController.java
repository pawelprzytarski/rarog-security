package eu.rarogsoftware.rarog.platform.app.configuration.dev;


import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Indexed;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Indexed
@ConditionalOnProperty("devMode")
@RestController
public @interface DevModeRestController {
}
