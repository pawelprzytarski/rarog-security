package eu.rarogsoftware.rarog.platform.core.settings;

public interface SettingsStore {
    String getSetting(String key);

    void setSetting(String key, String value);
}
