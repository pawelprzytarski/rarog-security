package eu.rarogsoftware.rarog.platform.core.metrics.prometheus;

import io.prometheus.client.Collector;

import java.util.Collection;

public abstract class PrometheusMetric {
    private final Collector collector;

    protected PrometheusMetric(Collector collector) {
        this.collector = collector;
    }

    Collection<Collector.MetricFamilySamples> collect() {
        return collector.collect();
    }

    Collector getCollector() {
        return collector;
    }
}
