package eu.rarogsoftware.rarog.platform.security.mfa;

import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaStatus;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaStore;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import java.time.Instant;
import java.util.Optional;

@ExportComponent
@Component("DefaultMfaStore")
public class SessionMfaStore implements MfaStore {
    public static final String MFA_FILTER_AUTHENTICATION_STATUS = SessionMfaStore.class.getName() + ".mfa.authentication.status";
    public static final String MFA_FILTER_AUTHENTICATION_TIMESTAMP = SessionMfaStore.class.getName() + ".mfa.authentication.timestamp";

    @Override
    public Optional<MfaStatus> getStatus(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        Object statusObject = session == null ? null : session.getAttribute(MFA_FILTER_AUTHENTICATION_STATUS);
        if (statusObject instanceof MfaStatus) {
            return Optional.of((MfaStatus) statusObject);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void authorize(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute(MFA_FILTER_AUTHENTICATION_STATUS, MfaStatus.MFA_PASSED);
        session.setAttribute(MFA_FILTER_AUTHENTICATION_TIMESTAMP, Instant.now().getEpochSecond());
    }

    @Override
    public void saveStatus(HttpServletRequest request, MfaStatus status) {
        HttpSession session = request.getSession();
        session.setAttribute(MFA_FILTER_AUTHENTICATION_STATUS, status);
    }
}
