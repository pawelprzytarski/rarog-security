package eu.rarogsoftware.rarog.platform.core.plugins;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;

import java.util.function.Supplier;

public record FeatureModuleDescriptor<T extends FeatureDescriptor>(Class<T> descriptorType,
                                                                   Supplier<FeatureModule<T>> featureModuleSupplier) implements FeatureDescriptor {
    FeatureModule<T> getModule() {
        return featureModuleSupplier.get();
    }
}
