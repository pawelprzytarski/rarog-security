package eu.rarogsoftware.rarog.platform.core.plugins.events;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventListenersDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventService;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModuleComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@AutoRegisterFeatureModuleComponent(EventListenersDescriptor.class)
public class EventListenersFeatureModule implements FeatureModule<EventListenersDescriptor> {
    private final Logger logger = LoggerFactory.getLogger(EventListenersFeatureModule.class);
    private final EventService eventService;

    public EventListenersFeatureModule(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void plugDescriptor(Plugin plugin, EventListenersDescriptor descriptor) {
        logger.debug("Registering Event Listeners from {}", plugin.getKey());
        descriptor.listenerBeans().forEach(eventService::registerListener);
    }

    @Override
    public void unplugDescriptor(Plugin plugin, EventListenersDescriptor descriptor) {
        logger.debug("Removing Event Listeners from {}", plugin.getKey());
        descriptor.listenerBeans().forEach(eventService::unregisterListener);
    }
}
