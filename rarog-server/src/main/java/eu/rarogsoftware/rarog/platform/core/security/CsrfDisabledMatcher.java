package eu.rarogsoftware.rarog.platform.core.security;

import eu.rarogsoftware.rarog.platform.api.security.CsrfDisabled;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;

import java.util.List;

@Component
public class CsrfDisabledMatcher extends AbstractMethodAnnotatedRequestMatcher {
    public CsrfDisabledMatcher(List<HandlerMapping> handlerMappings) {
        super(handlerMappings, CsrfDisabled.class);
    }
}
