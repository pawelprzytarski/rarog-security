package eu.rarogsoftware.rarog.platform.security.mfa;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;

import java.util.List;

@Component
public class MfaPageMatcherFactory {
    private final List<HandlerMapping> handlerMappings;

    public MfaPageMatcherFactory(List<HandlerMapping> handlerMappings) {
        this.handlerMappings = handlerMappings;
    }

    public MfaPageMatcher createPageMatcher(boolean registerOnly) {
        return new MfaPageMatcher(handlerMappings, registerOnly);
    }
}
