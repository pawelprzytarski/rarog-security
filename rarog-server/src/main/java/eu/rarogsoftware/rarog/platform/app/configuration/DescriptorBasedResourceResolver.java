package eu.rarogsoftware.rarog.platform.app.configuration;

import eu.rarogsoftware.rarog.platform.api.plugins.web.ResourceResolverDescriptor;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModuleComponent;
import eu.rarogsoftware.rarog.platform.core.plugins.templates.AbstractSimpleFeatureModule;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.resource.ResourceResolver;
import org.springframework.web.servlet.resource.ResourceResolverChain;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;

@AutoRegisterFeatureModuleComponent(ResourceResolverDescriptor.class)
public class DescriptorBasedResourceResolver extends AbstractSimpleFeatureModule<ResourceResolverDescriptor> implements ResourceResolver {
    private static final String PLUGIN_PREFIX = "p/";

    @Override
    public Resource resolveResource(HttpServletRequest request, String requestPath, List<? extends Resource> locations, ResourceResolverChain chain) {
        var urlResource = resolveResourceInternal(request, requestPath, locations, chain);
        if (urlResource == null) {
            return chain.resolveResource(request, requestPath, locations);
        }
        return urlResource;
    }

    private Resource resolveResourceInternal(HttpServletRequest request, String requestPath, List<? extends Resource> locations, ResourceResolverChain chain) {
        var descriptor = getDescriptor(requestPath);
        if (descriptor == null) {
            return null;
        }
        return descriptor.resourceResolver().resolveResource(request, requestPath, locations, chain);
    }

    private ResourceResolverDescriptor getDescriptor(String requestPath) {
        if (!requestPath.startsWith(PLUGIN_PREFIX)) {
            return null;
        }
        requestPath = requestPath.substring(PLUGIN_PREFIX.length());
        if (requestPath.isEmpty()) {
            return null;
        }
        var pluginKey = requestPath.split("/")[0];
        if (pluginKey.length() + 2 > requestPath.length()) {
            return null;
        }
        return getDescriptorMap().get(pluginKey + "-" + ResourceResolverDescriptor.class.getName());
    }

    @Override
    public String resolveUrlPath(String resourcePath, List<? extends Resource> locations, ResourceResolverChain chain) {
        var descriptor = getDescriptor(resourcePath);
        if (descriptor == null) {
            return chain.resolveUrlPath(resourcePath, locations);
        }
        return descriptor.resourceResolver().resolveUrlPath(resourcePath, locations, chain);
    }
}
