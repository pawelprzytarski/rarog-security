package eu.rarogsoftware.rarog.platform.app.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import eu.rarogsoftware.rarog.platform.api.i18n.I18nHelper;
import eu.rarogsoftware.rarog.platform.core.i18n.DefaultI18NextService;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowedLevel;
import io.swagger.v3.oas.annotations.Hidden;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.server.WebServerException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;

@RequestMapping("i18n/")
@RestController
@Hidden
public class I18NextResourcesController {
    private final Logger logger = LoggerFactory.getLogger(I18NextResourcesController.class);
    private final ObjectMapper jsonMapper = new ObjectMapper();
    private final DefaultI18NextService i18NextService;

    public I18NextResourcesController(DefaultI18NextService i18NextService) {
        this.i18NextService = i18NextService;
    }

    @GetMapping(
            value = "{locale}/{namespace}.json",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @AnonymousAllowed(AnonymousAllowedLevel.CORE)
    @ResponseBody
    public ResponseEntity<byte[]> getI18NextResource(@PathVariable("locale") String localeTag, @PathVariable("namespace") String namespace) {
        InputStream resourceInputStream;
        try {
            resourceInputStream = i18NextService.getI18NextResource(Locale.forLanguageTag(localeTag), namespace);
        } catch (IOException e) {
            throw new WebServerException("Failed to load i18Next resource", e);
        }
        if (resourceInputStream != null) {
            try (resourceInputStream) {
                byte[] bytes = IOUtils.toByteArray(resourceInputStream);
                if (localeTag.equals(I18nHelper.SYNTHETIC_LANGUAGE_TAG)) {
                    return ResponseEntity.ok(makeSyntheticLanguage(bytes));
                }
                return ResponseEntity.ok(bytes);
            } catch (IOException e) {
                logger.warn("Exception throw when trying to read translation", e);
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "i18Next resources for this URL doesn't exist");
    }

    private byte[] makeSyntheticLanguage(byte[] bytes) {
        try {
            var root = jsonMapper.readTree(bytes);
            var newRoot = new ObjectNode(jsonMapper.getNodeFactory());
            Queue<JsonNodeToProcess> queue = new ArrayDeque<>();
            root.fields().forEachRemaining(entry -> processJsonEntry(entry, newRoot, queue, entry.getKey()));
            while (!queue.isEmpty()) {
                var element = queue.poll();
                element.node.fields().forEachRemaining(entry -> processJsonEntry(entry, element.parent, queue, element.key + "." + entry.getKey()));
            }
            return newRoot.toString().getBytes(StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void processJsonEntry(Map.Entry<String, JsonNode> entry, ObjectNode parent, Queue<JsonNodeToProcess> queue, String key) {
        if (entry.getValue().isObject()) {
            var node = new ObjectNode(jsonMapper.getNodeFactory());
            parent.set(entry.getKey(), node);
            queue.add(new JsonNodeToProcess(key, node, entry.getValue()));
        } else {
            parent.set(entry.getKey(), new TextNode(key));
        }
    }

    private record JsonNodeToProcess(String key, ObjectNode parent, JsonNode node) {
    }
}
