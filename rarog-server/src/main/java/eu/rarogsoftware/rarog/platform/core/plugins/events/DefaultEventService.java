package eu.rarogsoftware.rarog.platform.core.plugins.events;

import eu.rarogsoftware.rarog.platform.api.commons.ClassHelpers;
import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventDeliveryFailedException;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventHandler;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventListener;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventService;
import eu.rarogsoftware.rarog.platform.api.task.TaskManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.time.Clock;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

@Service
@ExportComponent
public class DefaultEventService implements EventService {

    private static final Logger logger = LoggerFactory.getLogger(DefaultEventService.class);
    private static final String EVENT_MUST_NOT_BE_NULL = "Event must not be null.";

    private final Clock systemClock;
    private final TaskManager taskManager;

    private final Histogram eventDeliveryTimeHistogram;
    private final Histogram eventConsumerExecutionTimeHistogram;
    private final Counter successfulEventDeliveryCounter;
    private final Counter failedEventDeliveryCounter;
    Map<Class<?>, List<EventConsumerRegistration>> listeners = new ConcurrentHashMap<>();

    DefaultEventService(Clock clock, TaskManager taskManager, MetricsService metricsService) {
        this.systemClock = clock;
        this.taskManager = taskManager;

        eventDeliveryTimeHistogram = metricsService.createHistogram(HistogramSettings.settings()
            .name("rarog_event_service_event_delivery_duration")
            .labels("eventType")
            .description("Shows distribution of event delivery durations, labels are possible event types.")
            .unit("milliseconds")
            .buckets(1, 10, 100, 1000, 10000, 60000, 600000));

        eventConsumerExecutionTimeHistogram = metricsService.createHistogram(HistogramSettings.settings()
            .name("rarog_event_service_event_consumer_execution_duration")
            .labels("eventType")
            .description("Shows distribution of event consumer execution durations, labels are possible event types.")
            .unit("seconds")
            .buckets(0.001, 0.01, 0.1, 1, 10, 60, 600, 3600));

        successfulEventDeliveryCounter = metricsService.createCounter(MetricSettings.settings()
            .name("rarog_event_service_successful_event_delivery")
            .labels("eventType")
            .description("Shows count of event deliveries that succeeded, labels are possible event types."));

        failedEventDeliveryCounter = metricsService.createCounter(MetricSettings.settings()
            .name("rarog_event_service_failed_event_delivery")
            .labels("eventType")
            .description("Shows count of event deliveries that failed, labels are possible event types."));
    }

    @Autowired
    public DefaultEventService(TaskManager taskManager, MetricsService metricsService) {
        this(Clock.systemUTC(), taskManager, metricsService);
    }

    @Override
    public void publishSyncEvent(Object event) {
        Objects.requireNonNull(event, EVENT_MUST_NOT_BE_NULL);
        var wrappedEvent = new EventWrapper(event, systemClock.instant(), EventType.SYNC);

        callOnMatchingListeners(wrappedEvent, true, (listener, e) -> listener.eventConsumer().accept(e));
    }

    @Override
    public void publishSyncEventWithExceptions(Object event) {
        Objects.requireNonNull(event, EVENT_MUST_NOT_BE_NULL);
        var wrappedEvent = new EventWrapper(event, systemClock.instant(), EventType.SYNC);

        callOnMatchingListeners(wrappedEvent, false, (listener, e) -> listener.eventConsumer().accept(e));
    }

    @Override
    public void publishAsyncEvent(Object event) {
        Objects.requireNonNull(event, EVENT_MUST_NOT_BE_NULL);
        var wrappedEvent = new EventWrapper(event, systemClock.instant(), EventType.ASYNC);

        taskManager.runTask(() -> callOnMatchingListeners(wrappedEvent, true, (listener, e) -> listener.eventConsumer().accept(e)));
    }

    @Override
    public void publishParallelAsyncEvent(Object event) {
        Objects.requireNonNull(event, EVENT_MUST_NOT_BE_NULL);
        var wrappedEvent = new EventWrapper(event, systemClock.instant(), EventType.PARALLEL_ASYNC);

        callOnMatchingListeners(wrappedEvent, true, (listener, e) ->
            taskManager.runTask(() ->
                listener.eventConsumer().accept(e)));
    }

    @Override
    public void registerListener(eu.rarogsoftware.rarog.platform.api.plugins.events.EventListener listener) {
        Objects.requireNonNull(listener, "EventListener cannot be null.");
        logger.debug("Registering EventListener {}", listener.getClass().getName());
        AtomicInteger registeredHandlers = new AtomicInteger();
        Arrays.stream(listener.getClass().getMethods())
            .filter(method -> {
                if (method.isAnnotationPresent(EventHandler.class)) {
                    logger.trace("Found EventHandler method: {}#{}", listener.getClass().getName(), method.getName());
                    return true;
                }
                return false;
            })
            .filter(method -> {
                if (method.getParameterCount() != 1) {
                    logger.warn("Found EventHandler method with wrong parameter count, skipping: {}#{}", listener.getClass().getName(), method.getName());
                    return false;
                }
                return true;
            })
            .forEach(method -> {
                registeredHandlers.getAndIncrement();
                EventConsumerRegistration eventRegistration = getEventConsumerRegistration(listener, method);
                registerHandler(method, eventRegistration);
                logger.trace("Registered event {} handler {}#{}", method.getParameterTypes()[0], listener.getClass().getName(), method.getName());
            });
        if (registeredHandlers.get() == 0) {
            throw new IllegalArgumentException("EventListener %s has no valid @EventHandlers specified.".formatted(listener.getClass()));
        }
    }

    @Override
    public void unregisterListener(eu.rarogsoftware.rarog.platform.api.plugins.events.EventListener listener) {
        listeners.forEach((ignored, list) -> list.removeIf(registration -> registration.listenerClass.equals(listener.getClass())));
        listeners.entrySet().removeIf(entry -> entry.getValue().isEmpty());
    }

    private void callOnMatchingListeners(EventWrapper wrappedEvent, boolean swallowExceptions, BiConsumer<EventConsumerRegistration, EventWrapper> callback) {
        for (var eventClass = wrappedEvent.event().getClass(); eventClass != null; eventClass = eventClass.getSuperclass()) {
            listeners.getOrDefault(eventClass, Collections.emptyList())
                .forEach(listener -> callOnListener(wrappedEvent, swallowExceptions, listener, callback));
        }
        ClassHelpers.getImplementedInterfaces(wrappedEvent.event().getClass())
            .forEach(interfaze -> listeners.getOrDefault(interfaze, Collections.emptyList())
                .forEach(listener -> callOnListener(wrappedEvent, swallowExceptions, listener, callback)));
    }

    private static void callOnListener(EventWrapper wrappedEvent,
                                       boolean swallowExceptions,
                                       EventConsumerRegistration listener,
                                       BiConsumer<EventConsumerRegistration, EventWrapper> callback) {
        try {
            callback.accept(listener, wrappedEvent);
        } catch (EventDeliveryFailedException ex) {
            if (!swallowExceptions) {
                throw ex;
            }
            // else - already logged the error, swallowing.
        }
    }

    private void registerHandler(Method method, EventConsumerRegistration eventRegistration) {
        listeners.compute(method.getParameterTypes()[0], (type, oldRegistrations) -> {
            var newRegistrations = oldRegistrations == null ? new LinkedList<EventConsumerRegistration>() : oldRegistrations;
            newRegistrations.add(eventRegistration);
            return newRegistrations;
        });
    }

    private EventConsumerRegistration getEventConsumerRegistration(eu.rarogsoftware.rarog.platform.api.plugins.events.EventListener listener, Method method) {
        final Consumer<EventWrapper> eventConsumer = wrappedEvent -> {
            var timeFromPublishing = systemClock.instant().toEpochMilli() - wrappedEvent.publishedTime().toEpochMilli();
            eventDeliveryTimeHistogram
                .labels(wrappedEvent.eventType().name())
                .observe(timeFromPublishing);

            try (var ignored = eventConsumerExecutionTimeHistogram
                .labels(wrappedEvent.eventType().name())
                .startTimer()) {

                method.invoke(listener, wrappedEvent.event());

            } catch (Throwable e) {
                failedEventDeliveryCounter
                    .labels(wrappedEvent.eventType().name())
                    .increment();
                logger.error("Error occurred when invoking event handler {}#{}", listener.getClass(), method.getName(), e);
                throw new EventDeliveryFailedException(listener, method, e);
            }
            successfulEventDeliveryCounter
                .labels(wrappedEvent.eventType().name())
                .increment();
        };
        return new EventConsumerRegistration(listener.getClass(), eventConsumer);
    }

    private record EventConsumerRegistration(Class<? extends EventListener> listenerClass,
                                             Consumer<EventWrapper> eventConsumer) {
    }
}
