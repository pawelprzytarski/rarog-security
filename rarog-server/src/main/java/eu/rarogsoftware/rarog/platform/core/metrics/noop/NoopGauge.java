package eu.rarogsoftware.rarog.platform.core.metrics.noop;

import eu.rarogsoftware.rarog.platform.api.metrics.Gauge;
import org.apache.commons.lang3.time.StopWatch;

import java.io.IOException;
import java.util.concurrent.Callable;

class NoopGauge implements Gauge {
    @Override
    public void increase(double amount) {
        // do nothing
    }

    @Override
    public void decrease(double amount) {
        // do nothing
    }

    @Override
    public void set(double value) {
        // do nothing
    }

    @Override
    public Timer startTimer() {
        var stopWatch = new StopWatch();
        stopWatch.start();
        return new Timer() {
            @Override
            public double stop() {
                stopWatch.stop();
                return stopWatch.getTime()/1000.0;
            }

            @Override
            public void close() throws IOException {
                stop();
            }
        };
    }

    @Override
    public double measureExecutionTime(Runnable timeable) {
        var timer = startTimer();
        timeable.run();
        return timer.stop();
    }

    @Override
    public <T> T measureExecutionTime(Callable<T> timeable) throws Exception {
        return timeable.call();
    }

    @Override
    public Gauge labels(String... labels) {
        return this;
    }
}
