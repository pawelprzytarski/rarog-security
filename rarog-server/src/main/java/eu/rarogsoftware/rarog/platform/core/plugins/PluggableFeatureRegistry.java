package eu.rarogsoftware.rarog.platform.core.plugins;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;

import java.util.Collection;
import java.util.function.Supplier;

public interface PluggableFeatureRegistry {
    <T extends FeatureDescriptor> void registerFeatureModule(Class<T> descriptorType, Supplier<FeatureModule<T>> featureModule);

    void unregisterFeatureModule(Class<? extends FeatureDescriptor> descriptorType);

    void plugDescriptors(Plugin plugin, Collection<? extends FeatureDescriptor> descriptors);

    void unplugDescriptors(Plugin plugin, Collection<? extends FeatureDescriptor> descriptors);

    void purgeDescriptors(Plugin plugin, Collection<? extends FeatureDescriptor> descriptors);
}
