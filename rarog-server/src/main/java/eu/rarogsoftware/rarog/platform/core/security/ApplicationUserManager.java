package eu.rarogsoftware.rarog.platform.core.security;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.security.provisioning.UserDetailsManager;

public abstract class ApplicationUserManager implements UserDetailsManager, UserDetailsPasswordService {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    protected AuthenticationManager authenticationManager;

    protected StandardUser createApplicationUser(UserDetails user) {
        return StandardUser.builder()
                .addAuthorities(user.getAuthorities().toArray(new GrantedAuthority[0]))
                .id(user instanceof StandardUser ? ((StandardUser) user).getId() : -1)
                .username(user.getUsername())
                .password(user.getPassword())
                .enabled(user.isEnabled())
                .accountNonExpired(user.isAccountNonExpired())
                .accountNonLocked(user.isAccountNonLocked())
                .mfaEnabled(user instanceof StandardUser && ((StandardUser) user).isMfaEnabled())
                .build();
    }

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
        if (currentUser == null) {
            // This would indicate bad coding somewhere
            throw new AccessDeniedException(
                    "Can't change password as no Authentication object found in context " + "for current user.");
        }
        String username = currentUser.getName();
        this.logger.debug("Changing password for user '{}'", username);
        // If an authentication manager has been set, re-authenticate the user with the
        // supplied password.
        if (this.authenticationManager != null) {
            this.logger.debug("Reauthenticating user '{}' for password change request.", username);
            this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, oldPassword));
        } else {
            this.logger.debug("No authentication manager set. Password won't be re-checked.");
        }
        storeNewPassword(username, newPassword);
    }

    protected abstract void storeNewPassword(String username, String newPassword);
}
