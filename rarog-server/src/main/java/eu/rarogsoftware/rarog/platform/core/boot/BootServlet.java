package eu.rarogsoftware.rarog.platform.core.boot;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.ClassLoaderDynamicAssetsLoader;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoader;
import eu.rarogsoftware.rarog.platform.app.boot.SpringBootManager;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@SuppressFBWarnings(value = {"UNVALIDATED_REDIRECT", "XSS_SERVLET"}, justification = "Resource returns known resources, not XSSable")
public class BootServlet extends HttpServlet {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final Map<String, Action> actions;
    private final ClassLoaderDynamicAssetsLoader assetsLoader;
    private boolean redirectEnabled = true;
    private SpringBootManager.StartState startState;
    private SpringBootManager.AsyncStartState asyncStartState;

    public BootServlet() {
        Map<String, Action> actionsTemp = new HashMap<>();
        actionsTemp.put(null, this::index);
        actionsTemp.put("/", this::index);
        actionsTemp.put("/status", this::currentStatus);
        actionsTemp.put("/status/", this::currentStatus);
        actions = Collections.unmodifiableMap(actionsTemp);
        assetsLoader = new ClassLoaderDynamicAssetsLoader("descriptors/webpack-app.manifest.json", 10L);
    }

    public void updateStatus(SpringBootManager.StartState startState, SpringBootManager.AsyncStartState asyncStartState) {
        this.startState = startState;
        this.asyncStartState = asyncStartState;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!redirectEnabled) {
            redirectToMainPage(request, response);
        }
        var action = actions.get(request.getPathInfo());
        if (action != null) {
            action.doAction(request, response);
        } else if (request.getPathInfo().startsWith("/js/")) {
            js(request, response);
        } else if (request.getPathInfo().startsWith("/css/")) {
            css(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/boot/");
        }
    }

    private static void redirectToMainPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        var contextPath = request.getContextPath();
        if (!StringUtils.hasText(contextPath)) {
            contextPath = "/";
        }
        if (!contextPath.startsWith("/")) {
            contextPath = "/" + contextPath;
        }
        response.sendRedirect(contextPath);
    }

    private void currentStatus(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        Map<String, ? extends Serializable> jsonMap = getStatusJsonMap();
        response.getWriter().print(objectMapper.writeValueAsString(jsonMap));
    }

    private Map<String, ? extends Serializable> getStatusJsonMap() {
        if (startState == SpringBootManager.StartState.FAILED) {
            return Map.of("step", "Start failed", "progress", 100);
        } else {
            int numberOfSteps = SpringBootManager.StartState.values().length + SpringBootManager.AsyncStartState.values().length - 4;
            int currentStep = startState.ordinal() + asyncStartState.ordinal();
            var stepString = currentStep + "/" + numberOfSteps + " " + getStepDescription();
            return Map.of("step", stepString, "progress", currentStep * 100.0 / numberOfSteps);
        }
    }

    private String getStepDescription() {
        return switch (startState) {
            case INITIALIZED -> "Start initialized";
            case IOC_STARTED -> "Core system started";
            case NOT_STARTED -> "Not started yet";
            case SERVLET_CONTEXT_STARTED -> "Webapp started";
            case DATABASE_MIGRATED -> "Database migrated";
            case DATABASE_STARTED -> "Database connection started";
            case SYNCH_STARTED -> switch (asyncStartState) {
                case FULL_STARTED -> "Startup finished";
                case NOT_STARTED -> "Waiting for core";
                case PLUGINS_LOADED -> "Plugins loaded";
                case PLUGINS_LOADING -> "Loading plugins";
                case STARTING -> "Modules startup";
                case FAILED -> "Startup failed";
            };
            case FULL_STARTED -> "Startup finished";
            case FAILED -> "Startup failed";
        };
    }

    private void index(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("static/boot.html");
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            if (inputStream != null) {
                response.setStatus(HttpStatus.OK.value());
                response.setContentType("text/html");
                IOUtils.copy(inputStream, outputStream);
                String htmlPage = outputStream.toString(StandardCharsets.UTF_8).replace("<link rel=\"stylesheet\" href=\"boot.css\">", getCssAssetsHtml(request));
                String bootPage = htmlPage.replace("<script src=\"boot.js\"></script>", getJsAssetsHtml(request));
                response.getWriter().write(bootPage);
            } else {
                response.sendError(HttpStatus.NOT_FOUND.value());
            }
        }
    }

    private String getCssAssetsHtml(HttpServletRequest request) {
        StringBuilder builder = new StringBuilder();
        Optional<DynamicAssetsLoader.EntrypointAssetData> bootAssets = assetsLoader.getAssetsForEntryPoint("boot");
        String prefix = "<link rel=\"stylesheet\" href=\"" + request.getContextPath() + request.getServletPath();
        bootAssets.ifPresent(assets ->
                assets.css().forEach(webpackAssetData -> {
                    builder.append(prefix);
                    builder.append(webpackAssetData.source().replace("/static/", "/"));
                    builder.append("\">");
                }));
        return builder.toString();
    }

    private String getJsAssetsHtml(HttpServletRequest request) {
        StringBuilder builder = new StringBuilder();
        Optional<DynamicAssetsLoader.EntrypointAssetData> bootAssets = assetsLoader.getAssetsForEntryPoint("boot");
        String prefixJs = "<script src=\"" + request.getContextPath() + request.getServletPath();
        bootAssets.ifPresent(assets ->
                assets.js().forEach(webpackAssetData -> {
                    builder.append(prefixJs);
                    builder.append(webpackAssetData.source().replace("/static/", "/"));
                    builder.append("\"></script>");
                }));
        return builder.toString();
    }

    private void css(HttpServletRequest request, HttpServletResponse response) throws IOException {
        sendFile(response, "static" + request.getPathInfo(), "text/css");
    }

    private void js(HttpServletRequest request, HttpServletResponse response) throws IOException {
        sendFile(response, "static" + request.getPathInfo(), "text/javascript");
    }

    private void sendFile(HttpServletResponse response, String name, String mediaType) throws IOException {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(name);
             ServletOutputStream outputStream = response.getOutputStream()) {
            if (inputStream != null) {
                response.setStatus(HttpStatus.OK.value());
                response.setContentType(mediaType);
                IOUtils.copy(inputStream, outputStream);
            } else {
                response.sendError(HttpStatus.NOT_FOUND.value());
            }
        }
    }

    public void setRedirectEnabled(boolean enabled) {
        this.redirectEnabled = enabled;
    }

    interface Action {
        void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
    }
}
