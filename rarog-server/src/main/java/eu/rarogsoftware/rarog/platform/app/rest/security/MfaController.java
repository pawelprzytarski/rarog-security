package eu.rarogsoftware.rarog.platform.app.rest.security;

import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaEndpoint;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaMethod;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaService;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/mfa")
@Tag(name = "Multi factor authentication")
@SecurityRequirement(name = "logged_in")
public class MfaController {
    private final MfaService mfaService;

    public MfaController(MfaService mfaService) {
        this.mfaService = mfaService;
    }

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @MfaEndpoint
    @Operation(summary = "Get available MFA methods", description = "Returns list of available MFA methods "
            + "for currently logged in user")
    @Parameter(name = "user", hidden = true)
    @ApiResponse(responseCode = "200")
    public Collection<SanitizedMethod> mfaMethodList(@AuthenticationPrincipal StandardUser user) {
        return mfaService.getMethodsForUser(user).stream().map(SanitizedMethod::new).toList();
    }

    @Schema(name = "MFA method details")
    record SanitizedMethod(String imageUrl, String name, String nameKey, String descriptionKey, String resource,
                           Long order) {
        SanitizedMethod(MfaMethod method) {
            this(method.imageUrl(), method.name(), method.nameKey(), method.descriptionKey(), method.resource(), method.order());
        }
    }
}
