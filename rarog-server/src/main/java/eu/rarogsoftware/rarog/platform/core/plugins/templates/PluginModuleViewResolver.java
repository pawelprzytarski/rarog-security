package eu.rarogsoftware.rarog.platform.core.plugins.templates;

import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.templates.PluginViewResolver;
import eu.rarogsoftware.rarog.platform.api.plugins.templates.ViewResolverDescriptor;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModuleComponent;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@AutoRegisterFeatureModuleComponent(ViewResolverDescriptor.class)
public class PluginModuleViewResolver extends AbstractSimpleFeatureModule<ViewResolverDescriptor> implements ViewResolver, Ordered {
    private List<ViewResolver> viewResolvers = null;

    @Override
    public View resolveViewName(String viewName, Locale locale) throws Exception {
        List<ViewResolver> resolvers;
        Plugin plugin = getPluginForCurrentRequest();
        synchronized (this) {
            if (viewResolvers == null) {
                viewResolvers = calculateResolvers();
            }
            resolvers = new ArrayList<>(viewResolvers);
        }
        try {
            return resolvers.stream()
                    .map(resolver -> {
                        try {
                            return resolver instanceof PluginViewResolver pluginResolver && plugin != null
                                    ? pluginResolver.resolveViewName(viewName, locale, plugin)
                                    : resolver.resolveViewName(viewName, locale);
                        } catch (Exception e) {
                            throw new InternalException(e);
                        }
                    })
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElse(null);
        } catch (InternalException e) {
            throw e.innerException;
        }
    }

    private List<ViewResolver> calculateResolvers() {
        return getDescriptorMap().values().stream()
                .flatMap(descriptor -> descriptor.viewResolverSupplier().get().stream())
                .sorted(AnnotationAwareOrderComparator.INSTANCE)
                .toList();
    }

    @Override
    protected void clearCache() {
        synchronized (this) {
            viewResolvers = null;
        }
    }

    @Override
    public int getOrder() {
        return 10;
    }

    static class InternalException extends RuntimeException {
        private final Exception innerException;

        public InternalException(Exception cause) {
            super(cause);
            this.innerException = cause;
        }
    }
}
