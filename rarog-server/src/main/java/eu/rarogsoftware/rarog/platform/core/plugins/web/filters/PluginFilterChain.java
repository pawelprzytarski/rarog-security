package eu.rarogsoftware.rarog.platform.core.plugins.web.filters;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import eu.rarogsoftware.rarog.platform.api.metrics.Histogram;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class stores filters registered in plugins and the original filters.
 * It descends into the original chain after all the plugin filters are invoked.
 */
public class PluginFilterChain implements FilterChain {
    private final Logger logger = LoggerFactory.getLogger(PluginFilterChain.class);
    private final List<Filter> filters = new LinkedList<>();
    private Iterator<Filter> filtersIterator = null;
    private final FilterChain originalChain;
    private final Histogram chainExecutionHistogram;
    private Histogram.Timer chainExecutionTimer;

    /**
     * Constructor.
     *
     * @param originalChain The chain which will be called after all plugin filters execute.
     */
    public PluginFilterChain(FilterChain originalChain, Histogram chainExecutionHistogram) {
        this.originalChain = originalChain;
        this.chainExecutionHistogram = chainExecutionHistogram;
    }

    public void addFilter(Filter filter) {
        filters.add(filter);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        if (filtersIterator == null) {
            logger.trace("Initializing plugin filter chain");
            filtersIterator = filters.iterator();
            chainExecutionTimer = chainExecutionHistogram.startTimer();
        }
        if (filtersIterator.hasNext()) {
            Filter filter = filtersIterator.next();
            logger.trace("Invoking next filter in the plugin filter chain: {}", filter.getClass().getName());
            filter.doFilter(request, response, this);
        } else {
            chainExecutionTimer.observeDuration();
            logger.trace("No more filters left in plugin filter chain, going back into the parent chain");
            originalChain.doFilter(request, response);
        }
    }

    public List<Filter> getFilters() {
        return List.copyOf(filters);
    }

}
