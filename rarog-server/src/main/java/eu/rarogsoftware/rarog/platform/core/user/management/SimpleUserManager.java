package eu.rarogsoftware.rarog.platform.core.user.management;

import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.user.management.*;
import eu.rarogsoftware.rarog.platform.api.user.management.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@ExportComponent
public class SimpleUserManager implements UserManager {
    private final UserStore userStore;
    private final PasswordEncoder passwordEncoder;

    public SimpleUserManager(UserStore userStore,
                             PasswordEncoder passwordEncoder) {
        this.userStore = userStore;
        this.passwordEncoder = passwordEncoder;
    }

    private static void throwUserNotFound(String username) throws UserNotFoundException {
        throw new UserNotFoundException(username);
    }

    @Override
    public Long createUser(StandardUser user) throws UserManagementException {
        if (userStore.userExists(user.getUsername())) {
            throw new UsernameAlreadyTaken("Username %s is already taken".formatted(user.getUsername()));
        }
        return userStore.storeUser(convertUserTypes(user));
    }

    private SimpleUser convertUserTypes(StandardUser user) {
        return new SimpleUser(
                user.getUsername(),
                user.getPassword() != null ? passwordEncoder.encode(user.getPassword()) : null,
                user.isEnabled(),
                user.isMfaEnabled() ? 1 : 0,
                user.getAuthorities()
                        .stream()
                        .filter(RoleGrantedAuthority.class::isInstance)
                        .map(RoleGrantedAuthority.class::cast)
                        .map(RoleGrantedAuthority::role)
                        .collect(Collectors.toSet())
        );
    }

    @Override
    public void updateUser(StandardUser user) throws UserManagementException {
        throwIfUserDoesNotExist(user);
        userStore.overrideUserByUsername(convertUserTypes(user));
    }

    private void throwIfUserDoesNotExist(StandardUser user) throws UserNotFoundException {
        throwIfUserDoesNotExist(user.getUsername());
    }

    private void throwIfUserDoesNotExist(String username) throws UserNotFoundException {
        if (!userStore.userExists(username)) {
            throw new UserNotFoundException("User with name %s do not exist.".formatted(username));
        }
    }

    @Override
    public void updatePassword(String username, String newPassword) throws UserManagementException {
        if (!userExists(username)) {
            throwUserNotFound(username);
        }
        userStore.updatePassword(username, passwordEncoder.encode(newPassword));
    }

    @Override
    public void updatePassword(long id, String newPassword) throws UserManagementException {
        if (!userExists(id)) {
            throwUserNotFound(Long.toString(id));
        }
        userStore.updatePassword(id, passwordEncoder.encode(newPassword));
    }

    @Override
    public StandardUser getUser(String username) throws UserManagementException {
        return convertUserTypes(userStore.getUser(username));
    }

    private StandardUser convertUserTypes(SimpleUser user) {
        return StandardUser.builder()
                .id(user.id())
                .username(user.username())
                .password(user.passwordHash())
                .enabled(user.active())
                .mfaEnabled(user.mfaLevel() > 0)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .roles(user.roles())
                .build();
    }

    @Override
    public StandardUser getUser(Long id) throws UserManagementException {
        return convertUserTypes(userStore.getUser(id));
    }

    @Override
    public void deleteUser(String username) throws UserManagementException {
        throwIfUserDoesNotExist(username);
        userStore.deleteUser(username);
    }

    @Override
    public boolean userExists(String username) {
        return userStore.userExists(username);
    }

    @Override
    public boolean userExists(Long id) {
        return userStore.userExists(id);
    }
}
