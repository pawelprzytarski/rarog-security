package eu.rarogsoftware.rarog.platform.core.security;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.core.common.Range;

import java.time.Instant;
import java.util.List;

public interface LoginAttemptService {
    void registerAttempt(StandardUser user, LoginAttempt loginAttempt);

    List<LoginAttempt> getLoginAttempts(StandardUser user, int numberOfAttempts);

    List<LoginAttempt> getLoginAttempts(StandardUser user, Range<Instant> range);

    record LoginAttempt(Instant attemptTime, boolean successful, String attemptIp, String userAgent) {
        static LoginAttempt successfulAttempt(Instant attemptTime, String attemptIp, String userAgent) {
            return new LoginAttempt(attemptTime, true, attemptIp, userAgent);
        }

        static LoginAttempt failedAttempt(Instant attemptTime, String attemptIp, String userAgent) {
            return new LoginAttempt(attemptTime, false, attemptIp, userAgent);
        }
    }
}
