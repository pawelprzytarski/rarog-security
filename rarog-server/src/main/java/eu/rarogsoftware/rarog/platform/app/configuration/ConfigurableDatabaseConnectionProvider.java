package eu.rarogsoftware.rarog.platform.app.configuration;

import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.SQLTemplates;
import eu.rarogsoftware.commons.database.connection.AbstractDatabaseConnectionProvider;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.function.Supplier;

class ConfigurableDatabaseConnectionProvider extends AbstractDatabaseConnectionProvider {
    private final SQLTemplates templates;

    public ConfigurableDatabaseConnectionProvider(DataSource dataSource, SQLTemplates templates) {
        super(() -> DataSourceUtils.getConnection(dataSource), 0);
        this.templates = templates;
    }

    @Override
    protected SQLQueryFactory createSqlFactory(Supplier<Connection> connection) {
        return new SQLQueryFactory(templates, connection);
    }
}
