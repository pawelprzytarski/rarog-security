package eu.rarogsoftware.rarog.platform.core.common.urlutils;

import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;
import java.net.URI;

@Component
public class BaseUrlHelper {
    private final HttpServletRequest request;

    public BaseUrlHelper(HttpServletRequest request) {
        this.request = request;
    }

    public String getBaseRelativeUrl() {
        return request.getContextPath() + "/";
    }

    public String completeRelativeUrl(String url) {
        if (url.startsWith("/")) {
            url = url.substring(1);
        }
        return URI.create(getBaseRelativeUrl() + url).normalize().toString();
    }
}
