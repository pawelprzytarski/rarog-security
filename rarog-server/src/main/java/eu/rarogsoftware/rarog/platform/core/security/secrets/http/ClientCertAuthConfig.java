package eu.rarogsoftware.rarog.platform.core.security.secrets.http;

import eu.rarogsoftware.rarog.platform.api.security.secrets.HttpClientAuthConfigService;
import eu.rarogsoftware.rarog.platform.core.security.secrets.SslContextConfig;
import org.jetbrains.annotations.NotNull;

import javax.net.ssl.SSLParameters;
import java.net.http.HttpClient;
import java.util.Map;

public class ClientCertAuthConfig extends SslContextConfig implements HttpClientAuthConfigService.AuthConfig {
    private final SSLParameters sslParam;

    public ClientCertAuthConfig(String parameters) {
        super(parameters);
        sslParam = prepareParams();
    }

    public ClientCertAuthConfig(Map<String, Object> authenticationData) {
        super(authenticationData);
        sslParam = prepareParams();
    }

    @NotNull
    private SSLParameters prepareParams() {
        var params = new SSLParameters();
        params.setNeedClientAuth(true);
        return params;
    }

    @Override
    public HttpClient.Builder configureHttpClient(HttpClient.Builder builder) {
        return builder
                .sslParameters(sslParam)
                .sslContext(getSslContext());
    }
}
