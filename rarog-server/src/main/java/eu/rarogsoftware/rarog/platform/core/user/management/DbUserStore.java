package eu.rarogsoftware.rarog.platform.core.user.management;

import com.querydsl.core.Tuple;
import com.querydsl.sql.SQLQueryFactory;
import eu.rarogsoftware.commons.database.connection.DatabaseConnectionException;
import eu.rarogsoftware.commons.database.connection.DatabaseConnectionProvider;
import eu.rarogsoftware.rarog.platform.api.user.management.*;
import eu.rarogsoftware.rarog.platform.api.user.management.*;
import eu.rarogsoftware.rarog.platform.db.QAuthorities;
import eu.rarogsoftware.rarog.platform.db.QUsers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;


@Component
public class DbUserStore implements UserStore {
    private static final QUsers users = QUsers.users;
    private static final QAuthorities authorities = QAuthorities.authorities;
    private final Logger logger = LoggerFactory.getLogger(DbUserStore.class);
    private final DatabaseConnectionProvider databaseConnectionProvider;

    public DbUserStore(DatabaseConnectionProvider databaseConnectionProvider) {
        this.databaseConnectionProvider = databaseConnectionProvider;
    }

    private static void throwUserNotFound(String username) throws UserNotFoundException {
        throw new UserNotFoundException(username);
    }

    @Override
    public Long storeUser(SimpleUser user) throws UserManagementException, DatabaseConnectionException {
        return runAndCatchDbExceptions(() -> databaseConnectionProvider.getConnection(query -> {
            logger.trace("Storing user to database: {}", user);
            var userId = query.insert(users)
                    .set(users.username, user.username())
                    .set(users.active, user.active())
                    .set(users.mfaLevel, user.mfaLevel())
                    .set(users.password, user.passwordHash())
                    .executeWithKey(users.id);
            if (userId == null) {
                logger.debug("Failed to store user {}", user.username());
                throw new UserManagementException("Failed to create user silently. See database logs for details");
            }
            logger.trace("Saving authorities for users {} with id {}", user.username(), userId);
            var insert = query.insert(authorities);

            user.roles().forEach(role -> insert
                    .set(authorities.userId, userId)
                    .set(authorities.authority, role.getDatabaseCode())
                    .addBatch());
            insert.execute();
            logger.debug("User {} stored", user.username());
            return userId;
        }));
    }

    private <T> T runAndCatchDbExceptions(Operation<T> supplier) throws UserManagementException {
        try {
            return supplier.run();
        } catch (DatabaseConnectionException e) {
            throw new UserManagementException("Inner database exception", e);
        }
    }

    @Override
    public void overrideUserByUsername(SimpleUser user) throws UserManagementException {
        runAndCatchDbExceptions(() -> databaseConnectionProvider.getConnectionInTransaction(DatabaseConnectionProvider.TransactionLevel.READ_COMMITTED, query -> {
            logger.trace("Updating user {}", user);
            var savedUser = loadUserInConnection(user.username(), query);

            query.update(users)
                    .set(users.active, user.active())
                    .set(users.mfaLevel, user.mfaLevel())
                    .where(users.username.eq(user.username()))
                    .execute();

            var authoritiesToRemove = savedUser.roles().stream()
                    .distinct()
                    .filter(o -> !user.roles().contains(o))
                    .map(UserRole::getDatabaseCode)
                    .collect(Collectors.toSet());
            var authoritiesToAdd = user.roles().stream()
                    .distinct()
                    .filter(o -> !savedUser.roles().contains(o))
                    .map(UserRole::getDatabaseCode)
                    .collect(Collectors.toSet());
            logger.trace("Authorities to remove: {}. Authorities to add: {}", authoritiesToRemove, authoritiesToAdd);

            if (!authoritiesToRemove.isEmpty()) {
                query.delete(authorities)
                        .where(authorities.userId.eq(savedUser.id())
                                .and(authorities.authority.in(authoritiesToRemove)))
                        .execute();
            }

            if (!authoritiesToAdd.isEmpty()) {
                var insert = query.insert(authorities);
                authoritiesToAdd.forEach(authority -> insert
                        .set(authorities.userId, savedUser.id())
                        .set(authorities.authority, authority)
                        .addBatch());
                insert.execute();
            }

            logger.debug("User {} updated", user.username());
            return new Void[0];
        }));
    }

    @Override
    public void updatePassword(String username, String newPassword) throws UserManagementException {
        if (!userExists(username)) {
            throwUserNotFound(username);
        }
        runAndCatchDbExceptions(() -> databaseConnectionProvider.getConnectionInTransaction(DatabaseConnectionProvider.TransactionLevel.READ_COMMITTED, query -> {
            logger.trace("Updating password of user {}", username);
            query.update(users)
                    .set(users.password, newPassword)
                    .where(users.username.eq(username))
                    .execute();

            logger.debug("User {}'s password updated", username);
            return new Void[0];
        }));
    }

    @Override
    public void updatePassword(long id, String newPassword) throws UserManagementException {
        runAndCatchDbExceptions(() -> databaseConnectionProvider.getConnectionInTransaction(DatabaseConnectionProvider.TransactionLevel.READ_COMMITTED, query -> {
            logger.trace("Updating password of user {}", id);
            query.update(users)
                    .set(users.password, newPassword)
                    .where(users.id.eq(id))
                    .execute();

            logger.debug("User {}'s password updated", id);
            return new Void[0];
        }));
    }

    private SimpleUser loadUserInConnection(String username, SQLQueryFactory query) throws UserNotFoundException {
        var userTuple = query.select(
                        users.all())
                .from(users)
                .where(users.username.eq(username))
                .fetchOne();

        return translateTupleToUser(username, query, userTuple);
    }

    private SimpleUser translateTupleToUser(Object identificator, SQLQueryFactory query, Tuple userTuple) throws UserNotFoundException {
        if (userTuple == null) {
            logger.debug("User {} not found", identificator);
            throwUserNotFound(String.format("User %s not found", identificator));
        }
        logger.trace("User {} retrieved", identificator);

        var userAuthorities = query.select(authorities.authority)
                .from(authorities)
                .where(authorities.userId.eq(userTuple.get(users.id)))
                .fetch();

        if (userAuthorities.isEmpty()) {
            logger.trace("User {} has no authorities", identificator);
            throwUserNotFound(String.format("User %s has no authorities", identificator));
        }

        return new SimpleUser(
                userTuple.get(users.id),
                userTuple.get(users.username),
                userTuple.get(users.password),
                userTuple.get(users.active),
                userTuple.get(users.mfaLevel),
                userAuthorities.stream().map(UserRole::fromCode).collect(Collectors.toSet())
        );
    }

    @Override
    public SimpleUser getUser(String username) throws UserManagementException {
        return runAndCatchDbExceptions(() -> databaseConnectionProvider.getConnection(query -> loadUserInConnection(username, query)));
    }

    @Override
    public SimpleUser getUser(Long id) throws UserManagementException {
        logger.trace("Retrieving user by id: {}", id);
        return runAndCatchDbExceptions(() -> databaseConnectionProvider.getConnection(query -> {
            var userTuple = query.select(
                            users.all())
                    .from(users)
                    .where(users.id.eq(id))
                    .fetchOne();

            return translateTupleToUser(id, query, userTuple);
        }));
    }

    @Override
    public void deleteUser(String username) throws UserManagementException {
        runAndCatchDbExceptions(() -> databaseConnectionProvider.getConnection(query -> {
            logger.trace("Removing user {}", username);
            boolean userRemoved = query.delete(users)
                    .where(users.username.eq(username))
                    .execute() == 1;
            if (!userRemoved) {
                throwUserNotFound(username);
            }
            logger.debug("User {} removed successful: {}", username, userRemoved);
            return new Void[0];
        }));
    }

    @Override
    public boolean userExists(String username) {
        return databaseConnectionProvider.getConnection(query -> {
            logger.trace("Checking if user {} exists", username);
            boolean result = query.select(users.count())
                    .from(users)
                    .where(users.username.eq(username))
                    .fetchOne() == 1;
            logger.trace("User {} exists check result: {}", username, result);
            return result;
        });
    }

    @Override
    public boolean userExists(Long id) {
        return databaseConnectionProvider.getConnection(query -> {
            logger.trace("Checking if user {} exists", id);
            boolean result = query.select(users.count())
                    .from(users)
                    .where(users.id.eq(id))
                    .fetchOne() == 1;
            logger.trace("User {} exists check result: {}", id, result);
            return result;
        });
    }

    interface Operation<U> {
        U run() throws UserManagementException;
    }
}
