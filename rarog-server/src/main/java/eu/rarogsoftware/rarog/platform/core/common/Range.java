package eu.rarogsoftware.rarog.platform.core.common;

public record Range<T extends Comparable<T>>(RangePoint<T> start, RangePoint<T> stop) {

    public static <U extends Comparable<U>> Range<U> inclusive(U start, U stop) {
        return new Range<>(RangePoint.inclusive(start), RangePoint.inclusive(stop));
    }

    public static <U extends Comparable<U>> Range<U> exclusive(U start, U stop) {
        return new Range<>(RangePoint.exclusive(start), RangePoint.exclusive(stop));
    }

    public static <U extends Comparable<U>> Range<U> openEnd(U start, U stop) {
        return new Range<>(RangePoint.inclusive(start), RangePoint.exclusive(stop));
    }

    public static <U extends Comparable<U>> Range<U> openStart(U start, U stop) {
        return new Range<>(RangePoint.exclusive(start), RangePoint.inclusive(stop));
    }

    public boolean isInRange(T point) {
        return !(start().before(point) || stop().after(point));
    }

    public record RangePoint<T extends Comparable<T>>(T point, boolean inclusive) {
        static <U extends Comparable<U>> RangePoint<U> inclusive(U point) {
            return new RangePoint<>(point, true);
        }

        static <U extends Comparable<U>> RangePoint<U> exclusive(U point) {
            return new RangePoint<>(point, false);
        }

        public boolean before(T point) {
            return inclusive ? this.point.compareTo(point) > 0 : this.point.compareTo(point) >= 0;
        }

        public boolean after(T point) {
            return inclusive ? this.point.compareTo(point) < 0 : this.point.compareTo(point) <= 0;
        }
    }
}
