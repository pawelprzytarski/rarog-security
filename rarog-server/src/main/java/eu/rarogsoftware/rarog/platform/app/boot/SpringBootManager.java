package eu.rarogsoftware.rarog.platform.app.boot;

import eu.rarogsoftware.commons.utils.Pair;
import eu.rarogsoftware.rarog.platform.api.metrics.Gauge;
import eu.rarogsoftware.rarog.platform.api.metrics.MetricSettings;
import eu.rarogsoftware.rarog.platform.api.metrics.MetricsService;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginException;
import eu.rarogsoftware.rarog.platform.core.boot.AsyncBootTask;
import eu.rarogsoftware.rarog.platform.core.boot.BootInfoManager;
import eu.rarogsoftware.rarog.platform.core.plugins.osgi.JarPluginArtifact;
import eu.rarogsoftware.rarog.platform.core.plugins.osgi.OsgiMiddlewareManager;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.HomeDirectoryHelper;
import eu.rarogsoftware.rarog.platform.api.settings.SettingsInitializer;
import eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginManager;
import eu.rarogsoftware.rarog.platform.diagnostics.DiagnosticsManager;
import jakarta.servlet.ServletContext;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys.SYSTEM_ID_KEY;

public class SpringBootManager {
    private static final Logger logger = LoggerFactory.getLogger(SpringBootManager.class);
    private static SpringBootManager bootManager;
    private final BootInfoManager bootInfoManager = new BootInfoManager();
    private final EnumMap<StartState, DiagnosticsManager.MemoryInfo> cachedMemoryInfo = new EnumMap<>(StartState.class);
    private final EnumMap<StartState, Pair<Long, Long>> cachedDuration = new EnumMap<>(StartState.class);
    private final long startMillis = System.currentTimeMillis();
    private StartState startState = StartState.NOT_STARTED;
    private AsyncStartState asyncStartState = AsyncStartState.NOT_STARTED;
    private ConfigurableApplicationContext context;
    private Gauge memoryUsageGauge = null;
    private Gauge memoryPercentUsageGauge = null;
    private Gauge phaseDurationGauge = null;
    private Gauge totalDurationGauge = null;
    private long previousUpdate = System.currentTimeMillis();

    public static synchronized SpringBootManager getInstance() {
        if (bootManager == null) {
            bootManager = new SpringBootManager();
        }
        return bootManager;
    }

    public StartState getStartMode() {
        return startState;
    }

    public AsyncStartState getAsyncStartMode() {
        return asyncStartState;
    }

    public void startInitialized() {
        logger.info("Application starting...");
        updateSyncStartMode(StartState.INITIALIZED);
    }

    public void servletContextStart(ServletContext servletContext) {
        startState = StartState.SERVLET_CONTEXT_STARTED;
        bootInfoManager.registerReplacements(servletContext);
        bootInfoManager.updateStatus(startState, asyncStartState);
        logger.info("Servlet initialized");
    }

    public void postIocContainerStart() {
        logger.info("IoC initialized");
        updateSyncStartMode(StartState.IOC_STARTED);
        initializeSettings();
    }

    private void initializeSettings() {
        var settings = context.getBean(ApplicationSettings.class);
        setupSystemSettings(settings);
        context.getBeansOfType(SettingsInitializer.class).values().forEach(settingsInitializer -> settingsInitializer.initialize(settings));
    }

    private void setupSystemSettings(ApplicationSettings settings) {
        if (!StringUtils.hasText(settings.getPropertyBackedSettingAsString(SYSTEM_ID_KEY))) {
            var propertyId = context.getEnvironment().getProperty("rarog.system.id");
            var id = propertyId != null ? UUID.fromString(propertyId) : UUID.nameUUIDFromBytes(createIdBytes());
            logger.info("Set rarog.system.id to {}", id);
            settings.setSetting(SYSTEM_ID_KEY, id.toString());
        }
        settings.setDefaultSetting(SystemSettingKeys.SELF_CALL_URL, settings.getSettingAsString(SystemSettingKeys.BASE_URL).orElseGet(this::getSelfCallUrl));
    }

    private String getSelfCallUrl() {
        var servletContext = context.getBean(ServletContext.class);
        String host = context.getEnvironment().getProperty("server.address", "127.0.0.1");
        String port = context.getEnvironment().getProperty("server.port", "8080");
        return "http://" + host + ":" + port + "/" + servletContext.getContextPath();
    }

    private byte[] createIdBytes() {
        return Stream.concat(
                        Arrays.stream(context.getEnvironment().getActiveProfiles()),
                        Stream.of(SystemUtils.getHostName(),
                                SystemUtils.getUserName("dev"),
                                SystemUtils.JAVA_VM_INFO,
                                SystemUtils.OS_NAME,
                                SystemUtils.OS_VERSION,
                                SystemUtils.OS_ARCH)
                ).collect(Collectors.joining(","))
                .getBytes(StandardCharsets.UTF_8);
    }

    public SpringApplication decorateSpringApplication(BootManagerDelegatedSpringApplication springApplication) {
        springApplication.setBootManager(this);
        return springApplication;
    }

    public void setContext(ConfigurableApplicationContext context) {
        this.context = context;
    }

    public void blockingStartFinished() {
        updateSyncStartMode(StartState.DATABASE_MIGRATED);
        new Thread(() -> {
            notifyAsyncStart();
            runAsyncPrePluginsTasks();
            if (!startPluginSystem()) {
                return;
            }
            runAsyncPostPluginsTasks();
            notifyStartFinished();
        }).start();
    }

    private void notifyStartFinished() {
        updateAsyncStartMode(AsyncStartState.FULL_STARTED);
        logger.info("Async start finished");
        bootInfoManager.disableReplacements();
        logger.info("Rarog Platform started successfully!");
    }

    private void runAsyncPostPluginsTasks() {
        context.getBeansOfType(AsyncBootTask.class).values().parallelStream()
                .filter(asyncBootTask -> !asyncBootTask.beforePluginsStart())
                .forEach(task -> task.boot(context));
    }

    private boolean startPluginSystem() {
        updateAsyncStartMode(AsyncStartState.PLUGINS_LOADING);
        try {
            context.getBean("MainPluginsSystemManager", OsgiMiddlewareManager.class).startSystem();
            var pluginManager = context.getBean(PluginManager.class);
            var homeDirectoryHelper = context.getBean(HomeDirectoryHelper.class);
            var resourceResolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());
            try {
                var pluginsMap = new HashMap<String, JarPluginArtifact>();
                readPluginFromDirectory(resourceResolver, "classpath:/plugins/*.jar", pluginsMap);
                if (homeDirectoryHelper.getPluginsFile().exists()) {
                    readPluginFromDirectory(resourceResolver, "file://" + homeDirectoryHelper.getPluginsFile().getAbsolutePath() + "/*.jar", pluginsMap);
                }
                ArrayList<Plugin> installedPlugins = new ArrayList<>();
                for (var artifact : pluginsMap.values()) {
                    installedPlugins.add(pluginManager.installPlugin(artifact, false));
                    artifact.close();
                }
                for (var plugin : installedPlugins) {
                    pluginManager.initializePlugin(plugin);
                }

            } catch (IOException | PluginException e) {
                throw new RuntimeException(e);
            }
        } catch (RuntimeException e) {
            logger.error("Failed to start plugin system", e);
            bootInfoManager.setError("Failed to start plugin system", e);
            startState = StartState.FAILED;
            updateAsyncStartMode(AsyncStartState.FAILED);
            return false;
        }
        updateAsyncStartMode(AsyncStartState.PLUGINS_LOADING);
        return true;
    }

    private static void readPluginFromDirectory(PathMatchingResourcePatternResolver resourceResolver, String locationPattern, HashMap<String, JarPluginArtifact> pluginsMap) throws IOException, PluginException {
        logger.info("Reading plugins from {}", locationPattern);
        for (Resource resource : resourceResolver.getResources(locationPattern)) {
            logger.debug("Found resource {}", resource);
            var artifact = new JarPluginArtifact(resource);
            var pluginKey = artifact.getManifest().key();
            logger.debug("Found plugin {}", pluginKey);
            pluginsMap.put(pluginKey, artifact);
        }
    }

    private void runAsyncPrePluginsTasks() {
        context.getBeansOfType(AsyncBootTask.class).values().parallelStream()
                .filter(AsyncBootTask::beforePluginsStart)
                .forEach(task -> task.boot(context));
    }

    private void notifyAsyncStart() {
        updateSyncStartMode(StartState.SYNCH_STARTED);
        logger.info("Blocking start finished");
        logger.info("Starting async start");
        startState = StartState.FULL_STARTED;
        updateAsyncStartMode(AsyncStartState.STARTING);
    }

    private void updateAsyncStartMode(AsyncStartState mode) {
        asyncStartState = mode;
        bootInfoManager.updateStatus(startState, asyncStartState);
        registerUpdate(startState, asyncStartState);
    }

    private void updateSyncStartMode(StartState mode) {
        startState = mode;
        bootInfoManager.updateStatus(startState, asyncStartState);
        registerUpdate(startState, asyncStartState);
    }

    private void registerUpdate(StartState startState, AsyncStartState asyncStartState) {
        var memoryInfo = DiagnosticsManager.getMemoryInfo();
        var phaseDuration = System.currentTimeMillis() - previousUpdate;
        var totalDuration = System.currentTimeMillis() - startMillis;
        previousUpdate = System.currentTimeMillis();
        if (startState == StartState.IOC_STARTED) {
            initializeMetrics();
        }
        if (memoryUsageGauge != null) {
            memoryUsageGauge.labels(startState.toString(), asyncStartState.toString()).set(memoryInfo.used());
            memoryPercentUsageGauge.labels(startState.toString(), asyncStartState.toString()).set(memoryInfo.used() / memoryInfo.max());
            phaseDurationGauge.labels(startState.toString(), asyncStartState.toString()).set(phaseDuration);
            totalDurationGauge.labels(startState.toString(), asyncStartState.toString()).set(totalDuration);
        } else {
            cachedMemoryInfo.put(startState, memoryInfo);
            cachedDuration.put(startState, Pair.of(phaseDuration, totalDuration));
        }
    }

    private void initializeMetrics() {
        memoryUsageGauge = context.getBean(MetricsService.class).createGauge(MetricSettings.settings()
                .name("rarog_startup_memory_usage")
                .description("Memory usage during app startup")
                .labels("startMode", "asyncStartMode")
                .unit("megabytes"));
        memoryPercentUsageGauge = context.getBean(MetricsService.class).createGauge(MetricSettings.settings()
                .name("rarog_startup_memory_percent")
                .description("Percent of total available memory used by app startup")
                .labels("startMode", "asyncStartMode")
                .unit("percent"));
        phaseDurationGauge = context.getBean(MetricsService.class).createGauge(MetricSettings.settings()
                .name("rarog_startup_phase_duration")
                .description("Duration of startup phase")
                .labels("startMode", "asyncStartMode")
                .unit("milliseconds"));
        totalDurationGauge = context.getBean(MetricsService.class).createGauge(MetricSettings.settings()
                .name("rarog_startup_total_duration")
                .description("Duration since start of app")
                .labels("startMode", "asyncStartMode")
                .unit("milliseconds"));
        cachedMemoryInfo.forEach((mode, info) -> {
            memoryUsageGauge.labels(mode.toString(), AsyncStartState.NOT_STARTED.toString()).set(info.used());
            memoryPercentUsageGauge.labels(mode.toString(), AsyncStartState.NOT_STARTED.toString()).set(info.used() / info.max());
        });
        cachedMemoryInfo.clear();
        cachedDuration.forEach((mode, info) -> {
            phaseDurationGauge.labels(mode.toString(), AsyncStartState.NOT_STARTED.toString()).set(info.left());
            totalDurationGauge.labels(mode.toString(), AsyncStartState.NOT_STARTED.toString()).set(info.right());
        });
        cachedDuration.clear();
    }

    public enum StartState {
        NOT_STARTED,
        INITIALIZED,
        SERVLET_CONTEXT_STARTED,
        IOC_STARTED,
        DATABASE_STARTED,
        DATABASE_MIGRATED,
        SYNCH_STARTED,
        FULL_STARTED,
        FAILED
    }

    public enum AsyncStartState {
        NOT_STARTED,
        STARTING,
        PLUGINS_LOADING,
        PLUGINS_LOADED,
        FULL_STARTED,
        FAILED
    }
}
