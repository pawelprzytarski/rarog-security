package eu.rarogsoftware.rarog.platform.core.plugins.web;

import eu.rarogsoftware.commons.utils.Pair;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.web.HandlerMappingDescriptor;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModuleComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.util.ServletRequestPathUtils;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@AutoRegisterFeatureModuleComponent(HandlerMappingDescriptor.class)
public class PluginMappingHandlerMapping implements HandlerMapping, FeatureModule<HandlerMappingDescriptor> {
    public static final String HANDLER_PLUGIN_ATTRIBUTE = "PLUGIN_HANDLER_PLUGIN";
    public static final String PLUGINS_URL_PREFIX = "/plugin/";
    public static final String REST_URL_PREFIX = "/rest/";
    private final Logger logger = LoggerFactory.getLogger(PluginMappingHandlerMapping.class);
    private final Map<String, Pair<Plugin, CollectionMappingHandlerMapping>> handlerMappingMap = new ConcurrentHashMap<>();

    private static HandlerExecutionChain addPluginAddingInterceptor(HandlerExecutionChain handler, Plugin plugin) {
        if (handler == null) {
            return null;
        }
        handler.addInterceptor(new HandlerInterceptor() {
            @Override
            public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
                request.setAttribute(HANDLER_PLUGIN_ATTRIBUTE, plugin);
                return true;
            }

            @Override
            public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
                request.setAttribute(HANDLER_PLUGIN_ATTRIBUTE, null);
            }
        });
        return handler;
    }

    @Override
    public HandlerExecutionChain getHandler(HttpServletRequest request) throws Exception {
        var requestPath = request.getServletPath() + (request.getPathInfo() != null ? request.getPathInfo() : "");
        String pathType = PLUGINS_URL_PREFIX;
        if (requestPath.startsWith(PLUGINS_URL_PREFIX)) {
            requestPath = requestPath.substring(PLUGINS_URL_PREFIX.length() - 1);
        } else if (requestPath.startsWith(REST_URL_PREFIX)) {
            requestPath = requestPath.substring(REST_URL_PREFIX.length() - 1);
            pathType = REST_URL_PREFIX;
        } else {
            return null;
        }
        if (!ServletRequestPathUtils.hasCachedPath(request)) {
            ServletRequestPathUtils.parseAndCache(request);
        }
        try {
            final var finalRequestPath = requestPath;
            final var finalPathType = pathType;

            return handlerMappingMap.entrySet().stream()
                    .filter(entry -> finalRequestPath.startsWith(entry.getKey()))
                    .map(entry -> {
                        try {
                            HandlerExecutionChain handler = entry.getValue().right().getHandlerWithBaseUrl(request, StringUtils.trimTrailingCharacter(finalPathType, '/') + entry.getKey());
                            return addPluginAddingInterceptor(handler, entry.getValue().left());
                        } catch (Exception e) {
                            throw new EncapsulatingException(e);
                        }
                    })
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElse(null);
        } catch (EncapsulatingException e) {
            e.rethrow();
            return null;
        }
    }

    public Map<String, HandlerMapping> getHandlerMappings() {
        return handlerMappingMap.entrySet().stream()
                .flatMap(entry -> entry.getValue().right().handlerMappings.stream().map(handler -> Pair.of(entry.getKey() + "_" + handler.getClass().getName(), handler)))
                .collect(Collectors.toMap(
                        Pair::left,
                        Pair::right
                ));
    }

    @Override
    public void plugDescriptor(Plugin plugin, HandlerMappingDescriptor descriptor) {
        var baseUrl = descriptor.baseUrl().substring(1);
        logger.debug("Registering web handler mapping for plugin {} under {}{}", plugin.getKey(), PLUGINS_URL_PREFIX, baseUrl);
        if (handlerMappingMap.containsKey(descriptor.baseUrl())) {
            logger.error("Failed to register handler mapping for plugin {}. Mapping under url {}{} already exists", plugin.getKey(), PLUGINS_URL_PREFIX, baseUrl);
            throw new IllegalArgumentException("Cannot override already existing url mapping");
        }
        handlerMappingMap.put(descriptor.baseUrl(), Pair.of(plugin, new CollectionMappingHandlerMapping(descriptor.handlerMappings(), descriptor.exactMatchOnly())));
        logger.info("Registered web handler mapping for plugin {} under {}{}", plugin.getKey(), PLUGINS_URL_PREFIX, baseUrl);
    }

    @Override
    public void unplugDescriptor(Plugin plugin, HandlerMappingDescriptor descriptor) {
        handlerMappingMap.compute(descriptor.baseUrl(), (key, pair) -> {
            if (pair == null) {
                logger.warn("Plugin {} attempted to remove mappings under {}", plugin.getKey(), descriptor.baseUrl().substring(1));
                return null;
            }
            if (pair.left().getKey().equals(plugin.getKey())) {
                logger.info("Removed web handler mapping for plugin {} under {}", plugin, descriptor.baseUrl().substring(1));
                return null;
            } else {
                logger.warn("Plugin {} attempted to remove handler mapping of plugin {} under {}. It is not allowed!", plugin.getKey(), pair.left().getKey(), descriptor.baseUrl());
                return pair;
            }
        });
    }

    static class CollectionMappingHandlerMapping implements HandlerMapping {
        private final Collection<HandlerMapping> handlerMappings;
        private final boolean exactMatchOnly;

        CollectionMappingHandlerMapping(Collection<HandlerMapping> handlerMappings, boolean exactMatchOnly) {
            this.handlerMappings = handlerMappings;
            this.exactMatchOnly = exactMatchOnly;
        }

        @Override
        public HandlerExecutionChain getHandler(HttpServletRequest request) throws Exception {
            return getHandlerWithBaseUrl(request, null);
        }

        public HandlerExecutionChain getHandlerWithBaseUrl(HttpServletRequest request, String baseUrl) throws Exception {
            try {
                return handlerMappings.stream()
                        .map(handlerMapping -> {
                            try {
                                HandlerExecutionChain handler = handlerMapping.getHandler(request);
                                if (handler == null && !exactMatchOnly && baseUrl != null) {
                                    return handlerMapping.getHandler(new PluginServletHttpServletRequestWrapper(request, baseUrl));
                                }
                                return handler;
                            } catch (Exception e) {
                                throw new EncapsulatingException(e);
                            }
                        })
                        .filter(Objects::nonNull)
                        .findFirst()
                        .orElse(null);
            } catch (EncapsulatingException e) {
                e.rethrow();
                return null;
            }
        }
    }

    static class EncapsulatingException extends RuntimeException {
        private final Exception encapsulated;

        public EncapsulatingException(Exception cause) {
            super(cause);
            encapsulated = cause;
        }

        void rethrow() throws Exception {
            throw encapsulated;
        }
    }
}

