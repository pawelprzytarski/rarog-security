package eu.rarogsoftware.rarog.platform.core.i18n;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Locale;
import java.util.Optional;

public class UserProvidedValueLocaleResolver extends AcceptHeaderLocaleResolver {
    private static final Logger logger = LoggerFactory.getLogger(UserProvidedValueLocaleResolver.class);

    public UserProvidedValueLocaleResolver() {
        setSupportedLocales(Arrays.asList(new Locale("en"), Locale.forLanguageTag("en-MOON")));
    }

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String paramLang = request.getParameter("lng");
        String cookieLang = Optional.ofNullable(request.getCookies())
                .stream()
                .flatMap(Arrays::stream)
                .filter(cookie -> "X-Chosen-Locale".equalsIgnoreCase(cookie.getName()))
                .map(Cookie::getValue)
                .findFirst().orElse(null);
        return getMatchingLanguage(paramLang)
                .orElseGet(() ->
                        getMatchingLanguage(cookieLang)
                                .orElseGet(() -> {
                                    String headerLang = request.getHeader("Accept-Language");
                                    return headerLang == null || headerLang.isEmpty()
                                            ? Locale.getDefault()
                                            : Locale.lookup(Locale.LanguageRange.parse(headerLang), getSupportedLocales());
                                })
                );
    }

    private Optional<Locale> getMatchingLanguage(String langString) {
        if (langString == null) {
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(Locale.lookup(Locale.LanguageRange.parse(langString), getSupportedLocales()));
        } catch (IllegalArgumentException e) {
            logger.warn("Failed resolving locale: {}", langString, e);
            return Optional.empty();
        }
    }
}
