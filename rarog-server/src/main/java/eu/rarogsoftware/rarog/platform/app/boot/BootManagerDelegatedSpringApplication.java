package eu.rarogsoftware.rarog.platform.app.boot;

import eu.rarogsoftware.rarog.platform.app.configuration.ConfigDirectoryProtocolResolver;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ResourceLoader;

import jakarta.servlet.ServletContext;

public class BootManagerDelegatedSpringApplication extends SpringApplication implements ServletContextInitializer {
    private SpringBootManager bootManager;
    private boolean registerServletInitializer = false;

    public BootManagerDelegatedSpringApplication(Class<?>... primarySources) {
        super(primarySources);
    }

    public BootManagerDelegatedSpringApplication(ResourceLoader resourceLoader, Class<?>... primarySources) {
        super(resourceLoader, primarySources);
    }

    public void setBootManager(SpringBootManager bootManager) {
        this.bootManager = bootManager;
    }

    public void setRegisterServletInitializer(boolean registerServletInitializer) {
        this.registerServletInitializer = registerServletInitializer;
    }

    @Override
    protected ConfigurableApplicationContext createApplicationContext() {
        var applicationContext = super.createApplicationContext();
        applicationContext.addProtocolResolver(new ConfigDirectoryProtocolResolver());
        return applicationContext;
    }

    @Override
    public ConfigurableApplicationContext run(String... args) {
        bootManager.startInitialized();
        ConfigurableApplicationContext context = super.run(args);
        bootManager.blockingStartFinished();
        return context;
    }

    @Override
    protected void refresh(ConfigurableApplicationContext applicationContext) {
        if (registerServletInitializer) {
            applicationContext.getBeanFactory().registerSingleton("BootManagerDelegatedSpringApplication", this);
        }
        super.refresh(applicationContext);
    }

    @Override
    protected void afterRefresh(ConfigurableApplicationContext context, ApplicationArguments args) {
        bootManager.setContext(context);
        bootManager.postIocContainerStart();
    }

    @Override
    public void onStartup(ServletContext servletContext) {
        bootManager.servletContextStart(servletContext);
    }

}
