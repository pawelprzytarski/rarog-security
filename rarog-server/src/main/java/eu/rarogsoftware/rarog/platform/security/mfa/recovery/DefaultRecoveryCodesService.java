package eu.rarogsoftware.rarog.platform.security.mfa.recovery;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

@Component
public class DefaultRecoveryCodesService implements RecoveryCodesService {
    private static final int INITIAL_NUMBER_OF_CODES = 20;
    private static final int MINIMAL_NUMBER_OF_CODES = 4;
    private final RecoveryCodesStore recoveryCodesStore;

    public DefaultRecoveryCodesService(RecoveryCodesStore recoveryCodesStore) {
        this.recoveryCodesStore = recoveryCodesStore;
    }

    @Override
    public RecoveryCodeLoginResult useRecoveryCode(StandardUser user, String recoveryCode) {
        if (recoveryCodesStore.removeCode(user, recoveryCode)) {
            if (recoveryCodesStore.getCodesCount(user) < MINIMAL_NUMBER_OF_CODES) {
                return RecoveryCodeLoginResult.regenerated(generateRecoveryCodes(user));
            } else {
                return RecoveryCodeLoginResult.success();
            }
        }
        return RecoveryCodeLoginResult.failure();
    }

    @Override
    public List<String> generateRecoveryCodes(StandardUser user) {
        var newCodes = IntStream.range(0, INITIAL_NUMBER_OF_CODES)
                .mapToObj(i -> UUID.randomUUID().toString())
                .toList();
        recoveryCodesStore.replaceCodes(user, newCodes);
        return newCodes;
    }
}
