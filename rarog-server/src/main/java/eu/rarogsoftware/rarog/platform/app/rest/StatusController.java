package eu.rarogsoftware.rarog.platform.app.rest;

import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowedLevel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Map;

@RestController
@Tag(name = "Server status")
public class StatusController {
    @GetMapping("status")
    @AnonymousAllowed(AnonymousAllowedLevel.OPTIONAL)
    @Operation(summary = "Get server status", description = "Returns info about current status of server")
    @Parameter(name = "principal", hidden = true)
    @ApiResponse(responseCode = "200")
    public Map<String, String> getStatus(Principal principal) {
        return Map.of(
                "loggedInUser", principal != null ? principal.getName() : "",
                "serverStatus", "Ok"
        );
    }
}
