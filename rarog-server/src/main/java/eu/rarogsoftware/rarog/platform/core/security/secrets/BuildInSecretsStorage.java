package eu.rarogsoftware.rarog.platform.core.security.secrets;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
public class BuildInSecretsStorage {
    private static final Map<String, Map<String, Object>> SECRETS = Map.of(
            "telemetry_clientssl", Map.of(
                    "method", "client_cert",
                    "cert", "classpath:/buildin/telemetry.crt",
                    "privkey", "classpath:/buildin/telemetry.key" // yeah, I know, but it is an open secret anyway. It mostly to secure sent metrics itself and fight against crawlers
            )
    );

    public Optional<Map<String, Object>> getAuthConfigStringForName(String name) {
        return Optional.ofNullable(SECRETS.get(name));
    }
}
