package eu.rarogsoftware.rarog.platform.app.servlets;

import com.mysema.commons.lang.Pair;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

@Controller
public class AboutController {
    private static final Logger logger = LoggerFactory.getLogger(AboutController.class);

    @AnonymousAllowed
    @GetMapping(value = "about/")
    public String about(Model model) {
        var resourceResolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());
        try {
            Resource[] resources = resourceResolver.getResources("classpath:/META-INF/licenses/*.txt");
            Arrays.stream(resources)
                    .filter(Resource::exists)
                    .map(this::openFoundResource)
                    .filter(Objects::nonNull)
                    .forEach(pair -> model.addAttribute(pair.getFirst().replace('-', '_').toLowerCase(Locale.ROOT), pair.getSecond()));
        } catch (IOException e) {
            logger.warn("Failed to load licenses files", e);
        }
        return "admin/about";
    }

    private Pair<String, String> openFoundResource(Resource resource) {
        String filename = resource.getFilename();
        if (filename == null) {
            return null;
        }
        String namespace = filename.substring(0, filename.length() - 4);
        try {
            String content = new String(resource.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
            return Pair.of(namespace, content);
        } catch (IOException ignored) {
            // ignored
        }
        return null;
    }
}
