package eu.rarogsoftware.rarog.platform.core.metrics;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.core.metrics.noop.NoopMetricsService;
import eu.rarogsoftware.rarog.platform.core.metrics.prometheus.PrometheusMetricsService;
import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.function.Function;

import static eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys.METRICS_ENABLED_KEY;

@Primary
@Component
@ExportComponent
public class SwitchingMetricsService implements MetricsService {
    private final ApplicationSettings applicationSettings;
    private final PrometheusMetricsService prometheusMetricsService;
    private final NoopMetricsService noopMetricsService;

    public SwitchingMetricsService(ApplicationSettings applicationSettings,
                                   PrometheusMetricsService prometheusMetricsService) {
        this.applicationSettings = applicationSettings;
        this.prometheusMetricsService = prometheusMetricsService;
        this.noopMetricsService = new NoopMetricsService();
    }

    @Override
    public Counter createCounter(MetricSettings settings) {
        return invokeDelegate(service -> service.createCounter(settings));
    }

    @Override
    public Gauge createGauge(MetricSettings settings) {
        return invokeDelegate(service -> service.createGauge(settings));
    }

    @Override
    public Histogram createHistogram(HistogramSettings settings) {
        return invokeDelegate(service -> service.createHistogram(settings));
    }

    @Override
    public Info createInfo(MetricSettings settings) {
        return invokeDelegate(service -> service.createInfo(settings));
    }

    @Override
    public void closeMetric(BasicMetric metric) {
        invokeDelegate(metricsService -> {
            metricsService.closeMetric(metric);
            return 0;
        });
    }

    @Override
    public MetricsSnapshot snapshot() {
        return invokeDelegate(MetricsService::snapshot);
    }

    @Override
    public Collection<MetricSamples> sample() {
        return invokeDelegate(MetricsService::sample);
    }

    private <T> T invokeDelegate(Function<MetricsService, T> function) {
        if (applicationSettings.getSetting(METRICS_ENABLED_KEY, Boolean.class).orElse(true)) {
            return function.apply(prometheusMetricsService);
        } else {
            return function.apply(noopMetricsService);
        }
    }

    public void resetMetrics() {
        prometheusMetricsService.resetMetrics();
    }
}
