package eu.rarogsoftware.rarog.platform.core.settings;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This {@link SettingsStore} implementation stores setting in memory.
 */
public class InMemorySettingsStore implements SettingsStore {
    Map<String, String> settings = new ConcurrentHashMap<>();

    @Override
    public String getSetting(String key) {
        return settings.get(key);
    }

    @Override
    public void setSetting(String key, String value) {
        if (value == null) {
            settings.remove(key);
        } else {
            settings.put(key, value);
        }
    }
}
