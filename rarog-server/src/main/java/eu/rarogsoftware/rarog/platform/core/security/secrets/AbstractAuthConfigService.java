package eu.rarogsoftware.rarog.platform.core.security.secrets;

import eu.rarogsoftware.rarog.platform.api.security.secrets.AuthConfigService;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;

public abstract class AbstractAuthConfigService<T> implements AuthConfigService<T> {
    protected static final String AUTH_TYPE = "auth";
    protected static final String CERT_TYPE = "client_cert";
    protected static final String BUILDIN_TYPE = "buildin";
    protected static final String VAULT_TYPE = "vault";
    private final Supplier<T> noopResolver;
    private final Map<String, Resolver<T>> resolvers;

    AbstractAuthConfigService(BuildInSecretsStorage buildInSecretsStorage,
                              Supplier<T> noopResolver,
                              Map<String, Resolver<T>> customResolvers) {
        this.noopResolver = noopResolver;
        resolvers = new HashMap<>(customResolvers);
        resolvers.put(VAULT_TYPE, new VaultResolver());
        resolvers.put(BUILDIN_TYPE, new BuildInResolver(buildInSecretsStorage));
    }

    protected void addResolver(String type, Resolver<T> resolver) {
        resolvers.put(type, resolver);
    }

    @Override
    public T resolveFromString(String authenticationData) {
        if (StringUtils.isBlank(authenticationData)) {
            return noopResolver.get();
        }
        var split = authenticationData.split(":", 2);
        var resolver = resolvers.get(split[0].toLowerCase(Locale.ROOT));
        if (resolver == null) {
            throw new IllegalArgumentException("Specified authentication methods is not supported");
        }
        return resolver.resolve(split[1]);
    }

    @Override
    public T resolveFromSettings(Map<String, Object> authenticationData) {
        if (authenticationData == null || authenticationData.isEmpty() || StringUtils.isBlank((String) authenticationData.get("method"))) {
            return noopResolver.get();
        }
        var resolver = resolvers.get(authenticationData.getOrDefault("method", "").toString().toLowerCase(Locale.ROOT));
        if (resolver == null) {
            throw new IllegalArgumentException("Specified authentication methods is not supported");
        }
        return resolver.resolve(authenticationData);
    }

    interface Resolver<U> {
        U resolve(String authData);

        U resolve(Map<String, Object> authData);
    }

    private class BuildInResolver implements Resolver<T> {
        private final BuildInSecretsStorage buildInSecretsStorage;

        public BuildInResolver(BuildInSecretsStorage buildInSecretsStorage) {
            this.buildInSecretsStorage = buildInSecretsStorage;
        }

        @Override
        public T resolve(String name) {
            return buildInSecretsStorage
                    .getAuthConfigStringForName(name)
                    .map(AbstractAuthConfigService.this::resolveFromSettings)
                    .orElseThrow(() -> new IllegalArgumentException("There is not build in credentials for name " + name));
        }

        @Override
        public T resolve(Map<String, Object> authData) {
            return resolve(authData.get("value").toString());
        }
    }

    private class VaultResolver implements Resolver<T> {
        @Override
        public T resolve(String authData) {
            return resolve(Map.of());
        }

        @Override
        public T resolve(Map<String, Object> authData) {
            throw new IllegalArgumentException("Secrets vault is not supported");
        }
    }
}
