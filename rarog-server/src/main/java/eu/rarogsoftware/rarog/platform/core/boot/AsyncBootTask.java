package eu.rarogsoftware.rarog.platform.core.boot;

import org.springframework.context.ApplicationContext;

public interface AsyncBootTask {
    default boolean beforePluginsStart() {
        return false;
    }

    void boot(ApplicationContext context);
}
