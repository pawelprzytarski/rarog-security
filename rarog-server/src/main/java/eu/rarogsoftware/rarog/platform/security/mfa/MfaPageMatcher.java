package eu.rarogsoftware.rarog.platform.security.mfa;

import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaEndpoint;
import eu.rarogsoftware.rarog.platform.core.security.AbstractMethodAnnotatedRequestMatcher;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerMapping;

import java.util.List;
import java.util.Optional;

public class MfaPageMatcher extends AbstractMethodAnnotatedRequestMatcher {
    private final boolean registerOnly;

    public MfaPageMatcher(List<HandlerMapping> handlerMappings, boolean registerOnly) {
        super(handlerMappings, MfaEndpoint.class);
        this.registerOnly = registerOnly;
    }

    @Override
    protected boolean annotationPasses(HandlerMethod method) {
        return getMethodAnnotation(method)
                .filter(mfaEndpoint -> mfaEndpoint.registerOnly() == registerOnly)
                .isPresent();
    }

    private Optional<MfaEndpoint> getMethodAnnotation(HandlerMethod method) {
        MfaEndpoint methodAnnotation = method.getMethodAnnotation(MfaEndpoint.class);
        if (methodAnnotation == null) {
            methodAnnotation = method.getBeanType().getAnnotation(MfaEndpoint.class);
        }
        if (methodAnnotation == null) {
            MergedAnnotation<MfaEndpoint> mergedAnnotation = MergedAnnotations.from(method.getBeanType().getAnnotations()).get(MfaEndpoint.class);
            if (mergedAnnotation.isPresent()) {
                methodAnnotation = mergedAnnotation.synthesize();
            }
        }
        return Optional.ofNullable(methodAnnotation);
    }
}
