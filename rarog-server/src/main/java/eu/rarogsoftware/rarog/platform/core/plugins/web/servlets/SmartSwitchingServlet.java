package eu.rarogsoftware.rarog.platform.core.plugins.web.servlets;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.*;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModuleComponent;
import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.*;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.util.UrlPathHelper;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;

@AutoRegisterFeatureModuleComponent(PluginServletMappingDescriptor.class)
public class SmartSwitchingServlet extends GenericServlet implements FeatureModule<PluginServletMappingDescriptor> {
    private final transient Logger logger = LoggerFactory.getLogger(SmartSwitchingServlet.class);
    private final transient AntPathMatcher antPathMatcher = new AntPathMatcher("/");
    private final transient AtomicLong sequencer = new AtomicLong(0);
    private final transient SortedSet<DescriptorTuple> servlets = new ConcurrentSkipListSet<>(ServletComparator.COMPARATOR);
    private final transient ApplicationSettings applicationSettings;
    private final transient UrlPathHelper urlPathHelper;
    private final transient DispatcherServlet dispatcherServlet;
    private final transient WebMvcProperties webMvcProperties;
    private final Histogram servletsResolutionHistogram;
    private final Histogram servletsSpecialFunctionHistogram;
    private final Gauge activeServletsGauge;
    private transient ServletMapping springDispatcherDescriptor;

    public SmartSwitchingServlet(DispatcherServlet dispatcherServlet,
                                 WebMvcProperties webMvcProperties,
                                 ApplicationSettings applicationSettings,
                                 UrlPathHelper urlPathHelper,
                                 MetricsService metricsService) {
        this.applicationSettings = applicationSettings;
        this.urlPathHelper = urlPathHelper;
        this.dispatcherServlet = dispatcherServlet;
        this.webMvcProperties = webMvcProperties;
        this.servletsResolutionHistogram = metricsService.createHistogram(HistogramSettings.settings()
                .name("rarog_servlets_resolution_duration")
                .unit("s")
                .description("Duration servlets took to serve provided request")
                .buckets(0.001, 0.01, 0.05, 0.1, 0.5, 1, 5, 10, 60, 600)
        );
        this.servletsSpecialFunctionHistogram = metricsService.createHistogram(HistogramSettings.settings()
                .name("rarog_servlets_special_function_resolution_duration")
                .unit("s")
                .labels("special_type")
                .description("Duration servlets took to resolve special interface")
                .buckets(0.001, 0.01, 0.05, 0.1, 0.5, 1, 5, 10, 60, 600)
        );
        this.activeServletsGauge = metricsService.createGauge(MetricSettings.settings()
                .name("rarog_servlets_count")
                .description("Number of enabled servlets")
        );
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
        var servletMapping = webMvcProperties.getServlet().getServletMapping();
        if (servletMapping.equals("") || servletMapping.equals("/") || servletMapping.equals("/*")) {
            servletMapping = "/**";
        }
        springDispatcherDescriptor = new ServletMapping(servletMapping,
                dispatcherServlet);
        servlets.add(new DescriptorTuple("", -1, springDispatcherDescriptor));
        springDispatcherDescriptor.servlet().init(new ServletConfig() {
            @Override
            public String getServletName() {
                return "dispatcherServlet";
            }

            @Override
            public ServletContext getServletContext() {
                return config.getServletContext();
            }

            @Override
            public String getInitParameter(String name) {
                return config.getInitParameter(name);
            }

            @Override
            public Enumeration<String> getInitParameterNames() {
                return config.getInitParameterNames();
            }
        });
    }

    @Override
    public void destroy() {
        super.destroy();
        springDispatcherDescriptor.servlet().destroy();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        HttpServletRequest request;
        HttpServletResponse response;

        try {
            request = (HttpServletRequest) req;
            response = (HttpServletResponse) res;
            doService(request, response);
        } catch (ClassCastException e) {
            throw new ServletException("Non HTTP servlets not supported");
        }
    }

    private void doService(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try (var ignored = servletsResolutionHistogram.startTimer()) {
            var pathWithinApplication = urlPathHelper.getPathWithinApplication(request);
            if (applicationSettings.isTrue(SystemSettingKeys.FORCE_SYSTEM_API)
                    && serviceDescriptor(pathWithinApplication, request, response, springDispatcherDescriptor)) {
                return;
            }

            for (var servletDescriptor : servlets) {
                if (serviceDescriptor(pathWithinApplication, request, response, servletDescriptor.descriptor)) {
                    return;
                }
            }

            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        } catch (IOException | ServletException | RuntimeException e) {
            throw e;
        } catch (Exception e) {
            //ignore because it is thrown by timer
            logger.debug("Timer failed to close correctly", e);
        }
    }

    private boolean serviceDescriptor(String path, HttpServletRequest request, HttpServletResponse response, ServletMapping servletDescriptor) throws ServletException, IOException {
        try {
            if (antPathMatcher.match(servletDescriptor.pathMapping(), path)) {
                servletDescriptor.servlet().service(request, response);
                return true;
            }
        } catch (NoHandlerFoundException |
                 NotHandledException e) {
            // ignore, we are going to process registered servlets
        }
        return false;
    }

    @Override
    public void plugDescriptor(Plugin plugin, PluginServletMappingDescriptor descriptor) {
        descriptor.mappings().forEach(mapping -> {
            logger.info("Added new servlet for mapping {} by plugin {}", mapping.pathMapping(), plugin.getKey());
            initPluginServlet(mapping);
            var newDescriptorPair = new DescriptorTuple(plugin.getKey(), sequencer.getAndIncrement(), mapping);
            logConflicts(newDescriptorPair);
            servlets.add(newDescriptorPair);
            activeServletsGauge.increment();
        });
    }

    private void logConflicts(DescriptorTuple tuple) {
        servlets.stream()
                .filter(other -> other.descriptor.pathMapping().equalsIgnoreCase(tuple.descriptor.pathMapping())
                        || antPathMatcher.match(other.descriptor.pathMapping(), tuple.descriptor.pathMapping()))
                .forEach(other -> {
                    var logMessage = "Servlet {} of plugin {} for mapping {} is overriding servlet {} of plugin {} with mapping {}. "
                            + "Functionality of existing servlet may be not accessible, if newly added servlet do not support "
                            + "passing through.";
                    if (ServletComparator.COMPARATOR.compare(other, tuple) <= 0) {
                        logMessage = "Servlet {} of plugin {} for mapping {} is overridden by servlet {} of plugin {} with mapping {}. "
                                + "Functionality of newly added servlet may be not accessible.";
                    }
                    logger.info(logMessage,
                            tuple.descriptor.servlet().getClass().getName(),
                            tuple.pluginKey,
                            tuple.descriptor.pathMapping(),
                            other.descriptor.servlet().getClass().getName(),
                            other.pluginKey,
                            other.descriptor.pathMapping());
                });
    }

    private void initPluginServlet(ServletMapping mapping) {
        try {
            mapping.servlet().init(new ServletConfig() {
                @Override
                public String getServletName() {
                    return mapping.servlet().getClass().getName();
                }

                @Override
                public ServletContext getServletContext() {
                    return SmartSwitchingServlet.this.getServletContext();
                }

                @Override
                public String getInitParameter(String name) {
                    return mapping.initParameters().get(name);
                }

                @Override
                public Enumeration<String> getInitParameterNames() {
                    return Collections.enumeration(mapping.initParameters().keySet());
                }
            });
        } catch (ServletException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void unplugDescriptor(Plugin plugin, PluginServletMappingDescriptor descriptor) {
        descriptor.mappings().forEach(mapping -> {
            var pathMapping = mapping.pathMapping();
            var pluginKey = plugin.getKey();
            logger.info("Removing servlet of plugin {} for mapping {}", pluginKey, pathMapping);
            if (!servlets.removeIf(pair -> pluginKey.equals(pair.pluginKey)
                    && pathMapping.equals(pair.descriptor.pathMapping()))
            ) {
                logger.warn("Failed to remove descriptor {} for plugin {}. Usually it is caused by developer error", mapping, pluginKey);
                return;
            }
            activeServletsGauge.decrement();
            mapping.servlet().destroy();
            logger.trace("Removed servlet of plugin {} for mapping {}", pluginKey, pathMapping);
        });
    }

    public Optional<AuthorizationManager<RequestAuthorizationContext>> getServletsAuthorizationManager(HttpServletRequest servletRequest) {
        return getValueFromFirstMatchingServlet(servletRequest, AuthorizationManagerAware::getAuthorizationManager, AuthorizationManagerAware.class);
    }

    private <T, U> Optional<T> getValueFromFirstMatchingServlet(HttpServletRequest request, BiFunction<U, HttpServletRequest, Optional<T>> getFunction, Class<U> type) {
        try (var ignore = servletsSpecialFunctionHistogram.labels(type.getSimpleName()).startTimer()) {
            var path = urlPathHelper.getPathWithinApplication(request);
            if (applicationSettings.isTrue(SystemSettingKeys.FORCE_SYSTEM_API)
                    && antPathMatcher.match(springDispatcherDescriptor.pathMapping(), path)) {
                logger.debug("Using default for forced spring servlet");
                return Optional.empty();
            }

            for (var servletDescriptor : servlets) {
                if (servletDescriptor.descriptor() == springDispatcherDescriptor) {
                    logger.debug("Stopping search on spring servlet");
                    return Optional.empty();
                }
                var servlet = servletDescriptor.descriptor().servlet();
                if (antPathMatcher.match(servletDescriptor.descriptor().pathMapping(), path)
                        && type.isAssignableFrom(servlet.getClass())) {
                    var result = getFunction.apply((U) servlet, request);
                    logger.debug("Found result {} on servlet {} of plugin {} for mapping {}",
                            result,
                            servlet.getClass().getName(),
                            servletDescriptor.pluginKey,
                            servletDescriptor.descriptor.pathMapping());
                    if (result.isPresent()) {
                        return result;
                    }
                }
            }

            return Optional.empty();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            //ignore because it is thrown by timer
            logger.debug("Timer failed to close correctly", e);
            return Optional.empty();
        }
    }

    public Optional<Boolean> shouldDisableCsrfProtection(HttpServletRequest servletRequest) {
        return getValueFromFirstMatchingServlet(servletRequest, CsrfProtectionAware::shouldDisableCsrfProtection, CsrfProtectionAware.class);
    }

    record DescriptorTuple(String pluginKey, long sequence, ServletMapping descriptor) {
    }

    private static class ServletComparator implements Comparator<DescriptorTuple> {
        static final ServletComparator COMPARATOR = new ServletComparator();
        private final AntPathMatcher antPathMatcher = new AntPathMatcher("/");

        @Override
        public int compare(DescriptorTuple first, DescriptorTuple second) {
            var orderCompare = Integer.compare(first.descriptor.order(), second.descriptor.order());
            if (orderCompare == 0
                    && !first.descriptor.pathMapping().equalsIgnoreCase(second.descriptor.pathMapping())) {
                if (antPathMatcher.match(first.descriptor.pathMapping(), second.descriptor.pathMapping())) {
                    return 1;
                } else {
                    return -1;
                }
            }
            if (orderCompare == 0) {
                return Long.compare(first.sequence, second.sequence);
            }
            return orderCompare;
        }
    }
}
