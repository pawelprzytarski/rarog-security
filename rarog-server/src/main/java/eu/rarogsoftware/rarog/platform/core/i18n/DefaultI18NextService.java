package eu.rarogsoftware.rarog.platform.core.i18n;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.i18n.I18NextResourceDescriptor;
import eu.rarogsoftware.rarog.platform.api.i18n.I18NextTransformerDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.ResourceHoldingPlugin;
import eu.rarogsoftware.rarog.platform.core.common.JsonMergeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.data.util.Pair;
import org.springframework.lang.Nullable;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Implementation of I18NextService that supports {@link I18NextTransformerDescriptor} and {@link I18NextResourceDescriptor}.
 * This implementation loads resources, merges them and then apply transformers on result of merging. Because of that
 * this implementation supports only json formats.
 */
public class DefaultI18NextService implements FeatureModule<FeatureDescriptor>, I18NextService {
    private final Logger logger = LoggerFactory.getLogger(DefaultI18NextService.class);
    private static final String RESOURCES_BASE_NAME = "messages";
    private final Map<String, Pair<ResourceHoldingPlugin, I18NextResourceDescriptor>> resourcesDescriptors = new ConcurrentHashMap<>();
    private final Map<String, I18NextTransformerDescriptor> transformerDescriptors = new ConcurrentHashMap<>();
    private volatile long lastUpdate = System.currentTimeMillis();
    private String parentDirectory = "";
    private String defaultEncoding;

    @Override
    public void setParentDirectory(String parentDirectory) {
        this.parentDirectory = parentDirectory;
    }

    @Override
    @Nullable
    public String getDefaultEncoding() {
        return defaultEncoding;
    }

    @Override
    public void setDefaultEncoding(@Nullable String defaultEncoding) {
        this.defaultEncoding = defaultEncoding;
    }

    @Override
    public long getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void plugDescriptor(Plugin plugin, FeatureDescriptor descriptor) {
        if (descriptor instanceof I18NextResourceDescriptor resourceDescriptor) {
            if (plugin instanceof ResourceHoldingPlugin resourcePlugin) {
                logger.debug("Plugin {} exports translations using descriptor: {}", plugin.getKey(), descriptor);
                resourcesDescriptors.put(getKey(plugin, resourceDescriptor), Pair.of(resourcePlugin, resourceDescriptor));
            } else {
                logger.error("Tried to plug descriptor {} from plugin {} which don't provide resources", descriptor.getClass(), plugin.getKey());
                throw new IllegalArgumentException("Plugin, which is not capable of providing resources, declares i18Next resources");
            }
        } else if (descriptor instanceof I18NextTransformerDescriptor transformerDescriptor) {
            logger.debug("Plugin {} registers translation transformer using descriptor: {}", plugin.getKey(), descriptor);
            transformerDescriptors.put(getKey(plugin, transformerDescriptor), transformerDescriptor);
        } else {
            logger.error("Tried to plug descriptor {} to {} which doesn't support his type of descriptor", descriptor.getClass(), getClass());
            throw new IllegalArgumentException("Invoked plugDescriptor for unknown descriptor type");
        }
        logger.trace("Previous update {}", lastUpdate);
        lastUpdate = System.currentTimeMillis();
    }

    private static String getKey(Plugin plugin, FeatureDescriptor resourceDescriptor) {
        return plugin.getKey() + "-" + resourceDescriptor.getName();
    }

    @Override
    public void unplugDescriptor(Plugin plugin, FeatureDescriptor descriptor) {
        if (descriptor instanceof I18NextResourceDescriptor resourceDescriptor) {
            logger.debug("Plugin {} unexports translations using descriptor: {}", plugin.getKey(), descriptor);
            resourcesDescriptors.remove(getKey(plugin, resourceDescriptor));
        } else if (descriptor instanceof I18NextTransformerDescriptor transformerDescriptor) {
            logger.debug("Plugin {} unregisters translation transformer using descriptor: {}", plugin.getKey(), descriptor);
            resourcesDescriptors.remove(getKey(plugin, transformerDescriptor));
        } else {
            logger.error("Tried to unplug descriptor {} from {} which doesn't support his type of descriptor", descriptor.getClass(), getClass());
            throw new IllegalArgumentException("Invoked unplugDescriptor for unknown descriptor type");
        }
        logger.trace("Previous update {}", lastUpdate);
        lastUpdate = System.currentTimeMillis();
    }

    @Override
    public I18NextNamespacedResourceBundle getResourceBundle(String baseName, Locale locale, ClassLoader loader, boolean reload) throws IOException {
        var resourceName = getResourceName(baseName, locale, "*");
        logger.debug("Looking for translations under {}", resourceName);
        Map<String, ResourceBundle> bundles = loadNamespacedResources(baseName, locale, loader, resourceName, reload);

        if (!bundles.isEmpty()) {
            logger.info("Loaded translations for {}. Found namespaces: {}", locale, bundles.keySet().size());
            return new I18NextNamespacedResourceBundle(bundles);
        }
        logger.info("Failed to find any translations for {}", locale);
        return null;
    }

    private Map<String, ResourceBundle> loadNamespacedResources(String baseName, Locale locale, ClassLoader loader, String resourceName, boolean reload) throws IOException {
        Map<String, ResourceBundle> bundles = new HashMap<>();
        var appResources = getAppTranslations(baseName, loader, resourceName, reload);
        logger.debug("Found {} app translations", appResources.keySet().size());
        var allResources = getPluginResources(baseName, reload, resourceName.substring("i18n/".length()), appResources);
        logger.debug("Found {} all translations", allResources.keySet().size());

        for (var resource : allResources.entrySet()) {
            List<InputStream> translations = resource.getValue();
            if (translations.size() == 1) {
                bundles.put(resource.getKey(), readBundle(resource.getKey(), locale, translations.get(0)));
            } else {
                logger.debug("Merging {} resources for namespace {}", translations.size(), resource.getKey());
                bundles.put(resource.getKey(), readBundle(resource.getKey(), locale, JsonMergeUtils.mergeJsons(translations.toArray(new InputStream[0]))));
            }
        }
        return bundles;
    }

    private Map<String, List<InputStream>> getPluginResources(String baseName, boolean reload, String resourcePattern, Map<String, InputStream> appResources) {
        return Stream.concat(resourcesDescriptors.values()
                                .stream()
                                .sorted(Comparator.comparingInt(pair -> pair.getSecond().order()))
                                .map(pair -> pair.getFirst().findResources(cleanI18NextLocation(pair.getSecond().location()) + resourcePattern))
                                .flatMap(resources -> openResources(baseName, reload, resources.toArray(new Resource[0])).entrySet().stream()),
                        appResources.entrySet().stream()
                )
                .collect(Collectors.toMap(Map.Entry::getKey, resource -> new ArrayList<>(Collections.singleton(resource.getValue())), (list1, list2) -> {
                    list1.addAll(list2);
                    return list1;
                }));
    }

    private static String cleanI18NextLocation(String directory) {
        if (!directory.startsWith("/")) {
            directory = "/" + directory;
        }
        if (directory.endsWith("/")) {
            return directory;
        }
        return directory + "/";
    }

    private Map<String, InputStream> getAppTranslations(String baseName, ClassLoader loader, String resourceName, boolean reload) throws IOException {
        var resourceResolver = new PathMatchingResourcePatternResolver(loader);
        Resource[] resources = resourceResolver.getResources(resourceName);
        return openResources(baseName, reload, resources);
    }

    private Map<String, InputStream> openResources(String baseName, boolean reload, Resource[] resources) {
        return Arrays.stream(resources)
                .filter(Resource::exists)
                .map(resource -> openFoundResource(baseName, reload, resource))
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(Pair::getFirst, Pair::getSecond));
    }

    private ResourceBundle readBundle(String key, Locale locale, InputStream inputStream) throws IOException {
        ResourceBundle resourceBundle;
        var transformedInputStream = transformTranslationResource(key, locale, inputStream);
        if (getDefaultEncoding() != null) {
            try (var bundleReader = new InputStreamReader(transformedInputStream, getDefaultEncoding() == null ? Charset.defaultCharset() : Charset.forName(getDefaultEncoding()))) {
                resourceBundle = loadBundle(bundleReader);
            }
        } else {
            try (var bundleStream = inputStream) {
                resourceBundle = loadBundle(transformedInputStream);
            }
        }
        return resourceBundle;
    }

    private InputStream transformTranslationResource(String key, Locale locale, InputStream inputStream) throws IOException {
        if (transformerDescriptors.values().isEmpty()) {
            return inputStream;
        }
        var charset = getDefaultEncoding() == null ? Charset.defaultCharset() : Charset.forName(getDefaultEncoding());
        var json = new String(inputStream.readAllBytes(), charset);
        var transformedJson = transformerDescriptors.values()
                .stream()
                .sorted(Comparator.comparingInt(I18NextTransformerDescriptor::order))
                .map(I18NextTransformerDescriptor::i18NextTransformer)
                .reduce(json, (str, transformer) -> transformer.transform(key, locale, str), (str1, str2) -> str2);
        return new ByteArrayInputStream(transformedJson.getBytes(charset));
    }


    private ResourceBundle loadBundle(Reader reader) throws IOException {
        return new I18NextResourceBundle(reader);
    }

    private ResourceBundle loadBundle(InputStream inputStream) throws IOException {
        return new I18NextResourceBundle(inputStream);
    }

    @SuppressFBWarnings(value = "URLCONNECTION_SSRF_FD", justification = "these are cause by opening url provided by spring resources")
    private Pair<String, InputStream> openFoundResource(String baseName, boolean reload, Resource resource) {
        var filename = resource.getFilename();
        if (filename == null) {
            return null;
        }
        var namespace = filename.substring(baseName.length() + 1, filename.length() - 5);
        try {
            if (reload) {
                var url = resource.getURL();
                var connection = url.openConnection();
                if (connection != null) {
                    connection.setUseCaches(false);
                    return Pair.of(namespace, connection.getInputStream());
                }
            } else {
                return Pair.of(namespace, resource.getInputStream());
            }

        } catch (IOException e) {
            logger.warn("Failed to read resource", e);
        }
        return null;
    }

    private String getResourceName(String baseName, Locale locale, String namespace) {
        var sb = new StringBuilder(parentDirectory);

        if (locale != Locale.ROOT) {

            String language = locale.getLanguage();
            String script = locale.getScript();
            String country = locale.getCountry();
            String variant = locale.getVariant();

            if (Objects.equals(language, "") && Objects.equals(country, "") && Objects.equals(variant, "")) {
                return baseName;
            }
            sb.append('/');
            if (!script.equals("")) {
                if (!variant.equals("")) {
                    sb.append(language).append('/').append(script).append('/').append(country).append('/').append(variant);
                } else if (!country.equals("")) {
                    sb.append(language).append('/').append(script).append('/').append(country);
                } else {
                    sb.append(language).append('/').append(script);
                }
            } else {
                if (!variant.equals("")) {
                    sb.append(language).append('/').append(country).append('/').append(variant);
                } else if (!country.equals("")) {
                    sb.append(language).append('/').append(country);
                } else {
                    sb.append(language);
                }
            }
        }
        sb.append('/').append(baseName).append("_").append(namespace).append(".json");
        return sb.toString();
    }

    @Override
    public InputStream getI18NextResource(Locale locale, String namespace) throws IOException {
        logger.debug("Looking for {} translations for {}", namespace, locale);
        var resourceName = getResourceName(RESOURCES_BASE_NAME, locale, namespace);
        var resourceResolver = new PathMatchingResourcePatternResolver();
        List<Resource> resources = new ArrayList<>();
        resourcesDescriptors.values()
                .stream()
                .sorted(Comparator.comparingInt(pair -> pair.getSecond().order()))
                .map(pair -> pair.getFirst().getResource(cleanI18NextLocation(pair.getSecond().location()) + resourceName.substring("i18n/".length())))
                .filter(Objects::nonNull)
                .forEach(resources::add);

        var appResource = resourceResolver.getResource(resourceName);
        if (appResource.exists()) {
            logger.trace("Found app translation");
            resources.add(appResource);
        }

        logger.debug("Found {} resources", resources.size());

        InputStream resultStream = null;
        if (resources.size() == 1) {
            resultStream = resources.get(0).getInputStream();
        } else if (!resources.isEmpty()) {
            logger.debug("Merging {} resources", resources.size());
            List<InputStream> list = new ArrayList<>();
            for (Resource resource : resources) {
                list.add(resource.getInputStream());
            }
            resultStream = JsonMergeUtils.mergeJsons(list.toArray(new InputStream[0]));
        }
        if (resultStream != null) {
            return transformTranslationResource(namespace, locale, resultStream);
        }
        return null;
    }
}
