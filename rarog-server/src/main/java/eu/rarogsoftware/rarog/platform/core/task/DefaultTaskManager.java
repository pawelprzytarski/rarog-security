package eu.rarogsoftware.rarog.platform.core.task;

import eu.rarogsoftware.rarog.platform.api.metrics.MetricsService;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.task.AdvancedScheduledExecutorService;
import eu.rarogsoftware.rarog.platform.api.task.ScheduleConfig;
import eu.rarogsoftware.rarog.platform.api.task.TaskManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;

import static eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys.*;

@Component
@ExportComponent
public class DefaultTaskManager implements TaskManager {
    private final Logger logger = LoggerFactory.getLogger(DefaultTaskManager.class);
    private final ThreadFactory threadFactory;
    private final AdvancedScheduledExecutorService threadPoolExecutor;
    private final AdvancedScheduledExecutorService monitoredThreadPoolExecutor;
    private final ApplicationSettings applicationSettings;

    public DefaultTaskManager(MetricsService metricsService,
                              ApplicationSettings applicationSettings) {
        this.applicationSettings = applicationSettings;
        applicationSettings.setDefaultSetting(TASK_MANAGER_MAX_POOL_SIZE, Integer.MAX_VALUE);
        this.threadFactory = new MonitoredThreadFactory(metricsService);
        threadPoolExecutor = getThreadPoolExecutor(applicationSettings);
        monitoredThreadPoolExecutor = new MonitoredScheduledThreadPoolExecutor(metricsService, threadPoolExecutor);
    }

    private AdvancedScheduledExecutorService getThreadPoolExecutor(ApplicationSettings applicationSettings) {
        Integer maxPoolSize = applicationSettings.getSettingOrDefault(TASK_MANAGER_MAX_POOL_SIZE, Integer.class);
        logger.info("Creating thread pool with max size: {}", maxPoolSize);
        return new AdvancedScheduledThreadPoolExecutor(maxPoolSize, threadFactory);
    }

    @Override
    public Thread createThread(Runnable task) {
        return threadFactory.newThread(task);
    }

    @Override
    public Future<?> runTask(Runnable task) {
        return getThreadPoolExecutor().submit(task);
    }

    private AdvancedScheduledExecutorService getThreadPoolExecutor() {
        if (applicationSettings.isTrue(METRICS_ENABLED_KEY)) {
            return monitoredThreadPoolExecutor;
        } else {
            return threadPoolExecutor;
        }
    }

    @Override
    public <T> Future<T> runTask(Callable<T> task) {
        return getThreadPoolExecutor().submit(task);
    }

    @Override
    public ScheduledFuture<?> scheduleTask(Runnable task, ScheduleConfig<Void> scheduleConfig) {
        return getThreadPoolExecutor().scheduleAdvanced(task, scheduleConfig);
    }

    @Override
    public <T> ScheduledFuture<T> scheduleTask(Callable<T> task, ScheduleConfig<T> scheduleConfig) {
        return getThreadPoolExecutor().scheduleAdvanced(task, scheduleConfig);
    }

    @Override
    public ThreadFactory asThreadFactory() {
        return threadFactory;
    }

    @Override
    public AdvancedScheduledExecutorService asExecutorService() {
        return getThreadPoolExecutor();
    }
}
