package eu.rarogsoftware.rarog.platform.core.plugins.web.filters;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;

import org.springframework.stereotype.Component;

/**
 * This is a special {@link Filter} responsible for invoking Filters registered by the plugins.
 */
@Component
public class PluginFilterExecutorFilter implements Filter {

    PluginFilterResolver filterResolver;

    PluginFilterExecutorFilter(PluginFilterResolver filterResolver) {
        this.filterResolver = filterResolver;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        PluginFilterChain filterChain = filterResolver.getFilterChainForRequest(request, chain);
        filterChain.doFilter(request, response);
    }
}
