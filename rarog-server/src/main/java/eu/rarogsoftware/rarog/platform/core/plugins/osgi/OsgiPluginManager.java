package eu.rarogsoftware.rarog.platform.core.plugins.osgi;

import eu.rarogsoftware.rarog.platform.api.plugins.*;
import eu.rarogsoftware.rarog.platform.api.settings.HomeDirectoryHelper;
import eu.rarogsoftware.rarog.platform.core.plugins.PluggableFeatureRegistry;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginArtifact;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginManager;
import org.apache.commons.io.FileUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.wiring.BundleWiring;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;

import static eu.rarogsoftware.rarog.platform.api.plugins.Plugin.PluginState;
import static eu.rarogsoftware.rarog.platform.api.plugins.Plugin.PluginState.*;
import static java.io.File.pathSeparatorChar;

/**
 * Plugin manager implementation that is used to manage OSGi based plugins.
 */
@Component
@DependsOn({"MainPluginsSystemManager"})
public class OsgiPluginManager implements PluginManager, OsgiSystemBundleListener {
    private static final int DEFAULT_WAITING_TIME = 25;
    private static final int PURGE_TIMEOUT = 120;
    private static final int ENABLING_TIMEOUT = DEFAULT_WAITING_TIME;
    private final Logger logger = LoggerFactory.getLogger(OsgiPluginManager.class);
    private final Map<String, OsgiPlugin> installedPlugins = new HashMap<>();
    private final OsgiPluginRegistry pluginRegistry = new OsgiPluginRegistry();
    private final Map<String, String> installedPluginsLocations = new HashMap<>();
    private final ExecutorService executorService = new ForkJoinPool();
    private final PluggableFeatureRegistry featureRegistry;
    private final PluginsStateStore stateStore;
    private final HomeDirectoryHelper homeDirectoryHelper;
    private BundleContext systemBundle;

    public OsgiPluginManager(PluggableFeatureRegistry featureRegistry, PluginsStateStore stateStore, HomeDirectoryHelper homeDirectoryHelper) {
        this.featureRegistry = featureRegistry;
        this.stateStore = stateStore;
        this.homeDirectoryHelper = homeDirectoryHelper;
    }


    @Override
    public void systemBundledStarted(BundleContext context) throws Exception {
        systemBundle = context;
        logger.debug("Initialized with context: {}", context);
    }

    @Override
    public void systemBundledStopped(BundleContext context) {
        logger.debug("Stopped for context {}", context);
        systemBundle = null;
    }

    @Override
    public PluginArtifact savePluginArtifactToHome(PluginArtifact artifact) throws PluginException, IOException {
        var jarArtifact = validatePluginResource(artifact);
        var newPluginArtifactFile = Path.of(getPluginArtifactJarPath(jarArtifact.getManifest())).toFile();
        FileUtils.writeByteArrayToFile(newPluginArtifactFile, jarArtifact.getInputStream().readAllBytes());
        return new JarPluginArtifact(new FileSystemResource(newPluginArtifactFile));
    }

    private String getPluginArtifactJarPath(PluginManifest pluginManifest) {
        var pluginsDirectory = homeDirectoryHelper.getPluginsFile();
        return pluginsDirectory.getPath()
                + File.separator
                + pluginManifest.key().replace(pathSeparatorChar, '-')
                + "-"
                + pluginManifest.version() + ".jar";
    }

    @Override
    public void deletePluginArtifactFromHome(Plugin plugin) {
        var artifactFile = Path.of(getPluginArtifactJarPath(plugin.getManifest())).toFile();
        if (artifactFile.exists()) {
            if(!artifactFile.delete()) {
                logger.warn("Couldn't delete {} when uninstalling plugin.", artifactFile);
            }
        }
    }

    @Override
    public void validatePluginArtifact(PluginArtifact pluginArtifact) throws PluginException, IOException {
        var jarArtifact = validatePluginResource(pluginArtifact);
        validateManifest(jarArtifact.getId(), jarArtifact.getManifest());
    }

    @Override
    public synchronized Plugin installPlugin(PluginArtifact pluginArtifact, boolean storeState) throws IOException, PluginException {
        var plugin = installPluginWithoutStoringState(pluginArtifact);
        if (storeState) {
            stateStore.storePluginState(plugin, INSTALLED);
        }
        return plugin;
    }

    private OsgiPlugin installPluginWithoutStoringState(PluginArtifact pluginArtifact) throws PluginException, IOException {
        validateOsgiState();
        var jarArtifact = validatePluginResource(pluginArtifact);
        validatePluginNotEnabled(jarArtifact);
        var manifest = jarArtifact.getManifest();
        validateManifest(jarArtifact.getId(), manifest);
        uninstallOldPlugin(manifest);
        return installOsgiPlugin(jarArtifact, manifest);
    }

    @Override
    public synchronized Plugin initializePlugin(Plugin plugin) throws PluginException {
        var lastPluginState = stateStore.getPluginState(plugin);
        if (lastPluginState.isEmpty() || lastPluginState.get().equals(ENABLED)) {
            enablePlugin(plugin);
        } else {
            disablePlugin(plugin);
        }
        return plugin;
    }

    private void uninstallOldPlugin(PluginManifest manifest) throws PluginException {
        var oldPlugin = installedPlugins.get(manifest.key());
        if (oldPlugin == null) {
            return;
        }
        if (oldPlugin.isInUse()) {
            logger.warn("Cannot reinstall plugin which is in use");
            throw new PluginException("Plugin in use. Cannot reinstall");
        }
        logger.info("Uninstalling previous version of plugin {}", manifest.key());
        uninstall(oldPlugin);
    }

    private JarPluginArtifact validatePluginResource(PluginArtifact pluginArtifact) throws FileNotFoundException, PluginException {
        if (!(pluginArtifact instanceof JarPluginArtifact)) {
            logger.warn("Artifact {} is not valid type. Only OSGi Jar Plugins are supported!", pluginArtifact.getId());
            throw new PluginException("Artifact %s is not valid type".formatted(pluginArtifact.getId()));
        }
        if (!pluginArtifact.exists()) {
            logger.warn("Attempted to install not existing plugin {}", pluginArtifact.getId());
            throw new FileNotFoundException("Plugin under URI %s doesn't exist".formatted(pluginArtifact.getId()));
        }
        return (JarPluginArtifact) pluginArtifact;
    }

    private void validatePluginNotEnabled(JarPluginArtifact artifact) throws PluginException {
        String installedKey = installedPluginsLocations.get(artifact.getId());
        if (installedKey != null) {
            var osgiPlugin = installedPlugins.get(installedKey);
            if (osgiPlugin != null && osgiPlugin.isInUse()) {
                logger.warn("Cannot reinstall enabled plugin. Disable plugin before reinstalling it");
                throw new PluginException("Plugin %s is enabled. Cannot install".formatted(osgiPlugin.getKey()));
            }
        }
    }

    private OsgiPlugin installOsgiPlugin(JarPluginArtifact jarArtifact, PluginManifest manifest) throws IOException, PluginException {
        logger.info("Installing plugin from URI: {}", jarArtifact.getId());
        try (var inputStream = jarArtifact.getInputStream()) {
            Bundle bundle = systemBundle.installBundle(jarArtifact.getId(), inputStream);
            String bundleName = bundle.getHeaders().get("Bundle-Name");
            logger.debug("Plugin {} from URI {} installed to OSGi", bundleName, jarArtifact.getId());

            logger.info("Installed plugin {} with key {}", manifest.name(), manifest.key());
            installedPluginsLocations.put(jarArtifact.getId(), manifest.key());
            OsgiPlugin newPlugin = new OsgiPlugin(manifest, bundle);
            systemBundle.addBundleListener(newPlugin);
            installedPlugins.put(manifest.key(), newPlugin);
            pluginRegistry.registerMapping(newPlugin.getBundle(), newPlugin);
            return newPlugin;
        } catch (BundleException e) {
            logger.warn("Failed to install plugin {}", jarArtifact, e);
            throw new PluginException(e);
        }
    }

    private void validateManifest(String pluginId, PluginManifest manifest) throws PluginException {
        logger.trace("Validating manifest of plugin {}: {}", pluginId, manifest);
        try {
            Objects.requireNonNull(manifest.key(), "Key cannot be null");
            Objects.requireNonNull(manifest.version(), "Version cannot be null");
            Objects.requireNonNull(manifest.name(), "Name cannot be null");
            if (!StringUtils.hasText(manifest.key())) {
                throw new NullPointerException("Key cannot be empty");
            }
            if (!StringUtils.hasText(manifest.version())) {
                throw new NullPointerException("Version cannot be empty");
            }
            if (!StringUtils.hasText(manifest.name())) {
                throw new NullPointerException("Name cannot be empty");
            }
            logger.trace("Manifest of plugin {}->{} validated", pluginId, manifest.key());
        } catch (NullPointerException e) {
            logger.warn("Plugin read from URL {} is not valid rarog plugin. Manifest is incomplete", pluginId);
            throw new PluginException(e);
        }
    }

    private void validateOsgiState() {
        if (systemBundle == null) {
            logger.error("Attempted to install plugin before OSGi initialized");
            throw new IllegalStateException("OSGi middleware not initiated");
        }
    }

    @Override
    public synchronized void uninstall(Plugin plugin) throws PluginException {
        var osgiPlugin = installedPlugins.get(plugin.getKey());
        if (osgiPlugin.isInUse()) {
            logger.warn("Cannot uninstall plugin which is in use");
            throw new PluginException("Plugin in use. Cannot uninstall");
        }
        installedPlugins.remove(osgiPlugin.getKey());
        installedPluginsLocations.values().removeIf(key -> key.equals(osgiPlugin.getKey()));
        pluginRegistry.unregisterMapping(osgiPlugin.getBundle());
        try {
            osgiPlugin.getBundle().uninstall();
            deletePluginArtifactFromHome(plugin);
            stateStore.storePluginState(plugin, UNINSTALLED);
        } catch (BundleException e) {
            logger.error("Failed to uninstall plugin {}", plugin.getKey(), e);
            throw new PluginException("Failed to uninstall plugin.", e);
        }
    }

    @Override
    public synchronized Plugin enablePlugin(Plugin plugin) throws PluginException {
        validateOsgiState();
        var osgiPlugin = installedPlugins.get(plugin.getKey());
        if (osgiPlugin == null) {
            logger.warn("Tried enable not installed plugin {}", plugin.getKey());
            throw new PluginException("Plugin not installed");
        }
        if (osgiPlugin.isInUse()) {
            logger.info("Plugin {} already enabled!", plugin.getKey());
            return osgiPlugin;
        }
        return enableOsgiPlugin(osgiPlugin);
    }

    private OsgiPlugin enableOsgiPlugin(OsgiPlugin osgiPlugin) throws PluginException {
        return enableOsgiPlugin(osgiPlugin, new HashSet<>());
    }

    private OsgiPlugin enableOsgiPlugin(OsgiPlugin osgiPlugin, Set<OsgiPlugin> visitedPlugins) throws PluginException {
        logger.info("Enabling plugin {}", osgiPlugin.getKey());
        visitedPlugins.add(osgiPlugin);
        startOsgiBundle(osgiPlugin);
        enableDependencies(osgiPlugin, visitedPlugins);
        activatePlugin(osgiPlugin);
        stateStore.storePluginState(osgiPlugin, ENABLED);
        logger.info("Successfully enabled plugin {}", osgiPlugin.getKey());
        return osgiPlugin;
    }

    private void startOsgiBundle(OsgiPlugin osgiPlugin) throws PluginException {
        if (osgiPlugin.isInUse()) {
            logger.info("Plugin {} already enabled", osgiPlugin.getKey());
            return;
        }
        waitForMethod(osgiPlugin, ENABLED, osgiPlugin::enable);
        if (!osgiPlugin.isEnabled() && !osgiPlugin.isActive()) {
            logger.error("Plugin {} failed to start on time", osgiPlugin.getKey());
            throw new PluginException("Failed to start plugin");
        }
        logger.debug("Successfully started OSGi bundle for plugin {}", osgiPlugin.getKey());
    }

    private void activatePlugin(OsgiPlugin osgiPlugin) throws PluginException {
        if (osgiPlugin.isActive()) {
            logger.info("Plugin {} already active", osgiPlugin.getKey());
            return;
        }
        var activator = osgiPlugin.getPluginActivator();
        if (activator == null) {
            return;
        }
        logger.info("Activating plugin {}", osgiPlugin.getKey());
        var task = executorService.submit(() -> {
            try {
                activator.activate();
                var descriptors = activator.getFeatureDescriptors();
                if (descriptors != null) {
                    featureRegistry.plugDescriptors(osgiPlugin, descriptors);
                }
                osgiPlugin.markActive();
            } catch (RuntimeException e) {
                logger.error("Failed to execute late startup for plugin {}", osgiPlugin.getKey(), e);
                throw e;
            }
        });

        try {
            task.get(ENABLING_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Failed to remove plugin from system. Disabling plugin.");
            try {
                disablePlugin(osgiPlugin);
            } catch (RuntimeException | PluginException deactivateException) {
                logger.error("Failed to disable plugin {} after activation error", osgiPlugin.getKey(), deactivateException);
            }
            if (e instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            }
            throw new PluginException("Activating plugin failed", e);
        } catch (TimeoutException e) {
            logger.error("Failed to remove failed plugin from system in 25 seconds. Disabling OSGi bundle anyway.");
            throw new PluginException("Plugin activation failed", e);
        }
    }

    private void enableDependencies(OsgiPlugin osgiPlugin, Set<OsgiPlugin> visitedPlugins) throws PluginException {
        logger.info("Enabling dependencies of plugin {}", osgiPlugin.getKey());
        for (var dependencyPlugin : osgiPlugin.getBundle().adapt(BundleWiring.class)
                .getRequiredWires(null).stream()
                .map(bundleWire -> bundleWire.getProvider().getBundle())
                .map(pluginRegistry::getPlugin)
                .filter(Objects::nonNull)
                .filter(dependency -> !dependency.isActive() && dependency.canBeEnabled() && !visitedPlugins.contains(dependency))
                .toList()) {
            enableOsgiPlugin(dependencyPlugin, visitedPlugins);
        }
    }

    private void waitForMethod(OsgiPlugin osgiPlugin, PluginState waitFor, PluginRunnable method) throws PluginException {
        Semaphore semaphore = new Semaphore(1);
        PluginStateListener pluginStateListener = (oldState, newState) -> {
            if (newState == waitFor) {
                semaphore.release();
            }
        };
        osgiPlugin.addPluginStateListener(pluginStateListener);

        try {
            semaphore.acquire();

            var futureTask = executorService.submit(() -> {
                try {
                    method.run();
                } catch (PluginException e) {
                    semaphore.release();
                    return e;
                }
                return null;
            });

            int waitingTime = DEFAULT_WAITING_TIME;
            while (waitingTime >= 0) {
                logger.info("Waiting for plugin {} for {} seconds", osgiPlugin.getManifest().key(), waitingTime);
                if (!semaphore.tryAcquire(1, TimeUnit.SECONDS)) {
                    waitingTime--;
                } else {
                    break;
                }
            }
            if (!futureTask.isDone()) {
                futureTask.cancel(true);
            } else {
                var exception = futureTask.get();
                if (exception != null) {
                    throw exception;
                }
            }

        } catch (InterruptedException e) {
            logger.error("Cannot enable OSGi plugin. Interrupted.");
            Thread.currentThread().interrupt();
            throw new PluginException(e);
        } catch (ExecutionException e) {
            logger.error("Cannot enable OSGi plugin. Enabling threw unexpected exception");
            throw new PluginException(e);
        }
        osgiPlugin.removePluginStateListener(pluginStateListener);
    }

    @Override
    public synchronized Plugin disablePlugin(Plugin plugin) throws PluginException {
        return disablePlugin(plugin, true);
    }

    @Override
    public Plugin disablePlugin(Plugin plugin, boolean storeState) throws PluginException {
        validateOsgiState();
        var osgiPlugin = installedPlugins.get(plugin.getKey());
        if (osgiPlugin == null) {
            logger.warn("Tried disable not installed plugin {}", plugin.getKey());
            throw new PluginException("Plugin not installed");
        }
        if (!osgiPlugin.isInUse()) {
            logger.warn("Plugin {} already disabled!", plugin.getKey());
            return osgiPlugin;
        }
        return disableOsgiPlugin(osgiPlugin, storeState);
    }

    private OsgiPlugin disableOsgiPlugin(OsgiPlugin osgiPlugin, boolean storeState) throws PluginException {
        return disableOsgiPlugin(osgiPlugin, new HashSet<>(), storeState);
    }

    private OsgiPlugin disableOsgiPlugin(OsgiPlugin osgiPlugin, Set<Plugin> visitedPlugins, boolean storeState) throws PluginException {
        if (!osgiPlugin.isInUse()) {
            logger.info("Plugin {} already disabled", osgiPlugin.getKey());
            return osgiPlugin;
        }
        logger.info("Disabling plugin {}", osgiPlugin.getKey());
        visitedPlugins.add(osgiPlugin);
        disableDependants(osgiPlugin, visitedPlugins, storeState);
        deactivatePlugin(osgiPlugin);
        disableOsgiBundle(osgiPlugin, osgiPlugin);
        if (storeState) {
            stateStore.storePluginState(osgiPlugin, INSTALLED);
        }
        logger.info("Disabled plugin {}", osgiPlugin.getKey());
        return osgiPlugin;
    }

    private void disableOsgiBundle(Plugin plugin, OsgiPlugin osgiPlugin) throws PluginException {
        logger.info("Disabling OSGi bundle for plugin {}", plugin.getKey());
        osgiPlugin.freePluginActivator();
        waitForMethod(osgiPlugin, INSTALLED, osgiPlugin::disable);
        if (osgiPlugin.getState() != INSTALLED) {
            logger.error("Failed to stop plugin {}", plugin.getKey());
            throw new PluginException("Failed to stop plugin");
        }
    }

    private void deactivatePlugin(OsgiPlugin osgiPlugin) throws PluginException {
        var activator = osgiPlugin.getPluginActivator();
        if (activator == null) {
            return;
        }
        logger.info("Deactivating plugin {}", osgiPlugin.getKey());
        var task = executorService.submit(() -> {
            try {
                var descriptors = activator.getFeatureDescriptors();
                if (descriptors != null) {
                    featureRegistry.unplugDescriptors(osgiPlugin, descriptors);
                }
                activator.deactivate();
                osgiPlugin.markNotActive();
            } catch (RuntimeException e) {
                logger.error("Failed to execute late startup for plugin {}", osgiPlugin.getKey());
            }
            return 1;
        });
        try {
            task.get(ENABLING_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException e) {
            if (e instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            }
            logger.error("Failed to remove plugin from system. Disabling OSGi bundle anyway.");
        } catch (TimeoutException e) {
            logger.error("Failed to remove plugin from system in 25 seconds. Disabling OSGi bundle anyway.");
        }
    }

    private void disableDependants(OsgiPlugin osgiPlugin, Set<Plugin> visitedPlugins, boolean storeState) throws PluginException {
        logger.info("Disabling dependant plugins");
        for (var dependencyPlugin : osgiPlugin.getBundle().adapt(BundleWiring.class)
                .getProvidedWires(null).stream()
                .map(bundleWire -> bundleWire.getRequirer().getBundle())
                .map(pluginRegistry::getPlugin)
                .filter(Plugin::isInUse)
                .filter(plugin -> !visitedPlugins.contains(plugin))
                .toList()) {
            disableOsgiPlugin(dependencyPlugin, visitedPlugins, storeState);
        }
    }

    @Override
    public Plugin restartPlugin(Plugin plugin) throws PluginException {
        validateOsgiState();
        disablePlugin(plugin);
        return enablePlugin(plugin);
    }

    @Override
    public synchronized Collection<Plugin> listPlugins() {
        return new HashSet<>(installedPlugins.values());
    }

    @Override
    public Plugin getPlugin(String key) {
        return installedPlugins.get(key);
    }

    @Override
    public void purgePlugin(Plugin plugin) throws PluginException {
        validateOsgiState();
        var osgiPlugin = installedPlugins.get(plugin.getKey());
        if (osgiPlugin == null) {
            logger.warn("Tried disable not installed plugin {}", plugin.getKey());
            throw new PluginException("Plugin not installed");
        }
        logger.info("Purging plugin {}", osgiPlugin.getKey());
        Set<Plugin> visitedPlugins = new HashSet<>();
        visitedPlugins.add(osgiPlugin);
        disableDependants(osgiPlugin, visitedPlugins, true);
        deactivatePlugin(osgiPlugin);
        purgeOsgiPlugin(osgiPlugin);
        disableOsgiBundle(osgiPlugin, osgiPlugin);
        stateStore.storePluginState(plugin, INSTALLED);
        logger.info("Purged plugin {}", osgiPlugin.getKey());
    }

    private void purgeOsgiPlugin(OsgiPlugin osgiPlugin) throws PluginException {
        var activator = osgiPlugin.getPluginActivator();
        if (activator == null) {
            return;
        }
        logger.info("Purging plugin {}", osgiPlugin.getKey());
        var task = executorService.submit(() -> {
            try {
                var descriptors = activator.getFeatureDescriptors();
                if (descriptors != null) {
                    featureRegistry.purgeDescriptors(osgiPlugin, descriptors);
                }
                activator.purge();
            } catch (RuntimeException e) {
                logger.error("Failed to execute purge for plugin {}", osgiPlugin.getKey());
            }
            return 1;
        });
        try {
            task.get(PURGE_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException e) {
            if (e instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            }
            logger.error("Failed to purge plugin.");
        } catch (TimeoutException e) {
            logger.error("Failed to purge plugin from system in {} seconds.", PURGE_TIMEOUT);
        }
    }

    @PreDestroy
    public void shutdownPlugins() {
        installedPlugins.values().stream()
                .filter(OsgiPlugin::isInUse)
                .forEach(plugin -> {
                    try {
                        disablePlugin(plugin, false);
                    } catch (PluginException e) {
                        logger.error("Failed to shutdown plugin {} during shutdown", plugin.getKey(), e);
                    }
                });
    }

    interface PluginRunnable {
        void run() throws PluginException;
    }
}
