package eu.rarogsoftware.rarog.platform.core.plugins.spring;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
@AutoRegisterFeatureModule
public @interface AutoRegisterFeatureModuleComponent {
    @AliasFor(annotation = Component.class, attribute = "value")
    String beanName() default "";

    @AliasFor(annotation = AutoRegisterFeatureModule.class, attribute = "value")
    Class<? extends FeatureDescriptor>[] value() default {};
}
