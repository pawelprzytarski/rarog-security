package eu.rarogsoftware.rarog.platform.app.rest.security;


import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaEndpoint;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaStore;
import eu.rarogsoftware.rarog.platform.app.configuration.dev.DevModeRestController;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@DevModeRestController
@RequestMapping("/login/mfa/justpass")
@Hidden
public class MfaDevelopmentBypassResource {
    private final MfaStore mfaStore;

    public MfaDevelopmentBypassResource(MfaStore mfaStore) {
        this.mfaStore = mfaStore;
    }

    @GetMapping
    @MfaEndpoint
    public ResponseEntity<String> bypassMfaMechanism(HttpServletRequest request) {
        mfaStore.authorize(request);
        return ResponseEntity.ok("{\"success\": true}");
    }
}
