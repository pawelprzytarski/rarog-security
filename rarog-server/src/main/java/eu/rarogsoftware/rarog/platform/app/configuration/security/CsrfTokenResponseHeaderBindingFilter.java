package eu.rarogsoftware.rarog.platform.app.configuration.security;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.web.access.intercept.AuthorizationFilter;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
public class CsrfTokenResponseHeaderBindingFilter extends OncePerRequestFilter implements AutoImportingFilter {
    protected static final String REQUEST_ATTRIBUTE_NAME = "_csrf";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        CsrfToken token = (CsrfToken) request.getAttribute(REQUEST_ATTRIBUTE_NAME);

        if (token != null && request.getSession(false) != null) {
            response.setHeader(token.getHeaderName(), token.getToken());
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public FilterPlace getFilterPlace() {
        return new FilterPlace(AuthorizationFilter.class, FilterPlace.Direction.AFTER);
    }
}
