package eu.rarogsoftware.rarog.platform.app.rest;

import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.UiFragmentsDescriptor;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowedLevel;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModule;
import eu.rarogsoftware.rarog.platform.core.plugins.templates.AbstractSimpleFeatureModule;
import io.swagger.v3.oas.annotations.Hidden;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("ui/fragments")
@Hidden
@AutoRegisterFeatureModule(UiFragmentsDescriptor.class)
public class UiFragmentsController extends AbstractSimpleFeatureModule<UiFragmentsDescriptor> {
    private final Map<String, List<Fragment>> fragments = new ConcurrentHashMap<>();

    @Override
    protected void clearCache() {
        super.clearCache();
        fragments.clear();
    }

    @GetMapping("{placement}")
    @AnonymousAllowed(AnonymousAllowedLevel.BASIC)
    public List<Fragment> getFragments(@PathVariable("placement") String placement) {
        return fragments.compute(placement, (key, fragments) -> {
            if (fragments != null) {
                return fragments;
            }
            return calculateFragments(key);
        });
    }

    private List<Fragment> calculateFragments(String placement) {
        return getDescriptorMap().values().stream()
                .flatMap(descriptor -> descriptor.fragments().stream())
                .filter(uiFragment -> Objects.equals(uiFragment.placement(), placement))
                .sorted((f1, f2) -> {
                    int compare = Integer.compare(f1.order(), f2.order());
                    if (compare == 0) {
                        return StringUtils.compare(f1.key(), f2.key());
                    }
                    return compare;
                })
                .map(uiFragment -> new Fragment(uiFragment.key(), uiFragment.resource()))
                .toList();
    }

    record Fragment(String key, String resource) {
    }
}
