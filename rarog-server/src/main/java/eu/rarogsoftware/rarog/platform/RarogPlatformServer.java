package eu.rarogsoftware.rarog.platform;

import eu.rarogsoftware.rarog.platform.app.boot.BootManagerDelegatedSpringApplication;
import eu.rarogsoftware.rarog.platform.app.boot.SpringBootManager;
import eu.rarogsoftware.rarog.platform.app.configuration.LocalHomeDirectoryHelper;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.ServletException;
import org.apache.logging.log4j.LogManager;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigurationExcludeFilter;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ResourceLoader;

@Order(1)
@SpringBootApplication(
        exclude = {
                ManagementWebSecurityAutoConfiguration.class
        }
)
@ComponentScan(excludeFilters = {@ComponentScan.Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
        @ComponentScan.Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class),
        @ComponentScan.Filter(type = FilterType.REGEX, pattern = "eu\\.rarogsoftware\\.rarog\\.platform\\.plugins\\..*")})
public class RarogPlatformServer extends SpringBootServletInitializer {

    public static void main(String[] args) {
        customizeBuilder(new BootstrapApplicationBuilder()
                .registerServletInitializer(true)).run(args);
    }

    private static SpringApplicationBuilder customizeBuilder(SpringApplicationBuilder builder) {
        return builder
                .sources(RarogPlatformServer.class)
                .bannerMode(Banner.Mode.CONSOLE);
    }

    @Override
    protected SpringApplicationBuilder createSpringApplicationBuilder() {
        return new BootstrapApplicationBuilder();
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        SpringBootManager.getInstance().servletContextStart(servletContext);
        servletContext.addListener(new ServletContextListener() {
            @Override
            public void contextDestroyed(ServletContextEvent event) {
                LogManager.shutdown();
            }
        });
        super.onStartup(servletContext);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return customizeBuilder(builder).properties();
    }

    private static class BootstrapApplicationBuilder extends SpringApplicationBuilder {
        private BootManagerDelegatedSpringApplication springApplication;

        public BootstrapApplicationBuilder() {
        }

        public BootstrapApplicationBuilder registerServletInitializer(boolean registerServletInitializer) {
            springApplication.setRegisterServletInitializer(registerServletInitializer);
            return this;
        }

        @Override
        protected SpringApplication createSpringApplication(ResourceLoader resourceLoader, Class<?>... sources) {
            LocalHomeDirectoryHelper.getInstance().configureSystemProperties();
            springApplication = new BootManagerDelegatedSpringApplication(resourceLoader, sources);
            return SpringBootManager.getInstance().decorateSpringApplication(springApplication);
        }
    }
}
