package eu.rarogsoftware.rarog.platform.app.servlets;

import eu.rarogsoftware.rarog.platform.api.i18n.I18nHelper;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowedLevel;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaEndpoint;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Hidden
public class LoginController {
    private final I18nHelper i18nHelper;

    public LoginController(I18nHelper i18nHelper) {
        this.i18nHelper = i18nHelper;
    }

    @AnonymousAllowed(AnonymousAllowedLevel.CORE)
    @GetMapping(value = {"login/"})
    public String displayLogin(Authentication authentication, Model model) {
        if (authentication != null && authentication.isAuthenticated()) {
            return "redirect:/account/my";
        }
        setModelVars(model);
        return "reactapp";
    }

    private void setModelVars(Model model) {
        model.addAttribute("title", i18nHelper.getText("login.page.title"));
        model.addAttribute("reactEntrypointName", "login");
        model.addAttribute("theme", "theme/loginpage.html");
    }

    @GetMapping(value = "login/mfa/**")
    @MfaEndpoint
    public String displaySecondFactory(Model model) {
        setModelVars(model);
        return "reactapp";
    }

    @AnonymousAllowed(AnonymousAllowedLevel.CORE)
    @GetMapping(value = "logged-out/")
    public String displayLogout() {
        return "logoutpage";
    }
}
