package eu.rarogsoftware.rarog.platform.app.rest;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AdminPanelItem;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.AdminPanelItemsDescriptor;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoaderFactory;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModule;
import eu.rarogsoftware.rarog.platform.core.plugins.templates.AbstractSimpleFeatureModule;
import io.swagger.v3.oas.annotations.Hidden;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.framework.util.StringComparator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

@RestController
@RequestMapping(path = "admin/", produces = MediaType.APPLICATION_JSON_VALUE)
@AutoRegisterFeatureModule(AdminPanelItemsDescriptor.class)
@Hidden
@SuppressFBWarnings("IS2_INCONSISTENT_SYNC")
public class AdminPanelRestController extends AbstractSimpleFeatureModule<AdminPanelItemsDescriptor> {
    private final DynamicAssetsLoaderFactory dynamicAssetsLoaderFactory;
    private List<MenuElement> cachedElements;

    public AdminPanelRestController(@Qualifier("main") DynamicAssetsLoaderFactory dynamicAssetsLoaderFactory) {
        this.dynamicAssetsLoaderFactory = dynamicAssetsLoaderFactory;
    }

    @GetMapping("navigation")
    List<MenuElement> getNavigationElements() {
        var elements = cachedElements;
        if (elements == null) {
            synchronized (this) {
                final var buildCache = new HashMap<String, MenuElement>();
                final var builtElements = new ArrayList<MenuElement>();
                Stream.concat(
                                getDescriptorMap().values()
                                        .stream()
                                        .flatMap(descriptor -> descriptor.elements().stream()),
                                getDefaultItems())
                        .sorted((o1, o2) -> {
                            var compare = StringComparator.COMPARATOR.compare(o1.category(), o2.category());
                            if (compare == 0) {
                                compare = StringComparator.COMPARATOR.compare(o1.key(), o2.key());
                            }
                            return compare;
                        })
                        .forEach(item -> buildMenuElement(buildCache, builtElements, item));
                cachedElements = builtElements;
                return builtElements;
            }
        }
        return elements;
    }

    private static MenuElement buildMenuElement(HashMap<String, MenuElement> buildCache, ArrayList<MenuElement> builtElements, AdminPanelItem item) {
        if (StringUtils.isBlank(item.category())) {
            var newElement = buildNewElement(item, item.key());
            builtElements.add(newElement);
            buildCache.put(item.key(), newElement);
            return newElement;
        } else {
            var fullkey = item.category() + "/" + item.key();
            var newElement = buildNewElement(item, fullkey);
            var parent = buildCache.get(item.category());
            if (parent == null) {
                var categoryName = item.category();
                int index = categoryName.lastIndexOf('/');
                var parentKey = categoryName.substring(index + 1);
                var grandParent = categoryName.substring(0, index);
                parent = buildMenuElement(buildCache, builtElements, AdminPanelItem.simpleCategory(parentKey, grandParent, parentKey));
            }
            parent.subElements().add(newElement);
            buildCache.put(fullkey, newElement);
            return newElement;
        }
    }

    private static MenuElement buildNewElement(AdminPanelItem item, String fullkey) {
        return new MenuElement(
                item.key(),
                fullkey,
                item.name(),
                item.keywords(),
                item.resource(),
                item.embedded(),
                item.navigationOnly(),
                new ArrayList<>()
        );
    }

    private Stream<AdminPanelItem> getDefaultItems() {
        return Arrays.stream(new AdminPanelItem[]{
                AdminPanelItem.simplePage("",
                        "",
                        "admin.page.dashboard.title",
                        "true"
                ),
                AdminPanelItem.simplePage("settings",
                        "general",
                        "admin.page.general.settings.name",
                        getDynamicResourceJs("admin/generalSettings"),
                        "admin.page.general.settings.keywords"
                ),
                AdminPanelItem.simplePage("info",
                        "general",
                        "admin.page.general.info.name",
                        getDynamicResourceJs("admin/systemInfo"),
                        "admin.page.general.info.keywords"
                ),
                AdminPanelItem.simpleCategory("general", "", "admin.page.general.section")
        });
    }

    private String getDynamicResourceJs(String entrypoint) {
        return dynamicAssetsLoaderFactory
                .getNamedAssetsLoader("dynamicComponents")
                .orElseThrow(() -> new IllegalStateException("Entrypoint " + entrypoint + " is supposed to be always available"))
                .getAssetsForEntryPoint(entrypoint)
                .orElseThrow(() -> new IllegalStateException("Entrypoint " + entrypoint + " is supposed to be always available"))
                .js()
                .get(0)
                .source();
    }

    @Override
    protected void clearCache() {
        synchronized (this) {
            cachedElements = null;
        }
    }

    record MenuElement(String key,
                       String fullKey,
                       String name,
                       String keywords,
                       String resource,
                       boolean embedded,
                       boolean navOnly,
                       List<MenuElement> subElements) {
    }
}
