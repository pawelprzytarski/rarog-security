package eu.rarogsoftware.rarog.platform.app.rest.commons;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "Simple boolean value holder")
public record BooleanValueBean(boolean value) {
}
