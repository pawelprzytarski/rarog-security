package eu.rarogsoftware.rarog.platform.core.metrics.prometheus;

import eu.rarogsoftware.rarog.platform.api.metrics.Histogram;

import java.io.IOException;
import java.util.concurrent.Callable;

public class PrometheusHistogram extends PrometheusMetric implements Histogram {
    private final io.prometheus.client.Histogram delegate;
    private final io.prometheus.client.Histogram.Child child;

    public PrometheusHistogram(io.prometheus.client.Histogram delegate) {
        this(delegate, null);
    }

    public PrometheusHistogram(io.prometheus.client.Histogram delegate, io.prometheus.client.Histogram.Child child) {
        super(delegate);
        this.delegate = delegate;
        this.child = child;
    }

    @Override
    public Histogram labels(String... labels) {
        return new PrometheusHistogram(delegate, delegate.labels(labels));
    }

    @Override
    public double measureExecutionTime(Runnable timeable) {
        return child != null ? child.time(timeable) : delegate.time(timeable);
    }

    @Override
    public <T> T measureExecutionTime(Callable<T> timeable) throws Exception {
        return child != null ? child.time(timeable) : delegate.time(timeable);
    }

    @Override
    public void observe(double amt) {
        if (child != null) {
            child.observe(amt);
        } else {
            delegate.observe(amt);
        }
    }

    @Override
    public Timer startTimer() {
        return new Timer() {
            private final io.prometheus.client.Histogram.Timer timer = child != null ? child.startTimer() : delegate.startTimer();

            @Override
            public double observeDuration() {
                return timer.observeDuration();
            }

            @Override
            public void close() throws IOException {
                timer.close();
            }
        };
    }
}
