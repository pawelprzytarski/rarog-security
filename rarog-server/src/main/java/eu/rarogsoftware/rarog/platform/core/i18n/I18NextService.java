package eu.rarogsoftware.rarog.platform.core.i18n;

import eu.rarogsoftware.rarog.platform.api.i18n.I18NextResourceDescriptor;
import eu.rarogsoftware.rarog.platform.api.i18n.I18NextTransformerDescriptor;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

/**
 * Service loads, merges and transforms translations in format of <a href="https://www.i18next.com/">i18Next</a>.
 *
 * <p>
 * Service loads translations from app nad from plugins and merges translations under the same namespace.
 * Namespace is read from resource filename, for example resource `messages_common.json` will be treated as
 * translations under namespace common. If any two plugins define the same name of resource, then service
 * attempt merging, this is when order of translations matter.
 * Please be aware that it is possible to override translations from any plugin or from app.
 * </p>
 * <p>
 * Service supports also transformers, so it technically possible to fix some problems or even change form of translations
 * on the fly. Consult active implementation for more info about transformers and merging.
 * </p>
 *
 * @see I18NextResourceDescriptor
 * @see I18NextTransformerDescriptor
 */
public interface I18NextService {
    void setParentDirectory(String parentDirectory);

    @Nullable
    String getDefaultEncoding();

    void setDefaultEncoding(@Nullable String defaultEncoding);

    long getLastUpdate();

    I18NextNamespacedResourceBundle getResourceBundle(String baseName, Locale locale, ClassLoader loader, boolean reload) throws IOException;

    InputStream getI18NextResource(Locale locale, String namespace) throws IOException;
}
