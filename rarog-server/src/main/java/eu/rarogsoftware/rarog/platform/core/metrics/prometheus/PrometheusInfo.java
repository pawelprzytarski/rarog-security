package eu.rarogsoftware.rarog.platform.core.metrics.prometheus;

import eu.rarogsoftware.rarog.platform.api.metrics.Info;

import java.util.Map;

public class PrometheusInfo extends PrometheusMetric implements Info {
    private final io.prometheus.client.Info delegate;
    private final io.prometheus.client.Info.Child child;

    public PrometheusInfo(io.prometheus.client.Info delegate) {
        this(delegate, null);
    }

    public PrometheusInfo(io.prometheus.client.Info delegate, io.prometheus.client.Info.Child child) {
        super(delegate);
        this.delegate = delegate;
        this.child = child;
    }

    @Override
    public Info labels(String... labels) {
        return new PrometheusInfo(delegate, delegate.labels(labels));
    }

    @Override
    public void info(Map<String, String> info) {
        if (child != null) {
            child.info(info);
        } else {
            delegate.info(info);
        }
    }
}
