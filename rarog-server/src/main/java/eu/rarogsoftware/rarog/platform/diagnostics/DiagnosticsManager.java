package eu.rarogsoftware.rarog.platform.diagnostics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.UnknownHostException;

public class DiagnosticsManager {
    private static final Logger logger = LoggerFactory.getLogger(DiagnosticsManager.class);
    private static final String SEPARATOR = "==================================\n";
    private static final double MB_SIZE = 1024.0 * 1024.0;


    public static String getStartInfo() {
        StringBuilder infoBuilder = new StringBuilder();
        infoBuilder.append("Environment variables:\n");
        System.getenv().forEach((key, value) -> infoBuilder.append("+ ").append(key).append("=").append(value).append("\n"));
        infoBuilder.append(SEPARATOR);
        infoBuilder.append("System properties:\n");
        System.getProperties().forEach((key, value) -> infoBuilder.append("+ ").append(key).append("=").append(value).append("\n"));
        infoBuilder.append(SEPARATOR);
        infoBuilder.append("Runtime version:\n").append(Runtime.version()).append("\n");
        infoBuilder.append("Available processors: ").append(Runtime.getRuntime().availableProcessors()).append("\n");
        infoBuilder.append(getMemoryInfo());
        infoBuilder.append(SEPARATOR);
        infoBuilder.append("Home directory: ").append(System.getProperty("home.directory", System.getenv("RAROG_HOME"))).append("\n");
        infoBuilder.append("Running as user: ").append(System.getProperty("user.name")).append("\n");
        infoBuilder.append("Hostname: ");
        try {
            infoBuilder.append(java.net.InetAddress.getLocalHost().getHostName()).append("\n");
        } catch (UnknownHostException e) {
            infoBuilder.append("Unknown\n");
        }

        return infoBuilder.toString();
    }

    public static MemoryInfo getMemoryInfo() {
        Runtime runtime = Runtime.getRuntime();
        return new MemoryInfo(runtime.maxMemory() / MB_SIZE, runtime.totalMemory() / MB_SIZE, runtime.freeMemory() / MB_SIZE);
    }

    public record MemoryInfo(double max, double total, double free, double used) {

        public MemoryInfo(double max, double total, double free) {
            this(max, total, free, total - free);
        }

        @Override
        public String toString() {
            StringBuilder memoryInfo = new StringBuilder();
            Runtime runtime = Runtime.getRuntime();
            memoryInfo.append("Max memory: ").append(max()).append("MB\n");
            memoryInfo.append("Total memory: ").append(total()).append("MB\n");
            memoryInfo.append("Free memory: ").append(free()).append("MB\n");
            memoryInfo.append("Used memory: ").append((used() - runtime.freeMemory())).append("MB\n");
            return memoryInfo.toString();
        }
    }
}
