package eu.rarogsoftware.rarog.platform.dynamic.frontend.resources;

import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoader;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoaderFactory;
import eu.rarogsoftware.rarog.platform.core.common.urlutils.BaseUrlHelper;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.spring6.context.SpringContextUtils;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.templatemode.TemplateMode;

import java.util.*;

public abstract class DynamicAssetsLoaderProcessor extends AbstractElementTagProcessor {
    protected static final int PRECEDENCE = 1000;
    private static final String ENTRYPOINT_ATTRIBUTE_NAME = "entrypoint";
    private static final String ASSET_ATTRIBUTE_NAME = "asset";
    private final String ASSETS_CACHE_VARIABLE_NAME = DynamicAssetsLoaderProcessor.class.getName() + "_templateAssets";

    public DynamicAssetsLoaderProcessor(TemplateMode templateMode, String dialectPrefix, String elementName, boolean prefixElementName, String attributeName, boolean prefixAttributeName, int precedence) {
        super(templateMode, dialectPrefix, elementName, prefixElementName, attributeName, prefixAttributeName, precedence);
    }

    @Override
    protected void doProcess(ITemplateContext context,
                             IProcessableElementTag tag,
                             IElementTagStructureHandler structureHandler) {
        final var appCtx = SpringContextUtils.getApplicationContext(context);
        var assetsLoaderFactories = appCtx.getBeansOfType(DynamicAssetsLoaderFactory.class).values();
        var mainAssetsLoaderFactory = appCtx.getBean(LocalLoaderDynamicAssetsLoaderFactory.class);
        var baseUrlHelper = appCtx.getBean(BaseUrlHelper.class);

        var entrypoint = getEntrypointAttributeValue(context, tag);
        var asset = tag.getAttributeValue(ASSET_ATTRIBUTE_NAME);
        var assetsLoader = mainAssetsLoaderFactory.getMainAssetsLoader();
        if (entrypoint.contains(":")) {
            var split = entrypoint.split("\\|", 3);
            var namespace = split.length == 3 ? split[0] + "|" + split[1] : split[0];
            assetsLoader = assetsLoaderFactories
                    .stream()
                    .map(factory -> factory.getNamedAssetsLoader(namespace))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .findFirst()
                    .orElse(assetsLoader);
            entrypoint = split[split.length == 2 ? 1 : 2];
        }

        var assetsList = loadAssetsList(assetsLoader, entrypoint, asset);

        var model = createModelForAssets(baseUrlHelper, assetsList, context);

        structureHandler.replaceWith(model, false);
    }

    private static String getEntrypointAttributeValue(ITemplateContext context, IProcessableElementTag tag) {
        try {
            return StandardExpressions.getExpressionParser(context.getConfiguration()).parseExpression(context,
                            tag.getAttributeValue(ENTRYPOINT_ATTRIBUTE_NAME))
                    .execute(context)
                    .toString();
        } catch (TemplateProcessingException e) {
            return tag.getAttributeValue(ENTRYPOINT_ATTRIBUTE_NAME);
        }
    }

    private List<DynamicAssetsLoader.WebpackAssetData> loadAssetsList(DynamicAssetsLoader assetsLoader, String entrypoint, String asset) {
        List<DynamicAssetsLoader.WebpackAssetData> assetsList = Collections.emptyList();

        if (entrypoint != null) {
            var data = assetsLoader.getAssetsForEntryPoint(entrypoint);
            if (data.isPresent()) {
                assetsList = getEntrypointAssets(data.get());
            }
        } else if (asset != null) {
            var data = assetsLoader.getAssetData(asset);
            if (data.isPresent()) {
                assetsList = Collections.singletonList(data.get());
            }
        }
        return removeDuplicatedAssets(assetsList);
    }

    private List<DynamicAssetsLoader.WebpackAssetData> removeDuplicatedAssets(List<DynamicAssetsLoader.WebpackAssetData> assetsList) {
        var context = RenderContextHolder.getRenderContext();
        var assetsVariable = context.get(ASSETS_CACHE_VARIABLE_NAME);
        var assetsCache = (assetsVariable instanceof Set<?>) ? (Set<DynamicAssetsLoader.WebpackAssetData>) assetsVariable : new HashSet<DynamicAssetsLoader.WebpackAssetData>();

        var finalAssetsList = assetsList.stream()
                .filter(assetData -> !assetsCache.contains(assetData))
                .toList();
        assetsCache.addAll(finalAssetsList);
        context.put(ASSETS_CACHE_VARIABLE_NAME, assetsCache);
        return finalAssetsList;
    }

    protected abstract IModel createModelForAssets(BaseUrlHelper baseUrlHelper, List<DynamicAssetsLoader.WebpackAssetData> assetsList, ITemplateContext context);

    protected abstract List<DynamicAssetsLoader.WebpackAssetData> getEntrypointAssets(DynamicAssetsLoader.EntrypointAssetData data);
}
