package eu.rarogsoftware.rarog.platform.core.plugins.web.filters;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginSecurityFilterMapping;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginSecurityFilterMappingDescriptor;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModuleComponent;
import eu.rarogsoftware.rarog.platform.core.plugins.templates.AbstractSimpleFeatureModule;
import jakarta.servlet.Filter;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter;

import java.util.ArrayList;
import java.util.List;

@AutoRegisterFeatureModuleComponent(PluginSecurityFilterMappingDescriptor.class)
@SuppressFBWarnings("IS2_INCONSISTENT_SYNC")
public class PluginSecurityFilterResolver extends AbstractSimpleFeatureModule<PluginSecurityFilterMappingDescriptor> {
    private List<Filter> cachedFiltersList;
    private List<Filter> originalFilters;
    private static final int STEP_SIZE = 100;

    public List<Filter> getUpdatedFilters(List<Filter> filters) {
        synchronized (this) {
            if (filters != originalFilters || cachedFiltersList == null) {
                originalFilters = filters;
                cachedFiltersList = calculateFiltersOrder(filters);
            }
        }
        return cachedFiltersList;
    }

    @Override
    protected void clearCache() {
        super.clearCache();
        synchronized (this) {
            cachedFiltersList = null;
        }
    }

    private List<Filter> calculateFiltersOrder(List<Filter> filters) {
        List<OrderedFilter> orderedFilters = new ArrayList<>();
        var filterOrderRegistration = new FilterOrderRegistration();
        var order = new Step(filterOrderRegistration.getOrder(SwitchUserFilter.class) + STEP_SIZE, STEP_SIZE);
        var beggingOrder = new Step(0, -STEP_SIZE);
        int lastOrder = 0;
        for (Filter filter : filters) {
            var filterOrder = filterOrderRegistration.getOrder(filter.getClass());
            if (filterOrder == null) {
                filterOrder = lastOrder + STEP_SIZE / 2;
            }
            orderedFilters.add(new OrderedFilter(filterOrder, filter));
            lastOrder = filterOrder;
        }

        getDescriptorMap().values().stream()
                .flatMap(descriptor -> descriptor.filterMappings().stream())
                .forEach(mapping -> {
                            var newOrder = switch (mapping.placement()) {
                                case AT -> getRelativeOrder(filterOrderRegistration, mapping);
                                case BEFORE -> getRelativeOrder(filterOrderRegistration, mapping) - 1;
                                case AFTER -> getRelativeOrder(filterOrderRegistration, mapping) + 1;
                                case AT_BEGINNING -> beggingOrder.next();
                                case AT_END -> order.next();
                            };
                            filterOrderRegistration.put(mapping.filter().getClass(), newOrder);
                            orderedFilters.add(
                                    new OrderedFilter(
                                            newOrder,
                                            mapping.filter()));
                        }
                );

        return orderedFilters.stream()
                .sorted((first, second) -> {
                    int compare = Integer.compare(first.order(), second.order());
                    if (compare == 0) {
                        return AnnotationAwareOrderComparator.INSTANCE.compare(first, second);
                    }
                    return compare;
                })
                .map(OrderedFilter::filter)
                .toList();
    }

    private static Integer getRelativeOrder(FilterOrderRegistration filterOrderRegistration, PluginSecurityFilterMapping mapping) {
        return mapping.relativeToFilterName() != null
                ? filterOrderRegistration.getOrder(mapping.relativeToFilterName())
                : filterOrderRegistration.getOrder(mapping.relativeToFilter());
    }

    record OrderedFilter(int order, Filter filter) {
    }

    private static class Step {
        private int value;
        private final int stepSize;

        Step(int initialValue, int stepSize) {
            this.value = initialValue;
            this.stepSize = stepSize;
        }

        int next() {
            var returnValue = this.value;
            this.value += this.stepSize;
            return returnValue;
        }

    }
}
