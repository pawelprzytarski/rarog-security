package eu.rarogsoftware.rarog.platform.core.plugins.osgi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginException;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginManifest;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginArtifact;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.util.Objects;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;

public class JarPluginArtifact implements PluginArtifact, Closeable {
    private static final ObjectMapper jsonMapper = new ObjectMapper();
    private final Logger logger = LoggerFactory.getLogger(JarPluginArtifact.class);
    private final String location;
    private Resource resource;
    private JarFile jarFile;
    private File file;

    public JarPluginArtifact(URI uri) {
        this(uri.toString());
    }

    public JarPluginArtifact(URL url) {
        this(url.toString());
    }

    public JarPluginArtifact(String location) {
        this.location = location;
    }

    public JarPluginArtifact(Resource resource) {
        this.location = getIdFromResource(resource);
        this.resource = resource;
    }

    private String getIdFromResource(Resource resource) {
        try {
            return resource.getURL().getPath();
        } catch (IOException e) {
            return resource.getDescription();
        }
    }

    public Manifest getJarManifest() throws IOException {
        return getJar().getManifest();
    }

    private JarFile getJar() throws IOException {
        if (jarFile == null) {
            try {
                jarFile = new JarFile(resource.getFile());
            } catch (FileNotFoundException ignored) {
                file = File.createTempFile("rarogPluginJar", null);
                file.deleteOnExit();
                FileUtils.writeByteArrayToFile(file, resource.getInputStream().readAllBytes());
                jarFile = new JarFile(file);
            }
        }
        return jarFile;
    }

    public InputStream getInputStream() throws IOException {
        return Objects.requireNonNull(getResource()).getInputStream();
    }

    private Resource getResource() {
        if (resource == null) {
            resource = new DefaultResourceLoader(getClass().getClassLoader()).getResource(location);
        }
        return resource;
    }

    @Override
    public InputStream getResource(String location) throws IOException {
        ZipEntry entry = getJar().getEntry(location);
        if (entry == null) {
            return null;
        }
        return getJar().getInputStream(entry);
    }

    @Override
    public String getId() {
        return "jar:" + location;
    }

    @Override
    public boolean exists() {
        return getResource().exists();
    }

    @Override
    public PluginManifest getManifest() throws IOException, PluginException {
        PluginManifest manifest;
        logger.trace("Reading manifest file from plugin {}", location);
        try (var manifestStream = getResource("META-INF/rarog-plugin.json")) {
            if (manifestStream == null) {
                logger.warn("Jar {} is not correct rarog plugin", location);
                throw new PluginException("Not a plugin");
            }
            manifest = jsonMapper.readValue(manifestStream, PluginManifest.class);
        } catch (JsonProcessingException e) {
            logger.warn("Plugin read from URL {} is not valid rarog plugin. Cannot read parse manifest file", location, e);
            throw new PluginException(e);
        }
        return manifest;
    }

    @Override
    public void close() throws IOException {
        if (jarFile != null) {
            jarFile.close();
        }
        if (file != null) {
            file.delete();
        }
    }
}
