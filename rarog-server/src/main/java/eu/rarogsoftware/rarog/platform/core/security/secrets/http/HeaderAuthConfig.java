package eu.rarogsoftware.rarog.platform.core.security.secrets.http;

import eu.rarogsoftware.rarog.platform.api.security.secrets.HttpClientAuthConfigService;
import org.springframework.http.HttpHeaders;

import java.net.http.HttpRequest;
import java.util.Map;

public class HeaderAuthConfig implements HttpClientAuthConfigService.AuthConfig {
    private final String headerValue;

    public HeaderAuthConfig(String headerValue) {
        this.headerValue = headerValue;
    }

    public HeaderAuthConfig(Map<String, Object> authenticationData) {
        this(authenticationData.get("value").toString());
    }

    @Override
    public HttpRequest configureHttpRequest(HttpRequest request) {
        return HttpRequest.newBuilder(request, (name, value) -> !HttpHeaders.AUTHORIZATION.equals(name))
                .header(HttpHeaders.AUTHORIZATION, headerValue)
                .build();
    }
}
