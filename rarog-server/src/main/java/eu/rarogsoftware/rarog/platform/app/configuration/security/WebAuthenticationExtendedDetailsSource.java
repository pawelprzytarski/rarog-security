package eu.rarogsoftware.rarog.platform.app.configuration.security;

import org.springframework.security.authentication.AuthenticationDetailsSource;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

public class WebAuthenticationExtendedDetailsSource implements AuthenticationDetailsSource<HttpServletRequest, WebAuthenticationDetails> {
    public WebAuthenticationExtendedDetailsSource() {
    }

    public WebAuthenticationDetails buildDetails(HttpServletRequest request) {
        String remoteAddress = request.getRemoteAddr();
        HttpSession session = request.getSession(false);
        String sessionId = session != null ? session.getId() : null;
        String userAgent = request.getHeader("User-Agent");
        return new WebAuthenticationDetails(remoteAddress, sessionId, userAgent);
    }
}