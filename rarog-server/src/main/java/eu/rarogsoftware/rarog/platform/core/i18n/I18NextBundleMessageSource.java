package eu.rarogsoftware.rarog.platform.core.i18n;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Implementation of {@link org.springframework.context.MessageSource} that support I18Next files format.
 * Class in based on {@link ResourceBundleMessageSource}.
 */
@SuppressFBWarnings(value = "URLCONNECTION_SSRF_FD", justification = "URL are provided by spring, not by user")
public class I18NextBundleMessageSource extends ResourceBundleMessageSource {
    private final Map<ResourceBundle, Map<String, Map<Locale, MessageFormat>>> cachedBundleMessageFormats =
            new ConcurrentHashMap<>();
    private final DefaultI18NextService i18NextService;

    private volatile MessageSourceControl control = new MessageSourceControl();


    public I18NextBundleMessageSource(DefaultI18NextService i18NextService) {
        setBasename("messages");
        setDefaultEncoding("UTF-8");
        this.i18NextService = i18NextService;
    }

    @Override
    protected ResourceBundle doGetBundle(String basename, Locale locale) throws MissingResourceException {
        ClassLoader classLoader = getBundleClassLoader();
        Assert.state(classLoader != null, "No bundle ClassLoader set");

        MessageSourceControl control = this.control;
        if (control != null) {
            try {
                return ResourceBundle.getBundle(basename, locale, classLoader, control);
            } catch (UnsupportedOperationException ex) {
                // Probably in a Jigsaw environment on JDK 9+
                this.control = null;
            }
        }
        return ResourceBundle.getBundle(basename, locale, classLoader);
    }


    @Override
    @Nullable
    protected MessageFormat getMessageFormat(ResourceBundle bundle, String code, Locale locale)
            throws MissingResourceException {

        Map<String, Map<Locale, MessageFormat>> codeMap = this.cachedBundleMessageFormats.get(bundle);
        Map<Locale, MessageFormat> localeMap = null;
        if (codeMap != null) {
            localeMap = codeMap.get(code);
            if (localeMap != null) {
                MessageFormat result = localeMap.get(locale);
                if (result != null) {
                    return result;
                }
            }
        }

        var msg = getStringOrNull(bundle, code);
        if (msg != null) {
            if (codeMap == null) {
                codeMap = this.cachedBundleMessageFormats.computeIfAbsent(bundle, b -> new ConcurrentHashMap<>());
            }
            if (localeMap == null) {
                localeMap = codeMap.computeIfAbsent(code, c -> new ConcurrentHashMap<>());
            }
            var result = createMessageFormat(msg, locale);
            localeMap.put(locale, result);
            return result;
        }

        return null;
    }

    private class MessageSourceControl extends ResourceBundle.Control {
        @Override
        @Nullable
        public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
                throws IOException {

            if (format.equals("i18next.json")) {
                return i18NextService.getResourceBundle(baseName, locale, loader, reload);
            } else {
                throw new UnsupportedOperationException("Format: {" + format + "} is unsupported by this bundle");
            }
        }

        @Override
        public List<String> getFormats(String baseName) {
            return List.of("i18next.json");
        }

        @Override
        @Nullable
        public Locale getFallbackLocale(String baseName, Locale locale) {
            var defaultLocale = getDefaultLocale();
            return (defaultLocale != null && !defaultLocale.equals(locale) ? defaultLocale : null);
        }

        @Override
        public long getTimeToLive(String baseName, Locale locale) {
            long cacheMillis = getCacheMillis();
            return (cacheMillis >= 0 ? cacheMillis : super.getTimeToLive(baseName, locale));
        }

        @Override
        public boolean needsReload(
                String baseName, Locale locale, String format, ClassLoader loader, ResourceBundle bundle, long loadTime) {

            if (super.needsReload(baseName, locale, format, loader, bundle, loadTime) || i18NextService.getLastUpdate() >= loadTime) {
                cachedBundleMessageFormats.remove(bundle);
                return true;
            } else {
                return false;
            }
        }
    }
}