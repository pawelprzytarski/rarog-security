package eu.rarogsoftware.rarog.platform.core.settings;

import com.querydsl.sql.SQLQueryFactory;
import eu.rarogsoftware.commons.database.connection.DatabaseConnectionProvider;
import eu.rarogsoftware.rarog.platform.db.QApplicationSettings;
import org.springframework.stereotype.Component;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.configuration.FactoryBuilder;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.AccessedExpiryPolicy;
import javax.cache.expiry.Duration;
import javax.cache.integration.CacheLoader;
import javax.cache.integration.CacheLoaderException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.StreamSupport;

import static eu.rarogsoftware.commons.database.connection.DatabaseConnectionProvider.TransactionLevel.*;

/**
 * This {@link SettingsStore} implementation uses DB to store settings and has a caching layer
 * to improve read performance.
 */
@Component
public class DbSettingsStore implements SettingsStore {
    private static final String SETTINGS_CACHE_KEY = DbSettingsStore.class.getName() + "_cache";
    private static final Duration SETTINGS_CACHE_LIFETIME = Duration.TEN_MINUTES;
    private static final QApplicationSettings SETTINGS = QApplicationSettings.applicationSettings;

    private final DatabaseConnectionProvider dbConnectionProvider;

    private final Cache<String, String> settingsCache;

    public DbSettingsStore(DatabaseConnectionProvider databaseConnectionProvider, CacheManager cacheManager) {
        this.dbConnectionProvider = databaseConnectionProvider;
        this.settingsCache = getSettingsCache(cacheManager);
    }

    @Override
    public String getSetting(String key) {
        if (key == null) {
            throw new IllegalArgumentException("Setting key cannot be null.");
        }
        return settingsCache.get(key);
    }

    @Override
    public void setSetting(String key, String value) {
        if (key == null) {
            throw new IllegalArgumentException("Setting key cannot be null.");
        }
        settingsCache.remove(key);
        dbConnectionProvider.getConnectionInTransaction(READ_UNCOMMITTED,
            query -> {
                if (!isKeyInDb(key, query)) {
                    return insertTheNewSetting(key, value, query);
                } else {
                    return updateExistingSetting(query, key, value);
                }
        });
    }

    private boolean isKeyInDb(String key, SQLQueryFactory query) {
        return query.select(SETTINGS.all())
            .from(SETTINGS)
            .where(SETTINGS.settingKey.eq(key))
            .fetchOne() != null;
    }

    private Long insertTheNewSetting(String key, String value, SQLQueryFactory query) {
        var insertedID = query.insert(SETTINGS)
            .set(SETTINGS.settingKey, key)
            .set(SETTINGS.settingValue, value)
            .executeWithKey(SETTINGS.settingId);
        var lowestIdInDb = query.select(SETTINGS.all())
            .from(SETTINGS)
            .where(SETTINGS.settingKey.eq(key))
            .orderBy(SETTINGS.settingId.desc())
            .fetchFirst();
        //Remove the newly inserted row if any other was inserted after us
        if (!Objects.equals(lowestIdInDb.get(SETTINGS.settingId), insertedID)) {
            query.delete(SETTINGS)
                .where(SETTINGS.settingId.eq(insertedID))
                .execute();
            return -1L;
        }
        return insertedID;
    }

    private long updateExistingSetting(SQLQueryFactory query, String key, String value) {
        return query.update(SETTINGS)
            .set(SETTINGS.settingValue, value)
            .where(SETTINGS.settingKey.eq(key))
            .execute();
    }

    private Cache<String, String> getSettingsCache(CacheManager cacheManager) {
        var cache = cacheManager.getCache(SETTINGS_CACHE_KEY, String.class, String.class);
        if (cache == null) {
            cache = cacheManager.createCache(SETTINGS_CACHE_KEY, new MutableConfiguration<String, String>()
                .setStoreByValue(true)
                .setReadThrough(true)
                .setTypes(String.class, String.class)
                .setCacheLoaderFactory(new FactoryBuilder.SingletonFactory<>(
                    new SettingsCacheLoader(dbConnectionProvider)))
                .setExpiryPolicyFactory(AccessedExpiryPolicy.factoryOf(SETTINGS_CACHE_LIFETIME)));
        }
        return cache;
    }


    private record SettingsCacheLoader(DatabaseConnectionProvider dbConnectionProvider) implements CacheLoader<String, String> {

        public String load(String key) throws CacheLoaderException {
            return dbConnectionProvider.getConnection(query -> query.select(SETTINGS.settingValue)
                .from(SETTINGS)
                .where(SETTINGS.settingKey.eq(key))
                .fetchOne());
        }

        public Map<String, String> loadAll(Iterable<? extends String> keys)
            throws CacheLoaderException {
            var data = new HashMap<String, String>();
            dbConnectionProvider.getConnection(query -> query.select(SETTINGS.all())
                .from(SETTINGS)
                .where(SETTINGS.settingKey.in(StreamSupport.stream(keys.spliterator(), false).toList()))
                .fetch())
                .forEach(row -> data.put(row.get(1, String.class), row.get(2, String.class)));
            return data;
        }
    }
}
