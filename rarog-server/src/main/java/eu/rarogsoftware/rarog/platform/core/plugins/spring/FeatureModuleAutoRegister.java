package eu.rarogsoftware.rarog.platform.core.plugins.spring;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.core.plugins.PluggableFeatureRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@SuppressWarnings("rawtypes")
@Component
public class FeatureModuleAutoRegister implements InitializingBean, ApplicationContextAware {
    private final Logger logger = LoggerFactory.getLogger(FeatureModuleAutoRegister.class);
    private final PluggableFeatureRegistry featureRegistry;
    private ApplicationContext context = null;

    @Autowired
    public FeatureModuleAutoRegister(PluggableFeatureRegistry featureRegistry) {
        this.featureRegistry = featureRegistry;
    }

    @Override
    public void afterPropertiesSet() {
        if (context == null) {
            throw new IllegalStateException("ApplicationContext cannot be null");
        }
        var beans = context.getBeansWithAnnotation(AutoRegisterFeatureModule.class);
        beans.forEach((beanName, bean) -> {
            Class<?> beanClass = bean.getClass();
            logger.trace("Auto registering bean {} of type {} as feature module", beanName, beanClass);
            var annotation = context.findAnnotationOnBean(beanName, AutoRegisterFeatureModule.class);
            if (annotation == null || annotation.value() == null || annotation.value().length == 0) {
                logger.error("Bean {}:{} doesn't have correct annotation. You must specify descriptor type to register", beanName, beanClass);
                throw new IllegalArgumentException("Incorrectly annotated bean");
            }
            if (!(bean instanceof FeatureModule<?>)) {
                logger.error("Bean {}:{} does not implement FeatureModule interface", beanName, beanClass);
                throw new IllegalArgumentException("Incorrectly annotated bean");
            }
            Arrays.stream(annotation.value()).forEach(type -> featureRegistry.registerFeatureModule(type, () -> (FeatureModule) bean));
            logger.debug("Auto registered bean {} of type {} as feature module", beanName, beanClass);
        });
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
