package eu.rarogsoftware.rarog.platform.core.i18n;

import eu.rarogsoftware.rarog.platform.api.i18n.I18nHelper;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Collectors;

@ExportComponent
@Component
public class DefaultI18nHelper implements I18nHelper {
    private final I18NextBundleMessageSource messageSource;

    @Autowired
    DefaultI18nHelper(I18NextBundleMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public String getText(String key, Object... args) {
        return getText(LocaleContextHolder.getLocale(), key, args);
    }

    @Override
    public String getText(Locale locale, String key, Object... args) {
        if (locale.equals(SYNTHETIC_LANGUAGE)) {
            if (args.length == 0) {
                return key;
            } else {
                return Arrays.stream(args).map(Object::toString).collect(Collectors.joining(";", key + ":{", "}"));
            }
        }
        return messageSource.getMessage(key, args, locale);
    }
}