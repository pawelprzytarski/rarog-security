package eu.rarogsoftware.rarog.platform.security.mfa;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaService;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaStatus;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaStore;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.app.configuration.security.AutoImportingFilter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.intercept.AuthorizationFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@SuppressFBWarnings(value = {"UNVALIDATED_REDIRECT"}, justification = "Redirect crafted from current url")
@Component
public class MultiFactorEnforcingFilter extends HttpFilter implements AutoImportingFilter {
    public static final String FILTER_APPLIED = MultiFactorEnforcingFilter.class.getName() + ".applied";
    private static final Logger logger = LoggerFactory.getLogger(MultiFactorEnforcingFilter.class);
    private static final AntPathMatcher pathMatcher = new AntPathMatcher();
    private final MfaService mfaService;
    private final MfaStore mfaStore;
    private final MfaPageMatcherFactory matcherFactory;
    private final Set<String> pathMatchers = new HashSet<>();

    public MultiFactorEnforcingFilter(MfaService mfaService, MfaStore mfaStore, MfaPageMatcherFactory matcherFactory) {
        this.mfaService = mfaService;
        this.mfaStore = mfaStore;
        this.matcherFactory = matcherFactory;
    }

    @Override
    public FilterPlace getFilterPlace() {
        return new FilterPlace(AuthorizationFilter.class, FilterPlace.Direction.AFTER);
    }

    public MultiFactorEnforcingFilter addExcludedPathPatterns(String... pathPatterns) {
        pathMatchers.addAll(Arrays.asList(pathPatterns));
        return this;
    }

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (!redirectIfNecessary(request, response)) {
            super.doFilter(request, response, chain);
        }
    }

    private boolean redirectIfNecessary(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (request.getAttribute(FILTER_APPLIED) == null) {
            if (request.getAttribute(MfaService.EXCLUDE_REQUEST_FROM_MFA_ATTRIBUTE) == null) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication != null && authentication.isAuthenticated() && authentication.getPrincipal() != null) {
                    return redirectUser(request, response, authentication);
                }
            }
            request.setAttribute(FILTER_APPLIED, true);
        }
        return false;
    }

    private boolean redirectUser(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        if (authentication.getPrincipal().equals("anonymousUser")) {
            return false;
        }
        if (authentication.getPrincipal() instanceof StandardUser user) {
            MfaStatus status = getMfaStatus(request, user);
            if (shouldBlockAccessToSite(status) && !isMfaPageRequest(request, status)) {
                redirectUserToCorrectPage(request, response, user, status);
                return true;
            }
        } else {
            logger.error("Filter started on server that does not work with ApplicationUser interface");
            throw new IllegalStateException("Authentication contains principal that is not instance of ApplicationUser");
        }
        return false;
    }

    private void redirectUserToCorrectPage(HttpServletRequest request, HttpServletResponse response, StandardUser user, MfaStatus status) throws IOException {
        var redirectPath = "?redirect=" + request.getServletPath() + (request.getPathInfo() != null ? request.getPathInfo() : "");
        if (status == MfaStatus.MFA_NOT_PASSED) {
            logger.debug("User `{}` redirected to MFA login", user.getUsername());
            response.sendRedirect(request.getContextPath() + "/login/mfa/" + redirectPath);
        } else {
            logger.debug("User `{}` redirected to MFA configuration", user.getUsername());
            response.sendRedirect(request.getContextPath() + "/login/mfa/configure" + redirectPath);
        }
    }

    private boolean isMfaPageRequest(HttpServletRequest request, MfaStatus status) {
        boolean matches = matcherFactory.createPageMatcher(status == MfaStatus.MFA_NEED_TO_BE_CONFIGURED).matches(request);
        if (!matches) {
            String path = request.getServletPath() + (request.getPathInfo() == null ? "" : request.getPathInfo());
            return pathMatchers.stream().anyMatch(pattern -> pathMatcher.match(pattern, path));
        }
        return true;
    }

    private boolean shouldBlockAccessToSite(MfaStatus status) {
        return switch (status) {
            case MFA_DISABLED, MFA_PASSED -> false;
            case MFA_NEED_TO_BE_CONFIGURED, MFA_NOT_PASSED -> true;
        };
    }

    private MfaStatus getMfaStatus(HttpServletRequest request, StandardUser user) {
        return mfaStore.getStatus(request).orElseGet(() -> {
            var mfaStatus = calculateStatus(user);
            mfaStore.saveStatus(request, mfaStatus);
            return mfaStatus;
        });
    }

    private MfaStatus calculateStatus(StandardUser user) {
        if (!mfaService.isMfaEnabled()) {
            return MfaStatus.MFA_DISABLED;
        }
        if (mfaService.isMfaEnabledForUser(user)) {
            if (!mfaService.isMfaConfiguredForUser(user)) {
                return MfaStatus.MFA_NEED_TO_BE_CONFIGURED;
            }
            return MfaStatus.MFA_NOT_PASSED;
        }
        return MfaStatus.MFA_DISABLED;
    }
}
