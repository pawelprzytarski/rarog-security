package eu.rarogsoftware.rarog.platform.dynamic.frontend.resources;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.thymeleaf.spring6.view.ThymeleafView;

import java.util.Map;

public class RenderContextThymeleafView extends ThymeleafView {

    @Override
    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RenderContextHolder.runInNewContext(() -> super.render(model, request, response));
    }
}

