package eu.rarogsoftware.rarog.platform.app.configuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;
import eu.rarogsoftware.rarog.platform.api.i18n.I18NextResourceDescriptor;
import eu.rarogsoftware.rarog.platform.api.i18n.I18NextTransformerDescriptor;
import eu.rarogsoftware.rarog.platform.core.i18n.DefaultI18NextService;
import eu.rarogsoftware.rarog.platform.core.i18n.I18NextBundleMessageSource;
import eu.rarogsoftware.rarog.platform.core.i18n.UserProvidedValueLocaleResolver;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModule;
import eu.rarogsoftware.rarog.platform.core.plugins.templates.OsgiPluginTemplateResolver;
import eu.rarogsoftware.rarog.platform.core.plugins.web.PluginMappingHandlerMapping;
import eu.rarogsoftware.rarog.platform.core.plugins.web.servlets.SmartSwitchingServlet;
import eu.rarogsoftware.rarog.platform.dynamic.frontend.resources.DynamicAssetsDialect;
import eu.rarogsoftware.rarog.platform.dynamic.frontend.resources.RenderContextThymeleafView;
import io.swagger.v3.oas.models.OpenAPI;
import jakarta.servlet.MultipartConfigElement;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletPath;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.Ordered;
import org.springframework.http.CacheControl;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.MimeType;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.EncodedResourceResolver;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.spring6.SpringTemplateEngine;
import org.thymeleaf.spring6.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.nio.charset.Charset;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

@EnableWebMvc
@Configuration
@EnableConfigurationProperties(ThymeleafProperties.class)
@PropertySources(value = {
        @PropertySource("classpath:application.yml"),
        @PropertySource(value = "config:base.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "config:database.properties", ignoreResourceNotFound = true)
})
public class WebMvcConfiguration implements WebMvcConfigurer {
    private static final String CLASSPATH_PROTOCOL = "classpath:";
    @Value("${spring.messages.parentDirectory:i18n}")
    protected String messagesParentDirectory;
    @Value("${spring.mvc.static-path-pattern:/static/**}")
    private String staticPathPattern;
    @Value("${spring.resources.cache.cachecontrol.max-age:365d}")
    private Duration staticResourceMaxAge;
    @Value("${spring.resources.cache.cachecontrol.no-cache:false}")
    private boolean staticResourceNoCache;
    @Value("${spring.web.resources.static-locations:classpath:/static/}")
    private String[] staticResourcesLocations;
    @Value("${webpack.resources.integrity.enabled:true}")
    private boolean webpackResourcesIntegrityEnabled;
    private final ListableBeanFactory beanFactory;

    public WebMvcConfiguration(ListableBeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        addTrailingSlashToResourcesLocations();
        var updatedLocations = ArrayUtils.add(staticResourcesLocations, "classpath:/static/");
        registry.addResourceHandler(staticPathPattern)
                .addResourceLocations(updatedLocations)
                .setUseLastModified(true)
                .setCacheControl(staticResourceNoCache ? CacheControl.noCache() : CacheControl.maxAge(staticResourceMaxAge))
                .resourceChain(false)
                .addResolver(new EncodedResourceResolver())
                .addResolver(beanFactory.getBean(OsgiPluginResourceResolver.class))
                .addResolver(beanFactory.getBean(DescriptorBasedResourceResolver.class));
    }

    private void addTrailingSlashToResourcesLocations() {
        for (var i = 0; i < staticResourcesLocations.length; i++) {
            if (!staticResourcesLocations[i].endsWith("/")) {
                staticResourcesLocations[i] += "/";
            }
        }
    }

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.setUseTrailingSlashMatch(true);
    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        // workaround for Spring being Spring and not using ObjectMapper bean but creating new one without any configuration applied
        converters.removeIf(httpMessageConverter -> httpMessageConverter instanceof MappingJackson2HttpMessageConverter);
        converters.add(new MappingJackson2HttpMessageConverter(
                beanFactory.getBean(Jackson2ObjectMapperBuilder.class)
                        .build()
                        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                        .configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS, false)
                        .setDefaultPropertyInclusion(JsonInclude.Include.NON_NULL)
        ));
    }

    @Bean
    public PluginMappingHandlerMapping pluginMappingHandlerMapping() {
        return new PluginMappingHandlerMapping();
    }

    @Bean
    public SpringTemplateEngine templateEngine(ThymeleafProperties properties,
                                               ObjectProvider<ITemplateResolver> templateResolvers,
                                               ObjectProvider<IDialect> dialects) {
        var engine = new SpringTemplateEngine();
        engine.setEnableSpringELCompiler(properties.isEnableSpringElCompiler());
        engine.setRenderHiddenMarkersBeforeCheckboxes(properties.isRenderHiddenMarkersBeforeCheckboxes());
        templateResolvers.orderedStream().forEach(engine::addTemplateResolver);
        dialects.orderedStream().forEach(engine::addDialect);
        engine.addDialect(new DynamicAssetsDialect(!webpackResourcesIntegrityEnabled));
        return engine;
    }

    @Bean
    ThymeleafViewResolver thymeleafViewResolver(ThymeleafProperties properties,
                                                SpringTemplateEngine templateEngine) {
        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        resolver.setTemplateEngine(templateEngine);
        resolver.setCharacterEncoding(properties.getEncoding().name());
        resolver.setContentType(
                appendCharset(properties.getServlet().getContentType(), resolver.getCharacterEncoding()));
        resolver.setProducePartialOutputWhileProcessing(
                properties.getServlet().isProducePartialOutputWhileProcessing());
        resolver.setExcludedViewNames(properties.getExcludedViewNames());
        resolver.setViewNames(properties.getViewNames());
        resolver.setOrder(Ordered.LOWEST_PRECEDENCE - 5);
        resolver.setCache(properties.isCache());
        resolver.setViewClass(RenderContextThymeleafView.class);
        return resolver;
    }

    private String appendCharset(MimeType type, String charset) {
        if (type.getCharset() != null) {
            return type.toString();
        }
        LinkedHashMap<String, String> parameters = new LinkedHashMap<>();
        parameters.put("charset", charset);
        parameters.putAll(type.getParameters());
        return new MimeType(type, parameters).toString();
    }

    @Bean
    public LocaleResolver localeResolver() {
        return new UserProvidedValueLocaleResolver();
    }

    @Bean
    @AutoRegisterFeatureModule({I18NextTransformerDescriptor.class, I18NextResourceDescriptor.class})
    DefaultI18NextService i18NextService() {
        var i18NextService = new DefaultI18NextService();
        i18NextService.setParentDirectory(messagesParentDirectory);
        String defaultEncoding = System
                .getProperty("java.util.I18NextResourceBundle.encoding", "")
                .toUpperCase(Locale.ROOT);
        i18NextService.setDefaultEncoding(StringUtils.hasText(defaultEncoding) ? defaultEncoding : Charset.defaultCharset().toString());
        return i18NextService;
    }

    @Bean
    public MessageSource messageSource(DefaultI18NextService i18NextService) {
        var messageSource = new I18NextBundleMessageSource(i18NextService);
        messageSource.setFallbackToSystemLocale(true);
        messageSource.setCacheMillis(-1);
        messageSource.setUseCodeAsDefaultMessage(true);
        return messageSource;
    }

    @Bean
    public OsgiPluginTemplateResolver pluginTemplateResolver(ThymeleafProperties properties,
                                                             @Value("${spring.thymeleaf.osgi-template-resolver.prefix:classpath:/templates/}") String prefix,
                                                             @Value("${spring.thymeleaf.osgi-template-resolver-order:10000}") int order) {
        var resolver = new OsgiPluginTemplateResolver();
        if (prefix.startsWith(CLASSPATH_PROTOCOL)) {
            resolver.setPrefix(prefix.substring(CLASSPATH_PROTOCOL.length()));
        } else {
            resolver.setPrefix(properties.getPrefix());
        }
        resolver.setSuffix(properties.getSuffix());
        resolver.setTemplateMode(properties.getMode());
        if (properties.getEncoding() != null) {
            resolver.setCharacterEncoding(properties.getEncoding().name());
        }
        resolver.setCacheable(properties.isCache());
        resolver.setOrder(order);
        resolver.setCheckExistence(properties.isCheckTemplate());
        return resolver;
    }

    @Bean
    public OpenAPI customOpenAPI(OpenAPIConfigurer configurer) {
        return configurer.configure(new OpenAPI());
    }

    @Bean("dispatcherServletRegistration")
    public ServletRegistrationBean<SmartSwitchingServlet> dispatcherServletRegistration(SmartSwitchingServlet dispatcherServlet,
                                                                                        WebMvcProperties webMvcProperties,
                                                                                        ObjectProvider<MultipartConfigElement> multipartConfig) {
        var registration = new ServletRegistrationBean<>(dispatcherServlet, "/*");
        registration.setName("dispatcherServlet");
        registration.setLoadOnStartup(webMvcProperties.getServlet().getLoadOnStartup());
        multipartConfig.ifAvailable(registration::setMultipartConfig);
        return registration;
    }

    @Bean
    public DispatcherServletPath dispatcherServletPath(WebMvcProperties webMvcProperties) {
        return () -> webMvcProperties.getServlet().getPath();
    }
}
