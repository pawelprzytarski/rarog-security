package eu.rarogsoftware.rarog.platform.app.configuration;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.querydsl.sql.SQLTemplates;
import eu.rarogsoftware.commons.cache.CacheService;
import eu.rarogsoftware.commons.cache.DefaultCacheService;
import eu.rarogsoftware.commons.database.connection.DatabaseConnectionProvider;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.settings.HomeDirectoryHelper;
import liquibase.integration.spring.SpringLiquibase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.sql.init.dependency.DependsOnDatabaseInitialization;
import org.springframework.context.annotation.*;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.sql.DataSource;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;

@Configuration
@PropertySources(value = {
        @PropertySource("classpath:application.yml"),
        @PropertySource(value = "config:base.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "config:database.properties", ignoreResourceNotFound = true)
})
public class StandardConfiguration {
    private final Logger logger = LoggerFactory.getLogger(StandardConfiguration.class);

    public static SpringLiquibase getBaseSpringLiquibase(DataSource dataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog("classpath:db/rarog/platform/migrations/changelog.xml");
        liquibase.setDropFirst(false);
        liquibase.setDataSource(dataSource);
        return liquibase;
    }

    @Bean
    @ExportComponent
    public HomeDirectoryHelper homeDirectoryHelper() {
        return new LocalHomeDirectoryHelper();
    }

    @Bean
    @ConditionalOnMissingBean(SpringLiquibase.class)
    @ConditionalOnProperty("devMode")
    public SpringLiquibase devModeLiquibase(DataSource dataSource, ResourceLoader resourceLoader) {
        SpringLiquibase liquibase = StandardConfiguration.getBaseSpringLiquibase(dataSource);
        if (resourceLoader.getResource("classpath:/db/rarog/platform/migrations/testChangelog.xml").exists()) {
            liquibase.setChangeLog("classpath:/db/rarog/platform/migrations/testChangelog.xml");
        } else {
            liquibase.setChangeLog("classpath:/db/rarog/platform/migrations/changelog.xml");
        }
        return liquibase;
    }

    @Bean
    @ConditionalOnMissingBean(SpringLiquibase.class)
    @ConditionalOnProperty(value = "devMode", matchIfMissing = true)
    public SpringLiquibase liquibase(DataSource dataSource) {
        return StandardConfiguration.getBaseSpringLiquibase(dataSource);
    }

    @Bean(destroyMethod = "cleanupConnections")
    @DependsOnDatabaseInitialization
    @ExportComponent
    public DatabaseConnectionProvider databaseConnectionProvider(DataSource dataSource,
                                                                 @Value("${spring.datasource.querydsl-templates-class-name:com.querydsl.sql.H2Templates}") String templatesClassName,
                                                                 @Value("${spring.datasource.url:}") String connectionUrl) {
        logger.info("Using {} templates for querydsl", templatesClassName);
        logger.info("Using database connection: {}", connectionUrl);
        SQLTemplates templates;
        try {
            var templatesClass = (Class<? extends SQLTemplates>) this.getClass().getClassLoader().loadClass(templatesClassName);
            var constructor = templatesClass.getConstructor();
            templates = constructor.newInstance();
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException |
                 InvocationTargetException e) {
            logger.error("Failed to load valid querydsl templates: {}. Please provide correct class with simple constructor", templatesClassName, e);
            throw new RuntimeException(e);
        }
        return new ConfigurableDatabaseConnectionProvider(dataSource, templates);
    }

    @Bean
    @ExportComponent
    public CacheManager cacheManager() {
        return Caching.getCachingProvider("org.ehcache.jsr107.EhcacheCachingProvider").getCacheManager();
    }

    @Bean
    @ExportComponent
    public CacheService cacheService(CacheManager jCache) {
        return new DefaultCacheService(jCache);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(Jackson2ObjectMapperBuilder builder) {
        var objectMapper = builder.createXmlMapper(false).build();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
                .configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, true);
        return new MappingJackson2HttpMessageConverter(objectMapper) {
            @Override
            protected void writeInternal(Object object, Type type, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
                super.writeInternal(object, type, outputMessage);
            }
        };
    }
}
