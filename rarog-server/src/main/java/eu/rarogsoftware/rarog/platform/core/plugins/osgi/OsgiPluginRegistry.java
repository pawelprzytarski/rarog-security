package eu.rarogsoftware.rarog.platform.core.plugins.osgi;

import org.osgi.framework.Bundle;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class OsgiPluginRegistry {
    Map<Long, OsgiPlugin> bundleIdToPluginMapping = new ConcurrentHashMap<>();

    public OsgiPlugin getPlugin(Bundle bundle) {
        return bundleIdToPluginMapping.get(bundle.getBundleId());
    }

    public void registerMapping(Bundle bundle, OsgiPlugin plugin) {
        bundleIdToPluginMapping.put(bundle.getBundleId(), plugin);
    }

    public void unregisterMapping(Bundle bundle) {
        bundleIdToPluginMapping.remove(bundle.getBundleId());
    }
}
