package eu.rarogsoftware.rarog.platform.core.task;


import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;

import java.util.Objects;

class ServletContextAwareThreadWrapper extends Thread implements ServletContextListener {
    private final Thread delegate;

    public ServletContextAwareThreadWrapper(Thread delegate) {
        Objects.requireNonNull(delegate, "Delegate must not be null.");
        this.delegate = delegate;
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        this.interrupt();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("ServletContextAwareThreads are not cloneable.");
    }

    @Override
    public synchronized void start() {
        delegate.start();
    }

    @Override
    @SuppressFBWarnings
    public void run() {
        delegate.run();
    }

    @Override
    public void interrupt() {
        delegate.interrupt();
    }

    @Override
    public boolean isInterrupted() {
        return delegate.isInterrupted();
    }

    @Override
    public String toString() {
        return delegate.toString();
    }

    @Override
    public ClassLoader getContextClassLoader() {
        return delegate.getContextClassLoader();
    }

    @Override
    public void setContextClassLoader(ClassLoader cl) {
        delegate.setContextClassLoader(cl);
    }

    @Override
    public StackTraceElement[] getStackTrace() {
        return delegate.getStackTrace();
    }

    @Override
    public long getId() {
        return delegate.getId();
    }

    @Override
    public State getState() {
        return delegate.getState();
    }

    @Override
    public UncaughtExceptionHandler getUncaughtExceptionHandler() {
        return delegate.getUncaughtExceptionHandler();
    }

    @Override
    public void setUncaughtExceptionHandler(UncaughtExceptionHandler eh) {
        delegate.setUncaughtExceptionHandler(eh);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        ServletContextAwareThreadWrapper that = (ServletContextAwareThreadWrapper) o;
        return delegate.equals(that.delegate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(delegate);
    }
}
