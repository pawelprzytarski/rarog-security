package eu.rarogsoftware.rarog.platform.core.i18n;

import java.util.*;
import java.util.stream.Collectors;

public class I18NextNamespacedResourceBundle extends ResourceBundle {
    private final Map<String, ResourceBundle> bundles;
    private final Set<String> keysCache;

    public I18NextNamespacedResourceBundle(Map<String, ResourceBundle> bundles) {
        this.bundles = bundles;
        keysCache = new HashSet<>();
        bundles.forEach((namespace, value) -> {
            keysCache.addAll(value.keySet());
            keysCache.addAll(value.keySet().stream()
                    .map(key -> namespace + ":" + key)
                    .collect(Collectors.toSet()));
        });
    }

    @Override
    public Object handleGetObject(String key) {
        if (key.contains(":")) {
            String[] splitKey = key.split(":", 2);
            String namespace = splitKey[0];
            String translationKey = splitKey[1];
            if (bundles.containsKey(namespace)) {
                return bundles.get(namespace).getObject(translationKey);
            }
            throw new MissingResourceException("Can't find resource for bundle "
                    + this.getClass().getName()
                    + ", key " + key,
                    this.getClass().getName(),
                    key);
        } else {
            return findKeyInAll(key);
        }
    }

    private Object findKeyInAll(String key) {
        return bundles.values().stream()
                .filter(resourceBundle -> resourceBundle.containsKey(key))
                .map(resourceBundle -> resourceBundle.getObject(key))
                .findAny()
                .orElseThrow(() -> new MissingResourceException("Can't find resource for bundle "
                        + this.getClass().getName()
                        + ", key " + key,
                        this.getClass().getName(),
                        key)
                );
    }

    @Override
    public Enumeration<String> getKeys() {
        return Collections.enumeration(keysCache);
    }


    @Override
    public Set<String> handleKeySet() {
        return keysCache;
    }
}
