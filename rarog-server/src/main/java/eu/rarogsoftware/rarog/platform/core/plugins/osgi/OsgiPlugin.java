package eu.rarogsoftware.rarog.platform.core.plugins.osgi;

import eu.rarogsoftware.rarog.platform.api.plugins.*;
import eu.rarogsoftware.rarog.platform.api.plugins.*;
import org.apache.felix.resolver.util.CopyOnWriteSet;
import org.osgi.framework.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.AntPathMatcher;

import java.net.URL;
import java.util.*;

public class OsgiPlugin implements ResourceHoldingPlugin, SynchronousBundleListener {
    private final Logger logger = LoggerFactory.getLogger(OsgiPlugin.class);
    private final Bundle bundle;
    private final PluginManifest manifest;
    private final Set<PluginStateListener> stateListeners = new CopyOnWriteSet<>();
    private final AntPathMatcher pathMatcher = new AntPathMatcher();
    private PluginState pluginState = PluginState.INSTALLED;
    private ServiceReference<PluginActivator> serviceReference;

    public OsgiPlugin(PluginManifest manifest, Bundle bundle) {
        this.bundle = bundle;
        this.manifest = manifest;
    }

    @Override
    public PluginManifest getManifest() {
        return manifest;
    }

    @Override
    public boolean isEnabled() {
        return pluginState == PluginState.ENABLED;
    }

    @Override
    public PluginState getState() {
        return pluginState;
    }

    public void enable() throws PluginException {
        try {
            bundle.start();
        } catch (BundleException e) {
            throw new PluginException(e);
        }
    }

    public void disable() throws PluginException {
        try {
            bundle.stop();
        } catch (BundleException e) {
            throw new PluginException(e);
        }
    }

    @Override
    public void addPluginStateListener(PluginStateListener listener) {
        stateListeners.add(listener);
    }

    @Override
    public void removePluginStateListener(PluginStateListener listener) {
        stateListeners.remove(listener);
    }

    public Bundle getBundle() {
        return bundle;
    }

    public PluginActivator getPluginActivator() throws PluginException {
        if (bundle == null) {
            throw new IllegalStateException("Attempted to get PluginActivator from not enabled plugin");
        }
        if (serviceReference == null) {
            logger.debug("Retrieving PluginActivator service for plugin {}", getKey());
            ServiceReference<?>[] registeredServices = bundle.getRegisteredServices();
            if (registeredServices == null) {
                return null;
            }
            serviceReference = (ServiceReference<PluginActivator>) Arrays.stream(registeredServices)
                    .filter(reference -> Arrays.asList(((String[]) reference.getProperty("objectClass"))).contains(PluginActivator.class.getCanonicalName()))
                    .findAny()
                    .orElse(null);
            logger.trace("ServiceReference found for PluginActivator in plugin {}", getKey());
            if (serviceReference == null) {
                return null;
            }
        }
        logger.trace("Adapting PluginActivator in plugin {}", getKey());
        return bundle.getBundleContext().getService(serviceReference);
    }

    public void freePluginActivator() {
        if (bundle == null) {
            throw new IllegalStateException("Attempted to get PluginActivator from not enabled plugin");
        }
        logger.debug("Freeing PluginActivator service for plugin {}", getKey());
        if (serviceReference != null) {
            bundle.getBundleContext().ungetService(serviceReference);
            serviceReference = null;
        }
    }

    @Override
    public void bundleChanged(BundleEvent event) {
        if (bundle == event.getBundle()) {
            logger.debug("OSGi bundle change detected for plugin {}. OSGi bundle change {}", getKey(), event.getType());
            var newState = switch (event.getType()) {
                case BundleEvent.STARTED -> PluginState.ENABLED;
                case BundleEvent.INSTALLED, BundleEvent.STOPPED -> PluginState.INSTALLED;
                case BundleEvent.STARTING -> PluginState.ENABLING;
                case BundleEvent.STOPPING -> PluginState.DISABLING;
                case BundleEvent.UNINSTALLED -> PluginState.UNINSTALLED;
                default -> pluginState;
            };
            changeState(newState);
        }
    }

    private void changeState(PluginState newState) {
        var oldState = pluginState;
        pluginState = newState;
        if (newState != oldState) {
            logger.debug("Plugin {} state changed {}->{}.", getKey(), oldState, newState);
            stateListeners.forEach(listener -> listener.stateChanged(oldState, newState));
            logger.trace("Plugin {} state change {}->{}. StateListeners ended successfully", getKey(), oldState, newState);
        }
    }

    public void markActive() {
        changeState(PluginState.ACTIVE);
    }

    public void markNotActive() {
        changeState(PluginState.ENABLED);
    }

    @Override
    public Resource getResource(String path) {
        URL resource = bundle.getResource(path);
        if (resource != null) {
            return new UrlResource(resource);
        }
        return null;
    }

    @Override
    public Collection<Resource> findResources(String path) {
        if (pathMatcher.isPattern(path)) {
            List<Resource> resources = new ArrayList<>();
            Enumeration<URL> entries = bundle.findEntries("/", "*", true);
            while (entries.hasMoreElements()) {
                var element = entries.nextElement();
                if (pathMatcher.match(path, element.getPath())) {
                    resources.add(new UrlResource(element));
                }
            }
            return resources;
        }
        return null;
    }
}
