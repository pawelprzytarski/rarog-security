package eu.rarogsoftware.rarog.platform.dynamic.frontend.resources;

import eu.rarogsoftware.commons.cache.AbstractCacheLoader;
import eu.rarogsoftware.commons.cache.CacheService;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.ResourceHoldingPlugin;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginManager;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModuleComponent;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsDescriptor;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoader;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoaderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import javax.cache.Cache;
import javax.cache.integration.CacheLoaderException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@AutoRegisterFeatureModuleComponent(DynamicAssetsDescriptor.class)
@ExportComponent
public class PluginAwareDynamicAssetsLoaderFactory implements DynamicAssetsLoaderFactory, FeatureModule<DynamicAssetsDescriptor> {
    private final Logger logger = LoggerFactory.getLogger(PluginAwareDynamicAssetsLoaderFactory.class);
    private final Map<String, List<DynamicAssetsDescriptor>> descriptorsMap = new ConcurrentHashMap<>();
    private final Cache<String, Optional<DynamicAssetsLoader>> loaderCache;
    private final PluginManager pluginManager;
    @Value("${webpack.manifest.cache.duration:-1}")
    private Long cacheDuration = -1L;

    public PluginAwareDynamicAssetsLoaderFactory(CacheService cacheService,
                                                 PluginManager pluginManager) {
        this.pluginManager = pluginManager;
        loaderCache = getLoadersCache(cacheService);
    }

    private Cache<String, Optional<DynamicAssetsLoader>> getLoadersCache(CacheService cacheService) {
        var cacheBuilder = cacheService.<String, Optional<DynamicAssetsLoader>>getBuilder()
                .name(PluginAwareDynamicAssetsLoaderFactory.class.getName() + "_cache")
                .storeByValue(false)
                .cacheLoader(new AbstractCacheLoader<>() {
                    @Override
                    public Optional<DynamicAssetsLoader> load(String key) throws CacheLoaderException {
                        return loadWebpackAssetsLoader(key);
                    }
                });
        if (cacheDuration >= 0) {
            cacheBuilder
                    .expireAfterAccess(Duration.ofSeconds(cacheDuration));
        } else {
            cacheBuilder
                    .eternal();
        }
        return cacheBuilder.build();
    }

    private Optional<DynamicAssetsLoader> loadWebpackAssetsLoader(String key) {
        logger.debug("Loading manifest for {}", key);
        var split = key.split("\\|", 2);
        var pluginKey = split[0];
        var namespace = split.length == 2 ? split[1] : "main";
        var descriptors = descriptorsMap.get(pluginKey);
        var plugin = pluginManager.getPlugin(pluginKey);
        if (descriptors == null || !(plugin instanceof ResourceHoldingPlugin resourcePlugin)) {
            return Optional.empty();
        }

        var duration = PluginAwareDynamicAssetsLoaderFactory.this.cacheDuration;
        return descriptors.stream()
                .map(DynamicAssetsDescriptor::descriptorsPlaceholder)
                .map(path -> path.replace("{namespace}", namespace))
                .map(resourcePlugin::getResource)
                .filter(Objects::nonNull)
                .filter(Resource::exists)
                .map(resource -> (DynamicAssetsLoader) new ResourceDynamicAssetsLoader(pluginKey, resource, duration))
                .findFirst();
    }

    @Override
    public Optional<DynamicAssetsLoader> getNamedAssetsLoader(String name) {
        return loaderCache.get(name);
    }

    @Override
    public void plugDescriptor(Plugin plugin, DynamicAssetsDescriptor descriptor) {
        logger.trace("Plugging descriptor {} for plugin {}", descriptor, plugin.getKey());
        descriptorsMap.compute(plugin.getKey(), (key, list) -> {
            if (list == null) {
                list = Collections.synchronizedList(new ArrayList<>());
            }
            list.add(descriptor);
            return list;
        });
        loaderCache.clear();
        logger.debug("Plugged descriptor {} for plugin {}", descriptor, plugin.getKey());
    }

    @Override
    public void unplugDescriptor(Plugin plugin, DynamicAssetsDescriptor descriptor) {
        logger.trace("Unplugging descriptor {} for plugin {}", descriptor, plugin.getKey());
        descriptorsMap.compute(plugin.getKey(), (key, list) -> {
            if (list == null) {
                return null;
            }
            list.remove(descriptor);
            if (list.isEmpty()) {
                return null;
            }
            return list;
        });
        loaderCache.clear();
        logger.debug("Unplugged descriptor {} for plugin {}", descriptor, plugin.getKey());
    }
}
