package eu.rarogsoftware.rarog.platform.core.security.anonymous;

import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import eu.rarogsoftware.rarog.platform.core.security.AbstractMethodAnnotatedRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;

import java.util.List;

@Component
public class AnonymousAllowedMatcher extends AbstractMethodAnnotatedRequestMatcher {
    public AnonymousAllowedMatcher(List<HandlerMapping> handlerMappings) {
        super(handlerMappings, AnonymousAllowed.class);
    }
}
