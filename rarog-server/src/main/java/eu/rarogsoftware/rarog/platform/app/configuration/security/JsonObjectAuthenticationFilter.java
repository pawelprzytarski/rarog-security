package eu.rarogsoftware.rarog.platform.app.configuration.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.rarog.platform.api.user.management.AuthenticationType;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class JsonObjectAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        try {
            BufferedReader reader = request.getReader();
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            LoginCredentials authRequest = objectMapper.readValue(sb.toString(), LoginCredentials.class);
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    authRequest.getUsername(),
                    authRequest.getPassword()
            );
            setDetails(request, token);
            var authenticate = this.getAuthenticationManager().authenticate(token);
            if (authenticate.isAuthenticated()) {
                var extendedCredentials = new ArrayList<GrantedAuthority>(authenticate.getAuthorities());
                extendedCredentials.add(AuthenticationType.ORGANIC.getAuthority());
                authenticate = UsernamePasswordAuthenticationToken.authenticated(
                        authenticate.getPrincipal(),
                        authenticate.getCredentials(),
                        Collections.unmodifiableCollection(extendedCredentials)
                );
            }
            return authenticate;
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public static class LoginCredentials {
        private String username;
        private String password;

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }
    }
}
