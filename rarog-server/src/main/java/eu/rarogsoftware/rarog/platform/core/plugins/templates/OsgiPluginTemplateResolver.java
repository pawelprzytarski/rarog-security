package eu.rarogsoftware.rarog.platform.core.plugins.templates;

import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.ResourceHoldingPlugin;
import org.springframework.web.context.request.RequestContextHolder;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.templateresolver.AbstractConfigurableTemplateResolver;
import org.thymeleaf.templateresource.ITemplateResource;

import java.util.Map;
import java.util.Objects;

import static eu.rarogsoftware.rarog.platform.core.plugins.web.PluginMappingHandlerMapping.HANDLER_PLUGIN_ATTRIBUTE;
import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

public class OsgiPluginTemplateResolver extends AbstractConfigurableTemplateResolver {
    @Override
    protected String computeResourceName(IEngineConfiguration configuration, String ownerTemplate, String template, String prefix, String suffix, boolean forceSuffix, Map<String, String> templateAliases, Map<String, Object> templateResolutionAttributes) {
        return super.computeResourceName(configuration, ownerTemplate, template, prefix, suffix, forceSuffix, templateAliases, templateResolutionAttributes);
    }

    @Override
    protected ITemplateResource computeTemplateResource(IEngineConfiguration configuration, String ownerTemplate, String template, String resourceName, String characterEncoding, Map<String, Object> templateResolutionAttributes) {
        var plugin = (Plugin) Objects.requireNonNull(RequestContextHolder.getRequestAttributes()).getAttribute(HANDLER_PLUGIN_ATTRIBUTE, SCOPE_REQUEST);
        if (!(plugin instanceof ResourceHoldingPlugin)) {
            return new OsgiTemplateResource(null, resourceName, characterEncoding);
        } else {
            return new OsgiTemplateResource(((ResourceHoldingPlugin) plugin), resourceName, characterEncoding);
        }
    }
}
