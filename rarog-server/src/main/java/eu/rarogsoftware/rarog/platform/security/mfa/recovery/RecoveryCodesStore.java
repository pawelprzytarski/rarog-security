package eu.rarogsoftware.rarog.platform.security.mfa.recovery;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;

import java.util.List;

public interface RecoveryCodesStore {
    int getCodesCount(StandardUser user);

    boolean removeCode(StandardUser user, String code);

    void saveCodes(StandardUser user, List<String> codes);

    void replaceCodes(StandardUser user, List<String> codes);
}
