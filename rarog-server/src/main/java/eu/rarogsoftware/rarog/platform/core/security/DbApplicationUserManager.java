package eu.rarogsoftware.rarog.platform.core.security;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.commons.database.connection.DatabaseConnectionException;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.api.user.management.UserManagementException;
import eu.rarogsoftware.rarog.platform.api.user.management.UserNotFoundException;
import eu.rarogsoftware.rarog.platform.api.user.management.UserStore;
import eu.rarogsoftware.rarog.platform.core.user.management.SimpleUserManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
@SuppressFBWarnings(value = "HARD_CODE_PASSWORD", justification = "Child: Mom can we have SAST? Mom: No we have SAST at home. SAST at home:")
public class DbApplicationUserManager extends ApplicationUserManager {
    private final SimpleUserManager userManager;
    private final UserStore userStore;


    public DbApplicationUserManager(SimpleUserManager userManager,
                                    UserStore userStore) {
        this.userManager = userManager;
        this.userStore = userStore;
    }

    @Override
    public void createUser(UserDetails user) {
        try {
            userManager.createUser(StandardUser.builder()
                    .username(user.getUsername())
                    .enabled(user.isEnabled())
                    .password("")
                    .authorities(new HashSet<>(user.getAuthorities()))
                    .credentialsNonExpired(user.isCredentialsNonExpired())
                    .accountNonExpired(user.isAccountNonExpired())
                    .accountNonLocked(user.isAccountNonLocked())
                    .build());
            if (user.getPassword() != null) {
                storeNewPassword(user.getUsername(), user.getPassword());
            }
        } catch (UserManagementException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteUser(String username) {
        try {
            userManager.deleteUser(username);
        } catch (UserNotFoundException e) {
            throw new UsernameNotFoundException(e.getMessage(), e);
        } catch (UserManagementException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateUser(UserDetails user) {
        try {
            userManager.updateUser(StandardUser.builder()
                    .username(user.getUsername())
                    .enabled(user.isEnabled())
                    .authorities(new HashSet<>(user.getAuthorities()))
                    .credentialsNonExpired(user.isCredentialsNonExpired())
                    .accountNonExpired(user.isAccountNonExpired())
                    .accountNonLocked(user.isAccountNonLocked())
                    .build());
            if (user.getPassword() != null) {
                storeNewPassword(user.getUsername(), user.getPassword());
            }
        } catch (UserNotFoundException e) {
            throw new UsernameNotFoundException(e.getMessage(), e);
        } catch (UserManagementException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean userExists(String username) {
        return userManager.userExists(username);
    }

    @Override
    protected void storeNewPassword(String username, String newPassword) {
        if (!userExists(username)) {
            throw new UsernameNotFoundException("User with name %s does not exist".formatted(username));
        }
        try {
            userStore.updatePassword(username, newPassword);
        } catch (UserManagementException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public UserDetails updatePassword(UserDetails user, String newPassword) {
        deencapsulateInnerException(() -> storeNewPassword(user.getUsername(), newPassword));
        if (user instanceof StandardUser appUser) {
            appUser.setPassword(newPassword);
            return appUser;
        }
        return loadUserByUsername(user.getUsername());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            return userManager.getUser(username);
        } catch (UserNotFoundException e) {
            throw new UsernameNotFoundException(e.getMessage(), e);
        } catch (UserManagementException e) {
            throw new RuntimeException(e);
        }
    }

    private void deencapsulateInnerException(Runnable runnable) {
        try {
            runnable.run();
        } catch (DatabaseConnectionException e) {
            if (e.getCause() instanceof UsernameNotFoundException inner) {
                throw inner;
            }
            throw e;
        }
    }
}
