package eu.rarogsoftware.rarog.platform.security.mfa;

import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaMethod;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaMethodsRegister;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
public class BuildInMfaMethodsRegister implements MfaMethodsRegister {
    private static final Set<MfaMethod> methods = new HashSet<>();
    public static final MfaMethod RECOVERY_CODES = registerMethod(MfaMethod.builder()
            .name("RECOVERY-CODE")
            .imageUrl("/static/images/codes.png")
            .nameKey("mfa.methods.recovery-codes")
            .descriptionKey("mfa.methods.recovery-codes.description")
            .order(10000L)
    );
    public static final MfaMethod TOTP = registerMethod(MfaMethod.builder()
            .name("TOTP")
            .imageUrl("/static/images/email-authentication.png")
            .nameKey("mfa.methods.totp")
            .descriptionKey("mfa.methods.totp.description")
    );

    private static MfaMethod registerMethod(MfaMethod.Builder builder) {
        MfaMethod built = builder.build();
        methods.add(built);
        return built;
    }

    @Override
    public Set<MfaMethod> getMfaMethods() {
        return Collections.unmodifiableSet(methods);
    }
}
