package eu.rarogsoftware.rarog.platform.core.security;

public class UserChangeException extends RuntimeException {
    public UserChangeException() {
    }

    public UserChangeException(String message) {
        super(message);
    }

    public UserChangeException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserChangeException(Throwable cause) {
        super(cause);
    }

    public UserChangeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
