package eu.rarogsoftware.rarog.platform.core.plugins.templates;

import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.templates.PluginTemplateResolver;
import eu.rarogsoftware.rarog.platform.api.plugins.templates.TemplateResolverDescriptor;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModuleComponent;
import org.springframework.beans.factory.annotation.Value;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolution;

import java.util.*;

@AutoRegisterFeatureModuleComponent(TemplateResolverDescriptor.class)
public class PluginModuleTemplateResolver extends AbstractSimpleFeatureModule<TemplateResolverDescriptor> implements ITemplateResolver {
    @Value("${spring.thymeleaf.plugin-template-resolver-order:}")
    Integer order;
    private List<ITemplateResolver> templateResolvers = null;

    @Override
    public String getName() {
        return this.getClass().getName();
    }

    @Override
    public Integer getOrder() {
        if (order != null) {
            return order;
        }
        return Integer.MAX_VALUE / 2;
    }

    @Override
    public TemplateResolution resolveTemplate(IEngineConfiguration configuration, String ownerTemplate, String template, Map<String, Object> templateResolutionAttributes) {
        List<ITemplateResolver> resolvers;
        Plugin plugin = getPluginForCurrentRequest();
        synchronized (this) {
            if (templateResolvers == null) {
                templateResolvers = calculateResolvers();
            }
            resolvers = new ArrayList<>(templateResolvers);
        }
        return resolvers.stream()
                .map(resolver -> resolver instanceof PluginTemplateResolver pluginResolver && plugin != null
                        ? pluginResolver.resolveTemplate(configuration, ownerTemplate, template, templateResolutionAttributes, plugin)
                        : resolver.resolveTemplate(configuration, ownerTemplate, template, templateResolutionAttributes))
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
    }

    private List<ITemplateResolver> calculateResolvers() {
        return getDescriptorMap().values().stream()
                .flatMap(descriptor -> descriptor.templateResolvers().get().stream())
                .sorted(Comparator.comparingInt(ITemplateResolver::getOrder))
                .toList();
    }

    @Override
    protected void clearCache() {
        synchronized (this) {
            templateResolvers = null;
        }
    }
}
