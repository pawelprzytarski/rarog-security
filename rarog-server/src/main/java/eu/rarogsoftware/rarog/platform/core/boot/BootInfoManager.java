package eu.rarogsoftware.rarog.platform.core.boot;

import eu.rarogsoftware.rarog.platform.app.boot.SpringBootManager;

import jakarta.servlet.DispatcherType;
import jakarta.servlet.ServletContext;
import java.util.EnumSet;

public class BootInfoManager {
    private final BootServlet bootServlet = new BootServlet();
    private final BootFilter bootFilter = new BootFilter();

    public void registerReplacements(ServletContext context) {
        EnumSet<DispatcherType> request = EnumSet.of(DispatcherType.REQUEST, DispatcherType.ASYNC, DispatcherType.FORWARD);
        var servletRegistration = context.addServlet("bootServlet", bootServlet);
        var filterRegistration = context.addFilter("bootFilter", bootFilter);
        servletRegistration.setLoadOnStartup(1);
        servletRegistration.addMapping("/boot/*");
        filterRegistration.addMappingForUrlPatterns(request, false, "/*");
    }

    public void updateStatus(SpringBootManager.StartState startState, SpringBootManager.AsyncStartState asyncStartState) {
        bootServlet.updateStatus(startState, asyncStartState);
    }

    public void disableReplacements() {
        bootFilter.setRedirectEnabled(false);
        bootServlet.setRedirectEnabled(false);
    }

    public void setError(String errorMessage, RuntimeException e) {
        // do nothing yet
    }
}
