package eu.rarogsoftware.rarog.platform.core.plugins.web.filters;

import jakarta.servlet.Filter;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.SecurityFilterChain;

import java.util.List;

public class PluggableSecurityFilterChain implements SecurityFilterChain {
    private final DefaultSecurityFilterChain defaultChain;
    private final PluginSecurityFilterResolver pluginSecurityFilterResolver;

    public PluggableSecurityFilterChain(DefaultSecurityFilterChain defaultChain, PluginSecurityFilterResolver pluginSecurityFilterResolver) {
        this.defaultChain = defaultChain;
        this.pluginSecurityFilterResolver = pluginSecurityFilterResolver;
    }

    @Override
    public boolean matches(HttpServletRequest request) {
        return defaultChain.matches(request);
    }

    @Override
    public List<Filter> getFilters() {
        return pluginSecurityFilterResolver.getUpdatedFilters(defaultChain.getFilters());
    }
}
