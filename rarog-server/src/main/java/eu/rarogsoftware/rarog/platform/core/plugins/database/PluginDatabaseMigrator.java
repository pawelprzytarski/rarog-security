package eu.rarogsoftware.rarog.platform.core.plugins.database;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.commons.database.connection.DatabaseConnectionException;
import eu.rarogsoftware.commons.database.connection.DatabaseConnectionProvider;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.PurgeableFeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.database.DatabaseMigrationsDescriptor;
import eu.rarogsoftware.rarog.platform.core.plugins.osgi.OsgiPlugin;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModuleComponent;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseConnection;
import liquibase.database.DatabaseFactory;
import liquibase.database.OfflineConnection;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.OSGiResourceAccessor;
import liquibase.resource.ResourceAccessor;
import liquibase.util.MD5Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Locale;

@AutoRegisterFeatureModuleComponent(DatabaseMigrationsDescriptor.class)
public class PluginDatabaseMigrator implements PurgeableFeatureModule<DatabaseMigrationsDescriptor> {
    private final Logger log = LoggerFactory.getLogger(PluginDatabaseMigrator.class);

    private final DatabaseConnectionProvider databaseConnectionProvider;

    public PluginDatabaseMigrator(DatabaseConnectionProvider databaseConnectionProvider) {
        this.databaseConnectionProvider = databaseConnectionProvider;
    }

    @Override
    public void plugDescriptor(Plugin plugin, DatabaseMigrationsDescriptor descriptor) {
        if (plugin instanceof OsgiPlugin osgiPlugin) {
            doMigrations(plugin, descriptor, osgiPlugin);
        } else {
            throw new IllegalArgumentException("Only OSGi based plugins are supported at this time");
        }
    }

    private void doMigrations(Plugin plugin, DatabaseMigrationsDescriptor descriptor, OsgiPlugin osgiPlugin) {
        var resourceAccessor = getResourceAccessor(osgiPlugin);
        try (var liquibase = new Liquibase(descriptor.changelogFile(), resourceAccessor, getDatabase(plugin, resourceAccessor))) {

            var classLoader = Thread.currentThread().getContextClassLoader();
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            liquibase.update("");
            Thread.currentThread().setContextClassLoader(classLoader);
        } catch (LiquibaseException e) {
            throw new RuntimeException(e);
        }
    }

    private Database getDatabase(Plugin plugin, ResourceAccessor resourceAccessor) throws DatabaseException {
        DatabaseConnection liquibaseConnection;
        try {
            var connection = databaseConnectionProvider.getConnectionFactory().createConnection();
            liquibaseConnection = new JdbcConnection(connection);
        } catch (DatabaseConnectionException e) {
            log.warn("Failed to obtain connection to database. Using offline unknown database", e);
            liquibaseConnection = new OfflineConnection("offline:unknown", resourceAccessor);
        }

        var database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(liquibaseConnection);
        var changelogPrefix = MD5Util.computeMD5(plugin.getKey()).toUpperCase(Locale.ROOT);

        database.setDatabaseChangeLogLockTableName("DATABASECHANGELOGLOCK_" + changelogPrefix);
        database.setDatabaseChangeLogTableName("DATABASECHANGELOG_" + changelogPrefix);

        return database;
    }

    private ResourceAccessor getResourceAccessor(OsgiPlugin osgiPlugin) {
        return new OSGiResourceAccessor(osgiPlugin.getBundle());
    }

    @Override
    public void unplugDescriptor(Plugin plugin, DatabaseMigrationsDescriptor descriptor) {
        // do nothing. we want to keep database
    }

    @Override
    public void purgeDescriptor(Plugin plugin, DatabaseMigrationsDescriptor descriptor) {
        if (plugin instanceof OsgiPlugin osgiPlugin) {
            doPurge(plugin, descriptor, osgiPlugin);
        } else {
            throw new IllegalArgumentException("Only OSGi based plugins are supported at this time");
        }
    }

    @SuppressFBWarnings(value = "SQL_INJECTION_JDBC", justification = "There are used only hashes. No risk here")
    private void doPurge(Plugin plugin, DatabaseMigrationsDescriptor descriptor, OsgiPlugin osgiPlugin) {
        var resourceAccessor = getResourceAccessor(osgiPlugin);
        try (var liquibase = new Liquibase(descriptor.changelogFile(), resourceAccessor, getDatabase(plugin, resourceAccessor))) {

            var classLoader = Thread.currentThread().getContextClassLoader();
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            liquibase.rollback(Integer.MAX_VALUE, "");
            Thread.currentThread().setContextClassLoader(classLoader);
            try (var connection = databaseConnectionProvider.getConnectionFactory().createConnection();
                 var statement = connection.createStatement()) {
                var changelogPrefix = MD5Util.computeMD5(plugin.getKey()).toUpperCase(Locale.ROOT);
                statement.execute("DROP TABLE DATABASECHANGELOGLOCK_" + changelogPrefix);
                statement.execute("DROP TABLE DATABASECHANGELOG_" + changelogPrefix);
            } catch (SQLException e) {
                log.error("Failed to remove liquibase changelog tables");
            }
        } catch (LiquibaseException e) {
            throw new RuntimeException(e);
        }
    }
}
