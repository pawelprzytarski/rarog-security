package eu.rarogsoftware.rarog.platform.core.security.secrets;

import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.security.secrets.AuthConfiguredSocketFactoryService;
import org.springframework.stereotype.Component;

import javax.net.SocketFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Component
@ExportComponent
public class DefaultAuthConfiguredSocketFactoryService extends AbstractAuthConfigService<SocketFactory> implements AuthConfiguredSocketFactoryService {
    private final BuildInSecretsStorage buildinSecretsStorage;
    private final List<X509Certificate> trustedCertificates = new ArrayList<>();

    public DefaultAuthConfiguredSocketFactoryService(BuildInSecretsStorage buildInSecretsStorage) {
        super(buildInSecretsStorage,
                SocketFactory::getDefault,
                Map.of());
        this.buildinSecretsStorage = buildInSecretsStorage;
        addResolver(CERT_TYPE, new CertConfigResolver());
    }

    @Override
    public AuthConfiguredSocketFactoryService extendTrustedCertificates(Collection<X509Certificate> certificate) {
        var service = new DefaultAuthConfiguredSocketFactoryService(buildinSecretsStorage);
        service.trustedCertificates.addAll(trustedCertificates);
        service.trustedCertificates.addAll(certificate);
        return service;
    }

    private class CertConfigResolver implements Resolver<SocketFactory> {
        @Override
        public SocketFactory resolve(String authData) {
            return new SslContextConfig(authData).addTrustedCertificates(trustedCertificates).getSslContext().getSocketFactory();
        }

        @Override
        public SocketFactory resolve(Map<String, Object> authData) {
            return new SslContextConfig(authData).addTrustedCertificates(trustedCertificates).getSslContext().getSocketFactory();
        }
    }
}
