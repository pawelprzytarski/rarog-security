package eu.rarogsoftware.rarog.platform.core.metrics.prometheus;

import eu.rarogsoftware.rarog.platform.api.metrics.Counter;

class PrometheusCounter extends PrometheusMetric implements Counter {
    private final io.prometheus.client.Counter delegate;
    private final io.prometheus.client.Counter.Child child;

    public PrometheusCounter(io.prometheus.client.Counter counter) {
        this(counter, null);
    }

    public PrometheusCounter(io.prometheus.client.Counter parent, io.prometheus.client.Counter.Child child) {
        super(parent);
        this.delegate = parent;
        this.child = child;
    }

    @Override
    public void increase(double amount) {
        if (child == null) {
            delegate.inc(amount);
        } else {
            child.inc(amount);
        }
    }

    @Override
    public Counter labels(String... labels) {
        return new PrometheusCounter(delegate, delegate.labels(labels));
    }
}
