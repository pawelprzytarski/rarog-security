package eu.rarogsoftware.rarog.platform.app.servlets;

import eu.rarogsoftware.rarog.platform.api.security.anonymous.AnonymousAllowed;
import eu.rarogsoftware.rarog.platform.api.user.management.RarogUser;
import eu.rarogsoftware.rarog.platform.core.common.HomePageService;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Hidden
public class HomeController {
    private final HomePageService homePageService;

    public HomeController(HomePageService homePageService) {
        this.homePageService = homePageService;
    }

    @AnonymousAllowed
    @GetMapping(value = "/")
    String redirectToUserHome(@AuthenticationPrincipal RarogUser rarogUser) {
        return "redirect:" + homePageService.getHomePageForUser(rarogUser);
    }
}
