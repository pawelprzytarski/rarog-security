package eu.rarogsoftware.rarog.platform.app.configuration;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ProtocolResolver;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

public class ConfigDirectoryProtocolResolver implements ProtocolResolver {
    private static final String CONFIG_PROTOCOL = "config:";
    private final Logger logger = LoggerFactory.getLogger(ConfigDirectoryProtocolResolver.class);

    @Override
    public Resource resolve(String location, ResourceLoader resourceLoader) {
        if (location.startsWith(CONFIG_PROTOCOL)) {
            logger.trace("Started resolving location: {}", location);
            String absolutePath = LocalHomeDirectoryHelper.getInstance().getConfigFile().getAbsolutePath();
            if (!absolutePath.endsWith("/") || !absolutePath.endsWith("\\")) {
                absolutePath = absolutePath + "/";
            }
            absolutePath = "file:" + absolutePath + StringUtils.removeStart(location, CONFIG_PROTOCOL);
            logger.trace("Calculated absolute path: {}", absolutePath);
            Resource resource = resourceLoader.getResource(absolutePath);
            if (!resource.exists()) {
                logger.debug("Resource {} doesn't exist", absolutePath);
                return null;
            }
            logger.debug("Resource {} found", absolutePath);
            return resource;
        }
        return null;
    }
}
