package eu.rarogsoftware.rarog.platform.core.i18n;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.data.util.Pair;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.Pattern;

public class I18NextResourceBundle extends ResourceBundle {
    private final Logger logger = LoggerFactory.getLogger(I18NextResourceBundle.class);
    private static final String encoding = System
            .getProperty("java.util.I18NextResourceBundle.encoding", "")
            .toUpperCase(Locale.ROOT);
    private static final Pattern i18NextSubstitutionPattern = Pattern.compile("\\{\\{.*}");

    private final Map<String, Object> lookup;

    public I18NextResourceBundle(InputStream stream) throws IOException {
        this(new InputStreamReader(stream,
                "".equals(encoding) ? Charset.defaultCharset() :
                        Charset.forName(encoding)));
    }

    public I18NextResourceBundle(Reader reader) throws IOException {
        try (ByteArrayOutputStream os = new ByteArrayOutputStream();
             var writer = new OutputStreamWriter(os)) {
            reader.transferTo(writer);
            writer.flush();
            lookup = parse(os.toString());
        }
    }

    @SuppressWarnings({"unchecked"})
    private Map<String, Object> parse(String json) {
        logger.trace("Parsing json of size {} to properties", json.length());
        logger.trace("Parsing json to properties: {}", json);
        Map<String, Object> transformedMap = new HashMap<>();
        ArrayDeque<Pair<String, Map<String, Object>>> mapsToTransform = new ArrayDeque<>();
        mapsToTransform.add(Pair.of("", JsonParserFactory.getJsonParser().parseMap(json)));

        while (!mapsToTransform.isEmpty()) {
            Pair<String, Map<String, Object>> parsedMap = mapsToTransform.pop();
            parsedMap.getSecond().forEach((key, value) -> {
                var parentKey = getParentKey(parsedMap, key);
                var currentKey = parentKey + key;
                if (value instanceof Map<?, ?>) {
                    logger.trace("Add subtree {} to parse", currentKey);
                    mapsToTransform.add(Pair.of(currentKey, (Map<String, Object>) value));
                } else {
                    if (value instanceof String) {
                        value = i18NextSubstitutionPattern.matcher(value.toString()).replaceAll("{}");
                    }
                    logger.trace("Saving translation {} under key {}", value, currentKey);
                    transformedMap.put(currentKey, value);
                }
            });
        }
        logger.debug("Transformed json to {} property entries", transformedMap.keySet().size());
        return transformedMap;
    }

    private static String getParentKey(Pair<String, Map<String, Object>> parsedMap, String key) {
        if (StringUtils.hasText(parsedMap.getFirst())) {
            if (!StringUtils.hasText(key)) {
                return parsedMap.getFirst();
            }
            return parsedMap.getFirst() + ".";
        }
        return "";
    }

    @Override
    public Object handleGetObject(String key) {
        return lookup.get(key);
    }

    @Override
    public Enumeration<String> getKeys() {
        return Collections.enumeration(lookup.keySet());
    }


    @Override
    protected Set<String> handleKeySet() {
        return lookup.keySet();
    }
}
