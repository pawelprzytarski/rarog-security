package eu.rarogsoftware.rarog.platform.core.common;

import eu.rarogsoftware.rarog.platform.api.user.management.RarogUser;

public interface HomePageService {
    String getHomePageForUser(RarogUser rarogUser);
}
