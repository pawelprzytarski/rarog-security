package eu.rarogsoftware.rarog.platform.core.security.secrets;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import okhttp3.tls.HandshakeCertificates;
import okhttp3.tls.HeldCertificate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.*;

public class SslContextConfig {
    private final Logger logger = LoggerFactory.getLogger(SslContextConfig.class);
    private final Map<String, Object> authenticationData;
    private final List<X509Certificate> additionalTrustedCerts = new ArrayList<>();
    private SSLContext sslCtx;

    public SslContextConfig(Map<String, Object> authenticationData) {
        this.authenticationData = authenticationData;
    }

    public SslContextConfig(String parameters) {
        this(parseParameters(parameters));
    }

    protected static Map<String, Object> parseParameters(String parameters) {
        if (StringUtils.isBlank(parameters)) {
            throw new IllegalArgumentException("Configuration string cannot be empty");
        }
        var result = new HashMap<String, Object>();
        var pairs = parameters.split("(?<!\\\\),");
        for (var pair : pairs) {
            var separated = pair.split("(?<!\\\\)=", 2);
            result.put(SslContextConfig.removeSanitization(separated[0]), SslContextConfig.removeSanitization(separated[1]));
        }
        return result;
    }

    private static String removeSanitization(String sanitized) {
        return sanitized.replace("\\=", "=").replace("\\,", ",").replace("\\\\", "\\");
    }

    public synchronized SslContextConfig addTrustedCertificate(X509Certificate certificate) {
        additionalTrustedCerts.add(certificate);
        return this;
    }

    public synchronized SslContextConfig addTrustedCertificates(Collection<X509Certificate> certificates) {
        additionalTrustedCerts.addAll(certificates);
        return this;
    }

    public synchronized SSLContext getSslContext() {
        if (sslCtx == null) {
            sslCtx = prepareSslContextForMutualTls(authenticationData);
        }
        return sslCtx;
    }

    protected SSLContext prepareSslContextForMutualTls(Map<String, Object> authenticationData) {
        try (var certStream = readFromData(authenticationData, "cert_bytes", "cert");
             var privKeyStream = readFromData(authenticationData, "privkey_bytes", "privkey")) {
            if (certStream == null || privKeyStream == null) {
                throw new IllegalArgumentException("Either certificate or private key could not be found. Both need to be provided");
            }
            var certString = new String(certStream.readAllBytes(), StandardCharsets.UTF_8);
            var keyString = new String(privKeyStream.readAllBytes(), StandardCharsets.UTF_8);
            var certificate = HeldCertificate.decode(certString + keyString);
            var handshakeBuilder = new HandshakeCertificates.Builder()
                    .addPlatformTrustedCertificates()
                    .heldCertificate(certificate);
            additionalTrustedCerts.forEach(handshakeBuilder::addTrustedCertificate);
            var handshake = handshakeBuilder
                    .build();
            return handshake.sslContext();
        } catch (IOException e) {
            throw new IllegalArgumentException("Provided certificate or private key is not valid or cannot be read", e);
        }
    }

    @SuppressFBWarnings(value = "PATH_TRAVERSAL_IN", justification = "Intended to be used by admin users only.")
    private InputStream readFromData(Map<String, Object> authenticationData, String bytesKey, String fileKey) {
        InputStream inputStream = null;
        if (authenticationData.get(bytesKey) instanceof byte[] certBytes) {
            inputStream = new ByteArrayInputStream(certBytes);
        } else {
            if (authenticationData.get(fileKey) instanceof String certPath) {
                try {
                    if (certPath.startsWith("classpath:")) {
                        inputStream = getClass().getResourceAsStream(certPath.replace("classpath:", ""));
                    } else {
                        inputStream = new FileInputStream(certPath);
                    }
                } catch (IOException e) {
                    logger.warn("Failed to read certificate from specified path: {}", certPath, e);
                }
            }
        }
        return inputStream;
    }
}
