package eu.rarogsoftware.rarog.platform.core.task;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.metrics.*;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

class MonitoredThreadFactory implements ThreadFactory {
    private final Gauge activeThreadCountGauge;
    private final Counter createdThreadCounter;
    private final Histogram threadLifeDurationHistogram;
    private static final AtomicLong threadSequencer = new AtomicLong(0);

    public MonitoredThreadFactory(MetricsService metricsService) {
        this.activeThreadCountGauge = metricsService.createGauge(MetricSettings.settings()
                .name("rarog_task_manager_active_thread_count")
                .description("Shows count of active threads created with TaskManager")
        );
        this.createdThreadCounter = metricsService.createCounter(MetricSettings.settings()
                .name("rarog_task_manager_created_thread_count")
                .description("Shows count of created threads created with TaskManager during system lifetime")
        );
        this.threadLifeDurationHistogram = metricsService.createHistogram(HistogramSettings.settings()
                .name("rarog_task_manager_thread_life_duration")
                .description("Shows distribution of threads life duration")
                .unit("seconds")
                .buckets(0.001, 0.01, 0.1, 1, 10, 60, 600, 3600)
        );
    }

    @Override
    public Thread newThread(Runnable runnable) {
        return new ServletContextAwareThreadWrapper(new Thread(wrapRunnable(runnable), "TaskManagerThread#" + threadSequencer.incrementAndGet()));
    }

    private Runnable wrapRunnable(Runnable runnable) {
        return () -> {
            try (var ignored = threadLifeDurationHistogram.startTimer()) {
                activeThreadCountGauge.increment();
                createdThreadCounter.increment();
                runnable.run();
            } catch (Exception ignore) {
                // do nothing
            } finally {
                activeThreadCountGauge.decrement();
            }
        };
    }
}
