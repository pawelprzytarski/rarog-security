package eu.rarogsoftware.rarog.platform.app.configuration;

import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.CsrfHelper;
import eu.rarogsoftware.rarog.platform.api.user.management.UserRole;
import eu.rarogsoftware.rarog.platform.app.configuration.security.AutoImportingFilter;
import eu.rarogsoftware.rarog.platform.app.configuration.security.JsonLoginFormCustomizer;
import eu.rarogsoftware.rarog.platform.app.configuration.security.WebAuthenticationExtendedDetailsSource;
import eu.rarogsoftware.rarog.platform.core.plugins.web.filters.PluggableSecurityFilterChain;
import eu.rarogsoftware.rarog.platform.core.plugins.web.filters.PluginSecurityFilterResolver;
import eu.rarogsoftware.rarog.platform.core.plugins.web.servlets.CustomServletAwareAuthorizationManager;
import eu.rarogsoftware.rarog.platform.core.plugins.web.servlets.SmartSwitchingServlet;
import eu.rarogsoftware.rarog.platform.core.security.CsrfDisabledMatcher;
import eu.rarogsoftware.rarog.platform.core.security.anonymous.AnonymousAllowedMatcher;
import eu.rarogsoftware.rarog.platform.security.mfa.MultiFactorEnforcingFilter;
import jakarta.servlet.Filter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.header.HeaderWriter;
import org.springframework.security.web.header.writers.DelegatingRequestMatcherHeaderWriter;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.ArrayList;

import static org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter.XFRAME_OPTIONS_HEADER;

@EnableWebSecurity
@EnableMethodSecurity
@Configuration
public class SecurityConfig {
    private static final String H2_CONSOLE_PATTERN = "/h2-console/**";
    @Value("${spring.profiles.active}")
    protected String activeProfiles;
    @Value("${spring.mvn.static-path-pattern:/static/**}")
    private String resourcesPath;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http,
                                                   AutoImportingFilter[] autoImportingFilters,
                                                   MultiFactorEnforcingFilter multiFactorEnforcingFilter,
                                                   AnonymousAllowedMatcher anonymousAllowedMatcher,
                                                   CustomServletAwareAuthorizationManager servletAwareAuthorizationManager,
                                                   CsrfDisabledMatcher csrfDisabledMatcher,
                                                   SmartSwitchingServlet smartSwitchingServlet,
                                                   PluginSecurityFilterResolver pluginSecurityFilterResolver)
            throws Exception {
        configureSecurityHeaders(http);
        configureFilters(http, autoImportingFilters);
        configureEndpoints(http, multiFactorEnforcingFilter, anonymousAllowedMatcher, servletAwareAuthorizationManager)
                .httpBasic().disable()
                .formLogin().disable()
                .apply(new JsonLoginFormCustomizer<>())
                .loginPage("/login/")
                .loginProcessingUrl("/login/perform")
                .defaultSuccessUrl("/login/success", true)
                .failureForwardUrl("/login/failure")
                .authenticationDetailsSource(new WebAuthenticationExtendedDetailsSource())
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/logged-out")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                .csrf()
                .ignoringRequestMatchers(getCsrfRequestMatchers(csrfDisabledMatcher, smartSwitchingServlet).toArray(new RequestMatcher[0]));
        return new PluggableSecurityFilterChain(http.build(), pluginSecurityFilterResolver);
    }


    private void configureSecurityHeaders(HttpSecurity http) throws Exception {
        RequestMatcher h2Matcher = new AntPathRequestMatcher(H2_CONSOLE_PATTERN);
        RequestMatcher allMatcher = new AntPathRequestMatcher("/**");

        var h2HeaderWriter =
                new DelegatingRequestMatcherHeaderWriter(h2Matcher, new XFrameOptionsHeaderWriter(XFrameOptionsHeaderWriter.XFrameOptionsMode.SAMEORIGIN));
        var defaultFrameOptionsPolicyHeaderWriter = new DelegatingRequestMatcherHeaderWriter(allMatcher, new XFrameOptionsHeaderWriter(XFrameOptionsHeaderWriter.XFrameOptionsMode.DENY));
        HeaderWriter allHeaderWriter = (request, response) -> {
            if (response.getHeader(XFRAME_OPTIONS_HEADER) == null) {
                defaultFrameOptionsPolicyHeaderWriter.writeHeaders(request, response);
            }
        };


        http.headers(headers -> headers
                .frameOptions().disable()
                .addHeaderWriter(allHeaderWriter)
                .addHeaderWriter(h2HeaderWriter)
        );
    }

    private ArrayList<RequestMatcher> getCsrfRequestMatchers(RequestMatcher csrfDisabledMatcher,
                                                             SmartSwitchingServlet smartSwitchingServlet) {
        var csrfDisablingMatchers = new ArrayList<RequestMatcher>();
        csrfDisablingMatchers.add(csrfDisabledMatcher);
        if (activeProfiles.contains("debug")) {
            csrfDisablingMatchers.add(request -> "true".equalsIgnoreCase(request.getHeader(CsrfHelper.DISABLE_XSRF_ATTRIBUTE)));
        }
        csrfDisablingMatchers.add(request -> "true".equalsIgnoreCase(String.valueOf(request.getAttribute(CsrfHelper.DISABLE_XSRF_ATTRIBUTE))));
        csrfDisablingMatchers.add(new AntPathRequestMatcher(H2_CONSOLE_PATTERN));
        csrfDisablingMatchers.add(request -> smartSwitchingServlet.shouldDisableCsrfProtection(request).orElse(false));
        return csrfDisablingMatchers;
    }

    private HttpSecurity configureFilters(HttpSecurity httpSecurity, AutoImportingFilter[] autoImportingFilters) {
        for (AutoImportingFilter filter : autoImportingFilters) {
            if (filter instanceof Filter servletFilter) {
                httpSecurity = switch (filter.getFilterPlace().direction()) {
                    case AFTER -> httpSecurity.addFilterAfter(servletFilter, filter.getFilterPlace().filter());
                    case BEFORE -> httpSecurity.addFilterBefore(servletFilter, filter.getFilterPlace().filter());
                    case AT -> httpSecurity.addFilterAt(servletFilter, filter.getFilterPlace().filter());
                };
            } else {
                throw new IllegalStateException("Filter " + filter.getClass() + " is not jakarta.servlet.Filter");
            }
        }
        return httpSecurity;
    }

    protected HttpSecurity configureEndpoints(HttpSecurity http, MultiFactorEnforcingFilter multiFactorEnforcingFilter,
                                              AnonymousAllowedMatcher anonymousAllowedMatcher,
                                              CustomServletAwareAuthorizationManager servletAwareAuthorizationManager) throws Exception {
        multiFactorEnforcingFilter.addExcludedPathPatterns(
                "/error",
                "/login/success",
                "/logout/",
                "/logout",
                "/static/**",
                "/i18n/**");
        var urlRegistry = http
                .authorizeHttpRequests();
        if (activeProfiles.contains("debug")) {
            urlRegistry = urlRegistry
                    .requestMatchers(new AntPathRequestMatcher("/actuator/**"))
                    .permitAll()
                    .requestMatchers(new AntPathRequestMatcher(H2_CONSOLE_PATTERN))
                    .permitAll();
        }
        return urlRegistry
                .requestMatchers(anonymousAllowedMatcher)
                .permitAll()
                .requestMatchers(new AntPathRequestMatcher(resourcesPath))
                .permitAll()
                .requestMatchers(new AntPathRequestMatcher("static/**")).permitAll()
                .requestMatchers(new AntPathRequestMatcher("api/**")).hasRole(UserRole.USER.getRole())
                .requestMatchers(new AntPathRequestMatcher("api/admin/**")).hasRole(UserRole.ADMINISTRATOR.getRole())
                .requestMatchers(new AntPathRequestMatcher("/error")).permitAll()
                .requestMatchers(new AntPathRequestMatcher("/boot/**")).permitAll()
                .requestMatchers(new AntPathRequestMatcher("/favicon.ico")).permitAll()
                .requestMatchers(new AntPathRequestMatcher("/docs/**")).permitAll()
                .anyRequest()
                .access(servletAwareAuthorizationManager)
                .and();
    }

}

