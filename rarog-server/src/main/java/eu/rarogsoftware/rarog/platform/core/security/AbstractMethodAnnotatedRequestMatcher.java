package eu.rarogsoftware.rarog.platform.core.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.util.ServletRequestPathUtils;

import jakarta.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.util.List;

public abstract class AbstractMethodAnnotatedRequestMatcher implements RequestMatcher {
    private static final Logger logger = LoggerFactory.getLogger(AbstractMethodAnnotatedRequestMatcher.class);
    private final Class<? extends Annotation> annotationType;
    private final List<HandlerMapping> handlerMappings;

    public AbstractMethodAnnotatedRequestMatcher(List<HandlerMapping> handlerMappings,
                                                 Class<? extends Annotation> annotationType) {
        this.handlerMappings = handlerMappings;
        this.annotationType = annotationType;
    }

    @Override
    public boolean matches(HttpServletRequest request) {
        if (!ServletRequestPathUtils.hasCachedPath(request)) {
            ServletRequestPathUtils.parseAndCache(request);
        }
        try {
            for (var handlerMapping : handlerMappings) {
                try {
                    var method = getHandlerMethod(handlerMapping.getHandler(request));
                    if (method != null
                            && annotationPasses(method)) {
                        return true;
                    }
                } catch (IllegalArgumentException e) {
                    logger.debug("One of handlers thrown illegal argument exception", e);
                }
            }
        } catch (Exception e) {
            logger.warn("Cannot get handler for " + request.getServletPath(), e);
            return false;
        }
        return false;
    }

    protected boolean annotationPasses(HandlerMethod method) {
        return hasAnnotation(method);
    }

    protected boolean hasAnnotation(HandlerMethod method) {
        return isAnnotatedDirectlyWithAnnotation(method)
                || MergedAnnotations.from(method.getBeanType().getAnnotations()).isPresent(annotationType);
    }

    private boolean isAnnotatedDirectlyWithAnnotation(HandlerMethod method) {
        return method.hasMethodAnnotation(annotationType) || method.getBeanType().isAnnotationPresent(annotationType);
    }

    protected HandlerMethod getHandlerMethod(HandlerExecutionChain handlerChain) {
        if (handlerChain != null) {
            Object handler = handlerChain.getHandler();
            if (handler instanceof HandlerExecutionChain) {
                return getHandlerMethod((HandlerExecutionChain) handler);
            }
            if (handler instanceof HandlerMethod) {
                return (HandlerMethod) handler;
            }
        }
        return null;
    }
}
