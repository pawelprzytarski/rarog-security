package eu.rarogsoftware.rarog.platform.core.metrics.noop;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.metrics.*;

import java.util.Collection;
import java.util.Collections;

public class NoopMetricsService implements MetricsService {
    @Override
    public Counter createCounter(MetricSettings settings) {
        return new NoopCounter();
    }

    @Override
    public Gauge createGauge(MetricSettings settings) {
        return new NoopGauge();
    }

    @Override
    public Histogram createHistogram(HistogramSettings settings) {
        return new NoopHistogram();
    }

    @Override
    public Info createInfo(MetricSettings settings) {
        return new NoopInfo();
    }

    @Override
    public void closeMetric(BasicMetric metric) {
        // do nothing
    }

    @Override
    public MetricsSnapshot snapshot() {
        return MetricsSnapshot.EMPTY_SNAPSHOT;
    }

    @Override
    public Collection<MetricSamples> sample() {
        return Collections.emptySet();
    }
}
