package eu.rarogsoftware.rarog.platform.core.plugins.osgi;

import eu.rarogsoftware.rarog.platform.api.commons.ClassHelpers;
import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;
import eu.rarogsoftware.rarog.platform.api.settings.HomeDirectoryHelper;
import org.apache.commons.collections4.keyvalue.DefaultMapEntry;
import org.apache.felix.framework.Felix;
import org.apache.felix.framework.FrameworkFactory;
import org.apache.felix.framework.util.FelixConstants;
import org.apache.felix.main.AutoProcessor;
import org.osgi.framework.*;
import org.osgi.framework.launch.Framework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.data.util.Version;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;
import org.springframework.util.SystemPropertyUtils;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component("MainPluginsSystemManager")
public class FelixManager implements OsgiMiddlewareManager {
    private final Logger logger = LoggerFactory.getLogger(FelixManager.class);
    private final HomeDirectoryHelper homeDirectoryHelper;
    private final ApplicationContext applicationContext;
    private Framework felix;
    private static final List<String> PACKAGES_TO_SEARCH_FOR_EXPORT = Arrays.asList(
            "org.slf4j",
            "org.springframework",
            "org.thymeleaf",
            "jakarta.servlet",
            "com.querydsl",
            "io.swagger.v3.oas.annotations",
            "eu.rarogsoftware.rarog.platform.api",
            "eu.rarogsoftware.commons.database.connection",
            "javax.cache"
    );
    private static final Map<String, String> PACKAGES_DEFAULT_VERSIONS = Map.ofEntries(new DefaultMapEntry<>("javax.cache.*", "1.1.1"));
    private static final List<String> PACKAGES_TO_EXPORT = Arrays.asList("org.aopalliance.aop;version=1.6.4",
            "org.aopalliance.intercept;version=1.6.4");

    public FelixManager(HomeDirectoryHelper homeDirectoryHelper, ApplicationContext applicationContext) {
        this.homeDirectoryHelper = homeDirectoryHelper;
        this.applicationContext = applicationContext;
    }

    @Override
    public boolean isStarted() {
        return felix != null;
    }

    @Override
    public void startSystem() {
        logger.info("Starting plugins system");
        FrameworkFactory frameworkFactory = new FrameworkFactory();
        logger.trace("Creating configuration");
        Map configuration = getConfiguration();
        logger.trace("Creating felix framework");
        felix = frameworkFactory.newFramework(configuration);
        try {
            logger.trace("Initializing felix framework");
            felix.init();
            AutoProcessor.process(configuration, felix.getBundleContext());
            logger.debug("Starting felix framework");
            felix.start();
        } catch (BundleException e) {
            logger.error("Failed to start Apache Felix", e);
            throw new RuntimeException(e);
        }
        logger.info("Started plugins system");
    }

    private Map getConfiguration() {
        logger.trace("Getting exports components");
        var exportComponents = applicationContext.getBeansWithAnnotation(ExportComponent.class);
        logger.trace("Getting listeners");
        var listeners = applicationContext.getBeansOfType(OsgiSystemBundleListener.class);
        logger.trace("Creating config map");
        Map<String, Object> config = new HashMap<>();
        new File(homeDirectoryHelper.getCacheFile(), "felix").mkdirs();
        config.put(FelixConstants.FRAMEWORK_STORAGE, homeDirectoryHelper.getCache() + "felix");
        SystemBundleActivator systemBundleActivator = new SystemBundleActivator(exportComponents, listeners);
        config.put(FelixConstants.SYSTEMBUNDLE_ACTIVATORS_PROP, Collections.singletonList(systemBundleActivator));
        config.put(FelixConstants.LOG_LOGGER_PROP, FelixLoggerHelper.getLogger(Felix.class));
        config.put(FelixConstants.LOG_LEVEL_PROP, FelixLoggerHelper.DEBUG_LEVEL);
        config.put(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, findPackageNamesStartingWith(
                getPackagesToExport(exportComponents),
                PACKAGES_TO_EXPORT
        ));
        config.put(Constants.FRAMEWORK_SYSTEMCAPABILITIES_EXTRA, "osgi.contract;osgi.contract=\"JavaServlet\";version:List<Version>=\"2.6,3.0,3.1,4.0,6.0.0\";"
                + "uses:=\"jakarta.servlet,jakarta.servlet.http,jakarta.servlet.descriptor,jakarta.servlet.annotation\","
                + "osgi.contract;osgi.contract=\"JakartaServlet\";version:List<Version>=\"6.0.0\";"
                + "uses:=\"jakarta.servlet,jakarta.servlet.http,jakarta.servlet.descriptor,jakarta.servlet.annotation\"");
        logger.debug("Extra exports: {}", config.get(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA));
        logger.trace("Created config map");
        return Collections.unmodifiableMap(config);
    }

    private Set<String> getPackagesToExport(Map<String, Object> exportComponents) {
        Set<String> packages = new HashSet<>(
                PACKAGES_TO_SEARCH_FOR_EXPORT
        );
        packages.addAll(exportComponents.values().stream().map(object -> object.getClass().getPackageName()).toList());
        return packages;
    }

    protected String findPackageNamesStartingWith(Set<String> packages, List<String> simpleExports) {
        logger.trace("Preparing list of export packages for packages {} and using simple exports: {}", packages, simpleExports);
        return Stream.concat(packages.stream().map(this::getPackages)
                                .flatMap(Set::stream),
                        simpleExports.stream())
                .collect(Collectors.joining(","));
    }

    protected Set<String> getPackages(String basePackage) {
        try {
            logger.trace("Preparing list for package: {}", basePackage);
            ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
            MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);
            String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + resolveBasePackage(basePackage) + "/**/*.class";
            return Arrays.stream(resourcePatternResolver.getResources(packageSearchPath)).map(resource -> {
                        try {
                            logger.trace("Reading packages info for resource {}", resource);
                            var metadataReader = metadataReaderFactory.getMetadataReader(resource);
                            var aClass = Class.forName(metadataReader.getClassMetadata().getClassName());
                            String implementationVersion = aClass.getPackage().getImplementationVersion();
                            implementationVersion = StringUtils.hasText(implementationVersion) && implementationVersion.endsWith("-SNAPSHOT") ? implementationVersion.substring(0, implementationVersion.length() - 9) : implementationVersion;
                            if (StringUtils.hasText(implementationVersion)) {
                                implementationVersion = Version.parse(implementationVersion).toString();
                            } else {
                                var packageName = aClass.getPackage().getName();
                                implementationVersion = PACKAGES_DEFAULT_VERSIONS.entrySet()
                                        .stream()
                                        .filter(entry -> entry.getKey().endsWith("*")
                                                ? packageName.startsWith(entry.getKey().substring(0, entry.getKey().length() - 2))
                                                : packageName.equals(entry.getKey()))
                                        .map(Map.Entry::getValue)
                                        .findFirst()
                                        .orElse(null);
                            }
                            return aClass.getPackage().getName() + (StringUtils.hasText(implementationVersion) ? ";version=" + implementationVersion : "");
                        } catch (ClassNotFoundException | NoClassDefFoundError | ExceptionInInitializerError | IOException e) {
                            logger.trace("Error looking for provided package", e);
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            logger.warn("Error looking for provided resource", e);
            return Set.of();
        }
    }

    private String resolveBasePackage(String basePackage) {
        return ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(basePackage));
    }

    @Override
    @PreDestroy
    public void stopSystem() {
        logger.info("Shutting down plugins system");
        if (felix != null) {
            try {
                felix.stop();
                logger.debug("Started shutdown process. Waiting for felix to stop");
                felix.waitForStop(60);
                felix = null;
            } catch (BundleException e) {
                logger.error("Failed to stop Apache Felix", e);
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
        }
        logger.info("Shutdown of plugins system complete");
    }

    static class SystemBundleActivator implements BundleActivator {
        private final Logger logger = LoggerFactory.getLogger(SystemBundleActivator.class);
        private final Map<String, Object> exportComponents;
        private final Collection<OsgiSystemBundleListener> listeners;
        private final List<Bundle> frameworkBundles = new ArrayList<>();

        public SystemBundleActivator(Map<String, Object> exportComponents, Map<String, OsgiSystemBundleListener> listeners) {
            this.exportComponents = exportComponents;
            this.listeners = listeners.values();
        }

        @Override
        public void start(BundleContext context) throws Exception {
            logger.debug("Starting system bundle");
            logger.debug("Exporting components");
            exportComponents.forEach((beanName, component) -> {
                var interfaces = ClassHelpers.getImplementedInterfaces(component.getClass()).stream().map(Class::getName).toList().toArray(new String[0]);
                logger.trace("Exporting component: {} as interfaces {}", component.getClass(), interfaces);
                context.registerService(interfaces, component, new Hashtable<>(Map.of("BeanName", beanName)));
            });

            installFrameworkBundles(context);

            logger.debug("Invoking OsgiSystemBundleListeners");
            for (OsgiSystemBundleListener listener : listeners) {
                listener.systemBundledStarted(context);
            }
            logger.debug("Started system bundle");
        }

        private void installFrameworkBundles(BundleContext context) throws IOException, BundleException {
            logger.debug("Installing framework bundles");
            var resourceResolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());
            if (!resourceResolver.getResource("classpath:/frameworkbundles/").exists()) {
                logger.warn("No framework bundles found");
                return;
            }
            for (Resource resource : resourceResolver.getResources("classpath:/frameworkbundles/*.jar")) {
                logger.trace("Installing framework bundle: {}", resource.getURL());
                frameworkBundles.add(context.installBundle(resource.getURL().toString()));
            }
            for (var bundle : frameworkBundles) {
                logger.trace("Starting bundle: {}", bundle);
                bundle.start();
            }
            logger.debug("Installed framework bundles");
        }

        @Override
        public void stop(BundleContext context) throws Exception {
            logger.debug("Stopping system bundle");
            for (OsgiSystemBundleListener listener : listeners) {
                listener.systemBundledStopped(context);
            }

            logger.debug("Installing bundles");
            for (Bundle bundle : context.getBundles()) {
                if (bundle.getBundleId() != 0) {
                    logger.trace("Stopping and uninstalling bundle {}", bundle);
                    bundle.stop();
                    bundle.uninstall();
                    logger.trace("Stopped and uninstalled bundle {}", bundle);
                }
            }
            logger.debug("Uninstalled bundles");
        }
    }
}
