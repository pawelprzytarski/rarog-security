package eu.rarogsoftware.rarog.platform.app.configuration.dev;


import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Indexed;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Indexed
@ConditionalOnProperty("devMode")
@Component
public @interface DevModeComponent {
}
