package eu.rarogsoftware.rarog.platform.core.plugins.spring;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AutoRegisterFeatureModule {
    Class<? extends FeatureDescriptor>[] value() default {};
}
