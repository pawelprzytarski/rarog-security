package eu.rarogsoftware.rarog.platform.dynamic.frontend.resources;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import eu.rarogsoftware.commons.cache.CacheService;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.AbstractDynamicAssetsLoaderFactory;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.ClassLoaderDynamicAssetsLoader;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoader;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.FilesystemDynamicAssetsLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Optional;

@Component("main")
@SuppressFBWarnings(value = "PATH_TRAVERSAL_IN", justification = "this file path is provided by developer, not user")
public class LocalLoaderDynamicAssetsLoaderFactory extends AbstractDynamicAssetsLoaderFactory {
    private static final String CLASSPATH_PREFIX = "classpath:";
    private static final String FILE_PREFIX = "file:";
    private final Logger logger = LoggerFactory.getLogger(LocalLoaderDynamicAssetsLoaderFactory.class);

    public LocalLoaderDynamicAssetsLoaderFactory(CacheService cacheService,
                                                 @Value("${webpack.manifest.cache.duration:-1}")
                                                 Long cacheDuration,
                                                 @Value("${webpack.manifest.location:classpath:descriptors/webpack-{namespace}.manifest.json}")
                                                 String manifestLocationTemplate) {
        super(cacheService, cacheDuration, manifestLocationTemplate);
    }

    public DynamicAssetsLoader getMainAssetsLoader() {
        return getNamedAssetsLoader("app").orElseThrow(() -> new IllegalStateException("Cannot load main assets loader"));
    }

    @Override
    protected Optional<DynamicAssetsLoader> createCacheLoader(String key) {
        logger.debug("Loading manifest for {}", key);
        var manifestLocation = manifestLocationTemplate.replace("{namespace}", key);
        if (manifestLocation.startsWith(CLASSPATH_PREFIX)) {
            if (this.getClass().getClassLoader().getResource(manifestLocation.substring(CLASSPATH_PREFIX.length())) == null) {
                logger.warn("Manifest for key '{}' not found at {}", key, manifestLocation);
                return Optional.empty();
            }
            return Optional.of(new ClassLoaderDynamicAssetsLoader(manifestLocation.substring(CLASSPATH_PREFIX.length()), cacheDuration));
        } else if (manifestLocation.startsWith(FILE_PREFIX)) {
            var filePath = manifestLocation.substring(FILE_PREFIX.length());
            if (!new File(filePath).exists()) {
                logger.warn("Manifest for key '{}' not found at {}", key, manifestLocation);
                return Optional.empty();
            }
            return Optional.of(new FilesystemDynamicAssetsLoader(filePath, cacheDuration));
        } else {
            logger.warn("Unknown protocol for uri {}", manifestLocation);
            return Optional.empty();
        }
    }
}
