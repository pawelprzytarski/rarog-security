package eu.rarogsoftware.rarog.platform.core.plugins.osgi;

import org.apache.felix.framework.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class FelixLoggerHelper {
    public static final String ERROR_LEVEL = "1";
    public static final String WARN_LEVEL = "2";
    public static final String INFO_LEVEL = "3";
    public static final String DEBUG_LEVEL = "4";

    public static Logger getLogger(Class<?> loggerClass) {
        var logger = LoggerFactory.getLogger(loggerClass);
        Logger felixLogger = new Logger();
        felixLogger.setLogger(new SimpleLogger(logger));
        return felixLogger;
    }

    static class SimpleLogger {
        Map<Integer, BiConsumer<String, Throwable>> loggers;
        BiConsumer<String, Throwable> defaultLogger;

        public SimpleLogger(org.slf4j.Logger logger) {
            loggers = new HashMap<>();
            loggers.put(1, logger::error);
            loggers.put(2, logger::warn);
            loggers.put(3, logger::info);
            loggers.put(4, logger::debug);
            defaultLogger = logger::warn;
        }

        public void log(int level, String message, Throwable throwable) {
            loggers.getOrDefault(level, defaultLogger).accept(message, throwable);
        }
    }
}
