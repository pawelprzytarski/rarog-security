package eu.rarogsoftware.rarog.platform.security.mfa.recovery;

import com.querydsl.sql.SQLQueryFactory;
import eu.rarogsoftware.commons.database.connection.DatabaseConnectionProvider;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.db.QMfaRecoveryCodes;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Component
public class DbRecoveryCodesStore implements RecoveryCodesStore {
    private final DatabaseConnectionProvider databaseConnectionProvider;
    private final QMfaRecoveryCodes recoveryCodes = QMfaRecoveryCodes.mfaRecoveryCodes;
    private final MessageDigest digest;

    public DbRecoveryCodesStore(DatabaseConnectionProvider databaseConnectionProvider) throws NoSuchAlgorithmException {
        this.databaseConnectionProvider = databaseConnectionProvider;
        digest = MessageDigest.getInstance("SHA-256");
    }

    @Override
    public int getCodesCount(StandardUser user) {
        return databaseConnectionProvider.getConnection(query -> query.select(recoveryCodes.count())
                .from(recoveryCodes)
                .where(recoveryCodes.userId.eq(user.getId()))
                .fetchOne().intValue());
    }

    @Override
    public boolean removeCode(StandardUser user, String code) {
        String hashedCode = getHashedCode(code);
        return databaseConnectionProvider.getConnection(query -> query
                .delete(recoveryCodes)
                .where(recoveryCodes.code.eq(hashedCode)
                        .and(recoveryCodes.userId.eq(user.getId())))
                .execute() >= 1);
    }

    private String getHashedCode(String code) {
        return new String(Hex.encode(digest.digest(code.getBytes(StandardCharsets.UTF_8))));
    }

    @Override
    public void saveCodes(StandardUser user, List<String> codes) {
        databaseConnectionProvider.getConnection(query -> insertCodes(user, codes, query));
    }

    private long insertCodes(StandardUser user, List<String> codes, SQLQueryFactory query) {
        var insert = query.insert(recoveryCodes);
        codes.stream()
                .map(this::getHashedCode)
                .forEach(code -> insert
                        .set(recoveryCodes.code, code)
                        .set(recoveryCodes.userId, user.getId())
                        .addBatch());
        return insert.execute();
    }

    @Override
    public void replaceCodes(StandardUser user, List<String> codes) {
        databaseConnectionProvider.getConnection(query -> {
            query
                    .delete(recoveryCodes)
                    .where(recoveryCodes.userId.eq(user.getId()))
                    .execute();
            return insertCodes(user, codes, query);
        });
    }
}
