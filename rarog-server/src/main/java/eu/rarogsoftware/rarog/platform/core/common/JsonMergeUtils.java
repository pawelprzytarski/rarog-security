package eu.rarogsoftware.rarog.platform.core.common;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public final class JsonMergeUtils {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final JsonMergeUtils.MapTypeReference MAP_TYPE = new JsonMergeUtils.MapTypeReference();

    private JsonMergeUtils() {
    }

    public static String mergeJsons(String... jsons) throws IOException {
        List<Map<String, Object>> mapsToTransform = new ArrayList<>();
        for (var json : jsons) {
            mapsToTransform.add(objectMapper.readValue(json, MAP_TYPE));
        }

        var mergedMap = deepMergeMaps(mapsToTransform);
        try (var out = new ByteArrayOutputStream()) {
            objectMapper.writeValue(out, mergedMap);
            return out.toString();
        }
    }

    public static InputStream mergeJsons(InputStream... jsons) throws IOException {
        List<Map<String, Object>> mapsToTransform = new ArrayList<>();
        for (var inputStream : jsons) {
            try (inputStream) {
                mapsToTransform.add(objectMapper.readValue(inputStream, MAP_TYPE));
            }
        }

        var mergedMap = deepMergeMaps(mapsToTransform);
        try (var out = new ByteArrayOutputStream()) {
            objectMapper.writeValue(out, mergedMap);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    private static Map<String, Object> deepMergeMaps(List<Map<String, Object>> mapsToTransform) {
        if (mapsToTransform.size() == 1) {
            return mapsToTransform.get(0);
        }
        return mapsToTransform.stream()
                .flatMap(map -> map.keySet().stream())
                .collect(Collectors.toMap(key -> key, key -> {
                    var resultMap = mergeSubMaps(mapsToTransform, key);
                    if (resultMap.size() == 1 && resultMap.get("") != null) {
                        return resultMap.get("");
                    } else {
                        return resultMap;
                    }
                }, (object1, object2) -> object2));
    }

    private static Map<String, Object> mergeSubMaps(List<Map<String, Object>> mapsToTransform, String key) {
        if (!StringUtils.hasText(key)) {
            return mapsToTransform
                    .stream()
                    .map(subMap -> subMap.get(key))
                    .filter(Objects::nonNull)
                    .map(object -> (Map<String, Object>) ((object instanceof Map) ? object : Map.of("", object)))
                    .findFirst()
                    .orElse(Map.of("", ""));
        }
        return deepMergeMaps(mapsToTransform
                .stream()
                .map(subMap -> subMap.get(key))
                .filter(Objects::nonNull)
                .map(object -> (Map<String, Object>) ((object instanceof Map) ? object : Map.of("", object)))
                .toList());
    }

    private static class MapTypeReference extends TypeReference<Map<String, Object>> {

    }
}
