package eu.rarogsoftware.rarog.platform.core.plugins.web;

import org.springframework.util.StringUtils;

import jakarta.servlet.http.HttpServletMapping;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;
import jakarta.servlet.http.MappingMatch;

public class PluginServletHttpServletRequestWrapper extends HttpServletRequestWrapper {
    private final String baseUrl;

    public PluginServletHttpServletRequestWrapper(HttpServletRequest request, String baseUrl) {
        super(request);
        this.baseUrl = baseUrl;
    }

    @Override
    public String getServletPath() {
        return StringUtils.trimTrailingCharacter(baseUrl, '/');
    }

    @Override
    public String getPathInfo() {
        return super.getServletPath().substring(getServletPath().length()) + (super.getPathInfo() != null ? super.getPathInfo() : "");
    }

    @Override
    public HttpServletMapping getHttpServletMapping() {
        return new HttpServletMapping() {
            @Override
            public String getMatchValue() {
                return getServletPath();
            }

            @Override
            public String getPattern() {
                return getServletPath();
            }

            @Override
            public String getServletName() {
                return "";
            }

            @Override
            public MappingMatch getMappingMatch() {
                return MappingMatch.PATH;
            }
        };
    }
}
