package eu.rarogsoftware.rarog.platform.core.plugins.osgi;

import org.osgi.framework.BundleContext;

public interface OsgiSystemBundleListener {
    void systemBundledStarted(BundleContext context) throws Exception;

    void systemBundledStopped(BundleContext context) throws Exception;
}
