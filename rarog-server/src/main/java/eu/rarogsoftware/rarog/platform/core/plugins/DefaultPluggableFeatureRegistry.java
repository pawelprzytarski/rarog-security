package eu.rarogsoftware.rarog.platform.core.plugins;

import eu.rarogsoftware.commons.utils.Pair;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.PurgeableFeatureModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

@Component
public class DefaultPluggableFeatureRegistry implements PluggableFeatureRegistry {
    private final Logger logger = LoggerFactory.getLogger(DefaultPluggableFeatureRegistry.class);
    private final Map<Class<? extends FeatureDescriptor>, FeatureModuleDescriptor<?>> featureModules = new ConcurrentHashMap<>();
    private final Map<Class<? extends FeatureDescriptor>, List<Pair<Plugin, FeatureDescriptor>>> registeredDescriptors = new ConcurrentHashMap<>();
    private final List<Pair<Plugin, FeatureDescriptor>> unassignedDescriptors = new ArrayList<>();

    private static FeatureModule<FeatureDescriptor> getGenericModule(FeatureModuleDescriptor<?> moduleDescriptor) {
        return (FeatureModule<FeatureDescriptor>) moduleDescriptor.getModule();
    }

    @Override
    public <T extends FeatureDescriptor> void registerFeatureModule(Class<T> descriptorType, Supplier<FeatureModule<T>> featureModuleSupplier) {
        logger.trace("Registering feature module supplier for descriptor {} via PluggableFeatureRegistry#registerFeatureModule", descriptorType);
        registerFeatureModuleDescriptor(new FeatureModuleDescriptor<>(descriptorType, featureModuleSupplier));
    }

    @Override
    public void unregisterFeatureModule(Class<? extends FeatureDescriptor> descriptorType) {
        logger.trace("Unregistering feature module for descriptor {}", descriptorType);
        var module = featureModules.remove(descriptorType);
        var descriptors = registeredDescriptors.remove(descriptorType);
        if (module != null && descriptors != null) {
            logger.trace("Unplugging descriptors from feature module {}", descriptorType);
            descriptors.forEach(pair -> {
                logger.trace("Unplugging descriptor {} of plugin {}", pair.right().getClass(), pair.left().getKey());
                getGenericModule(module).unplugDescriptor(pair.left(), pair.right());
            });
            synchronized (unassignedDescriptors) {
                logger.trace("Moving descriptors of type {} to unassigned descriptors", descriptorType);
                unassignedDescriptors.addAll(descriptors);
            }
        }
        logger.info("Unregistered feature module for descriptor {}", descriptorType);
    }

    @Override
    public void plugDescriptors(Plugin plugin, Collection<? extends FeatureDescriptor> descriptors) {
        logger.trace("Plugging descriptors of plugin {}", plugin.getKey());
        descriptors.forEach(descriptor -> {
            if (descriptor instanceof FeatureModuleDescriptor<?> featureDescriptor) {
                logger.trace("Registering feature module supplier for descriptor {} via plugged descriptor", featureDescriptor.descriptorType());
                registerFeatureModuleDescriptor(featureDescriptor);
            } else {
                registerNormalDescriptor(plugin, descriptor);
            }
        });
        logger.debug("Plugged descriptors of plugin {}", plugin.getKey());
    }

    private void registerNormalDescriptor(Plugin plugin, FeatureDescriptor descriptor) {
        logger.trace("Plugging descriptor {} of plugin {}", descriptor.getClass(), plugin.getKey());
        var moduleDescriptor = featureModules.get(descriptor.getClass());
        registeredDescriptors.computeIfAbsent(descriptor.getClass(), key -> new ArrayList<>()).add(Pair.of(plugin, descriptor));
        if (moduleDescriptor == null) {
            logger.debug("Descriptor {} of plugin {} does not match any feature module. Saved for later.", descriptor.getClass(), plugin.getKey());
            synchronized (unassignedDescriptors) {
                unassignedDescriptors.add(Pair.of(plugin, descriptor));
            }
        } else {
            FeatureModule<FeatureDescriptor> genericModule = getGenericModule(moduleDescriptor);
            logger.trace("Descriptor {} of plugin {} registered to feature module {}.", descriptor.getClass(), plugin.getKey(), genericModule.getClass());
            genericModule.plugDescriptor(plugin, descriptor);
        }
        logger.trace("Plugged descriptor {} of plugin {}", descriptor.getClass(), plugin.getKey());
    }

    private void registerFeatureModuleDescriptor(FeatureModuleDescriptor<?> featureDescriptor) {
        logger.trace("Registering feature module supplier for descriptor {}", featureDescriptor.descriptorType());
        if (featureModules.containsKey(featureDescriptor.descriptorType())) {
            logger.warn("Cannot register feature module for the descriptor {} twice", featureDescriptor.descriptorType());
            throw new IllegalArgumentException("Cannot register feature module for the same descriptor type");
        }
        featureModules.put(featureDescriptor.descriptorType(), featureDescriptor);
        synchronized (unassignedDescriptors) {
            logger.trace("Plugging unassigned descriptors for feature module for {}", featureDescriptor.descriptorType());
            for (var iterator = unassignedDescriptors.iterator(); iterator.hasNext(); ) {
                var unassignedDescriptor = iterator.next();
                if (featureDescriptor.descriptorType().equals(unassignedDescriptor.right().getClass())) {
                    logger.trace("Plugging unassigned descriptor {} of plugin {}", unassignedDescriptor.right().getClass(), unassignedDescriptor.left().getKey());
                    getGenericModule(featureDescriptor).plugDescriptor(unassignedDescriptor.left(), unassignedDescriptor.right());
                    iterator.remove();
                    logger.trace("Plugged unassigned descriptor {} of plugin {}", unassignedDescriptor.right().getClass(), unassignedDescriptor.left().getKey());
                }
            }
            logger.trace("Plugged unassigned descriptors for feature module for {}", featureDescriptor.descriptorType());
        }
        logger.info("Registered feature module for descriptor {}", featureDescriptor.descriptorType());
    }

    @Override
    public void unplugDescriptors(Plugin plugin, Collection<? extends FeatureDescriptor> descriptors) {
        logger.trace("Unplugging descriptors of plugin {}", plugin.getKey());
        descriptors.forEach(descriptor -> {
            var module = featureModules.get(descriptor.getClass());
            if (module != null) {
                logger.trace("Unplugging descriptor {} of plugin {} from module {}", descriptor.getClass(), plugin.getKey(), module.getClass());
                getGenericModule(module).unplugDescriptor(plugin, descriptor);
                logger.trace("Unplugged descriptor {} of plugin {} from module {}", descriptor.getClass(), plugin.getKey(), module.getClass());
            }
            logger.trace("Removing descriptor {} of plugin {} from internal caches", descriptor.getClass(), plugin.getKey());
            var list = registeredDescriptors.get(descriptor.getClass());
            if (list != null) {
                list.removeIf(pair -> pair.right().equals(descriptor));
            }
            synchronized (unassignedDescriptors) {
                unassignedDescriptors.removeIf(pair -> pair.right().equals(descriptor));
            }
        });
        logger.debug("Unplugged descriptors of plugin {}", plugin.getKey());
    }

    @Override
    public void purgeDescriptors(Plugin plugin, Collection<? extends FeatureDescriptor> descriptors) {
        logger.trace("Purging descriptors of plugin {}", plugin.getKey());
        descriptors.forEach(descriptor -> {
            var module = featureModules.get(descriptor.getClass());
            if (module != null) {
                var genericModule = getGenericModule(module);
                if (genericModule instanceof PurgeableFeatureModule<FeatureDescriptor> purgeable) {
                    logger.trace("Purging descriptor {} of plugin {} from module {}", descriptor.getClass(), plugin.getKey(), module.getClass());
                    purgeable.purgeDescriptor(plugin, descriptor);
                    logger.trace("Purged descriptor {} of plugin {} from module {}", descriptor.getClass(), plugin.getKey(), module.getClass());
                } else {
                    logger.trace("Module {} do not support purging. Skipping", module.getClass());
                }
            }
        });
        logger.debug("Purged descriptors of plugin {}", plugin.getKey());
    }
}
