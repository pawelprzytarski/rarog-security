package eu.rarogsoftware.rarog.platform.dynamic.frontend.resources;

import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.AbstractResourceBasedDynamicAssetsLoader;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;

public class ResourceDynamicAssetsLoader extends AbstractResourceBasedDynamicAssetsLoader {
    private final Resource resource;

    public ResourceDynamicAssetsLoader(String pluginKey, Resource resource, Long cacheDuration1) {
        super(pluginKey + resource.getFilename(), cacheDuration1);
        this.resource = resource;
    }

    @Override
    protected InputStream getManifestResourceStream() throws IOException {
        return resource.getInputStream();
    }
}
