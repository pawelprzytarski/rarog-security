package eu.rarogsoftware.rarog.platform.app.servlets;

import eu.rarogsoftware.rarog.platform.api.i18n.I18nHelper;
import eu.rarogsoftware.rarog.platform.api.security.HumanOnly;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Hidden
@HumanOnly
public class MyAccountController {
    private final I18nHelper i18nHelper;

    public MyAccountController(I18nHelper i18nHelper) {
        this.i18nHelper = i18nHelper;
    }

    @GetMapping(value = {"account/my/*", "account/my"})
    public String displaySecondFactory(Model model) {
        model.addAttribute("title", i18nHelper.getText("account:account.page.title"));
        model.addAttribute("reactEntrypointName", "myaccount");
        return "reactapp";
    }
}
