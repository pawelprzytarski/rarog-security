package eu.rarogsoftware.rarog.platform.dynamic.frontend.resources;

import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoader;
import eu.rarogsoftware.rarog.platform.core.common.urlutils.BaseUrlHelper;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.AttributeValueQuotes;
import org.thymeleaf.model.IModel;
import org.thymeleaf.templatemode.TemplateMode;
import org.unbescape.html.HtmlEscape;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DynamicCssLoaderProcessor extends DynamicAssetsLoaderProcessor {
    private static final String TAG_NAME = "loadcss";
    private final boolean disableIntegrity;

    public DynamicCssLoaderProcessor(String dialectPrefix, boolean disableIntegrity) {
        super(
                TemplateMode.HTML,
                dialectPrefix,
                TAG_NAME,
                true,
                null,
                false,
                PRECEDENCE
        );
        this.disableIntegrity = disableIntegrity;
    }

    @Override
    protected IModel createModelForAssets(BaseUrlHelper baseUrlHelper, List<DynamicAssetsLoader.WebpackAssetData> assetsList, ITemplateContext context) {
        var modelFactory = context.getModelFactory();
        var model = modelFactory.createModel();
        assetsList.forEach(assetData -> {
            Map<String, String> attributes = new HashMap<>(Map.of(
                    "href", HtmlEscape.escapeHtml5(baseUrlHelper.completeRelativeUrl(assetData.source())),
                    "rel", "stylesheet"
            ));
            if (!disableIntegrity) {
                attributes.putAll(
                        Map.of(
                                "integrity", HtmlEscape.escapeHtml5(assetData.integrity()),
                                "crossorigin", "anonymous"));
            }
            model.add(modelFactory.createStandaloneElementTag("link", attributes, AttributeValueQuotes.DOUBLE, false, false));
        });
        return model;
    }

    @Override
    protected List<DynamicAssetsLoader.WebpackAssetData> getEntrypointAssets(DynamicAssetsLoader.EntrypointAssetData data) {
        return data.css();
    }
}
