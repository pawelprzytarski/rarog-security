package eu.rarogsoftware.rarog.platform.security.mfa;

import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaMethod;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaMethodsRegister;
import eu.rarogsoftware.rarog.platform.app.configuration.dev.DevModeComponent;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoader;
import eu.rarogsoftware.rarog.platform.api.resources.management.frontend.DynamicAssetsLoaderFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Set;

@DevModeComponent
public class DevMfaMethodsRegister implements MfaMethodsRegister {
    private final DynamicAssetsLoaderFactory assetsLoaderFactory;

    public DevMfaMethodsRegister(@Qualifier("main") DynamicAssetsLoaderFactory assetsLoaderFactory) {
        this.assetsLoaderFactory = assetsLoaderFactory;
    }

    @Override
    public Set<MfaMethod> getMfaMethods() {
        return Set.of(
                MfaMethod.builder()
                        .name("JUST-PASS")
                        .imageUrl("/static/images/codes.png")
                        .nameKey("mfa.methods.just-pass")
                        .descriptionKey("mfa.methods.just-pass.description")
                        .resource(assetsLoaderFactory.getNamedAssetsLoader("developmentMfa")
                                .flatMap(loader -> loader.getAssetData("justPass.js")
                                        .map(DynamicAssetsLoader.WebpackAssetData::source))
                                .orElse(null))
                        .order(1L)
                        .build()
        );
    }
}
