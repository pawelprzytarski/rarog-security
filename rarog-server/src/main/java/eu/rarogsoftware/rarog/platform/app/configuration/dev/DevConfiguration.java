package eu.rarogsoftware.rarog.platform.app.configuration.dev;

import eu.rarogsoftware.rarog.platform.core.boot.AsyncBootTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty("devMode")
public class DevConfiguration {
    private final Logger logger = LoggerFactory.getLogger(DevConfiguration.class);

    @Bean
    AsyncBootTask loggingAsyncBootTask() {
        return context -> {
            logger.info("Async task executed");
        };
    }

// TODO uncomment when H2 is migrated to jakarta
//
//    @Bean
//    ServletRegistrationBean h2servletRegistration(){
//        ServletRegistrationBean registrationBean = new ServletRegistrationBean( new WebServlet());
//        registrationBean.addUrlMappings("/h2-console/*");
//        return registrationBean;
//    }
}
