package eu.rarogsoftware.rarog.platform.core.common;

import eu.rarogsoftware.rarog.platform.api.security.AuthorityHelper;
import eu.rarogsoftware.rarog.platform.api.user.management.RarogUser;
import org.springframework.stereotype.Component;

@Component
public class SimpleHomePageService implements HomePageService {
    @Override
    public String getHomePageForUser(RarogUser rarogUser) {
        if (rarogUser == null || !AuthorityHelper.hasAdminRights(rarogUser)) {
            return "account/my";
        }
        return "admin";
    }
}
