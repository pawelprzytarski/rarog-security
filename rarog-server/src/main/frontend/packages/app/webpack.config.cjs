const webpack = require('webpack');
const WebpackAssetsManifest = require('webpack-assets-manifest');
const {merge} = require('webpack-merge');
const {common, assetsManifestCommonConfig} = require('../webpackCommon/src/webpackCommon');

module.exports = function (_env, argv) {
    return merge(common(_env, argv, undefined), {
        entry: {
            login: './src/login/index',
            boot: './src/boot/index',
            myaccount: './src/myaccount/index',
            admin: './src/admin/index',
            'theme/default': './src/theme/default/generic',
            'theme/default/admin/quickAccessMenu': './src/theme/default/admin/quickAccessMenu',
        },
        target: 'web',
        plugins: [
            new webpack.DefinePlugin({
                DEVELOPMENT: JSON.stringify(!!_env.dev),
                BUILT_AT: webpack.DefinePlugin.runtimeValue(Date.now)
            }),
            new WebpackAssetsManifest(assetsManifestCommonConfig('', 'app'))
        ].filter(Boolean),
        optimization: {
            runtimeChunk: false,
            splitChunks: {
                cacheGroups: {
                    vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name: 'vendors',
                        chunks: 'all'
                    }
                }
            }
        }
    });
};