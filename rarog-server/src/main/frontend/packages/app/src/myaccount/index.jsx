import '../utils/RegisterPluginDependencies';
import {AppFetch, DynamicResources, UrlUtils} from '@rarog/rarog-app-utils';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, {Suspense} from 'react';
import Alert from 'react-bootstrap/Alert';
import Nav from 'react-bootstrap/Nav';
import ReactDOM from 'react-dom';
import {ErrorBoundary} from 'react-error-boundary';
import {I18nextProvider, useTranslation} from 'react-i18next';
import {BrowserRouter, Route, Routes, useLocation, useNavigate, useParams} from 'react-router-dom';
import {Loading} from '../utils/Loading';
import './index.css';
import i18n from '../i18n';
import {Placeholder} from 'react-bootstrap';
import {DynamicResource} from "@rarog/dynamic-components";

const baseUrl = UrlUtils.getBaseUrl();
const AsyncResource = DynamicResources.AsyncResource;

function AccountSettingsTab({resource}) {
    const elements = resource.read();
    const [t] = useTranslation(['account', 'common']);
    const params = useParams();
    const tab = params.setting ? params.setting : 'main';
    const filtered = Object.values(elements)
        .flatMap(el => el)
        .filter(el => el.key === tab && el.resources)
        .flatMap(el => el.resources);
    const elementLink = filtered.length > 0 ? filtered[0] : null;
    if (elementLink) {
        return (<>
            <DynamicResource resource={UrlUtils.addBaseUrl(elementLink)} fallback={(
                <div>
                    <Placeholder xs={7}/> <Placeholder xs={4}/> <Placeholder xs={4}/>
                    <Placeholder xs={6}/> <Placeholder xs={8}/> <Placeholder xs={4}/>
                </div>
            )}/>
        </>);
    } else {
        return (<>
            <Alert variant="danger">
                <p>{t('common:data.loading.error')}</p>
            </Alert>
        </>);
    }
}

AccountSettingsTab.propTypes = {
    resource: PropTypes.object.isRequired,
};

function NavigationBar({resource}) {
    const elements = resource.read();
    const [t] = useTranslation(['account', 'common']);
    const nav = useNavigate();
    const {pathname} = useLocation();
    const pagePath = 'account/my/';
    const index = pathname.lastIndexOf(pagePath);
    const tabInUrl = index < 0 ? 'main' : pathname.substring(index + pagePath.length);
    const tab = tabInUrl.length === 0 ? 'main' : tabInUrl;
    return (
        <Nav activeKey={tab} onSelect={(selectKey) => nav(baseUrl + pagePath + (selectKey === 'main' ? '' : selectKey))}
             className="flex-column"
             variant="pills">
            {Object.entries(elements).map((entry) => {
                return (<div key={entry[0]}>
                    {entry[0] ? (<h6>{t(entry[0])}</h6>) : null}
                    {entry[1].map(element => (
                        <Nav.Item key={element.key}>
                            <Nav.Link eventKey={element.key}>{t(element.name)}</Nav.Link>
                        </Nav.Item>
                    ))}
                </div>);
            })}
        </Nav>
    );
}

NavigationBar.propTypes = {
    resource: PropTypes.instanceOf(AsyncResource).isRequired
};

const navigationElements = new DynamicResources.AsyncResource(AppFetch('/account/my/navigation').then(data => data.json()));

function MainPage() {
    const [t] = useTranslation(['account', 'common']);

    return (
        <>
            <ErrorBoundary
                fallback={
                    <Alert variant="danger">
                        <p>{t('common:data.loading.error')}</p>
                    </Alert>
                }>
                <BrowserRouter>
                    <nav className={'nav'}>
                        <div>
                            <h5>{t('account.page.title')}</h5>
                            <NavigationBar resource={navigationElements}/>
                        </div>
                    </nav>
                    <main className={'setting'}>
                        <Routes>
                            <Route path={baseUrl + 'account/my'}
                                   element={<AccountSettingsTab resource={navigationElements}/>}/>
                            <Route path={baseUrl + 'account/my/:setting'}
                                   element={<AccountSettingsTab resource={navigationElements}/>}/>
                        </Routes>
                    </main>
                </BrowserRouter>
            </ErrorBoundary>
        </>
    );
}

ReactDOM.render(
    <Suspense fallback={<Loading/>}>
        <I18nextProvider i18n={i18n}>
            <MainPage/>
        </I18nextProvider>
    </Suspense>,
    document.getElementById('react')
);