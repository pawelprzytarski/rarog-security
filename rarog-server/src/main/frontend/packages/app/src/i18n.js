import {UrlUtils} from '@rarog/rarog-app-utils';
import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import ChainingBackend from 'i18next-chained-backend';

import HttpBackend from 'i18next-http-backend';
import LocalStorageBackend from "i18next-localstorage-backend";
import {initReactI18next} from 'react-i18next';

i18n
    .use(ChainingBackend)
    .use(LanguageDetector)
    .use(initReactI18next)
    // for all options read: https://www.i18next.com/overview/configuration-options
    .init({
        fallbackLng: 'en',
        // eslint-disable-next-line no-undef
        debug: DEVELOPMENT,
        load: "languageOnly",
        backend: {
            backends: [
                LocalStorageBackend,
                HttpBackend
            ],
            backendOptions: [{
                // eslint-disable-next-line no-undef
                expirationTime: DEVELOPMENT ? 0 : 7 * 24 * 60 * 60 * 1000
            }, {
                loadPath: UrlUtils.getBaseUrl() + 'i18n/{{lng}}/{{ns}}.json'
            }]
        },
        react: {
            useSuspense: true,
            bindI18n: 'languageChanged',
            defaultTransParent: 'div',
            transWrapTextNodes: 'span',
            transSupportBasicHtmlNodes: true
        },
        detection: {
            order: ['querystring', 'cookie', 'localStorage', 'navigator', 'htmlTag'],
            lookupQuerystring: 'lng',
            lookupCookie: 'X-Chosen-Locale',
            lookupLocalStorage: 'X-Chosen-Locale',
            caches: ['localStorage', 'cookie']
        },
        interpolation: {
            escapeValue: false, // not needed for react as it escapes by default
        },

    });


export default i18n;
