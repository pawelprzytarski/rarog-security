import {UrlUtils} from '@rarog/rarog-app-utils';

let referrer = undefined;
let defaultRedirectAddress = 'account/my';

export function saveLoginReferrer() {
    const params = new URLSearchParams(window.location.search);
    referrer = params.has('redirect') ? params.get('redirect') : document.referrer;
}

const isAbsolute = new RegExp('^([a-z]+://|//)', 'i');

export function saveDefaultRedirect(defaultAddress) {
    defaultRedirectAddress = defaultAddress;
}

export function redirectAfterLogin(defaultAddress) {
    if (!defaultAddress) {
        defaultAddress = defaultRedirectAddress;
    }
    if (referrer) {
        const localReferrer = referrer;
        referrer = undefined;
        try {
            let url;
            if (isAbsolute.test(localReferrer)) {
                url = new URL(localReferrer);
                if (url.origin !== window.location.origin
                    || !url.pathname.startsWith(UrlUtils.getBaseUrl())) {
                    url = undefined;
                }
            } else {
                url = localReferrer.startsWith(UrlUtils.getBaseUrl()) ? localReferrer : new URL(UrlUtils.addAbsoluteBaseUrl(localReferrer));
            }
            if (url) {
                window.location = url;
                return;
            }
        } catch (ignored) {
            //ignored
        }
    }
    window.location = UrlUtils.addAbsoluteBaseUrl(defaultAddress);
}