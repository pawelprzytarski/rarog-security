import {AppFetch, StatusCodes, UrlUtils} from '@rarog/rarog-app-utils';
import React, {useEffect, useState} from 'react';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Spinner from 'react-bootstrap/Spinner';
import {useTranslation} from 'react-i18next';
import {useNavigate} from 'react-router-dom';
import './login.css';
import {redirectAfterLogin, saveDefaultRedirect} from './LoginRedirector';

export default function Login() {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [spinnerHidden, setSpinnerState] = useState(true);
    const [errorMessage, setErrorMessage] = useState('');
    const navigate = useNavigate();
    const {t} = useTranslation('core-admin');

    useEffect(() => {
        window.document.title = t('login.page.title');
    });

    function validateForm() {
        return spinnerHidden && login.length > 0 && password.length > 0;
    }

    function handleSubmit(event) {
        event.preventDefault();
        setSpinnerState(false);
        setErrorMessage('');
        setPassword('');
        AppFetch('/login/perform', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({username: login, password: password})
        })
            .then(response => {
                setSpinnerState(true);
                return response;
            })
            .then(response => {
                switch (response.status) {
                    case StatusCodes.OK:
                        return response.json()
                            .then(data => {
                                if (data['multiFactorLoginEnabled']) {
                                    saveDefaultRedirect(data['defaultAction']);
                                    navigate(UrlUtils.addBaseUrl('/login/mfa'));
                                } else {
                                    redirectAfterLogin(data['defaultAction']);
                                }
                            });
                    case StatusCodes.BAD_REQUEST:
                        return response.json()
                            .then(data => {
                                setErrorMessage(data.errorMessage);
                            });
                    default:
                        setErrorMessage(t('common:error.unknown'));
                        return;
                }
            })
            .catch(reason => {
                setErrorMessage(t('common:error.unknown'));
                console.error(reason);
                setSpinnerState(true);
            });
    }

    function renderErrorMessage() {
        if (errorMessage) {
            return (<Alert variant="danger" onClose={() => setErrorMessage('')} id={'loginErrorMessage'} dismissible>
                    <p>
                        {errorMessage}
                    </p>
                </Alert>
            );
        } else {
            return undefined;
        }
    }

    const errorMessageCode = renderErrorMessage();
    return (
        <section className="Login">
            <Form onSubmit={handleSubmit}>
                {errorMessageCode}
                <Form.Group size="lg" controlId="login">
                    <Form.Label>{t('login.form.username')}</Form.Label>
                    <Form.Control
                        aria-label={t('login.form.aria.username')}
                        aria-required="true"
                        autoFocus
                        type="login"
                        value={login}
                        onChange={(e) => setLogin(e.target.value)}
                    />
                </Form.Group>
                <Form.Group size="lg" controlId="password">
                    <Form.Label>{t('login.form.password')}</Form.Label>
                    <Form.Control
                        aria-label={t('login.form.aria.password')}
                        aria-required="true"
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Group>
                <div className="d-grid gap-2 ">
                    <Button size="lg" className="block" type="submit" id={'submit'}
                            aria-label={t('login.form.aria.loginButton')} disabled={!validateForm()}>
                        {t('login.form.loginButton')}
                    </Button>
                </div>
                <div className="d-grid gap-2">
                    <Spinner animation="border" variant="primary" hidden={spinnerHidden}/>
                </div>
            </Form>
        </section>
    );

}