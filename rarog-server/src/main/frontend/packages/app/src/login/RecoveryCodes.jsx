import {AppFetch, StatusCodes} from '@rarog/rarog-app-utils';
import PropTypes from 'prop-types';
import React, {useState} from 'react';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Spinner from 'react-bootstrap/Spinner';
import {useTranslation} from 'react-i18next';

export default function RecoveryCodes({successCallback}) {
    const {t} = useTranslation('core-admin');
    const [recoveryCode, setRecoveryCode] = useState('');
    const [spinnerHidden, setSpinnerState] = useState(true);
    const [errorMessage, setErrorMessage] = useState('');
    const [recoveryCodesDisplay, setRecoveryCodesDisplay] = useState('');

    function validateForm() {
        return spinnerHidden && recoveryCode.length > 0;
    }

    function handleSubmit(event) {
        event.preventDefault();
        setSpinnerState(false);
        setErrorMessage('');
        setRecoveryCodesDisplay('');
        AppFetch('/login/mfa/recovery-codes', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({recoveryCode: recoveryCode})
        })
            .then(response => {
                setSpinnerState(true);
                return response;
            })
            .then(response => {
                switch (response.status) {
                    case StatusCodes.OK:
                        return response.json()
                            .then(data => {
                                if (data.regenerated) {
                                    setRecoveryCodesDisplay(data.newCodes);
                                } else {
                                    successCallback();
                                }
                            });
                    case StatusCodes.BAD_REQUEST:
                        return response.json()
                            .then(() => {
                                setErrorMessage(t('login.error.InvalidCode'));
                            });
                    default:
                        setErrorMessage(t('common:error.unknown'));
                        return;
                }
            })
            .catch(reason => {
                setErrorMessage(t('common:error.unknown'));
                console.error(reason);
                setSpinnerState(true);
            });
    }

    function renderErrorMessage() {
        if (errorMessage) {
            return (<Alert variant="danger" onClose={() => setErrorMessage('')} id={'loginErrorMessage'} dismissible>
                    <p>
                        {errorMessage}
                    </p>
                </Alert>
            );
        } else {
            return undefined;
        }
    }

    const errorMessageCode = renderErrorMessage();

    function renderForm() {
        if (recoveryCodesDisplay) {
            return (
                <div>
                    <h3>{t('login.form.mfa.recovery.newCodes')}</h3>
                    <p>{t('login.form.mfa.recovery.newCodesDescription')}</p>
                    <ul id={'codes'}>
                        {recoveryCodesDisplay.map(code => (
                            <li key={code}>{code}</li>
                        ))}
                    </ul>
                    <Button variant="secondary" id="proceed"
                            onClick={() => successCallback()}>{t('login.form.mfa.recovery.newCodesAck')}</Button>
                </div>
            );
        } else {
            return (
                <Form onSubmit={handleSubmit}>
                    {errorMessageCode}
                    <h3>{t('login.form.mfa.recovery.info')}</h3>
                    <p>{t('login.form.mfa.recovery.description')}</p>
                    <Form.Group size="lg" controlId="code">
                        <Form.Control
                            aria-label={t('login.form.aria.recoverycode')}
                            aria-required="true"
                            autoFocus
                            type="text"
                            value={recoveryCode}
                            onChange={(e) => setRecoveryCode(e.target.value)}
                        />
                    </Form.Group>
                    <div className="d-grid gap-2 ">
                        <Button size="lg" className="block" type="submit" id="submit"
                                aria-label={t('login.form.aria.loginButton')} disabled={!validateForm()}>
                            {t('login.form.loginButton')}
                        </Button>
                    </div>
                    <div className="d-grid gap-2">
                        <Spinner animation="border" variant="primary" hidden={spinnerHidden}/>
                    </div>
                </Form>
            );
        }
    }

    return renderForm();
}

RecoveryCodes.propTypes = {
    successCallback: PropTypes.func.isRequired
};