import {UrlUtils} from '@rarog/rarog-app-utils';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, {Suspense} from 'react';
import Alert from 'react-bootstrap/Alert';
import {ErrorBoundary} from 'react-error-boundary';
import {useTranslation} from 'react-i18next';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import '../i18n';
import {Loading} from '../utils/Loading';
import '../utils/RegisterPluginDependencies';
import './index.css';
import {saveLoginReferrer} from './LoginRedirector';
import ReactDOM from 'react-dom';

const baseUrl = UrlUtils.getBaseUrl();

const Login = React.lazy(() => import('./login'));
const MfaMethodsSelection = React.lazy(() => import('./mfaMethodsSelection'));
const MfaMethod = React.lazy(() => import('./mfaMethod'));

saveLoginReferrer();

function MainPage() {
    const [t] = useTranslation('common');

    return (
        <div>
            <main>
                <ErrorBoundary
                    fallback={
                        <Alert variant="danger">
                            <p>{t('common:data.loading.error')}</p>
                        </Alert>
                    }>
                    <BrowserRouter>
                        <Routes>
                            <Route path={baseUrl + 'login'} element={<Login/>}/>
                            <Route path={baseUrl + 'login/mfa'} element={<MfaMethodsSelection/>}/>
                            <Route path={baseUrl + 'login/mfa/:method'} element={<MfaMethod/>}/>
                        </Routes>
                    </BrowserRouter>
                </ErrorBoundary>
            </main>
        </div>
    );
}

function SuspendedMainPage() {
    return (
        <Suspense fallback={<Loading/>}>
            <MainPage/>
        </Suspense>
    );
}

ReactDOM.render(
    <SuspendedMainPage/>,
    document.getElementById('react')
);