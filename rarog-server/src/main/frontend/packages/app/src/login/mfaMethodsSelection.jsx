import {AppFetch, DynamicResources, UrlUtils} from '@rarog/rarog-app-utils';
import PropTypes from 'prop-types';
import React, {Suspense} from 'react';
import Alert from 'react-bootstrap/Alert';
import {ErrorBoundary} from 'react-error-boundary';
import {useTranslation} from 'react-i18next';
import {Link} from 'react-router-dom';
import {Loading} from '../utils/Loading';
import './login.css';
import './multifactor.css';

const addBaseUrl = UrlUtils.addBaseUrl;
const CacheableAsyncResource = DynamicResources.CacheableAsyncResource;
const AsyncResource = DynamicResources.AsyncResource;

function MfaMethod({method}) {
    const [t] = useTranslation('core-admin');

    return (
        <Link to={method.name} id={method.name}>
            <img src={addBaseUrl(method.imageUrl)} title={method.name} alt={t(method.nameKey)}/>
            <div className={'data'}>
                <h3 className={'title'}>{t(method.nameKey)}</h3>
                <p className={'description'}>{t(method.descriptionKey)}</p>
            </div>
        </Link>
    );
}

MfaMethod.propTypes = {
    method: PropTypes.object.isRequired,
};

function MfaMethodsList({resource}) {
    const mfaMethods = resource.read();
    return (
        <ul className={'methodsList'}>
            {mfaMethods.sort((a, b) => a.order > b.order).map((method) => (
                <li key={method.name} className={'method'}>
                    <MfaMethod method={method}/>
                </li>
            ))}
        </ul>
    );
}

MfaMethodsList.propTypes = {
    resource: PropTypes.instanceOf(AsyncResource).isRequired
};

export default function MfaMethodsSelection() {
    const [t] = useTranslation('core-admin');
    const mfaMethodsResource = new CacheableAsyncResource('MfaMethods', () => AppFetch('/mfa/').then(data => data.json()));

    return (
        <Suspense fallback={<Loading/>}>
            <ErrorBoundary
                fallback={
                    <Alert variant="danger">
                        <p>{t('common:data.loading.error')}</p>
                    </Alert>
                }>
                <div>
                    <MfaMethodsList resource={mfaMethodsResource}/>
                </div>
            </ErrorBoundary>
        </Suspense>
    );
}