import '../utils/RegisterPluginDependencies'
import {AppFetch, DynamicResources, UrlUtils} from '@rarog/rarog-app-utils';
import React, {Suspense} from 'react';
import {useTranslation} from 'react-i18next';
import {Link, useParams} from 'react-router-dom';
import {redirectAfterLogin} from './LoginRedirector';
import {DynamicResource, IFrameMode} from "@rarog/dynamic-components";
import {Placeholder} from "react-bootstrap";

const addBaseUrl = UrlUtils.addBaseUrl;
const CacheableAsyncResource = DynamicResources.CacheableAsyncResource;

const mfaMethodsResource = new CacheableAsyncResource('MfaMethods', () => AppFetch('/mfa/').then(data => data.json()));

class UnrecognizedMethodError extends Error {
    constructor(message) {
        super(message);
        this.name = 'MyError';
    }
}

function loadBuildInMethod(method) {
    switch (method) {
        case 'TOTP':
            return React.lazy(() => import('./Totp'));
        case 'RECOVERY-CODE':
            return React.lazy(() => import('./RecoveryCodes'));
        default:
            throw new UnrecognizedMethodError();
    }
}

function loadMfaComponent(params) {
    const methods = mfaMethodsResource.read();
    const fallback = (<><Placeholder xs={5}/><Placeholder xs={2}/><Placeholder xs={6}/><Placeholder xs={4}/></>);
    const component = methods
        .filter(method => method.name === params.method)
        .map(method => {
            if (method.resource) {
                return (<DynamicResource resource={addBaseUrl(method.resource)} iframeMode={IFrameMode.DISALLOWED}
                                         key={params.method} successCallback={redirectAfterLogin} fallback={fallback}/>);
            } else {
                const Component = loadBuildInMethod(params.method);
                return (<Suspense key={params.method} fallback={fallback}><Component successCallback={redirectAfterLogin}/></Suspense>);
            }
        });
    if (component.length === 0) {
        throw new UnrecognizedMethodError();
    }
    return component[0];
}

export default function MfaMethod() {
    const params = useParams();
    const [t] = useTranslation('core-admin');
    let render;
    try {
        render = loadMfaComponent(params);
    } catch (e) {
        if (e instanceof UnrecognizedMethodError) {
            render = (<p>{t('errors.unrecognized.mfa.method')}</p>);
        } else {
            throw e;
        }
    }

    return (
        <div>
            <Link className={'navigation-go-back'} to={'/login/mfa'}>{t('navigation.go.back')}</Link>
            <div>
                {render}
            </div>
        </div>
    );
}