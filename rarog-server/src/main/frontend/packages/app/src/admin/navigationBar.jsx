import {useTranslation} from 'react-i18next';
import React, {useEffect} from 'react';
import {useLocation, useNavigate} from 'react-router-dom';
import {DynamicResources, UrlUtils} from '@rarog/rarog-app-utils';
import PropTypes from 'prop-types';
import {eventBus} from './eventBus';

const adminPanelUrl = '/admin';
const AsyncResource = DynamicResources.AsyncResource;

function toggleActive(event) {
    const element = event.currentTarget ? event.currentTarget : event.target;
    element.classList.toggle('active');
    if (!element.classList.contains('active')) {
        removeActive(element.nextSibling);
    }

    function removeActive(element) {
        if (!element) {
            return;
        }
        element.classList?.remove('active');
        for (const childNode of element.childNodes) {
            removeActive(childNode);
        }
    }
}

function navLinkClick(event, nav) {
    const linkKey = event.target.dataset.key;
    nav(UrlUtils.addBaseUrl(linkKey));
    event.preventDefault();
    return false;
}

function resolveNav(elements, t) {
    const nav = useNavigate();
    const {pathname} = useLocation();
    return (
        <ul>
            {elements.map((entry) => {
                const link = adminPanelUrl + '/' + entry.fullKey;
                const active = pathname === link || pathname.startsWith(link + '/');
                const linkClasses = (entry.subElements ? 'category ' : '')
                    + (active ? 'active ' : '')
                    + (pathname === link ? 'current ' : '')
                    + 'menuText';
                const subElements = entry.subElements?.filter(el => el.key !== '*');
                const standardLink = entry.navOnly && entry.resource;
                const linkAddress = UrlUtils.addBaseUrl(standardLink ? entry.resource : link);
                const onClick = standardLink ? () => {
                } : e => navLinkClick(e, nav);
                return (<li key={entry.key}>
                        <span onClick={toggleActive}
                              data-key={entry.fullKey}
                              className={linkClasses}>{entry.resource ?
                            <a data-key={link}
                               onClick={onClick}
                               href={linkAddress}
                            >{t(entry.name)}</a>
                            : t(entry.name)}
                            {subElements && subElements.length ?
                                <i className="fi-rr-arrow-small-down"></i> : null}
                        </span>
                    {subElements && subElements.length ? resolveNav(entry.subElements, t) : null}
                </li>);
            })
                .filter(el => !!el)}
        </ul>
    );
}

export function NavigationBar({resource}) {
    const elements = resource.read();
    const [t] = useTranslation(['core-admin', 'common']);

    const nav = useNavigate();

    function remoteNav(event) {
        nav(UrlUtils.addBaseUrl(event.key));
    }

    useEffect(() => {
        eventBus.register('mainRouterNav', remoteNav);
        return () => {
            eventBus.unregister('mainRouterNav', remoteNav);
        };
    }, []);

    return (<div id="navBar">
        {resolveNav(elements, t)}
    </div>);
}

NavigationBar.propTypes = {
    resource: PropTypes.instanceOf(AsyncResource).isRequired
};