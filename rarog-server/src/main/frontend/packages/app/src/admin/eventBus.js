export class EventBus {
    register(eventType, listener) {
        this.initListenersList(eventType);
        this.listeners[eventType].push(listener);
    }

    initListenersList(eventType) {
        if (!this.listeners) {
            this.listeners = {};
        }
        if (!this.listeners[eventType]) {
            this.listeners[eventType] = [];
        }
    }

    unregister(eventType, listener) {
        this.initListenersList(eventType);
        this.listeners[eventType] = this.listeners[eventType].filter(el => el === listener);
    }

    publish(event) {
        const eventType = event.type;
        this.initListenersList(eventType);
        this.listeners[eventType].forEach(el => el(event));
    }
}

export const eventBus = new EventBus();