import {DynamicResources, UrlUtils} from '@rarog/rarog-app-utils';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import Alert from 'react-bootstrap/Alert';
import {ErrorBoundary} from 'react-error-boundary';
import {useTranslation} from 'react-i18next';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import './adminPage.css';
import {NavigationBar} from './navigationBar';

const baseUrl = UrlUtils.getBaseUrl();
const AsyncResource = DynamicResources.AsyncResource;
const AdminDashboard = React.lazy(() => import('./adminDashboard'));
const AdminPanelTab = React.lazy(() => import('./adminPanelTab'));

function getSubRoutes(navigation, parentPath) {
    navigation = navigation.filter(element => !element.navOnly);
    const embedded = navigation.filter(element => element.embedded).map(transformToRoute);
    const direct = navigation.filter(element => !element.embedded).map(transformToRoute);

    function transformToRoute(element) {
        let children = [];
        const elementPath = element.element ? element.key : parentPath + '/' + element.key;
        if (element['subElements']) {
            children = getSubRoutes(element['subElements'], element.embedded ? '' : elementPath);
        }
        if (element.resource) {
            return (
                <React.Fragment key={element.fullKey}>
                    <Route path={elementPath}
                           key={element.fullKey}
                           element={<AdminPanelTab element={element}/>}
                    >
                        {children.embedded}
                    </Route>
                    {children.direct}
                </React.Fragment>
            );
        } else {
            return (
                <React.Fragment key={element.fullKey}>
                    <Route path={elementPath}
                           key={element.fullKey}>
                        {children.embedded}
                    </Route>
                    {children.direct}
                </React.Fragment>
            );
        }
    }

    return {embedded: embedded, direct: direct};
}

function AdminPanelRoutes({navigationResource}) {
    const navigation = navigationResource.read();
    const children = getSubRoutes(navigation.filter(el => el.key), baseUrl + 'admin');
    const [t] = useTranslation('common');
    return (
        <Routes>
            <Route path={baseUrl + 'admin'} key="admin" element={<AdminDashboard/>}>
                {children.embedded}
            </Route>
            {children.direct}
            <Route path="*" element={<Alert variant="warning">
                {t('error.not.found')}
            </Alert>}/>
        </Routes>
    );
}

AdminPanelRoutes.propTypes = {
    navigationResource: PropTypes.instanceOf(AsyncResource).isRequired
};

export default function MainPage({navigationElements}) {
    const [t] = useTranslation(['core-admin', 'common']);

    return (
        <>
            <ErrorBoundary
                fallback={
                    <Alert variant="danger">
                        <p>{t('common:data.loading.error')}</p>
                    </Alert>
                }>
                <BrowserRouter>
                    <nav className={'nav'}>
                        <div className="header" onClick={e => {
                            e.currentTarget.parentElement.classList.toggle('active');
                            e.preventDefault();
                        }}><h6>Menu</h6><i className="fi-rr-menu-burger sidebarToggle"></i></div>
                        <NavigationBar resource={navigationElements}/>
                    </nav>
                    <main className={'setting'}>
                        <AdminPanelRoutes navigationResource={navigationElements}/>
                    </main>
                </BrowserRouter>
            </ErrorBoundary>
        </>
    );
}

MainPage.propTypes = {
    navigationElements: PropTypes.object.isRequired
};