import './eventBus';
import '../utils/RegisterPluginDependencies';
import MainPage from './adminPage';
import QuickNav from './quickNav';
import {AppFetch, DynamicResources} from '@rarog/rarog-app-utils';
import ReactDOM from 'react-dom';
import React, {Suspense} from 'react';
import {Placeholder} from 'react-bootstrap';
import {I18nextProvider} from 'react-i18next';
import i18n from '../i18n';
import {Loading} from '../utils/Loading';


const navigationElements = new DynamicResources.AsyncResource(AppFetch('/admin/navigation').then(data => data.json()));

ReactDOM.render(
    <Suspense fallback={<Loading/>}>
        <I18nextProvider i18n={i18n}>
            <MainPage navigationElements={navigationElements}/>
        </I18nextProvider>
    </Suspense>,
    document.getElementById('react')
);

ReactDOM.render(
    <Suspense fallback={<Placeholder xs={7}/>}>
        <I18nextProvider i18n={i18n}>
            <QuickNav navigationElements={navigationElements}/>
        </I18nextProvider>
    </Suspense>,
    document.getElementById('adminQuickNavigation')
);
