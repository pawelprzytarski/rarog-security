import 'bootstrap/dist/css/bootstrap.min.css';
import React, {useEffect, useRef, useState} from 'react';
import {useTranslation} from 'react-i18next';
import {Dropdown, Form, InputGroup} from 'react-bootstrap';
import './quickNav.css';
import {UrlUtils} from '@rarog/rarog-app-utils';
import {eventBus} from './eventBus';
import PropTypes from 'prop-types';

const HISTORY_ELEMENTS_COUNT = 5;
const HISTORY_STORAGE_KEY = 'Rarog_AdminPanel_QuickNav_history';
const QUICK_NAV_GLOBAL_SHORTCUT = '/';
let transformedNavElements;

export default function QuickNav({navigationElements}) {
    const navElements = navigationElements.read();
    const [t] = useTranslation(['core-admin', 'common']);
    const [showDropDown, setShowDropDown] = useState(false);
    const itemsRef = useRef();
    const quickNavBox = useRef();
    let debounce;

    function transformNavigationElements(navElements) {
        const result = [];
        for (const element of navElements) {
            const elementKey = 'admin/' + element.fullKey;
            if (element.resource && element.key !== '*') {
                result.push({
                    key: elementKey,
                    name: t(element.name),
                    keywords: element.keywords ? element.keywords.split(',').map(keyword => t(keyword)).join(', ') : t(element.name)
                });
            }
            if (element.subElements) {
                result.push(...transformNavigationElements(element.subElements));
            }
        }
        return result;
    }

    if (!transformedNavElements) {
        transformedNavElements = transformNavigationElements(navElements);
    }

    function createDropdownItem(element, search) {
        function highlightSearch() {
            const start = element.keywords.indexOf(search);
            const end = start + search.length;
            if (start >= 0) {
                return (
                    <>
                        {start !== 0 ? element.keywords.substring(Math.max(start - 15, 0), start) : null}<b>{search}</b>{element.keywords.substring(end)}
                    </>
                );
            } else {
                return (
                    <>{element.keywords}</>
                );
            }
        }

        return <Dropdown.Item key={element.key} eventKey={element.key} onClick={event => event.preventDefault()}
                              href={UrlUtils.addBaseUrl('/' + element.key)}>
            <span className="name">{element.name}</span>
            <span className="keywords"> - {search ? highlightSearch() : element.keywords}</span>
        </Dropdown.Item>;
    }

    function getHistoricElements() {
        let history = JSON.parse(localStorage.getItem(HISTORY_STORAGE_KEY));
        const historyElements = {};
        if (!Array.isArray(history)) {
            history = [];
            transformedNavElements
                .slice(0, HISTORY_ELEMENTS_COUNT)
                .forEach(el => {
                    history.push(el.key);
                    historyElements[el.key] = el;
                });
        } else {
            transformedNavElements
                .filter(el => history.includes(el.key))
                .forEach(el => historyElements[el.key] = el);
        }

        return (<>
            {history
                .map(key => historyElements[key])
                .filter(element => element)
                .map(element => createDropdownItem(element))}
        </>);
    }

    function selectQuickNavElement(eventKey) {
        let history = JSON.parse(localStorage.getItem(HISTORY_STORAGE_KEY));
        if (!Array.isArray(history)) {
            history = [];
        }
        if (history.includes(eventKey)) {
            history = history.filter(el => el !== eventKey);
        }
        history = [eventKey, ...history.slice(0, HISTORY_ELEMENTS_COUNT - 1)];
        localStorage.setItem(HISTORY_STORAGE_KEY, JSON.stringify(history));
        setShowDropDown(false);
        setItems(getHistoricElements());
        eventBus.publish({type: 'mainRouterNav', key: '/' + eventKey});
    }

    const [items, setItems] = useState(getHistoricElements());

    function autocomplete(event) {
        clearTimeout(debounce);
        const search = event.target.value;
        debounce = setTimeout(() => {
            if (search) {
                setItems(<>
                    {transformedNavElements
                        .filter(el => el.name.indexOf(search) >= 0 || el.keywords.indexOf(search) >= 0)
                        .slice(0, HISTORY_ELEMENTS_COUNT)
                        .map(element => createDropdownItem(element, search))}
                </>);
            } else {
                setItems(getHistoricElements());
            }
        }, 100);
    }

    function keyDown(event) {
        function getCurrentSelected() {
            let selectedItem;
            for (const child of itemsRef.current.children) {
                if (child.classList.contains('selected')) {
                    selectedItem = child;
                }
            }
            return selectedItem;
        }

        if (event.key === 'ArrowDown') {
            let selectedItem = getCurrentSelected();
            selectedItem?.classList.toggle('selected');
            let nextChild = selectedItem ? selectedItem.nextSibling : undefined;
            if (!nextChild) {
                nextChild = itemsRef.current.firstChild;
            }
            nextChild?.classList.toggle('selected');
            event.preventDefault();
            return;
        }
        if (event.key === 'ArrowUp') {
            let selectedItem = getCurrentSelected();
            selectedItem?.classList.toggle('selected');
            let previousChild = selectedItem ? selectedItem.previousSibling : undefined;
            if (!previousChild) {
                previousChild = itemsRef.current.lastChild;
            }
            previousChild?.classList.toggle('selected');
            event.preventDefault();
            return;
        }
        if (event.key === 'Enter') {
            getCurrentSelected()?.click();
            event.preventDefault();
        }
        if (event.key === 'Escape') {
            quickNavBox.current.blur();
            setShowDropDown(false);
        }
    }

    function removeFocusQuickNavSearchBox(event) {
        let level = 0;
        let testElement = event.relatedTarget;
        while (testElement || level < 5) {
            if (testElement === itemsRef.current) {
                return;
            }
            level++;
            testElement = testElement?.parentElement;
        }
        setShowDropDown(false);
    }

    function pageKeyDown(event) {
        if (event.target === document.body
            && event.key === QUICK_NAV_GLOBAL_SHORTCUT) {
            quickNavBox.current.select();
            quickNavBox.current.focus();
            event.preventDefault();
        }
    }

    useEffect(() => {
        window.addEventListener('keydown', pageKeyDown);
        return () => {
            window.removeEventListener('keydown', pageKeyDown);
        };
    }, []);

    return (
        <div>
            <InputGroup onFocus={() => setShowDropDown(true)}
                        onBlur={removeFocusQuickNavSearchBox}>
                <Form.Control type="text" onChange={autocomplete} onKeyDown={keyDown} ref={quickNavBox}
                              aria-label={t('admin.page.quickNav.title')}/>
                <InputGroup.Text>{QUICK_NAV_GLOBAL_SHORTCUT}</InputGroup.Text>
            </InputGroup>
            <Dropdown show={showDropDown} onSelect={event => selectQuickNavElement(event)}>
                <Dropdown.Menu ref={itemsRef}>
                    {items}
                </Dropdown.Menu>
            </Dropdown>
        </div>
    );
}

QuickNav.propTypes = {
    navigationElements: PropTypes.object.isRequired
};