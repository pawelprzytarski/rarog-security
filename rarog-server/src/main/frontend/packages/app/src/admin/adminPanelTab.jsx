import {useTranslation} from 'react-i18next';
import React from 'react';
import {UrlUtils} from '@rarog/rarog-app-utils';
import {Placeholder} from 'react-bootstrap';
import Alert from 'react-bootstrap/Alert';
import PropTypes from 'prop-types';
import {ErrorBoundary} from "react-error-boundary";
import {DynamicResource} from "@rarog/dynamic-components";

export default function AdminPanelTab({element}) {
    const [t] = useTranslation(['core-admin', 'common']);
    return (<ErrorBoundary fallback={(<Alert variant="danger">
        <p>{t('common:data.loading.error')}</p>
    </Alert>)}>
        <DynamicResource resource={UrlUtils.addBaseUrl(element.resource)} fallback={(<div>
            <Placeholder xs={7}/> <Placeholder xs={4}/> <Placeholder xs={4}/>
            <Placeholder xs={6}/> <Placeholder xs={8}/> <Placeholder xs={4}/>
        </div>)}/>
    </ErrorBoundary>);
}

AdminPanelTab.propTypes = {
    element: PropTypes.object.isRequired,
};