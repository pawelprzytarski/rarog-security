import {StatusCodes} from "@rarog/rarog-app-utils";
import React, {useEffect, useState} from "react";
import {ProgressBar} from "react-bootstrap";
import "./boot.css"
import logo from "./bootLogo.svg"

export function Boot() {
    const [progressNow, setProgressNow] = useState(0);
    const [currentStep, setCurrentStep] = useState("0/0");

    useEffect(() => {
        window.document.title = 'App is starting...';
    })

    function updateStatus() {
        fetch("status", {
            method: 'GET'
        })
            .then(response => {
                switch (response.status) {
                    case StatusCodes.OK:
                        if(response.redirected) {
                            setProgressNow(100);
                            setCurrentStep('Finished!');
                            window.location = response.url;
                        }

                        return response.json()
                            .then(data => {
                                setProgressNow(data['progress']);
                                setCurrentStep(data['step']);
                            });
                }
            })
            .catch(reason => {
                console.log(reason);
                setCurrentStep("Step checking error. App may failed to start!");
            })
    }

    setInterval(updateStatus, 1000);

    return (
        <div className="loading">
            <div className="info">
                <img src={logo} alt="Loading logo"/>
                <h3>App is starting</h3>
                <p>Current step: {currentStep}</p>
                <ProgressBar animated now={progressNow}/>
            </div>
        </div>
    );

}
