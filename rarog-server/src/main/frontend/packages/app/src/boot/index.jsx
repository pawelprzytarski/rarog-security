import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import {Boot} from './boot';
import {createRoot} from 'react-dom/client';

createRoot(document.getElementById('react')).render(<Boot/>);