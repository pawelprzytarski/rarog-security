import * as AppUtils from '@rarog/rarog-app-utils';
import * as React from 'react';
import * as Bootstrap from 'bootstrap';
import * as ReactI18Next from 'react-i18next';
import * as ReactBootstrap from 'react-bootstrap';
import * as ReactRouterDom from 'react-router-dom';

let globals = window.globals || [];
window.globals = globals;

globals['react'] = React;
globals['react-router-dom'] = ReactRouterDom;
globals['react-i18next'] = ReactI18Next;
globals['@rarog/rarog-app-utils'] = AppUtils;
globals['react-bootstrap'] = ReactBootstrap;
globals['bootstrap'] = Bootstrap;