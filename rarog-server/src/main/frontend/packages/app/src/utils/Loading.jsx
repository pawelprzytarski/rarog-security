import React from 'react';
import Spinner from "react-bootstrap/Spinner";

export function Loading() {

    const divStyle = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        minHeight: '100vh',
        backgroundColor: '#7a7a7a',
        opacity: '50%'
    }

    return (
        <div style={divStyle}>
            <Spinner animation={"border"} variant="primary" aria-hidden={true} role={"status"}>
                <p className="visually-hidden">Loading...</p>
            </Spinner>
        </div>
    );
}