import '../../../utils/RegisterPluginDependencies'
import React, {Suspense, useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import {I18nextProvider, useTranslation} from 'react-i18next';
import i18n from '../../../i18n';
import {AppFetch, DynamicResources, UrlUtils} from '@rarog/rarog-app-utils';
import {Dropdown, Placeholder} from 'react-bootstrap';
import './quickAccessMenu.css';
import {DynamicResource} from "@rarog/dynamic-components";

const quickAccessElements = new DynamicResources.AsyncResource(AppFetch('/ui/fragments/adminPanelQuickAccess')
    .then(data => data.json())
    .then(data => data.map(el => ({
        key: el.key,
        component: UrlUtils.addBaseUrl(el.resource)
    })))
);

function QuickAccessMenu() {
    const elements = quickAccessElements.read();
    const [username, setUsername] = useState();
    const [avatar, setAvatar] = useState();
    const [t] = useTranslation('core-admin');

    useEffect(() => {
        setUsername(document.querySelector('meta[name="user-name"]').content);
        const avatar = document.querySelector('meta[name="user-avatar"]')?.content;
        if (avatar) {
            setAvatar(avatar);
        }
    }, []);

    return (
        <>
            {elements.map(el => (
                <DynamicResource resource={el.component} key={el.key} fallback={(<Placeholder xs={2}/>)}/>
            ))}
            <Dropdown className="profile dropdown">
                <Dropdown.Toggle title={t('admin.page.quickMenu.profile.button')} variant="secondary">
                    {avatar ? (<img src={avatar} alt="avatar" className="userIcon"/>) : (
                        <i className="fi-rr-user userIcon"></i>)} {username}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    <Dropdown.Item data-key="profile" href={UrlUtils.addBaseUrl('/account/my')}>Profile</Dropdown.Item>
                    <Dropdown.Item data-key="logout" href={UrlUtils.addBaseUrl('/logout')}>Logout</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </>
    );
}

window.addEventListener('load', () => {
    ReactDOM.render(
        <Suspense fallback={<Placeholder xs={6}/>}>
            <I18nextProvider i18n={i18n}>
                <QuickAccessMenu/>
            </I18nextProvider>
        </Suspense>,
        document.getElementById('quickAccessMenu')
    );
});