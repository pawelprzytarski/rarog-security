const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const path = require('path');

function assetsManifestCommonConfig(subdirectory, name) {
    return {
        publicPath: '/static/',
        output: `../descriptors/${subdirectory}webpack-${name}.manifest.json`,
        entrypoints: true,
        entrypointsUseAssets: true,
        integrity: true
    };
}

function plugin(_env, argv) {
    return {
        output: {
            libraryTarget: 'module',
            globalObject: 'globals'
        },
        experiments: {
            outputModule: true
        }
    };
}

function common(_env, argv, category) {
    const mode = _env.dev ? 'development' : 'production';
    const antiCacheSuffix = !_env.dev ? '[contenthash]' : 'debug';
    const outputPath = path.normalize(__dirname + '../../../../../../../target/classes/static/');
    console.log('Output directory:' + outputPath);
    const subdirectory = category ? category + '/' : '';
    return {
        mode: mode,
        devtool: _env.dev ? 'inline-source-map' : undefined,
        target: 'web',
        output: {
            path: outputPath,
            filename: `js/${subdirectory}[name].${antiCacheSuffix}.js`,
            chunkFilename: `js/${subdirectory}[name].${antiCacheSuffix}.js`,
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: `css/[name].${antiCacheSuffix}.css`,
                chunkFilename: `css/[name].${antiCacheSuffix}.css`
            }),
            new CompressionPlugin({
                algorithm: 'gzip',
                threshold: 1024,
                minRatio: 0.8
            })
        ].filter(Boolean),
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    use: [{
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            cacheCompression: true,
                            presets: ['@babel/preset-env', '@babel/preset-react']
                        }
                    }]
                },
                {
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        'css-loader'
                    ]
                },
                {
                    test: /\.svg$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 8 * 1024,
                            },
                        },
                    ],
                }
            ]
        },
        resolve: {
            extensions: ['.js', '.jsx'],
        },
        watch: !!_env.watch,
        watchOptions: {
            poll: true,
            ignored: '**/node_modules/'
        }
    };
}

module.exports = {plugin, common, assetsManifestCommonConfig};