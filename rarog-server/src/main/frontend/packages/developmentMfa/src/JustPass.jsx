import {AppFetch, UrlUtils} from '@rarog/rarog-app-utils';
import PropTypes from 'prop-types';
import React from 'react';

export function JustPass({successCallback}) {

    function passMfa(e) {
        e.preventDefault();
        AppFetch('/login/mfa/justpass')
            .then(() => {
                successCallback();
            });
        return false;
    }

    return (
        <p>Just pass: <a id={'justPassLink'} href={UrlUtils.addBaseUrl('/login/mfa/justpass')} onClick={passMfa}>Click
            here!</a></p>
    );
}

JustPass.propTypes = {
    successCallback: PropTypes.func.isRequired
};

export default JustPass;