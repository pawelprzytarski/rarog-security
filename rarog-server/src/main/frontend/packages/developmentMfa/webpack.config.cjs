const WebpackAssetsManifest = require('webpack-assets-manifest');
const webpack = require("webpack")

const {merge} = require("webpack-merge")
const {common, plugin, assetsManifestCommonConfig} = require("../webpackCommon/src/webpackCommon")

module.exports = function (_env, argv) {
    return merge(common(_env, argv, 'plugins'), plugin(_env, argv),
        {
            entry: {
                justPass: './src/JustPass'
            },
            externals: {
                react: 'global react',
                'app-utils': 'global @rarog/rarog-app-utils'
            },
            plugins: [
                new webpack.DefinePlugin({
                    DEVELOPMENT: JSON.stringify(!!_env.dev),
                    BUILT_AT: webpack.DefinePlugin.runtimeValue(Date.now)
                }),
                new WebpackAssetsManifest(assetsManifestCommonConfig('', 'developmentMfa'))
            ].filter(Boolean),
        })
}

