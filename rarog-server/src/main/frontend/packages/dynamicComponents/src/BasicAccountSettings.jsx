import {AppFetch, DynamicResources, StatusCodes} from '@rarog/rarog-app-utils';
import PropTypes from 'prop-types';
import React, {useEffect, useState} from 'react';
import {useTranslation} from 'react-i18next';
import {Alert, Button, Form, Spinner} from 'react-bootstrap';

function ChangePassword({settings}) {
    const [t] = useTranslation(['account', 'common']);
    const passwordRegex = new RegExp(settings.passwordRegex);
    const [validated, setValidated] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState(false);
    const [spinnerHidden, setSpinnerHidden] = useState(true);
    const [newPasswordInvalid, setNewPasswordInvalid] = useState(false);
    if (settings.canUserChangePassword) {
        const handleSubmit = (event) => {
            event.preventDefault();
            event.stopPropagation();
            const form = event.currentTarget;
            let isValid = form.checkValidity();
            if (!form.elements.newPassword.value
                || !passwordRegex.test(form.elements.newPassword.value)
                || form.elements.newPassword.value !== form.elements.repeatPassword.value) {
                isValid = false;
                setNewPasswordInvalid(true);
            } else {
                setNewPasswordInvalid(false);
            }
            setValidated(true);
            setErrorMessage('');
            setSuccessMessage(false);

            if (isValid) {
                setValidated(false);
                setSpinnerHidden(false);
                const request = {
                    oldPassword: form.elements.oldPassword.value,
                    newPassword: form.elements.newPassword.value,
                    repeatPassword: form.elements.repeatPassword.value
                };
                form.reset();
                AppFetch('/account/my/main/settings/changePassword', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(request)
                })
                    .then(response => {
                        setSpinnerHidden(true);
                        switch (response.status) {
                            case StatusCodes.UNAUTHORIZED:
                                window.location.reload();
                                return;
                            case StatusCodes.OK:
                                return response.json()
                                    .then(data => {
                                        setSuccessMessage(true);
                                        setErrorMessage(data.message);
                                    });
                            case StatusCodes.BAD_REQUEST:
                                return response.json()
                                    .then(data => {
                                        setErrorMessage(data.message);
                                    });
                            default:
                                setErrorMessage(t('common:error.unknown'));
                                return;
                        }
                    })
                    .catch(reason => {
                        setErrorMessage(t('common:error.unknown'));
                        console.error(reason);
                        setSpinnerHidden(true);
                    });
            } else {
                setErrorMessage('common:error.incorrectly.filled.form');
            }
        };

        const renderErrorMessage = () => {
            if (errorMessage) {
                return (
                    <Alert variant={successMessage ? 'success' : 'danger'} onClose={() => setErrorMessage('')}
                           id="changePasswordErrorMessage"
                           dismissible>
                        <p>
                            {t(errorMessage)}
                        </p>
                    </Alert>
                );
            } else {
                return undefined;
            }
        };

        return (
            <div>
                <h5 className="sectionHeader">{t('account.setting.main.forms.password.change.header')}</h5>
                <Form noValidate onSubmit={handleSubmit}>
                    {renderErrorMessage()}
                    <Form.Group className="mb-3" controlId="oldPassword">
                        <Form.Label>{t('account.setting.main.forms.password.change.old.pass')}</Form.Label>
                        <Form.Control type="password"
                                      placeholder="..."
                                      isValid={validated}
                                      required/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="newPassword">
                        <Form.Label>{t('account.setting.main.forms.password.change.new.pass')}</Form.Label>
                        <Form.Control type="password"
                                      placeholder="..."
                                      isInvalid={newPasswordInvalid}
                                      pattern={settings.passwordRegex}
                                      required/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="repeatPassword">
                        <Form.Label>{t('account.setting.main.forms.password.change.repeat.pass')}</Form.Label>
                        <Form.Control type="password"
                                      placeholder="..."
                                      isInvalid={newPasswordInvalid}
                                      pattern={settings.passwordRegex}
                                      required/>
                        <Form.Control.Feedback style={{'position': 'relative'}} type="invalid" tooltip>
                            {t('account.setting.main.errors.password.not.matching')}
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Button variant="primary" type="submit" id="changePasswordButton">
                        {t('account.setting.main.forms.password.change.submit')}
                    </Button>
                    <div className="d-grid gap-2">
                        <Spinner animation="border" variant="primary" hidden={spinnerHidden}/>
                    </div>
                </Form>
            </div>
        );
    } else {
        return (
            <Alert variant="warning">
                <p>{t('account.setting.errors.password.change.not.enabled')}</p>
            </Alert>
        );
    }
}

ChangePassword.propTypes = {
    settings: PropTypes.object.isRequired,
};

function Settings({resource}) {
    const settings = resource.read();
    const [t] = useTranslation('account');
    return (
        <div>
            <h3>{t('account.setting.main.hello', {username: settings.username})}</h3>
            <ChangePassword settings={settings}/>
        </div>
    );
}

Settings.propTypes = {
    resource: PropTypes.object.isRequired,
};

function BasicAccountSettings() {
    const resource = new DynamicResources.AsyncResource(AppFetch('/account/my/main/settings').then(data => data.json()));
    const [t] = useTranslation('account');
    useEffect(() => {
        document.title = t('account.page.title');
    });
    return (
        <Settings resource={resource}/>
    );
}

export default BasicAccountSettings;