const WebpackAssetsManifest = require('webpack-assets-manifest');
const webpack = require('webpack');

const {merge} = require('webpack-merge');
const {common, plugin, assetsManifestCommonConfig} = require('../webpackCommon/src/webpackCommon');

module.exports = function (_env, argv) {
    return merge(common(_env, argv, 'plugins'), plugin(_env, argv),
        {
            entry: {
                basicAccountSettings: './src/BasicAccountSettings',
                'admin/generalSettings': './src/admin/generalSettings',
                'admin/systemInfo': './src/admin/systemInfo',
            },
            externals: {
                react: 'global react',
                'app-utils': 'global @rarog/rarog-app-utils',
                'bootstrap': 'global bootstrap',
                'react-bootstrap': 'global react-bootstrap',
                'react-i18next': 'global react-i18next',
                'react-router-dom': 'global react-router-dom'
            },
            plugins: [
                new webpack.DefinePlugin({
                    DEVELOPMENT: JSON.stringify(!!_env.dev),
                    BUILT_AT: webpack.DefinePlugin.runtimeValue(Date.now)
                }),
                new WebpackAssetsManifest(assetsManifestCommonConfig('', 'dynamicComponents'))
            ].filter(Boolean),
        });
};

