package ut.rarogsoftware.rarog.platform.core.plugins.web;

import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.web.HandlerMappingDescriptor;
import eu.rarogsoftware.rarog.platform.core.plugins.web.PluginMappingHandlerMapping;
import eu.rarogsoftware.rarog.platform.core.plugins.web.PluginServletHttpServletRequestWrapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.HandlerMapping;

import jakarta.servlet.http.HttpServletMapping;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PluginMappingHandlerMappingTest {
    private final String TEST_URL = "/plugin/test/test";
    @Mock
    HttpServletRequest httpRequest;
    @Mock
    HttpServletMapping httpServletMapping;
    @Mock
    Plugin plugin;
    @Mock
    HandlerMapping testMapping;
    @Mock
    HandlerExecutionChain executionChain;
    PluginMappingHandlerMapping handlerMapping;

    @BeforeEach
    void setUp() {
        this.handlerMapping = new PluginMappingHandlerMapping();
        lenient().when(httpRequest.getHttpServletMapping()).thenReturn(httpServletMapping);
    }

    @Test
    void emptyMappingReturnsNothing() throws Exception {
        when(httpRequest.getServletPath()).thenReturn("/plugin/backdoor/test");
        when(httpRequest.getRequestURI()).thenReturn(TEST_URL);

        var result = handlerMapping.getHandler(httpRequest);

        assertNull(result);
    }

    @Test
    void registeredMappingDoNotMatchGeneralUrl() throws Exception {
        when(httpRequest.getServletPath()).thenReturn("/test");
        handlerMapping.plugDescriptor(plugin, getSimpleDescriptor());

        var result = handlerMapping.getHandler(httpRequest);

        assertNull(result);
    }

    @Test
    void registeredMappingDoNotMatchBaseUrl() throws Exception {
        when(httpRequest.getServletPath()).thenReturn("/plugin/another/test");
        when(httpRequest.getRequestURI()).thenReturn("/plugin/another/test");
        handlerMapping.plugDescriptor(plugin, getSimpleDescriptor());

        var result = handlerMapping.getHandler(httpRequest);

        assertNull(result);
    }

    @Test
    void registeredMappingDoNotMatchUrl() throws Exception {
        when(httpRequest.getServletPath()).thenReturn(TEST_URL);
        when(httpRequest.getRequestURI()).thenReturn(TEST_URL);
        handlerMapping.plugDescriptor(plugin, getSimpleDescriptor());

        var result = handlerMapping.getHandler(httpRequest);

        assertNull(result);
    }

    @Test
    void registeredMappingMatchUrl() throws Exception {
        when(httpRequest.getServletPath()).thenReturn(TEST_URL);
        when(httpRequest.getRequestURI()).thenReturn(TEST_URL);
        when(testMapping.getHandler(httpRequest)).thenReturn(executionChain);
        handlerMapping.plugDescriptor(plugin, getSimpleDescriptor());

        var result = handlerMapping.getHandler(httpRequest);

        assertEquals(executionChain, result);
    }

    @Test
    void registeredNotExactMatchingMatchUrl() throws Exception {
        when(httpRequest.getServletPath()).thenReturn(TEST_URL);
        when(httpRequest.getRequestURI()).thenReturn(TEST_URL);
        when(testMapping.getHandler(any())).thenReturn(null);
        when(testMapping.getHandler(any(PluginServletHttpServletRequestWrapper.class))).thenReturn(executionChain);
        handlerMapping.plugDescriptor(plugin, getSimpleDescriptor());

        var result = handlerMapping.getHandler(httpRequest);

        assertEquals(executionChain, result);
    }

    @Test
    void registeredExactMatchingMatchUrl() throws Exception {
        when(httpRequest.getServletPath()).thenReturn(TEST_URL);
        when(httpRequest.getRequestURI()).thenReturn(TEST_URL);
        when(testMapping.getHandler(any())).thenReturn(null);
        lenient().when(testMapping.getHandler(any(PluginServletHttpServletRequestWrapper.class))).thenReturn(executionChain);
        handlerMapping.plugDescriptor(plugin, new HandlerMappingDescriptor("/test/", Collections.singleton(testMapping), true));

        var result = handlerMapping.getHandler(httpRequest);

        assertNull(result);
    }

    @Test
    void registeredMultipleMappingsMatchCorrectOne() throws Exception {
        when(httpRequest.getServletPath()).thenReturn(TEST_URL);
        when(httpRequest.getRequestURI()).thenReturn(TEST_URL);
        when(testMapping.getHandler(httpRequest)).thenReturn(executionChain);
        handlerMapping.plugDescriptor(plugin, new HandlerMappingDescriptor("/test/", Arrays.asList(mock(HandlerMapping.class), mock(HandlerMapping.class), testMapping)));

        var result = handlerMapping.getHandler(httpRequest);

        assertEquals(executionChain, result);
    }

    @Test
    void registeredMultipleDescriptorsUseCorrectOne() throws Exception {
        when(httpRequest.getServletPath()).thenReturn(TEST_URL);
        when(httpRequest.getRequestURI()).thenReturn(TEST_URL);
        when(testMapping.getHandler(httpRequest)).thenReturn(executionChain);
        handlerMapping.plugDescriptor(plugin, new HandlerMappingDescriptor("/another/", Collections.singleton(mock(HandlerMapping.class))));
        handlerMapping.plugDescriptor(plugin, getSimpleDescriptor());

        var result = handlerMapping.getHandler(httpRequest);

        assertEquals(executionChain, result);
    }

    @Test
    void attemptRegisterMultipleDescriptorsUnderSameBaseUrl() throws Exception {
        handlerMapping.plugDescriptor(plugin, getSimpleDescriptor());
        assertThrows(IllegalArgumentException.class, () -> handlerMapping.plugDescriptor(plugin, getSimpleDescriptor()));
    }

    @Test
    void unregisterDescriptorDoNotReturnsHandler() throws Exception {
        when(httpRequest.getServletPath()).thenReturn(TEST_URL);
        when(httpRequest.getRequestURI()).thenReturn(TEST_URL);
        handlerMapping.plugDescriptor(plugin, getSimpleDescriptor());
        when(plugin.getKey()).thenReturn("Key1");

        handlerMapping.unplugDescriptor(plugin, getSimpleDescriptor());
        var result = handlerMapping.getHandler(httpRequest);

        assertNull(result);
        verify(testMapping, times(0)).getHandler(any());
    }

    @Test
    void attemptToUnregisterDescriptorOfDifferentPluginFails() throws Exception {
        when(httpRequest.getServletPath()).thenReturn(TEST_URL);
        when(httpRequest.getRequestURI()).thenReturn(TEST_URL);
        when(testMapping.getHandler(httpRequest)).thenReturn(executionChain);
        handlerMapping.plugDescriptor(plugin, getSimpleDescriptor());
        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getKey()).thenReturn("Key2");
        when(plugin.getKey()).thenReturn("Key1");

        handlerMapping.unplugDescriptor(mockPlugin, getSimpleDescriptor());
        var result = handlerMapping.getHandler(httpRequest);

        assertEquals(executionChain, result);
    }

    @Test
    void attemptToUnregisterUnknownDescriptor() throws Exception {
        when(httpRequest.getServletPath()).thenReturn(TEST_URL);
        when(httpRequest.getRequestURI()).thenReturn(TEST_URL);

        var result = handlerMapping.getHandler(httpRequest);

        assertNull(result);
    }

    private HandlerMappingDescriptor getSimpleDescriptor() {
        return new HandlerMappingDescriptor("/test/", Collections.singleton(testMapping), false);
    }
}