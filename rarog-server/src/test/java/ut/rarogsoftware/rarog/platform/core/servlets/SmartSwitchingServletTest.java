package ut.rarogsoftware.rarog.platform.core.servlets;

import eu.rarogsoftware.rarog.platform.api.metrics.MetricsService;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.web.servlets.*;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys;
import eu.rarogsoftware.rarog.platform.core.plugins.web.servlets.SmartSwitchingServlet;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.util.UrlPathHelper;

import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SmartSwitchingServletTest {
    @Mock
    DispatcherServlet dispatcherServlet;
    @Mock
    WebMvcProperties webMvcProperties;
    @Mock
    WebMvcProperties.Servlet servlet;
    @Mock
    ApplicationSettings applicationSettings;
    @Mock
    UrlPathHelper urlPathHelper;
    @Mock
    HttpServletRequest httpRequest;
    @Mock
    HttpServletResponse httpResponse;
    @Mock
    ServletConfig config;
    @Mock(extraInterfaces = {AuthorizationManagerAware.class, CsrfProtectionAware.class})
    HttpServlet anotherServlet;
    @Mock(extraInterfaces = AuthorizationManagerAware.class)
    HttpServlet differentServlet;
    @Mock
    Plugin plugin;
    @Mock(answer = Answers.RETURNS_MOCKS)
    MetricsService metricsService;
    @Mock
    AuthorizationManager authorizationManager;
    @InjectMocks
    SmartSwitchingServlet smartSwitchingServlet;
    private final String TEST_PATH = "/test/";

    @BeforeEach
    void setUp() {
        when(webMvcProperties.getServlet()).thenReturn(servlet);
        lenient().when(plugin.getKey()).thenReturn("TestKey");
        lenient().when(applicationSettings.isTrue(SystemSettingKeys.FORCE_SYSTEM_API)).thenReturn(false);
    }

    @Test
    void emptySwitchingServletUsesSpringDispatcher() throws ServletException, IOException {
        prepareStandardPaths();

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet).init(any());
        verify(dispatcherServlet).service(httpRequest, httpResponse);
    }

    @Test
    void emptySwitchingServletReturns404ForNotMatchedSpring() throws ServletException, IOException {
        when(servlet.getServletMapping()).thenReturn("/other/");
        when(urlPathHelper.getPathWithinApplication(any())).thenReturn(TEST_PATH);

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet, times(0)).service(httpRequest, httpResponse);
        verify(httpResponse).sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    @Test
    void catchAllMatchingWorksCorrectly() throws ServletException, IOException {
        when(servlet.getServletMapping()).thenReturn("/other/**");
        when(urlPathHelper.getPathWithinApplication(any())).thenReturn("/other");

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet).service(httpRequest, httpResponse);
    }

    @Test
    void exactMatchingWorksCorrectly() throws ServletException, IOException {
        when(servlet.getServletMapping()).thenReturn("/other/");
        when(urlPathHelper.getPathWithinApplication(any())).thenReturn("/other");

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet, times(0)).service(httpRequest, httpResponse);
        verify(httpResponse).sendError(HttpServletResponse.SC_NOT_FOUND);

        when(urlPathHelper.getPathWithinApplication(any())).thenReturn("/other/");
        smartSwitchingServlet.service(httpRequest, httpResponse);
        verify(dispatcherServlet).service(httpRequest, httpResponse);
    }

    @Test
    void switchingServletUsesSpringDispatcherWhenCollidingWithLessImportantServlets() throws ServletException, IOException {
        prepareStandardPaths();

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 20, anotherServlet)));
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet).service(httpRequest, httpResponse);
    }

    @Test
    void switchingServletUsesPluginServletWhenMoreImportantServletThrowNoHandlerException() throws ServletException, IOException {
        prepareStandardPaths();
        doThrow(NotHandledException.class).when(dispatcherServlet).service(any(), any());

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 20, anotherServlet)));
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet).service(httpRequest, httpResponse);
        verify(anotherServlet).init(any());
        verify(anotherServlet).service(httpRequest, httpResponse);
    }

    @Test
    void emptySwitchingServletReturns404WhenAllServletsReturnedNotFound() throws ServletException, IOException {
        prepareStandardPaths();
        doThrow(NotHandledException.class).when(dispatcherServlet).service(any(), any());
        doThrow(NotHandledException.class).when(anotherServlet).service(any(), any());

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 20, anotherServlet)));
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet).service(httpRequest, httpResponse);
        verify(anotherServlet).service(httpRequest, httpResponse);
        verify(httpResponse).sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    @Test
    void switchingServletMoreImportantServletWhenMoreImportantThanSpringServlet() throws ServletException, IOException {
        prepareStandardPaths();

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 1, anotherServlet)));
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet, times(0)).service(httpRequest, httpResponse);
        verify(anotherServlet).service(httpRequest, httpResponse);
    }

    @Test
    void switchingServletUsesSpringServletAsFirstWhenSettingActivated() throws ServletException, IOException {
        prepareStandardPaths();
        when(applicationSettings.isTrue(SystemSettingKeys.FORCE_SYSTEM_API)).thenReturn(true);

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 1, anotherServlet)));
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet).service(httpRequest, httpResponse);
        verify(anotherServlet, times(0)).service(httpRequest, httpResponse);
    }

    @Test
    void switchingServletUsesFirstAddedServletWhenAllHaveTheSameMappingAndOrder() throws ServletException, IOException {
        prepareStandardPaths();

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", anotherServlet)));
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet).service(httpRequest, httpResponse);
        verify(anotherServlet, times(0)).service(httpRequest, httpResponse);
    }

    @Test
    void switchingServletUsesMoreSpecificServletFirst() throws ServletException, IOException {
        prepareStandardPaths();

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("/test/", anotherServlet)));
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet, times(0)).service(httpRequest, httpResponse);
        verify(anotherServlet).service(httpRequest, httpResponse);
    }

    @Test
    void switchingServletUnplugsDestroysServletAndRemovesItFromProcessing() throws ServletException, IOException {
        prepareStandardPaths();

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("/test/", anotherServlet)));
        smartSwitchingServlet.unplugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("/test/", anotherServlet)));
        smartSwitchingServlet.service(httpRequest, httpResponse);

        verify(dispatcherServlet).service(httpRequest, httpResponse);
        verify(anotherServlet, times(0)).service(httpRequest, httpResponse);
        verify(anotherServlet).destroy();
    }

    @Test
    void switchingServletReturnsEmptyOptionalWhenSpringDispatcherIsCollidingWithLessImportantServlets() throws ServletException {
        prepareStandardPaths();

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 20, anotherServlet)));
        var resultVoter = smartSwitchingServlet.getServletsAuthorizationManager(httpRequest);

        assertThat(resultVoter).isEmpty();
    }

    @Test
    void switchingServletReturnsEmptyOptionalWhenNoVoterIsFound() throws ServletException {
        prepareStandardPaths();
        lenient().when(((AuthorizationManagerAware) anotherServlet).getAuthorizationManager(any())).thenReturn(Optional.empty());

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 20, anotherServlet)));
        var resultVoter = smartSwitchingServlet.getServletsAuthorizationManager(httpRequest);

        assertThat(resultVoter).isEmpty();
    }

    @Test
    void emptySwitchingServletReturnsLessImportantServletVoterWhenHigherServletAbstains() throws ServletException {
        prepareStandardPaths();
        lenient().when(((AuthorizationManagerAware) anotherServlet).getAuthorizationManager(any())).thenReturn(Optional.empty());
        lenient().when(((AuthorizationManagerAware) differentServlet).getAuthorizationManager(any())).thenReturn(Optional.of(authorizationManager));

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 2, anotherServlet)));
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 3, differentServlet)));
        var resultVoter = smartSwitchingServlet.getServletsAuthorizationManager(httpRequest);

        assertThat(resultVoter).contains(authorizationManager);
    }

    @Test
    void switchingServletReturnsVoterWhenServletIsMoreImportantThanSpringServlet() throws ServletException {
        prepareStandardPaths();
        lenient().when(((AuthorizationManagerAware) anotherServlet).getAuthorizationManager(any())).thenReturn(Optional.of(authorizationManager));

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 1, anotherServlet)));
        var resultVoter = smartSwitchingServlet.getServletsAuthorizationManager(httpRequest);

        assertThat(resultVoter).contains(authorizationManager);
    }

    @Test
    void switchingServletReturnsEmptyOptionalWhenSpringForcingSettingIsActivated() throws ServletException {
        prepareStandardPaths();
        when(applicationSettings.isTrue(SystemSettingKeys.FORCE_SYSTEM_API)).thenReturn(true);
        lenient().when(((AuthorizationManagerAware) anotherServlet).getAuthorizationManager(any())).thenReturn(Optional.of(authorizationManager));

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 1, anotherServlet)));
        var resultVoter = smartSwitchingServlet.getServletsAuthorizationManager(httpRequest);

        assertThat(resultVoter).isEmpty();
    }

    @Test
    void switchingServletReturnsVoterOfFirstAddedServletWhenAllHaveTheSameMappingAndOrder() throws ServletException {
        prepareStandardPaths();
        lenient().when(((AuthorizationManagerAware) anotherServlet).getAuthorizationManager(any())).thenReturn(Optional.of(authorizationManager));

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", anotherServlet)));
        var resultVoter = smartSwitchingServlet.getServletsAuthorizationManager(httpRequest);

        assertThat(resultVoter).isEmpty();
    }

    @Test
    void switchingServletReturnsVoterOfMoreSpecificServletFirst() throws ServletException {
        prepareStandardPaths();
        lenient().when(((AuthorizationManagerAware) anotherServlet).getAuthorizationManager(any())).thenReturn(Optional.of(authorizationManager));

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("/test/", anotherServlet)));
        var resultVoter = smartSwitchingServlet.getServletsAuthorizationManager(httpRequest);

        assertThat(resultVoter).contains(authorizationManager);
    }

    @Test
    void switchingServletUnplugsRemovesVoterFromProcessing() throws ServletException {
        prepareStandardPaths();
        lenient().when(((AuthorizationManagerAware) anotherServlet).getAuthorizationManager(any())).thenReturn(Optional.of(authorizationManager));

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("/test/", anotherServlet)));
        smartSwitchingServlet.unplugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("/test/", anotherServlet)));
        var resultVoter = smartSwitchingServlet.getServletsAuthorizationManager(httpRequest);

        assertThat(resultVoter).isEmpty();
    }

    @Test
    void switchingServletReturnsCsrfDisabledIfServletDefined() throws ServletException {
        prepareStandardPaths();
        lenient().when(((CsrfProtectionAware) anotherServlet).shouldDisableCsrfProtection(any())).thenReturn(Optional.of(true));

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 1, anotherServlet)));
        var csrfOverride = smartSwitchingServlet.shouldDisableCsrfProtection(httpRequest);

        assertThat(csrfOverride.get()).isTrue();
    }

    @Test
    void switchingServletReturnsCsrfEnabledIfNoServletDefinedStatus() throws ServletException {
        prepareStandardPaths();
        lenient().when(((CsrfProtectionAware) anotherServlet).shouldDisableCsrfProtection(any())).thenReturn(Optional.empty());

        smartSwitchingServlet.init(config);
        smartSwitchingServlet.plugDescriptor(plugin, new PluginServletMappingDescriptor(new ServletMapping("", 1, anotherServlet)));
        var csrfOverride = smartSwitchingServlet.shouldDisableCsrfProtection(httpRequest);

        assertThat(csrfOverride).isEmpty();
    }

    private void prepareStandardPaths() {
        when(servlet.getServletMapping()).thenReturn("");
        when(urlPathHelper.getPathWithinApplication(any())).thenReturn(TEST_PATH);
    }
}