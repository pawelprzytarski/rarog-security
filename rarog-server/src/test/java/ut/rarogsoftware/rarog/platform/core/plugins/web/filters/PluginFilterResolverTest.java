package ut.rarogsoftware.rarog.platform.core.plugins.web.filters;

import eu.rarogsoftware.rarog.platform.api.metrics.MetricsService;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.web.filters.PluginFilterMappingDescriptorBuilder;
import eu.rarogsoftware.rarog.platform.core.metrics.noop.NoopMetricsService;
import eu.rarogsoftware.rarog.platform.core.plugins.web.filters.PluginFilterChain;
import eu.rarogsoftware.rarog.platform.core.plugins.web.filters.PluginFilterResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import jakarta.servlet.DispatcherType;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.EnumSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
public class PluginFilterResolverTest {

    @Mock
    HttpServletRequest request;

    @Mock
    FilterChain originalChain;

    MetricsService metricsService = new NoopMetricsService();

    PluginFilterResolver resolver;

    @BeforeEach
    void setup() {
        resolver = new PluginFilterResolver(metricsService);
    }

    @Test
    void testNoMappingsGiveEmptyChain() throws IOException, ServletException {
        setupRequest("/some", "/path", DispatcherType.REQUEST);

        PluginFilterChain result = resolver.getFilterChainForRequest(request, originalChain);

        assertThat(result.getFilters()).isEmpty();
    }

    @Test
    void testMultipleDescriptors() throws IOException, ServletException {
        Filter filter_1 = getMockFilter();
        Filter filter_2 = getMockFilter();
        Filter filter_3 = getMockFilter();
        Filter filter_4 = getMockFilter();
        setupRequest("/some/extra", "/path", DispatcherType.REQUEST);
        resolver.plugDescriptor(getMockPlugin("test.plugin"), PluginFilterMappingDescriptorBuilder
            .newBuilder()
            .registerFilter(filter_1, EnumSet.of(DispatcherType.REQUEST), "/**")
            .registerFilter(filter_2, EnumSet.of(DispatcherType.REQUEST), "/non-match/**")
            .build()
        );

        resolver.plugDescriptor(getMockPlugin("test.plugin.2"), PluginFilterMappingDescriptorBuilder
            .newBuilder()
            .registerFilter(filter_3, EnumSet.of(DispatcherType.REQUEST), "/some/**")
            .registerFilter(filter_4, EnumSet.of(DispatcherType.REQUEST), "/some")
            .build()
        );

        PluginFilterChain result = resolver.getFilterChainForRequest(request, originalChain);

        assertThat(result.getFilters()).containsExactly(filter_1, filter_3);
    }

    @Test
    void testSingleDescriptor() throws IOException, ServletException {
        Filter filter_1 = getMockFilter();
        Filter filter_2 = getMockFilter();
        Filter filter_3 = getMockFilter();
        setupRequest("/some", "/path", DispatcherType.REQUEST);
        resolver.plugDescriptor(getMockPlugin("test.plugin"), PluginFilterMappingDescriptorBuilder
            .newBuilder()
            .registerFilter(filter_1, EnumSet.of(DispatcherType.REQUEST), "/**")
            .registerFilter(filter_2, EnumSet.of(DispatcherType.REQUEST), "/non-match/**")
            .registerFilter(filter_3, EnumSet.of(DispatcherType.REQUEST), "/some/**")
            .build()
        );

        PluginFilterChain result = resolver.getFilterChainForRequest(request, originalChain);

        assertThat(result.getFilters()).containsExactly(filter_1, filter_3);
    }

    @Test
    void testExactPathMatching() throws IOException, ServletException {
        Filter filter_1 = getMockFilter();
        Filter filter_2 = getMockFilter();
        Filter filter_3 = getMockFilter();
        Filter filter_4 = getMockFilter();
        setupRequest("/some", "/path", DispatcherType.REQUEST);
        resolver.plugDescriptor(getMockPlugin("test.plugin"), PluginFilterMappingDescriptorBuilder
            .newBuilder()
            .registerFilter(filter_1, EnumSet.of(DispatcherType.REQUEST), "/some/path")
            .registerFilter(filter_2, EnumSet.of(DispatcherType.REQUEST), "/some/path/")
            .registerFilter(filter_3, EnumSet.of(DispatcherType.REQUEST), "/some/path/**")
            .registerFilter(filter_4, EnumSet.of(DispatcherType.REQUEST), "/some/path", "/some/path/")
            .build()
        );

        PluginFilterChain result = resolver.getFilterChainForRequest(request, originalChain);

        assertThat(result.getFilters()).containsExactly(filter_1, filter_3, filter_4);
    }

    @Test
    void testDispatcherTypeIsMatched() throws IOException, ServletException {
        Filter filter_1 = getMockFilter();
        Filter filter_2 = getMockFilter();
        Filter filter_3 = getMockFilter();
        setupRequest("/some", "/path", DispatcherType.REQUEST);
        resolver.plugDescriptor(getMockPlugin("test.plugin"), PluginFilterMappingDescriptorBuilder
            .newBuilder()
            .registerFilter(filter_1, EnumSet.of(DispatcherType.REQUEST), "/**")
            .registerFilter(filter_2, EnumSet.of(DispatcherType.REQUEST), "/non-match/*")
            .registerFilter(filter_3, EnumSet.of(DispatcherType.ERROR), "/some/**")
            .build()
        );

        PluginFilterChain result = resolver.getFilterChainForRequest(request, originalChain);

        assertThat(result.getFilters()).containsExactly(filter_1);
    }


    @Test
    void testDuplicateDescriptorsThrow() {
        resolver.plugDescriptor(getMockPlugin("test.plugin"), PluginFilterMappingDescriptorBuilder.newBuilder()
            .build()
        );

        assertThrows(IllegalArgumentException.class, () -> resolver.plugDescriptor(getMockPlugin("test.plugin"), PluginFilterMappingDescriptorBuilder
            .newBuilder()
            .build()
        ));
    }

    @Test
    void testUnpluggingDescriptors() throws IOException, ServletException {
        Filter filter_1 = getMockFilter();
        Filter filter_2 = getMockFilter();
        Filter filter_3 = getMockFilter();
        Filter filter_4 = getMockFilter();
        setupRequest("/some/extra", "/path", DispatcherType.REQUEST);
        resolver.plugDescriptor(getMockPlugin("test.plugin"), PluginFilterMappingDescriptorBuilder
            .newBuilder()
            .registerFilter(filter_1, EnumSet.of(DispatcherType.REQUEST), "/**")
            .registerFilter(filter_2, EnumSet.of(DispatcherType.REQUEST), "/non-match/**")
            .build()
        );

        resolver.plugDescriptor(getMockPlugin("test.plugin.2"), PluginFilterMappingDescriptorBuilder
            .newBuilder()
            .registerFilter(filter_3, EnumSet.of(DispatcherType.REQUEST), "/some/**")
            .registerFilter(filter_4, EnumSet.of(DispatcherType.REQUEST), "/some")
            .build()
        );

        resolver.unplugDescriptor(getMockPlugin("test.plugin.2"), null);

        PluginFilterChain result = resolver.getFilterChainForRequest(request, originalChain);

        assertThat(result.getFilters()).containsExactly(filter_1);
    }


    private void setupRequest(String servletPath, String pathInfo, DispatcherType dispatcher) {
        lenient().when(request.getServletPath()).thenReturn(servletPath);
        lenient().when(request.getPathInfo()).thenReturn(pathInfo);
        lenient().when(request.getDispatcherType()).thenReturn(dispatcher);
    }

    private Plugin getMockPlugin(String key) {
        Plugin plugin = mock(Plugin.class);
        lenient().when(plugin.getKey()).thenReturn(key);
        return plugin;
    }

    private Filter getMockFilter() {
        return mock(Filter.class);
    }
}
