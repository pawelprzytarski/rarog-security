package ut.rarogsoftware.rarog.platform.mfa;

import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaService;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaStatus;
import eu.rarogsoftware.rarog.platform.api.security.mfa.MfaStore;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.security.mfa.MfaPageMatcher;
import eu.rarogsoftware.rarog.platform.security.mfa.MfaPageMatcherFactory;
import eu.rarogsoftware.rarog.platform.security.mfa.MultiFactorEnforcingFilter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.IOException;
import java.util.Optional;

import static eu.rarogsoftware.rarog.platform.api.security.mfa.MfaService.EXCLUDE_REQUEST_FROM_MFA_ATTRIBUTE;
import static eu.rarogsoftware.rarog.platform.security.mfa.MultiFactorEnforcingFilter.FILTER_APPLIED;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MultiFactorEnforcingFilterTest {
    @Mock
    MfaService mfaService;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    FilterChain chain;
    @Mock
    SecurityContext securityContext;
    @Mock
    StandardUser standardUser;
    @Mock
    Authentication authentication;
    @Mock
    UserDetails unknownUser;
    @Mock
    MfaStore mfaStore;
    @Mock
    MfaPageMatcherFactory matcherFactory;
    @Mock
    MfaPageMatcher pageMatcher;

    @InjectMocks
    MultiFactorEnforcingFilter filter;

    @BeforeEach
    void setUp() {
        when(request.getAttribute(anyString())).thenReturn(null);
        lenient().when(mfaStore.getStatus(request)).thenReturn(Optional.empty());
        SecurityContextHolder.setContext(securityContext);
    }

    @AfterEach
    void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    void testFilterAlreadyApplied() throws ServletException, IOException {
        when(request.getAttribute(FILTER_APPLIED)).thenReturn(true);

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
    }

    @Test
    void testFilterShouldBeSkipped() throws ServletException, IOException {
        when(request.getAttribute(EXCLUDE_REQUEST_FROM_MFA_ATTRIBUTE)).thenReturn(true);

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
    }

    @Test
    void testUserNotLoggedIn() throws ServletException, IOException {
        when(securityContext.getAuthentication()).thenReturn(null);

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
    }

    @Test
    void testUserIsIncorrectType() {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getPrincipal()).thenReturn(unknownUser);

        assertThrows(IllegalStateException.class, () -> filter.doFilter(request, response, chain));
    }

    @Test
    void testNotAuthorized() throws ServletException, IOException {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.isAuthenticated()).thenReturn(false);

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
    }

    @Test
    void testMfaDisabledStoredInSession() throws ServletException, IOException {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getPrincipal()).thenReturn(standardUser);
        when(mfaStore.getStatus(request)).thenReturn(Optional.of(MfaStatus.MFA_DISABLED));

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
        verifyNoInteractions(mfaService);
    }

    @Test
    void testMfaDisabledInService() throws ServletException, IOException {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getPrincipal()).thenReturn(standardUser);
        when(mfaService.isMfaEnabled()).thenReturn(false);

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
    }

    @Test
    void testMfaDisabledForUser() throws ServletException, IOException {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getPrincipal()).thenReturn(standardUser);
        when(mfaService.isMfaEnabled()).thenReturn(true);
        when(mfaService.isMfaEnabledForUser(standardUser)).thenReturn(false);

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
    }

    @Test
    void testMfaEnabledConfigured() throws ServletException, IOException {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getPrincipal()).thenReturn(standardUser);
        when(mfaService.isMfaEnabled()).thenReturn(true);
        when(mfaService.isMfaEnabledForUser(standardUser)).thenReturn(true);
        when(mfaService.isMfaConfiguredForUser(standardUser)).thenReturn(true);
        when(matcherFactory.createPageMatcher(false)).thenReturn(pageMatcher);
        when(pageMatcher.matches(request)).thenReturn(false);
        when(request.getContextPath()).thenReturn("/server");
        when(request.getServletPath()).thenReturn("/servlet");

        filter.doFilter(request, response, chain);

        verifyNoInteractions(chain);
        verify(response).sendRedirect("/server/login/mfa/?redirect=/servlet");
    }

    @Test
    void testMfaEnabledConfiguredWhenAccessingMfaPage() throws ServletException, IOException {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getPrincipal()).thenReturn(standardUser);
        when(mfaService.isMfaEnabled()).thenReturn(true);
        when(mfaService.isMfaEnabledForUser(standardUser)).thenReturn(true);
        when(mfaService.isMfaConfiguredForUser(standardUser)).thenReturn(true);
        when(matcherFactory.createPageMatcher(false)).thenReturn(pageMatcher);
        when(pageMatcher.matches(request)).thenReturn(true);

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
        verifyNoInteractions(response);
    }

    @Test
    void testMfaEnabledNotConfigured() throws ServletException, IOException {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getPrincipal()).thenReturn(standardUser);
        when(mfaService.isMfaEnabled()).thenReturn(true);
        when(mfaService.isMfaEnabledForUser(standardUser)).thenReturn(true);
        when(mfaService.isMfaConfiguredForUser(standardUser)).thenReturn(false);
        when(matcherFactory.createPageMatcher(true)).thenReturn(pageMatcher);
        when(pageMatcher.matches(request)).thenReturn(false);
        when(request.getContextPath()).thenReturn("/server");
        when(request.getServletPath()).thenReturn("/servlet");

        filter.doFilter(request, response, chain);

        verifyNoInteractions(chain);
        verify(response).sendRedirect("/server/login/mfa/configure?redirect=/servlet");
    }

    @Test
    void testMfaEnabledNotConfiguredAccessToConfigurationPage() throws ServletException, IOException {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getPrincipal()).thenReturn(standardUser);
        when(mfaService.isMfaEnabled()).thenReturn(true);
        when(mfaService.isMfaEnabledForUser(standardUser)).thenReturn(true);
        when(mfaService.isMfaConfiguredForUser(standardUser)).thenReturn(false);
        when(matcherFactory.createPageMatcher(true)).thenReturn(pageMatcher);
        when(pageMatcher.matches(request)).thenReturn(true);

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
        verifyNoInteractions(response);
    }

    @Test
    void testAccessToPagesSpecifiedViaAntMatcherPattern() throws ServletException, IOException {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getPrincipal()).thenReturn(standardUser);
        when(mfaService.isMfaEnabled()).thenReturn(true);
        when(mfaService.isMfaEnabledForUser(standardUser)).thenReturn(true);
        when(mfaService.isMfaConfiguredForUser(standardUser)).thenReturn(false);
        when(matcherFactory.createPageMatcher(true)).thenReturn(pageMatcher);
        when(pageMatcher.matches(request)).thenReturn(false);
        when(request.getPathInfo()).thenReturn("/path1");
        when(request.getServletPath()).thenReturn("/servlet");

        filter.addExcludedPathPatterns("/servlet/path1");
        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
        verifyNoInteractions(response);
    }

    @Test
    void testRedirectFromPagesNotSpecifiedViaAntMatcherPattern() throws ServletException, IOException {
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getPrincipal()).thenReturn(standardUser);
        when(mfaService.isMfaEnabled()).thenReturn(true);
        when(mfaService.isMfaEnabledForUser(standardUser)).thenReturn(true);
        when(mfaService.isMfaConfiguredForUser(standardUser)).thenReturn(false);
        when(matcherFactory.createPageMatcher(true)).thenReturn(pageMatcher);
        when(pageMatcher.matches(request)).thenReturn(false);
        when(request.getPathInfo()).thenReturn("/path1");
        when(request.getServletPath()).thenReturn("/servlet");
        when(request.getContextPath()).thenReturn("/server");

        filter.addExcludedPathPatterns("/servlet2/*");
        filter.doFilter(request, response, chain);

        verify(response).sendRedirect("/server/login/mfa/configure?redirect=/servlet/path1");
    }
}