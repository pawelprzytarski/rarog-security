package ut.rarogsoftware.rarog.platform.core.i18n;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.rarog.platform.api.i18n.I18NextResourceDescriptor;
import eu.rarogsoftware.rarog.platform.api.i18n.I18NextTransformerDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.ResourceHoldingPlugin;
import eu.rarogsoftware.rarog.platform.core.i18n.DefaultI18NextService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.util.InMemoryResource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DefaultI18NextServiceTest {
    private static final String PLUGIN_TRANSLATION = """
            {
                "test": {
                    "key1": {
                        "value" : "someValue"
                    },
                    "key2": "OtherValue"
                },
                "overriding": {
                    "key": "THIS"                
                }
            }""";
    private static final String OTHER_PLUGIN_TRANSLATION = """
            {
                "test": {
                    "key2": {
                        "value" : "someValue2"
                    },
                    "key1": "OtherValue2"
                },
                "different": {
                    "key": "DifferentValue"                
                },
                "overriding": {
                    "key": "OTHER"                
                }
            }""";
    private static final String COMMON_OVERRIDING_TRANSLATION = """
            {
                "navigation": {
                    "go": {
                        "back" : "OVERRIDE"
                    }
                }
            }""";
    private static final String MERGED_TRANSLATIONS = """
            {"test":{"key1":{"":"OtherValue2","value":"someValue"},"key2":{"":"OtherValue","value":"someValue2"}},"overriding":{"key":"THIS"},"different":{"key":"DifferentValue"}}""";
    private static final String ORDER_MERGED_TRANSLATIONS = """
            {"test":{"key1":{"":"OtherValue2","value":"someValue"},"key2":{"":"OtherValue","value":"someValue2"}},"overriding":{"key":"OTHER"},"different":{"key":"DifferentValue"}}""";
    private static final String OTHER_PLUGIN_KEY = "TEST_PLUGIN_2";
    private static final String TEST_PLUGIN_KEY = "TEST_PLUGIN";
    private static final String PARENT_DIRECTORY = "i18n";
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final String BASE_NAME = "messages";
    private static final String OTHER_LOCATION = "other";
    private static final String TEST_LOCATION = "test";
    private static final String TEST_RESOURCES_SEARCH = "/test/en/messages_*.json";
    private static final String OTHER_LOCATION_SEARCH = "/other/en/messages_*.json";
    private static final String TEST_RESOURCE_LOCATION = "/test/en/messages_plugin.json";
    private static final String OTHER_RESOURCE_LOCATION = "/other/en/messages_plugin.json";
    private static final String TEST_TRANSLATION_NAMESPACE = "plugin";
    private static final String TEST_TRANSLATION_FILENAME = "messages_plugin.json";
    private static final TypeReference<Map<String, Object>> MAP_TYPE = new TypeReference<>() {
    };
    @Mock
    private ResourceHoldingPlugin plugin;
    @Mock
    private ResourceHoldingPlugin otherPlugin;
    private DefaultI18NextService i18NextService;

    @BeforeEach
    void setUp() {
        i18NextService = new DefaultI18NextService();
    }

    @Test
    void serviceLoadsAppTranslationsForCorrectLocale() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        var bundle = i18NextService.getResourceBundle(BASE_NAME, Locale.ENGLISH, getClass().getClassLoader(), false);

        assertThat(bundle.handleKeySet().size()).isGreaterThan(1);
        assertThat(bundle.getString("common:navigation.go.back")).isEqualTo("Go back");
    }

    @Test
    void serviceThrowsExceptionForIncorrectLocale() {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        assertThrows(FileNotFoundException.class, () -> i18NextService.getResourceBundle(BASE_NAME, Locale.KOREAN, getClass().getClassLoader(), false));
    }

    @Test
    void serviceLoadsAppTranslationsFromPlugin() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.findResources(TEST_RESOURCES_SEARCH)).thenReturn(Collections.singleton(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME)));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 10));
        var bundle = i18NextService.getResourceBundle(BASE_NAME, Locale.ENGLISH, getClass().getClassLoader(), false);

        assertThat(bundle.getString("plugin:test.key1.value")).isEqualTo("someValue");
    }

    @Test
    void serviceLoadsAppTranslationsFromPluginAndReturnsKeyWithoutNamespace() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.findResources(TEST_RESOURCES_SEARCH)).thenReturn(Collections.singleton(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME)));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 10));
        var bundle = i18NextService.getResourceBundle(BASE_NAME, Locale.ENGLISH, getClass().getClassLoader(), false);

        assertThat(bundle.getString("test.key1.value")).isEqualTo("someValue");
    }

    @Test
    void serviceLoadsAppTranslationsFromPluginButThrowsExceptionForUnknownKeyWithNamespace() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.findResources(TEST_RESOURCES_SEARCH)).thenReturn(Collections.singleton(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME)));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 10));
        var bundle = i18NextService.getResourceBundle(BASE_NAME, Locale.ENGLISH, getClass().getClassLoader(), false);

        assertThrows(MissingResourceException.class, () -> bundle.getString("plugin:test.key1.unknown"));
    }

    @Test
    void serviceLoadsAppTranslationsFromPluginButThrowsExceptionForUnknownNamespace() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.findResources(TEST_RESOURCES_SEARCH)).thenReturn(Collections.singleton(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME)));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 10));
        var bundle = i18NextService.getResourceBundle(BASE_NAME, Locale.ENGLISH, getClass().getClassLoader(), false);

        assertThrows(MissingResourceException.class, () -> bundle.getString("unknown:test.key1.value"));
    }

    @Test
    void serviceMergeJsonFromPlugins() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.findResources(TEST_RESOURCES_SEARCH)).thenReturn(Collections.singleton(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME)));
        when(otherPlugin.getKey()).thenReturn(OTHER_PLUGIN_KEY);
        when(otherPlugin.findResources(OTHER_LOCATION_SEARCH)).thenReturn(Collections.singleton(new MemoryResourceWithFileName(DefaultI18NextServiceTest.OTHER_PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME)));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 10));
        i18NextService.plugDescriptor(otherPlugin, new I18NextResourceDescriptor(OTHER_LOCATION, 20));
        var bundle = i18NextService.getResourceBundle(BASE_NAME, Locale.ENGLISH, getClass().getClassLoader(), false);

        assertThat(bundle.getString("plugin:test.key1.value")).isEqualTo("someValue");
        assertThat(bundle.getString("plugin:test.key2.value")).isEqualTo("someValue2");
        assertThat(bundle.getString("plugin:test.key1")).isEqualTo("OtherValue2");
        assertThat(bundle.getString("plugin:test.key2")).isEqualTo("OtherValue");
        assertThat(bundle.getString("plugin:overriding.key")).isEqualTo("THIS");
    }

    @Test
    void serviceMergeTakesOrderIntoAccount() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.findResources(TEST_RESOURCES_SEARCH)).thenReturn(Collections.singleton(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME)));
        when(otherPlugin.getKey()).thenReturn(OTHER_PLUGIN_KEY);
        when(otherPlugin.findResources(OTHER_LOCATION_SEARCH)).thenReturn(Collections.singleton(new MemoryResourceWithFileName(DefaultI18NextServiceTest.OTHER_PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME)));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 20));
        i18NextService.plugDescriptor(otherPlugin, new I18NextResourceDescriptor(OTHER_LOCATION, 10));
        var bundle = i18NextService.getResourceBundle(BASE_NAME, Locale.ENGLISH, getClass().getClassLoader(), false);

        assertThat(bundle.getString("plugin:test.key1.value")).isEqualTo("someValue");
        assertThat(bundle.getString("plugin:test.key2.value")).isEqualTo("someValue2");
        assertThat(bundle.getString("plugin:test.key1")).isEqualTo("OtherValue2");
        assertThat(bundle.getString("plugin:test.key2")).isEqualTo("OtherValue");
        assertThat(bundle.getString("plugin:overriding.key")).isEqualTo("OTHER");
    }

    @Test
    void serviceMergeOverridesAppTranslations() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.findResources(TEST_RESOURCES_SEARCH)).thenReturn(Collections.singleton(new MemoryResourceWithFileName(DefaultI18NextServiceTest.COMMON_OVERRIDING_TRANSLATION, "messages_common.json")));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 20));
        var bundle = i18NextService.getResourceBundle(BASE_NAME, Locale.ENGLISH, getClass().getClassLoader(), false);

        assertThat(bundle.getString("common:navigation.go.back")).isEqualTo("OVERRIDE");
    }

    @Test
    void serviceLoadsAppTranslationResource() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        var inputStream = i18NextService.getI18NextResource(Locale.ENGLISH, "common");
        var response = new String(inputStream.readAllBytes());

        assertThat(response).contains("\"back\": \"Go back\"");
    }

    @Test
    void serviceReturnNullForNotExistingLocale() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        var inputStream = i18NextService.getI18NextResource(Locale.KOREAN, "common");

        assertThat(inputStream).isNull();
    }

    @Test
    void serviceLoadsResourceFromPlugin() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.getResource(TEST_RESOURCE_LOCATION)).thenReturn(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 10));
        var inputStream = i18NextService.getI18NextResource(Locale.ENGLISH, TEST_TRANSLATION_NAMESPACE);
        var response = new String(inputStream.readAllBytes());

        assertThat(response).isEqualTo(DefaultI18NextServiceTest.PLUGIN_TRANSLATION);
    }

    @Test
    void serviceMergeResourcesFromPlugins() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.getResource(TEST_RESOURCE_LOCATION)).thenReturn(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME));
        when(otherPlugin.getKey()).thenReturn(OTHER_PLUGIN_KEY);
        when(otherPlugin.getResource(OTHER_RESOURCE_LOCATION)).thenReturn(new MemoryResourceWithFileName(DefaultI18NextServiceTest.OTHER_PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 10));
        i18NextService.plugDescriptor(otherPlugin, new I18NextResourceDescriptor(OTHER_LOCATION, 20));
        var inputStream = i18NextService.getI18NextResource(Locale.ENGLISH, TEST_TRANSLATION_NAMESPACE);

        var responseJson = objectMapper.readValue(inputStream, MAP_TYPE);
        var expectedJson = objectMapper.readValue(DefaultI18NextServiceTest.MERGED_TRANSLATIONS, MAP_TYPE);

        Assertions.assertThat(responseJson).isEqualTo(expectedJson);
    }

    @Test
    void serviceMergeResourcesFromPluginsWithKeepingOrder() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.getResource(TEST_RESOURCE_LOCATION)).thenReturn(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME));
        when(otherPlugin.getKey()).thenReturn(OTHER_PLUGIN_KEY);
        when(otherPlugin.getResource(OTHER_RESOURCE_LOCATION)).thenReturn(new MemoryResourceWithFileName(DefaultI18NextServiceTest.OTHER_PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 20));
        i18NextService.plugDescriptor(otherPlugin, new I18NextResourceDescriptor(OTHER_LOCATION, 10));
        var inputStream = i18NextService.getI18NextResource(Locale.ENGLISH, TEST_TRANSLATION_NAMESPACE);

        var responseJson = objectMapper.readValue(inputStream, MAP_TYPE);
        var expectedJson = objectMapper.readValue(DefaultI18NextServiceTest.ORDER_MERGED_TRANSLATIONS, MAP_TYPE);

        Assertions.assertThat(responseJson).isEqualTo(expectedJson);
    }

    @Test
    void serviceApplyTransformers() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.getResource(TEST_RESOURCE_LOCATION)).thenReturn(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 20));
        i18NextService.plugDescriptor(plugin, new I18NextTransformerDescriptor((namespace, locale, json) -> json + "_Transformed", 10));
        var inputStream = i18NextService.getI18NextResource(Locale.ENGLISH, TEST_TRANSLATION_NAMESPACE);
        var response = new String(inputStream.readAllBytes());

        Assertions.assertThat(response).isEqualTo(DefaultI18NextServiceTest.PLUGIN_TRANSLATION + "_Transformed");
    }

    @Test
    void serviceApplyTransformersWithCorrectParams() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.getResource(TEST_RESOURCE_LOCATION)).thenReturn(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 20));
        i18NextService.plugDescriptor(plugin, new I18NextTransformerDescriptor((namespace, locale, json) -> json + "_Transformed_"+namespace+"+"+locale, 10));
        var inputStream = i18NextService.getI18NextResource(Locale.ENGLISH, TEST_TRANSLATION_NAMESPACE);
        var response = new String(inputStream.readAllBytes());

        Assertions.assertThat(response).isEqualTo(DefaultI18NextServiceTest.PLUGIN_TRANSLATION + "_Transformed_plugin+en");
    }

    @Test
    void serviceApplyMultipleTransformers() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.getResource(TEST_RESOURCE_LOCATION)).thenReturn(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME));
        when(otherPlugin.getKey()).thenReturn(OTHER_PLUGIN_KEY);

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 20));
        i18NextService.plugDescriptor(plugin, new I18NextTransformerDescriptor((namespace, locale, json) -> json + "_Transformed1", 10));
        i18NextService.plugDescriptor(otherPlugin, new I18NextTransformerDescriptor((namespace, locale, json) -> json + "_Transformed2", 20));
        var inputStream = i18NextService.getI18NextResource(Locale.ENGLISH, TEST_TRANSLATION_NAMESPACE);
        var response = new String(inputStream.readAllBytes());

        Assertions.assertThat(response).isEqualTo(DefaultI18NextServiceTest.PLUGIN_TRANSLATION + "_Transformed1_Transformed2");
    }

    @Test
    void serviceApplyMultipleTransformersWithCorrectOrder() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.getResource(TEST_RESOURCE_LOCATION)).thenReturn(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME));
        when(otherPlugin.getKey()).thenReturn(OTHER_PLUGIN_KEY);

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 20));
        i18NextService.plugDescriptor(plugin, new I18NextTransformerDescriptor((namespace, locale, json) -> json + "_Transformed1", 20));
        i18NextService.plugDescriptor(otherPlugin, new I18NextTransformerDescriptor((namespace, locale, json) -> json + "_Transformed2", 10));
        var inputStream = i18NextService.getI18NextResource(Locale.ENGLISH, TEST_TRANSLATION_NAMESPACE);
        var response = new String(inputStream.readAllBytes());

        Assertions.assertThat(response).isEqualTo(DefaultI18NextServiceTest.PLUGIN_TRANSLATION + "_Transformed2_Transformed1");
    }

    @Test
    void serviceApplyMultipleTransformersOnMergedTranslations() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.getResource(TEST_RESOURCE_LOCATION)).thenReturn(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME));
        when(otherPlugin.getKey()).thenReturn(OTHER_PLUGIN_KEY);

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 20));
        i18NextService.plugDescriptor(plugin, new I18NextTransformerDescriptor((namespace, locale, json) -> json + "_Transformed1", 20));
        i18NextService.plugDescriptor(otherPlugin, new I18NextTransformerDescriptor((namespace, locale, json) -> json + "_Transformed2", 10));
        var inputStream = i18NextService.getI18NextResource(Locale.ENGLISH, TEST_TRANSLATION_NAMESPACE);
        var response = new String(inputStream.readAllBytes());

        Assertions.assertThat(response).endsWith("_Transformed2_Transformed1");
    }

    @Test
    void serviceApplyTransformersToBundle() throws IOException {
        i18NextService.setParentDirectory(PARENT_DIRECTORY);
        when(plugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(plugin.findResources(TEST_RESOURCES_SEARCH)).thenReturn(Collections.singleton(new MemoryResourceWithFileName(DefaultI18NextServiceTest.PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME)));
        when(otherPlugin.getKey()).thenReturn(OTHER_PLUGIN_KEY);
        when(otherPlugin.findResources(OTHER_LOCATION_SEARCH)).thenReturn(Collections.singleton(new MemoryResourceWithFileName(DefaultI18NextServiceTest.OTHER_PLUGIN_TRANSLATION, TEST_TRANSLATION_FILENAME)));

        i18NextService.plugDescriptor(plugin, new I18NextResourceDescriptor(TEST_LOCATION, 20));
        i18NextService.plugDescriptor(otherPlugin, new I18NextResourceDescriptor(OTHER_LOCATION, 10));
        i18NextService.plugDescriptor(plugin, new I18NextTransformerDescriptor((namespace, locale, json) -> json.replaceAll("OtherValue", "ChangedValue_" + namespace + "_" + locale), 10));
        var bundle = i18NextService.getResourceBundle(BASE_NAME, Locale.ENGLISH, getClass().getClassLoader(), false);

        assertThat(bundle.getString("plugin:test.key1")).isEqualTo("ChangedValue_plugin_en2");
        assertThat(bundle.getString("plugin:test.key2")).isEqualTo("ChangedValue_plugin_en");
    }

    private static class MemoryResourceWithFileName extends InMemoryResource {
        private final String filename;

        public MemoryResourceWithFileName(String resourceContent, String filename) {
            super(resourceContent);
            this.filename = filename;
        }

        @Override
        public String getFilename() {
            return filename;
        }
    }
}