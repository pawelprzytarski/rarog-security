package ut.rarogsoftware.rarog.platform.core.plugins.events;

import eu.rarogsoftware.rarog.platform.api.metrics.MetricsService;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventDeliveryFailedException;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventHandler;
import eu.rarogsoftware.rarog.platform.api.plugins.events.EventListener;
import eu.rarogsoftware.rarog.platform.api.task.TaskManager;
import eu.rarogsoftware.rarog.platform.core.metrics.noop.NoopMetricsService;
import eu.rarogsoftware.rarog.platform.core.plugins.events.DefaultEventService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class DefaultEventServiceTest {

    @Mock
    TaskManager taskManager;

    MetricsService metricsService = new NoopMetricsService();

    DefaultEventService eventService;

    @BeforeEach
    void setup() {
        eventService = new DefaultEventService(taskManager, metricsService);
    }

    @Test
    void testRegisteredMultipleHandlerListenerReceivesEvents() {
        var listener = new TwoHandlerListener();

        eventService.registerListener(listener);
        eventService.publishSyncEvent(new TestEvent("Hi there"));
        eventService.publishSyncEvent("just a string wwow!");

        assertThat(listener.getReceivedTestEvents()).hasSize(1);
        assertThat(listener.getReceivedObjectEvents()).hasSize(2);
    }

    @Test
    void testInheritingListenerReceivesEvents() {
        var listener = new InheritingTestEventListener();

        eventService.registerListener(listener);
        eventService.publishSyncEvent(new TestEvent("Hi there"));
        eventService.publishSyncEvent(new AnotherTestEvent("just a string wwow!"));

        assertThat(listener.getReceivedTestEvents()).hasSize(1);
        assertThat(listener.getReceivedAnotherTestEvents()).hasSize(1);
    }

    @Test
    void testNotMatchingEventsAreNotPassed() {
        var listener = new TestEventListener();

        eventService.registerListener(listener);
        eventService.publishSyncEvent(new AnotherTestEvent("Hi there"));

        assertThat(listener.getReceivedTestEvents()).isEmpty();
    }

    @Test
    void testRegisteredListenerCanBeUnregistered() {
        var listener = new TwoHandlerListener();

        eventService.registerListener(listener);
        eventService.publishSyncEvent(new TestEvent("Hi there"));
        eventService.unregisterListener(listener);
        eventService.publishSyncEvent(new TestEvent("Hi there"));

        assertThat(listener.getReceivedTestEvents()).hasSize(1);
    }

    @Test
    void testMultipleListeners() {
        var testListener = new TestEventListener();
        var anotherTestListener = new AnotherTestEventListener();
        var multiListener = new TwoHandlerListener();

        eventService.registerListener(testListener);
        eventService.registerListener(anotherTestListener);
        eventService.registerListener(multiListener);
        eventService.publishSyncEvent(new AnotherTestEvent("Henlo sir!"));
        eventService.unregisterListener(multiListener);
        eventService.publishSyncEvent(new TestEvent("Henlo lady!"));


        assertThat(testListener.getReceivedTestEvents()).hasSize(1);
        assertThat(anotherTestListener.getReceivedTestEvents()).hasSize(1);
        assertThat(multiListener.getReceivedObjectEvents()).hasSize(1);
        assertThat(multiListener.getReceivedTestEvents()).isEmpty();
    }

    @Test
    void testCrashingListenerDoesntDestroyWorld() {
        var testListener = new TestEventListener();
        var theBastardListener = new NastyThrowingListener();
        var testListenerToo = new TestEventListener();

        eventService.registerListener(testListener);
        eventService.registerListener(theBastardListener);
        eventService.registerListener(testListenerToo);
        eventService.publishSyncEvent(new TestEvent("You shall not pass you nasty NPE"));

        assertThat(testListener.getReceivedTestEvents()).hasSize(1);
        assertThat(testListenerToo.getReceivedTestEvents()).hasSize(1);
    }

    @Test
    void testThrowingListenerExceptionIsPropagatedWhenWanted() {
        var theBastardListener = new NastyThrowingListener();
        eventService.registerListener(theBastardListener);

        assertThatThrownBy(() -> eventService.publishSyncEventWithExceptions(new TestEvent("You shall not pass you nasty NPE")))
                .isInstanceOf(EventDeliveryFailedException.class);
    }

    @Test
    void testPublishingEventWithoutRegisteredListenersDoesntFail() {
        assertThatCode(() -> eventService.publishSyncEvent(new TestEvent("Hi there")))
            .doesNotThrowAnyException();
    }

    @Test
    void testRegisteringListenerWithoutHandlersThrows() {
        var listener = new EmptyListener();

        assertThatThrownBy(() -> eventService.registerListener(listener)).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void testRegisteringNullListenerThrows() {
        assertThatThrownBy(() -> eventService.registerListener(null)).isInstanceOf(NullPointerException.class);
    }

    @Test
    void testPublishingNullEventThrows() {
        assertThatThrownBy(() -> eventService.publishSyncEvent(null)).isInstanceOf(NullPointerException.class);
    }

    @Test
    void testRegisteredMultipleHandlerListenerReceivesAsyncEvents() {
        var listener = new TwoHandlerListener();

        eventService.registerListener(listener);
        eventService.publishAsyncEvent(new TestEvent("Hi there"));
        eventService.publishAsyncEvent("just a string wwow!");

        verify(taskManager, times(2)).runTask(any(Runnable.class));
    }

    @Test
    void testRegisteredMultipleHandlerListenerReceivesParallelAsyncEvents() {
        var listener = new TwoHandlerListener();

        eventService.registerListener(listener);
        eventService.publishParallelAsyncEvent(new TestEvent("Hi there"));
        eventService.publishParallelAsyncEvent("just a string wwow!");

        verify(taskManager, times(3)).runTask(any(Runnable.class));
    }

    @Test
    void testEventsSubclassesAreMatchedCorrectly() {
        var listener = new InheritingEventsEventListener();

        eventService.registerListener(listener);

        eventService.publishSyncEvent(new BaseclassEvent());
        eventService.publishSyncEvent(new SubclassEvent());
        eventService.publishSyncEvent(new SubSubclassEvent());
        eventService.publishSyncEvent(new SubclassEventToo());

        assertThat(listener.getReceivedBaseclassEvents()).hasSize(4);
        assertThat(listener.getReceivedSubclassEvents()).hasSize(2);
    }

    @Test
    void testInterfaceImplementationsAreMatchedCorrectly() {
        var listener = new InheritingEventsEventListener();

        eventService.registerListener(listener);

        eventService.publishSyncEvent(new EventInterfaceImpl());
        eventService.publishSyncEvent(new EventInterfaceImplToo());

        assertThat(listener.getReceivedInterfaceEvents()).hasSize(2);
        assertThat(listener.getReceivedImplementationEvents()).hasSize(1);
    }


    public static class TestEventListener implements EventListener {
        private final List<TestEvent> receivedTestEvents = new LinkedList<>();

        @EventHandler
        public void eventHandler(TestEvent event) {
            receivedTestEvents.add(event);
        }

        public List<TestEvent> getReceivedTestEvents() {
            return receivedTestEvents;
        }
    }

    public static class NastyThrowingListener implements EventListener {
        @EventHandler
        public void eventHandler(TestEvent event) {
            throw new NullPointerException(
                "HEHE I TRICKED YOU, NO NULLS TO BE FOUND HERE. Wait, what are you doing?! Why the world is rotating? Blood is splashing around... Have you just stabbed me? Oh... On the other hand, the world will be a better place without me. Good bye, my cruel adversary...");
        }
    }

    public static class AnotherTestEventListener implements EventListener {
        private final List<AnotherTestEvent> receivedTestEvents = new LinkedList<>();

        @EventHandler
        public void eventHandler(AnotherTestEvent event) {
            receivedTestEvents.add(event);
        }

        public List<AnotherTestEvent> getReceivedTestEvents() {
            return receivedTestEvents;
        }
    }


    public static class InheritingTestEventListener extends TestEventListener implements EventListener {
        private final List<AnotherTestEvent> receivedTestEvents = new LinkedList<>();

        @EventHandler
        public void eventHandler(AnotherTestEvent event) {
            receivedTestEvents.add(event);
        }

        public List<AnotherTestEvent> getReceivedAnotherTestEvents() {
            return receivedTestEvents;
        }
    }

    public static class TwoHandlerListener implements EventListener {
        private final List<Object> receivedObjectEvents = new LinkedList<>();

        private final List<TestEvent> receivedTestEvents = new LinkedList<>();

        @EventHandler
        public void objectHandler(Object event) {
            receivedObjectEvents.add(event);
        }

        @EventHandler
        public void eventHandler(TestEvent event) {
            receivedTestEvents.add(event);
        }

        public List<Object> getReceivedObjectEvents() {
            return receivedObjectEvents;
        }

        public List<TestEvent> getReceivedTestEvents() {
            return receivedTestEvents;
        }
    }

    public static class EmptyListener implements EventListener {
        private final List<Object> receivedObjectEvents = new LinkedList<>();

        private final List<TestEvent> receivedTestEvents = new LinkedList<>();

        public List<Object> getReceivedObjectEvents() {
            return receivedObjectEvents;
        }

        public List<TestEvent> getReceivedTestEvents() {
            return receivedTestEvents;
        }

    }

    public static class InheritingEventsEventListener implements EventListener {
        private final List<BaseclassEvent> receivedBaseclassEvents = new LinkedList<>();
        private final List<SubclassEvent> receivedSubclassEvents = new LinkedList<>();
        private final List<EventInterface> receivedInterfaceEvents = new LinkedList<>();
        private final List<EventInterfaceImpl> receivedImplementationEvents = new LinkedList<>();

        @EventHandler
        public void baseEventHandler(BaseclassEvent event) {
            receivedBaseclassEvents.add(event);
        }

        @EventHandler
        public void subclassEventHandler(SubclassEvent event) {
            receivedSubclassEvents.add(event);
        }

        @EventHandler
        public void interfaceEventHandler(EventInterface event) {
            receivedInterfaceEvents.add(event);
        }

        @EventHandler
        public void implementationEventsHandler(EventInterfaceImpl event) {
            receivedImplementationEvents.add(event);
        }

        public List<BaseclassEvent> getReceivedBaseclassEvents() {
            return receivedBaseclassEvents;
        }

        public List<SubclassEvent> getReceivedSubclassEvents() {
            return receivedSubclassEvents;
        }

        public List<EventInterface> getReceivedInterfaceEvents() {
            return receivedInterfaceEvents;
        }

        public List<EventInterfaceImpl> getReceivedImplementationEvents() {
            return receivedImplementationEvents;
        }
    }

    private record TestEvent(String message) {
    }

    private record AnotherTestEvent(String message) {
    }

    private static class BaseclassEvent {
    }

    private static class SubclassEvent extends BaseclassEvent {
    }

    private static class SubSubclassEvent extends SubclassEvent {
    }

    private static class SubclassEventToo extends BaseclassEvent {
    }

    private interface EventInterface {
    }

    private static class EventInterfaceImpl implements EventInterface {
    }

    private static class EventInterfaceImplToo implements EventInterface {
    }
}