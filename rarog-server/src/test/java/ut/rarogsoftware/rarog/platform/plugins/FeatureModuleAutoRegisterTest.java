package ut.rarogsoftware.rarog.platform.plugins;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.core.plugins.PluggableFeatureRegistry;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.AutoRegisterFeatureModule;
import eu.rarogsoftware.rarog.platform.core.plugins.spring.FeatureModuleAutoRegister;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FeatureModuleAutoRegisterTest {
    @Mock
    PluggableFeatureRegistry featureRegistry;
    @Mock
    ApplicationContext context;
    @InjectMocks
    private FeatureModuleAutoRegister featureModuleAutoRegister;

    @Test
    void failIfContextNotPassed() {
        assertThrows(IllegalStateException.class, () -> featureModuleAutoRegister.afterPropertiesSet());
        verify(featureRegistry, times(0)).registerFeatureModule(any(), any());
    }

    @Test
    void doNothingWhenNoBeansFound() {
        when(context.getBeansWithAnnotation(AutoRegisterFeatureModule.class)).thenReturn(Map.of());
        featureModuleAutoRegister.setApplicationContext(context);

        featureModuleAutoRegister.afterPropertiesSet();

        verify(featureRegistry, times(0)).registerFeatureModule(any(), any());
    }

    @Test
    void failIfContextReturnsNotAnnotatedBean() {
        when(context.getBeansWithAnnotation(AutoRegisterFeatureModule.class)).thenReturn(Map.of("bean", new ClearTestBean()));
        featureModuleAutoRegister.setApplicationContext(context);

        assertThrows(IllegalArgumentException.class, () -> featureModuleAutoRegister.afterPropertiesSet());
        verify(featureRegistry, times(0)).registerFeatureModule(any(), any());
    }

    @Test
    void failIfContextReturnBeanAnnotatedWithWrongParameter() {
        when(context.getBeansWithAnnotation(AutoRegisterFeatureModule.class)).thenReturn(Map.of("bean", new WrongAnnotationTestBean()));
        featureModuleAutoRegister.setApplicationContext(context);

        assertThrows(IllegalArgumentException.class, () -> featureModuleAutoRegister.afterPropertiesSet());
        verify(featureRegistry, times(0)).registerFeatureModule(any(), any());
    }

    @Test
    void failIfContextReturnBeanOfWrongType() {
        when(context.getBeansWithAnnotation(AutoRegisterFeatureModule.class)).thenReturn(Map.of("bean", new WrongSubtypeTestBean()));
        featureModuleAutoRegister.setApplicationContext(context);

        assertThrows(IllegalArgumentException.class, () -> featureModuleAutoRegister.afterPropertiesSet());
        verify(featureRegistry, times(0)).registerFeatureModule(any(), any());
    }

    @Test
    void registerCorrectBean() {
        when(context.getBeansWithAnnotation(AutoRegisterFeatureModule.class)).thenReturn(Map.of("bean", new CorrectTestBean()));
        when(context.findAnnotationOnBean("bean", AutoRegisterFeatureModule.class)).thenReturn(AnnotationUtils.findAnnotation(CorrectTestBean.class, AutoRegisterFeatureModule.class));
        featureModuleAutoRegister.setApplicationContext(context);

        featureModuleAutoRegister.afterPropertiesSet();

        verify(featureRegistry).registerFeatureModule(eq(TestFeatureDescriptor.class), any());
    }

    static class TestFeatureDescriptor implements FeatureDescriptor {
    }

    static class ClearTestBean {
    }

    @AutoRegisterFeatureModule
    static class WrongAnnotationTestBean {
    }

    @AutoRegisterFeatureModule(TestFeatureDescriptor.class)
    static class WrongSubtypeTestBean {
    }

    @AutoRegisterFeatureModule(TestFeatureDescriptor.class)
    static class CorrectTestBean implements FeatureModule<TestFeatureDescriptor> {
        @Override
        public void plugDescriptor(Plugin plugin, TestFeatureDescriptor descriptor) {
        }

        @Override
        public void unplugDescriptor(Plugin plugin, TestFeatureDescriptor descriptor) {
        }
    }
}