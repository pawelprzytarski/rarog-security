package ut.rarogsoftware.rarog.platform.settings;

import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.core.settings.DefaultApplicationSettings;
import eu.rarogsoftware.rarog.platform.core.settings.InMemorySettingsStore;
import eu.rarogsoftware.rarog.platform.api.settings.SettingSerializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DefaultApplicationSettingsTest {
    private ApplicationSettings applicationSettings;

    @BeforeEach
    void setUp() {
        applicationSettings = new DefaultApplicationSettings(new InMemorySettingsStore());
    }

    @Test
    void testTypesStoring() {
        var testSetting = new TestSetting("Value", 23);

        applicationSettings.setSetting("integer", 23);
        applicationSettings.setSetting("double", 23.0);
        applicationSettings.setSetting("boolean", true);
        applicationSettings.setSetting("string", "justString");
        applicationSettings.setSetting("customObject", testSetting);

        assertEquals(23, applicationSettings.getSetting("integer", Integer.class).get());
        assertEquals(23.0, applicationSettings.getSetting("double", Double.class).get());
        assertEquals(true, applicationSettings.getSetting("boolean", Boolean.class).get());
        assertEquals("justString", applicationSettings.getSetting("string", String.class).get());
        assertEquals("justString", applicationSettings.getSettingAsString("string").get());
        assertEquals(testSetting, applicationSettings.getSetting("customObject", TestSetting.class).get());
    }

    @Test
    void testCustomSerializer() {
        var testSetting = new TestSetting("Value", 23);

        applicationSettings.addTypeSerializer(TestSetting.class, new SettingSerializer<TestSetting>() {
            @Override
            public TestSetting deserialize(String serialized) {
                String[] split = serialized.split("\\|");
                return new TestSetting(split[0], Integer.parseInt(split[1]));
            }

            @Override
            public String serialize(TestSetting setting) {
                return setting.someValue + "|" + setting.otherValue;
            }
        });
        applicationSettings.setSetting("customObject", testSetting);

        assertEquals("Value|23", applicationSettings.getSetting("customObject", String.class).get());
        assertEquals(testSetting, applicationSettings.getSetting("customObject", TestSetting.class).get());
    }

    @Test
    void testDefaultSettings() {
        var testSetting = new TestSetting("Value", 23);

        applicationSettings.setDefaultSetting("integer", 23);
        applicationSettings.setDefaultSetting("double", 23.0);
        applicationSettings.setDefaultSetting("boolean", true);
        applicationSettings.setDefaultSetting("string", "justString");
        applicationSettings.setDefaultSetting("customObject", testSetting);

        assertEquals(23, applicationSettings.getSettingOrDefault("integer", Integer.class));
        assertEquals(23.0, applicationSettings.getSettingOrDefault("double", Double.class));
        assertEquals(true, applicationSettings.getSettingOrDefault("boolean", Boolean.class));
        assertEquals("justString", applicationSettings.getSettingOrDefault("string", String.class));
        assertEquals("justString", applicationSettings.getSettingOrDefaultAsString("string"));
        assertEquals(testSetting, applicationSettings.getSettingOrDefault("customObject", TestSetting.class));
    }

    @Test
    void testGettingSettingsWhenDefaultsSet() {
        var testSetting = new TestSetting("Value", 23);
        var defaultSetting = new TestSetting("Value321", 123);

        applicationSettings.setDefaultSetting("integer", 123);
        applicationSettings.setDefaultSetting("double", 123.0);
        applicationSettings.setDefaultSetting("boolean", false);
        applicationSettings.setDefaultSetting("string", "justString123");
        applicationSettings.setDefaultSetting("customObject", defaultSetting);

        applicationSettings.setSetting("integer", 23);
        applicationSettings.setSetting("double", 23.0);
        applicationSettings.setSetting("boolean", true);
        applicationSettings.setSetting("string", "justString");
        applicationSettings.setSetting("customObject", testSetting);

        assertEquals(23, applicationSettings.getSettingOrDefault("integer", Integer.class));
        assertEquals(23.0, applicationSettings.getSettingOrDefault("double", Double.class));
        assertEquals(true, applicationSettings.getSettingOrDefault("boolean", Boolean.class));
        assertEquals("justString", applicationSettings.getSettingOrDefault("string", String.class));
        assertEquals("justString", applicationSettings.getSettingOrDefaultAsString("string"));
        assertEquals(testSetting, applicationSettings.getSettingOrDefault("customObject", TestSetting.class));
    }

    @Test
    void testPropertyBasedSetting() {
        System.setProperty("testProperty", "propertyValue");
        assertEquals("propertyValue", applicationSettings.getPropertyBackedSettingAsString("testProperty"));

        applicationSettings.setSetting("testProperty", "otherValue");
        assertEquals("propertyValue", applicationSettings.getPropertyBackedSettingAsString("testProperty"));

        applicationSettings.setSetting("testProperty2", "newValue");
        assertEquals("newValue", applicationSettings.getPropertyBackedSettingAsString("testProperty2"));
    }

    record TestSetting(String someValue, Integer otherValue) {
    }
}