package ut.rarogsoftware.rarog.platform.core.plugins.web.filters;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import eu.rarogsoftware.rarog.platform.api.metrics.HistogramSettings;
import eu.rarogsoftware.rarog.platform.core.metrics.noop.NoopMetricsService;
import eu.rarogsoftware.rarog.platform.core.plugins.web.filters.PluginFilterChain;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class})
class PluginFilterChainTest {

    @Mock
    private FilterChain originalChain;
    
    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private Filter mockFilter_1;

    @Mock
    private Filter mockFilter_2;

    private PluginFilterChain chain;

    @BeforeEach
    void setup() {
        chain = new PluginFilterChain(originalChain, new NoopMetricsService().createHistogram(HistogramSettings.settings()));
    }

    @Test
    void testDoFilterWithSingleFilter() throws IOException, ServletException {
        mockFilterBehaviour(mockFilter_1);
        
        chain.addFilter(mockFilter_1);
        chain.doFilter(request, response);

        InOrder inOrder = inOrder(mockFilter_1, originalChain);
        inOrder.verify(mockFilter_1).doFilter(request, response, chain);
        inOrder.verify(originalChain).doFilter(request, response);
    }

    @Test
    void testDoFilterWithMultipleFilters() throws IOException, ServletException {
        mockFilterBehaviour(mockFilter_1);
        mockFilterBehaviour(mockFilter_2);
        
        chain.addFilter(mockFilter_1);
        chain.addFilter(mockFilter_2);
        chain.doFilter(request, response);

        InOrder inOrder = inOrder(mockFilter_1, mockFilter_2, originalChain);
        inOrder.verify(mockFilter_1).doFilter(request, response, chain);
        inOrder.verify(mockFilter_2).doFilter(request, response, chain);
        inOrder.verify(originalChain).doFilter(request, response);
    }

    @Test
    void testFilterBreaksTheChain() throws IOException, ServletException {
        mockFilterBehaviour(mockFilter_1);


        chain.addFilter(mockFilter_1);
        chain.addFilter(mockFilter_2);
        chain.doFilter(request, response);

        InOrder inOrder = inOrder(mockFilter_1, mockFilter_2, originalChain);
        inOrder.verify(mockFilter_1).doFilter(request, response, chain);
        inOrder.verify(mockFilter_2).doFilter(request, response, chain);
        inOrder.verify(originalChain, never()).doFilter(request, response);
    }

    @Test
    void testFilterCallsTheChainTwice() throws IOException, ServletException {
        mockFilterBehaviour(mockFilter_2);
        mockDoubleCallingFilterBehaviour(mockFilter_1);


        chain.addFilter(mockFilter_1);
        chain.addFilter(mockFilter_2);
        chain.doFilter(request, response);

        InOrder inOrder = inOrder(mockFilter_1, mockFilter_2, originalChain);
        inOrder.verify(mockFilter_1).doFilter(request, response, chain);
        inOrder.verify(mockFilter_2).doFilter(request, response, chain);
        inOrder.verify(originalChain, times(2)).doFilter(request, response);
    }

    @Test
    void testDoFilterEmpty() throws IOException, ServletException {

        chain.doFilter(request, response);

        verify(originalChain).doFilter(request, response);
    }

    private void mockFilterBehaviour(Filter mockFilter) throws IOException, ServletException {
        doAnswer(invocation -> {
            ((FilterChain)invocation.getArgument(2)).doFilter(invocation.getArgument(0), invocation.getArgument(1));
            return null;
        })
        .when(mockFilter).doFilter(any(ServletRequest.class), any(ServletResponse.class), any(FilterChain.class));
    }

    private void mockDoubleCallingFilterBehaviour(Filter mockFilter) throws IOException, ServletException {
        doAnswer(invocation -> {
            ((FilterChain)invocation.getArgument(2)).doFilter(invocation.getArgument(0), invocation.getArgument(1));
            ((FilterChain)invocation.getArgument(2)).doFilter(invocation.getArgument(0), invocation.getArgument(1));
            return null;
        })
        .when(mockFilter).doFilter(any(ServletRequest.class), any(ServletResponse.class), any(FilterChain.class));
    }
}
