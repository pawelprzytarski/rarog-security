package ut.rarogsoftware.rarog.platform.rest;

import eu.rarogsoftware.rarog.platform.app.rest.I18NextResourcesController;
import eu.rarogsoftware.rarog.platform.core.i18n.DefaultI18NextService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class I18NextResourcesControllerTest {
    private static final String SIMPLE_RESOURCE = "Resource";
    private static final String COMPLEX_RESOURCE = """
            {
                "key": "value",
                "key2": "value2",
                "complex": {
                    "subkey": "subvalue",
                    "subkey2": {
                        "becauseWhyNot": "U OK?"
                    }
                }
            }
            """;
    private static final String SYNTHETIC_RESOURCE = """
            {"key":"key","key2":"key2","complex":{"subkey":"complex.subkey","subkey2":{"becauseWhyNot":"complex.subkey2.becauseWhyNot"}}}""";
    @Mock
    DefaultI18NextService i18NextService;
    @InjectMocks
    I18NextResourcesController controller;

    @Test
    void resourceMissingThrowsException() {
        assertThrows(ResponseStatusException.class, () -> controller.getI18NextResource("en-GB", "namespace"));
    }

    @Test
    void resourceExistsReturnsResource() throws IOException {
        when(i18NextService.getI18NextResource(any(), eq("namespace"))).thenReturn(new ByteArrayInputStream(SIMPLE_RESOURCE.getBytes(StandardCharsets.UTF_8)));
        var response = controller.getI18NextResource("en-GB", "namespace");

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertArrayEquals(SIMPLE_RESOURCE.getBytes(StandardCharsets.UTF_8), response.getBody());
    }

    @Test
    void jsonResourceReturnsJson() throws IOException {
        when(i18NextService.getI18NextResource(any(), eq("namespace"))).thenReturn(new ByteArrayInputStream(COMPLEX_RESOURCE.getBytes(StandardCharsets.UTF_8)));
        var response = controller.getI18NextResource("en-GB", "namespace");

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertArrayEquals(COMPLEX_RESOURCE.getBytes(StandardCharsets.UTF_8), response.getBody());
    }

    @Test
    void complexResourceIsConvertedForSyntheticLanguage() throws IOException {
        when(i18NextService.getI18NextResource(any(), eq("namespace"))).thenReturn(new ByteArrayInputStream(COMPLEX_RESOURCE.getBytes(StandardCharsets.UTF_8)));
        var response = controller.getI18NextResource("en-MOON", "namespace");

        System.out.println(new String(response.getBody()));
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertArrayEquals(SYNTHETIC_RESOURCE.getBytes(StandardCharsets.UTF_8), response.getBody());
    }
}