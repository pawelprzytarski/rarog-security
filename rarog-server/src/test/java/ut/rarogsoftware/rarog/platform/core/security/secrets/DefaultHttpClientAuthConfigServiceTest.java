package ut.rarogsoftware.rarog.platform.core.security.secrets;

import eu.rarogsoftware.rarog.platform.core.security.secrets.BuildInSecretsStorage;
import eu.rarogsoftware.rarog.platform.core.security.secrets.DefaultHttpClientAuthConfigService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class DefaultHttpClientAuthConfigServiceTest {
    @Mock
    HttpClient.Builder httpClientBuilder;
    @Mock
    BuildInSecretsStorage buildInSecretsStorage;
    @Mock
    HttpRequest httpRequest;
    @InjectMocks
    DefaultHttpClientAuthConfigService authConfigService;
    private final URI TEST_URI = URI.create("https://test");
    private final String TEST_HEADER = "HeaderContent";

    @Test
    void testNoAuthIsAppliedToRequestWhenAuthStringIsEmpty() {
        var authConfig = authConfigService.resolveFromString("");
        var authConfigForNull = authConfigService.resolveFromString(null);

        var client = authConfig.configureHttpClient(httpClientBuilder);
        var request = authConfig.configureHttpRequest(httpRequest);

        assertThat(client).isSameAs(httpClientBuilder);
        verifyNoInteractions(httpClientBuilder);
        assertThat(request).isSameAs(httpRequest);
        verifyNoInteractions(httpRequest);
        assertThat(authConfigForNull).isEqualTo(authConfig);
    }

    @Test
    void testNoAuthIsAppliedToRequestWhenNoRequestedAnything() {
        var authConfig = authConfigService.resolveFromSettings(Map.of("method", ""));
        var authConfigForEmpty = authConfigService.resolveFromSettings(Map.of());
        var authConfigForMissingType = authConfigService.resolveFromSettings(Map.of("other", "something"));
        var authConfigForNull = authConfigService.resolveFromSettings(null);

        var client = authConfig.configureHttpClient(httpClientBuilder);
        var request = authConfig.configureHttpRequest(httpRequest);

        assertThat(client).isSameAs(httpClientBuilder);
        verifyNoInteractions(httpClientBuilder);
        assertThat(request).isSameAs(httpRequest);
        verifyNoInteractions(httpRequest);
        assertThat(authConfig)
                .isEqualTo(authConfigForEmpty)
                .isEqualTo(authConfigForMissingType)
                .isEqualTo(authConfigForNull);
    }

    @Test
    void testAuthorizationHeaderIsAppliedToRequestWhenAuthStringIsProvidingAuthMethod() {
        var authConfig = authConfigService.resolveFromString("auth:" + TEST_HEADER);

        var client = authConfig.configureHttpClient(httpClientBuilder);
        var request = authConfig.configureHttpRequest(HttpRequest.newBuilder().uri(TEST_URI).build());

        assertThat(client).isSameAs(httpClientBuilder);
        verifyNoInteractions(httpClientBuilder);
        assertThat(request.headers().map()).containsEntry(HttpHeaders.AUTHORIZATION, List.of(TEST_HEADER));
    }

    @Test
    void testAuthorizationHeaderIsAppliedToRequestWhenAuthConfigIsProvidingValue() {
        var authConfig = authConfigService.resolveFromSettings(Map.of("method", "auth", "value", TEST_HEADER));

        var client = authConfig.configureHttpClient(httpClientBuilder);
        var request = authConfig.configureHttpRequest(HttpRequest.newBuilder().uri(TEST_URI).build());

        assertThat(client).isSameAs(httpClientBuilder);
        verifyNoInteractions(httpClientBuilder);
        assertThat(request.headers().map()).containsEntry(HttpHeaders.AUTHORIZATION, List.of(TEST_HEADER));
    }

    @Test
    void testVaultAccessThrowsException() {
        assertThatThrownBy(() -> authConfigService.resolveFromString("vault:something"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Secrets vault is not supported");
        assertThatThrownBy(() -> authConfigService.resolveFromSettings(Map.of("method", "vault")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Secrets vault is not supported");
    }

    @Test
    void testBuildInAuthStringResolvesToOtherMethod() {
        when(buildInSecretsStorage.getAuthConfigStringForName("test")).thenReturn(Optional.of(Map.of("method", "auth", "value", TEST_HEADER)));

        var authConfig = authConfigService.resolveFromString("buildin:test");

        var client = authConfig.configureHttpClient(httpClientBuilder);
        var request = authConfig.configureHttpRequest(HttpRequest.newBuilder().uri(TEST_URI).build());

        assertThat(client).isSameAs(httpClientBuilder);
        verifyNoInteractions(httpClientBuilder);
        assertThat(request.headers().map()).containsEntry(HttpHeaders.AUTHORIZATION, List.of(TEST_HEADER));
    }

    @Test
    void testBuildInAuthSettingsResolvesToOtherMethod() {
        when(buildInSecretsStorage.getAuthConfigStringForName("test")).thenReturn(Optional.of(Map.of("method", "auth", "value", TEST_HEADER)));

        var authConfig = authConfigService.resolveFromSettings(Map.of("method", "buildin", "value", "test"));

        var client = authConfig.configureHttpClient(httpClientBuilder);
        var request = authConfig.configureHttpRequest(HttpRequest.newBuilder().uri(TEST_URI).build());

        assertThat(client).isSameAs(httpClientBuilder);
        verifyNoInteractions(httpClientBuilder);
        assertThat(request.headers().map()).containsEntry(HttpHeaders.AUTHORIZATION, List.of(TEST_HEADER));
    }

    @Test
    void testBuildInAuthForNotExistingConfigThrows() {
        when(buildInSecretsStorage.getAuthConfigStringForName("test")).thenReturn(Optional.empty());

        assertThatThrownBy(() -> authConfigService.resolveFromString("buildin:test"))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void testUnknownAuthMethodThrowsException() {
        assertThatThrownBy(() -> authConfigService.resolveFromString("unknown:something"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Specified authentication methods is not supported");
        assertThatThrownBy(() -> authConfigService.resolveFromSettings(Map.of("method", "unknown")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Specified authentication methods is not supported");
    }
}