package ut.rarogsoftware.rarog.platform.mfa.recovery;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.security.mfa.recovery.DefaultRecoveryCodesService;
import eu.rarogsoftware.rarog.platform.security.mfa.recovery.RecoveryCodesService.RecoveryCodeLoginResult;
import eu.rarogsoftware.rarog.platform.security.mfa.recovery.RecoveryCodesStore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DefaultRecoveryCodesServiceTest {
    private final String CORRECT_CODE = "test1";
    private final String INCORRECT_CODE = "randomCode";
    @Mock
    RecoveryCodesStore recoveryCodesStore;
    @Mock
    StandardUser user;
    @InjectMocks
    DefaultRecoveryCodesService recoveryCodesService;

    @Test
    void testNotExistingWillNotPass() {
        when(recoveryCodesStore.removeCode(any(), any())).thenReturn(false);

        var result = recoveryCodesService.useRecoveryCode(user, INCORRECT_CODE);

        assertEquals(RecoveryCodeLoginResult.failure(), result);
    }

    @Test
    void testBigListWillPassForCorrectCode() {
        when(recoveryCodesStore.removeCode(user, CORRECT_CODE)).thenReturn(true);
        when(recoveryCodesStore.getCodesCount(user)).thenReturn(20);

        var result = recoveryCodesService.useRecoveryCode(user, CORRECT_CODE);

        assertEquals(RecoveryCodeLoginResult.success(), result);
        verify(recoveryCodesStore).removeCode(user, CORRECT_CODE);
    }

    @Test
    void testSmallListWillPassAndRegenerateForCorrectCode() {
        when(recoveryCodesStore.getCodesCount(user)).thenReturn(2);
        when(recoveryCodesStore.removeCode(user, CORRECT_CODE)).thenReturn(true);

        var result = recoveryCodesService.useRecoveryCode(user, CORRECT_CODE);

        assertTrue(result.isSuccess());
        assertEquals(RecoveryCodeLoginResult.Status.FOUND_AND_REGENERATED, result.status());
        assertEquals(20, result.newCodes().size());
        verify(recoveryCodesStore).replaceCodes(eq(user), any());
    }

    @Test
    void testGenerateCodes() {
        var result = new HashSet<>(recoveryCodesService.generateRecoveryCodes(user));

        assertEquals(20, result.size());
        verify(recoveryCodesStore).replaceCodes(eq(user), any());
    }
}