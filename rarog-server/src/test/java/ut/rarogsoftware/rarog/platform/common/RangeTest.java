package ut.rarogsoftware.rarog.platform.common;

import eu.rarogsoftware.rarog.platform.core.common.Range;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RangeTest {
    @Test
    void testInclusiveRanges() {
        Range<Integer> range = Range.inclusive(1, 3);
        assertFalse(range.isInRange(0));
        assertTrue(range.isInRange(1));
        assertTrue(range.isInRange(2));
        assertTrue(range.isInRange(3));
        assertFalse(range.isInRange(4));
    }

    @Test
    void testExclusiveRanges() {
        Range<Integer> range = Range.exclusive(1, 3);
        assertFalse(range.isInRange(0));
        assertFalse(range.isInRange(1));
        assertTrue(range.isInRange(2));
        assertFalse(range.isInRange(3));
        assertFalse(range.isInRange(4));
    }

    @Test
    void testOpenEndRanges() {
        Range<Integer> range = Range.openEnd(1, 3);
        assertFalse(range.isInRange(0));
        assertTrue(range.isInRange(1));
        assertTrue(range.isInRange(2));
        assertFalse(range.isInRange(3));
        assertFalse(range.isInRange(4));
    }

    @Test
    void testOpenStartRanges() {
        Range<Integer> range = Range.openStart(1, 3);
        assertFalse(range.isInRange(0));
        assertFalse(range.isInRange(1));
        assertTrue(range.isInRange(2));
        assertTrue(range.isInRange(3));
        assertFalse(range.isInRange(4));
    }
}