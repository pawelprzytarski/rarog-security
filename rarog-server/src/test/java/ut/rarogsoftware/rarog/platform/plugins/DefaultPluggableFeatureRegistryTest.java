package ut.rarogsoftware.rarog.platform.plugins;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.FeatureModule;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.PurgeableFeatureModule;
import eu.rarogsoftware.rarog.platform.core.plugins.DefaultPluggableFeatureRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DefaultPluggableFeatureRegistryTest {
    @Mock
    private Plugin testPlugin;
    @Mock
    private TestFeatureModule testFeatureModule;
    private DefaultPluggableFeatureRegistry featureRegistry;

    @BeforeEach
    void setUp() {
        featureRegistry = new DefaultPluggableFeatureRegistry();
    }

    @Test
    void registerTestModuleAndSuccessfullyReceivePluggedDescriptor() {
        TestFeatureDescriptor testFeatureDescriptor = new TestFeatureDescriptor();

        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> testFeatureModule);
        featureRegistry.plugDescriptors(testPlugin, Collections.singletonList(testFeatureDescriptor));

        verify(testFeatureModule).plugDescriptor(testPlugin, testFeatureDescriptor);
    }

    @Test
    void attemptToRegisterTwiceThrowsException() {
        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> testFeatureModule);
        assertThrows(IllegalArgumentException.class, () -> featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> testFeatureModule));
    }

    @Test
    void pluggingDescriptorWorksAfterRegisteringUnregisteredPreviousModule() {
        TestFeatureDescriptor testFeatureDescriptor = new TestFeatureDescriptor();

        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> mock(TestFeatureModule.class));
        featureRegistry.unregisterFeatureModule(TestFeatureDescriptor.class);

        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> testFeatureModule);

        featureRegistry.plugDescriptors(testPlugin, Collections.singletonList(testFeatureDescriptor));

        verify(testFeatureModule).plugDescriptor(testPlugin, testFeatureDescriptor);
    }

    @Test
    void unregisteredTestModuleDoNotReceivesDescriptor() {
        TestFeatureDescriptor testFeatureDescriptor = new TestFeatureDescriptor();

        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> testFeatureModule);
        featureRegistry.unregisterFeatureModule(TestFeatureDescriptor.class);
        featureRegistry.plugDescriptors(testPlugin, Collections.singletonList(testFeatureDescriptor));

        verify(testFeatureModule, times(0)).plugDescriptor(any(), any());
    }

    @Test
    void registerTestModulePlugsDescriptorUnassignedAfterUnregister() {
        TestFeatureDescriptor testFeatureDescriptor = new TestFeatureDescriptor();

        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> mock(TestFeatureModule.class));
        featureRegistry.plugDescriptors(testPlugin, Collections.singletonList(testFeatureDescriptor));
        featureRegistry.unregisterFeatureModule(TestFeatureDescriptor.class);
        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> testFeatureModule);

        verify(testFeatureModule).plugDescriptor(testPlugin, testFeatureDescriptor);
    }

    @Test
    void plugUnknownDescriptorDoNotInvolveRegisteredModules() {
        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> testFeatureModule);
        featureRegistry.plugDescriptors(testPlugin, Collections.singletonList(mock(FeatureDescriptor.class)));

        verify(testFeatureModule, times(0)).plugDescriptor(any(), any());
    }

    @Test
    void plugSubclassOfKnownDescriptorDoNotInvolveRegisteredModules() {
        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> testFeatureModule);
        featureRegistry.plugDescriptors(testPlugin, Collections.singletonList(new SubClassTestFeatureDescriptor()));

        verify(testFeatureModule, times(0)).plugDescriptor(any(), any());
    }

    @Test
    void unregisteringTestModuleUnplugsDescriptors() {
        TestFeatureDescriptor testFeatureDescriptor = new TestFeatureDescriptor();

        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> testFeatureModule);
        featureRegistry.plugDescriptors(testPlugin, Collections.singletonList(testFeatureDescriptor));
        featureRegistry.unregisterFeatureModule(TestFeatureDescriptor.class);

        verify(testFeatureModule).unplugDescriptor(testPlugin, testFeatureDescriptor);
    }

    @Test
    void unpluggingDescriptorUnplugsItFromModule() {
        TestFeatureDescriptor testFeatureDescriptor = new TestFeatureDescriptor();

        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> testFeatureModule);
        featureRegistry.plugDescriptors(testPlugin, Collections.singletonList(testFeatureDescriptor));
        featureRegistry.unplugDescriptors(testPlugin, Collections.singletonList(testFeatureDescriptor));

        verify(testFeatureModule).unplugDescriptor(testPlugin, testFeatureDescriptor);
    }

    @Test
    void unpluggedDescriptorDoesNotPlugsDuringRegisteringModule() {
        TestFeatureDescriptor testFeatureDescriptor = new TestFeatureDescriptor();

        featureRegistry.plugDescriptors(testPlugin, Collections.singletonList(testFeatureDescriptor));
        featureRegistry.unplugDescriptors(testPlugin, Collections.singletonList(testFeatureDescriptor));

        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> testFeatureModule);

        verify(testFeatureModule, times(0)).plugDescriptor(testPlugin, testFeatureDescriptor);
    }

    @Test
    void purgingDescriptorMakeModuleToPurgeData() {
        TestFeatureDescriptor testFeatureDescriptor = new TestFeatureDescriptor();
        var purgeableModule = mock(PurgeableTestFeatureModule.class);

        featureRegistry.registerFeatureModule(TestFeatureDescriptor.class, () -> purgeableModule);
        featureRegistry.purgeDescriptors(testPlugin, Collections.singletonList(testFeatureDescriptor));

        verify(purgeableModule).purgeDescriptor(testPlugin, testFeatureDescriptor);
    }

    static class TestFeatureDescriptor implements FeatureDescriptor {
    }

    static class SubClassTestFeatureDescriptor extends TestFeatureDescriptor {
    }

    static class TestFeatureModule implements FeatureModule<TestFeatureDescriptor> {
        @Override
        public void plugDescriptor(Plugin plugin, TestFeatureDescriptor descriptor) {
        }

        @Override
        public void unplugDescriptor(Plugin plugin, TestFeatureDescriptor descriptor) {
        }
    }

    static class PurgeableTestFeatureModule implements PurgeableFeatureModule<TestFeatureDescriptor> {
        @Override
        public void plugDescriptor(Plugin plugin, TestFeatureDescriptor descriptor) {
        }

        @Override
        public void unplugDescriptor(Plugin plugin, TestFeatureDescriptor descriptor) {
        }

        @Override
        public void purgeDescriptor(Plugin plugin, TestFeatureDescriptor descriptor) {
        }
    }
}