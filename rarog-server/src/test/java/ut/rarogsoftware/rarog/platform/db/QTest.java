package ut.rarogsoftware.rarog.platform.db;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;

import javax.annotation.Generated;
import java.sql.Types;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QTest1 is a Querydsl query type for QTest1
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QTest extends com.querydsl.sql.RelationalPathBase<QTest> {

    public static final QTest test1 = new QTest("test");
    private static final long serialVersionUID = -1466171147;
    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath name = createString("name");

    public final StringPath test = createString("test");

    public final com.querydsl.sql.PrimaryKey<QTest> test1Pk = createPrimaryKey(id);

    public QTest(String variable) {
        super(QTest.class, forVariable(variable), "PUBLIC", "test");
        addMetadata();
    }

    public QTest(String variable, String schema, String table) {
        super(QTest.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTest(String variable, String schema) {
        super(QTest.class, forVariable(variable), schema, "test");
        addMetadata();
    }

    public QTest(Path<? extends QTest> path) {
        super(path.getType(), path.getMetadata(), "PUBLIC", "test");
        addMetadata();
    }

    public QTest(PathMetadata metadata) {
        super(QTest.class, metadata, "PUBLIC", "test");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("ID").withIndex(1).ofType(Types.INTEGER).withSize(10).notNull());
        addMetadata(name, ColumnMetadata.named("NAME").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(test, ColumnMetadata.named("TEST").withIndex(2).ofType(Types.VARCHAR).withSize(255));
    }

}

