package eu.rarogsoftware.rarog.platform.core.task;

import eu.rarogsoftware.rarog.platform.api.task.ScheduleConfig;
import jakarta.servlet.ServletContextEvent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

class AdvancedScheduledThreadPoolExecutorTest {
    AdvancedScheduledThreadPoolExecutor scheduler;

    @BeforeEach
    void setUp() {
        scheduler = new AdvancedScheduledThreadPoolExecutor(1, Thread::new);
    }

    @AfterEach
    void tearDown() {
        scheduler.shutdown();
    }

    @Test
    void testScheduleAdvancedWithDelayOnlyExecutesOnce() {
        AtomicLong executionCount = new AtomicLong(0);
        scheduler.scheduleAdvanced(getSimpleTask(executionCount), ScheduleConfig.builder()
                .initialDelay(Duration.ofMillis(100))
                .build());

        assertThat(executionCount.get()).isZero();
        await()
                .atLeast(Duration.ofMillis(80))
                .atMost(Duration.ofMillis(120))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(1));
        // make sure it do not repeated
        await()
                .atLeast(Duration.ofMillis(80))
                .atMost(Duration.ofMillis(120))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(1));
    }

    @Test
    void testScheduleAdvancedWithPeriodAndRepeatCountExecutesSpecifiedAmountOnly() {
        AtomicLong executionCount = new AtomicLong(0);
        scheduler.scheduleAdvanced(getSimpleTask(executionCount), ScheduleConfig.builder()
                .initialDelay(Duration.ofMillis(100))
                .period(Duration.ofMillis(100))
                .repeatCount(5)
                .build());

        assertThat(executionCount.get()).isZero();
        await()
                .atLeast(Duration.ofMillis(80))
                .atMost(Duration.ofMillis(120))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(1));

        await()
                .atLeast(Duration.ofMillis(380))
                .atMost(Duration.ofMillis(450))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(5));
        // make sure it do not repeated
        await()
                .atLeast(Duration.ofMillis(80))
                .atMost(Duration.ofMillis(120))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(5));
    }

    @Test
    void scheduleAdvancedWithPeriodIsNotAffectedByTaskDuration() {
        AtomicLong executionCount = new AtomicLong(0);
        scheduler.scheduleAdvanced(getDelayedTask(executionCount), ScheduleConfig.builder()
                .initialDelay(Duration.ofMillis(100))
                .period(Duration.ofMillis(100))
                .repeatCount(5)
                .build());

        assertThat(executionCount.get()).isZero();
        await()
                .atLeast(Duration.ofMillis(80))
                .atMost(Duration.ofMillis(120))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(1));

        await()
                .atLeast(Duration.ofMillis(380))
                .atMost(Duration.ofMillis(420))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(5));
        // make sure it do not repeated
        await()
                .atLeast(Duration.ofMillis(80))
                .atMost(Duration.ofMillis(130))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(5));
    }

    @Test
    void scheduleAdvancedWithRepeatAfterIsAffectedByTaskDuration() {
        AtomicLong executionCount = new AtomicLong(0);
        scheduler.scheduleAdvanced(getDelayedTask(executionCount), ScheduleConfig.builder()
                .initialDelay(Duration.ofMillis(100))
                .repeatAfter(Duration.ofMillis(100))
                .repeatCount(5)
                .build());

        assertThat(executionCount.get()).isZero();
        await()
                .atLeast(Duration.ofMillis(80))
                .atMost(Duration.ofMillis(120))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(1));

        await()
                .atLeast(Duration.ofMillis(180))
                .atMost(Duration.ofMillis(220))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(2));
        await()
                .atLeast(Duration.ofMillis(570))
                .atMost(Duration.ofMillis(630))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(5));
        // make sure it do not repeated
        await()
                .atLeast(Duration.ofMillis(80))
                .atMost(Duration.ofMillis(220))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(5));
    }

    @Test
    void scheduleAdvancedWithPeriodAndExecutionConditionExecutesSpecifiedAmountOfTimes() {
        AtomicLong executionCount = new AtomicLong(0);
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        scheduler.scheduleAdvanced(getSimpleTask(executionCount), ScheduleConfig.builder()
                .initialDelay(Duration.ofMillis(50))
                .period(Duration.ofMillis(50))
                .repeatCount(5)
                .executionCondition(info -> atomicBoolean.get())
                .build());

        assertThat(executionCount.get()).isZero();
        await()
                .atLeast(Duration.ofMillis(80))
                .atMost(Duration.ofMillis(120))
                .untilAsserted(() -> assertThat(executionCount.get()).isZero());
        atomicBoolean.getAndSet(true);
        await()
                .atLeast(Duration.ofMillis(240))
                .atMost(Duration.ofMillis(320))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(5));
    }

    @Test
    void scheduleAdvancedWithPeriodAndCancelConditionExecutesLessTimesThanRepeatNumber() {
        AtomicLong executionCount = new AtomicLong(0);
        scheduler.scheduleAdvanced(getSimpleTask(executionCount), ScheduleConfig.builder()
                .initialDelay(Duration.ofMillis(50))
                .period(Duration.ofMillis(50))
                .repeatCount(5)
                .cancelCondition(info -> executionCount.get() >= 3)
                .build());

        await()
                .pollDelay(Duration.ofMillis(250))
                .untilAsserted(() -> assertThat(executionCount.get()).isEqualTo(3));
    }

    @Test
    void schedulerStopsOnServletContextDestroyed() {
        scheduler.contextDestroyed(Mockito.mock(ServletContextEvent.class));
        assertThat(scheduler.isShutdown()).isTrue();
    }

    private static Callable<Object> getSimpleTask(AtomicLong executionCount) {
        return executionCount::incrementAndGet;
    }

    private static Runnable getDelayedTask(AtomicLong executionCount) {
        return () -> {
            executionCount.incrementAndGet();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        };
    }
}