package eu.rarogsoftware.rarog.platform;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.core.security.LoginAttemptService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Import(TestBeansConfiguration.class)
@AutoConfigureMockMvc
class LoginAttemptServiceTests {
    @Autowired
    LoginAttemptService loginAttemptService;
    @Autowired
    MockMvc mockMvc;

    @Test
    void testFailedLogins() throws Exception {
        for (int i = 0; i < 5; i++) {
            mockMvc.perform(MockMvcRequestBuilders
                    .post("/login/perform")
                    .with(SecurityMockMvcRequestPostProcessors.csrf())
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("User-Agent", "TestUserAgent")
                    .content("{\"username\": \"admin\", \"password\": \"aaaa\"}")).andReturn().getResponse();
        }
        List<LoginAttemptService.LoginAttempt> attemptsList = loginAttemptService.getLoginAttempts(StandardUser.builder().id(1L).username("admin").build(), 10);
        assertEquals(5, attemptsList.size());

        attemptsList.forEach(loginAttempt -> {
            assertThat(loginAttempt.attemptTime(), greaterThan(Instant.EPOCH.minusSeconds(1)));
            assertFalse(loginAttempt.successful());
            assertEquals("127.0.0.1", loginAttempt.attemptIp());
            assertEquals("TestUserAgent", loginAttempt.userAgent());
        });
    }

    @Test
    void testSuccessfulLogins() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/login/perform")
                .with(SecurityMockMvcRequestPostProcessors.csrf())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .header("User-Agent", "TestUserAgent")
                .content("{\"username\": \"admin\", \"password\": \"admin\"}")).andReturn().getResponse();
        List<LoginAttemptService.LoginAttempt> attemptsList = loginAttemptService.getLoginAttempts(StandardUser.builder().id(1L).username("admin").build(), 1);
        LoginAttemptService.LoginAttempt loginAttempt = attemptsList.get(0);

        assertThat(loginAttempt.attemptTime(), greaterThan(Instant.EPOCH.minusSeconds(1)));
        assertTrue(loginAttempt.successful());
        assertEquals("127.0.0.1", loginAttempt.attemptIp());
        assertEquals("TestUserAgent", loginAttempt.userAgent());
    }

}
