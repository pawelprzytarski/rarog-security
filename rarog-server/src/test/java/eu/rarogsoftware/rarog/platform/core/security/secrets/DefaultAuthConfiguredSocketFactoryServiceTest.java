package eu.rarogsoftware.rarog.platform.core.security.secrets;

import okhttp3.tls.HandshakeCertificates;
import okhttp3.tls.HeldCertificate;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.net.ServerSocketFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
class DefaultAuthConfiguredSocketFactoryServiceTest {
    @Mock
    BuildInSecretsStorage buildInSecretsStorage;
    @InjectMocks
    DefaultAuthConfiguredSocketFactoryService socketFactoryService;

    @Test
    void testNoAuthSendCorrectData() throws IOException, InterruptedException {
        try (var serverSocket = ServerSocketFactory.getDefault().createServerSocket()) {
            var receivedData = new ArrayList<String>();
            var port = bindSocketToAnyFreePort(serverSocket);
            var thread = mockServerToReadSingleLine(serverSocket, receivedData);

            var socketFactory = socketFactoryService.resolveFromString("");
            try (var socket = socketFactory.createSocket()) {
                testSocketCommunication(receivedData, port, socket);
            } finally {
                thread.join(1000);
                if (thread.isAlive()) {
                    thread.interrupt();
                }
            }
        }
    }

    @Test
    void testMutualTlsAuthReadDataCorrectlyFromFiles() throws IOException, InterruptedException {
        try (var rootStream = getClass().getResourceAsStream("/test_certs/myca.crt");
             var keyStream = getClass().getResourceAsStream("/test_certs/client.key.nopass")) {
            var rootCertificate = HeldCertificate.decode(new String(rootStream.readAllBytes(), StandardCharsets.UTF_8)
                    + new String(keyStream.readAllBytes(), StandardCharsets.UTF_8));
            var localhostCertificate = new HeldCertificate.Builder()
                    .addSubjectAlternativeName("localhost")
                    .build();
            var serverCertificates = new HandshakeCertificates.Builder()
                    .heldCertificate(localhostCertificate)
                    .addPlatformTrustedCertificates()
                    .addTrustedCertificate(rootCertificate.certificate())
                    .build();

            try (var serverSocket = serverCertificates.sslContext().getServerSocketFactory().createServerSocket()) {
                var receivedData = new ArrayList<String>();
                var port = bindSocketToAnyFreePort(serverSocket);
                var thread = mockServerToReadSingleLine(serverSocket, receivedData);

                var socketFactory = socketFactoryService
                        .extendTrustedCertificates(List.of(localhostCertificate.certificate()))
                        .resolveFromString("client_cert:cert=classpath:/test_certs/client.crt,privkey=classpath:/test_certs/client.key.nopass");
                try (var socket = socketFactory.createSocket()) {
                    testSocketCommunication(receivedData, port, socket);
                } finally {
                    thread.join(1000);
                    if (thread.isAlive()) {
                        thread.interrupt();
                    }
                }
            }
        }
    }

    @Test
    void testMutualTlsAuthReadDataFromBytes() throws IOException, InterruptedException {
        try (var rootStream = getClass().getResourceAsStream("/test_certs/myca.crt");
             var keyStream = getClass().getResourceAsStream("/test_certs/client.key.nopass")) {
            var rootCertificate = new HeldCertificate.Builder()
                    .certificateAuthority(1)
                    .build();
            var localhostCertificate = new HeldCertificate.Builder()
                    .addSubjectAlternativeName("localhost")
                    .signedBy(rootCertificate)
                    .build();
            var serverCertificates = new HandshakeCertificates.Builder()
                    .heldCertificate(localhostCertificate)
                    .addPlatformTrustedCertificates()
                    .addTrustedCertificate(rootCertificate.certificate())
                    .build();
            var clientCertificate = new HeldCertificate.Builder()
                    .commonName("client")
                    .signedBy(rootCertificate)
                    .build();
            try (var serverSocket = serverCertificates.sslContext().getServerSocketFactory().createServerSocket()) {
                var receivedData = new ArrayList<String>();
                var port = bindSocketToAnyFreePort(serverSocket);
                var thread = mockServerToReadSingleLine(serverSocket, receivedData);

                var socketFactory = socketFactoryService
                        .extendTrustedCertificates(List.of(localhostCertificate.certificate()))
                        .resolveFromSettings(Map.of(
                                "method", "client_cert",
                                "cert_bytes", clientCertificate.certificatePem().getBytes(StandardCharsets.UTF_8),
                                "privkey_bytes", clientCertificate.privateKeyPkcs8Pem().getBytes(StandardCharsets.UTF_8)
                        ));
                try (var socket = socketFactory.createSocket()) {
                    testSocketCommunication(receivedData, port, socket);
                } finally {
                    thread.join(1000);
                    if (thread.isAlive()) {
                        thread.interrupt();
                    }
                }
            }
        }
    }

    private static void testSocketCommunication(ArrayList<String> receivedData, int port, Socket socket) throws IOException {
        socket.connect(new InetSocketAddress(port));
        var outputStream = socket.getOutputStream();
        outputStream.write("Test data\n".getBytes(StandardCharsets.UTF_8));
        var received = new String(socket.getInputStream().readAllBytes(), StandardCharsets.UTF_8);

        assertThat(received).isEqualTo("Server data");
        assertThat(receivedData).hasSize(1).contains("Test data");
    }

    @NotNull
    private static Thread mockServerToReadSingleLine(ServerSocket serverSocket, ArrayList<String> receivedData) {
        var thread = new Thread(() -> {
            try {
                var socket = serverSocket.accept();
                socket.getOutputStream().write("Server data".getBytes(StandardCharsets.UTF_8));
                var reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                var received = reader.readLine();
                receivedData.add(received);
                socket.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        thread.start();
        return thread;
    }

    private static int bindSocketToAnyFreePort(ServerSocket serverSocket) throws IOException {
        int startSocket = 23456;
        int endSocket = 23556;
        int port = startSocket;
        IOException lastException = null;
        for (; port <= endSocket; port++) {
            try {
                serverSocket.bind(new InetSocketAddress(port));
                return port;
            } catch (IOException e) {
                lastException = e;
            }
        }
        throw lastException;
    }
}