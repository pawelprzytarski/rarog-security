package eu.rarogsoftware.rarog.platform.core.settings;

import eu.rarogsoftware.rarog.platform.TestBeansConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;

import static org.assertj.core.api.Assertions.*;


@SpringBootTest
@Import({TestBeansConfiguration.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class TestDbSettingsStore {

    @Autowired
    DbSettingsStore settingsStore;

    @Test
    void testSettingIsStored() {
        settingsStore.setSetting("test", "value");

        assertThat(settingsStore.getSetting("test")).isEqualTo("value");
    }

    @Test
    void testExistingSettingIsUpdated() {
        settingsStore.setSetting("test", "value");

        settingsStore.setSetting("test", "value two");

        assertThat(settingsStore.getSetting("test")).isEqualTo("value two");
    }

    @Test
    void testNullSettingIsSaved() {
        settingsStore.setSetting("test", "value");

        settingsStore.setSetting("test", null);

        assertThat(settingsStore.getSetting("test")).isNull();
    }

    @Test
    void testNullKeyThrows() {
        assertThatThrownBy(() -> settingsStore.setSetting(null, "value")).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void testExistingSettingIsRetrieved() {
        assertThat(settingsStore.getSetting("preexisting-setting")).isEqualTo("Rarog reigns this world truly");
    }

    @Test
    void testNotExistingSettingIsRetrieved() {
        assertThat(settingsStore.getSetting("not-existing-setting")).isNull();
    }
}