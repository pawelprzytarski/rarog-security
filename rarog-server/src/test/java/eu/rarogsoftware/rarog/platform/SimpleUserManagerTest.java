package eu.rarogsoftware.rarog.platform;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.api.user.management.UserManagementException;
import eu.rarogsoftware.rarog.platform.api.user.management.UserNotFoundException;
import eu.rarogsoftware.rarog.platform.api.user.management.UserRole;
import eu.rarogsoftware.rarog.platform.core.user.management.SimpleUserManager;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Import(TestBeansConfiguration.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SimpleUserManagerTest {
    private static final String NOT_EXISTENT_USER_NAME = "nonExistentUser";
    private static final String TEST_USER = "testUser";
    private static final String TEST_PASSWORD = "testPassword";
    private static final String MFA_ADMIN = "mfaadmin";
    private static final String HASHED_PASSWORD_MFA_ADMIN = "$2a$12$yi2jWrreSaOz5vavf7eF.OcclkUubF1gmJEfffNLAe2UorBctaDGi";
    private static final String HASHED_PASSWORD_ADMIN = "$2a$12$eo2HzrWzUQ0zF8dJbuARKOaBlA.tbHGpjn4wy9O0v6LqKjkSpl0B";
    private static final long NOT_EXISTING_ID = 10000L;
    private static final String ADMIN_USERNAME = "admin";
    private static final long ADMIN_ID = 1L;
    private static final String NEW_PASSWORD_2 = "new2";
    private static final String NEW_PASSWORD_3 = "new3";
    private long testUserId = 5L;
    @Autowired
    private SimpleUserManager userManager;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    void testAdminUserIsPresentInTestData() throws UserManagementException {
        var expectedUser = StandardUser.builder()
                .id(ADMIN_ID)
                .username(ADMIN_USERNAME)
                .password(HASHED_PASSWORD_ADMIN)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .mfaEnabled(false)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();
        var adminUser = userManager.getUser(ADMIN_USERNAME);

        assertInstanceOf(StandardUser.class, adminUser);

        assertEquals(expectedUser, adminUser);
    }

    @Test
    void testAdminUserIsPresentInTestDataWhenRetrievedById() throws UserManagementException {
        var expectedUser = StandardUser.builder()
                .id(ADMIN_ID)
                .username(ADMIN_USERNAME)
                .password(HASHED_PASSWORD_ADMIN)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .mfaEnabled(false)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();
        var adminUser = userManager.getUser(ADMIN_ID);

        assertInstanceOf(StandardUser.class, adminUser);

        assertEquals(expectedUser, adminUser);
    }

    @Test
    void testMfaAdminUserIsPresentInTestData() throws UserManagementException {
        var expectedUser = StandardUser.builder()
                .id(2L)
                .username(MFA_ADMIN)
                .password(HASHED_PASSWORD_MFA_ADMIN)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .mfaEnabled(true)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();
        var adminUser = userManager.getUser(MFA_ADMIN);

        assertInstanceOf(StandardUser.class, adminUser);

        assertEquals(expectedUser, adminUser);
    }

    @Test
    @Order(1)
    void testCanAddAndRetrieveUser() throws UserManagementException {
        var expectedUser = StandardUser.builder()
                .id(testUserId)
                .username(TEST_USER)
                .password(TEST_PASSWORD)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .mfaEnabled(true)
                .roles(UserRole.USER, UserRole.ADMINISTRATOR)
                .build();

        var newUser = StandardUser.builder()
                .username(TEST_USER)
                .password(TEST_PASSWORD)
                .enabled(true)
                .mfaEnabled(true)
                .roles(UserRole.USER, UserRole.ADMINISTRATOR)
                .build();

        testUserId = userManager.createUser(newUser);
        var retrievedUser = userManager.getUser(TEST_USER);
        var retrievedUserById = userManager.getUser(testUserId);

        assertInstanceOf(StandardUser.class, retrievedUser);

        assertEquals(expectedUser, retrievedUser);
        assertEquals(expectedUser, retrievedUserById);
    }

    @Test
    @Order(2)
    void testCanUpdateAndRetrieveUser() throws UserManagementException {
        var expectedUser = StandardUser.builder()
                .id(testUserId)
                .username(TEST_USER)
                .password(TEST_PASSWORD)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(false)
                .mfaEnabled(false)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();

        var updateUser = StandardUser.builder()
                .username(TEST_USER)
                .enabled(false)
                .mfaEnabled(false)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();

        userManager.updateUser(updateUser);
        var retrievedUser = userManager.getUser(TEST_USER);

        assertInstanceOf(StandardUser.class, retrievedUser);

        assertEquals(expectedUser, retrievedUser);
    }

    @Test
    void testUserExists() {
        assertTrue(userManager.userExists(ADMIN_USERNAME));
        assertFalse(userManager.userExists(NOT_EXISTENT_USER_NAME));
    }

    @Test
    void testUserExistsById() {
        assertTrue(userManager.userExists(ADMIN_ID));
        assertFalse(userManager.userExists(NOT_EXISTING_ID));
    }

    @Test
    @Order(3)
    void testUpdatePassword() throws UserManagementException {
        var expectedUser = StandardUser.builder()
                .id(testUserId)
                .username(TEST_USER)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(false)
                .mfaEnabled(false)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();

        userManager.updatePassword(TEST_USER, NEW_PASSWORD_2);
        var retrievedUser = userManager.getUser(TEST_USER);

        userManager.updatePassword(retrievedUser.getId(), NEW_PASSWORD_3);
        var retrievedUserChangedById = userManager.getUser(TEST_USER);

        assertEquals(expectedUser, retrievedUser);
        assertEquals(expectedUser, retrievedUserChangedById);
        assertTrue(passwordEncoder.matches(NEW_PASSWORD_2, retrievedUser.getPassword()));
        assertTrue(passwordEncoder.matches(NEW_PASSWORD_3, retrievedUserChangedById.getPassword()));
    }

    @Test
    @Order(4)
    void testDeleteUser() throws UserManagementException {
        userManager.deleteUser(TEST_USER);
        var retrievedUser = userManager.userExists(TEST_USER);

        assertFalse(retrievedUser);
    }

    @Test
    void testRetrieveNonExistingUser() {
        assertThrows(UserNotFoundException.class, () -> userManager.getUser(NOT_EXISTENT_USER_NAME));
    }

    @Test
    void testRetrieveNonExistingUserById() {
        assertThrows(UserNotFoundException.class, () -> userManager.getUser(NOT_EXISTING_ID));
    }

    @Test
    void testUpdatePasswordForNonExistingUser() {
        assertThrows(UserNotFoundException.class, () -> userManager.updatePassword(NOT_EXISTENT_USER_NAME, NOT_EXISTENT_USER_NAME));
    }

    @Test
    void testUpdatePasswordForNonExistingUserById() {
        assertThrows(UserNotFoundException.class, () -> userManager.updatePassword(NOT_EXISTING_ID, NOT_EXISTENT_USER_NAME));
    }

    @Test
    void testDeleteNonExistingUser() {
        assertThrows(UserNotFoundException.class, () -> userManager.deleteUser(NOT_EXISTENT_USER_NAME));
    }

    @Test
    void testUpdateNonExistingUser() {
        var user = StandardUser.builder().username(NOT_EXISTENT_USER_NAME).build();
        assertThrows(UserNotFoundException.class, () -> userManager.updateUser(user));
    }
}
