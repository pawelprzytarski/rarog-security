package eu.rarogsoftware.rarog.platform;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureCache
class RarogPlatformServerTests {

    @Test
    void contextLoads() {
    }

}
