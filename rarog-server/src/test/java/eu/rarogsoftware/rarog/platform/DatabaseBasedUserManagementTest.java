package eu.rarogsoftware.rarog.platform;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.api.user.management.UserRole;
import eu.rarogsoftware.rarog.platform.core.security.ApplicationUserManager;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
@Import(TestBeansConfiguration.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DatabaseBasedUserManagementTest {
    @Autowired
    private ApplicationUserManager userDetailsManager;

    @AfterEach
    public void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    void testAdminUserIsPresentInTestData() {
        var expectedUser = StandardUser.builder()
                .id(1L)
                .username("admin")
                .password("$2a$12$eo2HzrWzUQ0zF8dJbuARKOaBlA.tbHGpjn4wy9O0v6LqKjkSpl0B")
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .mfaEnabled(false)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();
        var adminUser = userDetailsManager.loadUserByUsername("admin");

        assertInstanceOf(StandardUser.class, adminUser);

        assertEquals(expectedUser, adminUser);
    }

    @Test
    void testMfaAdminUserIsPresentInTestData() {
        var expectedUser = StandardUser.builder()
                .id(2L)
                .username("mfaadmin")
                .password("$2a$12$yi2jWrreSaOz5vavf7eF.OcclkUubF1gmJEfffNLAe2UorBctaDGi")
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .mfaEnabled(true)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();
        var adminUser = userDetailsManager.loadUserByUsername("mfaadmin");

        assertInstanceOf(StandardUser.class, adminUser);

        assertEquals(expectedUser, adminUser);
    }

    @Test
    @Order(1)
    void testCanAddAndRetrieveUser() {
        var expectedUser = StandardUser.builder()
                .id(5L)
                .username("testUser")
                .password("testPassword")
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .mfaEnabled(true)
                .roles(UserRole.USER, UserRole.ADMINISTRATOR)
                .build();

        var newUser = StandardUser.builder()
                .username("testUser")
                .password("testPassword")
                .enabled(true)
                .mfaEnabled(true)
                .roles(UserRole.USER, UserRole.ADMINISTRATOR)
                .build();

        userDetailsManager.createUser(newUser);
        var retrievedUser = userDetailsManager.loadUserByUsername("testUser");

        assertInstanceOf(StandardUser.class, retrievedUser);

        assertEquals(expectedUser, retrievedUser);
    }

    @Test
    @Order(2)
    void testCanUpdateAndRetrieveUser() {
        var expectedUser = StandardUser.builder()
                .id(5L)
                .username("testUser")
                .password("testPassword")
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(false)
                .mfaEnabled(false)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();

        var updateUser = StandardUser.builder()
                .username("testUser")
                .enabled(false)
                .mfaEnabled(false)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();

        userDetailsManager.updateUser(updateUser);
        var retrievedUser = userDetailsManager.loadUserByUsername("testUser");

        assertInstanceOf(StandardUser.class, retrievedUser);

        assertEquals(expectedUser, retrievedUser);
    }

    @Test
    void testUserExists() {
        assertTrue(userDetailsManager.userExists("admin"));
        assertFalse(userDetailsManager.userExists("nonExistentUser"));
    }

    @Test
    @Order(3)
    void testChangePassword() {
        final var expectedUser = StandardUser.builder()
                .id(5L)
                .username("testUser")
                .password("new")
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(false)
                .mfaEnabled(false)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();

        var securityContext = mock(SecurityContext.class);
        var authentication = mock(Authentication.class);
        SecurityContextHolder.setContext(securityContext);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("testUser");

        userDetailsManager.changePassword("old", "new");
        var retrievedUser = userDetailsManager.loadUserByUsername("testUser");

        assertEquals(expectedUser, retrievedUser);
    }

    @Test
    @Order(4)
    void testUpdatePassword() {
        var expectedUser = StandardUser.builder()
                .id(5L)
                .username("testUser")
                .password("new2")
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(false)
                .mfaEnabled(false)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();

        var updateUser = StandardUser.builder()
                .username("testUser")
                .enabled(false)
                .mfaEnabled(false)
                .roles(UserRole.USER, UserRole.SYSTEM_ADMINISTRATOR)
                .build();

        userDetailsManager.updatePassword(updateUser, "new2");
        var retrievedUser = userDetailsManager.loadUserByUsername("testUser");

        assertEquals(expectedUser, retrievedUser);
    }

    @Test
    @Order(5)
    void testDeleteUser() {
        userDetailsManager.deleteUser("testUser");
        var retrievedUser = userDetailsManager.userExists("testUser");

        assertFalse(retrievedUser);
    }

    @Test
    void testRetrieveNonExistingUser() {
        assertThrows(UsernameNotFoundException.class, () -> userDetailsManager.loadUserByUsername("nonExistentUser"));
    }

    @Test
    void testUpdatePasswordForNonExistingUser() {
        var user = StandardUser.builder().username("nonExistentUser").build();
        assertThrows(UsernameNotFoundException.class, () -> userDetailsManager.updatePassword(user, "nonExistentUser"));
    }

    @Test
    void testChangePasswordForNonExistingUser() {
        var securityContext = mock(SecurityContext.class);
        var authentication = mock(Authentication.class);
        SecurityContextHolder.setContext(securityContext);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("nonExistentUser");

        assertThrows(UsernameNotFoundException.class, () -> userDetailsManager.changePassword("old", "nonExistentUser"));
    }

    @Test
    void testDeleteNonExistingUser() {
        assertThrows(UsernameNotFoundException.class, () -> userDetailsManager.deleteUser("nonExistentUser"));
    }

    @Test
    void testUpdateNonExistingUser() {
        var user = StandardUser.builder().username("nonExistentUser").build();
        assertThrows(UsernameNotFoundException.class, () -> userDetailsManager.updateUser(user));
    }
}
