package eu.rarogsoftware.rarog.platform.plugins;

import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.PluginException;
import eu.rarogsoftware.rarog.platform.core.plugins.osgi.FelixManager;
import eu.rarogsoftware.rarog.platform.core.plugins.osgi.JarPluginArtifact;
import eu.rarogsoftware.rarog.platform.core.plugins.osgi.OsgiPlugin;
import eu.rarogsoftware.rarog.platform.core.plugins.osgi.OsgiPluginManager;
import eu.rarogsoftware.rarog.platform.TestBeansConfiguration;
import eu.rarogsoftware.rarog.platform.core.plugins.PluggableFeatureRegistry;
import org.junit.jupiter.api.*;
import org.osgi.framework.Bundle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

@SpringBootTest
@Import(TestBeansConfiguration.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class OsgiPluginsManagerTest {
    private static final String DISABLE_AUTOCONFIG_PROPERTY = "rarog.autoconfigure.disabled";
    public static final String NOT_ACTIVABLE_PLUGIN_KEY = "eu.rarogsoftware.rarog.platform.test.plugins:notactivable-test-plugin";
    private static final String TEST_PLUGIN_KEY = "eu.rarogsoftware.rarog.platform.test.plugins:test-plugin";
    private static final String FUNC_TEST_PLUGIN_KEY = "eu.rarogsoftware.rarog.platform.plugins:func-test-plugin";
    @Autowired
    private FelixManager felixManager;
    @Autowired
    private OsgiPluginManager pluginsManager;
    @MockBean
    private PluggableFeatureRegistry featureRegistry;
    private Plugin funcTestPlugin;
    private String originalProperty;

    @BeforeEach
    void setUp() {
        originalProperty = System.getProperty("home.directory");
        System.setProperty("home.directory", System.getProperty("user.dir") + "/target/home/");
        System.setProperty(DISABLE_AUTOCONFIG_PROPERTY, "true");
    }

    @AfterEach
    void tearDown() {
        if (originalProperty == null) {
            System.clearProperty("home.directory");
        } else {
            System.setProperty("home.directory", originalProperty);
        }
        System.clearProperty(DISABLE_AUTOCONFIG_PROPERTY);
    }

    @Test
    @Order(1)
    void installSystemPlugins() throws IOException, PluginException {
        felixManager.startSystem();
        var resourceResolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());
        List<Plugin> plugins = new ArrayList<>();
        for(var location : Arrays.asList(
                "classpath:/plugins/backdoor-plugin-*.jar",
                "classpath:/plugins/func-test-plugin-*.jar"
        )) {
            for (Resource resource : resourceResolver.getResources(location)) {
                plugins.add(pluginsManager.installPlugin(new JarPluginArtifact(resource), true));
            }
        }
        for (var plugin : plugins) {
            pluginsManager.enablePlugin(plugin);
        }
        funcTestPlugin = pluginsManager.getPlugin(FUNC_TEST_PLUGIN_KEY);
        assertNotNull(funcTestPlugin, "Func test plugin must be installed for tests");
        assertTrue(funcTestPlugin.isActive(), "Func test plugin must be activated");
    }

    @Test
    @Order(2)
    void stopAndStartFuncTestPlugin() throws PluginException {
        assumeCorrectState();
        pluginsManager.disablePlugin(funcTestPlugin);
        assertFalse(funcTestPlugin.isInUse());
        pluginsManager.enablePlugin(funcTestPlugin);
        assertTrue(funcTestPlugin.isActive());
    }

    private void assumeCorrectState() {
        assumeTrue(felixManager.isStarted(), "Felix is working");
        funcTestPlugin = pluginsManager.getPlugin(FUNC_TEST_PLUGIN_KEY);
        assumeTrue(funcTestPlugin != null, "Func test plugin is installed");
        assumeTrue(funcTestPlugin.isActive(), "Func test plugin is active");
    }

    @Test
    @Order(3)
    void installTestPlugin() throws PluginException, IOException {
        assumeCorrectState();
        var testPluginArtifact = getTestPluginArtifact("test-plugin");
        var plugin = pluginsManager.installPlugin(testPluginArtifact, true);

        assertNotNull(plugin);
        Assertions.assertFalse(plugin.isInUse());
        Assertions.assertEquals(TEST_PLUGIN_KEY, plugin.getKey());
        Assertions.assertEquals(Plugin.PluginState.INSTALLED, plugin.getState());
        Assertions.assertEquals(Bundle.INSTALLED, ((OsgiPlugin) plugin).getBundle().getState());
        Assertions.assertEquals(TEST_PLUGIN_KEY, plugin.getManifest().key());
        Assertions.assertEquals("Rarog Test Plugin", plugin.getManifest().name());
        Assertions.assertEquals("Valid test plugin used for testing. It does nothing!", plugin.getManifest().description());
        Assertions.assertEquals("/broken-padlock.png", plugin.getManifest().image());
        verifyNoInteractions(featureRegistry);
    }

    private JarPluginArtifact getTestPluginArtifact(String pluginName) throws IOException {
        var resourceResolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());
        var resources = resourceResolver.getResources("file:./target/test-plugins/" + pluginName + "-*.jar");
        assertEquals(1, resources.length);
        return new JarPluginArtifact(resources[0]);
    }

    @Test
    @Order(4)
    void enableAndDisableTestPluginWhenEverythingIsEnabled() throws PluginException {
        assumeCorrectState();
        var plugin = pluginsManager.getPlugin(TEST_PLUGIN_KEY);

        Assertions.assertFalse(plugin.isInUse());
        pluginsManager.enablePlugin(plugin);
        assertEquals(Bundle.ACTIVE, ((OsgiPlugin) plugin).getBundle().getState());
        Assertions.assertEquals(Plugin.PluginState.ACTIVE, plugin.getState());
        verify(featureRegistry).plugDescriptors(any(), any());
        Assertions.assertTrue(plugin.isActive());
        pluginsManager.disablePlugin(plugin);
        assertTrue(((OsgiPlugin) plugin).getBundle().getState() == Bundle.RESOLVED || ((OsgiPlugin) plugin).getBundle().getState() == Bundle.INSTALLED);
        Assertions.assertFalse(plugin.isInUse());
        verify(featureRegistry).unplugDescriptors(any(), any());
    }

    @Test
    @Order(5)
    void enableTestPluginWhenFuncTestPluginDisabled() throws PluginException {
        assumeCorrectState();
        var plugin = pluginsManager.getPlugin(TEST_PLUGIN_KEY);

        Assumptions.assumeFalse(plugin.isInUse());
        pluginsManager.disablePlugin(funcTestPlugin);
        assertFalse(funcTestPlugin.isInUse());

        pluginsManager.enablePlugin(plugin);

        assertTrue(funcTestPlugin.isActive());
        Assertions.assertTrue(plugin.isActive());
    }

    @Test
    @Order(6)
    void disableFuncTestPluginDisabledTestPlugin() throws PluginException {
        assumeCorrectState();
        var plugin = pluginsManager.getPlugin(TEST_PLUGIN_KEY);

        pluginsManager.disablePlugin(funcTestPlugin);
        assertFalse(funcTestPlugin.isInUse());
        Assertions.assertFalse(plugin.isInUse());
    }

    @Test
    @Order(7)
    void enableFuncTestPluginDoNotEnableTestPlugin() throws PluginException {
        assumeTrue(felixManager.isStarted(), "Felix is working");
        funcTestPlugin = pluginsManager.getPlugin(FUNC_TEST_PLUGIN_KEY);
        assumeTrue(funcTestPlugin != null, "Func test plugin is installed");

        var plugin = pluginsManager.getPlugin(TEST_PLUGIN_KEY);

        pluginsManager.enablePlugin(funcTestPlugin);
        assertTrue(funcTestPlugin.isInUse());
        Assertions.assertFalse(plugin.isInUse());
    }

    @Test
    @Order(8)
    void reinstallEnabledPlugin() throws IOException, PluginException {
        assumeTrue(felixManager.isStarted(), "Felix is working");

        var testPluginArtifact = getTestPluginArtifact("test-plugin");
        var plugin = pluginsManager.getPlugin(TEST_PLUGIN_KEY);
        pluginsManager.enablePlugin(plugin);
        assertThrows(PluginException.class, () -> pluginsManager.installPlugin(testPluginArtifact, true));
    }

    @Test
    @Order(9)
    void uninstallEnabledPlugin() {
        assumeTrue(felixManager.isStarted(), "Felix is working");

        var plugin = pluginsManager.getPlugin(TEST_PLUGIN_KEY);
        assertThrows(PluginException.class, () -> pluginsManager.uninstall(plugin));
    }

    @Test
    @Order(10)
    void purgePlugin() throws PluginException {
        assumeTrue(felixManager.isStarted(), "Felix is working");

        var plugin = pluginsManager.getPlugin(TEST_PLUGIN_KEY);
        pluginsManager.purgePlugin(plugin);
        verify(featureRegistry).unplugDescriptors(any(), any());
        verify(featureRegistry).purgeDescriptors(any(), any());
    }

    @Test
    @Order(11)
    void uninstallDisabledPlugin() throws PluginException {
        assumeTrue(felixManager.isStarted(), "Felix is working");

        var plugin = pluginsManager.getPlugin(TEST_PLUGIN_KEY);
        pluginsManager.uninstall(plugin);

        assertEquals(Bundle.UNINSTALLED, ((OsgiPlugin) plugin).getBundle().getState());
        Assertions.assertEquals(Plugin.PluginState.UNINSTALLED, plugin.getState());
    }

    @Test
    @Order(50)
    void installNotActivablePlugin() throws PluginException, IOException {
        assumeCorrectState();
        var testPluginArtifact = getTestPluginArtifact("notactivable-test-plugin");
        var plugin = pluginsManager.installPlugin(testPluginArtifact, true);

        assertNotNull(plugin);
        Assertions.assertFalse(plugin.isInUse());
        Assertions.assertEquals(NOT_ACTIVABLE_PLUGIN_KEY, plugin.getKey());
        assertEquals(Bundle.INSTALLED, ((OsgiPlugin) plugin).getBundle().getState());
        Assertions.assertEquals(Plugin.PluginState.INSTALLED, plugin.getState());
        Assertions.assertEquals(NOT_ACTIVABLE_PLUGIN_KEY, plugin.getManifest().key());
        Assertions.assertEquals("Rarog Test Plugin", plugin.getManifest().name());
        Assertions.assertEquals("Valid test plugin used for testing. It does nothing!", plugin.getManifest().description());
        Assertions.assertEquals("/broken-padlock.png", plugin.getManifest().image());
    }

    @Test
    @Order(51)
    void enableNotActivablePlugin() throws PluginException {
        assumeCorrectState();
        var plugin = pluginsManager.getPlugin(NOT_ACTIVABLE_PLUGIN_KEY);
        pluginsManager.enablePlugin(plugin);

        Assertions.assertEquals(Plugin.PluginState.ENABLED, plugin.getState());
        assertEquals(Bundle.ACTIVE, ((OsgiPlugin) plugin).getBundle().getState());
        Assertions.assertTrue(plugin.isInUse());
        Assertions.assertTrue(plugin.isEnabled());
        Assertions.assertFalse(plugin.isActive());
    }

    @Test
    @Order(52)
    void disableNotActivablePlugin() throws PluginException {
        assumeCorrectState();
        var plugin = pluginsManager.getPlugin(NOT_ACTIVABLE_PLUGIN_KEY);
        pluginsManager.disablePlugin(plugin);

        assertTrue(((OsgiPlugin) plugin).getBundle().getState() == Bundle.RESOLVED || ((OsgiPlugin) plugin).getBundle().getState() == Bundle.INSTALLED);
        Assertions.assertFalse(plugin.isInUse());
    }

    @Test
    @Order(53)
    void uninstallNotActivablePlugin() throws PluginException {
        assumeCorrectState();
        var plugin = pluginsManager.getPlugin(NOT_ACTIVABLE_PLUGIN_KEY);
        pluginsManager.uninstall(plugin);

        assertEquals(Bundle.UNINSTALLED, ((OsgiPlugin) plugin).getBundle().getState());
        Assertions.assertEquals(Plugin.PluginState.UNINSTALLED, plugin.getState());
    }

    @Test
    @Order(60)
    void installNotRarogPlugin() throws IOException {
        assumeCorrectState();

        var testPluginArtifact = getTestPluginArtifact("not-a-plugin");
        assertThrows(PluginException.class, () -> pluginsManager.installPlugin(testPluginArtifact, true));
    }

    @Test
    @Order(99)
    void disablePluginSystem() {
        assumeTrue(felixManager.isStarted());
        pluginsManager.shutdownPlugins();
        felixManager.stopSystem();
        assertFalse(felixManager.isStarted());
    }

}
