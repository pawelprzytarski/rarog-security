package eu.rarogsoftware.rarog.platform;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;
import eu.rarogsoftware.rarog.platform.security.mfa.recovery.RecoveryCodesStore;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Import(TestBeansConfiguration.class)
@AutoConfigureMockMvc
@AutoConfigureCache
class MfaRecoveryCodeLoginResourceTests {
    private final long USER_ID = 2L;
    private final StandardUser APPLICATION_USER = StandardUser.builder().id(USER_ID).build();
    @Autowired
    RecoveryCodesStore recoveryCodesStore;
    @Autowired
    MockMvc mockMvc;

    @Test
    @WithUserDetails("mfaadmin")
    void testCorrectCodeAndRegeneration() throws Exception {
        var originalCodes = Arrays.asList("test1", "test2", "test3", "test4");
        recoveryCodesStore.replaceCodes(APPLICATION_USER, originalCodes);

        var response = mockMvc.perform(MockMvcRequestBuilders
                        .post("/login/mfa/recovery-codes")
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"recoveryCode\": \"test1\"}"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        var codesCount = recoveryCodesStore.getCodesCount(APPLICATION_USER);

        assertEquals(20, codesCount);

        JsonNode root = new ObjectMapper().readTree(response.getBytes(StandardCharsets.UTF_8));
        assertTrue(root.get("success").asBoolean());
        assertTrue(root.get("regenerated").asBoolean());
        JsonNode newCodes = root.get("newCodes");
        assertEquals(20, newCodes.size());

        String generatedCode = newCodes.get(0).asText();

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/login/mfa/recovery-codes")
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"recoveryCode\": \"" + generatedCode + "\"}"))
                .andExpect(status().isOk());

        var updatedCodesCount = recoveryCodesStore.getCodesCount(APPLICATION_USER);
        assertEquals(19, updatedCodesCount);

        var newCodesSet = new HashSet<String>();
        newCodes.elements().forEachRemaining(code -> newCodesSet.add(code.asText()));
        assertTrue(originalCodes.stream().noneMatch(newCodesSet::contains));
    }

    @Test
    @WithUserDetails("mfaadmin")
    void testIncorrectCode() throws Exception {
        var originalCodes = Arrays.asList("test1", "test2", "test3", "test4");
        recoveryCodesStore.replaceCodes(APPLICATION_USER, originalCodes);

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/login/mfa/recovery-codes")
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"recoveryCode\": \"incorrectCode\"}"))
                .andExpect(status().isBadRequest());

        var codesCount = recoveryCodesStore.getCodesCount(APPLICATION_USER);

        assertEquals(originalCodes.size(), codesCount);
    }

    @Test
    @WithUserDetails("mfaadmin")
    void testCorrectCodeReuse() throws Exception {
        var originalCodes = Arrays.asList("test1", "test2", "test3", "test4");
        recoveryCodesStore.replaceCodes(APPLICATION_USER, originalCodes);

        var response = mockMvc.perform(MockMvcRequestBuilders
                        .post("/login/mfa/recovery-codes")
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"recoveryCode\": \"test1\"}"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/login/mfa/recovery-codes")
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"recoveryCode\": \"test1\"}"))
                .andExpect(status().isBadRequest());


        JsonNode root = new ObjectMapper().readTree(response.getBytes(StandardCharsets.UTF_8));
        assertTrue(root.get("success").asBoolean());
        assertTrue(root.get("regenerated").asBoolean());
        JsonNode newCodes = root.get("newCodes");
        assertEquals(20, newCodes.size());

        String generatedCode = newCodes.get(0).asText();
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/login/mfa/recovery-codes")
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"recoveryCode\": \"" + generatedCode + "\"}"))
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/login/mfa/recovery-codes")
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"recoveryCode\": \"" + generatedCode + "\"}"))
                .andExpect(status().isBadRequest());
    }
}
