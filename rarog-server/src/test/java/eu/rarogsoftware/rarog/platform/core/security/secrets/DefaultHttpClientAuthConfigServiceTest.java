package eu.rarogsoftware.rarog.platform.core.security.secrets;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.tls.HandshakeCertificates;
import okhttp3.tls.HeldCertificate;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@ExtendWith(MockitoExtension.class)
class DefaultHttpClientAuthConfigServiceTest {
    @Mock
    BuildInSecretsStorage buildInSecretsStorage;
    @InjectMocks
    DefaultHttpClientAuthConfigService authConfigService;
    SSLContext originalContext;

    @BeforeEach
    void setUp() throws NoSuchAlgorithmException {
        originalContext = SSLContext.getDefault();
    }

    @AfterEach
    void tearDown() {
        SSLContext.setDefault(originalContext);
    }

    @Test
    void testNoAuthSendCorrectData() throws IOException, InterruptedException {
        try (var mockServer = new MockWebServer()) {
            configureServerCertificate(mockServer);
            mockServer.enqueue(new MockResponse().setResponseCode(HttpStatus.OK.value()).setBody("OK"));
            mockServer.start();
            var baseUrl = mockServer.url("/test");
            var builder = HttpClient.newBuilder();

            var authConfig = authConfigService.resolveFromString("");

            var client = authConfig.configureHttpClient(builder).build();

            var response = client.send(authConfig.configureHttpRequest(HttpRequest.newBuilder()
                            .uri(baseUrl.uri())
                            .build()),
                    HttpResponse.BodyHandlers.ofString());

            assertThat(response.statusCode()).isEqualTo(HttpStatus.OK.value());
            assertThat(response.body()).isEqualTo("OK");

            var receivedRequest = mockServer.takeRequest();
            assertThat(receivedRequest.getHandshake()).isNotNull();
        }
    }

    @Test
    void testAuthHeaderSendsCorrectHeader() throws IOException, InterruptedException {
        try (var mockServer = new MockWebServer()) {
            configureServerCertificate(mockServer);
            mockServer.enqueue(new MockResponse().setResponseCode(HttpStatus.OK.value()).setBody("OK"));
            mockServer.start();
            var baseUrl = mockServer.url("/test");
            var builder = HttpClient.newBuilder();

            var authConfig = authConfigService.resolveFromString("auth:Test token");

            var client = authConfig.configureHttpClient(builder).build();

            var response = client.send(authConfig.configureHttpRequest(HttpRequest.newBuilder()
                            .uri(baseUrl.uri())
                            .build()),
                    HttpResponse.BodyHandlers.ofString());

            assertThat(response.statusCode()).isEqualTo(HttpStatus.OK.value());
            assertThat(response.body()).isEqualTo("OK");

            var receivedRequest = mockServer.takeRequest();
            assertThat(receivedRequest.getHandshake()).isNotNull();
            assertThat(receivedRequest.getHeader(HttpHeaders.AUTHORIZATION)).isEqualTo("Test token");
        }
    }

    @Test
    void testMutualTlsAuthReadDataCorrectlyFromFiles() throws IOException, InterruptedException {
        try (var mockServer = new MockWebServer();
             var rootStream = getClass().getResourceAsStream("/test_certs/myca.crt");
             var keyStream = getClass().getResourceAsStream("/test_certs/client.key.nopass")) {

            var rootCertificate = HeldCertificate.decode(new String(rootStream.readAllBytes(), StandardCharsets.UTF_8)
                    + new String(keyStream.readAllBytes(), StandardCharsets.UTF_8));
            var localhostCertificate = new HeldCertificate.Builder()
                    .addSubjectAlternativeName("localhost")
                    .build();
            var serverCertificates = new HandshakeCertificates.Builder()
                    .heldCertificate(localhostCertificate)
                    .addPlatformTrustedCertificates()
                    .addTrustedCertificate(rootCertificate.certificate())
                    .build();
            mockServer.useHttps(serverCertificates.sslSocketFactory(), false);
            mockServer.enqueue(new MockResponse().setResponseCode(HttpStatus.OK.value()).setBody("OK"));
            mockServer.requestClientAuth();
            mockServer.start();
            var baseUrl = mockServer.url("/test");
            var builder = HttpClient.newBuilder();

            SSLContext.setDefault(new HandshakeCertificates.Builder()
                    .addTrustedCertificate(localhostCertificate.certificate())
                    .build().sslContext());
            var authConfig = authConfigService.resolveFromString("client_cert:cert=classpath:/test_certs/client.crt,privkey=classpath:/test_certs/client.key.nopass");

            ((SslContextConfig) authConfig).addTrustedCertificate(localhostCertificate.certificate()); // because trick with sslcontext don't work here
            ((SslContextConfig) authConfig).addTrustedCertificate(rootCertificate.certificate());
            var client = authConfig.configureHttpClient(builder).build();

            var response = client.send(authConfig.configureHttpRequest(HttpRequest.newBuilder()
                            .uri(baseUrl.uri())
                            .build()),
                    HttpResponse.BodyHandlers.ofString());

            assertThat(response.statusCode()).isEqualTo(HttpStatus.OK.value());
            assertThat(response.body()).isEqualTo("OK");

            var receivedRequest = mockServer.takeRequest();
            assertThat(receivedRequest.getHandshake()).isNotNull();
            assertThat(receivedRequest.getHandshake().peerPrincipal().getName()).isEqualTo("CN=client,OU=Tests,O=Do not use in production,L=Tests,ST=State,C=PL");
        }
    }

    @Test
    void testMutualTlsAuthReadDataFromBytes() throws IOException, InterruptedException {
        try (var mockServer = new MockWebServer()) {
            var rootCertificate = new HeldCertificate.Builder()
                    .certificateAuthority(1)
                    .build();
            var localhostCertificate = new HeldCertificate.Builder()
                    .addSubjectAlternativeName("localhost")
                    .signedBy(rootCertificate)
                    .build();
            var serverCertificates = new HandshakeCertificates.Builder()
                    .heldCertificate(localhostCertificate)
                    .addPlatformTrustedCertificates()
                    .addTrustedCertificate(rootCertificate.certificate())
                    .build();
            var clientCertificate = new HeldCertificate.Builder()
                    .commonName("client")
                    .signedBy(rootCertificate)
                    .build();
            mockServer.useHttps(serverCertificates.sslSocketFactory(), false);
            mockServer.enqueue(new MockResponse().setResponseCode(HttpStatus.OK.value()).setBody("OK"));
            mockServer.requireClientAuth();
            mockServer.start();
            var baseUrl = mockServer.url("/test");
            var builder = HttpClient.newBuilder();

            var authConfig = authConfigService.resolveFromSettings(Map.of(
                    "method", "client_cert",
                    "cert_bytes", clientCertificate.certificatePem().getBytes(StandardCharsets.UTF_8),
                    "privkey_bytes", clientCertificate.privateKeyPkcs8Pem().getBytes(StandardCharsets.UTF_8)
            ));
            ((SslContextConfig) authConfig).addTrustedCertificate(rootCertificate.certificate()); // because trick with sslcontext don't work here

            var client = authConfig.configureHttpClient(builder).build();

            var response = client.send(authConfig.configureHttpRequest(HttpRequest.newBuilder()
                            .uri(baseUrl.uri())
                            .build()),
                    HttpResponse.BodyHandlers.ofString());

            assertThat(response.statusCode()).isEqualTo(HttpStatus.OK.value());
            assertThat(response.body()).isEqualTo("OK");

            var receivedRequest = mockServer.takeRequest();
            assertThat(receivedRequest.getHandshake()).isNotNull();
            assertThat(receivedRequest.getHandshake().peerPrincipal().getName()).isEqualTo("CN=client");
        }
    }

    private static void configureServerCertificate(MockWebServer mockServer) throws IOException {
        var localhostCertificate = new HeldCertificate.Builder()
                .addSubjectAlternativeName("localhost")
                .build();
        var serverCertificates = new HandshakeCertificates.Builder()
                .heldCertificate(localhostCertificate)
                .addPlatformTrustedCertificates()
                .build();
        mockServer.useHttps(serverCertificates.sslSocketFactory(), false);
        SSLContext.setDefault(new HandshakeCertificates.Builder()
                .addTrustedCertificate(localhostCertificate.certificate())
                .build().sslContext());
    }
}