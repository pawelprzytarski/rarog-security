package eu.rarogsoftware.rarog.platform.core.plugins;

import eu.rarogsoftware.rarog.platform.TestBeansConfiguration;
import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import static eu.rarogsoftware.rarog.platform.api.plugins.Plugin.PluginState.ENABLED;
import static eu.rarogsoftware.rarog.platform.api.plugins.Plugin.PluginState.INSTALLED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@SpringBootTest
@Import(TestBeansConfiguration.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith({MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
class DbBackedPluginStateStoreTest {
    private static final String TEST_PLUGIN_KEY = "test.rarog.plugin";
    private static final String TEST_PLUGIN_KEY_2 = "test.rarog.plugin2";

    @Mock
    private Plugin testPlugin;
    @Mock
    private Plugin testPlugin2;

    @Autowired
    private DbBackedPluginStateStore store;

    @BeforeEach
    void setup() {
        when(testPlugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        when(testPlugin2.getKey()).thenReturn(TEST_PLUGIN_KEY_2);
    }

    @Test
    @Order(1)
    void testNewValueIsSaved() {
        store.storePluginState(testPlugin, INSTALLED);

        assertThat(store.getPluginState(testPlugin)).isPresent().contains(INSTALLED);
    }

    @Test
    @Order(2)
    void testNotYetExistingValueReturnsEmptyOptional() {
        assertThat(store.getPluginState(testPlugin2)).isEmpty();
    }

    @Test
    @Order(3)
    void testExistingValueIsUpdated() {
        store.storePluginState(testPlugin, ENABLED);

        assertThat(store.getPluginState(testPlugin)).isPresent().contains(ENABLED);
    }

    @Test
    @Order(4)
    void testStoreNullPluginKeyThrows() {
        when(testPlugin.getKey()).thenReturn(null);
        assertThatThrownBy(() -> store.storePluginState(testPlugin, INSTALLED))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    @Order(5)
    void testGetNullPluginKeyThrows() {
        when(testPlugin.getKey()).thenReturn(null);
        assertThatThrownBy(() -> store.getPluginState(testPlugin))
                .isInstanceOf(IllegalArgumentException.class);
    }
}