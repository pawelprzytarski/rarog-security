package eu.rarogsoftware.rarog.platform.metrics;

import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.TestBeansConfiguration;
import eu.rarogsoftware.rarog.platform.api.metrics.*;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.core.metrics.SwitchingMetricsService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static eu.rarogsoftware.rarog.platform.api.settings.SystemSettingKeys.METRICS_ENABLED_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.lenient;

@SpringBootTest
@Import(TestBeansConfiguration.class)
@AutoConfigureCache
class MetricsServiceTest {
    @Mock
    ApplicationSettings applicationSettings;
    @Autowired
    MetricsService metricsService;

    @BeforeEach
    void setUp() {
        lenient().when(applicationSettings.getSetting(METRICS_ENABLED_KEY, Boolean.class)).thenReturn(Optional.of(true));
        ((SwitchingMetricsService) metricsService).resetMetrics();
    }

    @AfterEach
    void tearDown() {
        ((SwitchingMetricsService) metricsService).resetMetrics();
    }

    @Test
    void noMetricsServiceHasEmptySnapshot() {
        assertThat(metricsService.snapshot().samples(), empty());
    }

    @Test
    void allMetricsAreInNextSnapshotAfterClosing() {
        var counter = metricsService.createCounter(MetricSettings.settings().name("TestCounter"));
        counter.increment();
        var gauge = metricsService.createGauge(MetricSettings.settings().name("TestGauge"));
        gauge.increment();

        var snapshot = metricsService.snapshot();

        assertThat(snapshot.timestamp(), greaterThan(0L));
        assertThat(snapshot.samples().size(), equalTo(2));

        metricsService.closeMetric(counter);
        var snapshotAfterClose = metricsService.snapshot();
        assertThat(snapshotAfterClose.timestamp(), greaterThan(0L));
        assertThat(snapshotAfterClose.samples().size(), equalTo(2));

        var nextSnapshotAfterClose = metricsService.snapshot();
        assertThat(nextSnapshotAfterClose.timestamp(), greaterThan(0L));
        assertThat(nextSnapshotAfterClose.samples().size(), equalTo(1));
    }

    @Test
    void allMetricsAreInEachSampleAfterClosing() {
        var counter = metricsService.createCounter(MetricSettings.settings().name("TestCounter"));
        counter.increment();
        var gauge = metricsService.createGauge(MetricSettings.settings().name("TestGauge"));
        gauge.increment();

        var snapshot = metricsService.sample();

        assertThat(snapshot.size(), equalTo(2));

        metricsService.closeMetric(counter);
        metricsService.sample();
        var snapshotAfterClose = metricsService.sample();
        assertThat(snapshotAfterClose.size(), equalTo(2));
    }

    @Test
    void counterGeneratesTotalMetric() {
        var settings = MetricSettings.settings().name("TestCounter").labels("Test");
        var counter = metricsService.createCounter(settings);
        counter.labels("1").increment();
        counter.labels("2").increase(3);

        var snapshot = metricsService.snapshot();
        var sampleResult = metricsService.sample();

        var snapshotSample = snapshot.samples().stream().findAny().get();
        var sampleSample = sampleResult.stream().findAny().get();
        var counterSnapshotSample = snapshotSample.samples().stream().filter(sample -> !sample.name().endsWith("_created")).toList(); // prometheus add these *_created but we don't care
        var counterSampleSample = sampleSample.samples().stream().filter(sample -> !sample.name().endsWith("_created")).toList();

        assertThat(counterSnapshotSample.size(), equalTo(2));
        assertThat(snapshotSample.settings(), equalTo(MetricSettings.settings().name("TestCounter")));
        assertThat(snapshotSample.type(), equalTo(MetricSamples.Type.COUNTER));
        assertThat(counterSnapshotSample, Matchers.hasItem(new Sample("TestCounter_total", Collections.singletonList("Test"), Collections.singletonList("1"), 1.0)));
        assertThat(counterSnapshotSample, hasItem(new Sample("TestCounter_total", Collections.singletonList("Test"), Collections.singletonList("2"), 3.0)));

        assertThat("Sample and snapshot are equal", counterSampleSample, containsInAnyOrder(counterSampleSample.toArray(Sample[]::new)));
    }

    @Test
    void gaugeGenerateUnitMetric() {
        var settings = MetricSettings.settings().name("TestGauge").labels("Test").unit("unit");
        var gauge = metricsService.createGauge(settings);
        gauge.labels("1").increment();
        gauge.labels("2").increase(3.0);
        gauge.labels("2").decrement();

        var snapshot = metricsService.snapshot();
        var sampleResult = metricsService.sample();

        var snapshotSample = snapshot.samples().stream().findAny().get();
        var sampleSample = sampleResult.stream().findAny().get();
        var innerSnapshotSample = snapshotSample.samples().stream().filter(sample -> !sample.name().endsWith("_created")).toList(); // prometheus add these *_created but we don't care
        var innerSampleSample = sampleSample.samples().stream().filter(sample -> !sample.name().endsWith("_created")).toList();

        assertThat(innerSnapshotSample.size(), equalTo(2));
        assertThat(snapshotSample.type(), equalTo(MetricSamples.Type.GAUGE));
        assertThat(snapshotSample.settings(), equalTo(MetricSettings.settings().name("TestGauge_unit").description("TestGauge").unit("unit")));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestGauge_unit", Collections.singletonList("Test"), Collections.singletonList("1"), 1.0)));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestGauge_unit", Collections.singletonList("Test"), Collections.singletonList("2"), 2.0)));

        assertThat("Sample and snapshot are equal", innerSampleSample, containsInAnyOrder(innerSampleSample.toArray(Sample[]::new)));
    }

    @Test
    void infoGenerateInfoMetric() {
        var settings = MetricSettings.settings().name("TestInfo").labels("Test");
        var info = metricsService.createInfo(settings);
        info.labels("1").info("Value", "1");
        info.labels("2").info("Value", "1");

        var snapshot = metricsService.snapshot();
        var sampleResult = metricsService.sample();

        var snapshotSample = snapshot.samples().stream().findAny().get();
        var sampleSample = sampleResult.stream().findAny().get();
        var innerSnapshotSample = snapshotSample.samples().stream().filter(sample -> !sample.name().endsWith("_created")).toList(); // prometheus add these *_created but we don't care
        var innerSampleSample = sampleSample.samples().stream().filter(sample -> !sample.name().endsWith("_created")).toList();

        assertThat(innerSnapshotSample.size(), equalTo(2));
        assertThat(snapshotSample.type(), equalTo(MetricSamples.Type.INFO));
        assertThat(snapshotSample.settings(), equalTo(MetricSettings.settings().name("TestInfo")));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestInfo_info", Arrays.asList("Test", "Value"), Arrays.asList("1", "1"), 1.0)));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestInfo_info", Arrays.asList("Test", "Value"), Arrays.asList("2", "1"), 1.0)));

        assertThat("Sample and snapshot are equal", innerSampleSample, containsInAnyOrder(innerSampleSample.toArray(Sample[]::new)));
    }

    @Test
    void histogramGenerateMetric() {
        var settings = HistogramSettings.settings().name("TestHistogram").labels("Test").buckets(1, 2, 3);
        var gauge = metricsService.createHistogram(settings);
        gauge.labels("1").observe(2);
        gauge.labels("2").observe(2);

        var snapshot = metricsService.snapshot();
        var sampleResult = metricsService.sample();

        var snapshotSample = snapshot.samples().stream().findAny().get();
        var sampleSample = sampleResult.stream().findAny().get();
        var innerSnapshotSample = snapshotSample.samples().stream().filter(sample -> !sample.name().endsWith("_created")).toList(); // prometheus add these *_created but we don't care
        var innerSampleSample = sampleSample.samples().stream().filter(sample -> !sample.name().endsWith("_created")).toList();

        assertThat(innerSnapshotSample.size(), equalTo(12));
        assertThat(snapshotSample.type(), equalTo(MetricSamples.Type.HISTOGRAM));
        assertThat(snapshotSample.settings(), equalTo(MetricSettings.settings().name("TestHistogram")));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestHistogram_bucket", Arrays.asList("Test", "le"), Arrays.asList("1", "1.0"), 0.0)));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestHistogram_bucket", Arrays.asList("Test", "le"), Arrays.asList("2", "1.0"), 0.0)));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestHistogram_bucket", Arrays.asList("Test", "le"), Arrays.asList("1", "2.0"), 1.0)));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestHistogram_bucket", Arrays.asList("Test", "le"), Arrays.asList("2", "2.0"), 1.0)));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestHistogram_bucket", Arrays.asList("Test", "le"), Arrays.asList("1", "3.0"), 1.0)));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestHistogram_bucket", Arrays.asList("Test", "le"), Arrays.asList("2", "3.0"), 1.0)));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestHistogram_bucket", Arrays.asList("Test", "le"), Arrays.asList("1", "+Inf"), 1.0)));
        assertThat(innerSnapshotSample, hasItem(new Sample("TestHistogram_bucket", Arrays.asList("Test", "le"), Arrays.asList("2", "+Inf"), 1.0)));

        assertThat("Sample and snapshot are equal", innerSampleSample, containsInAnyOrder(innerSampleSample.toArray(Sample[]::new)));
    }

}