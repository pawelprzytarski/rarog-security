package eu.rarogsoftware.rarog.platform;


import eu.rarogsoftware.commons.test.utils.RestoreTool;
import eu.rarogsoftware.commons.utils.DateService;
import eu.rarogsoftware.commons.utils.FixedTimeDateService;
import eu.rarogsoftware.rarog.platform.app.configuration.StandardConfiguration;
import eu.rarogsoftware.rarog.platform.api.settings.ApplicationSettings;
import eu.rarogsoftware.rarog.platform.api.settings.SettingsInitializer;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.util.List;

@TestConfiguration
public class TestBeansConfiguration {
    @Value("${project.backups.directory:classpath:/backups/}")
    private String backupsBaseDir;
    @Value("${project.migrations.directory:classpath:db/rarog/platform/migrations/}")
    private String migrationsDirectory;

    @Bean
    public InitializingBean settingInitializer(List<SettingsInitializer> initializers, ApplicationSettings settings) {
        return () -> initializers.forEach(initializer -> initializer.initialize(settings));
    }

    @Bean
    @Scope("singleton")
    public DateService dateService(FixedTimeDateService fixedTimeDateService) {
        return fixedTimeDateService;
    }

    @Bean
    @Scope("singleton")
    public FixedTimeDateService fixedTimeDateService(Environment env) {
        return new FixedTimeDateService();
    }

    @Bean
    public RestoreTool restoreTool(DataSource dataSource) {
        RestoreTool restoreTool = new RestoreTool(() -> DataSourceUtils.getConnection(dataSource));
        restoreTool.setBaseDirectory(backupsBaseDir);
        restoreTool.setMigrationsDirectory(migrationsDirectory);
        restoreTool.setTestMigrationsDirectory(migrationsDirectory.replace("/main/", "/test/"));
        return restoreTool;
    }

    @Bean
    @Primary
    public SpringLiquibase liquibase(DataSource dataSource, ResourceLoader resourceLoader) {
        SpringLiquibase liquibase = StandardConfiguration.getBaseSpringLiquibase(dataSource);
        if (resourceLoader.getResource("%stestChangelog.xml".formatted(migrationsDirectory)).exists()) {
            liquibase.setChangeLog("%stestChangelog.xml".formatted(migrationsDirectory));
        } else {
            liquibase.setChangeLog("%schangelog.xml".formatted(migrationsDirectory));
        }
        liquibase.setDropFirst(true);
        return liquibase;
    }
}
