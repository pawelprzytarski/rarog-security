package eu.rarogsoftware.rarog.platform.app.rest;

import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.UiFragment;
import eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui.UiFragmentsDescriptor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.lenient;


@ExtendWith(MockitoExtension.class)
class UiFragmentsControllerTest {
    @Mock
    Plugin plugin1;
    @Mock
    Plugin plugin2;
    private UiFragmentsController uiFragmentsController;

    @BeforeEach
    void setUp() {
        lenient().when(plugin1.getKey()).thenReturn("plugin1");
        lenient().when(plugin2.getKey()).thenReturn("plugin2");
        uiFragmentsController = new UiFragmentsController();
    }

    @Test
    void testReturnsFragmentsInCorrectOrder() {
        uiFragmentsController.plugDescriptor(plugin1, new UiFragmentsDescriptor(
                List.of(
                        new UiFragment("key1", "test", "resource1", 10),
                        new UiFragment("key2", "test", "resource2", 5)
                )
        ));
        uiFragmentsController.plugDescriptor(plugin2, new UiFragmentsDescriptor(
                List.of(
                        new UiFragment("key3", "test", "resource3", 0)
                )
        ));

        var result = uiFragmentsController.getFragments("test");

        assertThat(result).containsExactlyInAnyOrder(
                new UiFragmentsController.Fragment("key3", "resource3"),
                new UiFragmentsController.Fragment("key2", "resource2"),
                new UiFragmentsController.Fragment("key1", "resource1")
        );
    }

    @Test
    void testReturnsFragmentsForCorrectPlacement() {
        uiFragmentsController.plugDescriptor(plugin1, new UiFragmentsDescriptor(
                List.of(
                        new UiFragment("key1", "test1", "resource1", 10),
                        new UiFragment("key2", "test1", "resource2", 5)
                )
        ));
        uiFragmentsController.plugDescriptor(plugin2, new UiFragmentsDescriptor(
                List.of(
                        new UiFragment("key3", "test2", "resource3", 0)
                )
        ));

        var result = uiFragmentsController.getFragments("test2");

        assertThat(result).containsExactlyInAnyOrder(
                new UiFragmentsController.Fragment("key3", "resource3")
        );
    }

    @Test
    void testFragmentsCacheIsUpdatedOnNewPlugin() {
        uiFragmentsController.plugDescriptor(plugin1, new UiFragmentsDescriptor(
                List.of(
                        new UiFragment("key1", "test", "resource1", 10),
                        new UiFragment("key2", "test", "resource2", 5)
                )
        ));

        var result1 = uiFragmentsController.getFragments("test");

        uiFragmentsController.plugDescriptor(plugin2, new UiFragmentsDescriptor(
                List.of(
                        new UiFragment("key3", "test", "resource3", 0)
                )
        ));

        var result2 = uiFragmentsController.getFragments("test");

        assertThat(result1).containsExactlyInAnyOrder(
                new UiFragmentsController.Fragment("key2", "resource2"),
                new UiFragmentsController.Fragment("key1", "resource1")
        );

        assertThat(result2).containsExactlyInAnyOrder(
                new UiFragmentsController.Fragment("key3", "resource3"),
                new UiFragmentsController.Fragment("key2", "resource2"),
                new UiFragmentsController.Fragment("key1", "resource1")
        );
    }
}