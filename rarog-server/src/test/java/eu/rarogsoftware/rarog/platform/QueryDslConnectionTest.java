package eu.rarogsoftware.rarog.platform;

import eu.rarogsoftware.commons.database.connection.DatabaseConnectionProvider;
import ut.rarogsoftware.rarog.platform.db.QTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@Import(TestBeansConfiguration.class)
class QueryDslConnectionTest {
    @Autowired
    private DatabaseConnectionProvider databaseConnectionProvider;

    @Test
    void testQueryDslConnection() {
        Long result = databaseConnectionProvider.getConnection(query -> {
            query.insert(QTest.test1)
                    .set(QTest.test1.id, 1)
                    .set(QTest.test1.name, "Test")
                    .set(QTest.test1.test, "Test")
                    .execute();

            return query
                    .from(QTest.test1)
                    .select(QTest.test1.count())
                    .fetchFirst();
        });
        assertEquals(1L, result);
    }
}
