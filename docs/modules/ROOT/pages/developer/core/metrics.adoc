= Metrics

A software metric is a measure of software characteristics that are quantifiable or countable.
Like performance, users way of using it, etc. This information is useful for admins and other developers to understand how application is behaving and if there are any actions that need to be taken, to improve it or prevent some potential harmful situations. That's why software should have meaningful metrics.

Rarog Platform comes with ready to use API for metrics, that have minimal performance impact. You can use this API in all OSGi based plugins.

== Usage code example

[source,java]
----


class ExampleClass {
    Counter invocationCounter;
    Histogram durationHistogram;
    
    ExampleClass(MetricsService metricsService,
                                         AdvancedScheduledExecutorService delegate) {
        this.invocationCounter = metricsService.createCounter(MetricSettings.settings()
                .name("example_counter")
                .description("Example of counter")
        );
        this.durationHistogram = metricsService.createHistogram(HistogramSettings.settings()
                .name("example_histogram")
                .description("Example of histogram")
                .buckets(0.001, 0.01, 0.1, 1, 10, 60, 600, 3600)
        );
    }

    void method(long repeatCount) {
        invocationCounter.increment();
        try (var ignored = durationHistogram.startTimer()) {
            for(int i = 0; i <repeatCount;i++) {
                // do something here
            }
        } catch (Exception e) {
            //ignore because thrown by timer, or log it
        }
    }
}
----

link:https://gitlab.com/rarogsoftware/rarog-platform/-/blob/master/plugins/telemetry-plugin/src/main/java/eu/rarogsoftware/rarog/security/plugins/telemetry/SnapshotService.java#L57[Live example]

== Usage in plugins

. Add dependency on eu.rarogsoftware.rarog.platform:rarog-platform-api to your plugin.
. Make sure package eu.rarogsoftware.rarog.platform.api.metrics is imported to your plugin by OSGi (happens automatically, when using default configuration, otherwise you need to update configuration of your maven-bundle-plugin).
. Add eu.rarogsoftware.rarog.platform.api.metrics.MetricsService to your component and import it from OSGi (use @ComponentImport on constructor parameter or import it manually using OSGi API)
. Use MetricsService to create new instance of you metric.
You can read javadoc to learn more about each metric.
. Use metric objects to monitor some actions (For example Counter#increment).
. Enable OpenMetrics page. See xref:metrics.adoc[metrics reference] for info how do it.
. Access ``<base_url>/rest/telemetry/1.0/expose/openmetrics/`` and look for your metric.
