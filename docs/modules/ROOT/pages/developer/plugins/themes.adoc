= Themes support

== Rarog themes

Rarog comes with premade page layouts and colors schemes, but developer can use its own themes if default one is not sufficient or not to liking.
Themes are built using Thymeleaf templates.

Each page import template fragments form theme template located under location: `theme/<type>.html` (example: `theme/genericpage.html`).
So by simply providing templates under these locations, each plugin can provide its own set of theme fragments.

Each theme template should consist of following fragments:

* `headblock` - `<head>` tag of html page to import to final page.
It should include all necessary css and js imports.
* `themeHeader` - can be any tag, which will be put before page content.
Usually it is `<header>` or `<nav>` tag for including common navigation/header panel on each page.
* `themeFooter` - can be any tag, which will be put after page content.
Usually it is `<footer>` tag for page common footer.

These fragments are necessary for each theme template.
Template may consist of more fragments, but they will not be imported by Rarog core pages.

=== Color scheme

Color scheme is intended to be implemented by theme templates.
Rarog pages use Bootstrap for UI components, which comes with https://getbootstrap.com/docs/5.3/customize/color-modes/[build-in support for color modes].
So all themes should keep compatiblity this mechanism by defining currrent color scheme using `data-bs-theme` on `html` tag.

TIP: Default Rarog theme comes with build-in dark mode support.
