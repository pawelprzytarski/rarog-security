# Rarog Platform
Rarog Platform - Open Source framework and platform for highly customizable application build for single server and small DC environments like home hosted servers, small company Data Centre.  

[Rarog](https://en.wikipedia.org/wiki/Rar%C3%B3g) -  a fire demon, often depicted as a fiery falcon. In Polish folklore, the rarog is a tiny bird that can be held in a pocket, and can bring people happiness.

# Usage
Rarog Platform is still in early stage of development, but you can start using it, we host all artifacts on Gitlab repositories. 
List of all artifact is available [here](https://gitlab.com/groups/rarogsoftware/-/packages)
Docker image of server is available under name `rarogsoftware/rarog-platform`.
When proper installator will be available we will share more details on usage.

# Open Source state
Project is delivered on Apache 2.0 license. It is also open for 3rd party contributions. But maintainers team 
(referred to as Rarog Team) took some decisions in this matter.
1. Thanks to nature of project, most of the contributions can be delivered in form of plugins and can be delivered independently. 
This kind of contribution do not require approval from the Rarog Team. You can freely deliver your plugins to all interested parties.
This contrition type will be referred to as Community Contribution.
2. Contributions to Rarog Platform must be approved by Rarog Team, who is responsible for keep architectural
consistency and high security level of the project. This contribution type will be referred to as Core Contribution. 
3. Not approved core contributions will not be accepted to the code base. If you want to contribute please reach our [Discord server](https://discord.gg/qQ6NpHG57t). 

## Support
If you are interested in supporting project, but you don't want or cannot contribute to project, you can cheer us on our [Discord server](https://discord.gg/qQ6NpHG57t).
Due to legal reasons and unstable schedule (we all still have normal jobs) we are not accepting donations. If you really 
want fund us beer or nice dinner you can reach us on Discord. We will work something out 😉 

If you want provide more material support like: server availability for documentation hosting, tools for prototyping, etc.
Please reach us on [Discord server](https://discord.gg/qQ6NpHG57t). We will talk about details with you.

## Documentation
You can visit [reference docs](https://docs.rarogsoftware.eu/), [javadocs](https://javadoc.rarogsoftware.eu/) for version specific documentation. Cross version documentation, news and blog are available at [our wiki](https://wiki.rarogsoftware.eu)

# Architecture
Rarog Platform is open source platform for single server and small data centers configurations (home DC, small-mid size companies), that is focused on configurability, extendability and security. It allows to develop, install and manage apps of any purpose to create system crafted specifically for user need.
Thanks to liberal Apache 2.0 license any developer can take Rarog Platform and create his own app on open source or commercial license.

From architectural perspective: it is monolith app designed for low to medium load, with heavy business logic includes via pluggable extensions. Right now it supports OSGi based plugins, but in plan is to accept microservice based plugins as well. Platform can be treated as central point, that provide basic functionalities like user management, basic configuration, app management, admin control, monitoring central, etc. All specialized functionalities are delivered with plugins.
Thanks to wide extensibility area it can be adjusted be used as platform for microservices. Still, out-of-box support for microservices architecture is far future.

# Team
Current core team (also known as Rarog Team) consist of 3 people: architect, backend dev and frontend dev. We are working 
on project after hour. We have various reasons for that: to learn, to use this project for our homes, to flex on LinkedIn 😉.
But not matter the reasons we give our 100% to keep high quality of this project. You can reach us on our [Discord server](https://discord.gg/qQ6NpHG57t),
where we are coordinating our efforts, plan future moves, share memes and black humour.

# Contribution
You can read [Code of conduct](https://wiki.rarogsoftware.eu/wiki/Code_of_conduct) and [Contribution guidelines](https://wiki.rarogsoftware.eu/wiki/Rarog_Platform:Contributing) to learn how to contribute to project as Core Contribution.
