import os

from rmake.RMakeAppRunningCommand import RMakeAppRunningCommand


class RMakeFuncTests(RMakeAppRunningCommand):
    def __init__(self):
        super().__init__('verify')
        self.default_path = 'server'

    def configureParser(self, parser):
        super().configureParser(parser)
        parser.add_argument('--debug-tests', action='store_true', default=False, help='Open debug port for tests')
        parser.add_argument('--debug-tests-port', default=5015, type=int,
                            help='Port to open for debugger for tests. Works only with option -d or -D. Default: 5015')
        parser.add_argument('-X', '--start-xvfb', action='store_true', default=False,
                            help='Start internal xvfb server for tests')

    def getDescription(self):
        return 'Starts Rarog Server using cargo maven plugin and runs functional tests'

    def run(self, args):
        args.dev_mode = True
        super().run(args)

    def _prepare_run(self, args):
        super()._prepare_run(args)
        if args.debug_tests:
            suspend_mode = ('y' if args.suspend else 'n')
            self._maven.add_option(
                f'-Dmaven.failsafe.debug="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend={suspend_mode},address=*:{args.debug_tests_port}"')
        if args.start_xvfb or not os.environ.get('DISPLAY'):
            self._maven.add_option('-Dxvfb.enable=true')
