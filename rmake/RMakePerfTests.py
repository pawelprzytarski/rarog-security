import glob
import os
import shutil
import subprocess
import uuid

from rmake.RMakeClean import RMakeClean
from rmake.RMakeMavenCommand import RMakeMavenCommand


class RMakePerfTests(RMakeMavenCommand):
    def __init__(self):
        super().__init__()
        self._clean = RMakeClean()

    def configureParser(self, parser):
        super().configureParser(parser)
        self._clean.addCleanOptions(parser)
        parser.add_argument('--fast', action='store_true', default=False,
                            help='Skip compilation of whole project and goes straight to docker commands')
        parser.add_argument('-T', '--type', default='smoke',
                            help='Define colon `,` separated perf tests categories to run. ALL will run all tests. '
                                 '"smoke" is default value.')
        parser.add_argument('--single-test', default=None,
                            help='Runs single test inside `tests` directory')
        parser.add_argument('--dump-database', action='store_true', default=False,
                            help='Dump postgres state after each test end.')
        parser.add_argument('--refresh-docker-images', action='store_true', default=False,
                            help='Rebuild docker images even if they are already build')

    def getDescription(self):
        return 'Build Rarog Docker image and runs tests on containers using k6 framework'

    def run(self, args):
        self._run_command('docker info > /dev/null &2>1')
        if not args.fast:
            self._clean.runIfOption(args)
            super().run(args)
            self._maven.add_command('package -pl !func-tests')
            self._maven.add_option('-DskipTests')
            self._maven.add_option('-DskipITs')
            self._maven.add_option('-DskipFuncTests')

            self._maven.add_option('-DdevMode')
            self._maven.add_option('-Dskip.frontend=false')
            self._maven.run()

        project_path = os.path.normpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../'))
        found_wars = glob.glob(project_path + '/rarog-server/target/rarog-server-*.war')
        if len(found_wars) == 0:
            print('Failed to build successfully war')
            exit(1)

        war_path = project_path + '/docker/ROOT.war'
        try:
            os.remove(war_path)
        except:
            pass
        shutil.copyfile(found_wars.pop(), war_path)

        print('Running docker commands using sudo. May ask for password')
        sudo_prefix = ''
        try:
            self._run_command(f'docker version')
        except:
            sudo_prefix = 'sudo '
        self._run_command(
            f'{sudo_prefix}docker build {project_path}/docker --tag gitlab.com/rarogsoftware/rarog-platform:perfTestsLocal')
        original_path = os.getcwd()
        os.chdir(f'{project_path}/perf-tests/docker')
        exit_code = 1
        if args.refresh_docker_images:
            self._run_command(f'{sudo_prefix}docker-compose build')
        try:
            env = {
                'TEST_TYPE': args.type,
                'TOMCAT_PASSWORD': str(uuid.uuid4())
                   }
            if args.dump_database:
                env['DUMP_DATABASE'] = 'true'
            if args.single_test:
                env['SINGLE_TEST'] = args.single_test
            exit_code = self._run_command_in_process(f'{sudo_prefix}docker-compose up --exit-code-from k6',
                                                     env).returncode
        finally:
            self._run_command_in_process('docker-compose down')
            os.chdir(original_path)
        exit(exit_code)

    @staticmethod
    def _run_command(command, envOverrides=dict()):
        process = RMakePerfTests._run_command_in_process(command, envOverrides)
        exitcode = process.returncode
        if exitcode != 0:
            print(f'Failed to run command: {command}')
            raise OSError(f'Command returned non-zero exit code: {exitcode}')

    @staticmethod
    def _run_command_in_process(command, envOverrides=dict()):
        env = os.environ.copy()
        for key in envOverrides:
            env[key] = envOverrides[key]
        process = subprocess.run(command, env=env, shell=True)
        return process
