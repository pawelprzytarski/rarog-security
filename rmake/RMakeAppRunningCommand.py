import os
from enum import Enum
from pathlib import Path

from rmake.RMakeClean import RMakeClean
from rmake.RMakeMavenCommand import RMakeMavenCommand


class DatabaseType(Enum):
    H2 = 'h2'
    H2_FILE = 'h2-file'
    POSTGRES_DOCKER = 'postgres-docker'
    POSTGRES = 'postgres'

    def __str__(self):
        return self.value


class RMakeAppRunningCommand(RMakeMavenCommand):
    def __init__(self, command):
        super().__init__()
        self._clean = RMakeClean()
        self.extra_args = ''
        self.command = command
        self.default_path = None
        self.precommands = []
        self.postcommands = []

    def configureParser(self, parser):
        super().configureParser(parser)
        parser.add_argument('--debug', action='store_true', default=False, help='Open debug port')
        parser.add_argument('-a', '--activemq-port', default=10001, type=int,
                            help='Port for application to start ActiveMQ broker. Default: 10001')
        parser.add_argument('-p', '--port', default=8080, type=int,
                            help='Port for application to start tomcat server. Default: 8080')
        parser.add_argument('--servlet-path', type=str,
                            help=('Define servlet path for application. Default: ' + (
                                'Empty' if self.default_path is None else self.default_path)))
        parser.add_argument('--debug-port', default=5005, type=int,
                            help='Port to open for debugger for application. Works only with option -d or -D. Default: 5005')
        parser.add_argument('-susp', '--suspend', action='store_true', default=False,
                            help='When debug port is open, wait for debugger to attach after JVM is started')
        parser.add_argument('-J', '--java-argument', action='append', help='Pass arguments to JVM')
        parser.add_argument('-q', '--quickstart', action='store_true', default=False,
                            help='Skip compilation and start application from already compiled sources')
        parser.add_argument('--fast', action='store_true', default=False,
                            help='Skip compilation of whole project and recompile only server project (reloads resources, etc)')
        parser.add_argument('-M', '--max-memory', default='512M', help='Max heap size for application')
        parser.add_argument('-m', '--min-memory', default='128M', help='Start heap size for application')
        parser.add_argument('--app-log-level', default='INFO', type=str,
                            help='Log level for project classes. Log level for library classes stays the same')
        parser.add_argument('--root-log-level', default='INFO', type=str, help='Default log level for all classes')
        parser.add_argument('--database-type', type=DatabaseType, choices=list(DatabaseType), default=DatabaseType.H2,
                            help="Type of database used to run Rarog with. Special values: h2-file (H2 configured to "
                                 "store data on disk instead of memory), postgres-docker (postgres instance that is "
                                 "automatically setup using system docker), postgres (runs with postgres on localhost:5432/rarog_db with "
                                 "rarog_user:rarog_password credentials by default")
        self._clean.addCleanOptions(parser)

    def run(self, args):
        if (args.fast or args.quickstart) and args.clean:
            print('Specified conflicting options together: (fast or quickstart) and clean. Failing build')
            exit(1)
        self._clean.runIfOption(args)
        if not args.quickstart and not args.fast:
            self.build(args)
        if not args.quickstart and args.fast:
            self.build_server(args)

        self._clearMaven(args)
        self._prepare_config(args)
        self._prepare_run(args)
        self._run_command(args)

    def _prepare_config(self, args):
        if Path("func-tests/target/test-classes/localconfig.properties").exists():
            os.remove("func-tests/target/test-classes/localconfig.properties")

    def build(self, args):
        super().run(args)
        self._maven.add_command('install')
        self._maven.add_option('-pl !func-tests')
        self._skip_tests()
        self._set_common_options(args)
        self._maven.run()

    def _set_common_options(self, args):
        if args.dev_mode:
            self._maven.add_option('-DdevMode')
            if not args.do_not_rebuild_frontend:
                self._maven.add_option('-Dskip.frontend=false')
        if args.do_not_rebuild_frontend:
            self._maven.add_option('-Dskip.frontend=true')

    def _skip_tests(self):
        self._maven.add_option('-DskipTests')
        self._maven.add_option('-DskipITs')
        self._maven.add_option('-DskipFuncTests')

    def build_server(self, args):
        super().run(args)
        self._maven.add_command('install')
        self._maven.add_option('-pl rarog-server')
        self._skip_tests()
        self._set_common_options(args)
        self._maven.run()

    def _prepare_run(self, args):
        self._set_common_options(args)
        self._maven.add_option('-pl func-tests')
        self._maven.add_option('-Dtomcat.port=' + str(args.port))
        self._maven.add_option('-Dactivemq.port=' + str(args.activemq_port))
        if args.servlet_path:
            self._maven.add_option(f'-Dtomcat.context.path={args.servlet_path}')
        self._add_extra_args(args)
        self._setup_database(args)

    def _setup_database(self, args):
        if args.database_type == DatabaseType.H2_FILE:
            self._maven.add_option(
                "\"-Ddatabase.url=jdbc:h2:file:\\${project.build.directory}/home/db/store;USER=sa;PASSWORD=sa;MODE=PostgreSQL;DATABASE_TO_LOWER=TRUE;CASE_INSENSITIVE_IDENTIFIERS=TRUE\"")
        if args.database_type == DatabaseType.POSTGRES:
            self._maven.add_option(
                "-Ddatabase.driver=org.postgresql.Driver -Ddatabase.url=jdbc:postgresql://localhost:5432/rarog_db "
                "-Ddatabase.templates=com.querydsl.sql.PostgreSQLTemplates -Ddatabase.password=rarog_password "
                "-Ddatabase.username=rarog_user")
        if args.database_type == DatabaseType.POSTGRES_DOCKER:
            self._maven.add_option("-Ddatabase.type=postgres-docker")

    def _add_extra_args(self, args):
        jvm_args = self.extra_args
        if args.java_argument is None:
            args.java_argument = []
        args.java_argument.append('-Xmx' + args.max_memory)
        args.java_argument.append('-Xms' + args.min_memory)
        jvm_args = jvm_args + ' ' + ' '.join(args.java_argument)

        suspend_mode = ('y' if args.suspend else 'n')
        self._maven.add_option('-Ddebug.suspend=' + suspend_mode)
        if args.debug or args.dev_mode:
            jvm_args = jvm_args + f' -DdevMode -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend={suspend_mode},address=*:{args.debug_port}'
            if args.app_log_level.upper() != 'TRACE' and args.app_log_level.upper() != 'ALL':
                args.app_log_level = 'DEBUG'

        jvm_args = jvm_args + f' -Drarog.logs.app.level={args.app_log_level}'
        jvm_args = jvm_args + f' -Drarog.logs.default.level={args.root_log_level}'

        self._maven.add_option(f'-Dtomcat.extraJvmArgs="{jvm_args}"')

    def _run_command(self, args):
        for command in self.precommands:
            self._maven.add_command(command)
        self._maven.add_command(self.command)
        for command in self.postcommands:
            self._maven.add_command(command)
        self._maven.run()
