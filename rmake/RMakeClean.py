from rmake.RMakeMavenCommand import RMakeMavenCommand


class RMakeClean(RMakeMavenCommand):
    def getDescription(self):
        return """Removes all build and run artifacts from system"""

    def addCleanOptions(self, parser):
        parser.add_argument('-c', '--clean', action='store_true', default=False,
                            help='Clean before build')

    def runIfOption(self, args):
        if args.clean:
            super().run(args)
            self._maven.add_command('clean')
            self._maven.run()

    def run(self, args):
        super().run(args)
        self._maven.add_command('clean')
        self._maven.run()