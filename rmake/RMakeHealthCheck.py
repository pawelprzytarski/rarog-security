import enum
import os
import re
import shutil
import sys
import traceback

from rmake.RMakeCommand import RMakeCommand
from rmake.utils.TerminalEffects import TerminalEffects


class HealthCheckResult(enum.Enum):
    success = 0
    warn = 1
    fail = 2
    error = 4


class HealthCheck:
    def __init__(self, function, name):
        self.check_function = function
        self.name = name
        self.optional = False
        self.subchecks = []

    def mark_optional(self):
        self.optional = True
        return self

    def add_subcheck(self, check, name):
        health_check = HealthCheck(check, name)
        self.subchecks.append(health_check)
        if self.optional:
            health_check.mark_optional()
        return self

    def add_optional_subcheck(self, check, name):
        health_check = HealthCheck(check, name)
        self.subchecks.append(health_check)
        health_check.mark_optional()
        return self

    def visit(self, visitor):
        try:
            [success, fix] = self.check_function()
            if success:
                result = HealthCheckResult.success
            else:
                if self.optional:
                    result = HealthCheckResult.warn
                else:
                    result = HealthCheckResult.fail
        except Exception:
            print()
            traceback.print_exc()
            result = HealthCheckResult.error
            fix = 'Exception was raised. Please refer to logs and fix it'

        visitor(self.name, result, fix, len(self.subchecks))

        if result == HealthCheckResult.success:
            for check in self.subchecks:
                check.visit(visitor)


class RMakeHealthCheck(RMakeCommand):
    minimum_docker_version = "20.10.0"

    def __init__(self) -> None:
        super().__init__()
        self.healthchecks = []
        self.add_health_check(self.check_python, "Correct Python Version")
        self.add_health_check(self.check_maven, "Correct Maven Version") \
            .add_subcheck(self.check_java, "Correct Java Version") \
            .add_subcheck(self.check_maven_artifactory, "Maven repository available")
        self.add_health_check(self.check_node, "Correct Node Version") \
            .mark_optional() \
            .add_subcheck(self.check_yarn, "Yarn installed") \
            .add_subcheck(self.check_npm_repository_availability, "NPM registry available")
        self.add_health_check(self.check_docker, "Correct docker version") \
            .add_subcheck(self.check_docker_registry_availability, "Docker registry available") \
            .add_optional_subcheck(self.check_docker_socker_availability, "Docker socker configured")

    def add_health_check(self, check, name):
        health_check = HealthCheck(check, name)
        self.healthchecks.append(health_check)
        return health_check

    def getDescription(self):
        return """Check if system is ready for development"""

    def run(self, args):
        delimiter = "============================================================"
        print("Executing health checks:\n%s\n" % delimiter)

        self.should_fail = False
        self.not_executed_checks = 0
        visitor = self.visit
        for check in self.healthchecks:
            check.visit(visitor)

        if self.should_fail:
            print(
                f"\n{delimiter}\n{TerminalEffects.RED}Failed health checks. Please fix them before continue.{TerminalEffects.RESET}")
            raise SystemExit(1)
        else:
            print(f"\n{delimiter}\n{TerminalEffects.GREEN}All required checks passed{TerminalEffects.RESET}")
        if self.not_executed_checks > 0:
            print(f"{TerminalEffects.YELLOW}Skipped checks: {self.not_executed_checks}{TerminalEffects.RESET}")

    def visit(self, name, result, fix, sub_checks_count):
        color = TerminalEffects.GREEN
        symbol = '\u2713'
        additional_info = ''
        if result == HealthCheckResult.fail or result == HealthCheckResult.error:
            symbol = 'X'
            color = TerminalEffects.RED
            additional_info = f" (Skipped: {sub_checks_count})\n\t\t{fix}\n"
            self.should_fail = True
            self.not_executed_checks = self.not_executed_checks + sub_checks_count

        if result == HealthCheckResult.warn:
            symbol = '?'
            color = TerminalEffects.YELLOW
            additional_info = f" (Skipped: {sub_checks_count})\n\t\t{fix}\n"
            self.not_executed_checks = self.not_executed_checks + sub_checks_count

        print(f'\t{color}[{symbol}] {name} {additional_info}{TerminalEffects.RESET}')

    @staticmethod
    def check_python():
        with open(".python-version", 'r') as pyenv_version:
            requested_python = pyenv_version.readline().strip()
        current_version = f"{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}"
        if requested_python != current_version:
            return [False, f"Install python {requested_python}"]
        return [True, None]

    @staticmethod
    def check_maven():
        if shutil.which('mvn') is None:
            return [False, "Install mvnvm. No maven installation found."]

        [current_mvn, current_java] = RMakeHealthCheck.get_maven_version()

        with open("mvnvm.properties", 'r') as mvnvm_version:
            requested_maven = mvnvm_version.readline().strip().replace("mvn_version=", "")

        if requested_maven != current_mvn:
            print("Incorrect maven version. Please install mvnvm or correct version of maven")
            return [False, "Please install mvnvm. It will select correct maven automatically"]
        return [True, None]

    @staticmethod
    def get_maven_version():
        maven_version = os.popen('mvn --version').read().replace('\n', '')
        match = re.compile(".*Apache Maven ([0-9.]+).*Java version: (.*), vendor.*").match(maven_version)
        current_mvn = match.group(1)
        current_java = match.group(2)
        return [current_mvn, current_java]

    @staticmethod
    def check_java():
        [current_mvn, current_java] = RMakeHealthCheck.get_maven_version()

        with open(".java-version", 'r') as java_version:
            requested_java = java_version.readline().strip()

        if not current_java.startswith(requested_java):
            return [False, "Please install correct version and/or use jenv for java versions management"]

        return [True, None]

    @staticmethod
    def check_maven_artifactory():
        if os.system(
                "mvn dependency:purge-local-repository -DmanualInclude=eu.rarogsoftware:commons-cache dependency:get "
                "-Dartifact=eu.rarogsoftware:commons-cache:0.0.1 > /dev/null &2> /dev/null"):
            return [False, "Failed to download test maven artifact. Check you network and run `./rmake.py setup`"]
        return [True, None]

    @staticmethod
    def check_node():
        if shutil.which("node") is None:
            return [False, "No node installed. Please install nvm and run `./rmake.py setup`"]

        node_version = os.popen('node --version').read().replace('\n', '')

        with open(".nvmrc", 'r') as nvm_version:
            requested_node = nvm_version.readline().strip()

        if not node_version.startswith(f"v{requested_node}"):
            return [False, "Incorrect node version. Please install nvm and run `./rmake.py setup`"]

        return [True, None]

    @staticmethod
    def check_yarn():
        if shutil.which("yarn") is None:
            return [False, "No node installed. Please install nvm and run `./rmake.py setup`"]

        return [True, None]

    @staticmethod
    def check_npm_repository_availability():
        if os.system("npm install --no-save @rarog/test > /dev/null ") != 0:
            return [False, "Failed to download test npm package. Check you network and run `./rmake.py setup`"]
        return [True, None]

    @staticmethod
    def check_docker():
        if shutil.which("docker"):
            if shutil.which("podman"):
                return [False, "Install docker or dvm (https://github.com/howtowhale/dvm) and use `dvm detect`"]
            else:
                return [True, None]

        docker_version_string = os.popen('docker --version').read().replace('\n', '')
        docker_version = re.compile("Docker version ([0-9.]+), .*").match(docker_version_string).group(1)

        if not docker_version > RMakeHealthCheck.minimum_docker_version:
            return [False,
                    f"Too old version of docker. Minimum supported version is {RMakeHealthCheck.minimum_docker_version}"]

        return [True, None]

    @staticmethod
    def check_docker_registry_availability():
        if os.system("docker pull registry.gitlab.com/rarogsoftware/rarog-platform/test:test > /dev/null &2> /dev/null"):
            return [False, f"Failed to download test docker image. Check you network and run `./rmake.py setup`"]
        return [True, None]

    @staticmethod
    def check_docker_socker_availability():
        import socket
        docker_socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        try:
            docker_socket.connect('/var/run/docker.sock')
            docker_socket.close()
        except FileNotFoundError:
            return [False, "Socket /var/run/docker.sock is not ready"]

        return [True, None]
