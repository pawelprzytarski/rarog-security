import os
import subprocess


class Maven(object):
    def __init__(self):
        self.subprocess = None
        self._options = None
        self._opts = None
        self._executable = None
        self._commands = None
        self._exit_on_failure = True

    def set_executable(self, executable):
        self._executable = executable

    def set_opts(self, opts):
        self._opts = opts

    def add_opts(self, opts):
        if self._opts is None:
            self.set_opts(opts)
        else:
            self._opts = self._opts + ' ' + opts

    def set_commands(self, commands):
        self._commands = commands

    def add_command(self, command):
        if self._commands is None:
            self.set_commands(command)
        else:
            self._commands = self._commands + ' ' + command

    def set_options(self, options):
        self._options = options

    def add_option(self, option):
        if self._options is None:
            self.set_options(option)
        else:
            self._options = self._options + ' ' + option

    def set_exit_on_failure(self, exit_on_failure):
        self._exit_on_failure = exit_on_failure

    def run(self):
        env = os.environ.copy()
        env['MAVEN_OPTS'] = self._opts
        command = f'{self._executable} {self._options} {self._commands}'
        print(f'Running \'{command}\' with MAVEN_OPTS: \'{self._opts}\'')
        exit_code = subprocess.run([command], env=env, shell=True).returncode
        if exit_code != 0 and self._exit_on_failure:
            exit(exit_code)
        else:
            return exit_code
