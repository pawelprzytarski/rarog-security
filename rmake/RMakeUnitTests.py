from rmake.RMakeClean import RMakeClean
from rmake.RMakeMavenCommand import RMakeMavenCommand


class RMakeUnitTests(RMakeMavenCommand):
    def __init__(self):
        super().__init__()
        self._clean = RMakeClean()

    def configureParser(self, parser):
        super().configureParser(parser)
        parser.add_argument('--debug', action='store_true', default=False, help='Open debug port')
        parser.add_argument('--debug-port', default=5005, type=int,
                            help='Port to open for debugger. Works only with option -d or -D. Default: 5005')
        parser.add_argument('-susp', '--suspend', action='store_true', default=False,
                            help='When debug port is open, wait for debugger to attach after JVM is started')
        self._clean.addCleanOptions(parser)

    def getDescription(self):
        return 'Run unit tests only'

    def run(self, args):
        self._clean.runIfOption(args)
        super().run(args)
        self._maven.add_command('test')
        self._maven.add_option('-DskipITs')
        self._maven.add_option('-DskipFuncTests')
        if args.debug and not args.dev_mode:
            suspend_mode = ('y' if args.suspend else 'n')
            self._maven.add_option(f'-Dmaven.surefire.debug=="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend={suspend_mode},address=*:{args.debug_port}"')
        self._maven.run()
