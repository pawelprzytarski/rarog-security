import os

from rmake.RMakeClean import RMakeClean
from rmake.RMakeMavenCommand import RMakeMavenCommand


class RMakeInstall(RMakeMavenCommand):
    def __init__(self):
        super().__init__()
        self._clean = RMakeClean()

    def configureParser(self, parser):
        super().configureParser(parser)
        parser.add_argument('-t', '--with-tests', action='store_true', default=False,
                            help='Run tests during install. Default:false')
        parser.add_argument('-d', '--dev-mode', action='store_true', default=False, help='Install dev mode artifacts')
        parser.add_argument('-X', '--start-xvfb', action='store_true', default=False,
                            help='Start internal xvfb server for tests')
        self._clean.addCleanOptions(parser)

    def getDescription(self):
        return 'Build and install maven artifacts to local repository. By default skip tests.'

    def run(self, args):
        self._clean.runIfOption(args)
        super().run(args)
        self._maven.add_command('install')
        if not args.with_tests:
            self._maven.add_option('-DskipTests')
            self._maven.add_option('-DskipITs')
            self._maven.add_option('-DskipFuncTests')
        else:
            if args.start_xvfb or not os.environ.get('DISPLAY'):
                self._maven.add_option('-Dxvfb.enable=true')

        if args.dev_mode:
            self._maven.add_option('-DdevMode')
            self._maven.add_option('-Dskip.frontend=false')
            if args.with_tests:
                self._maven.add_option('-DskipTests=false')
        self._maven.run()
