import os

from rmake.RMakeClean import RMakeClean
from rmake.RMakeMavenCommand import RMakeMavenCommand


class RMakeIntegrationTests(RMakeMavenCommand):
    def __init__(self):
        super().__init__()
        self._clean = RMakeClean()

    def configureParser(self, parser):
        super().configureParser(parser)
        parser.add_argument('--debug', action='store_true', default=False, help='Open debug port')
        parser.add_argument('--debug-port', default=5015, type=int,
                            help='Port to open for debugger. Works only with option -d or -D. Default: 5015')
        parser.add_argument('-susp', '--suspend', action='store_true', default=False,
                            help='When debug port is open, wait for debugger to attach after JVM is started')
        parser.add_argument('-X', '--start-xvfb', action='store_true', default=False,
                            help='Start internal xvfb server for tests')
        self._clean.addCleanOptions(parser)

    def getDescription(self):
        return 'Run integration tests aka tests that do not need working Rarog server'

    def run(self, args):
        self._clean.runIfOption(args)
        super().run(args)
        self._maven.add_command('verify')
        self._maven.add_option('-DskipUTs')
        self._maven.add_option('-pl !func-tests')
        self._maven.add_option('-DdevMode')
        if args.debug and not args.dev_mode:
            suspend_mode = ('y' if args.suspend else 'n')
            self._maven.add_option(
                f'-Dmaven.failsafe.debug=="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend={suspend_mode},address=*:{args.debug_port}"')
        if args.start_xvfb or not os.environ.get('DISPLAY'):
            self._maven.add_option('-Dxvfb.enable=true')
        self._maven.run()

