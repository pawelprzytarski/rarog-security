import os

from rmake.RMakeAppRunningCommand import RMakeAppRunningCommand, DatabaseType


class RMakeRun(RMakeAppRunningCommand):
    def __init__(self):
        super().__init__('cargo:run')

    def configureParser(self, parser):
        super().configureParser(parser)
        parser.add_argument('-d', '--dev-mode', action='store_true', default=False,
                            help='Run with dev mode artifacts and open debug port')
        parser.add_argument('-tt', '--test-ready', action='store_true', default=False,
                            help='Set all necessary setting to values necessary to run func tests from IDE')

    def getDescription(self):
        return 'Build and start Rarog server'

    def run(self, args):
        if args.test_ready:
            args.dev_mode = True
            args.servlet_path = 'server'
        if args.dev_mode:
            if args.java_argument is None:
                args.java_argument = []
            resourcesPath = os.getcwd() + "/rarog-server/src/main/resources"
            classpathPath = os.getcwd() + "/rarog-server/target/classes"
            args.java_argument.append(f"-Dspring.web.resources.static-locations=file:{resourcesPath}/static/,file:{classpathPath}/static/")
            args.java_argument.append(f"-Dspring.thymeleaf.prefix=file:{resourcesPath}/templates/")
            args.java_argument.append(f"-Dwebpack.manifest.location=file:{classpathPath}/descriptors/webpack-{{namespace}}.manifest.json")
            args.java_argument.append(f"-Dwebpack.manifest.cache.duration=1")
            args.java_argument.append(f"-Dspring.thymeleaf.cache=false")
            args.java_argument.append(f"-Dspring.resources.cache.cachecontrol.no-cache=true")

        super().run(args)

    def _prepare_run(self, args):
        super()._prepare_run(args)
        self.precommands.append("generate-test-resources")
        self.precommands.append("process-test-resources")
        if args.database_type == DatabaseType.POSTGRES_DOCKER:
            self._maven.add_option("-Ddatabase.type=postgres-docker")
            self.precommands.append('docker:start')
            self.postcommands.append('docker:stop')
        self._maven.add_option('-Pcargo-run')
