import argparse
import time

from rmake.RMakeClean import RMakeClean
from rmake.RMakeFuncTests import RMakeFuncTests
from rmake.RMakeHealthCheck import RMakeHealthCheck
from rmake.RMakeInstall import RMakeInstall
from rmake.RMakeIntegrationTests import RMakeIntegrationTests
from rmake.RMakeRun import RMakeRun
from rmake.RMakeSetup import RMakeSetup
from rmake.RMakeUnitTests import RMakeUnitTests
from rmake.RMakePerfTests import RMakePerfTests


class RMake(object):
    def __init__(self):
        self._version = '1.0'
        self._commands = {
            'install': RMakeInstall(),
            'clean': RMakeClean(),
            'run': RMakeRun(),
            'unit-tests': RMakeUnitTests(),
            'integration-tests': RMakeIntegrationTests(),
            'func-tests': RMakeFuncTests(),
            'perf-tests': RMakePerfTests(),
            'setup': RMakeSetup(),
            'healthcheck': RMakeHealthCheck()
        }

    def start(self):
        parser = argparse.ArgumentParser(
            description='Tool for building and running Ranoh security. It is just a clever overlay for maven. See commands to learn how use this tool.',
            prog='rmake')
        parser.add_argument('--version', action='version', version='%(prog)s ' + self._version)

        subparsers = parser.add_subparsers(title='commands', dest='command')

        for key in self._commands.keys():
            command = self._commands[key]
            new_parser = subparsers.add_parser(key, description=command.getDescription())
            command.configureParser(new_parser)

        args = parser.parse_args()

        if args.command is not None:
            try:
                self._commands[args.command].run(args)
            except KeyboardInterrupt:
                print('Keyboard interruption caught. Waiting 1 seconds for process to finish')
                time.sleep(1)
        else:
            parser.print_help()
