import os
import re
import shutil
from os.path import exists

from rmake.Maven import Maven
from rmake.RMakeCommand import RMakeCommand


class RMakeMavenCommand(RMakeCommand):
    def __init__(self):
        self._maven = Maven()

    def configureParser(self, parser):
        self._prepareMavenArgs(parser)

    def _prepareMavenArgs(self, parser):
        parser.add_argument('-O', '--maven-opts', type=str,
                            help='MAVEN_OPTS to be passed to maven. Memory settings are overridden! Default:empty')
        parser.add_argument('--maven-executable',
                            help='Path to maven executable. If not specified tries to find maven automatically.')
        parser.add_argument('-Xmx', default='2048M', help='Maximum memory available for maven command. Default:2048M')
        parser.add_argument('-Xms', default='512M', help='Set initial allocated memory for maven command. Default:512M')
        parser.add_argument('--maven-debug', default=False, action='store_true', help='Setup debugger for mvn')
        parser.add_argument('--maven-debug-port', default=8000, type=int,
                            help='Port on which debugger for mvn can be accessed. Default:8000')
        parser.add_argument('-F', '--maven-flag', action='append', help='Pass additional flags and settings to maven')
        parser.add_argument('-D', '--property', action='append',
                            help='Pass additional property to maven. Does the same as -D directly on maven')
        parser.add_argument('--rebuild-plugin', action='store_true', default=False,
                            help='Rebuild rarog-maven-plugin before running any command')
        parser.add_argument('-b', '--do-not-rebuild-frontend', action='store_true', default=False,
                            help='Do not rebuild frontend. Makes run faster but will fail is frontend is not build already')

        return parser

    def _clearMaven(self, args):
        self._maven = Maven()
        self._prepareMavenCommand(args)

    def run(self, args):
        if not exists('maven-rarog-plugin/target') or args.rebuild_plugin:
            args.rebuild_plugin = False
            self._build_rarog_maven_plugin(args)
        else:
            self._prepareMavenCommand(args)

    def _build_rarog_maven_plugin(self, args):
        # Install parent
        self._prepareMavenCommand(args)
        self._maven.add_command("install")
        self._maven.add_option("-N")
        self._maven.run()
        self._clearMaven(args)
        # Install plugin
        original_cwd = os.getcwd()
        os.chdir('./rarog-maven-plugin')
        self._prepareMavenCommand(args)
        self._maven.add_option('-DskipTests')
        self._maven.add_command('clean')
        self._maven.add_command('install')
        self._maven.run()
        os.chdir(original_cwd)
        self._clearMaven(args)

    def _prepareMavenCommand(self, args):
        maven_executable = self._get_maven_executable(args)
        self._maven.set_executable(maven_executable)
        opts = self._prepare_maven_opts(args)
        self._maven.set_opts(opts)
        options = self._prepare_maven_options(args)
        if args.do_not_rebuild_frontend:
            self._maven.add_option('-Dskip.frontend=true')
        properties = self._prepare_maven_properties(args)
        self._maven.set_options(options + properties)

    @staticmethod
    def _prepare_maven_opts(args):
        opts = f'-Xmx{args.Xmx} -Xms{args.Xms} '
        if args.maven_opts is not None:
            opts = opts + re.sub('-Xm[xs]\d+[mMgGtTkK]?', "", args.mvn_opts)
        if args.maven_debug:
            opts = opts + f' -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address={args.maven_debug_port}'
        return opts

    def _get_maven_executable(self, args):
        if self._is_executable(args.maven_executable):
            return args.maven_executable

        mvn_executable = shutil.which('mvn')
        if mvn_executable is not None:
            return mvn_executable

        raise Exception('mvn executable is not present in system')

    @staticmethod
    def _is_executable(executable):
        return executable is not None \
               and os.path.exists(executable) \
               and os.path.isfile(executable) \
               and os.access(executable, os.X_OK)

    @staticmethod
    def _prepare_maven_options(args):
        if args.maven_flag is not None and len(args.maven_flag) > 0:
            return ' '.join(args.maven_flag)
        else:
            return ''

    @staticmethod
    def _prepare_maven_properties(args):
        if args.property is not None and len(args.property) > 0:
            return ' -D' + ' -D'.join(args.property)
        else:
            return ''
