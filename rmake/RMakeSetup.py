import os
import re
import sys
import xml.dom.minidom as minidom
import shutil
from pathlib import Path

from rmake.RMakeCommand import RMakeCommand


class RMakeSetup(RMakeCommand):
    def getDescription(self):
        return """Setup system settings to access maven repository"""

    def run(self, args):
        self.configure_tools_versions()
        self.configure_access_token()

    def configure_tools_versions(self):
        success = self.configure_python()
        success = self.configure_node() and success
        success = self.check_maven() and success

        if not success:
            print("Failed to configure all tool. Referer to logs and manually configure your environment")

    def configure_python(self):
        with open(".python-version", 'r') as pyenv_version:
            requested_python = pyenv_version.readline().strip()
        current_version = f"{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}"
        if requested_python != current_version:
            print(f"Installing python {requested_python} with pyenv")
            if os.system("pyenv install") != 0:
                return False
        os.system("python3 --version")
        return True

    def configure_node(self):
        if os.system("~/.nvm/nvm.sh use") != 0 or os.system("~/.nvm/nvm.sh install") != 0:
                return False

        if shutil.which("yarn") is None:
            os.system("npm install -g yarn")

        os.system("node --version")
        os.system("npm --version")
        os.system("yarn --version")
        return True

    def check_maven(self):
        if os.system("java --version") != 0:
            print("Failed to invoke java executable. Please install jdk")
            return False
        if os.system("mvn --version") != 0:
            print("Failed to invoke maven executable. Please install mvnvm")
            return False

        maven_version = os.popen('mvn --version').read().replace('\n', '')

        match = re.compile(".*Apache Maven ([0-9.]+).*Java version: (.*), vendor.*").match(maven_version)
        current_mvn = match.group(1)
        current_java = match.group(2)

        with open("mvnvm.properties", 'r') as mvnvm_version:
            requested_maven = mvnvm_version.readline().strip().replace("mvn_version=", "")

        if requested_maven != current_mvn:
            print("Incorrect maven version. Please install mvnvm or correct version of maven")
            return False

        with open(".java-version", 'r') as java_version:
            requested_java = java_version.readline().strip()

        if not current_java.startswith(requested_java):
            print("Incorrect java version. Please install correct version and/or use jenv for java versions management")
            return False

        return True

    def configure_access_token(self):
        print("Configuring remote repositories for npm, maven and optionally docker")
        token = input(
            "Provide token to GitLab instance. Required scopes: api,read_registry. Optional scopes: write_registry. Empty to skip this step. Docs: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html\nToken: ")
        if len(token) == 0:
            print("Empty token. Skip configuration")
        else:
            self.save_maven_settings(token)
            self.login_to_npm(token)
        self.login_to_docker()

    def login_to_npm(self, token):
        print("configuring npm")
        os.system("npm config set @rarog:registry https://gitlab.com/api/v4/projects/33048100/packages/npm/")
        os.system("npm config set @rarog-commons:registry https://gitlab.com/api/v4/projects/33441474/packages/npm/")
        os.system(f"""npm config set -- '//gitlab.com/api/v4/projects/33048100/packages/npm/:_authToken' "{token}" """)
        os.system(f"""npm config set -- '//gitlab.com/api/v4/projects/33441474/packages/npm/:_authToken' "{token}" """)

    def login_to_docker(self):
        docker_exec = shutil.which("docker")
        podman_exec = shutil.which("podman")
        if docker_exec is None and podman_exec is None:
            print("No docker or podman detected")

        if docker_exec is None:
            docker_exec = podman_exec

        print("Attempting to login to docker registry. You will be asked for your gitlab credentials if you are not logged already. Docs: https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry.")
        print(f"{docker_exec} login https://registry.gitlab.com/")
        os.system(f"{docker_exec} login https://registry.gitlab.com/")

    def save_maven_settings(self, token):
        print("Configuring maven")
        settings_path = Path(Path.home(), ".m2/settings.xml")
        if settings_path.exists():
            shutil.copy(settings_path, str(settings_path)+".backup")
            self.update_existing_maven_settings(settings_path, token)
        else:
            self.create_new_maven_settings(settings_path, token)

    def create_new_maven_settings(self, settings_path, token):
        settings_path.parent.mkdir(parents=True, exist_ok=True)
        base_path = Path(Path(__file__).parent, "resources/simpleSettings.xml")
        with base_path.open('r') as base_file:
            base = base_file.read()
        with settings_path.open('w+') as file:
            file.write(base.format(token))

    def update_existing_maven_settings(self, settings_path, token):
        xml: minidom.Document = minidom.parse(str(settings_path))
        root = xml.firstChild
        if root.nodeName != 'settings':
            raise AssertionError("Root of settings.xml is not settings")
        servers = root.getElementsByTagName('servers')
        if servers is None or len(servers) == 0:
            servers = [xml.createElement('servers')]
            root.appendChild(servers[0])
        servers = servers[0]
        keys = ['gitlab-maven', 'gitlab-maven-commons']
        server_list = []
        for child in servers.getElementsByTagName('server'):
            child_id = child.getElementsByTagName('id')
            if len(child_id) == 1 and keys.__contains__(child_id[0].firstChild.nodeValue):
                server_list.append(child)
                keys.remove(child_id[0].firstChild.nodeValue)
        for key in keys:
            server: minidom.Element = xml.createElement('server')
            server_id = xml.createElement('id')
            server_id.appendChild(xml.createTextNode(key))
            server.appendChild(server_id)
            servers.appendChild(server)
        for server in servers.getElementsByTagName('server'):
            configuration = server.getElementsByTagName('configuration')
            if len(configuration) == 0:
                configuration = xml.createElement('configuration')
                server.appendChild(configuration)
            else:
                configuration = configuration[0]
            headers = configuration.getElementsByTagName('httpHeaders')
            if len(headers) == 0:
                headers = xml.createElement('httpHeaders')
                configuration.appendChild(headers)
            else:
                headers: minidom.Element = headers[0]
            properties = headers.getElementsByTagName('property')
            if len(properties) == 0:
                token_property = None
            else:
                token_property = None
                for child in properties:
                    names = child.getElementsByTagName('name')
                    if len(names) == 1 and names[0].firstChild.nodeValue == 'Private-Token':
                        token_property = child
                        break
            if token_property is None:
                token_property: minidom.Element = xml.createElement('property')
                headers.appendChild(token_property)

            token_names = token_property.getElementsByTagName('name')
            if len(token_names) == 0:
                token_name = xml.createElement('name')
                token_name.appendChild(xml.createTextNode("Private-Token"))
                token_property.appendChild(token_name)

            token_values = token_property.getElementsByTagName('value')
            if len(token_values) > 0:
                for token_value in token_values:
                    token_property.removeChild(token_value)
            token_value = xml.createElement('value')
            token_value.appendChild(xml.createTextNode(token))
            token_property.appendChild(token_value)

        pretty_xml = xml.toprettyxml(standalone=True)
        # remove the weird newline issue:
        pretty_xml = os.linesep.join([s for s in pretty_xml.splitlines()
                                      if s.strip()])
        with settings_path.open('w+') as file:
            file.write(pretty_xml)
