package eu.rarogsoftware.rarog.platform.api.security;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.*;

/**
 * Enforces access restriction to endpoint(s) to users with role admin.
 * <p>
 * It enforce role requirements with {@link PreAuthorize} using Spring login
 * and documents this behaviour with {@link SecurityRequirement} for OpenAPI.
 * </p>
 * <p>
 * Code annotated with {@link AdminOnly}
 * <pre>
 *          &#064;GetMapping("test")
 *          &#064;AdminOnly
 *          ResponseEntity testMethod()
 *     </pre>
 * is equivalent of code annotated with both {@link PreAuthorize} and {@link SecurityRequirement}
 * <pre>
 *          &#064;GetMapping("test")
 *          &#064;PreAuthorize("hasRole('ROLE_ADMIN')")
 *          &#064;SecurityRequirement(name = "admin")
 *          ResponseEntity testMethod()
 *     </pre>
 * </p>
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
@SecurityRequirement(name = "admin")
public @interface AdminOnly {
}
