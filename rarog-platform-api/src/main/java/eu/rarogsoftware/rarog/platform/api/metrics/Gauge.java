package eu.rarogsoftware.rarog.platform.api.metrics;

import java.io.Closeable;
import java.util.concurrent.Callable;

/**
 * Gauge metric, to report instantaneous values.
 */
public interface Gauge extends BasicMetric<Gauge> {
    /**
     * Increment gauge by given amount.
     *
     * @param amount amount to add gauge value
     */
    void increase(double amount);

    /**
     * Increment gauge by 1.
     */
    default void increment() {
        increase(1.0);
    }

    /**
     * Decrement gauge by given amount.
     *
     * @param amount amount to add gauge value
     */
    void decrease(double amount);

    /**
     * Decrement gauge by 1.
     */
    default void decrement() {
        decrease(1.0);
    }

    /**
     * Set gauge to given amount.
     *
     * @param value amount to add gauge value
     */
    void set(double value);

    /**
     * Start a timer to track a duration.
     *
     * <p>
     * Call {@link Timer#stop} at the end of what you want to measure duration of.
     * </p>
     * <p>
     * {@link Timer} is {@link Closeable} to you can use it in try with resources to auto call {@link Timer#stop()}
     * </p>
     *
     * @return timer object
     */
    Timer startTimer();

    /**
     * Executes runnable code and observes a duration of how long it took to run.
     *
     * @param timeable Code that is being timed
     * @return Measured duration in seconds for timeable to complete.
     */
    double measureExecutionTime(Runnable timeable);

    /**
     * Executes runnable code and observes a duration of how long it took to run.
     *
     * @param timeable Code that is being timed
     * @return result of timeable function
     * @throws Exception exception thrown by timeable function
     */
    <T> T measureExecutionTime(Callable<T> timeable) throws Exception;

    /**
     * Represents an event being timed.
     *
     * <p>
     * Observe duration on {@link AutoCloseable#close()}
     * </p>
     */
    interface Timer extends AutoCloseable {
        /**
         * Set the amount of time in seconds since {@link Gauge#startTimer} was called.
         *
         * @return Measured duration in seconds since {@link Gauge#startTimer} was called.
         */
        double stop();
    }
}
