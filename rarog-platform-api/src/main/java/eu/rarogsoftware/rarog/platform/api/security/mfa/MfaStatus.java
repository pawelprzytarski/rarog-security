package eu.rarogsoftware.rarog.platform.api.security.mfa;

/**
 * Current status of MFA authentication for logged-in user.
 * Used internally by {@link MfaStore} and MFA related filters and servlets
 *
 * @see MfaStore
 */
public enum MfaStatus {
    /**
     * MFA is disabled in system or for current user
     */
    MFA_DISABLED,
    /**
     * MFA is enabled and configured but user didn't pass it yet.
     */
    MFA_NOT_PASSED,
    /**
     * MFA is enabled but is not configured for current user. User need to configure at least one method.
     */
    MFA_NEED_TO_BE_CONFIGURED,
    /**
     * MFA passed for current user, so user can access system.
     */
    MFA_PASSED
}
