package eu.rarogsoftware.rarog.platform.api.resources.management.frontend;

import java.util.List;
import java.util.Optional;

/**
 * Allows to load to compile resources based on descriptor file provided by frontend packager like Webpack.
 *
 * This tool is extremely useful for loading resources that don't have static names. For example resources generated
 * with webpack. Tool make possible to read path to single resources as well as "entrypoint" - set of resources that
 * execute themselves and don't need external invoker.
 *
 * Usually it is used in background by thymeleaf for templates like:
 * <pre>
 *     <!DOCTYPE html>
 * <html xmlns:dynamicassets="http://rarogsoftware.eu/schemas/dynamicassets.xsd">
 * <head>
 *     <title>Example</title>
 *     <dynamicassets:loadcss entrypoint="pluginkey|namespace|entrypointName"/>
 * </head>
 * <body>
 * <div id="react"></div>
 * <dynamicassets:loadjs entrypoint="pluginkey|namespace|entrypointName"/>
 * </body>
 * </html>
 * </pre>
 *
 * Manifest that contains info about entrypoint and assets have specified JSON format:
 * <pre>
 *     {
 *         "assets": {
 *             "assetName": {
 *                 "src": "url address relative to base url of Rarog app",
 *                 "integrity": "integrity hash",
 *                 // additional fields
 *             },
 *             "entrypoints": {
 *                 "entrypointName": {
 *                     "assets": {
 *                         "js": [
 *                             "src": "url address relative to base url of Rarog app",
 *                             "integrity": "integrity hash",
 *                             // additional fields
 *                         ],
 *                         "css": [
 *                             "src": "url",
 *                             "integrity": "hash",
 *                             // fields
 *                         ],
 *                         // other supported types
 *                     },
 *                     // additional fields
 *                 }
 *             }
 *         }
 *     }
 * </pre>
 *
 * @see DynamicAssetsLoaderFactory
 * @see DynamicAssetsDescriptor
 */
public interface DynamicAssetsLoader {
    /**
     * Returns data of set of resource that need to be loaded on page to make them execute as independent functionality.
     * This concept is similar to webpacks entrypoint concept.
     * @param entrypoint name of entry point in descriptor
     * @return list of css and js resources that need to loaded on page to make their functionality work correctly
     */
    Optional<EntrypointAssetData> getAssetsForEntryPoint(String entrypoint);

    /**
     * Returns single resource data. Resources are unique inside single descriptor
     * @param assetName
     * @return asset data
     */
    Optional<WebpackAssetData> getAssetData(String assetName);

    record WebpackAssetData(String source, String integrity) {
    }

    record EntrypointAssetData(List<WebpackAssetData> js, List<WebpackAssetData> css) {
    }
}
