package eu.rarogsoftware.rarog.platform.api.settings;

/**
 * Interface invoked during application start or during plugin activation.
 * Allows to initialize application settings before any use action is invoked.
 * Invoked after spring context initialization but before any other action on app/plugin.
 */
public interface SettingsInitializer {
    /**
     * Invoked during plugin activation or app startup.
     *
     * @param settings system application settings object
     */
    void initialize(ApplicationSettings settings);
}
