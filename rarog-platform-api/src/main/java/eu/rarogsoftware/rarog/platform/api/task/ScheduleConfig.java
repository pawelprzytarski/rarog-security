package eu.rarogsoftware.rarog.platform.api.task;

import java.time.Duration;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;

/**
 * Configuration for {@link TaskManager} scheduled tasks
 *
 * @param initialDelay       time from now to delay execution
 * @param period             repeat period aka next execution start is calculated since previous execution start time.
 *                           It's mutually exclusive with repeatAfter.
 * @param repeatAfter        wait duration before next execution aka next execution start is calculated since previous execution end time.
 *                           It's mutually exclusive with period. -1 for unlimited
 * @param repeatCount        defines how many repeat task should do before it removes itself from queue
 * @param executionCondition Predicate for running task. if condition is false repeat counter will not increase.
 *                           This condition is executed before task is ran.
 * @param cancelCondition    Predicate for cancelling task after running it. If predicate returns true, then task will be cancelled
 */
public record ScheduleConfig<T>(Duration initialDelay,
                                Duration period,
                                Duration repeatAfter,
                                int repeatCount,
                                Predicate<ExecutionInfo<T>> executionCondition,
                                Predicate<ExecutionInfo<T>> cancelCondition) {

    public ScheduleConfig {
        requireNonNull(period);
        requireNonNull(initialDelay);
        requireNonNull(repeatAfter);
        assert repeatCount == -1 || repeatCount > 0;
        assert !initialDelay.isNegative();
        assert !period.isNegative();
        assert !repeatAfter.isNegative();
    }

    public static <T> SchedulerConfigBuilder<T> builder() {
        return new SchedulerConfigBuilder<>();
    }

    public static <T> SchedulerConfigBuilder<T> builder(Class<T> ignoredBuilderType) {
        return new SchedulerConfigBuilder<>();
    }

    public boolean isPeriodic() {
        return !repeatAfter.isZero() || !period.isZero();
    }
}
