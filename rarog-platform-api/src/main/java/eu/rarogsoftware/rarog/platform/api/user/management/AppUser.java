package eu.rarogsoftware.rarog.platform.api.user.management;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serial;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of {@link RarogUser} for users that represent apps.
 * Use {@link AppUserBuilder} to build instance of this class.
 */
public class AppUser implements RarogUser {
    @Serial
    private static final long serialVersionUID = 8685356980931487752L;
    private final String appName;
    private final Set<GrantedAuthority> authorities;
    private final boolean enabled;

    protected AppUser(
            String appName,
            Set<GrantedAuthority> authorities,
            boolean enabled) {
        this.appName = appName;
        this.authorities = new HashSet<>(authorities);
        this.authorities.add(RarogUser.APP_AUTHORITY);
        this.enabled = enabled;
    }

    public static AppUserBuilder builder() {
        return new AppUserBuilder();
    }

    public String getAppName() {
        return appName;
    }

    @Override
    public Set<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return getAppName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return isEnabled();
    }

    @Override
    public boolean isAccountNonLocked() {
        return isEnabled();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isEnabled();
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void eraseCredentials() {
        // no credentials stored
    }

    @Override
    public boolean isHuman() {
        return false;
    }

    @Override
    public boolean isApp() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof AppUser appUser)) return false;

        return new EqualsBuilder().append(enabled, appUser.enabled).append(appName, appUser.appName).append(authorities, appUser.authorities).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(appName).append(authorities).append(enabled).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("appName", appName)
                .append("authorities", authorities)
                .append("enabled", enabled)
                .toString();
    }
}
