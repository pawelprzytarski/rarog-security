package eu.rarogsoftware.rarog.platform.api.user.management;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serial;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of {@link RarogUser} for users that represent humans.
 * It stores additional data like MFA state and potential more.
 * Use {@link StandardUserBuilder} to build instance of this class.
 */
public class StandardUser implements RarogUser {
    @Serial
    private static final long serialVersionUID = 7711301896990182175L;
    private final Long id;
    private final String username;
    private final Set<GrantedAuthority> authorities;
    private final boolean accountNonExpired;
    private final boolean accountNonLocked;
    private final boolean credentialsNonExpired;
    private final boolean enabled;
    private final boolean mfaEnabled;
    private String password;

    protected StandardUser(Long id,
                           String username,
                           String password,
                           Set<GrantedAuthority> authorities,
                           boolean accountNonExpired,
                           boolean accountNonLocked,
                           boolean credentialsNonExpired,
                           boolean enabled,
                           boolean mfaEnabled) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.authorities = new HashSet<>(authorities);
        this.authorities.add(RarogUser.HUMAN_AUTHORITY);
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.enabled = enabled;
        this.mfaEnabled = mfaEnabled;
    }

    protected StandardUser(String username,
                           String password,
                           Set<GrantedAuthority> authorities,
                           boolean accountNonExpired,
                           boolean accountNonLocked,
                           boolean credentialsNonExpired,
                           boolean enabled,
                           boolean mfaEnabled) {
        this(null, username, password, authorities, accountNonExpired, accountNonLocked, credentialsNonExpired, enabled, mfaEnabled);
    }

    public StandardUser(Long id, String username, String password, boolean enabled, boolean mfaEnabled, Set<GrantedAuthority> authorities) {
        this(id, username, password, authorities, true, true, true, enabled, mfaEnabled);
    }

    public static StandardUserBuilder builder() {
        return new StandardUserBuilder();
    }

    public StandardUserBuilder builderFromCurrent() {
        return new StandardUserBuilder()
                .authorities(authorities)
                .accountNonExpired(accountNonExpired)
                .accountNonLocked(accountNonLocked)
                .id(id)
                .credentialsNonExpired(credentialsNonExpired)
                .enabled(enabled)
                .mfaEnabled(mfaEnabled)
                .username(username);
    }

    public boolean hasAuthority(GrantedAuthority authority) {
        return authorities.contains(authority);
    }

    public boolean hasRole(UserRole role) {
        return authorities.contains(role.getAuthority());
    }

    public Long getId() {
        return id;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public Boolean isMfaEnabled() {
        return mfaEnabled;
    }

    @Override
    public void eraseCredentials() {
        password = null;
    }

    @Override
    public boolean isHuman() {
        return true;
    }

    @Override
    public boolean isApp() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StandardUser that = (StandardUser) o;
        return accountNonExpired == that.accountNonExpired
                && accountNonLocked == that.accountNonLocked
                && credentialsNonExpired == that.credentialsNonExpired
                && enabled == that.enabled
                && username.equals(that.username)
                && authorities.equals(that.authorities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, authorities, accountNonExpired, accountNonLocked, credentialsNonExpired, enabled);
    }

    @Override
    public String toString() {
        return "ApplicationUser{"
                + "username='" + username + '\''
                + ", password=[PROTECTED]"
                + ", authorities=" + authorities
                + ", accountNonExpired=" + accountNonExpired
                + ", accountNonLocked=" + accountNonLocked
                + ", credentialsNonExpired=" + credentialsNonExpired
                + ", enabled=" + enabled
                + '}';
    }
}
