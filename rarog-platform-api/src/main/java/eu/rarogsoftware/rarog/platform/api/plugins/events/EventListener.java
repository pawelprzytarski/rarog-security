package eu.rarogsoftware.rarog.platform.api.plugins.events;


/**
 * Classes implementing this interface will be scanned for methods annotated with {@link EventHandler}.
 * To make your life simpler you can also use Spring magic and define following method in plugins configuration:
 * <pre>
 * {@code
 *     &#64;Bean
 *     public EventListenersDescriptor eventListenersDescriptor(List<EventListener> eventListeners) {
 *         return new EventListenersDescriptor(new HashSet<>(eventListeners));
 *     }
 * }
 * </pre>
 * <p>
 * This will register all {@link EventHandler}s found in all classes implementing this interface in your plugin.
 */
public interface EventListener {
}
