package eu.rarogsoftware.rarog.platform.api.resources.management.frontend;

import eu.rarogsoftware.commons.cache.CacheService;

import javax.cache.Cache;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public abstract class AbstractDynamicAssetsLoaderFactory implements DynamicAssetsLoaderFactory {
    protected final Cache<String, Optional<DynamicAssetsLoader>> loadersCache;
    protected final Long cacheDuration;
    protected final String manifestLocationTemplate;

    protected AbstractDynamicAssetsLoaderFactory(CacheService cacheService, Long cacheDuration, String manifestLocationTemplate) {
        this.cacheDuration = cacheDuration;
        this.manifestLocationTemplate = manifestLocationTemplate;
        var cacheBuilder = cacheService.<String, Optional<DynamicAssetsLoader>>getBuilder()
                .name(getCacheName())
                .storeByValue(false);
        if (this.cacheDuration >= 0) {
            cacheBuilder
                    .expireAfterAccess(this.cacheDuration, TimeUnit.SECONDS);
        } else {
            cacheBuilder
                    .eternal();
        }
        this.loadersCache = cacheBuilder.build();
    }

    protected String getCacheName() {
        return getClass().getName() + "_cache";
    }

    @Override
    public Optional<DynamicAssetsLoader> getNamedAssetsLoader(String name) {
        var cachedLoader = loadersCache.get(name);
        if (cachedLoader == null) {
            var loader = createCacheLoader(name);
            loadersCache.put(name, loader);
            return loader;
        }
        return cachedLoader;
    }

    protected abstract Optional<DynamicAssetsLoader> createCacheLoader(String name);
}
