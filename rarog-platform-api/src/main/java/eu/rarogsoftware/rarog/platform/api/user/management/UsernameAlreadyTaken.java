package eu.rarogsoftware.rarog.platform.api.user.management;

public class UsernameAlreadyTaken extends UserManagementException {
    public UsernameAlreadyTaken() {
    }

    public UsernameAlreadyTaken(String message) {
        super(message);
    }

    public UsernameAlreadyTaken(String message, Throwable cause) {
        super(message, cause);
    }

    public UsernameAlreadyTaken(Throwable cause) {
        super(cause);
    }

    public UsernameAlreadyTaken(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
