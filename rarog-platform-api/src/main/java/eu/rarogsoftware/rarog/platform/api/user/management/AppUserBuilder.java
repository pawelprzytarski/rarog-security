package eu.rarogsoftware.rarog.platform.api.user.management;

import org.springframework.security.core.GrantedAuthority;

import java.util.HashSet;
import java.util.Set;

public class AppUserBuilder {
    private String appName;
    private Set<GrantedAuthority> authorities = new HashSet<>();
    private boolean enabled = true;

    public AppUserBuilder appName(String appName) {
        this.appName = appName;
        return this;
    }

    public AppUserBuilder authorities(Set<GrantedAuthority> authorities) {
        this.authorities = authorities;
        return this;
    }

    public AppUserBuilder enabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public AppUser build() {
        return new AppUser(appName, authorities, enabled);
    }
}