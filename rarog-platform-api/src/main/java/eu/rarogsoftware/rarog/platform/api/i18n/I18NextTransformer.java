package eu.rarogsoftware.rarog.platform.api.i18n;

import java.util.Locale;

/**
 * Transform i18Next resource before they are returned for client app or transformed into {@link java.util.ResourceBundle}
 * It must take json* on input and valid i18Next json on output.
 */
public interface I18NextTransformer {
    /**
     * Transforms i18Next resource
     *
     * @param namespace       namaspace of resource
     * @param locale          locale of resource
     * @param jsonToTransform json resource to transform
     * @return transformed json
     */
    String transform(String namespace, Locale locale, String jsonToTransform);
}
