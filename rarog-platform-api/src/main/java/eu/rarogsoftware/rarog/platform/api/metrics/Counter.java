package eu.rarogsoftware.rarog.platform.api.metrics;

/**
 * Counter metric to track counts of events or running totals.
 *
 * <p>
 * Counters can only go up, if your use case can go down you should use a {@link Gauge} instead.
 * </p>
 */
public interface Counter extends BasicMetric<Counter> {
    /**
     * Increase value of counter.
     *
     * @param amount amount to add to counter value
     */
    void increase(double amount);

    /**
     * Increase value of counter by 1.
     */
    default void increment() {
        increase(1.0);
    }
}
