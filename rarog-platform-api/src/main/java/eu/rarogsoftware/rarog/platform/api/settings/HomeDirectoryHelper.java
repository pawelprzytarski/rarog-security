package eu.rarogsoftware.rarog.platform.api.settings;

import java.io.File;

public interface HomeDirectoryHelper {
    String getHome();

    File getHomeFile();

    String getConfig();

    File getConfigFile();

    String getLogs();

    File getLogsFile();

    String getCache();

    File getCacheFile();

    String getPlugins();

    File getPluginsFile();
}
