package eu.rarogsoftware.rarog.platform.api.plugins.database;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;

public record DatabaseMigrationsDescriptor(String changelogFile) implements FeatureDescriptor {

}
