package eu.rarogsoftware.rarog.platform.api.plugins.web.servlets;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.web.csrf.CsrfToken;

public final class CsrfHelper {
    public static final String DISABLE_XSRF_ATTRIBUTE = "X-CSRF-DISABLE";

    private CsrfHelper() {
    }

    public static CsrfToken csrfToken(HttpServletRequest request) {
        return (CsrfToken) request.getAttribute("_csrf");
    }
}
