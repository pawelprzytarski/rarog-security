package eu.rarogsoftware.rarog.platform.api.security.secrets;

import java.util.Map;

/**
 * Base interface for all services that helps to get stored credentials.
 * <p>
 * All classes implementing this interface are intended to help resolve authentication configurations that
 * can be either stored as single string or list of pair key->value.
 * <p>
 * Authentication string must be follow syntax: {@code <method>:<method_data>}.
 * Where {@code <method>} is one of supported authentication methods for {@link java.net.http.HttpClient}
 * and {@code <method_data>} are data necessary for this method. Parameters follow syntax
 * {@code <parameter1>=<value1>,<parameter2>=<value2>}, unless specified otherwise. Special characters `,=`
 * in parameters can be escaped with backslash character `\`. Double backslash `\\` will be
 * translated to single backslash.
 * <p>
 * Alternative format for authentication data is parameters map. It supports any kind of data. It is used primary
 * for credentials data, that cannot be stored as authentication string and for internal purposes.
 * Each parameters map have required entry {@code method} which is the equivalent of method from authentication string.
 * All other data in map translates to method parameters.
 * <p>
 * See details of each service for more info. Do not implement this interface of your own,
 * it exists for technical reasons.
 */
public interface AuthConfigService<T> {
    T resolveFromString(String authenticationData);

    T resolveFromSettings(Map<String, Object> authenticationData);
}
