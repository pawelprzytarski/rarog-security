package eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui;

import java.util.List;
import java.util.Objects;

/**
 * Represents single menu element that will be presented on account settings page for users.
 * <p>
 * NOTE: elements are showed in alphabetical order under each category and categories are also sort alphabetically
 *
 * @param key       unique setting key used to identify element (conflict may cause unexpected effects)
 * @param category  category under which element will be displayed (Rarog will use this name as translation key).
 *                  Empty string for no category.
 * @param name      translation key for name
 * @param resources list of relative urls to js script that should be loaded when menu item is selected. Exactly one
 *                  item should be module that returns react component.
 */
public record AccountSettingsMenuElement(String key, String category, String name, List<String> resources) {
    public AccountSettingsMenuElement {
        Objects.requireNonNull(key);
        Objects.requireNonNull(category);
        Objects.requireNonNull(name);
        Objects.requireNonNull(resources);
    }
}

