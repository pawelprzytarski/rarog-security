package eu.rarogsoftware.rarog.platform.api.security.mfa;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;

import java.util.Collection;

/**
 * Service used to access info about MFA status in system and for current user.
 * Use this service if you are providing custom MFA UI.
 * For creating new MFA method see {@link MfaStore}.
 *
 * @see MfaStore
 * @see MfaEndpoint
 */
public interface MfaService {
    String EXCLUDE_REQUEST_FROM_MFA_ATTRIBUTE = "rarog.mfa.authentication.request.excluded";
    boolean isMfaEnabled();

    boolean isMfaForced();

    boolean isMfaEnabledForUser(StandardUser user);

    boolean isMfaConfiguredForUser(StandardUser user);

    Collection<MfaMethod> getMethods();

    Collection<MfaMethod> getMethodsForUser(StandardUser standardUser);
}
