package eu.rarogsoftware.rarog.platform.api.security.anonymous;

/**
 * Levels of anonymous access that are determining which endpoints have disabled anonymous access,
 *  when admin disables system-wide anonymous access.
 */
public enum AnonymousAllowedLevel {
    /**
     * Endpoints annotated as CORE endpoints will never have disabled anonymous.
     * It's level reserved to endpoints that are necessary for system to work, like `/status` endpoint.
     * Endpoints of this level cannot leak any user data or system configuration that can be considered
     * confidential.
     */
    CORE,
    /**
     * Default level of anonymous access for endpoints.
     * Endpoints of this type provide some functionality that is not necessary for system to work,
     * but disabling them may cause some functionalities or integrations to stop working.
     * Endpoints of this level must strictly control what kind of data they expose.
     */
    BASIC,
    /**
     * Endpoints with this level provide functionalities for users that don't have account in system.
     * These endpoints usually expose data that may be undesirable to be exposed when no anonymous users
     * are intended in system.
     * Endpoints of this level are intended to provide functionality for anonymous users and can be safely
     * disabled, or they work correctly for logged-in users.
     */
    OPTIONAL
}
