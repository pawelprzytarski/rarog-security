package eu.rarogsoftware.rarog.platform.api.resources.management.frontend;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@SuppressFBWarnings("PATH_TRAVERSAL_IN")
public class FilesystemDynamicAssetsLoader extends AbstractResourceBasedDynamicAssetsLoader {

    public FilesystemDynamicAssetsLoader(String manifestLocation, Long cacheDuration) {
        super(manifestLocation, cacheDuration);
    }

    @Override
    protected InputStream getManifestResourceStream() throws FileNotFoundException {
        return new FileInputStream(manifestLocation);
    }

}
