package eu.rarogsoftware.rarog.platform.api.plugins.web.filters;

import jakarta.servlet.DispatcherType;
import jakarta.servlet.Filter;

import java.util.List;
import java.util.Set;

/**
 * Simple mapping, which allows to associate the URL pattern with the filters.
 * The filters will be chained in order they are defined.
 * <code>org.springframework.security.web.util.matcher.AntPathRequestMatcher</code> is used to match the patterns.
 *
 * @param filter      The {@link Filter} instance which will be registered in the chain
 * @param dispatchers The {@link Set} of {@link DispatcherType}s for which the filter will be invoked.
 * @param urlPatterns URL patterns which control when the filter is invoked.
 */
public record PluginFilterMapping(Filter filter, Set<DispatcherType> dispatchers, List<String> urlPatterns) {
}
