package eu.rarogsoftware.rarog.platform.api.plugins;

/**
 * Object representing plugin manifest that contains basic info about plugin.
 *
 * @param key unique plugin key - plugins with the same key are recognized as different version of the same plugin
 * @param name human-readable plugin name
 * @param version plugin version
 * @param description human-readable plugin description
 * @param image url to image to be shown in list of plugins
 */
public record PluginManifest(String key, String name, String version, String description, String image) {
}
