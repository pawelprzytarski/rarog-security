package eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;

import java.util.List;

/**
 * Gives possibility for plugins to define their own pages in admin panel.
 *
 * @param elements list of menu elements to add to admin panel navigation
 */
public record AdminPanelItemsDescriptor(List<AdminPanelItem> elements) implements FeatureDescriptor {
}
