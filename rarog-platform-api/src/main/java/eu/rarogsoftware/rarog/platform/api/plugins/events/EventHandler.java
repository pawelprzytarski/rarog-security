package eu.rarogsoftware.rarog.platform.api.plugins.events;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Used with {@link EventListener} interface to create classes listening for events.
 * Single argument methods within classes implementing {@link EventListener} can be marked
 * with this annotation.
 * Annotated methods will be invoked if the event of the parameter type (or its subclass)
 * is published to {@link EventService}.
 * <p>
 * Example usage:
 *
 * <pre>
 * {@code
 * &#64;Component
 * public class FuncTestEventListener implements EventListener {
 *
 *     private final Logger logger = LoggerFactory.getLogger(FuncTestEventListener.class);
 *
 *     &#64;EventHandler
 *     public void onFuncTestEvent(FuncTestEvent event) {
 *         logger.debug("Received FuncTestEvent yay. Count: {}");
 *     }
 *
 *     &#64;EventHandler
 *     public void onStringEvent(String event) {
 *         logger.debug("Received String yay. Count: {}");
 *     }
 * }
 * }
 * </pre>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface EventHandler {
}
