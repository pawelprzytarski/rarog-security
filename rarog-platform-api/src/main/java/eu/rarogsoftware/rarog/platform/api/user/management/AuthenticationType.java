package eu.rarogsoftware.rarog.platform.api.user.management;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public enum AuthenticationType {
    ORGANIC("ORGANIC"),
    SYNTHETIC("SYNTHETIC");

    private final String role;

    AuthenticationType(String role) {
        this.role = role;
    }


    public String getRole() {
        return role;
    }

    public GrantedAuthority getAuthority() {
        return new SimpleGrantedAuthority("AUTH_" + role);
    }
}
