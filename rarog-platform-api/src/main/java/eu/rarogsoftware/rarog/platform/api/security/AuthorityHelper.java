package eu.rarogsoftware.rarog.platform.api.security;

import eu.rarogsoftware.rarog.platform.api.user.management.RarogUser;
import eu.rarogsoftware.rarog.platform.api.user.management.UserRole;

public final class AuthorityHelper {
    private AuthorityHelper() {
    }

    public static boolean hasAdminRights(RarogUser rarogUser) {
        return rarogUser.getAuthorities()
                .stream().anyMatch(authority -> UserRole.ADMINISTRATOR.getAuthority().getAuthority().equals(authority.getAuthority())
                        || UserRole.SYSTEM_ADMINISTRATOR.getAuthority().getAuthority().equals(authority.getAuthority()));
    }

    public static boolean hasSystemAdminRights(RarogUser rarogUser) {
        return rarogUser.getAuthorities()
                .stream().anyMatch(authority -> UserRole.SYSTEM_ADMINISTRATOR.getAuthority().getAuthority().equals(authority.getAuthority()));
    }
}
