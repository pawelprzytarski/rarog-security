package eu.rarogsoftware.rarog.platform.api.task;

import java.util.concurrent.*;

/**
 * An {@link ScheduledExecutorService} that can support advanced commands
 * likes repeats count, auto cancel conditions and run conditions.
 *
 * <p>The {@code scheduleAdvanced} methods create tasks using advanced
 * config {@link ScheduleConfig} which defines advance behaviour of task
 * and returns a task object that can be used to cancel or check execution.
 * If specified in {@link ScheduleConfig} task will repeat until cancelled
 * or config conditions are met.
 * <p>
 * All other methods work the same way they work in {@link ScheduledExecutorService}.
 *
 * <p>All {@code schedule} methods accept <em>relative</em> delays and
 * periods as arguments, not absolute times or dates. It is a simple
 * matter to transform an absolute time represented as a {@link
 * java.util.Date} to the required form. For example, to schedule at
 * a certain future {@code date}, you can use: {@code schedule(task,
 * date.getTime() - System.currentTimeMillis(),
 * TimeUnit.MILLISECONDS)}. Beware however that expiration of a
 * relative delay need not coincide with the current {@code Date} at
 * which the task is enabled due to network time synchronization
 * protocols, clock drift, or other factors.
 *
 * <h2>Usage Example</h2>
 * <p>
 * Here is a class with a method that sets up a AdvancedScheduledExecutorService
 * to beep every ten seconds for an hour:
 *
 * <pre> {@code
 * import static java.util.concurrent.TimeUnit.*;
 * class BeeperControl {
 *   private final ScheduledExecutorService scheduler =
 *     Executors.newScheduledThreadPool(1);
 *
 *   public void beepForAnHour() {
 *     Runnable beeper = () -> System.out.println("beep");
 *     scheduler.scheduleAdvanced(beeper, ScheduleConfig.builder()
 *                         .period(Duration.ofSeconds(10))
 *                         .repeatCount(360)
 *                 .createSchedulerConfig());
 *   }
 * }}</pre>
 */
public interface AdvancedScheduledExecutorService extends ScheduledExecutorService {
    /**
     * Submits an action with advanced config that becomes enabled first after the
     * given initial delay, and subsequently with the given period or with
     * given time after previous execution. Repeats count may be limited
     * if provided such information in config.
     *
     * <p>The sequence of task executions continues indefinitely until
     * one of the following completions occur:
     * <ul>
     * <li>Repeat count are reached
     * <li>Condition of specified predicate for cancellation was met
     * <li>The task is {@linkplain Future#cancel explicitly cancelled}
     * via the returned future.
     * <li>The executor terminates, also resulting in task cancellation.
     * <li>An execution of the task throws an exception.  In this case
     * calling {@link Future#get() get} on the returned future will throw
     * {@link ExecutionException}, holding the exception as its cause.
     * </ul>
     * Subsequent executions are suppressed.  Subsequent calls to
     * {@link Future#isDone isDone()} on the returned future will
     * return {@code true}.
     *
     * <p>If any execution of this task takes longer than its period, then
     * subsequent executions may start late, but will not concurrently
     * execute.
     * <p>If task is not executed due to provided in config predicate, then
     * repeat count does not go up. Repeats are only counted for execution that
     * took place. So if task is specified to repeat 5 time, it will be run 5 times.
     * For example: if task is scheduled to repeat each 10 second 5 times,
     * and will execute 3 times, then execution predicate will prevent it from execution
     * for 1h. The remaining 2 times will be executed after this 1h.
     *
     * @param command the task to execute
     * @param config  the config of task
     * @return a ScheduledFuture representing pending completion of
     * the series of repeated tasks.  The future's {@link
     * Future#get() get()} method will never return normally,
     * and will throw an exception upon task cancellation or
     * abnormal termination of a task execution.
     * @throws RejectedExecutionException if the task cannot be
     *                                    scheduled for execution
     * @throws NullPointerException       if command or config is null
     */
    ScheduledFuture<?> scheduleAdvanced(Runnable command, ScheduleConfig<?> config);

    /**
     * Submits an action with advanced config that becomes enabled first after the
     * given initial delay, and subsequently with the given period or with
     * given time after previous execution. Repeats count may be limited
     * if provided such information in config.
     *
     * <p>The sequence of task executions continues indefinitely until
     * one of the following completions occur:
     * <ul>
     * <li>Repeat count are reached
     * <li>Condition of specified predicate for cancellation was met
     * <li>The task is {@linkplain Future#cancel explicitly cancelled}
     * via the returned future.
     * <li>The executor terminates, also resulting in task cancellation.
     * <li>An execution of the task throws an exception.  In this case
     * calling {@link Future#get() get} on the returned future will throw
     * {@link ExecutionException}, holding the exception as its cause.
     * </ul>
     * Subsequent executions are suppressed.  Subsequent calls to
     * {@link Future#isDone isDone()} on the returned future will
     * return {@code true}.
     *
     * <p>If any execution of this task takes longer than its period, then
     * subsequent executions may start late, but will not concurrently
     * execute.
     * <p>If task is not executed due to provided in config predicate, then
     * repeat count does not go up. Repeats are only counted for execution that
     * took place. So if task is specified to repeat 5 time, it will be run 5 times.
     * For example: if task is scheduled to repeat each 10 second 5 times,
     * and will execute 3 times, then execution predicate will prevent it from execution
     * for 1h. The remaining 2 times will be executed after this 1h.
     *
     * @param command the task to execute
     * @param config  the config of task
     * @return a ScheduledFuture representing pending completion of
     * the series of repeated tasks.  The future's {@link
     * Future#get() get()} method will never return normally,
     * and will throw an exception upon task cancellation or
     * abnormal termination of a task execution.
     * @throws RejectedExecutionException if the task cannot be
     *                                    scheduled for execution
     * @throws NullPointerException       if command or config is null
     */
    <T> ScheduledFuture<T> scheduleAdvanced(Callable<T> command, ScheduleConfig<T> config);
}
