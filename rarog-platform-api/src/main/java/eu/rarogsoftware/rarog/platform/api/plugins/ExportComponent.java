package eu.rarogsoftware.rarog.platform.api.plugins;

import java.lang.annotation.*;

/**
 * Information for bridge between Spring context and OSGi system which beans should be exported
 * as services to OSGi.
 *
 * <p>
 * Components annotated with this annotation will be automatically exported to OSGi in rarog-server module
 * and in plugin that utilize spring-plugin-activator library
 * </p>
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ExportComponent {
    String value() default "";

    Class<?>[] interfaces() default {};
}
