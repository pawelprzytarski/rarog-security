package eu.rarogsoftware.rarog.platform.api.security.secrets;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.util.Map;

/**
 * This service enables support for stored credentials when using {@link java.net.http.HttpClient}.
 * <p>
 * Service resolves provided authentication data to valid visitor of type {@link AuthConfig},
 * which when applied on {@link java.net.http.HttpClient} and {@link java.net.http.HttpRequest},
 * adds all necessary credentials to request and client. It removes burden of managing credentials
 * from developers and move it to system.
 * <p>
 * Authentication string must follow syntax: {@code <method>:<method_data>}.
 * Where {@code <method>} is one of supported authentication methods for {@link java.net.http.HttpClient}
 * and {@code <method_data>} are data necessary for this method. Parameters follow syntax
 * {@code <parameter1>=<value1>,<parameter2>=<value2>}, unless specified otherwise. Special characters `,=`
 * in parameters can be escaped with backslash character `\`. Double backslash `\\` will be
 * translated to single backslash.
 * <p>
 * Alternative format for authentication data is parameters map. It supports any kind of data. It is used primary
 * for credentials data, that cannot be stored as authentication string and for internal purposes.
 * Each parameters map have required entry {@code method} which is the equivalent of method from authentication string.
 * All other data in map translates to method parameters.
 * <p>
 * Currently supported auth methods are:
 * <ul>
 *     <li>{@code client_cert} - which gives support for TLS mutual authentication.</li>
 *     <li>{@code auth} which - gives support for {@code Authentication} header.</li>
 *     <li>{@code build_in} - which resolves to one of other methods for which configuration is embedded into system.</li>
 *     <li>{@code vault} - which resolves to one of other methods for which configuration is stored in secure vault (if supported).</li>
 * </ul>
 * <p>
 * Authentication method {@code client_cert} supports following parameters:
 * <ul>
 *     <li>{@code cert} - which should point to X.509 certificate file (required)</li>
 *     <li>{@code privkey} - which should point to PKCS8 private key file (required)</li>
 *     <li>{@code password_file} - which should point to file with password to certificate or private key (optional)</li>
 *     <li>{@code password} - which should be password to certificate or private key (optional)</li>
 *     <li>{@code cert_bytes} - certificate passed as byte array (alternative to {@code cert}). Supported only in parameters map.</li>
 *     <li>{@code privkey_bytes} - private key passed as byte array (alternative to {@code privkey}). Supported only in parameters map.</li>
 * </ul>
 * Example: `client_cert:cert_file=/ssl/app.crt,privatekey_file=/ssl/key.pem,password=MyPassword`
 * <p>
 * Authentication method {@code auth} do not support parameters syntax in auth string, but expects content of {@code Authorization} header
 * as parameter. Example `auth:Token your_token`. For parameters map use {@code value} parameter to pass header value.
 * <p>
 * Authentication method {@code vault} do not support parameters syntax in auth string, but expects storage key
 * of credential to retrieve from vault. For parameters map use {@code value} parameter to pass storage key.
 */
public interface HttpClientAuthConfigService extends AuthConfigService<HttpClientAuthConfigService.AuthConfig> {
    /**
     * Creates instance of helper {@link AuthConfig} based on provided auth string.
     * Auth string must have format {@code <method>:<method_data>}. See interface javadoc for details.
     *
     * @param authenticationData auth string containing settings
     * @return configured {@link AuthConfig} object
     */
    @Override
    AuthConfig resolveFromString(String authenticationData);

    /**
     * Creates instance of helper {@link AuthConfig} based on provided auth data.
     *
     * @param authenticationData map of auth data. See interface javadoc for details.
     * @return configured {@link AuthConfig} object
     */
    @Override
    AuthConfig resolveFromSettings(Map<String, Object> authenticationData);

    /**
     * Configurator for Java11 HttpClient
     */
    interface AuthConfig {
        default HttpClient.Builder configureHttpClient(HttpClient.Builder builder) {
            return builder;
        }

        default HttpRequest configureHttpRequest(HttpRequest request) {
            return request;
        }
    }
}