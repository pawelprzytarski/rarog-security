package eu.rarogsoftware.rarog.platform.api.plugins;

/**
 * Represents single pluggable functionality in system.
 * Plugin can expose its functionality for other plugins with this interface.
 * Plugin can configure and/or provide resources for module with {@link FeatureDescriptor}
 * FeatureDescriptor are automatically plugged/unplugged from system. There is no option to manually
 * manipulate FeatureModules.
 *
 * @param <T> type of supported {@link FeatureDescriptor}
 */
public interface FeatureModule<T extends FeatureDescriptor> {
    /**
     * Registers new descriptor into module.
     * Module will use resources represented by descriptor to provide its functionality
     * as long as descriptor is unplugged.
     *
     * @param plugin     plugin that is parent of descriptor
     * @param descriptor feature descriptor to plug
     */
    void plugDescriptor(Plugin plugin, T descriptor);

    /**
     * Unregisters new descriptor into module.
     * Module will no longer use resources provided by specified descriptor
     *
     * @param plugin     plugin that is parent of descriptor
     * @param descriptor feature descriptor to unplug
     */
    void unplugDescriptor(Plugin plugin, T descriptor);
}
