package eu.rarogsoftware.rarog.platform.api.plugins.web;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import org.springframework.web.servlet.HandlerMapping;

import java.util.Collection;

/**
 * Registers spring based {@link HandlerMapping}s under specified base url.
 *
 * <p>
 *     Used to register custom spring based endpoints. It allows to develop endpoints with Spring MVC as in any other Spring
 *     application. The base usage example with spring-plugin-activator that automatically exports feature descriptors:
 *
 * <pre>
 * @ Configuration
 * public class PluginConfiguration {
 *       @ Bean("restRequestMappingHandlerMapping")
 *       RequestMappingHandlerMapping requestMappingHandlerMapping() {
 *           RequestMappingHandlerMapping requestMappingHandlerMapping = new RequestMappingHandlerMapping();
 *           requestMappingHandlerMapping.setPathPrefixes(Map.of(
 *           "/plugin/backdoor/", clazz -> !MergedAnnotations.from(clazz.getAnnotations()).isPresent(RestController.class),
 *           "/rest/backdoor/1.0/", clazz -> MergedAnnotations.from(clazz.getAnnotations()).isPresent(RestController.class)));
 *           return requestMappingHandlerMapping;
 *       }
 *
 *        public HandlerMappingDescriptor handlerMappingDescriptor(List<HandlerMapping> handlerMappings) {
 *              return new HandlerMappingDescriptor("/backdoor/", handlerMappings, true);
 *        }
 *
 * </pre>
 * </p>
 *
 * @param baseUrl base url part used for matching requests that should be handled by specified mappings. Will map urls:
 *                [/rest/{baseUrl}/, /plugin/{baseUrl}/]
 * @param handlerMappings collection of {@link HandlerMapping}
 * @param exactMatchOnly if true mapping in provided handlerMappings must match full url, if false mapping can skip base url
 */
public record HandlerMappingDescriptor(String baseUrl,
                                       Collection<HandlerMapping> handlerMappings,
                                       boolean exactMatchOnly) implements FeatureDescriptor {
    public HandlerMappingDescriptor {
        if (!baseUrl.startsWith("/") || !baseUrl.endsWith("/")) {
            throw new IllegalArgumentException("baseUrl doesn't start or/and doesn't end with slash '/'");
        }
    }

    public HandlerMappingDescriptor(String baseUrl, Collection<HandlerMapping> handlerMappings) {
        this(baseUrl, handlerMappings, false);
    }

    @Override
    public String getName() {
        return FeatureDescriptor.super.getName() + ":" + baseUrl;
    }
}
