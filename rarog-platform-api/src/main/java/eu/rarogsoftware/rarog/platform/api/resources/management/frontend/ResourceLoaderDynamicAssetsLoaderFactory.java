package eu.rarogsoftware.rarog.platform.api.resources.management.frontend;

import eu.rarogsoftware.commons.cache.CacheService;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public class ResourceLoaderDynamicAssetsLoaderFactory extends AbstractDynamicAssetsLoaderFactory {
    private final ResourceLoader resourceLoader;

    public ResourceLoaderDynamicAssetsLoaderFactory(ResourceLoader resourceLoader,
                                                    CacheService cacheService,
                                                    Long cacheDuration,
                                                    String manifestLocationTemplate) {
        super(cacheService, cacheDuration, manifestLocationTemplate);
        this.resourceLoader = resourceLoader;
    }

    @Override
    protected Optional<DynamicAssetsLoader> createCacheLoader(String name) {
        var location = manifestLocationTemplate.replace("{namespace}", name);
        var resource = resourceLoader.getResource(location);
        if (resource.exists()) {
            return Optional.of(new AbstractResourceBasedDynamicAssetsLoader(location, 0L) {
                @Override
                protected InputStream getManifestResourceStream() throws IOException {
                    return resource.getInputStream();
                }
            });
        }
        return Optional.empty();
    }
}
