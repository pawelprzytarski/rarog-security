package eu.rarogsoftware.rarog.platform.api.plugins;

import java.util.Optional;

/**
 * Interface allowing to persist state of installed plugins between applications startups.
 */
public interface PluginsStateStore {

    /**
     * When called specified plugin state is persisted ina a non-volatile way.
     *
     * @param plugin plugin which state is to be saved.
     * @param pluginState plugin's state to be persisted.
     */
    void storePluginState(Plugin plugin, Plugin.PluginState pluginState);

    /**
     * Retrieves last saved plugin.
     *
     * @param plugin plugin which state is to be retrieved.
     * @return last saved state of passed plugin.
     */
    Optional<Plugin.PluginState> getPluginState(Plugin plugin);
}
