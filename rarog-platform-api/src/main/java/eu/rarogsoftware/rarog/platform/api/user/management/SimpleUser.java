package eu.rarogsoftware.rarog.platform.api.user.management;

import java.util.Set;

/**
 * The simplest user record representing single user instance.
 */
public record SimpleUser(Long id,
                         String username,
                         String passwordHash,
                         boolean active,
                         int mfaLevel,
                         Set<UserRole> roles) {

    public SimpleUser {
    }

    public SimpleUser(String username, String passwordHash, boolean active, int mfaLevel, Set<UserRole> roles) {
        this(null, username, passwordHash, active, mfaLevel, roles);
    }
}
