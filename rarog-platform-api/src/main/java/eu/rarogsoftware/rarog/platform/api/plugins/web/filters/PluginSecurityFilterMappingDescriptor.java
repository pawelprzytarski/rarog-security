package eu.rarogsoftware.rarog.platform.api.plugins.web.filters;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;

import java.util.List;

/**
 * Descriptor that stores all the {@link PluginSecurityFilterMapping}s for the plugin.
 * There can be only one such descriptor bean defined in the plugin configuration.
 *
 * @param filterMappings The list of filter mappings defined in plugin.
 */
public record PluginSecurityFilterMappingDescriptor(
        List<PluginSecurityFilterMapping> filterMappings) implements FeatureDescriptor {
}
