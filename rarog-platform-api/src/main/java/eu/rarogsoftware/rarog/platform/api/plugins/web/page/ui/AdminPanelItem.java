package eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui;

import java.util.Objects;

/**
 * Represents single page that will be navigable and will be display in admin panel sidebar nav.
 * <p>
 * NOTE: elements are showed in alphabetical order under each category and categories are also sort alphabetically
 *
 * @param key      unique (under the same category) page key used to identify element. It is used to create URL address of page (conflict may cause unexpected effects).
 *                 It can have special value '*' (match all) which makes it to match all addresses under category address.
 *                 NOTE: match all value must be always be embedded (embedded set to true) and is hidden from navigation.
 * @param category category under which element will be displayed. Category is hierarchical and contains of other elements
 *                 - slash character '/' is used to separate categories levels.
 * @param name     translation key for name
 * @param resource relative link to resource to be displayed or null if item is category without page
 * @param embedded valid only for react components. Tells react app to render component as child of component rendered
 *                 by category resource. If false component will be rendered in place of parent category component.
 *                 Leave false for non-react components
 * @param keywords list of keywords (or translation keys for keywords) to be used for quick search functionality
 */
public record AdminPanelItem(String key,
                             String category,
                             String name,
                             String resource,
                             boolean embedded,
                             boolean navigationOnly,
                             String keywords) {
    public AdminPanelItem {
        Objects.requireNonNull(key);
        Objects.requireNonNull(category);
        Objects.requireNonNull(name);
        assert !"*".equals(key) || embedded;
    }

    public static AdminPanelItem simplePage(String key, String parentCategory, String name, String resource) {
        return simplePage(key, parentCategory, name, resource, null);
    }

    public static AdminPanelItem embeddedChild(String key, String parentCategory, String name, String resource) {
        return embeddedChild(key, parentCategory, name, resource, null);
    }

    public static AdminPanelItem embeddedChild(String key, String parentCategory, String name, String resource, String keywords) {
        return new AdminPanelItem(key, parentCategory, name, resource, true, false, keywords);
    }

    public static AdminPanelItem navLink(String key, String parentCategory, String name, String resource) {
        return navLink(key, parentCategory, name, resource, null);
    }

    public static AdminPanelItem navLink(String key, String parentCategory, String name, String resource, String keywords) {
        return new AdminPanelItem(key, parentCategory, name, resource, false, true, keywords);
    }

    public static AdminPanelItem simplePage(String key, String parentCategory, String name, String resource, String keywords) {
        return new AdminPanelItem(key, parentCategory, name, resource, false, false, keywords);
    }

    public static AdminPanelItem simpleCategory(String key, String parentCategory, String name) {
        return new AdminPanelItem(key, parentCategory, name, null, false, false, null);
    }
}

