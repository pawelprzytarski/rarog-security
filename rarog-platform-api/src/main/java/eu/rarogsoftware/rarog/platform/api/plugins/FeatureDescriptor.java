package eu.rarogsoftware.rarog.platform.api.plugins;

/**
 * Base interface for all FeatureDescriptors.
 * Each feature descriptor needs to implement this interface in order to be recognized by system.
 */
public interface FeatureDescriptor {
    /**
     * Unique name for descriptor in scope of plugin.
     * If descriptor can repeat, then different names need to be returned for each one.
     *
     * @return unique name of feature descriptor
     */
    default String getName() {
        return this.getClass().getName();
    }
}
