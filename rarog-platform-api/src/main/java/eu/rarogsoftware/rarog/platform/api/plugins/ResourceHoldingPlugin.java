package eu.rarogsoftware.rarog.platform.api.plugins;

import org.springframework.core.io.Resource;

import java.util.Collection;

/**
 * Represents plugin able to hold/provide resources
 */
public interface ResourceHoldingPlugin extends Plugin {
    /**
     * Retrieve single resource from plugin.
     * Method do not support wildcard characters.
     *
     * @param path path to resource
     * @return {@link Resource} or null if resource is not accessible or not found.
     */
    Resource getResource(String path);

    /**
     * Retrieve all resources from plugin that match provided path.
     * Method do support ant wildcard characters like '**'.
     *
     * @param path path to resources
     * @return collection of {@link Resource} or null if resources are not accessible or path is not correct.
     * @see org.springframework.util.AntPathMatcher
     */
    Collection<Resource> findResources(String path);
}
