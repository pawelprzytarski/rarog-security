package eu.rarogsoftware.rarog.platform.api.plugins.web.filters;

import jakarta.servlet.DispatcherType;
import jakarta.servlet.Filter;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * This convenience class which builds {@link PluginFilterMappingDescriptor}.
 * On match, the plugins will be executed in order they are defined.
 * Can add multiple mappings like in the example:
 * <pre>
 * &#64;Bean
 * public PluginFilterMappingDescriptor pluginFilterDescriptor() {
 * EnumSet&lt;DispatcherType&gt; dispatcherTypes = EnumSet.of(DispatcherType.REQUEST, DispatcherType.ASYNC, DispatcherType.FORWARD);
 *
 * return PluginFilterMappingDescriptorBuilder.newBuilder()
 * .registerFilter(new TestPluginFilter(), dispatcherTypes, "/status/**")
 * .registerFilter(new TestPluginFilter2(), dispatcherTypes, "/status/**", "/rest/func-test/**")
 * .build();
 * }
 * </pre>
 */
public class PluginFilterMappingDescriptorBuilder {

    private final List<PluginFilterMapping> mappings = new LinkedList<>();

    public static PluginFilterMappingDescriptorBuilder newBuilder() {
        return new PluginFilterMappingDescriptorBuilder();
    }

    public PluginFilterMappingDescriptorBuilder registerFilter(Filter filter, Set<DispatcherType> dispatchers, String... urlPatterns) {
        mappings.add(new PluginFilterMapping(filter, dispatchers, List.of(urlPatterns)));
        return this;
    }

    public PluginFilterMappingDescriptor build() {
        return new PluginFilterMappingDescriptor(mappings);
    }

    private PluginFilterMappingDescriptorBuilder() {
    }
}
