package eu.rarogsoftware.rarog.platform.api.metrics;

import java.util.Collection;
import java.util.Collections;

public record MetricsSnapshot(Collection<MetricSamples> samples, long timestamp) {
    public static MetricsSnapshot EMPTY_SNAPSHOT = new MetricsSnapshot(Collections.emptyList(), -1);
}
