package eu.rarogsoftware.rarog.platform.api.plugins;

import java.io.IOException;
import java.io.InputStream;

/**
 * Interface representing an artifact used to install the plugin.
 */
public interface PluginArtifact {
    /**
     * Returns the resource located in the artifact.
     *
     * @param location determines the resource to be returned
     * @return stream of the requested resource
     * @throws IOException when problems accessing the resource occur.
     */
    InputStream getResource(String location) throws IOException;

    /**
     * Retrieves a {@link PluginManifest} from the artifact.
     *
     * @return plugin's manifest
     * @throws IOException when problems accessing the artifact occur.
     * @throws PluginException when the artifact doesn't contain a valid manifest.
     */
    PluginManifest getManifest() throws IOException, PluginException;

    /**
     * Returns a string identifying the artifact.
     *
     * @return string identifying the artifact e.g. 'jar:/path/to/file.jar'
     */
    String getId();

    /**
     * Checks if the underlying resource actually exists.
     *
     * @return does the underlying resource actually exists.
     */
    boolean exists();
}
