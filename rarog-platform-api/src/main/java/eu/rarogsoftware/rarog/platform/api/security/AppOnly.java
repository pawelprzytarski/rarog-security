package eu.rarogsoftware.rarog.platform.api.security;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.*;

/**
 * Enforces access restriction to endpoint(s) to users that are applications.
 * <p>
 * It enforce role requirements with {@link PreAuthorize} using Spring login.
 * </p>
 * <p>
 * Code annotated with {@link AppOnly}
 * <pre>
 *          &#064;GetMapping("test")
 *          &#064;HumanOnly
 *          ResponseEntity testMethod()
 *     </pre>
 * is equivalent of code annotated with both {@link PreAuthorize}
 * <pre>
 *          &#064;GetMapping("test")
 *          &#064;PreAuthorize("hasRole('ROLE_APP')")
 *          &#064;SecurityRequirement(name = "admin")
 *          ResponseEntity testMethod()
 *     </pre>
 * </p>
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@PreAuthorize("hasAuthority('TYPE_APP')")
public @interface AppOnly {
}
