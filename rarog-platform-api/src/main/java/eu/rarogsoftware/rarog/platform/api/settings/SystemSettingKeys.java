package eu.rarogsoftware.rarog.platform.api.settings;

public final class SystemSettingKeys {
    public static final String SYSTEM_ID_KEY = "rarog.system.id";
    public static final String BASE_URL = "rarog.system.base.url";
    public static final String FORCE_SYSTEM_API = "rarog.prevent.system.api.override";
    public static final String SELF_CALL_URL = "rarog.system.self.call.url";
    public static final String METRICS_ENABLED_KEY = "rarog.metrics.enabled";
    public static final String TASK_MANAGER_MAX_POOL_SIZE = "rarog.system.tasks.max.pool.size";

    private SystemSettingKeys() {
    }
}
