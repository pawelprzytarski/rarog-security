package eu.rarogsoftware.rarog.platform.api.security.mfa;

import eu.rarogsoftware.rarog.platform.api.plugins.ExportComponent;

import java.util.Set;

/**
 * SPI for Multi-Factor Authentication methods.
 * Implement and export this interface with {@link ExportComponent}
 * to provide list of mfa method provided by your plugin.
 * See {@link MfaMethod} to learn more.
 */
public interface MfaMethodsRegister {
    Set<MfaMethod> getMfaMethods();
}
