package eu.rarogsoftware.rarog.platform.api.metrics;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Arrays;
import java.util.Collection;

/**
 * Histogram metric settings.
 */
@JsonTypeName("histogramSettings")
public class HistogramSettings extends BaseMetricSettings<HistogramSettings> {
    private double[] buckets = null;

    /**
     * @return empty instance of settings object. Convenience method.
     */
    public static HistogramSettings settings() {
        return new HistogramSettings();
    }

    /**
     * Sets the upper bounds of buckets for the histogram.
     *
     * @param buckets upper bounds of buckets for the histogram
     * @return settings object
     */
    public HistogramSettings buckets(double... buckets) {
        this.buckets = buckets;
        return this;
    }

    /**
     * See {@link HistogramSettings#labels(String...)}.
     *
     * @param buckets upper bounds of buckets for the histogram
     * @return settings object
     */
    HistogramSettings buckets(Collection<Double> buckets) {
        return buckets(buckets.stream().mapToDouble(value -> value).toArray());
    }

    /**
     * @return upper bounds of buckets for the histogram
     */
    public double[] buckets() {
        return buckets;
    }

    @Override
    public String toString() {
        return "HistogramSettings{" +
                super.toString() +
                ",buckets=" + Arrays.toString(buckets) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HistogramSettings that)) return false;
        if (!super.equals(o)) return false;
        return Arrays.equals(buckets, that.buckets);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(buckets);
        return result;
    }
}
