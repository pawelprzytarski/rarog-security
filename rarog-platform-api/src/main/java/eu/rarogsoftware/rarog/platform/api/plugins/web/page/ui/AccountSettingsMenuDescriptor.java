package eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;

import java.util.List;

/**
 * Gives possibility for plugins to define their own menu element in account settings page.
 *
 * @param elements list of menu elements to add to account settings page
 */
public record AccountSettingsMenuDescriptor(List<AccountSettingsMenuElement> elements) implements FeatureDescriptor {
}
