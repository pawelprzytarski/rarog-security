package eu.rarogsoftware.rarog.platform.api.i18n;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import eu.rarogsoftware.rarog.platform.api.plugins.ResourceHoldingPlugin;

/**
 * Descriptor tells app that plugin exports i18Next translation under specified location.
 * Note that location is used for {@link ResourceHoldingPlugin#getResource(String)}.
 *
 * @param location location under which resources are residing
 * @param order    used during merging to tell which translations are more important
 */
public record I18NextResourceDescriptor(String location, int order) implements FeatureDescriptor {
    public I18NextResourceDescriptor {
    }

    public I18NextResourceDescriptor() {
        this("i18n", 100);
    }

    @Override
    public String getName() {
        return FeatureDescriptor.super.getName() + "-" + location;
    }

}
