package eu.rarogsoftware.rarog.platform.api.plugins;

/**
 * Represents single plugin installed in system.
 */
public interface Plugin {
    /**
     * @return unique plugin key
     */
    default String getKey() {
        return getManifest().key();
    }

    /**
     * @return plugin manifest
     */
    PluginManifest getManifest();

    /**
     * @return true if plugin is enabled but not yet activated, false otherwise
     */
    default boolean isEnabled() {
        return getState() == PluginState.ENABLED;
    }

    /**
     * @return true if plugin is activated, otherwise false
     */
    default boolean isActive() {
        return getState() == PluginState.ACTIVE;
    }

    /**
     * @return true if plugin is enabled or activated, false otherwise
     */
    default boolean isInUse() {
        return isEnabled() || isActive();
    }

    /**
     * @return true if plugin is in state that allows to install it, otherwise false
     */
    default boolean canBeEnabled() {
        return getState() == PluginState.ENABLED || getState() == PluginState.INSTALLED;
    }

    /**
     * @return current plugin state
     */
    PluginState getState();

    /**
     * Registers plugin state listener. Listener will be invoked on any change in plugin state
     * @param listener listener to register
     */
    void addPluginStateListener(PluginStateListener listener);

    /**
     * Unregisters plugin state listener
     * @param listener listener to unregister
     */
    void removePluginStateListener(PluginStateListener listener);

    /**
     * Represents internal state of plugin in system
     */
    enum PluginState {
        /**
         * Plugin is installed in system and is ready to be enabled.
         */
        INSTALLED,
        /**
         * Plugin is preparing to be serving functionality to other plugins.
         */
        ENABLING,
        /**
         * Plugin is serving functionality to other plugins, but it is not serving any functionality for users.
         */
        ENABLED,
        /**
         * Plugin is enabled and serving functionality for users.
         */
        ACTIVE,
        /**
         * Plugin is going down and is no longer serving any functionality.
         */
        DISABLING,
        /**
         * Plugin is awaiting removal from system and cannot be enabled.
         */
        UNINSTALLED;
    }
}

