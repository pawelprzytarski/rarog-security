package eu.rarogsoftware.rarog.platform.api.plugins;

/**
 * Listener for installed plugin state.
 * It is sent by <i>eu.rarogsoftware.rarog.platform.core.plugins.PluginManager</i> when plugin state changes.
 * Listener don't have power to change plugin state. It is only information.
 */
public interface PluginStateListener {
    void stateChanged(Plugin.PluginState oldState, Plugin.PluginState newState);
}
