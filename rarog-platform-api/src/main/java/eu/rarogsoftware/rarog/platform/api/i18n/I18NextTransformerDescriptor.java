package eu.rarogsoftware.rarog.platform.api.i18n;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;

/**
 * Descriptor used to provide {@link I18NextTransformer} implementation to be used by app.
 *
 * @param i18NextTransformer transformer
 * @param id unique id (matters only if plugin specifies more than one transformer
 * @param order used to sort tranformers (if there is any need for transformers to work in order). It does not give any
 *              warranty that two tranformers will execute one after another. It only make transformers to not be execute ealier
 *              than other transformer.
 */
public record I18NextTransformerDescriptor(I18NextTransformer i18NextTransformer, String id,
                                           int order) implements FeatureDescriptor {
    public I18NextTransformerDescriptor {
    }

    public I18NextTransformerDescriptor(I18NextTransformer i18NextTransformer, int order) {
        this(i18NextTransformer, i18NextTransformer.getClass().getName(), order);
    }

    @Override
    public String getName() {
        return FeatureDescriptor.super.getName() + "-" + id();
    }
}
