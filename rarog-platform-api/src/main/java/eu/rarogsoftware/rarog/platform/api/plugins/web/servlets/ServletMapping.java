package eu.rarogsoftware.rarog.platform.api.plugins.web.servlets;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.http.HttpServlet;

import java.util.Collections;
import java.util.Map;

/**
 * Servlet mapping, which allows to associate the URL pattern with the servlet.
 * It is advanced mapping that allow not only to define url mapping but also define order of servlets. Plugin defined
 * servlets are designed to allow overriding and passing through.
 * <p>
 * Overriding works by utilizing order parameter. Servlets will lesser value of order are executed earlier,
 * and when two servlets have the same order, then the one with more specific path matcher is executed as first.
 * So it means if plugin with path mapping `/**` and order 1 will execute before all servlets with order 10. It
 * allows to override paths defined by other servlets, which can be useful for use cases like: overriding login form,
 * overriding MFA mechanism, etc. It is powerful tool, so consider using other tools before using servlets override.
 * <p>
 * Passing through is supplemental functionality to overriding. If plugin defined servlet throws {@link NotHandledException},
 * then request will be passed to the next servlet that matches request. For example if servlet matching `/login/**` throws
 * {@link NotHandledException} then {@link org.springframework.web.servlet.DispatcherServlet} will be invoked.
 * If all servlets matching request will throw {@link NotHandledException} then 404 Not Found error will be returned.
 * <p>
 * {@link org.springframework.web.servlet.DispatcherServlet} provided by spring, that supports all spring based controllers
 * (including ones defined by plugins) is defined as mapping `/**` with order 10, and it passes through all requests that don't
 * match any controller. So it is possible to define servlets that matches all not mapped requests by defining servlet
 * mapping `/**` with order 11.
 * <p>
 * There are implemented some security mechanisms that affects all requests like authentication or CSRF protection.
 * To enable some degree of control over these mechanisms provided servlet may implement control interfaces that are
 * available in package {@link eu.rarogsoftware.rarog.platform.api.plugins.web.servlets} and their name ends with `Aware`,
 * like {@link AuthorizationManagerAware} or {@link CsrfProtectionAware}.
 * Example:
 * <pre>
 * public class OverridingServlet extends HttpServlet implements AuthorizationManagerAware, CsrfProtectionAware {
 *     &#64;Override
 *     protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
 *         resp.setStatus(HttpServletResponse.SC_OK);
 *         resp.getWriter().write("Overriding servlet response");
 *     }
 *
 *     &#64;Override
 *     protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
 *         resp.setStatus(HttpServletResponse.SC_OK);
 *         resp.getWriter().write("Overriding servlet response");
 *     }
 *
 *     &#64;Override
 *     public Optional<AccessDecisionVoter> getAccessDecisionVoter(HttpServletRequest request) {
 *         return Optional.of(new AccessDecisionVoter() {
 *             &#64;Override
 *             public boolean supports(ConfigAttribute attribute) {
 *                 return true;
 *             }
 *
 *             &#64;Override
 *             public boolean supports(Class clazz) {
 *                 return true;
 *             }
 *
 *             &#64;Override
 *             public int vote(Authentication authentication, Object object, Collection collection) {
 *                 return ACCESS_GRANTED;
 *             }
 *         });
 *     }
 *
 *     &#64;Override
 *     public Optional<Boolean> shouldDisableCsrfProtection(HttpServletRequest request) {
 *         return Optional.of(true);
 *     }
 * }
 * </pre>
 * <p>
 * NOTE: <code>org.springframework.security.web.util.matcher.AntPathRequestMatcher</code> is used to match the patterns.
 *
 * @param pathMapping    ant path to match urls that will be handled by provided servlet
 * @param order          order of servlet used to define overriding order (negative values are also supported but not recommended)
 * @param servlet        servlet to be invoked when path is matched
 * @param initParameters parameters to be passed to {@link jakarta.servlet.Servlet#init(ServletConfig)} when servlet is initiated
 * @see NotHandledException
 * @see CsrfProtectionAware
 * @see AuthorizationManagerAware
 * @see CsrfHelper
 * @see PluginServletMappingDescriptor
 */
public record ServletMapping(String pathMapping, int order, HttpServlet servlet,
                             Map<String, String> initParameters) implements FeatureDescriptor {
    public static final int DEFAULT_ORDER = 10;

    public ServletMapping {
        if (pathMapping.equals("")) {
            pathMapping = "/**";
        }
    }

    public ServletMapping(String pathMapping, int order, HttpServlet servlet) {
        this(pathMapping, order, servlet, Collections.emptyMap());
    }

    public ServletMapping(String pathMapping, HttpServlet servlet) {
        this(pathMapping, DEFAULT_ORDER, servlet, Collections.emptyMap());
    }
}
