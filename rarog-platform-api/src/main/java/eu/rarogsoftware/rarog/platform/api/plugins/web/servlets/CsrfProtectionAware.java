package eu.rarogsoftware.rarog.platform.api.plugins.web.servlets;

import jakarta.servlet.http.HttpServletRequest;

import java.util.Optional;

/**
 * Extension for {@link jakarta.servlet.http.HttpServlet} defined with {@link PluginServletMappingDescriptor}, that
 * allows to disable build-in CSRF protection. It is usually helpful when CSRF protection is unwanted or servlet
 * defines its own CSRF protection. It supports the same design as {@link PluginServletMappingDescriptor},
 * with overriding and passing through. If servlet doesn't implement {@link CsrfProtectionAware} or
 * {@link CsrfProtectionAware#shouldDisableCsrfProtection(HttpServletRequest)} (HttpServletRequest)} returns {@link Optional#empty()},
 * then control is passed to the next servlet.
 * <p>
 */
public interface CsrfProtectionAware {
    /**
     * @param request request for which decision is taken
     * @return CSRF protection decision if servlet supports request and want to decide if it supports or not CSRF protection
     * or empty optional if servlet do not support request.
     */
    Optional<Boolean> shouldDisableCsrfProtection(HttpServletRequest request);
}
