package eu.rarogsoftware.rarog.platform.api.user.management;

public class UserManagementException extends Exception {
    public UserManagementException() {
    }

    public UserManagementException(String message) {
        super(message);
    }

    public UserManagementException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserManagementException(Throwable cause) {
        super(cause);
    }

    public UserManagementException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

