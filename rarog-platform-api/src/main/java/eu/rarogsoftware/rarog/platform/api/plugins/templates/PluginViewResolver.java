package eu.rarogsoftware.rarog.platform.api.plugins.templates;

import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

import java.util.Locale;

/**
 * Plugin request aware version of {@link ViewResolver}
 * Have additional method that accept {@link Plugin} in which context request is executed.
 */
public interface PluginViewResolver extends ViewResolver {
    /**
     * Resolve the given view by name.
     * See {@link ViewResolver#resolveViewName(String, Locale)} for more info.
     *
     * @param viewName name of the view to resolve
     * @param locale the Locale in which to resolve the view.
     * ViewResolvers that support internationalization should respect this.
     * @param plugin plugin in which context request is executed
     * @return the View object, or {@code null} if not found
     * (optional, to allow for ViewResolver chaining)
     * @throws Exception if the view cannot be resolved
     * (typically in case of problems creating an actual View object)
     */
    @Nullable
    View resolveViewName(String viewName, Locale locale, Plugin plugin) throws Exception;
}
