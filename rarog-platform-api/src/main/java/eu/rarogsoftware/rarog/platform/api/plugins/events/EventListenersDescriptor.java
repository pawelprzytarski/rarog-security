package eu.rarogsoftware.rarog.platform.api.plugins.events;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;

import java.util.Set;

/**
 * Feature descriptor used to register {@link java.util.EventListener}s within Rarog.
 * Usage example:
 * <pre>
 * {@code
 *     &#64;Bean
 *     public EventListenersDescriptor eventListenersDescriptor(List<EventListener> eventListeners) {
 *         return new EventListenersDescriptor(new HashSet<>(eventListeners));
 *     }
 * }
 * </pre>
 *
 * @param listenerBeans - set of classes containing {@link EventHandler}s.
 */
public record EventListenersDescriptor(Set<EventListener> listenerBeans) implements FeatureDescriptor {
}
