package eu.rarogsoftware.rarog.platform.api.security.mfa;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Service stores state of MFA authentication for user that sent related request.
 * It can be used by MFA methods to authenticate user or save {@link MfaStatus}
 * Status is stored between the same user requests.
 */
public interface MfaStore {
    /**
     * Gets current status of MFA authentication.
     * Useful to check if MFA is configured for current user or/and user bypassed MFA authentication.
     *
     * @param request related request (potentially current request)
     * @return status of MFA authentication
     */
    Optional<MfaStatus> getStatus(HttpServletRequest request);

    /**
     * Stores {@link MfaStatus#MFA_PASSED} in store for provided request.
     *
     * @param request related request (potentially current request)
     */
    void authorize(HttpServletRequest request);

    /**
     * Store custom MFA status.
     * Useful to inform system that MFA requires reconfiguration.
     *
     * @param request related request (potentially current request)
     * @param status  status to store for request
     */
    void saveStatus(HttpServletRequest request, MfaStatus status);
}
