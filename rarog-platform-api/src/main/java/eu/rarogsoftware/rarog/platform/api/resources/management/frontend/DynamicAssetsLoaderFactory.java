package eu.rarogsoftware.rarog.platform.api.resources.management.frontend;

import java.util.Optional;

/**
 * Loads {@link DynamicAssetsLoader} for specified plugin keys and namespaces.
 *
 * Any plugin can specify as many as it wants namespaces, which usually translate to single page. There no limit to
 * number of namespaces, but they are read from plugin as resources under path specified by {@link DynamicAssetsDescriptor}, which
 * naturally enforced uniqueness of namespaces, if only one path is declared. If more than one path is declared,
 * factory search for first of resources that matches namespace.
 *
 * Manifest details are described in javadoc for {@link DynamicAssetsLoader}.
 *
 * @see DynamicAssetsLoader
 * @see DynamicAssetsDescriptor
 */
public interface DynamicAssetsLoaderFactory {
    /**
     * Loads and returns {@link DynamicAssetsLoader} for specified plugin and namespace.
     *
     * @param name name for namespace that consist of plugin key and namespace key, separated by `|` (pipe) character.
     *             Example: `pluginGroup:pluginArtifact|namespace`
     * @return {@link DynamicAssetsLoader} for specified namespace or empty optional if no namespace was found
     */
    Optional<DynamicAssetsLoader> getNamedAssetsLoader(String name);
}
