package eu.rarogsoftware.rarog.platform.api.security.mfa;

import eu.rarogsoftware.rarog.platform.api.user.management.StandardUser;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Record representing single MFA method that exists in system.
 * This record contains all information needed to present MFA method to user.
 *
 * @param condition             predicate which indicate if user can choose method for authentication
 * @param name                  unique method name
 * @param nameKey               translation key for method
 * @param descriptionKey        translation key for method description
 * @param imageUrl              url of image used to represent method
 * @param order                 positive number used to sort methods
 * @param resource              url to authentication flow JS react component exported as ES6+ module
 * @param configurationResource url to configuration JS react component exported as ES6+ module
 * @param adminUrl              url to configuration page in admin panel. Used if adminResource not specified
 * @param adminResource         url to react component used to render configuration in admin panel
 */
public record MfaMethod(String imageUrl, String name, String nameKey, String descriptionKey, String resource,
                        Long order, Predicate<StandardUser> condition, String configurationResource,
                        String adminUrl, String adminResource) {

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String imageUrl;
        private String name;
        private String nameKey;
        private String descriptionKey;
        private String resource;
        private Long order = 100L;
        private String configurationResource;
        private String adminUrl;
        private String adminResource;
        private Predicate<StandardUser> condition = user -> true;

        public Builder imageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder nameKey(String nameKey) {
            this.nameKey = nameKey;
            return this;
        }

        public Builder descriptionKey(String descriptionKey) {
            this.descriptionKey = descriptionKey;
            return this;
        }

        public Builder resource(String resource) {
            this.resource = resource;
            return this;
        }

        public Builder order(Long order) {
            this.order = order;
            return this;
        }

        public Builder condition(Predicate<StandardUser> condition) {
            this.condition = condition;
            return this;
        }

        public Builder configurationResource(String configurationResource) {
            this.configurationResource = configurationResource;
            return this;
        }

        public Builder adminUrl(String adminUrl) {
            this.adminUrl = adminUrl;
            return this;
        }

        public Builder adminResource(String adminResource) {
            this.adminResource = adminResource;
            return this;
        }

        public MfaMethod build() {
            Objects.requireNonNull(imageUrl);
            Objects.requireNonNull(name);
            Objects.requireNonNull(condition);
            return new MfaMethod(imageUrl, name, nameKey, descriptionKey, resource, order, condition, configurationResource, adminUrl, adminResource);
        }
    }
}
