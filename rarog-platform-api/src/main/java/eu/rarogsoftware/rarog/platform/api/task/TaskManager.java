package eu.rarogsoftware.rarog.platform.api.task;

import java.util.concurrent.*;

/**
 * Manages background task ran using {@link java.util.concurrent.ExecutorService}.
 *
 * <p>
 * It can be treated as adapter on {@link java.util.concurrent.ExecutorService}, that adds support for:
 * <ul>
 *     <li>task monitoring,</li>
 *     <li>system wide thread pool,</li>
 *     <li>manageable pool size,</li>
 *     <li>throttling,</li>
 *     <li>easy scheduling and conditioning.</li>
 * </ul>
 * </p>
 * <p>
 *     Overall it is always preferable to use {@link TaskManager} instead of {@link java.util.concurrent.ExecutorService}
 *     or {@link Thread}. It is designed for short tasks, tasks that share the same memory pool (JVM) as main job,
 *     tasks that blocks request execution and all other kinds of light tasks, that don't need to be plugin state aware.
 * </p>
 * <p>
 *     If you need support for long running tasks, tasks that must work even after restart, tasks that may wait for execution,
 *     or even can be started on different machine, you should consider using {@link BackgroundJobManager}.
 *     It provides support for heavy jobs and is plugin aware.
 * </p>
 * <p>
 *     Usage:
 *     <pre>
 *         Some source code here
 *     </pre>
 * </p>
 *
 * @see BackgroundJobManager
 * @see ScheduledFuture
 * @see FutureTask
 */
public interface TaskManager {
    /**
     * Start new monitored thread outside of thread pool.
     * It's equivalent of {@link Thread#Thread(Runnable)}
     *
     * @param task task to run
     * @return not started thread with monitoring
     */
    Thread createThread(Runnable task);

    /**
     * Runs provided task as soon as there is free thread in the pool.
     *
     * @param task task to run
     * @return the same {@link Future} that would be returned by {@link java.util.concurrent.ExecutorService}
     */
    Future<?> runTask(Runnable task);

    /**
     * Runs provided task as soon as there is free thread in the pool.
     *
     * @param task task to run
     * @return the same {@link Future} that would be returned by {@link java.util.concurrent.ExecutorService}
     */
    <T> Future<T> runTask(Callable<T> task);

    /**
     * Runs provided task using provided config
     *
     * @param task           task to run
     * @param scheduleConfig config for scheduling task
     * @return controller for scheduled task
     */
    ScheduledFuture<?> scheduleTask(Runnable task, ScheduleConfig<Void> scheduleConfig);

    /**
     * Runs provided task using provided config
     *
     * @param task           task to run
     * @param scheduleConfig config for scheduling task
     * @return controller for scheduled task
     */
    <T> ScheduledFuture<T> scheduleTask(Callable<T> task, ScheduleConfig<T> scheduleConfig);

    /**
     * Returns {@link ThreadFactory} that provide the same functionality as {@link TaskManager}
     *
     * @return {@link ThreadFactory}
     */
    ThreadFactory asThreadFactory();

    /**
     * Returns {@link ScheduledExecutorService} that provide the same functionality as {@link TaskManager}
     *
     * @return {@link ScheduledExecutorService}
     */
    AdvancedScheduledExecutorService asExecutorService();
}
