package eu.rarogsoftware.rarog.platform.api.user.management;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 * Represents an authority granted to an {@link Authentication} object.
 * Extends {@link GrantedAuthority} to be able to work with {@link UserRole} API.
 *
 * @param role role of authority
 */
public record RoleGrantedAuthority(
        UserRole role) implements GrantedAuthority {

    @Override
    public String getAuthority() {
        return "ROLE_" + role.getRole();
    }
}
