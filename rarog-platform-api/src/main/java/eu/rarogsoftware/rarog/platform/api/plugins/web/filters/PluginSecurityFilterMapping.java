package eu.rarogsoftware.rarog.platform.api.plugins.web.filters;

import jakarta.servlet.Filter;

/**
 * Simple mapping, which allows to associate put filters in Spring security chain.
 * The filters will be chained according to calculated order based on placement.
 * Filter with the same placement may be invoked in random order.
 *
 * @param filter               The {@link Filter} instance which will be registered in the chain
 * @param placement            The {@link Placement} of filter relative to
 * @param relativeToFilterName full class name of filter that is used as reference for placement
 * @param relativeToFilter     class of filter that is used as reference for placement
 */
public record PluginSecurityFilterMapping(Filter filter, Placement placement, String relativeToFilterName,
                                          Class<? extends Filter> relativeToFilter) {
    public enum Placement {
        /**
         * Assigns the same order as relative filter
         */
        AT,
        /**
         * Assigns order lesser than relative filter (provided filter will execute earlier)
         */
        BEFORE,
        /**
         * Assigns order higher than relative filter (provided filter will execute later)
         */
        AFTER,
        /**
         * Assigns the lowest order (plugin will always execute after all filters with other placements and all spring
         * defined filters).
         */
        AT_END,
        /**
         * Assigns the highest order (plugin will always execute before all filters with other placements and all spring
         * defined filters).
         */
        AT_BEGINNING
    }

    public PluginSecurityFilterMapping {
        assert placement.ordinal() >= Placement.AT_END.ordinal()
                || (relativeToFilter != null || relativeToFilterName != null);
        assert filter != null;
    }

    public PluginSecurityFilterMapping(Filter filter, Placement placement, String relativeToFilter) {
        this(filter, placement, relativeToFilter, null);
    }

    public PluginSecurityFilterMapping(Filter filter, Placement placement, Class<? extends Filter> relativeToFilter) {
        this(filter, placement, null, relativeToFilter);
    }
}
