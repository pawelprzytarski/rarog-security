package eu.rarogsoftware.rarog.platform.api.user.management;

import org.springframework.security.core.GrantedAuthority;

import java.util.HashMap;
import java.util.Map;

/**
 * Base user roles in Rarog system. It is used for first high level permission checks.
 */
public enum UserRole {
    /**
     * Represents role of Administrator user that can change system settings but cannot access system resources.
     */
    ADMINISTRATOR("ADMIN", 1),
    /**
     * Represents role of Administrator user with full power over system.
     */
    SYSTEM_ADMINISTRATOR("SYSADMIN", 3),
    /**
     * Represents basic user without any administrative powers.
     */
    USER("USER", 0);

    private static final Map<Integer, UserRole> codeToRoleCache = new HashMap<>();
    private final String role;
    private final int databaseCode;

    UserRole(String role, int databaseCode) {
        this.role = role;
        this.databaseCode = databaseCode;
    }

    public static UserRole fromCode(int code) {
        if (codeToRoleCache.isEmpty()) {
            for (UserRole value : UserRole.values()) {
                codeToRoleCache.put(value.databaseCode, value);
            }
        }
        return codeToRoleCache.get(code);
    }

    public int getDatabaseCode() {
        return databaseCode;
    }

    public String getRole() {
        return role;
    }

    public GrantedAuthority getAuthority() {
        return new RoleGrantedAuthority(this);
    }
}
