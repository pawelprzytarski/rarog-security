package eu.rarogsoftware.rarog.platform.api.task;

/**
 * Info about last execution and current execution.
 *
 * @param previousValue     value of previous execution or null if first execution or execution ended with exception
 * @param runNumber         number of current run
 * @param lastExecutionTime time in milliseconds of last execution or null if first execution
 * @param thisExecutionTime time in millisecond when this execution should start
 */
public record ExecutionInfo<T>(T previousValue,
                               long runNumber,
                               long lastExecutionTime,
                               long thisExecutionTime) {
}
