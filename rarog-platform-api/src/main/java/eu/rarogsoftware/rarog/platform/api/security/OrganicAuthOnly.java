package eu.rarogsoftware.rarog.platform.api.security;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.*;

/**
 * Enforces access restriction to endpoint(s) to users that logged-in using "organic means" aka login to system by user like
 * using login form, session stored authentication, SSO, etc.
 * It will exclude all kinds of logins from secondary sources like using access tokens, JWT
 * OAuth2 (when Rarog is identity provider), etc. which are designed to be used by automations and external systems.
 * <p>
 * It enforce role requirements with {@link PreAuthorize} using Spring login.
 * </p>
 * <p>
 * Code annotated with {@link OrganicAuthOnly}
 * <pre>
 *          &#064;GetMapping("test")
 *          &#064;OrganicAuthOnly
 *          ResponseEntity testMethod()
 *     </pre>
 * is equivalent of code annotated with both {@link PreAuthorize}
 * <pre>
 *          &#064;GetMapping("test")
 *          &#064;PreAuthorize("hasRole('AUTH_ORGANIC')")
 *          ResponseEntity testMethod()
 *     </pre>
 * </p>
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@PreAuthorize("hasAuthority('AUTH_ORGANIC')")
public @interface OrganicAuthOnly {
}
