package eu.rarogsoftware.rarog.platform.api.metrics;

import java.io.Closeable;
import java.util.concurrent.Callable;

/**
 * Histogram metric, to track distributions of events.
 *
 * <p>
 * <em>Note:</em> Each bucket is one timeseries. Many buckets and/or many dimensions with labels
 * can produce large amount of time series, that may cause performance problems.
 * </p>
 */
public interface Histogram extends BasicMetric<Histogram> {
    /**
     * Executes runnable code and observes a duration of how long it took to run.
     *
     * @param timeable Code that is being timed
     * @return Measured duration in seconds for timeable to complete.
     */
    double measureExecutionTime(Runnable timeable);

    /**
     * Executes runnable code and observes a duration of how long it took to run.
     *
     * @param timeable Code that is being timed
     * @return result of timeable function
     * @throws Exception exception thrown by timeable function
     */
    <T> T measureExecutionTime(Callable<T> timeable) throws Exception;

    /**
     * Observes specified value and increments correct bucket.
     *
     * @param value value to be added to histogram
     */
    void observe(double value);

    /**
     * Start a timer to observe a duration.
     *
     * <p>
     * Call {@link Gauge.Timer#stop} at the end of what you want to measure duration of.
     * </p>
     * <p>
     * {@link Gauge.Timer} is {@link Closeable} to you can use it in try with resources to auto call {@link Gauge.Timer#stop()}
     * </p>
     *
     * @return timer object
     */
    Timer startTimer();

    /**
     * Represents an event being timed.
     *
     * <p>
     * Observe duration on {@link AutoCloseable#close()}
     * </p>
     */
    interface Timer extends AutoCloseable {
        /**
         * Observe the amount of time in seconds since {@link Histogram#startTimer} was called.
         *
         * @return Measured duration in seconds since {@link Histogram#startTimer} was called.
         */
        double observeDuration();
    }
}
