package eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui;

import java.util.Objects;

/**
 * Represents single UI element that should be dynamically rendered.
 * <p>
 * NOTE: elements are showed sorted by order property, then by key.
 *
 * @param key       unique key used to identify element under type
 * @param placement string value identifying unique place in application where fragment should be rendered.
 *                  Placement value is defined by component using UI Fragments. For example "adminPanelQuickAccess"
 *                  is top left menu with navigation elements.
 * @param resource  relative link to resource to be displayed. Usually it should be link to JS with react component,
 *                  but it may be different for some placements. The best is to consult with documentation for specific
 *                  placement to make sure.
 * @param order     numerical key used to sort elements under the same placement
 */
public record UiFragment(String key, String placement, String resource, int order) {
    public UiFragment {
        Objects.requireNonNull(key);
        Objects.requireNonNull(placement);
        Objects.requireNonNull(resource);
    }
}

