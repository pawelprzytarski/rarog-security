package eu.rarogsoftware.rarog.platform.api.metrics;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

/**
 * Base for all metric settings containing common settings like name, description, etc.
 * It is convenience class. It cannot be instantiated directly.
 *
 * @param <T> child type
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = MetricSettings.class, name = "metricSettings"),
        @JsonSubTypes.Type(value = HistogramSettings.class, name = "histogramMetricSettings")
})
public abstract class BaseMetricSettings<T extends BaseMetricSettings> {
    private String name;
    private String description;
    private String[] labels = new String[0];
    private String unit = "";

    /**
     * Set name of metric.
     * It doesn't have any restriction, but some implementations may not accept all characters. Best to keep name
     * passing [A-Za-z0-9_]* regex. Some characters like period `.` may be treated as special characters depending
     * on implementation or metrics server.
     *
     * @param name name of metric
     * @return settings object
     */
    @JsonSetter("name")
    public T name(String name) {
        this.name = name;
        if (description == null) {
            description = name;
        }
        return (T) this;
    }

    /**
     * Sets description. It not set will default to name.
     *
     * @param description description of metric
     * @return settings object
     */
    @JsonSetter("description")
    public T description(String description) {
        this.description = description;
        return (T) this;
    }

    /**
     * Ordered list of labels names, that will be required to provide to use metric.
     * Labels are usually used to add dimensions to metric. It is especially useful when metric measures some functionality
     * that may have different behaviours depending on input data or when you need to measure different aspect of functionality.
     *
     * @param labels ordered list of labels
     * @return settings object
     */
    @JsonSetter("labels")
    public T labels(String... labels) {
        this.labels = labels;
        return (T) this;
    }

    /**
     * See {@link BaseMetricSettings#labels(String...)}.
     *
     * @param labels ordered list of labels
     * @return settings object
     */
    public T labels(Collection<String> labels) {
        this.labels = labels.toArray(String[]::new);
        return (T) this;
    }

    /**
     * Unit of metric. Usually is used as suffix of reported metric name, but it may change depending on implementation.
     *
     * @param unit unit of metric (e.g.: meters, bytes)
     * @return settings object
     */
    @JsonSetter("unit")
    public T unit(String unit) {
        this.unit = unit;
        return (T) this;
    }

    /**
     * @return name of metric
     */
    @JsonGetter("name")
    public String name() {
        return name;
    }

    /**
     * @return description of metric
     */
    @JsonGetter("description")
    public String description() {
        return description;
    }

    /**
     * @return labels (dimentions) of metric
     */
    @JsonGetter("labels")
    public String[] labels() {
        return labels;
    }

    /**
     * @return unit of metric
     */
    @JsonGetter("unit")
    public String unit() {
        return unit;
    }

    @Override
    public String toString() {
        return "MetricSettings{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", labels=" + Arrays.toString(labels) +
                ", unit='" + unit + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseMetricSettings<?> that)) return false;
        return Objects.equals(name, that.name) && Objects.equals(description, that.description) && Arrays.equals(labels, that.labels) && Objects.equals(unit, that.unit);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, description, unit);
        result = 31 * result + Arrays.hashCode(labels);
        return result;
    }
}
