package eu.rarogsoftware.rarog.platform.api.resources.management.frontend;

import java.io.InputStream;

public class ClassLoaderDynamicAssetsLoader extends AbstractResourceBasedDynamicAssetsLoader {

    public ClassLoaderDynamicAssetsLoader(String manifestLocation, Long cacheDuration) {
        super(manifestLocation, cacheDuration);
    }

    @Override
    protected InputStream getManifestResourceStream() {
        return this.getClass().getClassLoader().getResourceAsStream(manifestLocation);
    }

}
