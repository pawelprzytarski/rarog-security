package eu.rarogsoftware.rarog.platform.api.plugins.web.servlets;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;

import java.util.Collections;
import java.util.List;

public record PluginServletMappingDescriptor(List<ServletMapping> mappings) implements FeatureDescriptor {

    public PluginServletMappingDescriptor(ServletMapping mapping) {
        this(Collections.singletonList(mapping));
    }
}