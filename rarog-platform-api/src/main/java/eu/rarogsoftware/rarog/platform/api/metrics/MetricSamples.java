package eu.rarogsoftware.rarog.platform.api.metrics;

import java.util.Collection;

/**
 * Object representing samples from specific metric.
 *
 * @param settings settings of metric
 * @param type     type of metric. It is used by some exposer and exporters
 * @param samples  samples extracted from metric
 */
public record MetricSamples(MetricSettings settings, Type type, Collection<Sample> samples) {
    /**
     * Type of metric.
     *
     * <p>
     * It is mapped from Prometheus metric types, so we can support Prometheus format.
     * </p>
     */
    public enum Type {
        // based on prometheus types
        UNKNOWN,
        COUNTER,
        GAUGE,
        STATE_SET,
        INFO,
        HISTOGRAM,
        GAUGE_HISTOGRAM,
        SUMMARY,
    }
}
