package eu.rarogsoftware.rarog.platform.api.resources.management.frontend;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;

/**
 * Tells Rarog app where look for dynamic assets descriptor in plugin if plugin is able to hold resources.
 * @param descriptorsPlaceholder path with `{namespace}` placeholder that is later replaced by valid namespace name
 */
public record DynamicAssetsDescriptor(String descriptorsPlaceholder) implements FeatureDescriptor {
    public DynamicAssetsDescriptor {
    }

    public DynamicAssetsDescriptor() {
        this("descriptors/assets-{namespace}.manifest.json");
    }
}
