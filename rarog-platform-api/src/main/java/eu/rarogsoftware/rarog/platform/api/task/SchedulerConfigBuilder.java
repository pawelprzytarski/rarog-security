package eu.rarogsoftware.rarog.platform.api.task;

import java.time.Duration;
import java.util.function.Predicate;

/**
 * Convenient builder for {@link ScheduleConfig}
 */
public class SchedulerConfigBuilder<T> {
    private Duration initialDelay = Duration.ZERO;
    private Duration period = Duration.ZERO;
    private Duration repeatAfter = Duration.ZERO;
    private int repeatCount = -1;
    private Predicate<ExecutionInfo<T>> executionCondition = null;
    private Predicate<ExecutionInfo<T>> cancelCondition = null;

    /**
     * Initial delay for running scheduled task. If zero then start as soon as possible.
     * Default: {@link Duration#ZERO}
     *
     * @param initialDelay initial delay duration
     * @return this builder
     */
    public SchedulerConfigBuilder<T> initialDelay(Duration initialDelay) {
        this.initialDelay = initialDelay;
        return this;
    }

    /**
     * Set how often task should repeat. If zero together with {@link SchedulerConfigBuilder#repeatAfter} then do not repeat task.
     * Default: {@link Duration#ZERO}. It's mutually exclusive with {@link SchedulerConfigBuilder#repeatAfter}. The last
     * set is final.
     *
     * @param period task period
     * @return this builder
     */
    public SchedulerConfigBuilder<T> period(Duration period) {
        this.period = period;
        this.repeatAfter = Duration.ZERO;
        return this;
    }


    /**
     * Set delay for next execution. If zero together with {@link SchedulerConfigBuilder#period} then do not repeat task.
     * Default: {@link Duration#ZERO}. It's mutually exclusive with {@link SchedulerConfigBuilder#period}. The last
     * set is final.
     *
     * @param repeatAfter task delay for next repeat
     * @return this builder
     */
    public SchedulerConfigBuilder<T> repeatAfter(Duration repeatAfter) {
        this.period = Duration.ZERO;
        this.repeatAfter = repeatAfter;
        return this;
    }

    /**
     * Set count of repeats for task. Task will execute specified amount if not cancelled earlier.
     *
     * @param repeatCount number of task repeats
     * @return this builder
     */
    public SchedulerConfigBuilder<T> repeatCount(int repeatCount) {
        this.repeatCount = repeatCount;
        return this;
    }

    /**
     * Task will repeat until cancelled.
     *
     * @return this builder
     */
    public SchedulerConfigBuilder<T> unlimitedRepeats() {
        this.repeatCount = -1;
        return this;
    }

    /**
     * Condition for executing task. If condition returns false then task is not executed and repeat count is not increased.
     * If not set, task will execute each time when it is scheduled.
     *
     * @param executionCondition execution condition predicate
     * @return this builder
     */
    public SchedulerConfigBuilder<T> executionCondition(Predicate<ExecutionInfo<T>> executionCondition) {
        this.executionCondition = executionCondition;
        return this;
    }

    /**
     * Condition for cancelling task after execution. If not set then task will not automatically cancel itself.
     *
     * @param cancelCondition cancel condition predicate
     * @return this builder
     */
    public SchedulerConfigBuilder<T> cancelCondition(Predicate<ExecutionInfo<T>> cancelCondition) {
        this.cancelCondition = cancelCondition;
        return this;
    }

    /**
     * Build new instance of config
     *
     * @return fully build {@link ScheduleConfig}
     */
    public ScheduleConfig<T> build() {
        return new ScheduleConfig<>(initialDelay, period, repeatAfter, repeatCount, executionCondition, cancelCondition);
    }
}