package eu.rarogsoftware.rarog.platform.api.plugins;

/**
 * Represents {@link FeatureModule} that support cleaning stale resources.
 * When admin decide it no longer need plugin, it can decide to remove all data related to plugin.
 * In such case all module that store data must to remove them from the system. This interface
 * represents modules that need cleaning in such case.
 * <p>
 * NOTE: purge is invoked on deactived plugin, so none of it's functionalities is working.
 * </p>
 *
 * @param <T> type of supported {@link FeatureDescriptor}
 */
public interface PurgeableFeatureModule<T extends FeatureDescriptor> extends FeatureModule<T> {
    /**
     * Purge data related to descriptor by module.
     * Purge works on not active plugin.
     *
     * @param plugin     plugin that is parent of descriptor
     * @param descriptor feature descriptor to purge
     */
    void purgeDescriptor(Plugin plugin, T descriptor);
}
