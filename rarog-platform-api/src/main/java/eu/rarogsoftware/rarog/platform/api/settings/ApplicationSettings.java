package eu.rarogsoftware.rarog.platform.api.settings;

import java.util.Optional;

/**
 * Allows retrieving or persisting application settings.
 * Use only for application settings that are not user specific
 */
public interface ApplicationSettings {
    /**
     * Get setting value from default setting store. Returns empty if no value found
     *
     * @param key  setting key
     * @param type returned type
     * @param <T>  returned setting type
     * @return setting for key
     */
    <T> Optional<T> getSetting(String key, Class<T> type);

    /**
     * Get setting value from default setting store. Returns empty if no value found
     *
     * @param key setting key
     * @return setting for key
     */
    Optional<String> getSettingAsString(String key);

    /**
     * Get setting value from default setting store. Returns default value or null if no default value
     *
     * @param key  setting key
     * @param type returned type
     * @param <T>  returned setting type
     * @return setting for key
     */
    <T> T getSettingOrDefault(String key, Class<T> type);

    /**
     * Assumes that setting is boolean type and returns primitive boolean.
     *
     * @param key  setting key
     * @return true if setting is set to true or if default value is set to true, false otherwise
     */
    boolean isTrue(String key);

    /**
     * Get setting value from default setting store. Returns default value or null if no default value
     *
     * @param key setting key
     * @return setting for key
     */
    String getSettingOrDefaultAsString(String key);

    /**
     * Get setting value from system properties or default setting store or default value.
     *
     * @param key  setting key
     * @param type returned type
     * @param <T>  returned setting type
     * @return setting for key
     */
    <T> T getPropertyBackedSetting(String key, Class<T> type);

    /**
     * Get setting value from system properties or default setting store or default value.
     *
     * @param key setting key
     * @return setting for key
     */
    String getPropertyBackedSettingAsString(String key);

    /**
     * Persist setting value. If no serializer specified then tries store setting value as json
     *
     * @param key   setting key
     * @param value value to persist
     * @param <T>   returned setting type
     */
    <T> void setSetting(String key, T value);

    /**
     * Configures default value for setting.
     *
     * @param key          setting key
     * @param defaultValue value to persist
     * @param <T>          returned setting type
     */
    <T> void setDefaultSetting(String key, T defaultValue);

    /**
     * Adds serializer for custom type. Replaces default json serializer for provided type
     *
     * @param type       type of serializer
     * @param serializer instance of serializer for provided type
     * @param <T>        serializable type
     */
    <T> void addTypeSerializer(Class<T> type, SettingSerializer<T> serializer);
}
