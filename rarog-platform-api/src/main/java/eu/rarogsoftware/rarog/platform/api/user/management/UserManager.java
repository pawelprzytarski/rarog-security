package eu.rarogsoftware.rarog.platform.api.user.management;

/**
 * Allows to manipulate user accounts in system without checking permission access.
 * Usage of this manager is preferable to {@link org.springframework.security.provisioning.UserDetailsManager}
 * because it works on {@link StandardUser} instead of {@link org.springframework.security.core.userdetails.UserDetails}.
 */
public interface UserManager {

    /**
     * Create a new user with the supplied details.
     *
     * @param user full details of user to create
     * @return id of created user
     */
    Long createUser(StandardUser user) throws UserManagementException;

    /**
     * Update the specified user. It gets user by username.
     * Note: password is not updated in this method even if provided. Use {@link UserManager#updatePassword(String, String)}
     *
     * @param user full details of user to update
     */
    void updateUser(StandardUser user) throws UserManagementException;

    /**
     * Change password of user with provided username.
     *
     * @param username    username of user for password change
     * @param newPassword not encrypted password
     */
    void updatePassword(String username, String newPassword) throws UserManagementException;

    /**
     * Change password of user with provided id.
     *
     * @param id          id of user for password change
     * @param newPassword not encrypted password
     */
    void updatePassword(long id, String newPassword) throws UserManagementException;

    /**
     * Get user with provided username
     *
     * @param username username of user
     * @return application user with provided username of null
     */
    StandardUser getUser(String username) throws UserManagementException;

    /**
     * Get user for provided id
     *
     * @param id id of user
     * @return application user for provided id
     */
    StandardUser getUser(Long id) throws UserManagementException;

    /**
     * Remove the user with the given login name from the system.
     *
     * @param username username of user to remove
     */
    void deleteUser(String username) throws UserManagementException;

    /**
     * Check if a user with the supplied login name exists in the system.
     *
     * @param username username to check
     * @return true if user exists, false otherwise
     */
    boolean userExists(String username);

    /**
     * Check if a user with the supplied id exists in the system.
     *
     * @param id id to check
     * @return true if user exists, false otherwise
     */
    boolean userExists(Long id);
}
