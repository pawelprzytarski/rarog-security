package eu.rarogsoftware.rarog.platform.api.user.management;

import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Builder for {@link StandardUser}.
 * Use this builder instead of creating instance of {@link StandardUser} directly.
 */
public class StandardUserBuilder {
    private Long id;
    private String username;
    private String password;
    private Set<GrantedAuthority> authorities = new HashSet<>();
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;
    private boolean mfaEnabled = true;

    public StandardUserBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public StandardUserBuilder username(String username) {
        this.username = username;
        return this;
    }

    public StandardUserBuilder password(String password) {
        this.password = password;
        return this;
    }

    public StandardUserBuilder authorities(Set<GrantedAuthority> authorities) {
        this.authorities = new HashSet<>(authorities);
        return this;
    }

    public StandardUserBuilder addAuthorities(GrantedAuthority... authorities) {
        this.authorities.addAll(Arrays.asList(authorities));
        return this;
    }

    public StandardUserBuilder addAuthorities(Set<GrantedAuthority> authorities) {
        this.authorities.addAll(authorities);
        return this;
    }

    public StandardUserBuilder addAuthority(GrantedAuthority grantedAuthority) {
        this.authorities.add(grantedAuthority);
        return this;
    }

    public StandardUserBuilder roles(UserRole... roles) {
        Set<GrantedAuthority> authorities = migrateRolesToAuthorities(Arrays.asList(roles));
        return authorities(authorities);
    }

    public StandardUserBuilder roles(Set<UserRole> roles) {
        Set<GrantedAuthority> authorities = migrateRolesToAuthorities(roles);
        return authorities(authorities);
    }

    private Set<GrantedAuthority> migrateRolesToAuthorities(Collection<UserRole> roles) {
        Set<GrantedAuthority> authorities = new HashSet<>(roles.size());
        for (UserRole role : roles) {
            authorities.add(role.getAuthority());
        }
        return authorities;
    }

    public StandardUserBuilder addRole(UserRole role) {
        this.authorities.add(role.getAuthority());
        return this;
    }

    public StandardUserBuilder addRoles(UserRole... roles) {
        Set<GrantedAuthority> authorities = migrateRolesToAuthorities(Arrays.asList(roles));
        return addAuthorities(authorities);
    }

    public StandardUserBuilder accountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
        return this;
    }

    public StandardUserBuilder accountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
        return this;
    }

    public StandardUserBuilder credentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
        return this;
    }

    public StandardUserBuilder enabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public StandardUserBuilder mfaEnabled(Boolean mfaEnabled) {
        this.mfaEnabled = mfaEnabled;
        return this;
    }

    public StandardUser build() {
        if (authorities.contains(UserRole.SYSTEM_ADMINISTRATOR.getAuthority())) {
            authorities.add(UserRole.ADMINISTRATOR.getAuthority());
        }
        return new StandardUser(id, username, password, authorities, accountNonExpired, accountNonLocked, credentialsNonExpired, enabled, mfaEnabled);
    }
}