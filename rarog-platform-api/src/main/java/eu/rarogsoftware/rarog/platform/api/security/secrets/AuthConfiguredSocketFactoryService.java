package eu.rarogsoftware.rarog.platform.api.security.secrets;

import javax.net.SocketFactory;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Map;


/**
 * This service enables support for stored credentials when using {@link SocketFactory}.
 * <p>
 * Service resolves provided authentication data to valid {@link SocketFactory} which
 * performs authorization or encryption as specified in authentication string.
 * <p>
 * Authentication string must follow syntax: {@code <method>:<method_data>}.
 * Where {@code <method>} is one of supported authentication methods
 * and {@code <method_data>} are data necessary for this method. Parameters follow syntax
 * {@code <parameter1>=<value1>,<parameter2>=<value2>}, unless specified otherwise. Special characters `,=`
 * in parameters can be escaped with backslash character `\`. Double backslash `\\` will be
 * translated to single backslash.
 * <p>
 * Alternative format for authentication data is parameters map. It supports any kind of data. It is used primary
 * for credentials data, that cannot be stored as authentication string and for internal purposes.
 * Each parameters map have required entry {@code method} which is the equivalent of method from authentication string.
 * All other data in map translates to method parameters.
 * <p>
 * Currently supported auth methods are:
 * <ul>
 *     <li>{@code client_cert} - which gives support for TLS mutual authentication.</li>
 *     <li>{@code build_in} - which resolves to one of other methods for which configuration is embedded into system.</li>
 *     <li>{@code vault} - which resolves to one of other methods for which configuration is stored in secure vault (if supported).</li>
 * </ul>
 * <p>
 * Authentication method {@code client_cert} supports following parameters:
 * <ul>
 *     <li>{@code cert} - which should point to X.509 certificate file (required)</li>
 *     <li>{@code privkey} - which should point to PKCS8 private key file (required)</li>
 *     <li>{@code password_file} - which should point to file with password to certificate or private key (optional)</li>
 *     <li>{@code password} - which should be password to certificate or private key (optional)</li>
 *     <li>{@code cert_bytes} - certificate passed as byte array (alternative to {@code cert}). Supported only in parameters map.</li>
 *     <li>{@code privkey_bytes} - private key passed as byte array (alternative to {@code privkey}). Supported only in parameters map.</li>
 * </ul>
 * Example: `client_cert:cert_file=/ssl/app.crt,privatekey_file=/ssl/key.pem,password=MyPassword`
 * <p>
 * Authentication method {@code vault} do not support parameters syntax in auth string, but expects storage key
 * of credential to retrieve from vault. For parameters map use {@code value} parameter to pass storage key.
 */
public interface AuthConfiguredSocketFactoryService extends AuthConfigService<SocketFactory> {
    /**
     * Creates new version of {@link AuthConfiguredSocketFactoryService} that will trust certificates
     * passed in parameter on top of already trusted certificates. Useful to connecting to services
     * that use custom root certificates
     *
     * @param certificates collection of certificates to be trusted by created instances of {@link SocketFactory}
     * @return new instance of {@link AuthConfiguredSocketFactoryService} with extended list of trusted certificates
     */
    AuthConfiguredSocketFactoryService extendTrustedCertificates(Collection<X509Certificate> certificates);

    /**
     * Creates instance of helper {@link SocketFactory} based on provided auth string.
     * Auth string must have format {@code <method>:<method_data>}. See interface javadoc for details.
     *
     * @param authenticationData auth string containing settings
     * @return configured {@link SocketFactory} object
     */
    @Override
    SocketFactory resolveFromString(String authenticationData);

    /**
     * Creates instance of helper {@link SocketFactory} based on provided auth data.
     *
     * @param authenticationData map of auth data. See interface javadoc for details.
     * @return configured {@link SocketFactory} object
     */
    @Override
    SocketFactory resolveFromSettings(Map<String, Object> authenticationData);
}
