package eu.rarogsoftware.rarog.platform.api.metrics;

import java.util.List;

/**
 * Single metric sample. Represents state of metric in given time.
 *
 * @param name        name of sample/metric
 * @param labelsNames list of ordered labels (dimensions) names
 * @param labels      list of ordered labels values (the same order as labelsNames)
 * @param value       value of metric
 */
public record Sample(String name, List<String> labelsNames, List<String> labels, double value, long timestamp) {
    public Sample {
    }

    public Sample(String name, List<String> labelsNames, List<String> labels, double value) {
        this(name, labelsNames, labels, value, -1);
    }
}
