package eu.rarogsoftware.rarog.platform.api.metrics;

import java.util.HashMap;
import java.util.Map;

/**
 * Info metric, key-value pairs.
 *
 * <p>
 * Examples of Info include build information, version information, and potential target metadata.
 *
 * <p>
 * Example Info:
 * <pre>
 * {@code
 *     void func() {
 *          // Your code here.
 *         buildInfo.info("branch", "HEAD", "version", "1.2.3", "revision", "3d07f7");
 *     }
 * }
 * </pre>
 * </p>
 */
public interface Info extends BasicMetric<Info> {
    /**
     * Sets info.
     *
     * @param info map of infos
     */
    void info(Map<String, String> info);

    /**
     * Sets info.
     *
     * @param info list of key:value pairs
     */
    default void info(String... info) {
        if (info.length % 2 == 1) {
            throw new IllegalArgumentException("There must be even number of param strings");
        }
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < info.length; i += 2) {
            map.put(info[i], info[i + 1]);
        }
        info(map);
    }
}
