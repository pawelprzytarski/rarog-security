package eu.rarogsoftware.rarog.platform.api.plugins;

import java.io.IOException;
import java.util.Collection;

/**
 * Interface used to manage plugins and their artifacts.
 */
public interface PluginManager {

    /**
     * Saves an artifact in 'home'/plugins directory and returns an object representing the saved artifact.
     *
     * @param artifact artifact to be saved in home directory.
     * @return artifact pointing at the resource in home directory.
     * @throws PluginException if the artifact is not a valid plugin.
     * @throws IOException if something goes wrong when saving the artifact in the filesystem.
     */
    PluginArtifact savePluginArtifactToHome(PluginArtifact artifact) throws PluginException, IOException;

    /**
     * Deletes plugin artifact if present in the home directory.
     *
     * @param plugin plugin which artifact should be deleted.
     */
    void deletePluginArtifactFromHome(Plugin plugin);

    /**
     * Validates correctness of the passed artifact.
     *
     * @param pluginArtifact artifact to be validated.
     * @throws PluginException if passed artifact is not a valid plugin.
     * @throws IOException if there are problems accessing the artifact's resource.
     */
    void validatePluginArtifact(PluginArtifact pluginArtifact) throws PluginException, IOException;

    /**
     * Installs plugin in the system and if required stores the state if the installation went correctly.
     * Doesn't save the plugin artifact in home directory.
     *
     * @param artifact artifact to install the plugin from.
     * @param storeState should the state be stored after installation.
     * @return {@link Plugin} referencing installed plugin.
     * @throws IOException if there are problems accessing the artifact's resource.
     * @throws PluginException if the artifact is invalid or the installation fails.
     */
    Plugin installPlugin(PluginArtifact artifact, boolean storeState) throws IOException, PluginException;

    /**
     * Checks the recently saved state and either disables or enables the plugin if installed.
     * Useful e.g. on instance startup.
     *
     * @param plugin plugin to initialize.
     * @return plugin that was enabled or disabled.
     * @throws PluginException if enabling or disabling the plugin fails.
     */
    Plugin initializePlugin(Plugin plugin) throws PluginException;

    /**
     * Removes the plugin from the system and removes the artifact from home if there was any.
     *
     * @param plugin plugin to be uninstalled.
     * @throws PluginException if the plugin is in use or uninstallation wasn't successful.
     */
    void uninstall(Plugin plugin) throws PluginException;

    /**
     * Enables the previously installed plugin as well as its dependencies storing this state persistently.
     *
     * @param plugin plugin to be enabled.
     * @return the enabled plugin.
     * @throws PluginException if plugin is not installed or there are problems enabling the plugin or its dependencies.
     */
    Plugin enablePlugin(Plugin plugin) throws PluginException;

    /**
     * Disables and enables the plugin.
     *
     * @param plugin plugin to be restarted.
     * @return restarted plugin.
     * @throws PluginException if there are problems during restart.
     */
    Plugin restartPlugin(Plugin plugin) throws PluginException;

    /**
     * Disables the plugin and stores this state persistently - plugin won't be enabled after instance restart.
     *
     * @param plugin plugin to be disabled.
     * @return disabled plugin.
     * @throws PluginException if the plugin is not installed or there are problems disabling the plugin.
     */
    Plugin disablePlugin(Plugin plugin) throws PluginException;

    /**
     * Disables the plugin and if required - stores this state persistently.
     *
     * @param plugin plugin to be disabled.
     * @param storeState should the store be saved after disabling the plugin.
     * @return disabled plugin.
     * @throws PluginException if the plugin is not installed or there are problems disabling the plugin.
     */
    Plugin disablePlugin(Plugin plugin, boolean storeState) throws PluginException;

    /**
     * Returns a list consisting of all installed plugins.
     *
     * @return a list of installed plugins.
     */
    Collection<Plugin> listPlugins();

    /**
     * Get the installed plugins with passed key.
     *
     * @param key key of the plugin to get.
     * @return installed plugin with given key.
     */
    Plugin getPlugin(String key);

    /**
     * Deletes all plugin data (db tables, resources, etc.) and disables the plugin.
     *
     * @param plugin plugin to be purged.
     * @throws PluginException if the plugin is not installed or there were problems purging the plugin.
     */
    void purgePlugin(Plugin plugin) throws PluginException;
}

