package eu.rarogsoftware.rarog.platform.api.resources.management.frontend;

public class DynamicManifestNotAvailable extends RuntimeException {
    public DynamicManifestNotAvailable(String message) {
        super(message);
    }

    public DynamicManifestNotAvailable(String message, Throwable cause) {
        super(message, cause);
    }
}
