package eu.rarogsoftware.rarog.platform.api.plugins.web.servlets;

import jakarta.servlet.ServletException;

/**
 * Thrown by servlet that conditionally overrides other servlets.
 * If thrown by servlet, next matching servlet will be invoked.
 * See {@link PluginServletMappingDescriptor} for details.
 *
 * @see PluginServletMappingDescriptor
 */
public class NotHandledException extends ServletException {
    public NotHandledException() {
    }

    public NotHandledException(String message) {
        super(message);
    }

    public NotHandledException(String message, Throwable rootCause) {
        super(message, rootCause);
    }

    public NotHandledException(Throwable rootCause) {
        super(rootCause);
    }
}
