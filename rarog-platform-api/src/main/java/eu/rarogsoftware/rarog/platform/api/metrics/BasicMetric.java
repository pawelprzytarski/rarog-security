package eu.rarogsoftware.rarog.platform.api.metrics;

import java.util.Collection;

/**
 * Basic interface for metric that can have labels (dimensions).
 *
 * @param <T> Child type of metric
 */
public interface BasicMetric<T extends BasicMetric<T>> {
    /**
     * Returns child with the given labels.
     * <p>
     * Must be passed the same number of labels are were passed to {@link BaseMetricSettings#labels(String...)}.
     * </p>
     *
     * @param labels labels (dimensions) of metric to log
     */
    default T labels(Collection<String> labels) {
        return labels(labels.toArray(String[]::new));
    }

    /**
     * Returns child with the given labels.
     * <p>
     * Must be passed the same number of labels are were passed to {@link BaseMetricSettings#labels(String...)}.
     * </p>
     *
     * @param labels labels (dimensions) of metric to log
     */
    T labels(String... labels);
}
