package eu.rarogsoftware.rarog.platform.api.metrics;

import java.util.Collection;

/**
 * Service for creating and managing metrics in system.
 * <p>
 * Use this service to create metrics in system. All metric created with this service will be automatically
 * exposed and exported. Any metric created without using this service will not be properly exported.
 * </p>
 */
public interface MetricsService {
    /**
     * @param settings metric settings
     * @return new instance of {@link Counter} metric
     */
    Counter createCounter(MetricSettings settings);

    /**
     * @param settings metric settings
     * @return new instance of {@link Gauge} metric
     */
    Gauge createGauge(MetricSettings settings);

    /**
     * @param settings metric settings
     * @return new instance of {@link Histogram} metric
     */
    Histogram createHistogram(HistogramSettings settings);

    /**
     * @param settings metric settings
     * @return new instance of {@link Info} metric
     */
    Info createInfo(MetricSettings settings);

    /**
     * Close instance of metric.
     *
     * <p>
     * Closed metric are no longer exported as part of snapshot. They will still be part of exposed metrics,
     * but they are not stored in snapshot nor exported to external systems. Closed metric will be included in
     * the nearest snapshot.
     * </p>
     *
     * @param metric metric to close. Must be metric created by the same instance of {@link MetricsService}
     */
    void closeMetric(BasicMetric metric);

    /**
     * Gets snapshot state of metrics active in current moment.
     *
     * @return snapshot of metrics state in current moment
     */
    MetricsSnapshot snapshot();

    /**
     * Get last known state of all metrics in system.
     *
     * @return collection of last known samples of all metrics in system
     */
    Collection<MetricSamples> sample();
}
