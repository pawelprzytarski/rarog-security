package eu.rarogsoftware.rarog.platform.api.security.mfa;

import java.lang.annotation.*;

/**
 * Marks endpoint as available even if multi factory authentication is not passed yet.
 * Endpoints not marked with this annotation are not accessible when MFA is enabled.
 *
 * @see MfaService
 * @see MfaStore
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MfaEndpoint {
    /**
     * If true endpoint is intended to be accessible only when user is configuring MFA for first time.
     * If false endpoint is intended to be accessible when user have configured MFA but didn't authenticate yet.
     * registerOnly endpoints are not accessible when user has configured MFA.
     */
    boolean registerOnly() default false;
}
