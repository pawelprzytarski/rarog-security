package eu.rarogsoftware.rarog.platform.api.security;

import java.lang.annotation.*;

/**
 * Informs Spring security mechanism that this endpoint should not be csrf protected.
 * Usually useful with REST APIs, which are intended to be stateless only.
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CsrfDisabled {
}
