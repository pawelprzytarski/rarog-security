package eu.rarogsoftware.rarog.platform.api.plugins.web.servlets;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;

import java.util.Optional;

/**
 * Extension for {@link jakarta.servlet.http.HttpServlet} defined with {@link PluginServletMappingDescriptor}, that
 * allows to control authorization using {@link AuthorizationManager}. It supports the same design as {@link PluginServletMappingDescriptor},
 * with overriding and passing through. If servlet doesn't implement {@link AuthorizationManagerAware} or
 * {@link AuthorizationManagerAware#getAuthorizationManager(HttpServletRequest)} returns {@link Optional#empty()},
 * then control is passed to the next servlet. If none servlet matches/defines voter, then default settings
 * set by Spring based controllers are in power (which usually translated to access only for logged-in users).
 */
public interface AuthorizationManagerAware {
    /**
     * @param request request for which decision is taken
     * @return manager if servlet supports request and define custom access rights or empty optional if servlet do not support
     * request or want to use default access rights.
     */
    Optional<AuthorizationManager<RequestAuthorizationContext>> getAuthorizationManager(HttpServletRequest request);
}
