package eu.rarogsoftware.rarog.platform.api.plugins.events;

import java.lang.reflect.Method;

/**
 * Exception thrown by the {@link EventService} when the event is sent with a method propagating exceptions
 * and one of the listeners throws any {@link Throwable} when handling the event.
 */
public class EventDeliveryFailedException extends RuntimeException {
    public EventDeliveryFailedException(EventListener listener, Method method, Throwable e) {
        super("Error occurred when invoking event handler %s#%s".formatted(listener.getClass(), method.getName()), e);
    }
}
