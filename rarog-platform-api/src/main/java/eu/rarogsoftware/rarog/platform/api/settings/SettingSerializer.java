package eu.rarogsoftware.rarog.platform.api.settings;

public interface SettingSerializer<T> {
    T deserialize(String serialized);

    default String serialize(T serializable) {
        return serializable.toString();
    }
}
