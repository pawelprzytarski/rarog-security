package eu.rarogsoftware.rarog.platform.api.plugins.templates;

import eu.rarogsoftware.rarog.platform.api.plugins.Plugin;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolution;

import java.util.Map;

/**
 * Plugin request aware version of {@link ITemplateResolver}
 * Have additional method that accept {@link Plugin} in which context request is executed.
 */
public interface PluginTemplateResolver extends ITemplateResolver {
    default boolean isApplicableFor(Plugin plugin) {
        return true;
    }

    /**
     * <p>
     *   Tries to resolve a template. See {@link ITemplateResolver#resolveTemplate(IEngineConfiguration, String, String, Map)}
     *   for more info.
     * </p>
     *
     * @param configuration the engine configuration.
     * @param ownerTemplate the containing template from which we want to resolve a new one as a fragment. Can be null.
     * @param template the template to be resolved (usually its name).
     * @param templateResolutionAttributes the template resolution attributes to be used (usually coming from a
     *                                     {@link org.thymeleaf.TemplateSpec} instance. Can be null.
     * @param plugin plugin in which context request is executed
     * @return a TemplateResolution object (which might represent an existing resource or not), or null if the
     *         template could not be resolved.
     */
    default TemplateResolution resolveTemplate(
            final IEngineConfiguration configuration,
            final String ownerTemplate, final String template,
            final Map<String, Object> templateResolutionAttributes,
            final Plugin plugin) {
        if (isApplicableFor(plugin)) {
            return resolveTemplate(configuration, ownerTemplate, template, templateResolutionAttributes);
        }
        return null;
    }
}
