package eu.rarogsoftware.rarog.platform.api.plugins.web.page.ui;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;

import java.util.List;

/**
 * Gives possibility for plugins to define their own UI Fragments.
 *
 * @param fragments list of UI Fragments
 */
public record UiFragmentsDescriptor(List<UiFragment> fragments) implements FeatureDescriptor {
}
