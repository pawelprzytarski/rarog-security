package eu.rarogsoftware.rarog.platform.api.plugins.events;


import eu.rarogsoftware.rarog.platform.api.task.TaskManager;

/**
 * Rarog's event service - used to handle events published by plugins.
 * As opposed to the Spring event system - events published by one plugin can be received by others.
 * Exceptions thrown by event listeners are logged, but aren't breaking the execution.
 * Any object can be published as event, it will be passed to {@link EventHandler}s with
 * matching parameter types.
 */
public interface EventService {
    enum EventType {
        SYNC,
        ASYNC,
        PARALLEL_ASYNC,
        SYNC_WITH_EXCEPTIONS
    }

    /**
     * Publishes the event in a synchronous way, the method returns after all the handlers process the event.
     * Swallows the exceptions in listeners
     *
     * @param event Event to be published.
     */
    void publishSyncEvent(Object event);

    /**
     * Publishes the event in a synchronous way, the method returns after all the handlers process the event.
     * Rethrows the exceptions in listeners as {@link EventDeliveryFailedException}
     *
     * @param event Event to be published.
     */
    void publishSyncEventWithExceptions(Object event);

    /**
     * Publishes the event in an asynchronous manner.
     * The method returns just after single task of invoking all handlers sequentially
     * is submitted to {@link TaskManager}
     * Swallows the exceptions in listeners
     *
     * @param event Event to be published.
     */
    void publishAsyncEvent(Object event);

    /**
     * Publishes the event in an asynchronous, parallel manner.
     * The method submits single task per event handler to {@link TaskManager} and returns.
     * Events are processed in parallel if there are available threads in TaskManager's thread pool.
     * Swallows the exceptions in listeners
     *
     * @param event Event to be published.
     */
    void publishParallelAsyncEvent(Object event);

    /**
     * Publishes the event in the way defined by the eventType parameter.
     *
     * @param event     Event to be published.
     * @param eventType Defines how the event will be processed
     */
    default void publishEvent(Object event, EventType eventType) {
        switch (eventType) {
            case SYNC -> publishSyncEvent(event);
            case ASYNC -> publishAsyncEvent(event);
            case PARALLEL_ASYNC -> publishParallelAsyncEvent(event);
            case SYNC_WITH_EXCEPTIONS -> publishSyncEventWithExceptions(event);
        }
    }

    /**
     * Registers all {@link EventHandler}s from passed object.
     * If handlers with more than one parameter are found, this fact is logged and the method is skipped but processing is not stopped.
     * If the passed {@link EventListener} doesn't have any valid handlers, {@link IllegalArgumentException} is thrown.
     *
     * @param listener Event Listener class which defines {@link EventHandler} methods.
     */
    void registerListener(EventListener listener);

    /**
     * Unregisters all {@link EventHandler}s from passed object.
     *
     * @param listener Event Listener class which defines {@link EventHandler} methods.
     */
    void unregisterListener(EventListener listener);

}
