package eu.rarogsoftware.rarog.platform.api.commons;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Helper tools for extracting data from {@link Class}
 */
public final class ClassHelpers {
    private ClassHelpers() {
    }

    public static List<Class<?>> getImplementedInterfaces(Class<?> componentClass) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (!Object.class.equals(componentClass)) {
            interfaces.addAll(Arrays.asList(componentClass.getInterfaces()));
            componentClass = componentClass.getSuperclass();
        }
        return interfaces;
    }
}
