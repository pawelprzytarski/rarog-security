package eu.rarogsoftware.rarog.platform.api.plugins.web;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import org.springframework.web.servlet.resource.ResourceResolver;

/**
 * Registers custom resource resolver for Spring MVC - {@link ResourceResolver}.
 * Allows to provide resources from not default locations in plugin or from any other location in system.
 *
 * @param resourceResolver list of custom resource resolver
 */
public record ResourceResolverDescriptor(
        ResourceResolver resourceResolver) implements FeatureDescriptor {
}
