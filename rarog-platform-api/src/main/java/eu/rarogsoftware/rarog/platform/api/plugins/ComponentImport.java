package eu.rarogsoftware.rarog.platform.api.plugins;

import java.lang.annotation.*;

/**
 * Informs Spring-OSGi bridge which component should be imported from OSGi.
 * Used by spring-plugin-activator to initialize spring context in plugins.
 * <p>
 *     Usable only on autowired component in plugins spring contexts initiated by spring-plugin-activator library.
 *     Annotated autowired components will be automatically imported from OSGi based on interface type. As OSGi doesn't
 *     support exporting classes that are not interfaces, this annotation only work with interfaces.
 * </p>
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.FIELD})
public @interface ComponentImport {
    /**
     * Name of bean to autowire. If empty will autowire using interface.
     */
    String value() default "";

    /**
     * If true, will not import service until it is needed to autowire.
     */
    boolean lateBind() default false;
}
