package eu.rarogsoftware.rarog.platform.api.user.management;

public interface UserStore {
    Long storeUser(SimpleUser user) throws UserManagementException;

    void overrideUserByUsername(SimpleUser user) throws UserManagementException;

    void updatePassword(String username, String newPassword) throws UserManagementException;

    void updatePassword(long id, String newPassword) throws UserManagementException;

    SimpleUser getUser(String username) throws UserManagementException;

    SimpleUser getUser(Long id) throws UserManagementException;

    void deleteUser(String username) throws UserManagementException;

    boolean userExists(String username);

    boolean userExists(Long id);
}
