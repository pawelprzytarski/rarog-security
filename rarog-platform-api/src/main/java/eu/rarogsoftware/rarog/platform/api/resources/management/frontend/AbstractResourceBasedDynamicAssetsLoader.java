package eu.rarogsoftware.rarog.platform.api.resources.management.frontend;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public abstract class AbstractResourceBasedDynamicAssetsLoader implements DynamicAssetsLoader {
    protected static final ObjectMapper mapper = new ObjectMapper();
    protected static final String ASSETS_FIELD = "assets";
    protected static final String SOURCE_FIELD = "src";
    protected static final String INTEGRITY_FIELD = "integrity";
    protected static final String ENTRYPOINTS_FIELD = "entrypoints";
    protected final String manifestLocation;
    private final Logger logger = LoggerFactory.getLogger(ClassLoaderDynamicAssetsLoader.class);
    protected Long cacheDuration;
    protected ManifestData cachedManifest = null;

    public AbstractResourceBasedDynamicAssetsLoader(String manifestLocation, Long cacheDuration) {
        this.manifestLocation = manifestLocation;
        this.cacheDuration = cacheDuration;
    }

    @Override
    public Optional<EntrypointAssetData> getAssetsForEntryPoint(String entrypoint) {
        return Optional.ofNullable(getManifest().entryPointsData().get(entrypoint));
    }

    @Override
    public Optional<WebpackAssetData> getAssetData(String assetName) {
        return Optional.ofNullable(getManifest().assets().get(assetName));
    }

    public void setCacheDuration(Long cacheDuration) {
        this.cacheDuration = cacheDuration;
    }

    protected ManifestData getManifest() {
        if (cachedManifest == null || isManifestOutdated(cachedManifest)) {
            cachedManifest = loadManifest();
        }
        return cachedManifest;
    }

    protected boolean isManifestOutdated(ManifestData cachedManifest) {
        if (cacheDuration < 0) {
            return false;
        }
        return cachedManifest.loaded() + cacheDuration < Instant.now().toEpochMilli();
    }

    protected ManifestData loadManifest() {
        try (var inputStream = getManifestResourceStream()) {
            if (inputStream == null) {
                logger.warn("Failed to load manifest at {}. No resource found", manifestLocation);
                throw new DynamicManifestNotAvailable("Manifest cannot be found at: " + manifestLocation);
            }
            return readManifest(inputStream);
        } catch (IOException e) {
            logger.warn("Failed to load manifest at {}", manifestLocation, e);
            throw new DynamicManifestNotAvailable("Manifest cannot be read at: " + manifestLocation, e);
        }
    }

    protected abstract InputStream getManifestResourceStream() throws IOException;

    protected ManifestData readManifest(InputStream inputStream) throws IOException {
        var root = mapper.readTree(inputStream);
        var entrypoints = readEntrypoints(root.get(ENTRYPOINTS_FIELD));
        var assets = readAssets(root);
        return new ManifestData(entrypoints, assets, Instant.now().toEpochMilli());
    }

    protected Map<String, EntrypointAssetData> readEntrypoints(JsonNode entrypoints) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(entrypoints.fields(), Spliterator.ORDERED), false)
                .filter(field -> field.getValue().get(ASSETS_FIELD) != null)
                .collect(Collectors.toMap(Map.Entry::getKey, field -> {
                    var assetsNode = field.getValue().get(ASSETS_FIELD);
                    return new EntrypointAssetData(convertAssets(assetsNode, "js"), convertAssets(assetsNode, "css"));
                }));
    }

    protected List<WebpackAssetData> convertAssets(JsonNode assetsNode, String type) {
        return Optional.ofNullable(assetsNode.get(type))
                .filter(JsonNode::isArray)
                .stream()
                .flatMap(node -> StreamSupport.stream(node.spliterator(), false))
                .filter(node -> node.isObject() && node.get(SOURCE_FIELD) != null && node.get(INTEGRITY_FIELD) != null)
                .map(node -> new WebpackAssetData(node.get(SOURCE_FIELD).asText(), node.get(INTEGRITY_FIELD).asText()))
                .toList();
    }

    protected Map<String, WebpackAssetData> readAssets(JsonNode root) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(root.fields(), Spliterator.ORDERED), false)
                .filter(field -> !field.getKey().equals(ENTRYPOINTS_FIELD))
                .filter(field -> {
                    var node = field.getValue();
                    return node.isObject() && node.has(SOURCE_FIELD) && node.has(INTEGRITY_FIELD);
                })
                .collect(Collectors.toMap(Map.Entry::getKey, field -> {
                    var node = field.getValue();
                    return new WebpackAssetData(node.get(SOURCE_FIELD).asText(), node.get(INTEGRITY_FIELD).asText());
                }));
    }

    protected record ManifestData(Map<String, EntrypointAssetData> entryPointsData,
                                  Map<String, WebpackAssetData> assets,
                                  Long loaded) {
    }
}
