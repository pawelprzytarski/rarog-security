package eu.rarogsoftware.rarog.platform.api.i18n;

import java.util.Locale;

/**
 * Simple helper tool for getting translated strings.
 */
public interface I18nHelper {
    /**
     * Special language tag that makes helper to return untranslated translation keys instead of translations.
     */
    String SYNTHETIC_LANGUAGE_TAG = "en-MOON";
    /**
     * Special locale that makes helper to return untranslated translation keys instead of translations.
     */
    Locale SYNTHETIC_LANGUAGE = Locale.forLanguageTag(SYNTHETIC_LANGUAGE_TAG);

    /**
     * Get translated string for provided key for current user locale.
     *
     * @param key  translation key (can be either simple key or pair namespace:key) Examples: `key.to.translate`, `common:key.to.translate`
     * @param args values to pass into translation
     * @return translated string
     */
    String getText(String key, Object... args);

    /**
     * Get translated string for provided key for provided locale.
     *
     * @param locale requested locale of translation
     * @param key    translation key (can be either simple key or pair namespace:key) Examples: `key.to.translate`, `common:key.to.translate`
     * @param args   values to pass into translation
     * @return translated string
     */
    String getText(Locale locale, String key, Object... args);
}
