package eu.rarogsoftware.rarog.platform.api.user.management;

import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Base interface for all types of users in Rarog application.
 * Rarog uses objects of this class instead of standard UserDetails objects.
 * If you need anywhere to get information about user executing request, you should use
 * this class.
 */
public interface RarogUser extends UserDetails, CredentialsContainer {
    GrantedAuthority HUMAN_AUTHORITY = new SimpleGrantedAuthority("TYPE_HUMAN");
    GrantedAuthority APP_AUTHORITY = new SimpleGrantedAuthority("TYPE_APP");

    boolean isHuman();

    boolean isApp();
}
