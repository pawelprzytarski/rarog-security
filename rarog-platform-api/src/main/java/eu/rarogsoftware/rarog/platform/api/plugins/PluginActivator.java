package eu.rarogsoftware.rarog.platform.api.plugins;

import java.util.Set;

/**
 * Special interface that must be implemented and exported by all plugins providing any functionality.
 * This interface is only point which can be used to provide {@link FeatureDescriptor}s in whole plugin.
 * It is directly extracted by <i>PluginManager</i>.
 *
 * <p>
 * Activation order:
 * <ol>
 * <li>Plugin is enabled aka plugin is loaded and ready (specifics depends on plugin type)</li>
 * <li>Resources are prepared for plugin (if necessary) (specifics depends on plugin type)</li>
 * <li>Plugin is activated with {@link PluginActivator#activate()} method</li>
 * <li>Plugin {@link FeatureDescriptor}s are plugged into corresponding {@link FeatureModule}s</li>
 * </ol>
 * </p>
 *
 * <p>
 * Deactivation order:
 * <ol>
 * <li>Plugin {@link FeatureDescriptor}s are unplugged from corresponding {@link FeatureModule}s</li>
 * <li>Plugin is deactivated with {@link PluginActivator#deactivate()} method</li>
 * <li>Resources reserved for plugin are freed (if necessary) (specifics depends on plugin type)</li>
 * <li>Plugin is disabled (specifics depends on plugin type)</li>
 * </ol>
 * </p>
 */
public interface PluginActivator {
    /**
     * Invoked by PluginsManager when plugin is activated.
     * Allows plugin to prepare resources and environment for plugin work.
     * It is invoked before manager imports {@link FeatureDescriptor} list and plug it into feature modules.
     */
    void activate();

    /**
     * Invoked by PluginsManager before plugin is deactivated.
     * Allows plugin to clean up all resources and environment.
     * It is invoked after disabling dependant plugins.
     */
    void deactivate();

    /**
     * Set of {@link FeatureDescriptor}s returned by this method is plugged into registered
     * {@link FeatureModule}s.
     *
     * @return Set of {@link FeatureDescriptor}s provided by plugin
     */
    Set<FeatureDescriptor> getFeatureDescriptors();

    /**
     * Invoked by PluginsManager when plugin is request to remove all its data.
     */
    default void purge() {
    }
}
