package eu.rarogsoftware.rarog.platform.api.plugins;

import java.util.Collection;

/**
 * Provides access to services exported by plugins.
 */
public interface PluginsService {
    /**
     * Returns instance of services that implement specified interface.
     *
     * @param interfaceClass interface of service to retrieve
     * @return instances of service or empty collection if no services found
     * @param <T> type of interface
     */
    <T> Collection<? extends T> getInstancesOf(Class<? extends T> interfaceClass);
}
