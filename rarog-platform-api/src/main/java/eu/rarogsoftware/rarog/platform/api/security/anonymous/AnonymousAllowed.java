package eu.rarogsoftware.rarog.platform.api.security.anonymous;

import java.lang.annotation.*;

/**
 * Informs Spring dispatcher that this endpoint can be accessed by anonymous users.
 * See {@link AnonymousAllowedLevel} for details about disabling anonymous access in product
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AnonymousAllowed {
    /**
     * Level of anonymous access taken into account during disabling system-wide anonymous access.
     * See {@link AnonymousAllowedLevel} for details.
     */
    AnonymousAllowedLevel value() default AnonymousAllowedLevel.BASIC;
}
