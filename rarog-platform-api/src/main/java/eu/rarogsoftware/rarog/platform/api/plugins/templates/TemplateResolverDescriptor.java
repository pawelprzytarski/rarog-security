package eu.rarogsoftware.rarog.platform.api.plugins.templates;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * Registers custom template resolvers for Thymeleaf.
 * Supports plugin request aware {@link PluginTemplateResolver} as well.
 *
 * @param templateResolvers supplier of custom template resolvers
 */
public record TemplateResolverDescriptor(
        Supplier<Collection<ITemplateResolver>> templateResolvers) implements FeatureDescriptor {
}
