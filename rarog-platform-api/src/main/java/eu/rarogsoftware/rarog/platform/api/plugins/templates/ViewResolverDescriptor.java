package eu.rarogsoftware.rarog.platform.api.plugins.templates;

import eu.rarogsoftware.rarog.platform.api.plugins.FeatureDescriptor;
import org.springframework.web.servlet.ViewResolver;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * Registers custom view resolvers for Spring MVC - {@link ViewResolver}.
 * Supports plugin request aware {@link PluginViewResolver}
 *
 * <p>
 *     With this feature descriptor is possible to introduce new templates engines other than Thymeleaf.
 * </p>
 * @param viewResolverSupplier supplier for custom view resolvers
 */
public record ViewResolverDescriptor(
        Supplier<Collection<ViewResolver>> viewResolverSupplier) implements FeatureDescriptor {
}
