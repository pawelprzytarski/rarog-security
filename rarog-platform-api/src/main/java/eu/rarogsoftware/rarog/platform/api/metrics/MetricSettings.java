package eu.rarogsoftware.rarog.platform.api.metrics;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * The most common settings of metric.
 * It represents metric that doesn't contain any complicated settings like {@link HistogramSettings}.
 */
@JsonTypeName("metricSettings")
public class MetricSettings extends BaseMetricSettings<MetricSettings> {
    /**
     * @return empty instance of settings object. Convenience method.
     */
    public static MetricSettings settings() {
        return new MetricSettings();
    }
}

